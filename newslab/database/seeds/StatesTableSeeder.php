<?php

use App\City;
use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    use CSVSeeder;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = public_path('state.csv');
        $states = $this->csv_to_array($csvFile);

        foreach($states as $state)
        {
            DB::table('state')->insert([
                'data' => $state['data'],
                'state' => $state['state'],
                'default_price' => $state['default_price']
            ]);
        }
    }
}
