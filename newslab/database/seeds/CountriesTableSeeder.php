<?php

use App\City;
use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;


class CountriesTableSeeder extends Seeder
{
    use CSVSeeder;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->insert([
            'code'=>12,
            'name'=>'Uk',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('countries')->insert([
            'code'=>34,
            'name'=>'USA',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('countries')->insert([
            'code'=>12,
            'name'=>'Australlia',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('countries')->insert([
            'code'=>31,
            'name'=>'Brazil',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
    }
}
