<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class NotificationListingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('notifications_listing')->insert([
            'id'=>5,
            'name'=>'Change contact number',
            'field_name'=>'change_contact_number',
            'role_id'=>4,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>6,
            'name'=>'Change payment method',
            'field_name'=>'change_payment_method',
            'role_id'=>3,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>7,
            'name'=>'Available Campaign',
            'field_name'=>'approved_campaign',
            'role_id'=>4,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>11,
            'name'=>'When receive payment',
            'field_name'=>'When_receive_payment',
            'role_id'=>4,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>12,
            'name'=>'Upload records of completed campaign',
            'field_name'=>'upload_records_of_completed_campaign',
            'role_id'=>4,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>13,
            'name'=>'When sticker has been shippped',
            'field_name'=>'When_sticker_has_been_shipped',
            'role_id'=>4,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>16,
            'name'=>'Available Campaign',
            'field_name'=>'approved_campaign',
            'role_id'=>3,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>26,
            'name'=>'Campaign Request Approvel',
            'field_name'=>'campaign_approved_by_admin',
            'role_id'=>4,
            'status'=>0,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>27,
            'name'=>'Campaign Request Approvel',
            'field_name'=>'campaign_approved_by_admin',
            'role_id'=>3,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>29,
            'name'=>'support notification',
            'field_name'=>'support',
            'role_id'=>0,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>34,
            'name'=>'support email',
            'field_name'=>'support_email',
            'role_id'=>0,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>35,
            'name'=>'Sign up',
            'field_name'=>'sign_up',
            'role_id'=>1,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>38,
            'name'=>'Change Password',
            'field_name'=>'change_password',
            'role_id'=>0,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>39,
            'name'=>'Campaign Request Approved',
            'field_name'=>'campaign_request_approved',
            'role_id'=>4,
            'status'=>0,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>40,
            'name'=>'Campaign Request Approved',
            'field_name'=>'campaign_request_approved',
            'role_id'=>3,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('notifications_listing')->insert([
            'id'=>41,
            'name'=>'Campaign create',
            'field_name'=>'campaign_create',
            'role_id'=>1,
            'status'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

    }
}
