<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'id'=>1,
            'name'=>'Sales',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('departments')->insert([
            'id'=>2,
            'name'=>'operational',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('departments')->insert([
            'id'=>3,
            'name'=>'finance',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('departments')->insert([
            'id'=>4,
            'name'=>'Techincal',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('departments')->insert([
            'id'=>5,
            'name'=>'Marketing',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('departments')->insert([
            'id'=>6,
            'name'=>'support',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('departments')->insert([
            'id'=>9999,
            'name'=>'admin',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('statuses')->insert([
            'id'=>1,
            'name'=>'Pending for payment',
            'class'=>'primary',
            'is_active'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('statuses')->insert([
            'id'=>2,
            'name'=>'In progress',
            'class'=>'info',
            'is_active'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('statuses')->insert([
            'id'=>3,
            'name'=>'Approved',
            'class'=>'success',
            'is_active'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('statuses')->insert([
            'id'=>4,
            'name'=>'Rejected',
            'class'=>'danger',
            'is_active'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('statuses')->insert([
            'id'=>5,
            'name'=>'Cancelled',
            'class'=>'warning',
            'is_active'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('statuses')->insert([
            'id'=>6,
            'name'=>'In Review',
            'class'=>'primary',
            'is_active'=>1,
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);




    }
}
