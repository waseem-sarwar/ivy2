<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
        {   DB::table('users')->insert([
            'name'=>'fake',
            'email'=>'fake@fake.com',
//            'email_verified_at'=>now(),
            'password' => Hash::make('20thfloor'),
            'role_id'=>'2',
            'department_id'=>'',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);

        DB::table('users')->insert([
            'name'=>'advertisor_20thfloor',
            'email'=>'advertisor@gmail.com',
            'email_verified_at'=>now(),
            'password' => Hash::make('20thfloor'),
            'role_id'=>'2',
            'department_id'=>'',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('users')->insert([
            'name'=>'driver_20thfloor',
            'email'=>'driver@gmail.com',
            'email_verified_at'=>now(),
            'password' => Hash::make('20thfloor'),
            'role_id'=>'3',
            'department_id'=>'',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('users')->insert([
            'name'=>'ent_driver_20thfloor',
            'email'=>'ent_driver@gmail.com',
            'email_verified_at'=>now(),
            'password' => Hash::make('20thfloor'),
            'department_id'=>'',
            'role_id'=>'4',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
        DB::table('admins')->insert([
            'id'=>0,
            'name'=>'Admin_20thfloor',
            'email'=>'admin@gmail.com',
            'email_verified_at'=>now(),
            'password' => Hash::make('20thfloor'),
            'role_id'=>'1',
            'department_id'=>'9999',
            'created_at'=>now(),
            'updated_at'=>now(),
        ]);
    }
}
