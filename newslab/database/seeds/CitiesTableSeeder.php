<?php

use App\City;
use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    use CSVSeeder;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = public_path('city.csv');
        $data = $this->csv_to_array($csvFile);
        foreach($data as $entry)
        {
            DB::table('city')->insert([
                'primary_city' => $entry['primary_city'],
                'state' => $entry['state'],
                'state_id' => $entry['state_id']
            ]);
        }
    }
}
