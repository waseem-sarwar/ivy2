<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name'=>'admin',
        ]);
        DB::table('roles')->insert([
            'name'=>'advertisor',
        ]);
        DB::table('roles')->insert([
            'name'=>'driver',
        ]);
        DB::table('roles')->insert([
            'name'=>'ent_driver',
        ]);
    }
}
