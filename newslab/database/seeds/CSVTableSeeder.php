<?php

use App\City;
use App\State;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CSVTableSeeder extends Seeder
{
        use CSVSeeder;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = public_path('state.csv');
        $states = $this->csv_to_array($csvFile);

        foreach($states as $state)
        {
            DB::table('state')->insert([
                'data' => $state['data'],
                'state' => $state['state'],
                'default_price' => $state['default_price']
            ]);
        }


//        $csvFile = public_path('city.csv');
//        $data = $this->csv_to_array($csvFile);
//        foreach($data as $entry)
//        {
//            DB::table('city')->insert([
//                'primary_city' => $entry['primary_city'],
//                'state' => $entry['state'],
//                'state_id' => $entry['state_id']
//            ]);
//        }
    }
}
