<?php


use App\City;
use App\State;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ZipsTableSeeder extends Seeder
{
    use CSVSeeder;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csvFile = public_path('zips.csv');
        $data = $this->csv_to_array($csvFile);
        foreach($data as $entry)
        {
            DB::table('zips')->insert([
                'primay_city' => $entry['primay_city'],
                'zip' => $entry['zip'],
                'city_id' => $entry['city_id']
            ]);
        }
    }
}
