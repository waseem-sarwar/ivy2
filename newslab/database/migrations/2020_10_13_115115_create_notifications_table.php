<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('subject')->nullable();
            $table->string('detail')->nullable();
            $table->unsignedBigInteger('notification_type_id')->nullable();
            $table->foreign('notification_type_id')->references('id')->on('notifications_listing')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('is_read')->default(0);
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
//            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
