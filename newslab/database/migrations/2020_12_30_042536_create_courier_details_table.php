<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourierDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courier_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('applied_campaigns_id')->nullable();
            $table->foreign('applied_campaigns_id')->references('id')->on('applied_campaigns')->onDelete('cascade')->onUpdate('cascade');
            $table->string('courier_company')->nullable();
            $table->string('tracking_id')->nullable();
            $table->string('address')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courier_details');
    }
}
