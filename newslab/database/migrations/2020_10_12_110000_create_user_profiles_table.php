<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_profiles', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('job_designation')->nullable();
            $table->string('company_name')->nullable();
            $table->string('industry')->nullable();
            $table->string('profile_pic')->nullable();
            $table->string('type_of_company')->nullable();
            $table->string('contact_no')->nullable();
            $table->string('dob')->nullable();
            $table->unsignedBigInteger('country_id')->unsigned()->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onUpdate('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('state_id')->unsigned()->nullable();
            $table->foreign('state_id')->references('id')->on('state')->onUpdate('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('city_id')->unsigned()->nullable();
            $table->foreign('city_id')->references('id')->on('city')->onUpdate('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('address')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('company_size')->nullable();
            $table->string('no_of_drivers')->nullable();
            $table->string('registration_no')->nullable();
            $table->string('street_address')->nullable();
            $table->timestamps();

//            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_profiles');
    }
}
