<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaignCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_cars', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('campaign_id')->nullable();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('car_location')->nullable();
//            $table->foreign('car_location')->references('id')->on('campaign_locations')->onDelete('cascade')->onUpdate('cascade');
            $table->string('make')->nullable();
            $table->string('type')->nullable();
            $table->string('model')->nullable();
            $table->string('year')->nullable();
            $table->string('no_of_cars')->nullable();
            $table->string('price_per_mile')->nullable();
            $table->string('drive_days')->nullable();
            $table->string('zip')->nullable();
            $table->string('mileage')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaign_cars');
    }
}
