<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverEnrollmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_enrollments', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('address')->nullable();
            $table->string('country_personal')->nullable();
            $table->string('city_personal')->nullable();
            $table->string('street_personal')->nullable();
            $table->string('billing_address_personal')->nullable();
            $table->string('zip_code_personal')->nullable();
            $table->string('zip_code')->nullable();
            $table->string('car_make')->nullable();
            $table->string('car_type')->nullable();
            $table->string('car_model')->nullable();
            $table->string('car_name')->nullable();
            $table->string('car_year')->nullable();
            $table->string('car_photo')->nullable();
            $table->string('car_video')->nullable();
            $table->string('car_milage_proof')->nullable();
            $table->string('car_registration_proof')->nullable();
            $table->string('driving_state')->nullable();
            $table->string('car_drive_city')->nullable();
            $table->string('car_drive_address')->nullable();
            $table->string('car_drive_country')->nullable();
            $table->string('car_drive_zip')->nullable();
            $table->string('car_location')->nullable();
            $table->string('car_city')->nullable();
            $table->string('car_address')->nullable();
            $table->string('cnic_path')->nullable();
            $table->string('days')->nullable();
            $table->string('mileage_of_car')->nullable();
            $table->string('days_drive_in_month')->nullable();
            $table->string('hours_drive_a_car')->nullable();
            $table->string('driving_license_path')->nullable();
            $table->string('cnic')->nullable();
            $table->string('duration interval')->nullable();
            $table->string('milage_drive')->nullable();
            $table->unsignedBigInteger('country_id')->nullable();
            $table->foreign('country_id')->references('id')->on('countries')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('state_id')->nullable();
            $table->foreign('state_id')->references('id')->on('state')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('city_id')->references('id')->on('city')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('city_id')->nullable()->unsigned();
            $table->unsignedBigInteger('created_by')->nullable()->unsigned();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_enrollments');
    }
}
