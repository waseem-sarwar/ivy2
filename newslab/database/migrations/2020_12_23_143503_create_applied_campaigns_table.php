<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppliedCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applied_campaigns', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('campaign_id')->nullable();
            $table->foreign('campaign_id')->references('id')->on('campaigns')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('state_id')->nullable();
            $table->foreign('state_id')->references('id')->on('state')->onDelete('cascade')->onUpdate('cascade');
            $table->string('status')->nullable();
            $table->string('giving_amount')->nullable();
            $table->string('amount_gives')->nullable();
            $table->string('courier_driver_feedback',1000)->nullable();
            $table->string('documents_feedback',1000)->nullable();
            $table->string('document_sticker_feedback',1000)->nullable();
            $table->enum('courier_status',['Mail sent','Mail not sent','Mail Acknowledged','Courier resend request'])->nullable();
            $table->enum('documents_status',['Waits for sticker video upload','Waiting for video sticker approval','Waiting for miles','Waiting for starting miles approval','Miles approved','Waiting for resend miles','Waiting for resend sticker'])->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applied_campaigns');
    }
}
