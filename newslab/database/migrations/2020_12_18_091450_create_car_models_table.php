<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('car_models', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->foreign('company_id')->references('id')->on('car_companies')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('car_name_id')->nullable();
            $table->foreign('car_name_id')->references('id')->on('car_name')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('car_type')->nullable();
            $table->foreign('car_type')->references('id')->on('car_type')->onDelete('cascade')->onUpdate('cascade');
            $table->unsignedBigInteger('car_model_id')->nullable();
            $table->foreign('car_model_id')->references('id')->on('create_car_model')->onDelete('cascade')->onUpdate('cascade');
            $table->boolean('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('car_models');
    }
}
