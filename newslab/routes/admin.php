<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();
    //dd($users);
    return redirect('/');;
})->name('home');

Route::get('/state-price-export/{state_id}','adminController@exportState')->name('state.price.export');
Route::get('/campaigns-export/','adminController@exportCampaign')->name('campaigns.export');
Route::get('/advertisor-payment-export','adminController@exportAdvertisorPayment')->name('advertiser.payments.export');
Route::get('/driver-payment-export','adminController@exportDriverPayment')->name('driver.payments.export');
Route::get( '/delete-range/{id}', 'PagesController@deleteRange')->name('delete.range');;
Route::get( '/status-update-range/{id}', 'PagesController@statusUpdateRange')->name('status.update.range');
Route::get('/setHighPriceStatus', 'PagesController@setHighPriceStatus')->name('setHighPriceStatus');

Route::get('/campaign-detail/{slug}', 'PagesController@campaignDetail')->name('camp.detail');

Route::get('/dashboard', 'adminController@index')->name('admin.dashboard');
//Manage Campaign
Route::get('/campaigns', 'adminController@manage_campaign')->name('campaigns.listing');
Route::post('/campaignStatusUpdate', 'PagesController@campaignStatusUpdate')->name('campaign.status.update');
Route::post('/campaignbespokeUpdate', 'PagesController@campaignbespokeUpdate')->name('campaignbespokeUpdate');

Route::get('/state-price-range-check', 'PagesController@getRangeAvailabeCheck')->name('miles.available.check');


Route::get('/car-info', 'adminController@carInfo')->name('carInfo');
Route::get('/campaign-type', 'adminController@campaignType')->name('campaignType');
Route::get('/delete-type{id}', 'adminController@deleteType')->name('deleteType');
Route::post('add-maker', 'adminController@addMaker')->name('addMaker');
Route::get('filter/car/{id}', 'adminController@filterCar')->name('filterCar');
Route::get('delete-car/{id}', 'adminController@deleteCar')->name('deleteCar');
Route::post('create-car', 'adminController@createCar')->name('createCar');
Route::post('/make-car', 'adminController@makeCar')->name('makeCar');
Route::get('/make-car', 'adminController@makeCar1')->name('make-car');
Route::get('/make-model', 'adminController@makeModel1')->name('make-model');
Route::get('/get/car/{id}', 'adminController@getCar')->name('getCar');


Route::get('/chats', 'adminController@chats');
Route::get('/tickets', 'PagesController@tickets')->name('admin.tickets');
Route::get('/notificationsa', 'adminController@advertiser_notifications');
Route::get('/notifications', 'PagesController@notifications')->name('notifications');



Route::get('/manage-state-price', 'PagesController@managePrice')->name('priceRanges');

//Route::get('/Dashboard/tickets', 'PagesController@tickets')->name('admin.tickets');

Route::get('reports', 'adminController@reports');
//Route::get('payments', 'adminController@payments');


Route::get('/personal_information', 'DriverEntController@personal_information')->name('admin.personal.info');

Route::get('/change-password', 'DriverEntController@admin_change_password')->name('admin.change.password');

Route::get('/notification-setting', 'DriverEntController@admin_notification_setting')->name('admin.notification.setting');


Route::post('/personal-update', 'PagesController@admin_update')->name('admin.update');
Route::post('/update-passwordd', 'PagesController@admin_update_password')->name('admin.update.password');
Route::post('admin-update-notification', 'PagesController@admin_update_notification')->name('update.notification');


Route::get('/add-departments','DriverEntController@addDepartments')->name('add.departments');
Route::post('/create-department','DriverEntController@createDepartment')->name('create.department');
Route::get('/disable-department/{id}','DriverEntController@disableDepartment')->name('disable.department');
Route::get('/enable-department/{id}','DriverEntController@enableDepartment')->name('enable.department');

Route::get('/add-industry','DriverEntController@addIndustry')->name('add.industry');
Route::post('/create-industry','DriverEntController@createIndustry')->name('create.industry');
Route::get('/disable-industry/{id}','DriverEntController@disableIndustry')->name('disable.industry');
Route::get('/enable-industry/{id}','DriverEntController@enableIndustry')->name('enable.industry');

Route::get('/ticket-types-list','DriverEntController@tickettypes')->name('ticket.type');
Route::post('/create-ticket-type','DriverEntController@createTicketType')->name('create.ticket.type');
Route::get('/disable-ticket-type/{id}','DriverEntController@disableTicketType')->name('disable.ticket.type');
Route::get('/enable-ticket-type/{id}','DriverEntController@enableTicketType')->name('enable.ticket.type');


Route::get('/advertisors','HomeController@getAllAdvertisors')->name('getAllAdvertisors');
Route::get('/drivers','HomeController@getAllDrivers')->name('getAllDrivers');
Route::get('/driver-campaigns/{id}','HomeController@appliedCampaigns')->name('getDriverCampaigns');
Route::get('/ent-drivers','HomeController@getAllEntDrivers')->name('getAllEntDrivers');
Route::get('/user-status-update','HomeController@userStatusUpdate')->name('userStatusUpdate');

Route::get('/manage-notifications', 'PagesController@manageNotifications');

Route::post('/create-notification', 'PagesController@createNotification')->name('create.notification');
Route::post('/update-notification', 'PagesController@updateNotification')->name('update.notification1');
Route::post('/updateStatesPrices', 'PagesController@updateStatesPrices')->name('update.states.prices');
//Route::post('/update-password', 'PagesController@update_password')->name('update.password');


Route::post( '/updateHighPercentage', 'PagesController@updateHighPercentage')->name('update.highpercentage');
Route::post('ticket-thread', 'PagesController@ticketThread')->name('ticket.thread.create');

Route::post('/create-type', 'adminController@createType')->name('createType');
Route::post('/create-template', 'StickerTemplatesController@createTemplate')->name('createTemplate');
Route::post('/update-template', 'StickerTemplatesController@updateTemplate')->name('updateTemplate');
Route::post('/create-template-color', 'StickerTemplatesController@createTemplateColor')->name('createTemplate.color');
Route::post('/sticker-create-type', 'StickerTemplatesController@createType')->name('stickerCreateType');
Route::post('/sticker-update-type', 'StickerTemplatesController@stickerUpdateType')->name('stickerUpdateType');

Route::get('/get/model/{id}', 'adminController@getModel')->name('getModel');
Route::get('/get-models/{id}', 'adminController@getModels')->name('get-models');
Route::post('make-model', 'adminController@makeModel')->name('makeModel');
Route::post('add-type', 'adminController@addType')->name('addType');
Route::get('add-type', 'adminController@addType1')->name('add-type');

Route::get('/claimStatusUpdate', 'PagesController@claimStatusUpdate')->name('claimStatusUpdate');
Route::post('/driver-transaction', 'PagesController@driverTransaction')->name('driverTransaction');


Route::get( '/sendCourierMailDriver', 'PagesController@sendCourierMailDriver')->name('sendCourierMailDriver');
Route::get( '/driverApproveSticker', 'PagesController@driverApproveSticker')->name('driverApproveSticker');
Route::get( '/driverApproveMiles', 'PagesController@driverApproveMiles')->name('driverApproveMiles');
Route::get( '/driver-reject-miles', 'PagesController@driverRejectMiles')->name('driverRejectMiles');
Route::get( '/driver-reject-sticker', 'PagesController@driverRejectSticker')->name('driverRejectSticker');
Route::get( '/getStickerDetails', 'PagesController@getStickerDetails')->name('getStickerDetails');
Route::get( '/getCourierDetails', 'PagesController@getCourierDetails')->name('getCourierDetails');
Route::get( '/sendCourierDetailsMail', 'PagesController@sendCourierMailDriver1')->name('sendCourierDetailsMail');
Route::get( '/send-courier-details-with-mail', 'PagesController@sendCourierMailDriver2')->name('sendCourierDetailsMail2');
Route::get( '/getDriverDocuments', 'PagesController@getDriverDocuments')->name('getDriverDocuments');
Route::get( '/getDriverCompletedDocuments', 'PagesController@getDriverCompletedDocuments')->name('getDriverCompletedDocuments');
Route::get( '/driverApproveStartDocuments', 'PagesController@driverApproveStartDocuments')->name('driverApproveStartDocuments');
Route::get ( '/adv-payments','PaymentsController@getAllPayments')->name('adv.payments');
Route::get ( '/drv-payments','PaymentsController@getAllPaymentsdrivers')->name('drv.payments');

Route::get ( '/stickers','StickerTemplatesController@index')->name('stickers.index');
