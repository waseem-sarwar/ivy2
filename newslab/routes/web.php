<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//site
//rehman urls

Route::get('/cron', function () {
    Artisan::call('daily:notification');
});


Route::get('/Advertiser/Dashboard/view-campaign/{id}', 'PagesController@view_campaign');
//rehman urls ends

Route::get('/', 'PagesController@home')->name('hom');

Route::get('/about', 'PagesController@about')->name('about');
Route::get('/adv/sign-up', 'PagesController@advertisor1')->name('advSignup');
Route::get('/Signup/Advertiser-Business', 'PagesController@advertisor2');
Route::get('/contact', 'PagesController@contact');
Route::get('/drv/sign-up', 'PagesController@driver1')->name('drvSignup');
Route::get('/Signup/Driver/Driver-Info', 'PagesController@driver2');
Route::get('/faqs', 'PagesController@faqs');
Route::get('/how-it-works', 'PagesController@how_works');
Route::get('/login-site', 'PagesController@login_home');
Route::get('/reset-password', 'PagesController@reset_password');
Route::get('/terms', 'PagesController@terms')->name('terms');
Route::get('/copy', 'PagesController@copy');

Route::get('/admin/dashboard', 'PagesController@index');
Route::get('/Advertiser/Dashboard/', 'PagesController@advertisor_home');
Route::get('/dashboard', 'adminController@index')->name('dashboard');
//create Campaign

Route::get('/Advertiser/Dashboard/edit-campaign', 'PagesController@edit_campaign');
Route::get('/Advertiser/Dashboard/create-campaign-step2', 'PagesController@create_campaign_step2');
//manage campaign




//Chats


Route::get('/Advertiser/Dashboard/Notifications', 'PagesController@advertiser_notifications');

//Profile
Route::get('/Advertiser/Dashboard/personal-information', 'PagesController@personal_information');
Route::get('/Advertiser/Dashboard/account-information', 'PagesController@account_information');

Route::get('/Advertiser/Dashboard/change-password', 'PagesController@adv_change_password');
Route::get('/Advertiser/Dashboard/notification-setting', 'PagesController@advertisor_notification_setting');
Route::get('Advertiser/Dashboard/Reports', 'PagesController@reports');
//Route::get('Advertiser/Dashboard/Payments', 'PagesController@payments');











Route::get('/Driver/Dashboard/', 'PagesController@driver_home');
//manage campaign



//Route::post('/create-type', 'adminController@createType')->name('createType');
//manage campaign
//Route::get('/Driver/Dashboard/Manage-Campaign', 'PagesController@driver_manage_campaign');
Route::get('/Driver/Dashboard/Enrollment', 'DriverEntController@basic_enrollment');

//Chats
Route::get('/Driver/Dashboard/chats', 'PagesController@driver_chats');
Route::get('/Driver/Dashboard/tickets', 'PagesController@driver_tickets');
Route::get('/Driver/Dashboard/Notifications', 'PagesController@driver_notifications');
Route::get('messages', 'ChatsController@fetchMessages');
Route::post('messages', 'ChatsController@sendMessage');

//Profile
Route::get('/Driver/Dashboard/personal-information', 'PagesController@driver_personal_information');
Route::get('/Driver/Dashboard/account-information', 'PagesController@driver_account_information');
Route::get('/Driver/Dashboard/change-password', 'PagesController@driver_change_password');
Route::get('/Driver/Dashboard/notification-setting', 'PagesController@driver_notification_setting');

Route::get('/Driver/Dashboard/Reports', 'PagesController@driver_reports');

Route::get('Driver/Dashboard/Payment-details', 'PagesController@driver_payment_details');


Route::get('/Driver/Ent/Dashboard', 'DriverEntController@ent_driver_home');

//useless routes
Route::get('/personal_information', 'DriverEntController@personal_information')->name('personal.info');
Route::get('/bussiness-information', 'DriverEntController@account_information')->name('bussiness.info');
Route::get('/change-password', 'DriverEntController@driver_change_password')->name('change.password');
Route::get('/notification-setting', 'DriverEntController@notification_setting')->name('notification.setting');
//Enrollment

//Route::get('/enrollment-update/{id}', 'DriverEntController@basic_enrollment_update');
//Route::get('/enrollment-update/{id}', 'DriverEntController@basic_enrollment_update');

//manage campaign
Route::get('/campaigns', 'DriverEntController@manage_campaign');
//Chats
Route::get('/Driver/Ent/Dashboard/chats', 'DriverEntController@chats');
Route::get('/Driver/Ent/Dashboard/tickets', 'DriverEntController@tickets');
Route::get('/Driver/Ent/Dashboard/Notifications', 'DriverEntController@driver_notifications');
//Profile


//Route::get('/personal_information', 'DriverEntController@personal_information')->name('personal.info');
//Route::get('/bussiness-information', 'DriverEntController@account_information')->name('bussiness.info');
//Route::get('/change-password', 'DriverEntController@driver_change_password')->name('change.password');
//Route::get('/notification-setting', 'DriverEntController@notification_setting')->name('notification.setting');



Route::get('/Driver/Ent/Dashboard/Reports', 'PagesController@driver_ent_reports');
Route::get('/Driver/Ent/Dashboard/Payments', 'PagesController@driver_ent_payments');
Route::get('Driver/Ent/Dashboard/Payment-details', 'PagesController@driver_ent_payment_details');




//Chats
Route::get('/chats', 'PagesController@chats');
Route::get('/tickets', 'PagesController@tickets');

//Profile
Route::get('/personal-information', 'PagesController@personal_information')->name('personalInformation');
Route::get('/account-information', 'PagesController@account_information');




// Demo routes
//Route::get('/datatables', 'PagesController@datatables');
//Route::get('/ktdatatables', 'PagesController@ktDatatables');
//Route::get('/select2', 'PagesController@select2');
//Route::get('/icons/custom-icons', 'PagesController@customIcons');
//Route::get('/icons/flaticon', 'PagesController@flaticon');
//Route::get('/icons/fontawesome', 'PagesController@fontawesome');
//Route::get('/icons/lineawesome', 'PagesController@lineawesome');
//Route::get('/icons/socicons', 'PagesController@socicons');
//Route::get('/icons/svg', 'PagesController@svg');

// Quick search dummy route to display html elements in search dropdown (header search)
Route::get('/quick-search', 'PagesController@quickSearch')->name('quick-search');
Route::post('/check', 'PagesController@index')->name('check');
//Route::post('/login-site-thok', 'PagesController@login_site_thok')->name('login-site-thok');
Route::post('/login-site-thok2', 'PagesController@index')->name('login-site-thok');
Route::get('/ajaxGetCountries', 'PagesController@ajaxGetCountries');

//Admin Routes
Route::get('/Admin/Dashboard', 'adminController@index');

//Admin Create Campaign
Route::get('/Admin/Dashboard/create-campaign-step2', 'adminController@create_campaign_step2');



//Admin Chats


//Admin Campaign Settings
//
//Route::get('/get/model/{id}', 'adminController@getModel')->name('getModel');
//Route::get('/get-models/{id}', 'adminController@getModels')->name('get-models');
//Route::post('make-model', 'adminController@makeModel')->name('makeModel');
//Route::post('add-type', 'adminController@addType')->name('addType');
//Route::get('add-type', 'adminController@addType1')->name('add-type');





//create campaigns car filters


//Route::get('car-info', 'adminController@carInfo')->name('carInfo');

//Profile
//Route::get('/Admin/Dashboard/personal-information', 'adminController@personal_information');
//Route::get('/Admin/Dashboard/account-information', 'adminController@account_information');

Route::get('/Admin/Dashboard/change-password', 'adminController@driver_change_password');
Route::get('/Admin/Dashboard/notification-setting', 'adminController@advertisor_notification_setting');

// Manage user
Route::get('/Admin/Dashboard/manage-users', 'adminController@admin_manage_users');


Route::get('/Admin/Dashboard/Payment-details', 'adminController@payment_details');


//Route::get('/campaign-detail/{id}', 'PagesController@campaignDetail')->name('camp.detail');

//ajax calls

Route::get('/getCities', 'PagesController@getCities')->name('getCities');


Route::get('/get-cities', 'PagesController@get_cities')->name('get.cities');
Route::get('/get-zips', 'PagesController@get_zips')->name('get.zips');


Route::get( '/getDriverGivingAmount', 'PagesController@getDriverGivingAmount');


Auth::routes(['verify' => true]);

//Route::get('/home', 'PagesController@home')->name('home');


Route::post ( '/upload-img','PagesController@img');

// Driver Earninf
Route::post( '/driver-earning','adminController@driverEarning')->name('driverEarning');

Route::get('/forget-password','Auth\ForgotPasswordController@showLinkRequestForm')->name('forgetPassword');
Route::get('/reset-page',function (){return view('pages.site.reset_password');});

//after making account
Route::get ( '/thankyou',function (){
    return view('pages.thankyou');
});

Route::get ( 'adv/log-in',function (){return view('pages.site.login_home');})->name('adv.login');
Route::get ( 'drv/log-in',function (){return view('pages.site.login_home');})->name('drv.login');
Route::get ( '/administrator-access',function (){return view('pages.site.login_home');})->name('admin.login');

Route::get('/clear', function () {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
//    only call below command when new project uploaded.first time
//    Artisan::call('storage:link');
    return "Cleared!";
});

Route::get('/message/{id}','ChatsController@getMessage')->name('message');
Route::post('/message','ChatsController@sendMessage');
Route::get('/state-price-export/{state_id}','adminController@exportState')->name('state.price.export');


//department
Route::group(['prefix' => 'admin'], function () {
  Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('login');
  Route::post('/login', 'Admin\Auth\LoginController@login');
  Route::post('/logout', 'Admin\Auth\LoginController@logout')->name('logout');

  Route::get('/register', 'Admin\Auth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'Admin\Auth\RegisterController@register');

  Route::post('/password/email', 'Admin\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'Admin\Auth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.reset');
  Route::get('/password/reset/{token}', 'Admin\Auth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'user'], function () {


    Route::get('/login', 'User\Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'User\Auth\LoginController@login');
    Route::post('/logout', 'User\Auth\LoginController@logout')->name('logout');

    Route::get('/register', 'User\Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'User\Auth\RegisterController@register');

    Route::post('/password/email', 'User\Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'User\Auth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'User\Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'User\Auth\ResetPasswordController@showResetForm');
});
