<?php

Route::get('/home', function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('user')->user();
    //dd($users);
    return redirect('/');
})->name('home');

Route::get('/dashboard', 'adminController@index')->name('dashboard');
//Auth::routes(['verify' => true]);

//create campaigns car filters
Route::get('/get/cars/for/{id}', 'PagesController@getCarsForCampaigns')->name('get.cars.for.compaigns');
Route::get('/get/year/for/{id}', 'PagesController@getYearForCampaigns')->name('get.year.for.compaigns');
Route::get('/get/type/for/{id}', 'PagesController@getTypeForCampaigns')->name('get.type.for.compaigns');

Route::get('/state-price-range-check', 'PagesController@statePriceRangeCheck')->name('miles.get.price');
Route::post('user/Payments', 'PagesController@payment_store')->name('advertisor.payment.store');
Route::post('Advertiser/Dashboard/Payments/Update', 'PagesController@payment_update')->name('advertisor.payment.update');
Route::get('payments/delete', 'PagesController@payment_delete')->name('advertisor.payment.delete');
Route::post('Advertiser/Dashboard/Payments/import', 'PagesController@payment_import')->name('advertisor.payment.import');
Route::get('/notifications', 'PagesController@notifications')->name('notifications');

Route::get('/dashboard', 'adminController@index')->name('dashboard');
Route::post('/upload-sticker', 'PagesController@uploadSticker')->name('uploadSticker');

Route::get('/campaign-detail/{slug}', 'PagesController@campaignDetail')->name('camp.detail')->middleware('role:2|3');

Route::get('/personal_information', 'DriverEntController@personal_information')->name('personal.info');
Route::get('/email_update', 'DriverEntController@email_update')->name('email_update');
Route::get('/bussiness-information', 'DriverEntController@account_information')->name('bussiness.info')->middleware('role:2|4');
Route::get('/change-password', 'DriverEntController@driver_change_password')->name('change.password');
Route::get('/notification-setting', 'DriverEntController@notification_setting')->name('notification.setting');

Route::post('/personal-update', 'PagesController@driver_update')->name('update.personal');

Route::post('ticket-thread', 'PagesController@ticketThread')->name('ticket.thread.create');


Route::get('/payments', 'PagesController@driver_payments');

Route::post('/update-password', 'PagesController@admin_update_password')->name('update.password');
Route::post('user-update-bussiness', 'PagesController@driver_update_bussiness')->name('update.bussiness');
Route::post('user-update-notification', 'PagesController@driver_update_notification')->name('update.notification');
Route::post('/update-password', 'PagesController@update_password')->name('update.password');

//generic
Route::post('/claim', 'PagesController@claim')->name('claim');
Route::get('/availability-status-update', 'PagesController@availabilityStatusUpdate')->name('availabilityStatusUpdate');

Route::get('chats', 'PagesController@chats');
Route::get('tickets', 'PagesController@tickets')->name('userTickets');

Route::get('/payment-credentials', 'PagesController@payment_details');
Route::post('ticketStore', 'PagesController@ticketStore')->name('ticket.store');
Route::get('ticketClose', 'PagesController@ticketClose')->name('ticket.close');

Route::Post('/user/enrollmentUpdate', 'DriverEntController@enrollmentUpdate')->name('enrollment.update');
Route::get('/enrollment-update/{id}', 'DriverEntController@basic_enrollment_update');

//advertisorruser.stripeCharge
Route::get('create-campaign', 'PagesController@create_campaign')->middleware('role:2');
Route::get('create_campaign', 'PagesController@createCampaign2')->middleware('role:2');
Route::get('campaign-listing', 'PagesController@manage_campaign')->name('campaigns')->middleware('role:2');

//Ajax
Route::post( '/createCampaign', 'PagesController@createCampaign')->name('create.campaign.data')->middleware('role:2');
Route::get( '/gettemplatesdata', 'PagesController@gettemplatesdata')->name('gettemplatesdata')->middleware('role:2');

Route::get('/campaigns', 'DriverEntController@manage_campaign')->name('driverManagecampaign')->name('campaigns')->middleware('role:3');

Route::get('/enroll-update', 'DriverEntController@enrollment_update')->middleware('role:3|4');
Route::get('/ent/drivers', 'DriverEntController@drivers')->name('drivers')->middleware('role:4');
Route::get('/driver-campaigns/{id}','HomeController@appliedCampaigns')->name('getDriverCampaigns')->middleware('role:4');

Route::get('/enrollment', 'DriverEntController@basic_enrollment')->name('driver.enrollment')->middleware('role:3|4');
Route::get('/my-campaigns', 'DriverEntController@myCampaigns')->middleware('role:3');

Route::Post('/enrollmentCreate', 'DriverEntController@enrollmentCreate')->name('enrollment.create')->middleware('role:3|4');
Route::get('/ent-campaigns', 'DriverEntController@manage_ent_campaign')->middleware('role:4');
Route::Post('/driver/enrollmentcreate_driver', 'DriverEntController@enrollmentCreate1')->name('enrollment.create.driver');
Route::post( '/campaign-start-documents', 'PagesController@campaignStartDocuments')->name('campaign.start.documents');
Route::post( '/campaign-start-documentss', 'PagesController@campaignStartDocuments1')->name('campaign.start.documents.miles');
Route::post( '/campaign-finish-documents', 'PagesController@campaignFinishDocuments')->name('campaign.finish.documents');
Route::get( '/driverApproveCourier', 'PagesController@driverApproveCourier')->name('driverApproveCourier');
Route::get( '/driverApproveCourierReceived', 'PagesController@driverApproveCourierReceived')->name('driverApproveCourierReceived');
Route::get( '/driverRejectCourier', 'PagesController@driverRejectCourier')->name('driverRejectCourier');
Route::post( '/driver_rejectCourier_delivery', 'PagesController@driverRejectCourierDeleivery')->name('driverRejectCourierDeleivery');
Route::get( '/getStickerDetails', 'PagesController@getStickerDetails')->name('getStickerDetails');

Route::post ( '/stripe-paymnent-charge','PaymentsController@stripePayment')->name('stripeCharge');
Route::post ( '/stripe-sticker-paymnent-charge','PaymentsController@stripeStickerPayment')->name('stripeChargeStricker');
Route::get ( '/stripe-paymnent-charge','PaymentsController@stripePaymentView');
Route::get ( '/adv-payments','PaymentsController@getPayments')->middleware('role:2')->name('adv.payment');
Route::get ( '/driver-payments','PaymentsController@getDriverPayments')->middleware('role:3')->name('drv.payments');
Route::get ( '/enrolled-driver-payments','PaymentsController@getMyDriverPayments')->middleware('role:4');


Route::get('/campaign-car-update', 'PagesController@campaignCarUpdate')->name('campaign.car.update');
Route::post('/campaign-update', 'PagesController@campaignUpdate')->name('campaign.update');
Route::post('/campaign-sticker-update', 'PagesController@campaignStickerUpdate')->name('campaign.sticker.update');

Route::get( '/getDriverCompletedDocument', 'PagesController@getDriverCompletedDocuments')->name('getDriverCompletedDocuments');



