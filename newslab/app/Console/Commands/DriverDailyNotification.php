<?php

namespace App\Console\Commands;

use App\AppliedCampaigns;
use App\User;
use App\UserNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class DriverDailyNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a daily notification to assigned drivers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("Send a daily notification to driver Cron executed!");
        $users = AppliedCampaigns::where('status','Assigned')->with('campaign.campaign')->get();
    foreach($users as $user){
        $now=Carbon::now()->format('m/d/Y');
        $interval = explode("-", $user->campaign->campaign->interval);
        $remaining_days = Carbon::parse($interval[1])->diffInDays($now);
        $not_create = new  UserNotification();
        $not_create->user_id = $user->user_id;
        $not_create->notification_type_id = '44';
        $not_create->subject = "Your campaign CMP-$user->car_id have $remaining_days days left";
        $not_create->detail = "Today's miles to cover are: ".$user->campaign->mileage;
        $not_create->save();
        }

        }
}
