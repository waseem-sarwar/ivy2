<?php

namespace App\Models;

use App\Notifications\UserResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\AppliedCampaigns;
use App\Campaign;
use App\CarCompany;
use App\CarModels;
use App\Country;
use App\Department;
use App\DriverEnrollment;
use App\Industry;
use App\Notification;
use App\ProfileDepartment;
use App\State;
use App\Status;
use App\UserNotificationSetting;
use App\UserProfile;
use Carbon\Carbon;


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password','role_id'
    ];
    protected $with = ['profile.country','enrolment'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'name', 'email', 'password',
    // ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPassword($token));
    }
    
    
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }

    public function supportNotificationOnUsers()
    {
        return $this->hasOne('App\UserNotificationSetting')->where('notification_id','29');
    }
    public function supportEmailOnUsers()
    {
        return $this->hasOne('App\UserNotificationSetting')->where('notification_id','34');
    }


    public function messages()
    {
        return $this->hasMany(Message::class);
    }
    public function appliedcampaigns(){
        return $this->hasMany(AppliedCampaigns::class);
    }
    public function companydrivers(){
        return $this->hasMany(DriverEnrollment::class,'created_by','id');
    }

    public function enrolment(){
        return $this->hasOne(DriverEnrollment::class,'user_id','id');
    }
    public function role(){
        return $this->belongsTo(Role::class, 'role_id','id');
    }
}
