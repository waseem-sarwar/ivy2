<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tickets extends Model
{
    protected $table ='tickets';

    protected $with = ['threads.fromUser','threads.toUser'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function campaign(){
        return $this->belongsTo('App\CampaignCar','campaign_id','id');
    }
    public function ticketStatus(){
        return $this->belongsTo('App\TicketStatus','status_id','id');
    }
    public function gettype()
    {
        return $this->belongsTo('App\TicketType','type','id');
    }
    public function supporters()
    {
        return  $this->hasMany('App\User','department_id','department_id');
    }

    public function threads()
    {
        return  $this->hasMany('App\TicketThread','ticket_id','id')->orderby('id','desc');
    }
    public function choosedStatus()
    {
        return $this->belongsTo('App\Status','campaign_status','id');
    }

}
