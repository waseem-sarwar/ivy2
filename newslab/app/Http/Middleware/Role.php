<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $roles)
    {
        $roles=explode("|",$roles);
        $user = Auth::user();

        if (in_array($user->role_id,$roles)) {
            return $next($request);
        }else{
            abort(403, "You are not authorize to view this page");
        }

    }
}
