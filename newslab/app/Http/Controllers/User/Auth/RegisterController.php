<?php

namespace App\Http\Controllers\User\Auth;

use App\Models\User;
use App\UserNotification;
use App\UserNotificationSetting;
use App\UserProfile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Validation\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/user/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('user.guest');
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
//        dd();

//        if(isset($data['driver_type'])){
//            dd($data['driver_type']);
//        }
            return \Illuminate\Support\Facades\Validator::make($data, [
                'first_name' => ['required', 'string', 'max:255', 'min:3'],
                'last_name' => ['required', 'string', 'max:255','min:3'],
                'country' => ['required', 'string', 'max:255'],
                'city' => ['required', 'string', 'max:255'],
                'state' => ['required', 'string', 'max:255'],
                'zip' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'contact_no' => ['required', 'numeric','digits_between:7,15','unique:user_profiles,contact_no'],
                'pasword' => ['required', 'string', 'min:4'],
                'dob' =>'required|date|before:18 years ago',
            ]);

        }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */



    protected function create(array $data)
    {
        $role='';
        if(isset($data['driver_type'])) {
            if ($data['driver_type']=='4') {
                $validator = \Illuminate\Support\Facades\Validator::make($data, [
                    'company_name' => ['required', 'string', 'min:3', 'unique:user_profiles,company_name'],
                    'registration_no' => ['required', 'string', 'min:3', 'unique:user_profiles,registration_no'],
                    'type_of_company' => ['required', 'string', 'unique:user_profiles,registration_no'],
                ])->validate();
            }
            $role=$data['driver_type'];
        }
        if(isset($data['role'])) {
            if ($data['role']=='2') {
                $validator = \Illuminate\Support\Facades\Validator::make($data, [
                    'company_name' => ['required', 'string', 'min:3', 'unique:user_profiles,company_name'],
                    'registration_no' => ['required', 'string', 'min:3', 'unique:user_profiles,registration_no'],
                    'type_of_company' => ['required', 'string', 'unique:user_profiles,registration_no'],
                ])->validate();
            }
            $role=$data['role'];
        }
        $data2 = \App\User::create([
            'name' => $data['first_name'],
            'email' => $data['email'],
            'password' => Hash::make($data['pasword']),
            'role_id' =>$role,
        ]);
        if(isset($data['driver_type'])){
//            dd($data['driver_type']);
          UserProfile::create([
             'first_name'=>@$data['first_name'],
            'last_name'=>@$data['last_name'],
            'contact_no'=>@$data['contact_no'],
            'dob'=>@$data['dob'],
            'registration_no'=>@$data['registration_no'],
            'country_id'=>@$data['country'],
            'state_id'=>@$data['state'],
            'city_id'=>@$data['city'],
            'address'=>@$data['address'],
            'zip_code'=>@$data['zip'],
            'no_of_drivers'=>@$data['no_of_drivers'],
            'no_of_cars'=>@$data['no_of_cars'],
            'user_id'=>@$data2['id'],
            'type_of_company'=>@$data['type_of_company'],
            'industry'=>@$data['industry'],
              'company_name'=>@$data['company_name'],
              'company_size'=>@$data['size_of_company']
            ]);

        }
       else{
//            dd($data['driver_type']);

            UserProfile::create([
                'first_name'=>@$data['first_name'],
                'last_name'=>@$data['last_name'],
                'contact_no'=>@$data['contact_no'],
                'dob'=>@$data['dob'],
                'registration_no'=>@$data['registration_no'],
                'country_id'=>@$data['country'],
                'state_id'=>@$data['state'],
                'city_id'=>@$data['city'],
                'address'=>@$data['address'],
                'zip_code'=>@$data['zip'],
                'no_of_drivers'=>@$data['no_of_drivers'],
                'no_of_cars'=>@$data['no_of_cars'],
                'user_id'=>@$data2['id'],
                'type_of_company'=>@$data['type_of_company'],
                'industry'=>@$data['industry'],
                'company_name'=>@$data['company_name'],
                'company_size'=>@$data['size_of_company']
            ]);

        }
//        dd($data->driver_type);

//        Session::flash('name', $data['first_name']);
//dd();
        if($data2) {
            if($role==2){
                $text='advertisor';
            } else if($role==3){
                $text='driver';
            }else{
                $text='enterprise driver';
            }
            $notification_on_users = UserNotificationSetting::where('notification_id','35')->get();
            foreach ($notification_on_users as $user) {
                $not_create = new  UserNotification();
                $not_create->user_id = $user->user_id;
                $not_create->notification_type_id = $user->notification_id;
                $not_create->subject = "New ".$text." registered";
                $not_create->detail = "Name:".$data2->name." & Email:".$data2->email;
                $not_create->save();
            }
            return $data2;
        }
    }



    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('user.auth.register');
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('user');
    }
}
