<?php

namespace App\Http\Controllers;

use App\AdminProfile;
use App\AdvertisorPayment;
use App\AppliedCampaigns;
use App\Campaign;
use App\CampaignCar;
use App\CampaignDocument;
use App\CampaignDriverDocuments;
use App\CampaignFinishDriverDocuments;
use App\CampaignLocation;
use App\CampaignSticker;
use App\CampaignTypes;
use App\CampaignZip;
use App\CarModels;
use App\CarType;
use App\City;
use App\Country;
use App\CourierDetail;
use App\Department;
use App\DriverEnrollment;
use App\DriverPayment;
use App\Http\Requests\BussinessProfile;
use App\Http\Requests\ChangePassword;
use App\Http\Requests\profile;
use App\Imports\PaymentImport;
use App\Industry;
use App\Mail\AdminApprovedDriverAcceptMail;
use App\Mail\AdminApprovedRequestDriverMail;
use App\Mail\CampaignApprovedMail;
use App\Mail\CampaignAssignedDriverMail;
use App\Mail\CampaignAvailableMail;
use App\Mail\CampaignCourierDriverMail;
use App\Mail\CampaignCreatedAdminMail;
use App\Mail\CampaignPaymentAdvMail;
use App\Mail\CourierAceeptedAdminMail;
use App\Mail\CourierAddressAceeptedAdminMail;
use App\Mail\CourierDeliveryRejectedAdminMail;
use App\Mail\CourierDriverMai;
use App\Mail\CourierDriverMail;
use App\Mail\CourierRejectedAdminMail;
use App\Mail\DocumentCompleteAdminMail;
use App\Mail\DocumentReceiveAdminMail;
use App\Mail\DocumentSendDriverMail;
use App\Mail\DocumentSendMileageMail;
use App\Mail\RequstForApprovalAdminMail;
use App\Mail\TicketCreationMail;
use App\Mail\TicketThreadCreationMail;
use App\MakeModel;
use App\Models\Admin;
use App\Notification;
use App\PriceRange;
use App\ProfileDepartment;
use App\Role;
use App\State;
use App\Status;
use App\StickerTemplate;
use App\StickerTemplateType;
use App\SystemSetting;
use App\Tickets;
use App\TicketThread;
use App\TicketType;
use App\User;
use App\UserBank;
use App\UserNotification;
use App\UserNotificationSetting;
use App\UserProfile;
use App\Zip;
use Carbon\Carbon;
use http\Url;
use Illuminate\Http\Request as RequestAlias;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Console\Input\Input;
use App\Mail\CampaignAvaibleMail;

class PagesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified'])->except('home', 'advertisor1', 'driver1', 'get_cities', 'get_zips', 'terms', 'how_works', 'about', 'contact');
    }

    public function user_id()
    {
        return Auth::user()->id;
    }

    public function statePriceRangeCheck(RequestAlias $request){
        $range = PriceRange::where('state_id', '=', $request->state_id)
            ->where('status', 1)
            ->where('min_range', '<=', (integer)$request->range)
            ->where('max_range', '>=', (integer)$request->range)
            ->first();
        if ($range) {
            if ($range->is_set_high == '1') {
                $percentage = SystemSetting::where('name', 'high_percentage')->first();
                $percentage = $range->price * $percentage->data / 100;
                $range->price = number_format($range->price + $percentage, 2);
                return $range;
            } else {
                return $range;
            }

        }
    }

    public function getRangeAvailabeCheck(RequestAlias $request){
        $range_array = explode(";", $request->range);
        $ranges = PriceRange::where('state_id', $request->state_id)->get();
//        dd($ranges);
        $found = 0;
        if ($ranges) {
//            for ($i = 0; $i <= 3000; $i++) {
            foreach ($ranges as $range) {
//                    if($range->min_range == $i) {
                for ($j = (int)$range->min_range; $j <= (int)$range->max_range; $j++) {
//                       echo $j .'<br>';
                    for ($k = (int)$range_array[0]; $k <= (int)$range_array[1]; $k++) {
                        if ($j == $k) {
                            return $found = 1;
                        }
//                       }
                    }

                }
//                }
            }
            if ($found == 0) {
                return 0;
            } else {
                return 1;
            }

        } else {
            return 0;
        }
    }
    public function gettemplatesdata(RequestAlias $request){
        $data = StickerTemplate::where('type', $request->id)->has('color')->with('color')->get();
        if (count($data)>0) {
            return $data;
        } else {
            return 0;
        }
    }

    public function send()
    {
        $objDemo = new \stdClass();
        $objDemo->demo_one = 'Demo One Value';
        $objDemo->demo_two = 'Demo Two Value';
        $objDemo->sender = 'SenderUserName';
        $objDemo->receiver = 'Mr.Ahmad';
        Mail::to("receiver@example.com")->send(new CampaignAvailableMail($objDemo));

    }

    public function home()
    {
        $page_title = 'Dashboard';
        $page_description = '';
//        $arr=expldriver_updateode('/',url()->previous());
//        dd($arr);
//        dd(url()->previous());
        if (!Auth::guard('user')->check()) {
            return view('pages.site.home', compact('page_title', 'page_description'));
        }
        if (Auth::guard('admin')->check() && strpos(url()->previous(), 'admin') !== false) {
            return redirect()->route('admin.dashboard');
        }
        if (Auth::guard('user')->check() == "user" && strpos(url()->previous(), 'user') !== false) {
            return redirect()->route('user.dashboard');
        }

        if (Auth::guard('user')) {
            return redirect()->route('user.dashboard');
        }


        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.dashboard');
        }


        return view('pages.site.home', compact('page_title', 'page_description'));
    }

    public function copy()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor.copys', compact('page_title', 'page_description'));
    }

    public function view_campaign()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor.view_campaign', compact('page_title', 'page_description'));
    }

    public function manageNotifications()
    {
        $notifications = Notification::orderby('id', 'desc')->with('role')->paginate(20);

        $roles = Role::orderby('id', 'desc')->get();
        return view('pages.admin.notificationss', compact('notifications', 'roles'));
    }

    public function managePrice(RequestAlias $request)
    {
        if ($request->state_id) {
            $states_dropdown_values = State::orderby('state', 'asc')->get();
            $states = State::where('id', $request->state_id)->with('priceRanges')->paginate(20);
            $highpercentage = SystemSetting::where('name', 'high_percentage')->first();
            return view('pages.admin.state-price-listing', compact('states', 'highpercentage', 'states_dropdown_values'));
        }


        $states_dropdown_values = State::orderby('state', 'asc')->get();
        $states = State::orderby('state', 'asc')->with('priceRanges')->paginate(20);
        $highpercentage = SystemSetting::where('name', 'high_percentage')->first();
        return view('pages.admin.state-price-listing', compact('states', 'highpercentage', 'states_dropdown_values'));
    }

    public function deleteRange($id)
    {
        PriceRange::find($id)->delete();
        return 1;
    }

    public function statusUpdateRange(RequestAlias $request, $id)
    {
        $status = PriceRange::find($id);
        $status->status = $request->status;
        if ($status->save()) {
            return $status;
        } else {
            return 0;
        }
    }

    public function setHighPriceStatus(RequestAlias $request)
    {
        if ($request->id) {
            $PriceRange = PriceRange::find($request->id);
            $PriceRange->is_set_high = $request->is_set_high;
            $PriceRange->save();
            return 1;
        } else {
            return 0;
        }
    }

    public function updateHighPercentage(RequestAlias $request)
    {

        $this->validate($request, [
            'price' => "required|numeric|between:0,100|regex:/^\d*(\.\d{1,2})?$/"
        ]);
        if ($request->price) {
            $PriceRange = SystemSetting::where('name', $request->name)->first();
            if ($PriceRange) {
                $PriceRange->data = $request->price;
                $PriceRange->save();
            } else {
                $PriceRange = new SystemSetting;
                $PriceRange->data = $request->price;
                $PriceRange->name = $request->name;
                $PriceRange->save();
            }
            return redirect()->back()->with('updated', 'Percentage Updated successfully');
        } else {
            return redirect()->back()->with('Error', 'Record not Updated ');
        }
    }

    public function updateStatesPrices(RequestAlias $request)
    {
//        if ($request->update_default_price) {
//            $state = State::find($request->state_id);
//            $state->default_price = $request->update_default_price;
//            $state->save();
//        }
        if (isset($request->range[0]) && isset($request->range_price[0]) && ($request->range[0]) != null && ($request->range_price[0]) != null) {
//            dd($request->range_price);
            foreach ($request->range as $index => $range) {
                if (isset($request->range_price[$index]) & isset($request->state_id)) {
                    $range = explode(';', $range);
                    $pricerange = new PriceRange;
                    $pricerange->state_id = $request->state_id;
                    $pricerange->min_range = $range[0];
                    $pricerange->max_range = $range[1];
                    $pricerange->price = $request->range_price[$index];
                    $pricerange->save();
//                    dd($pricerange);
                }
            }
            return redirect()->back()->with('updated', 'Record Updated successfully');
        } else {
            return redirect()->back()->with('error', 'validation error');
        }

    }

    public function createNotification(RequestAlias $request)
    {
        $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
        ]);

        if ($request->status == 'on') {
            $status = 1;
        } else {
            $status = 0;
        }

        if ($request->role == '0') {
            $notification = new Notification;
            $notification->name = $request->name;
            $notification->role_id = 0;
            $notification->status = $status;
            $notification->field_name = $request->name;
            $notification->save();
        } else {
            $notification = new Notification;
            $notification->name = $request->name;
            $notification->role_id = $request->role;
            $notification->status = $status;
            $notification->field_name = $request->name;
            $notification->save();
        }
        return redirect()->back()->with('created', 'Notification Created successfully');
    }

    public function updateNotification(RequestAlias $request){
        if ($request->update_status == 'on') {
            $status = 1;
        } else {
            $status = 0;
        }
//        dd($request->update_id);

        $notification = Notification::find($request->update_id);
//        dd($notification);
//        $notification->name = $request->update_name;
//           $notification->role_id=$request->role;
        $notification->status = $status;
//            $notification->field_name=$request->update_name;
        $notification->save();

        return redirect()->back()->with('updated', 'Notification Updated successfully');
    }

    public function about(){
        $page_title = 'About';
        $page_description = '';

        return view('pages.site.about', compact('page_title', 'page_description'));
    }

    public function advertisor1()
    {

        if (Auth::guard('user')->check()) {
            return redirect()->route('dashboard');
        }

        $countries = Country::all();
        $states = State::orderby('state', 'asc')->get();
        $page_title = 'Dashboard';
        $profile_departments = ProfileDepartment::where('status', '1')->get();
        $page_description = '';
        $industries = Industry::where('status', '1')->get();

        return view('pages.site.advertisor1', compact('page_title', 'page_description', 'countries', 'states', 'profile_departments', 'industries'));
    }

    public function advertisor2()
    {
        if (Auth::check()) {
            return redirect()->route('dashboard');
        }
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.advertisor2', compact('page_title', 'page_description'));
    }

    public function contact()
    {
        $page_title = 'Dashboard';
        $page_description = '';
        return view('pages.site.contact', compact('page_title', 'page_description'));
    }

    public function driver1()
    {
        if (Auth::guard('user')->check()) {
            return redirect()->route('dashboard');
        }
        $countries = Country::all();
        $states = State::orderby('state', 'asc')->get();
        $page_title = 'Dashboard';
        $profile_departments = ProfileDepartment::where('status', '1')->get();
        $industries = Industry::where('status', '1')->get();
        $page_description = '';

        return view('pages.site.driver1', compact('industries', 'page_title', 'page_description', 'countries', 'states', 'profile_departments'));
    }

    public function driver2()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.driver2', compact('page_title', 'page_description'));
    }

    public function faqs()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.faqs', compact('page_title', 'page_description'));
    }

    public function how_works()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.how_works', compact('page_title', 'page_description'));
    }

    public function login_home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.login_home', compact('page_title', 'page_description'));
    }

    public function reset_password()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.reset_password', compact('page_title', 'page_description'));
    }

    public function terms()
    {
//        phpinfo();
//        dd('here');
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.terms', compact('page_title', 'page_description'));
    }

    public function index(RequestAlias $request)
    {

//        print_r($request->all());
//        die($request->all());
        $input = $request->all();
        $email = $input['email'];
//        dd($input['email']);
        if ($email == 'driver@gmail.com') {
//            dd('driver ');
            return redirect('/Driver/Dashboard/');
//           fvf dd('here');
//                return redirect(RouteServiceProvider::HOME);
//            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
        }
        if ($email == 'ent_driver@gmail.com') {
//            dd('driver ');
            return redirect('/Driver/Ent/Dashboard');
//           fvf dd('here');
//                return redirect(RouteServiceProvider::HOME);
//            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
        }
        if ($email == 'advertisor@gmail.com') {
//            dd('driver ');
            return redirect('/Advertiser/Dashboard/');
//            dd('here');
//                return redirect(RouteServiceProvider::HOME);
//            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
        }
        if ($email == 'admin@gmail.com') {
//            dd('driver ');
            return redirect('/Admin/Dashboard/');
//
//                return redirect(RouteServiceProvider::HOME);
//            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
        }
//        if (Auth::user()->role_id == 1) {
//            dd('advertisor');
//            return redirect('/Advertiser/Dashboard/');
//
//        }

//        $page_title = 'Dashboard';
//        $page_description = '';

//        return view('pages.advertisor-dashboard', compact('page_title', 'page_description'));
    }

    public function index2()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_dashboard', compact('page_title', 'page_description'));
    }

    public function login_site_thok()
    {
        dd('here');
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_dashboard', compact('page_title', 'page_description'));
    }

    public function basic_enrollment()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver.driver-enrollment', compact('page_title', 'page_description'));
    }

    public function advertisor_home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.dashboard', compact('page_title', 'page_description'));
    }

    public function driver_home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_dashboard', compact('page_title', 'page_description'));
    }

    public function ent_driver_home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.enterprise_driver_dashboard', compact('page_title', 'page_description'));
    }

    public function create_campaign(){
//        $templates = StickerTemplate::where('status', '1')->with('activeColor')->orderBy('id', 'desc')->limit('7')->get();;
        $page_title = 'Dashboard';
        $page_description = '';
        $data['states'] = State::orderby('state', 'asc')->get();
        $data['campaign_types'] = CampaignTypes::orderby('id', 'desc')->where('status', '1')->get();
        $data['template_types'] = StickerTemplateType::orderby('id', 'desc')->has('templates')->with('templates')->where('status', '1')->get();
//        dd($data['template_types'][0]->templates[0]);
        $Cars = CarModels::select('company_id')->distinct()->get();
        $Cartypes = CarType::orderby('id','desc')->get();
        return view('pages.advertisor.create-campaign1', compact('page_title', 'page_description', 'data', 'Cars','Cartypes'));
    }

    public function createCampaign2()
    {
        $page_title = 'Dashboard';
        $page_description = '';
        $data['states'] = State::orderby('state', 'asc')->get();
        $data['campaign_types'] = CampaignTypes::orderby('id', 'desc')->where('status', '1')->get();
        $Cars = CarModels::select('company_id')->distinct()->get();
        return view('pages.create-campaign', compact('page_title', 'page_description', 'data', 'Cars'));
    }

    public function get_cities(RequestAlias $request)
    {
        $data = City::where('state_id', $request->state_id)->orderby('primary_city', 'asc')->get();
        return $data;
    }

    public function get_zips(RequestAlias $request)
    {

        $city = City::where('id', $request->city_id)->first();
        $data = Zip::where('primay_city', "$city->primary_city")->orderby('zip', 'desc')->get();
        return $data;
    }

    public function edit_campaign()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor.edit-campaign', compact('page_title', 'page_description'));
    }

    public function create_campaign_step2()

    {

        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor.sticker-creation', compact('page_title', 'page_description'));
    }

    public function chats()
    {

        $page_title = 'Dashboard';

        $page_description = '';

        return view('pages.advertisor.chats', compact('page_title', 'page_description'));
    }

    public function driver_chats()
    {
        $page_title = 'Dashboard';
        $page_description = '';
        $users = User::where('id', '!=', Auth::id())->where('role_id', '1')->get();
        return view('pages.driver.chats', compact('users', 'page_title', 'page_description'));
    }

    public function tickets(RequestAlias $request)
    {

        $data['tickets_all']=array();
        $ticket_types = TicketType::orderBy('id', 'desc')->get();
        $campaign_statuses = Status::orderBy('id', 'desc')->whereNotIn('id', array(1))->get();
        $dept_id = Auth::user()->department_id;
        if ($dept_id == '9999') {
            $data['tickets_all'] = Tickets::orderby('updated_at', 'desc')->get()->pluck('id');
            $data['tickets'] = Tickets::with('user','ticketStatus','supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->orderby('updated_at', 'desc')->paginate(15);
            if ($request->type || $request->ticket) {
                $data['tickets_all'] = Tickets::orderby('updated_at', 'desc')->get()->pluck('id');
                $data['tickets'] = Tickets::with('user','ticketStatus', 'supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->where('type', $request->type)->orderby('updated_at', 'desc')->paginate(15);
//                dd($data['tickets']);
                if($request->type && $request->ticket){
                    $data['tickets'] = Tickets::with('user','ticketStatus', 'supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->where('type', $request->type)->where('id', $request->ticket)->orderby('updated_at', 'desc')->paginate(15);
                }  if(!$request->type && $request->ticket){
                    $data['tickets'] = Tickets::with('user','ticketStatus', 'supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->where('id', $request->ticket)->orderby('updated_at', 'desc')->paginate(15);
                }
           }
        } else {
            $data['tickets'] = Tickets::where('user_id', Auth::user()->id)->with('user', 'supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->orderby('updated_at', 'desc')->paginate(20);
//            dd($data['tickets']);
                $data['tickets_all'] = Tickets::orderby('updated_at', 'desc')->where('user_id',Auth::user()->id)->get()->pluck('id');
            if ($request->type || $request->ticket) {
                $data['tickets_all'] = Tickets::orderby('updated_at', 'desc')->where('user_id', Auth::user()->id)->get()->pluck('id');
                $data['tickets'] = Tickets::with('user','ticketStatus', 'supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->where('user_id',Auth::user()->id)->where('type', $request->type)->orderby('updated_at', 'desc')->paginate(15);
                if ($request->type && $request->ticket) {
                    $data['tickets'] = Tickets::with('user','ticketStatus', 'supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->where('user_id',Auth::user()->id)->where('type', $request->type)->where('id', $request->ticket)->orderby('updated_at', 'desc')->paginate(15);
                }
                if (!$request->type && $request->ticket) {
                    $data['tickets'] = Tickets::with('user','ticketStatus','supporters', 'campaign', 'gettype','choosedStatus','campaign.status')->withCount('threads')->where('user_id',Auth::user()->id)->where('id', $request->ticket)->orderby('updated_at', 'desc')->paginate(15);
                }
            }



        }

        $data['admin'] = Admin::where('role_id', '1')->pluck('name')->first();
        $page_title = 'Dashboard';
        $page_description = '';
        $data['departments'] = Department::orderby('id', 'desc')->get();
        if (Auth::user()->role_id == '3') {
            $data['campaigns'] = AppliedCampaigns::where('user_id', Auth::user()->id)->with('campaign')->orderby('id', 'desc')->get();
        } elseif (Auth::user()->role_id == '2') {
            $data['campaigns'] = CampaignCar::where('user_id', Auth::user()->id)->with('campaign.campaign')->orderby('id', 'desc')->get();
        } elseif (Auth::user()->role_id == '4') {
            $data['campaigns'] = DriverEnrollment::with('appliedcampaigns.campaign.campaign')->where('created_by', Auth::user()->id)->orderBy('updated_at', 'DESC')->get();
//       dd($data['campaigns'][1]);
        }
        return view('pages.advertisor.tickets', compact('page_title', 'page_description', 'data', 'ticket_types','campaign_statuses'));
    }

    public function driver_tickets()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver.tickets', compact('page_title', 'page_description'));
    }

    public function personal_information()
    {
        $page_title = 'Dashboard';
        $page_description = '';
        return view('pages.advertisor.personal-information', compact('page_title', 'page_description'));
    }

    public function driver_payment_details()
    {
        $page_title = 'Dashboard';
        $page_description = '';
        return view('pages.driver.payment-details', compact('page_title', 'page_description'));
    }

    public function driver_ent_payment_details()
    {
        $page_title = '';
        $page_description = '';
        return view('pages.driver_ent.payment-details', compact('page_title', 'page_description'));
    }

    public function driver_personal_information()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver.personal-information', compact('page_title', 'page_description'));
    }

    public function account_information()
    {

        $page_title = 'Dashboard';
        $page_description = '';
//        $industries=Industry::where('status','1')->get();
        return view('pages.advertisor.account-information', compact('page_title', 'page_description', 'industries'));
    }

    public function driver_account_information()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver.account-information', compact('page_title', 'page_description'));
    }

    public function driver_change_password()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver.change-password', compact('page_title', 'page_description'));
    }

    public function adv_change_password()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor.change-password', compact('page_title', 'page_description'));
    }

    public function driver_notification_setting()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver.notification-setting', compact('page_title', 'page_description'));
    }

    public function advertisor_notification_setting()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor.notification-setting', compact('page_title', 'page_description'));
    }

    public function driver_notifications()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver.notifications', compact('page_title', 'page_description'));
    }

    public function advertiser_notifications()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor.notifications', compact('page_title', 'page_description'));
    }

    public function login_site()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.login_home', compact('page_title', 'page_description'));
    }
    /**
     * Demo methods below
     */
    // Datatables
    public function manage_campaign(RequestAlias $request)
    {
        $check = 0;
        if ($request->campaign_status || $request->search) {
            $status = Status::orderby('id', 'desc')->get();
            $campaigns = CampaignCar::
            where('user_id', Auth::user()->id)->
            where(function ($query) use ($request) {
                $query->where('status_id', $request->campaign_status);
            })->whereHas('campaign', function ($q) use ($request) {
                $q->where('title', 'LIKE', '%' . $request->search . '%');
            })->orderby('id', 'desc')->with('status', 'sticker', 'campaign', 'state', 'city', 'appliedDrivers', 'refund_amount')->paginate(20);
            $check = 1;
            return view('pages.advertisor.manage-campaign', compact('campaigns', 'status', 'check'));

        }
        $page_title = 'Campaign';
        $page_description = 'Campaign Listing';
        $status = Status::orderby('id', 'desc')->get();
//        $campaigns = Campaign::where('user_id', Auth::user()->id)->orderby('id', 'desc')->with('status','sticker')->get();
        $campaigns = CampaignCar::where('user_id', Auth::user()->id)->orderby('id', 'desc')->with('status', 'sticker', 'campaign', 'state', 'city', 'appliedDrivers', 'refund_amount')->paginate(20);
//      dd($campaigns);
        return view('pages.advertisor.manage-campaign', compact('page_title', 'page_description', 'campaigns', 'status', 'check'));
    }

    public function payments()
    {
        $page_title = '';
        $page_description = '';

        return view('pages.advertisor.payments', compact('page_title', 'page_description'));
    }

    public function driver_payments()
    {
        $page_title = '';
        $page_description = '';

        return view('pages.driver.payments', compact('page_title', 'page_description'));
    }

    public function driver_ent_payments()
    {
        $page_title = '';
        $page_description = '';

        return view('pages.driver_ent.payments', compact('page_title', 'page_description'));
    }

    public function reports()
    {
        $page_title = '';
        $page_description = '';

        return view('pages.advertisor.reports', compact('page_title', 'page_description'));
    }

    public function driver_reports()
    {
        $page_title = '';
        $page_description = '';

        return view('pages.driver.reports', compact('page_title', 'page_description'));
    }

    public function driver_ent_reports()
    {
        $page_title = '';
        $page_description = '';

        return view('pages.driver_ent.reports', compact('page_title', 'page_description'));
    }

    public function driver_manage_campaign(RequestAlias $request)
    {
        $check = 0;
        if ($request->campaign_status || $request->search || $request->payment_status) {
            $status = Status::orderby('id', 'desc')->where('name', '!=', 'Approved')->where('name', '!=', 'Pending')->get();
            $campaigns = Campaign::
            where('user_id', Auth::user()->id)->
            where(function ($query) use ($request) {
                $query->where('title', '=', "$request->search");
                $query->orwhere('status_id', $request->campaign_status);
            })
                ->orderby('id', 'desc')->with('status')->paginate(10);
            $check = 1;
            return view('pages.driver.manage-campaign', compact('campaigns', 'status', 'check'));

        }


        $page_title = 'Campaign';
        $page_description = 'Campaign Management';

        $status = Status::orderby('id', 'desc')->where('name', '!=', 'Approved')->get();
//        dd($status);
        $campaigns = Campaign::where('user_id', Auth::user()->id)->orderby('id', 'desc')->with('status')->paginate(10);
        return view('pages.driver.manage-campaign', compact('page_title', 'page_description', 'campaigns', 'status', 'check'));
    }

    // KTDatatables
    public function ktDatatables()
    {
        $page_title = 'KTDatatables';
        $page_description = 'This is KTdatatables test page';

        return view('pages.ktdatatables', compact('page_title', 'page_description'));
    }

    // Select2
    public function select2()
    {
        $page_title = 'Select 2';
        $page_description = 'This is Select2 test page';

        return view('pages.select2', compact('page_title', 'page_description'));
    }

    // custom-icons
    public function customIcons()
    {
        $page_title = 'customIcons';
        $page_description = 'This is customIcons test page';

        return view('pages.icons.custom-icons', compact('page_title', 'page_description'));
    }

    // flaticon
    public function flaticon()
    {
        $page_title = 'flaticon';
        $page_description = 'This is flaticon test page';
        return view('pages.icons.flaticon', compact('page_title', 'page_description'));
    }

    // fontawesome
    public function fontawesome()
    {
        $page_title = 'fontawesome';
        $page_description = 'This is fontawesome test page';

        return view('pages.icons.fontawesome', compact('page_title', 'page_description'));
    }

    // lineawesome
    public function lineawesome()
    {
        $page_title = 'lineawesome';
        $page_description = 'This is lineawesome test page';

        return view('pages.icons.lineawesome', compact('page_title', 'page_description'));
    }

    // socicons
    public function socicons()
    {
        $page_title = 'socicons';
        $page_description = 'This is socicons test page';

        return view('pages.icons.socicons', compact('page_title', 'page_description'));
    }

    // svg
    public function svg()
    {
        $page_title = 'svg';
        $page_description = 'This is svg test page';

        return view('pages.icons.svg', compact('page_title', 'page_description'));
    }

    public function ajaxGetCountries()
    {
        $page_title = 'svg';
        $page_description = 'This is svg test page';
        $guard = new Country();
        $guard = $guard->getAll();
        return (
        [
            'responseCode' => 1,
            'message' => 'Guard Retrieved Successfully',
            'data' => $guard
        ]
        );
    }

    // Quicksearch Result
    public function quickSearch()
    {
        return view('layout.partials.extras._quick_search_result');
    }

    public function payment_store(RequestAlias $request)
    {
        $data = new UserBank;
        $data['company_name'] = $request->card_copmany;
        $data['card_number'] = $request->card_no;
        $data['cvc'] = $request->cvc;
        $data['expiry'] = $request->expiry;
        $data['user_id'] = Auth::user()->id;
        $data->save();
        return redirect()->back();
    }

    public function payment_details()
    {
        $data = UserBank::where('user_id', Auth::user()->id)->paginate(10);
        $page_title = '';
        $page_description = '';
        return view('pages.advertisor.payment-details', compact('page_title', 'page_description', 'data'));
    }

    public function payment_update(RequestAlias $request)
    {
        $data = UserBank::find($request->data_id);
        $data['company_name'] = $request->company_name;
        $data['card_number'] = $request->card_number;
        $data['cvc'] = $request->cvc;
        $data['expiry'] = $request->ex_date;
        $data->save();

        return redirect()->back();
    }

    public function payment_delete(RequestAlias $request)
    {
//        dd();
        $data = UserBank::findOrFail($request->id);
        $data->delete();
        return redirect()->back();
    }

    public function payment_import(RequestAlias $request)
    {
        Excel::import(new PaymentImport, request()->file('import_file'));
        return redirect()->back();
    }

    public function driver_update(Profile $request)
    {
        $profile_pic = null;
//        $request->validate([
//            'fname' => 'required|max:25',
//            'lname' => 'required|max:25',
//            'state_id' => 'required|max:22',
//            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
//            'contact_no' => 'integer|digits_between:7,16',
//            'dob' => 'required|date|before:-18 years',
//        ]);


        if ($request->image) {
            $profile_pic = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images/profile'), $profile_pic);

        }
//        dd($request);
        $user_id = Auth::user()->id;
//        dd($user_id);
        $data = User::find($user_id);
        $data['name'] = $request->fname;
        $data->save();
        $data1 = [];
        $data1['first_name'] = $request->fname;
        $data1['last_name'] = $request->lname;
//        $dob=Carbon::parse($request->dob)->format('Y-d-m');
        $data1['dob'] = $request->dob;
        $data1['contact_no'] = $request->contact_no;
        $data1['country_id'] = $request->country_id;
        $data1['state_id'] = $request->state_id;
        $data1['city_id'] = $request->city_id;
        $data1['zip_code'] = $request->zip_code;
        $data1['street_address'] = $request->street_address;
        $data1['address'] = $request->address;
        $data1['user_id'] = $user_id;
        if ($profile_pic) {
            $data1['profile_pic'] = $profile_pic;
        }

        $finduser = UserProfile::where('user_id', $user_id)->first();

        if ($finduser) {
            UserProfile::where('user_id', $user_id)->update($data1);
//            dd($finduser);
        } else {

            UserProfile::create($data1);

        }
//        if($user){
//            dd('here');
//        }


        return redirect()->back()->with('updated', 'User updated successfully');


    }

    public function admin_update(RequestAlias $request)
    {
//        dd( Auth::user());
        $profile_pic = null;
        $request->validate([
            'fname' => 'required|max:25',
            'email' => 'required|email|max:35',
            'lname' => 'required|max:25',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'contact_no' => 'integer|digits_between:7,16',
        ]);
//dd($request->image);
        if ($request->image) {
            $profile_pic = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images/profile'), $profile_pic);
        }

        $user_id = Auth::user()->id;
        $data = Admin::find($user_id);
//dd($data->id);
        $data['name'] = $request->fname;
        $data['email'] = $request->email;
        $data->save();
        $data2 = [];
        $data2['first_name'] = $request->fname;
        $data2['user_id'] = $data->id;
        $data2['last_name'] = $request->lname;
        $data2['dob'] = $request->dob;
        $data2['contact_no'] = $request->contact_no;
        $data2['country_id'] = $request->country_id;
        $data2['state_id'] = $request->state_id;
        $data2['city_id'] = $request->city_id;
        $data2['zip_code'] = $request->zip_code;
        $data2['user_id'] = $user_id;
        $data2['street_address'] = $request->street_address;
        $data2['address'] = $request->address;
        if ($profile_pic) {
            $data2['profile_pic'] = $profile_pic;
        }
        if (AdminProfile::where('user_id', $data->id)->first()) {
            AdminProfile::where('user_id', $data->id)->update($data2);
        } else {
            AdminProfile::create($data2);
        }


//        if($user){
//            dd('here');
//        }


        return redirect()->back()->with('updated', 'User updated successfully');


    }

    public function driver_update_bussiness(BussinessProfile $request)
    {

        $user_id = Auth::user()->id;
        $data = UserProfile::where('user_id', $user_id)->first();
        $data['company_name'] = $request->company_name;
        $data['industry'] = $request->industry;
        $data['type_of_company'] = $request->type_of_company;
        $data['company_size'] = $request->company_size;
        $data['registration_no'] = $request->registration_no;
        $data->save();
        return redirect()->back()->with('updated', 'Bussiness info updated successfully');
    }

    public function update_password(ChangePassword $request)
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);

        $decrypted = Hash::check($request->passsword, $user->password);
        if ($decrypted != true) {
            return redirect()->back()->with('err', 'Your old password is wrong')->withInput($request->only('passsword', 'pwd_new', 'pwd_confirm'));
        }
        $user['password'] = Hash::make($request->pwd_confirm);
        $user->save();

        $notification_id = Notification::where('field_name', 'change_password')->where('role_id', 0)->first();
        if ($notification_id) {
            $notification_check_on = UserNotificationSetting::where('user_id', $user_id)->where('notification_id', $notification_id->id)->first();
            if ($notification_check_on) {
                $not_create = new  UserNotification();
                $not_create->user_id = $user_id;
                $not_create->notification_type_id = $notification_check_on->notification_id;
                $not_create->subject = "Password Changed";
                $not_create->detail = "Your password is changed successfully at: " . date('d-m-Y H:i A');
                $not_create->save();
            }
        }
        return redirect()->back()->with('updated', 'Password updated successfully');
    }

    public function admin_update_password(ChangePassword $request)
    {
        $user_id = Auth::user()->id;
//        dd($user_id);
        $user = Admin::find($user_id);
        $decrypted = Hash::check($request->passsword, $user->password);
        if ($decrypted != true) {
            return redirect()->back()->with('err', 'Your old password is wrong')->withInput($request->only('passsword', 'pwd_new', 'pwd_confirm'));
        }
        $user['password'] = Hash::make($request->pwd_confirm);
        $user->save();

        $notification_id = Notification::where('field_name', 'change_password')->where('role_id', 0)->first();
        if ($notification_id) {
            $notification_check_on = UserNotificationSetting::where('user_id', $user_id)->where('notification_id', $notification_id->id)->first();
            if ($notification_check_on) {
                $not_create = new  UserNotification();
                $not_create->user_id = $user_id;
                $not_create->notification_type_id = $notification_check_on->notification_id;
                $not_create->subject = "Password Changed";
                $not_create->detail = "Your password changed successfully";
                $not_create->save();
            }
        }
        return redirect()->back()->with('updated', 'Password updated successfully');
    }

    public function driver_update_notification(RequestAlias $request)
    {
        $collection = collect($request);
        $user = UserNotificationSetting::where('user_id', Auth::user()->id);
        $user->delete();

        foreach ($collection as $index => $value) {
            if ($index != '_token') {
                if ($value == 'on') {
                    $value = 1;
                }
                $data = new UserNotificationSetting;
                $data['user_id'] = Auth::user()->id;
                $data['notification_id'] = $index;
                $data['status'] = $value;
                $data->save();
//                dd($data);
            }
        }
        return redirect()->back()->with('updated', 'Settings updated successfully');

    }

    public function admin_update_notification(RequestAlias $request)
    {
//        dd();
        $collection = collect($request);
        $user = UserNotificationSetting::where('user_id', Auth::user()->id);

        $user->delete();
//        dd( $collection);
//        dd($request);
        foreach ($collection as $index => $value) {
            if ($index != '_token') {
                if ($value == 'on') {
                    $value = 1;
                }
                $data = new UserNotificationSetting;
                $data['user_id'] = Auth::user()->id;
                $data['notification_id'] = $index;
                $data['status'] = $value;
                $data->save();
//                dd($data);
            }
        }
        return redirect()->back()->with('updated', 'Settings updated successfully');
    }

    public function campaignStatusUpdate(RequestAlias $request){
        if ($request->campaigner_email) {
            $user1 = User::where('email', $request->campaigner_email)->first();
        } else {
            $user1 = User::where('id', $request->user_id)->first();
        }

        $car = CampaignCar::where('id', $request->car_id)->first();
        if ($request->s_u == '1') {
            if ($user1) {
                Mail::send(new CampaignApprovedMail($user1->name, $user1->email, $car->slug));
                $notification_on_advertisor = UserNotificationSetting::where('notification_id', '42')->where('user_id', $user1->id)->first();
                if ($notification_on_advertisor) {
                    $not_create = new  UserNotification();
                    $not_create->user_id = $user1->id;
                    $not_create->notification_type_id = 42;
                    $not_create->subject = "Campaign broadcasted successfully";
                    $not_create->detail = "Your campaign#$request->car_id is successfully sends to all certified drivers";
                    $not_create->save();
                }
            }

            $carzips = explode(",", $car->zip);
            $car_types = explode(",", $car->types);
            $route = route('user.camp.detail', ['slug' => $car->slug]);
            $year_range =explode("-", $car->year_range);
            $max_year=(int)$year_range[1];
            $min_year=(int)$year_range[0];
            foreach ($carzips as $zip) {
                $drivers = DriverEnrollment::where('car_drive_zip', $zip)->get();
//dd($drivers);
                foreach ($drivers as $driver) {
//                    dd($driver->car_year);
                    $year=MakeModel::where('id',$driver->car_year)->first();
//                    dd($year);
                    if ($zip == $driver->car_drive_zip) {
                        foreach ($car_types as $type) {
                            if ($driver->car_type == $type){
                                $notification_on_user = UserNotificationSetting::where('notification_id', '16')->where('user_id', $driver->user_id)->first();
                                if (isset($notification_on_user) && ($notification_on_user->user_id == $driver->user_id)) {
                                    for ($vari = $min_year; $vari <= $max_year; $vari++) {
                                        if(! is_null($year)){
                                        if ($year->model_name == $vari) {
                                            $not_create = new  UserNotification();
                                            $not_create->user_id = $driver->user_id;
                                            $not_create->notification_type_id = '16';
                                            $not_create->subject = "New Campaign is Available";
                                            $not_create->detail = "CMP-$car->id is available to claim.For details click <a href=" . $route . ">here</a>";
                                            $not_create->save();
                                            $usr = User::find($driver->user_id);
                                            $details['receiver_name'] = $usr->name;
                                            $details['receiver_email'] = $usr->email;
                                            $details['sender_name'] = env('APP_NAME');
                                            $details['campaign_url'] = $car->slug;
                                            Mail::send(new CampaignAvailableMail($details));
                                        }
                                    }
                                }
                                }
                            }
                        }
                    }
                }
            }


        }


        if ($request->s_u == '3') {
            $email_on_user = UserNotificationSetting::where('notification_id', '43')->where('user_id', $user1->id)->first();
            if ($email_on_user) {
                $details['receiver_name'] = $user1->name;
                $details['receiver_email'] = $user1->email;
                $details['sender_name'] = env('APP_NAME');
                $details['campaign_url'] = $car->slug;
                $details['total_price'] = $car->total_price;
                Mail::send(new CampaignPaymentAdvMail($details));
            }
            $car->payment_status = 'Pending';
            $car->save();

        }
        if ($request->s_u == '4') {
            $not_create = new  UserNotification();
            $not_create->user_id = $user1->id;
            $not_create->notification_type_id = 42;
            $not_create->subject = "Campaign was rejected by admin";
            $route = route('user.camp.detail', ['slug' => $car->slug]);
            $not_create->detail = "Your campaign#CMP-$request->car_id is cancelled by admin.<a href='$route'>details</a>";
            $not_create->save();
            $car->rejected_comment = $request->comment;
            $car->save();
        }
        if ($request->s_u == '2') {
            $cars = CampaignCar::where('campaign_id', $request->camp_id)->get();
            foreach ($cars as $car2) {
                $car2->is_edit = 0;
                $car2->save();
            }
            $route = route('user.camp.detail', ['slug' => $car->slug]);
            $not_create = new  UserNotification();
            $not_create->user_id = $user1->id;
            $not_create->notification_type_id = 42;
            $not_create->subject = "Campaign is in process";
            $not_create->detail = "Your campaign#CMP-$request->car_id is reviewing by admin. <a href='$route'>details</a>";
            $not_create->save();
        }
        if ($request->s_u == '1') {
            $car->payment_status = 'Paid';
            $car->status_id = 3;
            $car->save();
        } else {
            $car->status_id = $request->s_u;
            $car->save();
        }
        return redirect()->back()->with('updated', 'Status updated successfully');
    }

    public function campaignbespokeUpdate(RequestAlias $request){

        $sticker = CampaignSticker::where('campaign_id', $request->campaign_id)->first();
        $sticker->bespoke_status="Waiting for sticker payment";
        $sticker->bespoke_price=$request->price;
        $sticker->bespoke_days=$request->deliver;
        $sticker->save();

                    $not_create = new  UserNotification();
                    $not_create->user_id = $request->user_id;
                    $not_create->notification_type_id = 45;
                    $not_create->subject = "Your Campaign is waiting for sticker payment";
                    $not_create->detail = "Please pay $$request->price. so, then it will be broadcasted";
                    $not_create->save();


        return redirect()->back()->with('updated', 'Data updated successfully');
    }

    public function claim(RequestAlias $request)
    {
        $camp = AppliedCampaigns::where('car_id', $request->car_id)->where('user_id', Auth::user()->id)->first();
        $car = CampaignCar::where('id', $request->car_id)->first();
        if ($camp) {
            return 1;
        }
        $app_camp = new AppliedCampaigns();
        $app_camp->car_id = $request->car_id;
        $app_camp->user_id = Auth::user()->id;
        $app_camp->state_id = Auth::user()->enrolment->driving_state;
        $app_camp->applied_zip = Auth::user()->enrolment->car_drive_zip;
        $app_camp->status = 'Applied';
        $app_camp->save();
        $admin = Admin::where('department_id', '9999')->where('role_id', '1')->first();
        if ($admin) {
            Mail::to($admin->email)->send(new RequstForApprovalAdminMail($admin->name, Auth::user()->name, $car->slug, $admin->email, $car->id));
        }
        return 1;

    }

    public function availabilityStatusUpdate(RequestAlias $request)
    {
        $app_camp = AppliedCampaigns::where('user_id', Auth::user()->id)->where('car_id', $request->campaign_id)->first();
        if ($app_camp) {
            $app_camp->availability = $request->availability;
            $app_camp->save();
            $admin = Admin::where('department_id', '9999')->where('role_id', '1')->first();

            if ($admin && $request->availability == '2') {
                $not_create = new  UserNotification();
                $not_create->user_id = $admin->id;
                $not_create->notification_type_id = 45;
                $not_create->subject = "Driver " . Auth::user()->name . " are not available for campaign#CMP-" . $request->campaign_id;
                $not_create->detail = "";
                $not_create->save();
            }
            if ($admin && $request->availability == '1') {
                $not_create = new  UserNotification();
                $not_create->user_id = $admin->id;
                $not_create->notification_type_id = 45;
                $not_create->subject = "Driver " . Auth::user()->name . " are available for campaign#CMP-" . $request->campaign_id;
                $not_create->detail = "";
                $not_create->save();
            }
            return 1;
        } else {
            return 0;
        }

    }

    public function campaignUpdate(RequestAlias $request)
    {
        $campaign = Campaign::where('id', $request->camp_id)->first();
        $cars = CampaignCar::where('campaign_id', $request->camp_id)->get();

        $campaign->title = $request->camp_title;
        $campaign->type = $request->camp_type;
        $interval = explode("-", $request->time_interval);
        $total_days = Carbon::parse($interval[1])->diffInDays(Carbon::parse($interval[0]));
        $total_days += 1;

        foreach ($cars as $car) {
            $total = 0;
            $total = $car->no_of_cars * $car->price_per_mile * $total_days;
//            dd($total);
            $car->total_price = number_format($total, 2);
            $car->save();
        }

        $campaign->interval = $request->time_interval;
        $campaign->campaign_total_days = $total_days;
        $campaign->save();


        return redirect()->back()->with('updated', 'Campaign updated successfully');

    }

    public function campaignStickerUpdate(RequestAlias $request)
    {
        $campaign = CampaignSticker::where('campaign_id', $request->camp_id)->first();
        $campaign->slogan = $request->slogan;
        $campaign->detail = $request->company;
        $campaign->contact = $request->contact;
        $campaign->sticker_details = $request->sticker_details;

        if ($request->has('sticker')) {
            @$path = public_path() . "/images/campaign-sticker/" . $campaign->image_url;
            @unlink($path);
            $sticker1 = Str::random(15) . '.' . $request->sticker->extension();
            $request->sticker->move(public_path('images/campaign-sticker'), $sticker1);
            $campaign->image_url = $sticker1;
        }

        $campaign->save();


        return redirect()->back()->with('updated', 'Campaign updated successfully');

    }

    public function campaignCarUpdate(RequestAlias $request)
    {
        $campaign = Campaign::where('id', $request->camp_id)->first();
        $car = CampaignCar::where('id', $request->id)->first();
        $minus = (int)$car->no_of_cars * (int)$car->price_per_mile * (int)$campaign->campaign_total_days;
        $car->delete();
        $total = (int)$campaign->total_price;
        $campaign->total_price = $total - $minus;
        $campaign->save();
//dd($minus.'aaa'.$campaign->total_price.'ssss'.$total);
        return redirect()->back()->with('updated', 'Campaign updated successfully');
    }

    public function claimStatusUpdate(RequestAlias $request)
    {
        $dat = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->update(['status' => 'Waiting for documents', 'availability' => '0']);
        if ($dat) {
            $not_create = new  UserNotification();
            $not_create->user_id = $request->driver_id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Your request for campaign is successfully accepted";
            $not_create->detail = "Please confirm your availability. Click <a href='" . route('user.camp.detail', ['slug' => $request->slug]) . "'>here </a> for details ";
            $not_create->save();
            Mail::to($request->driver_email)->send(new AdminApprovedDriverAcceptMail($request->driver_name, $request->campaign_id, $request->slug));
            return DriverEnrollment::where('user_id', $request->driver_id)->first();;
        } else {
            return 0;
        }
    }

    public function driverTransaction(RequestAlias $request)
    {
        $dat = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        $dat->status = 'Completed';
        $dat->amount_gives = $request->giving_amount;
        $dat->save();
        $car = CampaignCar::where('id', $request->campaign_id)->first();
        $car->completed_drivers++;
        $car->assigned_drivers--;
        $car->save();
        if ($dat) {
            $not_create = new  UserNotification();
            $not_create->user_id = $request->driver_id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Campaign completed";
            $not_create->detail = "Your campaign CMP-" . $request->campaign_id . " is successfully completed by admin. You earned from this campaign is $" . $request->giving_amount;
            $not_create->save();
            CampaignFinishDriverDocuments::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->update(['admin_miles' => $request->miles]);
            $payment = new AdvertisorPayment;
            $payment->campaign_id = $request->campaign_id;
            $payment->user_id = $car->user_id;
            $payment->type = 'Refundable';
            $payment->amount = $request->refundable_amount;
            $payment->applied_driver_id = $dat->id;
            $payment->save();

            $create_payment = new DriverPayment;
            $create_payment->user_id = $request->driver_id;
            $create_payment->car_id = $request->campaign_id;
            $create_payment->amount = $request->giving_amount;
            $create_payment->save();
            return 1;
        } else {
            return 0;
        }
    }


    public function sendCourierMailDriver1(RequestAlias $request)
    {
        $driver = User::where('id', $request->driver_id)->first();
        $applied_campaign = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->car_id)->first();
        $applied_campaign->update(['courier_status' => 'Waiting for address confirmation']);
        $cd = CourierDetail::where(['applied_campaigns_id' => $applied_campaign->id])->first();
        if ($cd == null) {
            $cd = new CourierDetail;
        }
        $cd->applied_campaigns_id = $applied_campaign->id;
        $cd->address = $request->mail_address;
        $cd->save();

        if ($driver) {
            Mail::send(new CampaignCourierDriverMail($driver->name, $driver->email, $request->car_id, $request->mail_address));
        }
        return 1;
    }

    public function sendCourierMailDriver2(RequestAlias $request)
    {
        $driver = User::where('id', $request->driver_id)->first();
        $applied_campaign = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->car_id)->first();
        $applied_campaign->update(['courier_status' => 'Waits for courier received confirmation']);
        $cd = CourierDetail::where(['applied_campaigns_id' => $applied_campaign->id])->first();
        if ($cd == null) {
            $cd = new CourierDetail;
        }
        $cd->applied_campaigns_id = $applied_campaign->id;
        $cd->address = $request->mail_address;
        $cd->courier_company = $request->courier_company;
        $cd->url = $request->courier_url;
        $cd->tracking_id = $request->tracking_id;
        $cd->save();

        if ($driver) {
            $not_create = new  UserNotification();
            $not_create->user_id = $request->driver_id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Campaign documents sent against campaign#CMP-" . $request->car_id;
            $not_create->detail = "Please Send document received confirmation when received";
            $not_create->save();
//            $driver->courier_status='Waits for courier received confirmation';
//            $driver->save();
        }
        return 1;

    }

    public function campaignStartDocuments(RequestAlias $request)
    {
        $camp = AppliedCampaigns::where('car_id', $request->campaign_id)->where('user_id', Auth::user()->id)->first();
        $camp->documents_status = 'Waiting for video sticker approval';
        $data = CampaignDriverDocuments::where('campaign_id', $request->car_id)->where('user_id', Auth::user()->id)->first();
        if (!$data) {
            $data = new CampaignDriverDocuments;
        } else {
            @$path = public_path() . "/images/driver-documents/" . $data->sticker_video;
            @unlink($path);
            @$path = public_path() . "/images/driver-documents/" . $data->sticker_picture;
            @unlink($path);
        }
        if ($request->has('sticker_pic')) {
            $sticker_pic = Str::random(18) . '.' . $request->sticker_pic->extension();
            $request->sticker_pic->move(public_path('images/driver-documents'), $sticker_pic);
            $data->sticker_picture = $sticker_pic;
        }
//        if ($request->has('mileage_pic')) {
//            $mileage_pic = Str::random(18) . '.' . $request->mileage_pic->extension();
//            $request->mileage_pic->move(public_path('images/driver-documents'), $mileage_pic);
//            $data->mileage_picture = $mileage_pic;
//        }
        if ($request->has('sticker_video')) {
            $sticker_video = Str::random(19) . '.' . $request->sticker_video->extension();
            $request->sticker_video->move(public_path('images/driver-documents'), $sticker_video);
            $data->sticker_video = $sticker_video;
        }
//        $data->campaign_id =;
        $data->car_id = $request->campaign_id;
        $data->user_id = Auth::user()->id;
        $data->save();
        Mail::to(Auth::user()->email)->send(new DocumentSendDriverMail(Auth::user()->name, $request->campaign_id));
        $admin = Admin::where('department_id', '9999')->where('role_id', '1')->first();
        if ($admin) {
            Mail::to($admin->email)->send(new DocumentReceiveAdminMail($admin->name, Auth::user()->name, $request->campaign_id));
            $not_create = new  UserNotification();
            $not_create->user_id = $admin->id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Sticker related data uploaded by driver: " . Auth::user()->name;
            $not_create->detail = "Campaign#" . $request->campaign_id . ". Please take action on it.";
            $not_create->save();
        }
        $camp->save();

        return redirect()->back()->with('success', 'Documents uploaded successfully.Please wait for approval');
    }

    public function campaignStartDocuments1(RequestAlias $request)
    {
//        dd($request);
        $camp = AppliedCampaigns::where('car_id', $request->campaign_id)->where('user_id', Auth::user()->id)->first();
        $camp->documents_status = 'Waiting for starting miles approval';
        $camp->save();
        $data = CampaignDriverDocuments::where('car_id', $request->campaign_id)->where('user_id', Auth::user()->id)->first();
        if (!$data) {
            $data = new CampaignDriverDocuments;
        } else {
            @$path = public_path() . "/images/driver-documents/" . $data->mileage_picture;
            @unlink($path);

        }
        if ($request->has('mileage_pic')) {
            $mileage_pic = Str::random(22) . '.' . $request->mileage_pic->extension();
            $request->mileage_pic->move(public_path('images/driver-documents'), $mileage_pic);
            $data->mileage_picture = $mileage_pic;
        }
        $data->car_id = $request->campaign_id;
        $data->user_id = Auth::user()->id;
        $data->starting_miles = $request->mileage_input;
        $data->save();

        $admin = Admin::where('department_id', '9999')->where('role_id', '1')->first();
        if ($admin) {
            Mail::to($admin->email)->send(new DocumentSendMileageMail($admin->name, Auth::user()->name, $request->campaign_id));
            $not_create = new  UserNotification();
            $not_create->user_id = $admin->id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Start miles related data uploaded by driver: " . Auth::user()->name;
            $not_create->detail = "Campaign no. :" . $request->campaign_id . ". Please take action on it.";
            $not_create->save();
        }

        return redirect()->back()->with('success', 'Data uploaded successfully. Please wait for approval');
    }

    public function campaignFinishDocuments(RequestAlias $request)
    {
//        dd($request);
        $request->campaign_id = (int)$request->campaign_id;
        if ($request->termss == '1') {
            $data = new CampaignFinishDriverDocuments;
            if ($request->has('sticker_pic')) {
                $sticker_pic = Str::random(20) . '.' . $request->sticker_pic->extension();
                $request->sticker_pic->move(public_path('images/driver-documents'), $sticker_pic);
                $data->sticker_picture = $sticker_pic;
            }
            if ($request->has('mileage_pic')) {
                $mileage_pic = Str::random(20) . '.' . $request->mileage_pic->extension();
                $request->mileage_pic->move(public_path('images/driver-documents'), $mileage_pic);
                $data->mileage_picture = $mileage_pic;
            }
            $data->mileage = $request->miles;
            $data->car_id = $request->campaign_id;
            $data->user_id = Auth::user()->id;
            $data->campaign_finish_date = \Carbon\Carbon::now()->format('Y-m-d H:i');
            $data->save();
            $camp = $notification = AppliedCampaigns::where('car_id', $request->campaign_id)->where('user_id', Auth::user()->id)->first();
            $camp->status = 'Finished';
            $camp->save();
//            Mail::to(Auth::user()->email)->send(new DocumentSendDriverMail(Auth::user()->name, $request->campaign_id));
            $admin = Admin::where('department_id', '9999')->where('role_id', '1')->first();
            if ($admin) {
                Mail::to($admin->email)->send(new DocumentCompleteAdminMail($admin->name, Auth::user()->name, $request->campaign_id));
            }

            return redirect()->back()->with('success', 'Documents sent successfully');
        }
    }

    public function campaignDetail($slug)
    {
        $car_types='';
        $detail = CampaignCar::where('slug', $slug)->with('campaign', 'status', 'user', 'state', 'city', 'sticker', 'payment')->first();
        $typs = explode(",", $detail->types);
        foreach($typs as $typ){
            $car_types.=CarType::where('id',$typ)->pluck('name')->first().' ';
        }
//dd($car_types);
        if (!$detail) {
            abort(404);
        }
        $role_id = Auth::user()->role_id;
        $user_id = $this->user_id();
        $claim = 0;
        if ($role_id == '3' || $role_id == '4') {
            $detail2 = AppliedCampaigns::where('user_id', $user_id)->where('car_id', $detail->id)->first();

            if ($detail2) {
                $amount = DriverPayment::where('user_id', $user_id)->where('car_id', $detail->id)->pluck('amount')->first();
                if (!$amount) {
                    $amount = 0;
                }
                $claim = $detail2;
                $claim['amount'] = $amount;
            }
        }
//        dd($claim->payment->amount);
//        dd($claim);
        $campaign_types_dropdown = CampaignTypes::orderby('id', 'desc')->where('status', '1')->get();
        $status = Status::orderby('id', 'desc')->get();
        return view('pages.view-campaign', compact('detail', 'status', 'claim', 'campaign_types_dropdown','car_types'));
    }

    public function getCities(RequestAlias $request)
    {
        $states = State::where('country_id', $request->id)->get();
        return $states;
    }

    public function getDriverDocuments(RequestAlias $request)
    {
        $data = CampaignDriverDocuments::where('user_id', $request->driver_id)->where('campaign_id', $request->campaign_id)->first();
        if ($data) {
            return $data;
        } else {
            return 0;
        }
    }

    public function getDriverGivingAmount(RequestAlias $request)
    {
        $camp = CampaignCar::where('campaign_id', '=', $request->campaign_id)
            ->where('state_id', '=', $request->state_id)
            ->first();

        $range = PriceRange::where('state_id', $camp->state_id)
            ->where('status', 1)
            ->where('min_range', '<=', $camp->range)
            ->where('max_range', '>=', $camp->range)
            ->first();
        if ($range) {
            if ($range->is_set_high == '1') {
                $percentage = SystemSetting::where('name', 'high_percentage')->first();
//          return $percentage->data;
                $percentage = $range->price * $percentage->data / 100;
                $range->price = $range->price + $percentage;
                return $range;
            } else {
                return $range;
            }

        }


        $data = CampaignDriverDocuments::where('user_id', $request->driver_id)->where('campaign_id', $request->campaign_id)->first();
        if ($data) {
            return $data;
        } else {
            return 0;
        }

    }

    public function getDriverCompletedDocuments(RequestAlias $request)
    {
        $data['start'] = CampaignDriverDocuments::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        $start = Carbon::parse($data['start']->campain_start_date);
        $starting_miles = $data['start']->starting_miles;

        $data['complete'] = CampaignFinishDriverDocuments::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        $end = Carbon::parse($data['complete']->campaign_finish_date);
        $finish_miles = $data['complete']->mileage;
        $data['total_days'] = $end->diffInDays($start);
        if ($data['total_days'] == '0') {
            $data['total_days'] = 1;
        }
        $covered_miles = (int)$finish_miles - (int)$starting_miles;
        $data['covered_miles'] = $covered_miles;
        $car = CampaignCar::where('id', $request->campaign_id)->first();

        $campaigntotaldays = Campaign::where('id', $car->campaign_id)->pluck('campaign_total_days')->first();
        $milesperday = number_format($data['covered_miles'] / $campaigntotaldays);
        $data['campaigntotaldays'] = $campaigntotaldays;
        $price = PriceRange::where('state_id', '=', $car->state_id)
            ->where('min_range', '<=', (integer)$milesperday)
            ->where('max_range', '>=', (integer)$milesperday)
            ->first();
        if ($price) {
            if ($price->is_set_high == 1) {
                $percetnage = SystemSetting::where('name', 'high_percentage')->pluck('data')->first();
                $price = $price->price + (($percetnage / 100) * $price->price);
            } else {
                $price = $price->price;
            }
        } else {
            $price = 0;
        }

//        dd( 'driver covered miles per day :'.$milesperday.'driver covered miles:'.$covered_miles.'campaigntotaldays:'.$campaigntotaldays);

        $applied_campaign = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        $data['campaign'] = $applied_campaign;
        $data['price'] = $price * $data['total_days'];
        $data['advertisorrefundableamount'] = ($car->price_per_mile * $campaigntotaldays) - $data['price'];
        $data['actual_amountt'] = ($car->price_per_mile * $campaigntotaldays);
        if ($applied_campaign->status == "Completed") {
//                dd($applied_campaign->id);
            $data['advertisorrefundableamount'] = AdvertisorPayment::where('applied_driver_id', $applied_campaign->id)->pluck('amount')->first();
//                dd($data['advertisorrefundableamount']);
        }
        if ($data) {
            return $data;
        } else {
            return 0;
        }
    }

    public function driverApproveStartDocuments(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', $request->driver_id)->where('campaign_id', $request->campaign_id)->first();
        $data->status = 'Assigned';
        $data->save();

        $data1 = CampaignDriverDocuments::where('user_id', $request->driver_id)->where('campaign_id', $request->campaign_id)->first();
        $data1->campain_start_date = \Carbon\Carbon::now()->format('Y-m-d H:i');
        $data1->save();


        $usr = User::find($request->driver_id);
        if ($usr) {
            Mail::send(new CampaignAssignedDriverMail($usr->email, $usr->name, $request->campaign_id));
        }
        if ($data) {
            return 1;
        } else {
            return 0;
        }
    }

    public function driverApproveCourier(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', Auth::user()->id)->where('car_id', $request->campaign_id)->first();
        if ($data) {
            $data->courier_status = 'Address Acknowledged';
            $data->address_acknowledged = '1';
            $data->save();
            $usr = Admin::where('role_id', 1)->first();
            if ($usr) {
                Mail::send(new CourierAddressAceeptedAdminMail($usr->email, $usr->name, $request->campaign_id, Auth::user()->name, Auth::user()->email));
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function driverApproveCourierReceived(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', Auth::user()->id)->where('car_id', $request->campaign_id)->first();
        if ($data) {
            $data->courier_status = 'Courier Received';
            $data->documents_status = 'Waits for sticker video upload';
            $data->save();
            $usr = Admin::where('role_id', 1)->first();
            if ($usr) {
                Mail::send(new CourierAceeptedAdminMail($usr->email, $usr->name, $request->campaign_id, Auth::user()->name, Auth::user()->email));
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function driverApproveSticker(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        if ($data) {
            $data->documents_status = 'Waiting for miles';
            $data->start_sticker_status = '1';
            $data->save();
            $not_create = new  UserNotification();
            $not_create->user_id = $request->driver_id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Campaign sticker approved against campaign#CMP-" . $request->campaign_id;
            $not_create->detail = "Please upload the car miles detail";
            $not_create->save();
            return 1;
        } else {
            return 0;
        }
    }

    public function driverApproveMiles(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        $car = CampaignCar::where('id', $request->campaign_id)->first();
        $car->assigned_drivers++;
        $car->save();

        if ($data) {
            $data->documents_status = 'Miles approved';
            $data->status = 'Assigned';
            $data->save();
            $not_create = new  UserNotification();
            $not_create->user_id = $request->driver_id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Campaign miles approved against campaign#CMP-" . $request->campaign_id;
            $not_create->detail = "Campaign starts right now";
            $not_create->save();
            $data1 = CampaignDriverDocuments::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
            $data1->campain_start_date = \Carbon\Carbon::now()->format('Y-m-d H:i');
            $data1->save();
            $usr = User::find($request->driver_id);
            if ($usr) {
                Mail::send(new CampaignAssignedDriverMail($usr->email, $usr->name, $request->campaign_id));
            }
            return 1;
        } else {
            return 0;
        }

    }

    public function driverRejectMiles(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        if ($data) {
            $data->documents_status = 'Waiting for resend miles';
            $data->documents_feedback = $request->comment;
            $data->save();
            $not_create = new  UserNotification();
            $not_create->user_id = $request->driver_id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Campaign miles are rejected by admin against campaign#CMP-" . $request->campaign_id;
            $not_create->detail = "Please re upload the miles";
            $not_create->save();
            return 1;
        } else {
            return 0;
        }
    }

    public function driverRejectSticker(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', $request->driver_id)->where('car_id', $request->campaign_id)->first();
        if ($data) {
            $data->documents_status = 'Waiting for resend sticker';
            $data->document_sticker_feedback = $request->comment;
            $data->save();
            $not_create = new  UserNotification();
            $not_create->user_id = $request->driver_id;
            $not_create->notification_type_id = 44;
            $not_create->subject = "Campaign Sticker are rejected by admin against campaign#CMP-" . $request->campaign_id;
            $not_create->detail = "Please re upload the sticker info";
            $not_create->save();
            return 1;
        } else {
            return 0;
        }
    }

    public function driverRejectCourier(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', Auth::user()->id)->where('car_id', $request->campaign_id)->first();
        $data->courier_status = 'Address resend request';
        $data->courier_driver_feedback = $request->comment;
        $data->save();
        if ($data) {
            $usr = Admin::where('role_id', 1)->first();
            if ($usr) {
                Mail::send(new CourierRejectedAdminMail($usr->email, $usr->name, $request->campaign_id, Auth::user()->name, Auth::user()->email, $request->comment));
            }
            return 1;
        } else {
            return 0;
        }
    }

    public function driverRejectCourierDeleivery(RequestAlias $request)
    {
        $data = AppliedCampaigns::where('user_id', Auth::user()->id)->where('car_id', $request->campaign_id)->first();
        $data->courier_status = 'Courier not received';
        $data->courier_reject_message = $request->coment;
        if ($request->not_received_file) {
            $imageName = time() . '.' . $request->not_received_file->extension();
            $request->not_received_file->move(public_path('images/driver/courierreject/'), $imageName);
            $data->courier_rejected_image = $imageName;
        }
        $data->save();
        if ($data) {
            $usr = Admin::where('role_id', 1)->first();
            if ($usr) {
                Mail::send(new CourierDeliveryRejectedAdminMail($usr->email, $usr->name, $request->campaign_id, Auth::user()->name, Auth::user()->email, $request->coment));
            }
            return redirect()->back()->with('success', 'Message sent successfully');
        }

    }

    public function getCourierDetails(RequestAlias $request)
    {
//        return $request->applied_id;
        $data = CourierDetail::where('applied_campaigns_id', $request->applied_id)->first();
        if ($data) {
            return $data;
        } else {
            return 0;
        }
    }

    public function getStickerDetails(RequestAlias $request)
    {
//        return $request->applied_id;
        $data = CampaignDriverDocuments::where('car_id', $request->campaign_id)->where('user_id', $request->driver_id)->first();
        $data['applied_campaign'] = AppliedCampaigns::where('car_id', $request->campaign_id)->where('user_id', $request->driver_id)->first();
        if ($data) {
            return $data;
        } else {
            return 0;
        }
    }

    public function notifications(RequestAlias $request)
    {
        $user_id = $this->user_id();
        UserNotification::where('user_id', $user_id)->update(['is_read' => '1']);
        $notifications = UserNotification::where('user_id', $user_id)->orderBy('id', 'desc')->paginate(20);
//        dd($data);
        return view('pages.advertisor.notifications', compact('notifications'));
    }

    public function ticketClose(RequestAlias $request){
        $ticket = Tickets::where('id', $request->ticket_id)->first();
        if ($ticket) {
            $ticket->status_id=3;
            $ticket->close_message=$request->mesg;
            $ticket->save();
            return 1;
        }else{
            return 0;
        }

    }
        public function ticketStore(RequestAlias $request){
        $imageName = null;
        if ($request->image) {
            $imageName = time() . '.' . $request->image->extension();
            $request->image->move(public_path('images/tickets'), $imageName);
        }
        $campaign = new Tickets;
        $campaign->user_id = Auth::user()->id;
        $campaign->title = $request->title;
        $campaign->campaign_status = $request->selected_camp_status;
        $campaign->status_id ='1';
        $campaign->campaign_id = $request->campaign_id;
        $campaign->detail = $request->detail;
        $campaign->type = $request->type;
        $campaign->img_url = $imageName;
        $campaign->save();
        $admin = Admin::with('supportNotificationOnUsers', 'supportEmailOnUsers')->first();
        if ($admin) {
            if (isset($admin->supportNotificationOnUsers)) {
                $not_create = new  UserNotification();
                $not_create->user_id = $admin->id;
                $not_create->notification_type_id = $admin->supportNotificationOnUsers->notification_id;
                $not_create->subject = "New support Ticket created by " . Auth::user()->name;
                $not_create->detail = "Ticket id is: " . $campaign->id . ". Campaign id is:CMP#" . $campaign->id . " Please take action on it.";
                $not_create->save();
            }

            if (isset($admin->supportEmailOnUsers)) {
                $details['from'] = Auth::user()->name;
                $details['from-email'] = Auth::user()->email;
//                    $details['from-email'] = Auth::user()->email;
                $details['ticket_id'] = $campaign->id;
                $details['to'] = $admin->name;
                Mail::to($admin->email)->send(new TicketCreationMail($details));
            }


        }


//        if ($request->department) {
//            $users = User::where('department_id', $request->department)->orwhere('department_id', '9999')->with('supportNotificationOnUsers', 'supportEmailOnUsers')->get();
//            foreach ($users as $user) {
//                if ($user->supportNotificationOnUsers) {
//                    $not_create = new  UserNotification();
//                    $not_create->user_id = $user->id;
//                    $not_create->notification_type_id = $user->supportNotificationOnUsers->notification_id;
//                    $not_create->subject = "Support Ticket  created by " . Auth::user()->name;
//                    $not_create->detail = "New support Ticket is created. Ticket id is: " . $campaign->id . ". Please take action on it.";
//                    $not_create->save();
//                }
//                if ($user->supportEmailOnUsers) {
//                    $details['from'] = Auth::user()->name;
//                    $details['from-email'] = Auth::user()->email;
////                    $details['from-email'] = Auth::user()->email;
//                    $details['ticket_id'] = $campaign->id;
//                    $details['to'] = $user->name;
////                    dd($user->email);
//
//                    Mail::to($user->email)->send(new TicketCreationMail($details));
//                }
//
//
//            }
//        }

        return redirect()->back()->with('created', 'Ticket created successfully. Your Tracking id is: ' . $campaign->id);
    }

    public function ticketThread(RequestAlias $request){
//        dd($request);
        $admin = Admin::where('department_id', '9999')->with('supportNotificationOnUsers', 'supportEmailOnUsers')->first();
        $user = User::where('id', $request->creator_id)->with('supportNotificationOnUsers', 'supportEmailOnUsers')->first();
        $ticket = Tickets::where('id', $request->ticket_id)->first();
       $ticket->updated_at=now();

        $ticket->save();

        if (Request::segment(1) == "user") {
            if ($admin->supportEmailOnUsers) {
                $details['from'] = Auth::user()->name;
                $details['ticket_id'] = $request->ticket_id;
                $details['to'] = $admin->name;
                $details['title'] = $request->detail;
                $details['detail'] = $request->subject;
                Mail::to($admin->email)->send(new TicketThreadCreationMail($details));
            }
            if ($admin->supportNotificationOnUsers) {
                $not_create = new  UserNotification();
                $not_create->user_id = $admin->id;
                $not_create->notification_type_id = $admin->supportNotificationOnUsers->notification_id;
                $not_create->subject = "Reply from " . Auth::user()->name . " against ticket #" . $request->ticket_id;
                $not_create->detail = "Reply against ticket #" . $request->ticket_id . ". Please take action on it.";
                $not_create->save();
            }
        } else {
            if ($user->supportEmailOnUsers) {
                $details['from'] = $admin->name;
                $details['ticket_id'] = $request->ticket_id;
                $details['to'] = $user->name;
                $details['title'] = $request->detail;
                $details['detail'] = $request->subject;
                Mail::to($user->email)->send(new TicketThreadCreationMail($details));
            }
            if ($user->supportNotificationOnUsers){
                $not_create = new  UserNotification();
                $not_create->user_id = $user->id;
                $not_create->notification_type_id = $user->supportNotificationOnUsers->notification_id;
                $not_create->subject = "Reply from " . Auth::user()->name . " against ticket #" . $request->ticket_id;
                $not_create->detail = "New Reply against ticket #" . $request->ticket_id;
                $not_create->save();
            }
        }
        $campaign = new TicketThread;
        $to = (Request::segment(1) == "admin") ? $request->creator_id : $admin->id;
        $campaign->from_id = Auth::user()->id;
        @$campaign->to_id = $to;
        $campaign->subject = $request->subject;
        $campaign->ticket_id = $request->ticket_id;
        $campaign->message = $request->detail;
        $campaign->save();
        return redirect()->back()->with('created', '');
    }

//    public function img(Request $request)
//    {
//        dd($request->image, $request->image);;
//    }
    public function uploadSticker(RequestAlias $request)
    {
        $sticker = CampaignSticker::where('campaign_id',$request->cmp_id)->first();
         if ($sticker) {
             if ($request['photo']) {
//                 return $request->photo;
                 $data = $request->photo;
                 $image_array_1 = explode(";", $data);
                 $image_array_2 = explode(",", $image_array_1[1]);
                 $data = base64_decode($image_array_2[1]);
                 $name=time() . '.png';
                 $image_name = $name ;
                 Storage::disk('local')->put($image_name, $data);
                 $sticker->sticker_url = $name;
                 $sticker->save();
                 return 1;
             }
             if ($request->has('img')) {
                 $sticker1 = Str::random(15) . '.' . $request->img->extension();
                 $request->img->move(public_path('/images/campaign-sticker'), $sticker1);
                 $sticker->image_url = $sticker1;
             }
             $sticker->save();
         }

        return redirect()->route('user.campaigns')->with('created', 'Campaign created successfully!!!');

    }

    public function createCampaign(RequestAlias $request){
        $campaign = new Campaign;
        $campaign->user_id = Auth::user()->id;
        $campaign->title = $request->campaign_detail['campaign_title'];
        $campaign->type = $request->campaign_detail['campaign_type'];
        $campaign->interval = $request->campaign_detail['daterange'];
        $campaign->status_id = '6';
        $campaign->save();
        $total = 0;
        $interval = explode("-", $request->campaign_detail['daterange']);
        $total_days = Carbon::parse($interval[1])->diffInDays(Carbon::parse($interval[0]));
        $total_days += 1;


        foreach ($request->locations as $key => $location) {
            $loc = json_decode($location, true);
            $campaign_location = new CampaignLocation();
            $campaign_location->campaign_id = $campaign->id;
            $campaign_location->country_id = $loc['countries[]'];
            $campaign_location->state_id = $loc['states[]'];
            $campaign_location->city_id = $loc['cities[]'];
            $campaign_location->save();
            foreach ($loc['zips[]'] as $zip) {
                $loc_zip = new CampaignZip;
                $loc_zip->camp_location_id = $campaign_location->id;
                $loc_zip->zip_code = $zip;
                $loc_zip->save();
            }
            foreach ($request->cars as $cars) {
                $car = json_decode($cars, true);
                if ($key == $car['location[]']) {

                    $cartotal = $car['carQuntity[]'] * $car['price_per_mile[]'] * $total_days;
                    $campaign_car = new CampaignCar();
                    $campaign_car->campaign_id = $campaign->id;
                    $campaign_car->price_per_mile = $car['price_per_mile[]'];
                    $campaign_car->mileage = $car['mileage[]'];
                    $campaign_car->car_location = $campaign_location->id;
                    $campaign_car->no_of_cars = $car['carQuntity[]'];
//                    $campaign_car->make = $car['carMake[]'];
//                    $campaign_car->model = $car['car_model[]'];
//                    $campaign_car->year = $car['car_year[]'];
//                    $campaign_car->type = $car['car_type[]'];
                    $campaign_car->status_id = 6;
                    $total += $cartotal;
                    $campaign_car->total_price = number_format($cartotal, 2);
                    $campaign_car->slug = Str::slug($campaign->title . '-' . Str::random(3), '-');
                    $campaign_car->city_id = $campaign_location->city_id;
                    $campaign_car->state_id = $campaign_location->state_id;
                    $campaign_car->user_id = Auth::user()->id;
                    $campaign_car->payment_status = 'Pending';
                    $campaign_car->year_range = $car['year'];
//return $car['VehicleType'][0];
                    $zips = null;
                    if (isset($car['carZip[]'])) {
                        foreach ($car['carZip[]'] as $zip) {
                            if ($zip != null)
                                $zips .= $zip . ',';
                        }
                    }
                    if ($zips != null) {
                        $campaign_car->zip = rtrim($zips, ", ");
                    }
                    $types = null;
                    if (isset($car['VehicleType'])) {
                        foreach ($car['VehicleType'] as $type) {
                            $types .= $type . ',';
                        }
                    }
                        $campaign_car->types = rtrim($types, ", ");

                    $campaign_car->save();
//                    return $campaign_car;
                    $notification_on_users = UserNotificationSetting::where('notification_id', '41')->get();
                    foreach ($notification_on_users as $user) {
                        $not_create = new  UserNotification();
                        $not_create->user_id = $user->user_id;
                        $not_create->notification_type_id = $user->notification_id;
                        $not_create->subject = "New Campaign Created";
                        $not_create->detail = "New campaign#CMP-$campaign_car->id created by:" . Auth::user()->name . "<a href=" . route('camp.detail', ['slug' => $campaign_car->slug]) . "> details</a>";
                        $not_create->save();

                        $admin = Admin::first();
                        $details['receiver_name'] = $admin->name;
                        $details['receiver_email'] = $admin->email;
                        $details['sender_name'] = Auth::user()->name;
                        $details['sender_email'] = Auth::user()->email;
                        $details['campaign_url'] = $campaign_car->slug;
                        Mail::send(new CampaignCreatedAdminMail($details));
                    }
                    $notification_on_user = UserNotificationSetting::where('notification_id', '42')->where('user_id', Auth::user()->id)->first();
                    if ($notification_on_user) {
                        $not_create = new  UserNotification();
                        $not_create->user_id = Auth::user()->id;
                        $not_create->notification_type_id = 42;
                        $not_create->subject = "Campaign created successfully";
                        $not_create->detail = "Your campaign#$campaign_car->id is now waiting for admin approvel";
                        $not_create->save();
                    }
                }


            }
        }
        $campaign->campaign_total_days = $total_days;
        $campaign->total_price = $total;
        $campaign->save();
        if ($request->sticker_detail) {
            $sticker = new CampaignSticker();
            $sticker->campaign_id = $campaign->id;
            $sticker->detail = $request->sticker_detail['company_details'];
            $sticker->contact = $request->sticker_detail['contact'];
            $sticker->slogan = $request->sticker_detail['slogan'];
            $sticker->sticker_details = $request->sticker_detail['sticker_details'];
            $sticker->sticker_id = $request->StickerTemp['StickerID'];
            $sticker->Sticker_bg = $request->StickerTemp['Stickerbg'];
            $sticker->sticker_txt_color = $request->StickerTemp['StickertxtColor'];
            $sticker->sticker_height = $request->StickerTemp['StickerHeight'];
            $sticker->sticker_width = $request->StickerTemp['Stickerwidth'];
            $sticker->save();
        }
        return $campaign->id;
    }

    public function createCampaign1(RequestAlias $request)
    {
//return  $request;
        $campaign = new Campaign;
        $campaign->user_id = Auth::user()->id;
        $campaign->title = $request->campaign_detail['campaign_title'];
        $campaign->type = $request->campaign_detail['campaign_type'];
//            $campaign->duration = $request->campaign_detail['campaign_duration'];
        $campaign->interval = $request->campaign_detail['daterange'];
        $campaign->status_id = '6';
        $campaign->save();
        $total = 0;
        $interval = explode("-", $request->campaign_detail['daterange']);
        $total_days = Carbon::parse($interval[1])->diffInDays(Carbon::parse($interval[0]));
        $total_days += 1;


        foreach ($request->locations as $key => $location) {
            $loc = json_decode($location, true);
            $campaign_location = new CampaignLocation();
            $campaign_location->campaign_id = $campaign->id;
            $campaign_location->country_id = $loc['countries[]'];
            $campaign_location->state_id = $loc['states[]'];
            $campaign_location->city_id = $loc['cities[]'];
            $campaign_location->save();
            foreach ($loc['zips[]'] as $zip) {
                $loc_zip = new CampaignZip;
                $loc_zip->camp_location_id = $campaign_location->id;
                $loc_zip->zip_code = $zip;
                $loc_zip->save();
            }
            foreach ($request->cars as $cars) {
                $car = json_decode($cars, true);
                if ($key == $car['location[]']) {
                    $campaign_car = new CampaignCar();
                    $campaign_car->campaign_id = $campaign->id;
                    $campaign_car->price_per_mile = $car['price_per_mile[]'];
                    $campaign_car->mileage = $car['mileage[]'];
                    $campaign_car->car_location = $campaign_location->id;
                    $campaign_car->no_of_cars = $car['carQuntity[]'];
                    $campaign_car->make = $car['carMake[]'];
                    $campaign_car->model = $car['car_model[]'];
                    $campaign_car->year = $car['car_year[]'];
                    $campaign_car->type = $car['car_type[]'];
                    $total += $car['carQuntity[]'] * $car['price_per_mile[]'] * $total_days;
//                    $days = null;
//                    if (isset($car['days'])) {
//                        foreach ($car['days'] as $day) {
//                            if ($day != null)
//                                $days .= $day . ',';
//                        }
//                    }
//                    if ($days != null) {
//                        $campaign_car->drive_days = rtrim($days, ", ");
//                    }
                    $zips = null;
                    if (isset($car['carZip[]'])) {
                        foreach ($car['carZip[]'] as $zip) {
                            if ($zip != null)
                                $zips .= $zip . ',';
                        }
                    }
                    if ($zips != null) {
                        $campaign_car->zip = rtrim($zips, ", ");
                    }
                    $campaign_car->save();
                }
            }
        }
        $campaign->campaign_total_days = $total_days;
        $campaign->total_price = $total;
        $campaign->save();
        if ($request->sticker_detail) {
            $sticker = new CampaignSticker();
            $sticker->campaign_id = $campaign->id;
            $sticker->detail = $request->sticker_detail['company_details'];
            $sticker->contact = $request->sticker_detail['contact'];
            $sticker->slogan = $request->sticker_detail['slogan'];
            $sticker->sticker_details = $request->sticker_detail['sticker_details'];
            $sticker->save();
        }
        if ($campaign) {
            $notification_on_users = UserNotificationSetting::where('notification_id', '41')->get();
            foreach ($notification_on_users as $user) {
                $not_create = new  UserNotification();
                $not_create->user_id = $user->user_id;
                $not_create->notification_type_id = $user->notification_id;
                $not_create->subject = "New Campaign Created";
                $not_create->detail = "New campaign#$campaign->id created by:" . Auth::user()->name;
                $not_create->save();

                $admin = Admin::first();
                $details['receiver_name'] = $admin->name;
                $details['receiver_email'] = $admin->email;
                $details['sender_name'] = Auth::user()->name;
                $details['sender_email'] = Auth::user()->email;
                $details['campaign_url'] = $campaign->id;
                Mail::send(new CampaignCreatedAdminMail($details));
            }
            $notification_on_user = UserNotificationSetting::where('notification_id', '42')->where('user_id', Auth::user()->id)->first();
            if ($notification_on_user) {
                $not_create = new  UserNotification();
                $not_create->user_id = Auth::user()->id;
                $not_create->notification_type_id = 42;
                $not_create->subject = "Campaign created successfully";
                $not_create->detail = "Your campaign#$campaign->id is waiting for admin approvel";
                $not_create->save();
            }

        }
        return $campaign->id;
    }

    public function getCarsForCampaigns($id)
    {
        $modelsss = CarModels::where('company_id', $id)->select('car_name_id')->distinct()->get();

        return view('pages.advertisor.partials._car_models', compact('modelsss'));
    }

    public function getYearForCampaigns($id)
    {
        $years = CarModels::where('car_name_id', $id)->select('car_model_id')->distinct()->get();

        return view('pages.advertisor.partials._car_year', compact('years'));
    }

    public function getTypeForCampaigns($id)
    {
        $types = CarModels::where('car_model_id', $id)->select('car_type')->distinct()->get();
        return view('pages.advertisor.partials._car_type', compact('types'));
    }


}
