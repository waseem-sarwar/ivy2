<?php

namespace App\Http\Controllers;

use App\Events\MyEvent;
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Pusher\Pusher;

class ChatsController extends Controller
{
public function getMessage($user_id){
    $my_id=Auth::id();
$messages=Message::where(function ($query) use ($user_id,$my_id){
    $query->where('from',$my_id)->where('to',$user_id);
})->orWhere(function ($query) use ($user_id,$my_id){
    $query->where('from',$user_id)->where('to',$my_id);
})->get();
//    dd($messages[0]->message);
    return view('pages.driver._messages',['messages'=>$messages]);
}
public function sendMessage(Request $request){

    $from=Auth::id();
    $to=$request->receiver_id;
    $message=$request->message;
    $data=new Message();
    $data->from=$from;
    $data->to=$to;
    $data->message=$message;
    $data->is_read=0;
    $data->save();

////    pusher
    $options =array(
        'cluster' => 'ap2',
//        'forceTLS' => true
    );

    $pusher = new Pusher (
        env('PUSHER_APP_KEY'),
        env('PUSHER_APP_SECRET'),
        env('PUSHER_APP_ID'),
        $options
    );
$data=['from'=> $from, 'to'=>$to];
event(new MyEvent($data));
//$pusher->trigger('my_channel','my-event',$data);
//dd($pusher);
}
}
