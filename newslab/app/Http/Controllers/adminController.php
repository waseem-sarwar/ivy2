<?php

namespace App\Http\Controllers;

use App\AdvertisorPayment;
use App\Campaign;
use App\CampaignCar;
use App\CampaignTypes;
use App\CarCompany;
use App\CarModels;
use App\CarName;
use App\CarType;
use App\Department;
use App\DriverPayment;
use App\Exports\AdvertisorPaymentnExport;
use App\Exports\CampaignExport;
use App\Exports\DriverPaymentExport;
use App\Exports\PricesExport;
use App\MakeModel;
use App\PriceRange;
use App\Status;
use App\AppliedCampaigns;
use App\DriverEnrollment;
use App\SystemSetting;
use App\Tickets;
use App\User;
use http\Client\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Type;
use function GuzzleHttp\Psr7\str;

class adminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth','verified']);
    }


    public function exportState($state_id)
    {
        $high_percentage=SystemSetting::where('name','high_percentage')->pluck('data')->first();
        if($state_id){
            return  Excel::download(new PricesExport($high_percentage, $state_id), 'bucket_prices.xlsx');
        }
         return  Excel::download(new PricesExport($high_percentage,'null'), 'bucket_prices.xlsx');
        $page_title = '';
        $page_description = '';
    }

    public function exportCampaign(Request $req){
//        $high_percentage=SystemSetting::where('name','high_percentage')->pluck('data')->first();
        return  Excel::download(new CampaignExport($req->status),date('m-d-y').'-campaigns.xlsx');
    }
    public function exportAdvertisorPayment(Request $req){
        return  Excel::download(new AdvertisorPaymentnExport($req->status), 'adv-payments-'.date('m-d-y').'.xlsx');
    }
    public function exportDriverPayment(Request $req){
        return  Excel::download(new DriverPaymentExport($req->status), 'driver -payments-'.date('m-d-y').'.xlsx');
    }
    public function index()
    {
//        dd()
        $page_title = 'Dashboard';
        $page_description = '';
        $user_id=Auth::id();
        $Payment_count=0;
        $dept_id = Auth::user()->department_id;
        if ($dept_id == '9999') {
             $data['tickets'] = Tickets::with('user', 'supporters')->orderBy('updated_at', 'DESC')->take(5)->get();
        } elseif ($dept_id) {
             $data['tickets'] = Tickets::where('department_id', $dept_id)->with('user', 'supporters')->orderBy('updated_at', 'DESC')->take(5)->get();
        } else {
             $data['tickets'] = Tickets::where('user_id', Auth::user()->id)->with('user', 'supporters')->orderBy('updated_at', 'DESC')->take(5)->get();
        }
            $amount=0;
        if(Auth::user()->role_id == 3 ){
            $my_campaigns=AppliedCampaigns::where('user_id',$user_id)->with('campaign.campaign')->orderBy('updated_at', 'DESC')->take(5)->get();
            $Payment_count=DriverPayment::where('user_id',$user_id)->count();
            $amount=DriverPayment::where('user_id',$user_id)->sum('amount');
        }
//        dd($my_campaigns);
        if(Auth::user()->role_id == 1){
            $my_campaigns=CampaignCar::with('campaign','user','status')->orderBy('updated_at' , 'DESC')->take(5)->get();
            $Payment_count=AdvertisorPayment::count();
            $amount=AdvertisorPayment::where('type','Paid')->sum('amount');
        }
        if(Auth::user()->role_id == 2 ){
            $my_campaigns=CampaignCar::where('user_id',$user_id)->with('campaign','user','status')->orderBy('updated_at' , 'DESC')->take(5)->get();
            $Payment_count=AdvertisorPayment::where('user_id',$user_id)->count();
            $amount=AdvertisorPayment::where('user_id',$user_id)->where('type','Paid')->sum('amount');
        }
        if(Auth::user()->role_id == 4){
            $my_campaigns=DriverEnrollment::with('appliedcampaigns.campaign.campaign','appliedcampaigns.user')->where('created_by',$user_id)->orderBy('updated_at' , 'DESC')->take(5)->get();
//        dd($my_campaigns);
        }
        if(session('email_verified')) {
            Session::forget('email_verified');
            Session::flash('emailverified', 'Email Verified Successfully');
        }
        $total_campaigns=CampaignCar::all()->count();
        $total_pending_campaigns=CampaignCar::where('status_id','6')->count();
//        Session::flash('message', 'Email Verified Successfully');
        return view('pages.dashboard', compact('page_title', 'page_description','my_campaigns','data','total_campaigns','total_pending_campaigns','Payment_count','amount'));
    }
    public function create_campaign_step2()
{
    $page_title = 'Dashboard';
    $page_description = 'Some description for the page';

    return view('pages.admin.sticker-creation', compact('page_title', 'page_description'));
}
    public function chats()
    {
        $page_title = '';
        $page_description = '';
        $users=User::where('id','!=',Auth::id())->where('role_id','3')->get();
        return view('pages.admin.chats', compact('page_title', 'page_description','users'));
    }

    public function tickets()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('pages.admin.tickets', compact('page_title', 'page_description'));
    }
    public function manage_campaign(Request $request){
        $check = 0;
        if ($request->campaign_status || $request->search ) {
            $status = Status::orderby('id', 'desc')->get();
            $campaigns = CampaignCar::where(function ($query) use ($request) {
                $query->where('status_id', $request->campaign_status);
            })->whereHas('campaign', function($q)use ($request) {
                $q->where('title', 'LIKE', '%' . $request->search . '%');
            })->orderby('id', 'desc')->with('status','sticker','campaign','state','city','appliedDrivers.driverDetail.campaignscount','refund_amount','sticker_payment')->paginate(20);
            $check = 1;
            return view('pages.admin.manage-campaign', compact('campaigns', 'status', 'check'));
        }

        $page_title = 'Campaign';
        $page_description = 'Campaign Management';
        $status=Status::orderby('id','desc')->get();
        $campaigns = CampaignCar::orderby('id', 'desc')->with('status','sticker','campaign','state','city','appliedDrivers.driverDetail.campaignscount','refund_amount')->paginate(20);
//        dd($campaigns);
        return view('pages.admin.manage-campaign', compact('page_title', 'page_description','campaigns','status', 'check'));
    }
    public function advertiser_notifications()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('pages.admin.notifications', compact('page_title', 'page_description'));
    }
    public function personal_information()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('pages.admin.personal-information', compact('page_title', 'page_description'));
    }
    public function account_information()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('pages.admin.account-information', compact('page_title', 'page_description'));
    }
    public function driver_change_password()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('pages.admin.change-password', compact('page_title', 'page_description'));
    }
    public function advertisor_notification_setting()
    {
        $page_title = 'Dashboard';
        $page_description = 'Some description for the page';

        return view('pages.admin.notification-setting', compact('page_title', 'page_description'));
    }
    public function admin_manage_users(){

        $page_title = 'Dashboard';
        $page_description = 'User Managment';

        return view('pages.admin.manage-user',compact('page_title', 'page_description'));
    }
    public function payments()
    {
        $page_title = '';
        $page_description = '';
        return view('pages.admin.payments', compact('page_title', 'page_description'));
    }
    public function reports()
    {
        $page_title = '';
        $page_description = '';

        return view('pages.admin.reports', compact('page_title', 'page_description'));
    }
    public function payment_details()
    {
        $page_title = '';
        $page_description = '';
        return view('pages.admin.payment-details', compact('page_title', 'page_description'));
    }
    public function driverEarning(Request $request){
        $compaignid=$request['compaign_id'];
       $data1=AppliedCampaigns::where('campaign_id',$compaignid)->get();
       foreach($data1 as $key=>$row){
           $driver_id[]=$row['user_id'];
        $drives[]=DriverEnrollment::where('user_id',$row['user_id'])->get();
         };

       foreach($drives as $driver){
           foreach($driver as $dir){
//            $dir['price']=;

        }
     };
        return response()->json($request);
    }
    public function carInfo(){
        $carmakers=CarCompany::with('car_names')->orderBy('id','desc')->get();
//        dd($carmakers);
        $carTypes=CarType::all();
     return view('pages.admin.car-info',compact('carmakers','carTypes'));

    }
    public function campaignType(){
            $types=CampaignTypes::all();
        return view('pages.admin.campaign-type',compact('types'));
    }
    public function addMaker(Request $request){
        $type=CarCompany::where('company',$request['companyy'])->first();
        if($type){
            return redirect()->back()->with('error','Company already exist')->withInput();
        }

        $data=CarCompany::create([
            'company'=>$request['companyy'],
        ]);
        if ($data){
           return redirect()->route('carInfo')->with('created','Company created successfully');
        }
    }
    public function filterCar($id){
    $carlists=CarModels::where('company_id',$id)->with('maker')->get();

    return view('pages.admin.partials._car-list',compact('carlists'));
}
    public function createType(Request $request){
//dd();
        $type=CampaignTypes::where('name',$request['name'])->first();
        if($type){
            return redirect()->back()->with('error','Type already exist')->withInput();
        }
        $data=CampaignTypes::create([
            'name'=>$request['name'],
            'status'=>1
        ]);
        if($data){
            return redirect()->route('campaignType')->with('success','Type created successfully');
        }

    }
    public function deleteType($id){
        $check=CampaignTypes::where('id',$id)->pluck('status')->first();
        if($check == 0){
        $data=CampaignTypes::where('id',$id)->update([
            "status"=>1
        ]);
        if($data){
            return redirect()->back()->with('deleted', 'Type Enabled successfully');
        }
        }
        else {

            $data = CampaignTypes::where('id', $id)->update([
                "status" => 0
            ]);
            if($data){
                return redirect()->back()->with('deleted', 'Type Disabled successfully');
            }
        }
    }
    public function deleteCar($id){
        $check=CarModels::where('id',$id)->pluck('status')->first();
        if($check == 1){
        $data=CarModels::where('id',$id)->update([
        "status"=>0
    ]);
            if($data){
                return redirect()->route('carInfo')->with('deleted','Disabled Successfully');
            }
        }else{
            $data=CarModels::where('id',$id)->update([
                "status"=>1
            ]);
            if($data){
                return redirect()->route('carInfo')->with('deleted','Enabled Successfully');
            }
        }

    }
    public function createCar(Request $request){
//        dd($request);
        $data=CarModels::create([
            "company_id"=>$request['maker_id'],
            "car_name_id"=>$request['car_name'],
            "car_model_id"=>$request['car_model'],
            "car_type"=>$request['car_type'],
        ]);
    if($data){
    return redirect()->route('carInfo')->with('created','Car created successfully');
        }else{
        return redirect()->route('carInfo')->with('error','Something Went Wrong');
    }

    }
    public function makeCar(Request $request){
//        $this->validate($request, [
//            'car_name' => ['required', 'string', 'max:255','unique:car_name.car_name'],
//        ]);
        $car = new CarName;



        $car->car_name = ucfirst($request->car_name);
        $car->company_id =ucfirst($request->company_id);
        $car->save();
        if ($car){
            return redirect()->route('carInfo')->with('created','Car created successfully');
        }
        else{
            return redirect()->route('carInfo')->with('error','Something went wrong');
        }
    }

    public function makeCar1(Request $request){
        $rules = array('car_name' =>'required|string|max:255|unique:car_name');
        $validator = Validator::make($request->all(), $rules);
                if ($validator->fails())
                {
                    return response()->json(array(
                        'success' => false,
                        'errors' => $validator->getMessageBag()->toArray()

                    ), 200);
                }
//                $this->validate($request, [
//            'car_name' => ['required', 'string', 'max:255','unique:car_name'],
//        ]);


        $car = new CarName;
        $car->car_name = ucfirst($request->car_name);
        $car->company_id =$request->car_make_id;
        $car->save();
        if ($car){
            return $car;
        }else{
           return null;
        }

    }
    public function getCar($id){
        $car_name = CarName::where('company_id',$id)->get();
        return view('pages.admin.partials._select_car_name' ,compact('car_name'));

    }
    public function getModel($id){
//        dd($id);
        $car_model = MakeModel::where('car_name_id',$id)->get();
//       dd($car_model);
        return view('pages.admin.partials._car_models' ,compact('car_model'));

    }    public function getModels($id){
        $car_model = MakeModel::where('car_name_id',$id)->get();
//       dd($car_model);
        return $car_model;

    }
    public function makeModel(Request $request){

        $data=MakeModel::create([
            "company_id"=>$request['company_id'],
            "car_name_id"=>$request['car_name'],
            "model_name"=>$request['model'],
        ]);
        if ($data){
            return redirect()->route('carInfo')->with('created','Model Created Successfully');
        }
        else{
            return redirect()->route('carInfo')->with('error','Something Went Wrong');
        }
    }
    public function makeModel1(Request $request){
//        return $request;
        $data=MakeModel::where([
            "company_id"=>$request['car_company'],
            "car_name_id"=>$request['car_name'],
            "model_name"=>$request['option_value']])->first();

        if ($data)
        {
            return response()->json(array(
                'success' => false,
                'errors' => 'Data Already exist'
            ), 200);
        }
        $data=MakeModel::create([
            "company_id"=>$request['car_company'],
            "car_name_id"=>$request['car_name'],
            "model_name"=>ucfirst($request['option_value']),
        ]);
        if ($data){
            return $data;
        }
        else{
            return null;
        }
    }
    public function addType(Request $request){
        $data=CarType::create([
            "name"=>ucfirst($request['car_type'])
        ]);
        if ($data){
            return redirect()->route('carInfo')->with('created','Type Created Successfully');
        }
        else{
            return redirect()->route('carInfo')->with('error','Something Went Wrong');
        }
    }
    public function addType1(Request $request){
        $data=CarType::where('name',ucfirst($request['option_value']))->first();
        if ($data) {
            return response()->json(array(
                'success' => false,
                'errors' => 'Type already exist'
            ), 200);
        }
        $rules = array('option_value' =>'required|string|max:255');
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails())
        {return response()->json(array(
                'success' => false,
                'errors' => 'Field is empty'

            ), 200);
        }

//        dd($request);
        $data=CarType::create([
            "name"=>ucfirst($request['option_value'])
        ]);
        if ($data){
            return $data;
        }
        else{
            return null;
        }
    }


}
