<?php

namespace App\Http\Controllers;
use App\AdvertisorPayment;
use App\Campaign;
use App\CampaignCar;
use App\CampaignSticker;
use App\DriverEnrollment;
use App\DriverPayment;
use App\Http\Controllers\Controller;

use App\Models\Admin;
use App\Payment;
use App\StickerPayment;
use App\User;
use App\UserNotification;
use App\UserNotificationSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentsController extends Controller
{
    public function getPayments(Request $request){
        if($request->status){
            $payments=AdvertisorPayment::where('status',$request->status)->with('campaign')->where('user_id',Auth::user()->id)->orderby('id','desc')->paginate('20');
            return view('pages.advertisor.payments', compact('payments'));
        }

        $payments=AdvertisorPayment::where('user_id',Auth::user()->id)->with('campaign')->orderby('id','desc')->paginate('20');
        $total=AdvertisorPayment::where('user_id',Auth::user()->id)->where('type','Paid')->sum('amount');
        return view('pages.advertisor.payments', compact('payments','total'));
    }

    public function getDriverPayments(Request $request){

        if($request->status){
            $payments=DriverPayment::where('user_id',Auth::user()->id)->with('campaign')->where('status',$request->status)->with('campaign.campaign')->orderby('id','desc')->paginate('20');
            return view('pages.driver.paymentss', compact('payments'));
        }
         $payments=DriverPayment::where('user_id',Auth::user()->id)->with('campaign.campaign')->orderby('id','desc')->paginate('20');
         $total=DriverPayment::where('user_id',Auth::user()->id)->sum('amount');
        return view('pages.driver.paymentss', compact('payments','total'));
    }
    public function getMyDriverPayments(Request $request){
        $payments=DriverEnrollment::where('created_by',Auth::user()->id)->with('payments.user')
            ->whereHas('payments', function($q) {
            $q->groupBy('payments');
        })->paginate(50);
//dd($payments);
//         $total=DriverPayment::where('user_id',Auth::user()->id)->sum('amount');
        return view('pages.driver.enrolled-payments', compact('payments'));
    }

    public function getAllPaymentsdrivers(Request $request){
        $drivers=User::where('role_id','3')->whereHas('payments')->get();
        if($request->status){
            $payments=DriverPayment::where('user_id',$request->status)->with('user','campaign')->orderby('id','desc')->paginate('20');
            return view('pages.admin.driver_payments', compact('payments','drivers'));
        }
         $payments=DriverPayment::with('user','campaign')->orderby('id','desc')->paginate('20');
         $total=DriverPayment::sum('amount');

        return view('pages.admin.driver_payments', compact('payments','total','drivers'));
    }



    public function getAllPayments(Request $request){
        if($request->status){
            $payments=AdvertisorPayment::where('status',$request->status)->with('user')->orderby('id','desc')->paginate('20');
            return view('pages.admin.payments',  compact('payments'));
        }

        $payments=AdvertisorPayment::orderby('id','desc')->with('user')->paginate('20');
        $total=AdvertisorPayment::where('type','Paid')->sum('amount');
//        dd($total);
        return view('pages.admin.payments', compact('payments','total'));
    }



        public function stripePayment(Request $request){
        $campaign=CampaignCar::where('id', $request->campaign_id)->first();
        $amount=100*$campaign->total_price;
        \Stripe\Stripe::setApiKey ('sk_test_51HlYDGK7l7e9CDWNkoO0qNADzMNhm6gE8uXZqMYJ3GkYglm99gAbSYS0xwFSienndwk3nPLyra0KSLuQ8jWtsd9M00HnKL1Nhy');
        try {
            $payment_charge_response=\Stripe\Charge::create (array (
                "amount" => $amount,
                "currency" => "usd",
                "source" => $request->input ( 'stripeToken' ), // obtained with Stripe.js
                "description" => 'User id ='.Auth::user()->id.' & Amount = '. $amount,
            ) );


//            $this->saveTransaction($request,$payment_charge_response);
            $campaign->status_id=3;
            $campaign->payment_status='Paid';
            $campaign->save();
            $payment=new AdvertisorPayment;
            $payment->campaign_id=$campaign->id;
            $payment->user_id=Auth::user()->id;
            $payment->stripe_response=$payment_charge_response;
            $payment->token_id = $request->input ('stripeToken');
            $payment->type = 'Paid';
            $payment->billing_detail = $payment_charge_response->billing_details;
            $payment->company_name=$request->company_name;
            $payment->status=$payment_charge_response->status;
            $payment->charge_id = $payment_charge_response->id;
            $payment->amount = $campaign->total_price;
            $payment->save();

            $admin=Admin::where('role_id', '1')->first();
            if ($admin){
                $not_create = new  UserNotification();
                $not_create->user_id = $admin->id;
                $not_create->notification_type_id = 45;
                $not_create->subject = "Payment paid by advertiser against campaign#CMP-".$campaign->id;
                $not_create->detail = "Paid Amount is:$".$campaign->total_price;
                $not_create->save();
            }


            return redirect()->back()->with('success', 'Transaction successfull');
        } catch ( \Exception $e ) {
            return redirect()->back()->with('error', 'Error occured. Try again');
        }

    }

        public function stripeStickerPayment(Request $request){
        $sticker=CampaignSticker::where('campaign_id', $request->campaign_id)->first();
        $amount=100*$sticker->bespoke_price;
//        $stripe = new \Stripe\StripeClient('sk_test_51HlYDGK7l7e9CDWNkoO0qNADzMNhm6gE8uXZqMYJ3GkYglm99gAbSYS0xwFSienndwk3nPLyra0KSLuQ8jWtsd9M00HnKL1Nhy');
//        $customer = $stripe->customers->create([
//            'description' => 'example customer',
//            'email' => 'email@example.com',
//            'payment_method' => 'pm_card_visa',
//        ]);
//        \Stripe\Stripe::setApiKey (env('STRIPE_SECRET_KEY'));
        \Stripe\Stripe::setApiKey ('sk_test_51HlYDGK7l7e9CDWNkoO0qNADzMNhm6gE8uXZqMYJ3GkYglm99gAbSYS0xwFSienndwk3nPLyra0KSLuQ8jWtsd9M00HnKL1Nhy');
        try {
            $payment_charge_response=\Stripe\Charge::create ( array (
                "amount" => $amount,
                "currency" => "usd",
                "source" => $request->input ( 'stripeToken' ), // obtained with Stripe.js
                "description" => 'User id ='.Auth::user()->id.' & Amount = '. $amount." against bespoke sticker",
            ) );
//            $this->saveTransaction($request,$payment_charge_response);
            $sticker->bespoke_status='Paid';
            $sticker->save();
            $payment=new StickerPayment;
            $payment->campaign_id=$sticker->campaign_id;
            $payment->user_id=Auth::user()->id;
            $payment->stripe_response=$payment_charge_response;
            $payment->token_id = $request->input ('stripeToken');
            $payment->type = 'Paid';
            $payment->billing_detail = $payment_charge_response->billing_details;
            $payment->company_name=$request->company_name;
            $payment->status=$payment_charge_response->status;
            $payment->charge_id = $payment_charge_response->id;
            $payment->medium = 'Stripe';
            $payment->amount = $sticker->bespoke_price;
            $payment->save();

            $admin=Admin::where('role_id', '1')->first();
            if ($admin) {
                $not_create = new  UserNotification();
                $not_create->user_id = $admin->id;
                $not_create->notification_type_id = 45;
                $not_create->subject = "Sticker Payment paid by advertiser: ".Auth::user()->name;
                $not_create->detail = "Paid amount is:$".$sticker->bespoke_price;
                $not_create->save();
            }


            return redirect()->back()->with('success', 'Transaction successfull');
        } catch ( \Exception $e ) {
            return redirect()->back()->with('error', 'Error occured. Try again');
        }

    }

//    public function saveTransaction($request,$payment_charge_response){
//        $transaction= new Payment();
//        $transaction->user_id = Auth::user()->id;
//        $transaction->campaign_id='11';
//        $transaction->type = 'stripe';
//        $transaction->charge_id = $payment_charge_response->id;
//        $transaction->token_id = $request->input ('stripeToken');
//        $transaction->response = $payment_charge_response->billing_details;
//        $transaction->amount='300';
//        $transaction->status=$payment_charge_response->status;
//        $transaction->save();
//
//        return true;
//
//    }

    public function stripePaymentView(Request $request)
    {
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://api.escrow.com/2017-09-01/transaction/',
//            CURLOPT_RETURNTRANSFER => 1,
//            CURLOPT_USERPWD => 'wasimsarwar03@gmail.com:14347_YSgqR7VZ26KPsxjVDRwl7vlsNukAmVRg2SKTRw8wEwzQlUczIBJ6wrycYfAaa3E4',
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json'
//            )
//        ));
//
//        $output = curl_exec($curl);
//        dd( $output);
//        curl_close($curl);
//
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://api.escrow.com/2017-09-01/transaction',
//            CURLOPT_RETURNTRANSFER => 1,
//            CURLOPT_USERPWD => 'wasimsarwar03@gmail.com:14347_YSgqR7VZ26KPsxjVDRwl7vlsNukAmVRg2SKTRw8wEwzQlUczIBJ6wrycYfAaa3E4',
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json'
//            ),
//            CURLOPT_POSTFIELDS => json_encode(
//                array(
//                    'currency' => 'usd',
//                    'items' => array(
//                        array(
//                            'description' => 'johnwick.com',
//                            'schedule' => array(
//                                array(
//                                    'payer_customer' => 'me',
//                                    'amount' => '1000.0',
//                                    'beneficiary_customer' => 'keanu.reaves@test.escrow.com',
//                                ),
//                            ),
//                            'title' => 'johnwick.com',
//                            'inspection_period' => '259200',
//                            'type' => 'domain_name',
//                            'quantity' => '1',
//                        ),
//                    ),
//                    'description' => 'The sale of johnwick.com',
//                    'parties' => array(
//                        array(
//                            'customer' => 'me',
//                            'role' => 'buyer',
//                        ),
//                        array(
//                            'customer' => 'keanu.reaves@test.escrow.com',
//                            'role' => 'seller',
//                        ),
//                    ),
//                )
//            )
//        ));
//        $output = curl_exec($curl);
//        dd($output);
//        curl_close($curl);











//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://api.escrow.com/2017-09-01/customer/me',
//            CURLOPT_RETURNTRANSFER => 1,
//            CURLOPT_USERPWD => 'wasimsarwar03@gmail.com:14347_YSgqR7VZ26KPsxjVDRwl7vlsNukAmVRg2SKTRw8wEwzQlUczIBJ6wrycYfAaa3E4',
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json'
//            ),
//        ));
//
//        $output = curl_exec($curl);
//         dd($output);
//        curl_close($curl);


//        14347_YSgqR7VZ26KPsxjVDRwl7vlsNukAmVRg2SKTRw8wEwzQlUczIBJ6wrycYfAaa3E4
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => 'https://api.escrow.com/integration/pay/2018-03-31',
//            CURLOPT_RETURNTRANSFER => 1,
//            CURLOPT_USERPWD => 'wasimsarwar03@gmail.com:Aa6751634',
//            CURLOPT_HTTPHEADER => array(
//                'Content-Type: application/json'
//            ),
//            CURLOPT_POSTFIELDS => json_encode(
//                array(
//                    'currency' => 'usd',
//                    'description' => 'Perfect sedan for the snow',
//                    'items' => array(
//                        array(
//                            'extra_attributes' => array(
//                                'make' => 'BMW',
//                                'model' => '328xi',
//                                'year' => '2008',
//                            ),
//                            'fees' => array(
//                                array(
//                                    'payer_customer' => 'me',
//                                    'split' => '1',
//                                    'type' => 'escrow',
//                                ),
//                            ),
//                            'inspection_period' => '259200',
//                            'quantity' => '1',
//                            'schedule' => array(
//                                array(
//                                    'amount' => '8000',
//                                    'beneficiary_customer' => 'me',
//                                    'payer_customer' => 'john.wick@test.escrow.com',
//                                ),
//                            ),
//                            'title' => 'BMW 328xi',
//                            'type' => 'motor_vehicle',
//                        ),
//                    ),
//                    'parties' => array(
//                        array(
//                            'address' => array(
//                                'city' => 'San Francisco',
//                                'country' => 'US',
//                                'line1' => '180 Montgomery St',
//                                'line2' => 'Suite 650',
//                                'post_code' => '94104',
//                                'state' => 'CA',
//                            ),
//                            'agreed' => 'true',
//                            'customer' => 'john.wick@test.escrow.com',
//                            'date_of_birth' => '1980-07-18',
//                            'first_name' => 'John',
//                            'initiator' => 'false',
//                            'last_name' => 'Wick',
//                            'phone_number' => '4155555555',
//                            'role' => 'buyer',
//                        ),
//                        array(
//                            'agreed' => 'true',
//                            'customer' => 'me',
//                            'initiator' => 'true',
//                            'role' => 'seller',
//                        ),
//                    ),
//                )
//            )
//        ));

//        $output = curl_exec($curl);
//        dd($output);
//        curl_close($curl);

        // dd($request->id);
        return view('pages.payments.stripe');
    }

}
