<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;


    public function show(Request $request)
    {

        if(Auth::guard('user')->user()==null){
            return redirect('/');
        }





//        dd('aa');
        return Auth::guard('user')->user()->hasVerifiedEmail()
            ? redirect($this->redirectPath())
            : view('auth.verify');
    }


    public function resend(Request $request)
    {
        if (Auth::guard('user')->user()->hasVerifiedEmail()) {
            return $request->wantsJson()
                ? new JsonResponse([], 204)
                : redirect($this->redirectPath());
        }

        Auth::guard('user')->user()->sendEmailVerificationNotification();

        return $request->wantsJson()
            ? new JsonResponse([], 202)
            : back()->with('resent', true);
    }


    public function verify(Request $request,$id)
    {

        if(Auth::guard('user')->user()==null){
          $data=User::where('id',$id)->update(['email_verified_at'=>Carbon::now()]);
            session(['email_verified' => 'true']);
            $user = User::find($id);
          if($user){
              Auth::guard('user')->login($user);
              return redirect()->route('user.dashboard');
          }
        }
        if (! hash_equals((string) $request->route('id'), (string) Auth::guard('user')->user()->getKey())) {
            throw new AuthorizationException;
        }
//

        if (! hash_equals((string) $request->route('hash'), sha1(Auth::guard('user')->user()->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if (Auth::guard('user')->user()->hasVerifiedEmail()) {

            return $request->wantsJson()
                ? new JsonResponse([], 204)
                : redirect($this->redirectPath());
        }

        if (Auth::guard('user')->user()->markEmailAsVerified()) {
            session(['email_verified' => 'true']);
            event(new Verified($request->user()));
        }

        if ($response = $this->verified($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new JsonResponse([], 204)
            : redirect($this->redirectPath())->with('verified', true);
    }




    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
}
