<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\CampaignAvailableMail;
use App\Providers\RouteServiceProvider;
use App\User;
use App\UserNotification;
use App\UserNotificationSetting;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
//    protected function validator(array $data)
//    {
//        dd($data);
//        return Validator::make($data, [
////            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
////            'password' => ['required', 'string', 'min:8', 'confirmed'],
//            'first_name' => ['required', 'string', 'max:255'],
//            'last_name' => ['required', 'string', 'max:255'],
//            'country_id' => ['required', 'string', 'max:255'],
//            'city_id' => ['required', 'string', 'max:255'],
//            'zip' => ['required', 'string', 'max:255'],
//            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
//            'pwd' => ['required', 'string', 'min:4'],
//        ]);
//    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
//    protected function create(array $data)
//    {
////       dd( $data);
////        return User::create([
////            'name' => $data['name'],
////            'email' => $data['email'],
////            'password' => Hash::make($data['password']),
////        ]);
////        dd($data);
//         $role='';
//        if(isset($data['driver_type'])){
//            $role=$data['driver_type'];
//        }
//        else if(isset($data['role'])){
//            $role=$data['role'];
//        }
//        $data2 =User::create([
//            'name' => $data['first_name'],
//            'email' => $data['email'],
//            'password' => Hash::make($data['pwd']),
//            'role_id' =>$role,
//        ]);
//        if($data2) {
//            $notification_on_users = UserNotificationSetting::where('notification_id','35')->get();
//            foreach ($notification_on_users as $user) {
//                $not_create = new  UserNotification();
//                $not_create->user_id = $user->user_id;
//                $not_create->notification_type_id = $user->notification_id;
//                $not_create->subject = "New user registered";
//                $not_create->detail = "Name:".$data2->name." & Email:".$data2->email;
//                $not_create->save();
//            }
//            return $data2;
//        }
//    }
}
