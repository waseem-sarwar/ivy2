<?php

namespace App\Http\Controllers;

use App\AppliedCampaigns;
use App\Campaign;
use App\CampaignCar;
use App\CarCompany;
use App\CarModels;
use App\Country;
use App\Department;
use App\DriverEnrollment;
use App\Industry;
use App\MakeModel;
use App\Models\Admin;
use App\Notification;
use App\ProfileDepartment;
use App\State;
use App\Status;
use App\TicketType;
use App\User;
use App\UserNotificationSetting;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class DriverEntController extends Controller
{

    public function __construct()
    {
        $this->middleware(['verified']);

    }

    public function home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.home', compact('page_title', 'page_description'));
    }public function about()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.about', compact('page_title', 'page_description'));
    }public function advertisor1()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.advertisor1', compact('page_title', 'page_description'));
    }public function advertisor2()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.advertisor2', compact('page_title', 'page_description'));
    }public function contact()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.contact', compact('page_title', 'page_description'));
    }public function driver1()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.driver1', compact('page_title', 'page_description'));
    }public function driver2()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.driver2', compact('page_title', 'page_description'));
    }public function faqs()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.faqs', compact('page_title', 'page_description'));
    }public function how_works()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.how_works', compact('page_title', 'page_description'));
    }public function login_home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.login_home', compact('page_title', 'page_description'));
    }public function reset_password()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.reset_password', compact('page_title', 'page_description'));
    }
    public function terms()
    {
//        phpinfo();
//        dd('here');
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.terms', compact('page_title', 'page_description'));
    }
    public function index()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor-dashboard', compact('page_title', 'page_description'));
    }
    public function basic_enrollment()
    {
        $page_title = 'Enrollment';
        $page_description = '';
        $countries=country::all();
        $car_companies=CarCompany::all();
        $Car_models=CarModels::all();
        $states= State::orderby('state', 'asc')->get();
        $Cars = CarModels::select('company_id')->distinct()->get();

        return view('pages.driver_ent.driver-enrollment', compact('Car_models','page_title', 'page_description','countries','car_companies','states','Cars'));

    }

    public function enrollmentCreate(Request $request)
    {
        @$request->dob= Carbon::parse($request->dob)->format('m/d/Y');
//dd($request->passsword);
        $request->validate([
            'first_name' => 'required|max:22',
            'last_name' => 'required|max:22',
            'email' => 'required|email|unique:users|max:33',
            'passsword' => 'required|min:4|max:12|',
            'car_make' => 'required|max:33',
            'car_model' => 'required|max:23',
            'car_type' => 'required|max:33',
            'car_year' => 'required|max:21',
            'driving_state' => 'required|max:22',
            'car_drive_city' => 'required|max:22',
            'car_drive_zip' => 'required|max:22',
            'dob' => 'before:-18 years',
        ]);
        Session::flash('pwd', $request->passsword);
        Session::flash('email', $request->email);
        Session::flash('name', $request->first_name);
//        $ent_id=auth()->id();
//        $company_data=UserProfile::where('user_id',$ent_id)->get()->first();
        $user=User::create([
            "name"=>$request->first_name,
            "email"=>$request->email,
            "password"=>Hash::make($request->passsword),
            "role_id"=>3,
        ]);
//        $user['pwd']=$request->passsword;
        event(new Registered($user));

        $user_profile=UserProfile::create([
            "first_name"=>$request->first_name,
            "last_name"=>$request->last_name,
            "dob"=>$request->dob,
//            "country_id"=>$request->country_personal,
            "city_id"=>$request->car_drive_city,
            "user_id"=>$user->id,
            "address"=>$request->billing_address,
//            "street_address"=>$request->street,
            "zip_code"=>$request->car_drive_zip,

        ]);
        $user_id = $user->id;
        $data=new DriverEnrollment;
        $data->user_id= $user_id;
        $data->created_by= Auth::id();
        $data->first_name= $request->first_name;
        $data->last_name= $request->last_name;
        $data->dob= $request->dob;
        $data->billing_address_personal= $request->billing_address;
        $data->zip_code_personal= $request->zip_code;
        $data->car_make= $request->car_make;
        $data->car_model= $request->car_model;
        $data->car_type= $request->car_type;
        $data->car_year= $request->car_year;
        $data->days_drive_in_month= $request->days_drive_in_month;
        $data->car_drive_country= $request->car_drive_country;
        $data->driving_state= $request->driving_state;
        $data->car_drive_city= $request->car_drive_city;
        $data->car_drive_zip= $request->car_drive_zip;
        $data->car_drive_address= $request->drive['address'];

        //days
        $str=null;
//        dd($request->car_info['days']);
        if($request->car_info) {
            foreach ($request->car_info['days'] as $index => $days) {
                $str .= $days . ', ';
            }
            $str = rtrim($str, ' ,');
            }

                $data->days=  $str;
                $car_photo=null;
                if($request->car_photo){
                    $car_photo = 'car_photo_'.time().'.'.$request->car_photo->extension();
                    $request->car_photo->move(public_path('images/enrollements'), $car_photo);

                }
                $car_video=null;
                if($request->car_video){
                    $car_video = 'car_video_'.time().'.'.$request->car_video->extension();
                    $request->car_video->move(public_path('images/enrollements'), $car_video);

                }
                $mileage_proof=null;
                if($request->mileage_proof){
                    $mileage_proof = 'mileage_proof_'.time().'.'.$request->mileage_proof->extension();
                    $request->mileage_proof->move(public_path('images/enrollements'), $mileage_proof);
                }
                $car_registration_proof=null;
                if($request->car_registration_proof){
                    $car_registration_proof = 'car_registration_proof_'.time().'.'.$request->car_registration_proof->extension();
                    $request->car_registration_proof->move(public_path('images/enrollements'), $car_registration_proof);
                }
                $license=null;
                if($request->license){
                    $license = 'license_'.time().'.'.$request->license->extension();
                    $request->license->move(public_path('images/enrollements'), $license);
                }
                $cnic=null;
                if($request->cnic){
                    $cnic = 'cnic_'.time().'.'.$request->cnic->extension();
                    $request->cnic->move(public_path('images/enrollements'), $cnic);
                }
        $data->car_photo= $car_photo;
        $data->car_video= $car_video;
        $data->car_milage_proof= $mileage_proof;
        $data->car_registration_proof= $car_registration_proof;
        $data->driving_license_path= $license;
        $data->cnic_path= $cnic;
        $data->hours_drive_a_car= $request->how_many_hours_drive;
        $data->mileage_of_car= $request->how_often_mileage;
        $data->save();

        return redirect('/user/ent/drivers')->with('message','Enrolled successfully');
//        return redirect('/ent/drivers?success=true');
    }




    public function enrollmentCreate1(Request $request)
    {
//        dd($request);
        $request->validate([
//            'first_name' => 'required|max:255',
//            'last_name' => 'required|max:255',
            'car_make' => 'required|max:255',
            'dob' => 'date_format:m/d/Y|before:-18 years',
            'car_model' => 'required|max:255',
            'car_type' => 'required|max:255',
            'car_year' => 'required|max:255',
            'billing_address' => 'required|max:255',
            'driving_state' => 'required|max:255',
            'car_drive_city' => 'required|max:255',
            'car_drive_zip' => 'required|max:255',
        ]);


        $data=new DriverEnrollment;
        $data->user_id= Auth::id();
//        $data->first_name= $request->first_name;
//        $data->last_name= $request->last_name;
        $data->dob= $request->dob;
//        $data->country_personal= $request->driver['country'];
//        $data->street_personal= $request->street;
        $data->billing_address_personal= $request->billing_address;
//        $data->zip_code_personal= $request->zip_code;
        $data->car_make= $request->car_make;
        $data->car_model= $request->car_model;
        $data->car_type= $request->car_type;
        $data->car_year= $request->car_year;
        $data->days_drive_in_month= $request->days_drive_in_month;
        $data->driving_state= $request->driving_state;
        $data->car_drive_city= $request->car_drive_city;
        $data->car_drive_zip= $request->car_drive_zip;
        $data->car_drive_address= $request->drive['address'];


        //days
        $str=null;

        if($request->car_info){
            foreach ($request->car_info['days'] as $index => $days) {
                $str .= $days . ', ';
            }
            $str = rtrim($str, ' ,');
        }

        $data->days=  $str;
//        dd($request);
        $car_photo=null;
        if($request->car_photo){
            $car_photo = 'car_photo_'.time().'.'.$request->car_photo->extension();
            $request->car_photo->move(public_path('images/enrollements'), $car_photo);

        }
        $car_video=null;
        if($request->car_video){
            $car_video = 'car_video_'.time().'.'.$request->car_video->extension();
            $request->car_video->move(public_path('images/enrollements'), $car_video);

        }
        $mileage_proof=null;
        if($request->mileage_proof){
            $mileage_proof = 'mileage_proof_'.time().'.'.$request->mileage_proof->extension();
            $request->mileage_proof->move(public_path('images/enrollements'), $mileage_proof);
        }
        $car_registration_proof=null;
        if($request->car_registration_proof){
            $car_registration_proof = 'car_registration_proof_'.time().'.'.$request->car_registration_proof->extension();
            $request->car_registration_proof->move(public_path('images/enrollements'), $car_registration_proof);
        }
        $license=null;
        if($request->license){
            $license = 'license_'.time().'.'.$request->license->extension();
            $request->license->move(public_path('images/enrollements'), $license);
        }
        $cnic=null;
        if($request->cnic){
            $cnic = 'cnic_'.time().'.'.$request->cnic->extension();
            $request->cnic->move(public_path('images/enrollements'), $cnic);
        }
        $data->car_photo= $car_photo;
        $data->car_video= $car_video;
        $data->car_milage_proof= $mileage_proof;
        $data->car_registration_proof= $car_registration_proof;
        $data->driving_license_path= $license;
        $data->cnic_path= $cnic;
        $data->hours_drive_a_car= $request->how_many_hours_drive;
        $data->mileage_of_car= $request->how_often_mileage;
        $data->save();
        return redirect()->back()->with('update', 'You have enrolled Successfully');
    }

    public function email_update(Request $request)
    {
        $request->validate([
            'email' => 'required|email|max:255|unique:users',
        ]);

        $data=User::find($request->id);
        $data->email=$request->email;
        $data->email_verified_at=NULL;
        $data->remember_token=NULL;

        $data->save();
        event(new Registered($data));
        return 1;
    }
    public function enrollmentUpdate(Request $request)
    {
//dd($request);
        $request->validate([
            'car_make' => 'required|max:255',
            'car_model' => 'required|max:255',
            'car_type' => 'required|max:255',
            'car_year' => 'required|max:255',
            'billing_address' => 'required|max:255',
            'driving_state' => 'required|max:255',
            'car_drive_city' => 'required|max:255',
            'car_drive_zip' => 'required|max:255',
            'dob' => 'before:-18 years',
        ]);

        $data=DriverEnrollment::find($request->driver_id);
        $data->dob= $request->dob;
//        $data->country_personal= $request->driver['country'];
//        $data->street_personal= $request->driver['street'];
        $data->billing_address_personal= $request->billing_address;
        $data->zip_code_personal= $request->zip_code;
        $data->car_make= $request->car_make;
        $data->car_model= $request->car_model;
        $data->car_type= $request->car_type;
        $data->car_year= $request->car_year;
        $data->days_drive_in_month= $request->days_drive_in_month;
//        $data->car_drive_country= $request->drive['country'];;
        $data->driving_state= $request->driving_state;
        $data->car_drive_city= $request->car_drive_city;
        $data->car_drive_zip= $request->car_drive_zip;
        $data->car_drive_address= $request->drive['address'];

            $str=null;
            if($request->car_info) {
                foreach ($request->car_info['days'] as $index => $days) {
                    $str .= $days . ', ';
                }
                $str = rtrim($str, ' ,');
                }
                $data->days=  $str;
                $car_photo=null;
                if($request->car_photo){
                    $car_photo = 'car_photo_'.time().'.'.$request->car_photo->extension();
                    $request->car_photo->move(public_path('images/enrollements'), $car_photo);
                    $data->car_photo= $car_photo;

                }
                $car_video=null;
                if($request->car_video){
                    $car_video = 'car_video_'.time().'.'.$request->car_video->extension();
                    $request->car_video->move(public_path('images/enrollements'), $car_video);
                    $data->car_video= $car_video;

                }
                $mileage_proof=null;
                if($request->mileage_proof){
                    $mileage_proof = 'mileage_proof_'.time().'.'.$request->mileage_proof->extension();
                    $request->mileage_proof->move(public_path('images/enrollements'), $mileage_proof);
                    $data->car_milage_proof= $mileage_proof;
                }
                $car_registration_proof=null;
                if($request->car_registration_proof){
                    $car_registration_proof = 'car_registration_proof_'.time().'.'.$request->car_registration_proof->extension();
                    $request->car_registration_proof->move(public_path('images/enrollements'), $car_registration_proof);
                    $data->car_registration_proof= $car_registration_proof;
                }
                $license=null;
                if($request->license){
                    $license = 'license_'.time().'.'.$request->license->extension();
                    $request->license->move(public_path('images/enrollements'), $license);
                    $data->driving_license_path= $license;
                }
                $cnic=null;
                if($request->cnic){
                    $cnic = 'cnic_'.time().'.'.$request->cnic->extension();
                    $request->cnic->move(public_path('images/enrollements'), $cnic);
                    $data->cnic_path= $cnic;
                }
        $data->hours_drive_a_car= $request->how_many_hours_drive;
        $data->mileage_of_car= $request->how_often_mileage;
        $data->save();
        return redirect()->back()->with('update', 'Driver record updated Successfully');
    }
    public function basic_enrollment_update($id)
    {

        $states=State::orderby('state', 'asc')->get();
        $driver = DriverEnrollment::where('id',$id)->with('car_yearr','car_typee','car_namee')->first();
        $countries=country::all();
        $car_companies=CarCompany::all();
        $Cars = CarModels::select('company_id')->distinct()->get();
//        dd($Cars);
        return view('pages.driver_ent.driver-enrollment-update', compact('driver','countries','car_companies','Cars','states'));
    }

    public function enrollment_update()
    {

        $id=Auth::user()->id;
        $driver = DriverEnrollment::where('user_id',$id)->with('city')->first();
        $countries=country::all();
        $car_companies=CarModels::all();
        $Cars = CarModels::select('company_id')->distinct()->get();
        $states=State::orderby('state', 'asc')->get();
        return view('pages.driver_ent.driver-enrollment-update', compact('driver','countries','car_companies','Cars','states'));
    }
    public function advertisor_home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.advertisor-dashboard', compact('page_title', 'page_description'));
    }
    public function driver_home()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_dashboard', compact('page_title', 'page_description'));
    } public function ent_driver_home()
    {
//        dd('dfdfdfd');
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.enterprise_driver_dashboard', compact('page_title', 'page_description'));
    }
    public function create_campaign()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.create-campaign', compact('page_title', 'page_description'));
    }public function create_campaign_step2()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.sticker-creation', compact('page_title', 'page_description'));
    }
    public function chats()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.chats', compact('page_title', 'page_description'));
    }public function driver_chats()
    {

        $page_title = 'Dashboard';

        $page_description = '';

        return view('pages.driver_ent.chats', compact('page_title', 'page_description'));
    }
    public function tickets()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.tickets', compact('page_title', 'page_description'));
    }public function driver_tickets()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.tickets', compact('page_title', 'page_description'));
    }
    public function personal_information()
    {

       $id=Auth::user()->id;
       if(Auth::user()->role_id=='1'){
           $user =Admin::where('id',$id)->with('profile')->first();
       }else {
           $user = User::with('profile')->where('id', $id)->first();
//           dd();
       }
        $countries = Country::all();
        $states = State::orderby('state', 'asc')->get();
        $page_title = 'Profile';
        $page_description = '';

        if(Auth::user()->role_id=='1'){
//            dd('d');
            return view('admin.pages.personal-information',compact(['page_title', 'page_description','user','countries','states']));
        }
//        dd();
        return view('pages.driver_ent.personal-information', compact(['page_title', 'page_description','user','countries','states']));
    }
    public function driver_personal_information()
    {


        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.personal-information', compact('page_title', 'page_description'));
    }
    public function account_information()
    {
        $id=Auth::user()->id;
        $departments_dropdown=ProfileDepartment::where('status','1')->orderby('id','desc')->get();
        $user = User::with('profile')->where('id',$id)->first();
        $industries=Industry::where('status','1')->get();
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.account-information', compact(['page_title', 'page_description','user','departments_dropdown','industries']));
    }
    public function driver_account_information()
        {
            $page_title = 'Dashboard';
            $page_description = '';

            return view('pages.driver_ent.account-information', compact('page_title', 'page_description'));
        }
    public function driver_change_password()
    {

        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.change-password', compact('page_title', 'page_description'));
    }
    public function admin_change_password()
    {

        $page_title = 'Dashboard';
        $page_description = '';

        return view('admin.pages.change-password', compact('page_title', 'page_description'));
    }
    public function notification_setting()
    {
        $role=Auth::user()->role_id;
        $data=Notification::wherein('role_id',[$role,'0'] )->where('status','1')->get();
        $data2=UserNotificationSetting::where('user_id',Auth::user()->id)->get();
//        dd($data);
        $arr=array();
        foreach ($data2 as $data1){
            $arr[]=  $data1->notification_id;
        }
//        dd(Auth::user()->id);
//        $page_title = 'Dashboard';
//        $page_description = '';

        return view('pages.driver_ent.notification-setting', compact('data','arr'));
    }
    public function admin_notification_setting()
    {
        $role=Auth::user()->role_id;
        $data=Notification::wherein('role_id',[$role,'0'] )->where('status','1')->get();
        $data2=UserNotificationSetting::where('user_id',Auth::user()->id)->get();
//        dd($data);
        $arr=array();
        foreach ($data2 as $data1){
            $arr[]=  $data1->notification_id;
        }
//        dd(Auth::user()->id);
//        $page_title = 'Dashboard';
//        $page_description = '';

        return view('admin.pages.notification-setting', compact('data','arr'));
    }
    public function advertisor_notification_setting()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.notification-setting', compact('page_title', 'page_description'));
    }
    public function driver_notifications()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.notifications', compact('page_title', 'page_description'));
    }
    public function advertiser_notifications()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.driver_ent.notifications', compact('page_title', 'page_description'));
    }public function login_site()
    {
        $page_title = 'Dashboard';
        $page_description = '';

        return view('pages.site.login_home', compact('page_title', 'page_description'));
    }

    /**
     * Demo methods below
     */


        public function manage_campaign(Request $request){
            $page_title = 'Campaign';
            $page_description = 'Campaigns Management';
                        if(Auth::user()->role_id=='3'){
                            if(Auth::user()->enrolment == null){
                                return redirect('user/enroll-update')->with('message','Enrolled your self first');
                            }
                        }
              $check=0;
        $status=Status::orderby('id','desc')->where('name','!=','Approved')->get();
        $campaigns=CampaignCar::where('status_id','3')->where('payment_status','Paid')->without('car_make','car_type','car_name','car_model')->orderby('id','desc')->with('campaign','user','state','city','sticker','driver_amount')->get();
        $data=array();
        foreach($campaigns as $campaign) {
            $carzips = explode(",", $campaign->zip);
            $car_types = explode(",", $campaign->types);
            $year_range =explode("-", $campaign->year_range);
//            $year=MakeModel::find()->pluck('model_name')->first();
            $year=MakeModel::where('id',Auth::user()->enrolment->car_year)->first();
            $max_year=(int)$year_range[1];
            $min_year=(int)$year_range[0];

                foreach ($car_types as $type) {
                            if (Auth::user()->enrolment->car_type == $type) {
                                foreach ($carzips as $carzip) {
                                    if ($carzip == Auth::user()->enrolment->car_drive_zip) {
                                        for($vari=$min_year;$vari<=$max_year; $vari++){
                                            if(!is_null($year)) {
                                                if ($year->model_name == $vari) {
                                                    $campaign['stat'] = $campaign->state->state;
                                                    $campaign['city'] = $campaign->city->primary_city;
                                                    $campaign['zip'] = $carzip;
                                                    $campaign['working_status'] = AppliedCampaigns::where('user_id', Auth::id())->where('car_id', $campaign->id)->pluck('status')->first();
                                                    $data[] = $campaign;
                                                }
                                            }
                                }

                            }
                        }
                    }
                }
            }

        return view('pages.driver_ent.manage-campaign', compact('page_title', 'page_description','data','status','check'));
    }


    public function myCampaigns(Request $request){
            $page_title = 'Campaign';
            $page_description = 'Campaigns Management';
                if(Auth::user()->role_id=='3'){
                    if(Auth::user()->enrolment == null){
                        return redirect('user/enroll-update')->with('message','Enrolled your self first');
                    }

                }
        $check=0;
        $status=Status::orderby('id','desc')->where('name','!=','Approved')->get();
        $campaigns=AppliedCampaigns::where('user_id', Auth::id())->with('campaign.sticker','campaign.campaign')->orderBy('id','desc') ->get();
//        dd($campaigns);
        return view('pages.driver_ent.my-campaigns', compact('page_title', 'page_description','campaigns','status','check'));
    }





    public function manage_ent_campaign(Request $request){

        $page_title = 'Campaign';
        $page_description = 'Campaigns Management';
        $status=Status::orderby('id','desc')->where('name','!=','Approved')->get();
        $my_campaigns=DriverEnrollment::with('appliedcampaigns.campaign.campaign','appliedcampaigns.user')->where('created_by',Auth::user()->id)->orderBy('updated_at' , 'DESC')->get();
//dd($my_campaigns);
        return view('pages.driver_ent.manage-campaign-ent', compact('page_title', 'page_description','my_campaigns','status'));
    }

    public function drivers(Request $request)
    {
        $i=0;
        $campaign=array();
        $drivers=DriverEnrollment::where('created_by', Auth::user()->id)->with('country','state','city','user','car_company','car_yearr','car_namee','car_typee','appliedcampaigns.campaign.campaign')->orderBy('id', 'desc')->paginate(20);
        $campaign_cars=CampaignCar::whereColumn('assigned_drivers','<','no_of_cars')->with('campaign')->get();
        foreach($drivers as $driver) {
            foreach ($campaign_cars as $car) {
                if(@$car->status_id==3 && $car->payment_status=='Paid'){
                    $carzips = explode(",", $car->zip);
                    foreach ($carzips as $carzip) {
                        if ($carzip == $driver->car_drive_zip) {
                            $i++;
                            $campaign[]=$car;
                        }
                    }
                    }
                }
            $driver['total_available']= $i;
            $driver['campaigns']= $campaign;
            $i=0;
            $campaign=[];
        }
//        dd($drivers);

        $page_title = 'Driver';
        $page_description = 'Driver List';
//        dd( $drivers);

        return view('pages.driver_ent.drivers', compact('page_title', 'page_description','drivers'));
    }

    // KTDatatables
    public function ktDatatables()
    {
        $page_title = 'KTDatatables';
        $page_description = 'This is KTdatatables test page';

        return view('pages.ktdatatables', compact('page_title', 'page_description'));
    }

    // Select2
    public function select2()
    {
        $page_title = 'Select 2';
        $page_description = 'This is Select2 test page';

        return view('pages.select2', compact('page_title', 'page_description'));
    }

    // custom-icons
    public function customIcons()
    {
        $page_title = 'customIcons';
        $page_description = 'This is customIcons test page';

        return view('pages.icons.custom-icons', compact('page_title', 'page_description'));
    }

    // flaticon
    public function flaticon()
    {
        $page_title = 'flaticon';
        $page_description = 'This is flaticon test page';

        return view('pages.icons.flaticon', compact('page_title', 'page_description'));
    }

    // fontawesome
    public function fontawesome()
    {
        $page_title = 'fontawesome';
        $page_description = 'This is fontawesome test page';

        return view('pages.icons.fontawesome', compact('page_title', 'page_description'));
    }

    // lineawesome
    public function lineawesome()
    {
        $page_title = 'lineawesome';
        $page_description = 'This is lineawesome test page';

        return view('pages.icons.lineawesome', compact('page_title', 'page_description'));
    }

    // socicons
    public function socicons()
    {
        $page_title = 'socicons';
        $page_description = 'This is socicons test page';

        return view('pages.icons.socicons', compact('page_title', 'page_description'));
    }

    // svg
    public function svg()
    {
        $page_title = 'svg';
        $page_description = 'This is svg test page';

        return view('pages.icons.svg', compact('page_title', 'page_description'));
    }
    public function ajaxGetCountries()
    {
        $page_title = 'svg';
        $page_title = 'svg';
        $page_description = 'This is svg test page';
        $guard = new Country();
        $guard = $guard->getAll();
        return (
            [
                'responseCode' => 1 ,
                'message' => 'Guard Retrieved Successfully' ,
                'data'=>$guard
            ]
        );
    }

    // Quicksearch Result
     public function quickSearch()
    {
        return view('layout.partials.extras._quick_search_result');
    }

//Departments on admin dash
    public function addDepartments(){
            $departments=ProfileDepartment::all();
            return view('pages.admin.department',compact('departments'));
    }
    public function disableDepartment($id){
        $departments=ProfileDepartment::where('id',$id)->update([
            'status'=>'0'
        ]);
        return redirect()->back()->with('deleted','disabled successfully');
    }
    public function enableDepartment($id){
        $departments=ProfileDepartment::where('id',$id)->update([
            'status'=>'1'
        ]);
        return redirect()->back()->with('deleted','enabled successfully');
    }
    public function createDepartment(Request $request){
        $departments=ProfileDepartment::create([
            'name'=>$request['name'],
                    ]);
        return redirect()->back()->with('created','created successfully');
    }
//   END Department on admin dash


//    Industry on admin dash
    public function addIndustry(){
        $industries=Industry::all();
        return view('pages.admin.industry',compact('industries'));
    }
    public function createIndustry(Request $request){
        $type=Industry::where('name',$request['name'])->first();
        if($type){
            return redirect()->back()->with('error','Industry already exist');
        }
        $industry=Industry::create([
            'name'=>$request['name']
        ]);
        return redirect()->back()->with('created','created successfully');
    }
    public function disableIndustry($id){
        $industry=Industry::where('id',$id)->update([
            'status'=>'0'
        ]);
        return redirect()->back()->with('deleted','disabled successfully');
    }
    public function enableIndustry($id){
        $industry=Industry::where('id',$id)->update([
            'status'=>'1'
        ]);
        return redirect()->back()->with('deleted','enabled successfully');
    }
//   END Industry on admin dash


//    ticket types on admin dash
    public function tickettypes(){
        $industries=TicketType::paginate(10);
        return view('pages.admin.ticket-type',compact('industries'));
    }
    public function createTicketType(Request $request){
        $this->validate($request, [
            'namee' => ['required', 'string', 'unique:ticket_types,name'],
        ]);
       TicketType::create([
            'name'=>$request['namee']
        ]);
        return redirect()->back()->with('created','created successfully');
    }
    public function disableTicketType($id){
        $industry=TicketType::where('id',$id)->update([
            'status'=>'0'
        ]);
        return redirect()->back()->with('deleted','disabled successfully');
    }
    public function enableTicketType($id){
        $industry=TicketType::where('id',$id)->update([
            'status'=>'1'
        ]);
        return redirect()->back()->with('deleted','enabled successfully');
    }
//   END ticket type on admin dash




}
