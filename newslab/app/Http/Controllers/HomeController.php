<?php

namespace App\Http\Controllers;

use App\AppliedCampaigns;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function getAllAdvertisors(Request $request){
                if (!isset($request->search) && $request->email ) {
                    $users=User::where('role_id','2')->
                    where(function ($query) use ($request) {
                        $query->orwhere('name', 'LIKE', '%'.$request->search.'%');
                        $query->where('email', $request->email);
                    })->withCount('campaigns')->orderBy('campaigns_count', 'desc')->orderByDesc('created_at')->paginate(20);
                    return view('pages.admin.advertisors',compact('users'));
                }

                if ($request->search || $request->email ) {
                    $users=User::where('role_id','2')->
                    where(function ($query) use ($request) {
                        $query->orwhere('name', 'LIKE', '%'.$request->search.'%');
                        $query->orwhere('email', $request->email);
                    })->withCount('campaigns')->orderBy('campaigns_count', 'desc')->orderByDesc('created_at')->paginate(20);
                    return view('pages.admin.advertisors',compact('users'));
                }

        $users=User::where('role_id','2')->withCount('campaigns')->orderBy('campaigns_count', 'desc')->orderByDesc('created_at')->paginate(20);
//        dd($users[5]->campaigns_count);
        return view('pages.admin.advertisors',compact('users'));
    }
    public function getAllDrivers(Request $request){
                if (!isset($request->search) && $request->email ) {
                    $users=User::where('role_id','3')
                        ->with('enrolment.country','enrolment.car_company','enrolment.car_yearr','enrolment.car_namee','enrolment.car_typee')->withCount('appliedcampaigns')
                        ->where(function ($query) use ($request) {
                        $query->orwhere('name', 'LIKE', '%'.$request->search.'%');
                        $query->where('email', $request->email);
                    })->orderby('id', 'desc')->paginate(20);
                    return view('pages.admin.drivers',compact('users'));
                }
                if ($request->search || $request->email ) {
                    $users=User::where('role_id','3')->with('enrolment.country','enrolment.car_company','enrolment.car_yearr','enrolment.car_namee','enrolment.car_typee')->withCount('appliedcampaigns')
                    ->where(function ($query) use ($request) {
                        $query->orwhere('name', 'LIKE', '%'.$request->search.'%');
                        $query->orwhere('email', $request->email);
                    })->orderby('id', 'desc')->paginate(20);
                    return view('pages.admin.drivers',compact('users'));
                }

        $users=User::where('role_id','3')->with('enrolment.country','enrolment.car_company','enrolment.car_yearr','enrolment.car_namee','enrolment.car_typee')->withCount('appliedcampaigns')->orderBy('appliedcampaigns_count', 'desc')->orderByDesc('created_at')->paginate(20);
//        dd($users);
        return view('pages.admin.drivers',compact('users'));
    }
    public function getAllEntDrivers(Request $request){
                if (!isset($request->search) && $request->email ) {
                    $users=User::where('role_id','4')->with('companydrivers')->
                    where(function ($query) use ($request) {
                        $query->orwhere('name', 'LIKE', '%'.$request->search.'%');
                        $query->where('email', $request->email);
                    })->orderby('id', 'desc')->paginate(20);
                    return view('pages.admin.ent-drivers',compact('users'));
                }

                if ($request->search || $request->email ) {
                    $users=User::where('role_id','4')->with('companydrivers')->
                    where(function ($query) use ($request) {
                        $query->orwhere('name', 'LIKE', '%'.$request->search.'%');
                        $query->orwhere('email', $request->email);
                    })->orderby('id', 'desc')->paginate(20);
                    return view('pages.admin.ent-drivers',compact('users'));
                }

        $users=User::where('role_id','4')->with('companydrivers.appliedcampaigns')->orderByDesc('created_at')->paginate(20);
//        dd($users);
        return view('pages.admin.ent-drivers',compact('users'));
    }

    public function userStatusUpdate(Request $request){
        $update=User::where('id', $request->id)->update(['status' => $request->status]);
        if($update){
            return 1;
        }else{
            return 0;
        }
    }
    public function appliedCampaigns(Request $request){
        $data=AppliedCampaigns::where('user_id', $request->id)->with('campaign.campaign')->paginate(20);
        $user=User::where('id', $request->id)->first();
        return view('pages.admin.driver-campaigns',compact('data','user'));
//        dd($data);
    }
}
