<?php

namespace App\Http\Controllers;

use App\CampaignTypes;
use App\StickerTemplate;
use App\StickerTemplateLogoDetail;
use App\StickerTemplateType;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class StickerTemplatesController extends Controller
{
    public function index()
    {
        $templates = StickerTemplate::orderBy('id','desc')->with('typ','color')->paginate('15');
        $status = StickerTemplateType::where('status',1)->get();
        $cats = StickerTemplateType::orderBy('id','desc')->get();
        return view('pages.admin.templates', compact('templates', 'status','cats'));


    }

    public function createTemplate(Request $request){
        $this->validate($request, [
            'title' => "required",
            'type' => "required",
            'template' => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
        ]);
        $sticker= new StickerTemplate;
        if ($request->has('template')) {
            $sticker->width=getimagesize($request->template)[0];
            $sticker->height=getimagesize($request->template)[1];
            $sticker1 = Str::random(16) . '.' . $request->template->extension();
            $request->template->move(public_path('images/campaign-templates'), $sticker1);
            $sticker->url = $sticker1;
        }
        $sticker->name=$request->name;
        $sticker->title=$request->title;
        $sticker->type=$request->type;
        $sticker->status=$request->status;
        $sticker->description=$request->description;
        if ( $sticker->save()) {
            return redirect()->back()->with('success', 'Template created successfully');
        }
    }    public function updateTemplate(Request $request){
        $this->validate($request, [
            'title' => "required",
            'type' => "required",
            'background_colour' => "required",
            'font_colour' => "required",
            'contact_detail' => "required",
            'template' => "image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            'company_logo' => "image|mimes:jpeg,png,jpg,gif,svg|max:2048",
        ]);
        $sticker= StickerTemplate::find($request->template_id);
        if ($request->has('template')) {
            @$path = public_path()."/images/campaign-templates/".$sticker->url;
            @unlink($path);
            $sticker->width=getimagesize($request->template)[0];
            $sticker->height=getimagesize($request->template)[1];
            $sticker1 = Str::random(16) . '.' . $request->template->extension();
            $request->template->move(public_path('/images/campaign-templates/'), $sticker1);
            $sticker->url = $sticker1;
        }
        $sticker->name=$request->name;
        $sticker->title=$request->title;
        $sticker->type=$request->type;
        $sticker->status=$request->status;
        $sticker->description=$request->description;
        $sticker->save();

    $sticker= StickerTemplateLogoDetail::where('template_id',$request->template_id)->first();
    if ($request->has('company_logo')) {
        @$path = public_path()."/images/campaign-templates/logo/".$sticker->company_logo;
        @unlink($path);

        $sticker1 = Str::random(16) . '.' . $request->company_logo->extension();
        $request->company_logo->move(public_path('/images/campaign-templates/logo/'), $sticker1);
        $sticker->company_logo = $sticker1;

    }
    $sticker->background_colour=$request->background_colour;
    $sticker->font_colour=$request->font_colour;
    $sticker->contact_detail=$request->contact_detail;
    $sticker->status=$request->status1;
    $sticker->side_text=$request->side_text;
    if ($sticker->save()) {
        return redirect()->back()->with('success', 'Template updated successfully');
    }
    }
    public function createTemplateColor(Request $request){
        $this->validate($request, [
            'background_colour' => "required",
            'font_colour' => "required",
            'contact_detail' => "required",
            'company_logo' => "required|image|mimes:jpeg,png,jpg,gif,svg|max:2048",
        ]);
        $sticker= new StickerTemplateLogoDetail;
        if ($request->has('company_logo')) {
            $sticker1 = Str::random(16) . '.' . $request->company_logo->extension();
            $request->company_logo->move(public_path('/images/campaign-templates/logo/'), $sticker1);
            $sticker->company_logo = $sticker1;
        }
        $sticker->background_colour=$request->background_colour;
        $sticker->font_colour=$request->font_colour;
        $sticker->contact_detail=$request->contact_detail;
        $sticker->status=$request->status;
        $sticker->side_text=$request->side_text;
        $sticker->template_id=$request->template_id;
        if ( $sticker->save()) {
            return redirect()->back()->with('success', 'Data created successfully');
        }
    }
    public function createType(Request $request){
        $this->validate($request, [
            'title' => "required|unique:sticker_template_types",
        ]);
//        dd(getimagesize($request->template)[0]);
        $sticker= new StickerTemplateType;
        $sticker->title=$request->title;
        $sticker->status=$request->type;


        if ( $sticker->save()) {
            return redirect()->back()->with('success', 'Category created successfully');
        }
    }
    public function stickerUpdateType(Request $request){
        $this->validate($request, [
            'title' =>'required|max:25|unique:sticker_template_types,title,'.$request->cat_id,

        ]);
        $sticker= StickerTemplateType::find($request->cat_id);
        $sticker->title=$request->title;
        $sticker->status=$request->type;

        if ( $sticker->save()) {
            return redirect()->back()->with('success', 'Category updated successfully');
        }
    }

}
