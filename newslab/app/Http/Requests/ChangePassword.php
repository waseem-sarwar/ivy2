<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangePassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'passsword' => 'required',
                'pwd_new' => 'required|min:4|max:12|different:passsword',
                'pwd_confirm' => 'required_with:pwd_new|max:12|same:pwd_new|min:4'

        ];
    }

    public function messages()
    {
        return [
            'passsword.required' => 'Password is required',
            'pwd_new.different' => 'New and old password must be different',
            'pwd_confirm.same' => 'New and retyped password must be same'

        ];
    }

}
