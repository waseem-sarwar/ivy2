<?php

namespace App\Http\Requests;

use App\UserProfile;
use Illuminate\Foundation\Http\FormRequest;

class BussinessProfile extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = \Auth::user()->profile->id;
        return [
            'company_name' => 'required|max:25|unique:user_profiles,company_name,'.$id,
            'industry' => 'required|max:25',
            'registration_no' => 'required|max:22|unique:user_profiles,registration_no,'.$id,
            'type_of_company' => 'required|max:22',
            'company_size' => 'required|max:22',
             ];
    }
    public function messages()
    {
        return [
            'company_name.required' => 'Company name is required',
            'industry.required' => 'Industry is required',
            'type_of_company.required' => 'Department is required',
            'registration_no.required' => 'Registration number is required',
            'company_size.required' => 'Company size is required',
            'registration_no.unique' => 'This company registration number is already registered in our system',
            'company_name.unique' => 'This company is already registered in our system',

        ];
    }
}
