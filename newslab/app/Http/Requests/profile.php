<?php

namespace App\Http\Requests;

use App\UserProfile;
use Illuminate\Foundation\Http\FormRequest;

class profile extends FormRequest
{

    public function __construct()
    {
//        dd($id);
//        $this->id=$request;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = \Auth::user()->profile->id;
        return [
            'fname' => 'required|max:25',
            'lname' => 'required|max:25',
            'state_id' => 'required|max:22',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'contact_no' => 'required|min:7|max:22|unique:user_profiles,contact_no,'.$id,
            'dob' => 'date|before:-18 years'
             ];
    }
    public function messages()
    {
        return [
            'dob.required' => 'Date of birth is required',
            'dob.before' => 'Date of birth must be greater than 18',
            'lname.required' => 'Last name is required',
            'fname.required' => 'First name is required',
            'contact_no.digits_between' => 'Contact number must be between 7 to 20 digits',
            'contact_no.integer' => 'Contact number must be numeric digits',
            'contact_no.required' => 'Contact number is required',
            'contact_no.unique' => 'This phone number is already registered in our system'

        ];
    }
}
