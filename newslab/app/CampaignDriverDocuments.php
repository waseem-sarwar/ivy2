<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignDriverDocuments extends Model
{
    protected $table='campaign_driver_documents';

//    public function driverDetail()
//    {
//        return $this->belongsTo('App\User','user_id','id');
//    }


    protected $casts = [
        'created_at' => 'datetime:Y-m-d h:i:s',
        'updated_at' => 'datetime:Y-m-d h:i:s',
        'campain_start_date' => 'Y-m-d H:i'
    ];


}
