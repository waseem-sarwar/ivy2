<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignSticker extends Model
{
   protected $table='campaign_stickers';
    protected $guarded = ['id'];
}
