<?php
use Illuminate\Support\Facades\Auth;


function notificationCount()
{
    $user_id =Auth::user()->id;
//dd($user_id );
    $count=\App\UserNotification::where('user_id',$user_id)->where('is_read','0')->count();
    return  $count;
}

function systemAlerts()
{
    $user_id =Auth::user()->id;
    $data=\App\UserNotification::where('user_id',$user_id)->where('is_read','0')->orderBy('id', 'desc')->take(5)->get();
    return  $data;
}

function jobAlerts()
{
    $user_id =Auth::user()->id;
    $role_id =Auth::user()->role_id;
    $notification_ids=\App\Notification::where('role_id',$role_id)->wherein('field_name',['approved_campaign','campaign_approved_by_admin','campaign_request_approved'])->get();
    $array=array();
    foreach($notification_ids as  $notification){
        $array[]=$notification->id;

    }
//        dd($notification_ids);
    $data=0;
    if($notification_ids) {
        $data = \App\UserNotification::where('user_id', $user_id)->where('is_read', '0')->wherein('notification_type_id',$array)->orderBy('id', 'desc')->get();
        if(count($data)==0){
            $data=0;
        }
    }
    return  $data;
}

function allNotifications()
{
    $user_id =Auth::user()->id;
    $data=\App\UserNotification::where('user_id',$user_id)->where('is_read','0')->orderBy('id', 'desc')->get();
    return  $data;
}
function paymentCount()
{
    $user_id =Auth::user()->id;
    $data=\App\DriverPayment::where('user_id',$user_id)->count();
    return  $data;
}
function driverTotal()
{
    $user_id =Auth::user()->id;
    $data=\App\DriverPayment::where('user_id',$user_id)->sum('amount');
    return  number_format($data,2);
}



