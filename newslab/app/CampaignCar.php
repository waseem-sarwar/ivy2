<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CampaignCar extends Model
{
    protected $table ='campaign_cars';

//    protected $with = ['car_location_data','car_make','car_model','car_name','car_type'];
    protected $with = ['car_make','car_model','car_name','car_type'];
//    protected $primaryKey = 'slug';

    public function car_location_data()
    {
        return $this->belongsTo('App\CampaignLocation','car_location','id');
    }
    public function campaign()
    {
        return $this->belongsTo('App\Campaign','campaign_id','id');
    }
    public function status()
    {
        return $this->belongsTo('App\Status','status_id','id');
    }

//    public function state()
//    {
//        return $this->belongsTo('App\CampaignLocation','car_location','id');
//    }
    public function car_make()
    {
        return $this->belongsTo('App\CarCompany','make','id');
    }
    public function car_model()
    {
        return $this->belongsTo('App\MakeModel','year','id');
    }
    public function car_name(){
        return $this->belongsTo(CarName::class,'model','id');
    }
    public function car_type(){
        return $this->belongsTo(CarType::class,'type','id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function state()
    {
        return $this->belongsTo('App\State','state_id','id');
    }
    public function city()
    {
        return $this->belongsTo('App\City','city_id','id');
    }
    public function sticker()
    {
        return $this->hasOne('App\CampaignSticker','campaign_id','campaign_id');
    }
    public function appliedDrivers()
    {
        return $this->hasMany('App\AppliedCampaigns','car_id','id');
    }
    public function payment(){
        return $this->hasOne('App\AdvertisorPayment','campaign_id','id')->where('type','Paid');
    }
    public function refund_amount()
    {
        return $this->hasMany('App\AdvertisorPayment','campaign_id')->where("type","Refundable");
    }
    public function drivers_cost()
    {
        return $this->hasMany('App\DriverPayment','car_id');
    }
    public function driver_amount()
    {
        return $this->hasOne('App\DriverPayment','car_id')->where('user_id', Auth::user()->id);
    }
    public function sticker_payment()
    {
        return $this->hasOne('App\StickerPayment','campaign_id','campaign_id');
    }



}
