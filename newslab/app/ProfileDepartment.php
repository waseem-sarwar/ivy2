<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileDepartment extends Model
{
    protected $table='profile_departments';
    protected $fillable=['name','status'];
}
