<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StickerTemplateType extends Model
{
    public function templates(){
      return $this->hasMany('App\StickerTemplate','type','id');
}
}
