<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id'
    ];
    protected $with = ['profile.country',];
//    protected $with = ['profile.country','enrolment'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function profile()
    {
        return $this->hasOne('App\UserProfile');
    }

    public function supportNotificationOnUsers()
    {
        return $this->hasOne('App\UserNotificationSetting')->where('notification_id','29');
    }
    public function supportEmailOnUsers()
    {
        return $this->hasOne('App\UserNotificationSetting')->where('notification_id','34');
    }


    public function messages()
    {
        return $this->hasMany(Message::class);
    }
    public function appliedcampaigns(){
        return $this->hasMany(AppliedCampaigns::class);
    }
    public function campaigns_count(){
        return $this->hasMany(AppliedCampaigns::class)->count();
    }
    public function campaigns(){
        return $this->hasMany(Campaign::class);
    }

    public function payments(){
        return $this->hasMany(DriverPayment::class);
    }
    public function companydrivers(){
        return $this->hasMany(DriverEnrollment::class,'created_by','id');
    }

    public function enrolment(){
        return $this->hasOne(DriverEnrollment::class,'user_id','id');
    }
    public function role(){
        return $this->belongsTo(Role::class, 'role_id','id');
    }
    public function campaignscount()
    {
        return $this->hasMany('App\AppliedCampaigns','user_id','id')->where('status','Assigned');
    }

}
