<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MakeModel extends Model
{
    protected $with=['CarName'];
    protected $table='create_car_model';
    protected $fillable=['company_id','car_name_id','model_name','status'];

    public function CarName(){
        return $this->hasOne(CarName::class,'id','car_name_id');
    }
}
