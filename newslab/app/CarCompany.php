<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarCompany extends Model
{
    protected $table='car_companies';
    protected $fillable=['company'];

    public function car_names(){
        return $this->hasMany(CarName::class,'company_id','id');
}

}
