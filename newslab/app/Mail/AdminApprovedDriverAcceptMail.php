<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminApprovedDriverAcceptMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$camp_id,$slug)
    {
        $this->name =$name ;
        $this->camp_id = $camp_id;
        $this->slug = $slug;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

//        dd();
//        dd($this->from(env('MAIL_FROM_ADDRESS'))
//            ->to('nomangul934@gmail.com')
//            ->subject('New Campaign Available')
//            ->view('mails.campaign_available')
//            ->with(
//                [
//                    'testVarOne' => '1',
//                    'testVarTwo' => '2',
//                ]
//            ));
        return  $this->from('no-reply@slabcar.com','Slab Car')
            ->subject('Request Accepted')
            ->view('mails.request_approved_for_campaign_driver')->with(['name' => $this->name,'camp_id' => $this->camp_id,'slug' => $this->slug]);
//         dd($this);
//        return $this;
    }
}
