<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampaignAssignedDriverMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($driver_email,$driver_name,$camp_id)
    {
        $this->driver_name =$driver_name ;
        $this->camp_id =$camp_id ;
        $this->driver_email =$driver_email ;
    }
    public function build()
    {

//        dd();
//        dd($this->from(env('MAIL_FROM_ADDRESS'))
//            ->to('nomangul934@gmail.com')
//            ->subject('New Campaign Available')
//            ->view('mails.campaign_available')
//            ->with(
//                [
//                    'testVarOne' => '1',
//                    'testVarTwo' => '2',
//                ]
//            ));
        return  $this->from('no-reply@slabcar.com','Slab Car')
            ->to($this->driver_email)
            ->subject('Campaign Assigned')
            ->view('mails.document_campaign_assigned_driver')->with(['name' => $this->driver_name,'camp_id' => $this->camp_id]);
//         dd($this);
//        return $this;
    }
}
