<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampaignCourierDriverMail extends Mailable
{
use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$camp_id,$mail_address)
    {
        $this->name = $name;
        $this->email = $email;
        $this->camp_id = $camp_id;
        $this->mail_address = $mail_address;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
         return  $this->from('no-reply@slabcar.com','Slab Car')
            ->to($this->email)
             ->subject('Courier address confirmation request')
            ->view('mails.campaign_courier_mail')->with(['name' => $this->name,'email' => $this->email,'camp_id' => $this->camp_id,'mail_address'=>$this->mail_address]);
           ;
//         dd($this);
//        return $this;
    }
}
