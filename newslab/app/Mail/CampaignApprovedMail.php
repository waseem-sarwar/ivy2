<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampaignApprovedMail extends Mailable
{
use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$email,$camp_id)
    {
        $this->name = $name;
        $this->email = $email;
        $this->camp_id = $camp_id;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

//        dd();
//        dd($this->from(env('MAIL_FROM_ADDRESS'))
//            ->to('nomangul934@gmail.com')
//            ->subject('New Campaign Available')
//            ->view('mails.campaign_available')
//            ->with(
//                [
//                    'testVarOne' => '1',
//                    'testVarTwo' => '2',
//                ]
//            ));
         return  $this->from('no-reply@slabcar.com','Slab Car')
            ->to($this->email)
            ->subject('Campaign Approved')
            ->view('mails.campaign_approved')->with(['name' => $this->name,'email' => $this->email,'camp_id' => $this->camp_id]);
           ;
//         dd($this);
//        return $this;
    }
}
