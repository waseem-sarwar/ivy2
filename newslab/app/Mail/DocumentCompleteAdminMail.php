<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

class DocumentCompleteAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin_name,$driver_name,$camp_id)
    {
        $this->admin_name =$admin_name;
        $this->driver_name =$driver_name ;
        $this->camp_id =$camp_id;
    }
    public function build()
    {

//        dd();
//        dd($this->from(env('MAIL_FROM_ADDRESS'))
//            ->to('nomangul934@gmail.com')
//            ->subject('New Campaign Available')
//            ->view('mails.campaign_available')
//            ->with(
//                [
//                    'testVarOne' => '1',
//                    'testVarTwo' => '2',
//                ]
//            ));
        return  $this->from(Auth::user()->email,$this->driver_name)
            ->subject('Campaign CMP-'.$this->camp_id.' completed by driver '.$this->driver_name)
            ->view('mails.document_complete_admin')->with(['driver_name' => $this->driver_name,'admin' => $this->admin_name,'camp_id' => $this->camp_id]);
//         dd($this);
//        return $this;
    }
}
