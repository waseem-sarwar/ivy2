<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourierDriverMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($driver_name,$driver_address,$camp_id)
    {
        $this->driver_name =$driver_name ;
        $this->driver_address = $driver_address;
        $this->camp_id = $camp_id;
    }
    public function build()
    {
        return  $this->from('no-reply@slabcar.com','Slab Car')
            ->subject('Campaign Documents Sent')
            ->view('mails.courier_mail_driver')->with(['driver_name' => $this->driver_name,'driver_address' => $this->driver_address,'camp_id' => $this->camp_id]);
//         dd($this);
//        return $this;
    }
}
