<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CampaignAvailableMail extends Mailable
{
use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
//        dd($this->details['receiver_email']);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

//        dd();
//        dd($this->from(env('MAIL_FROM_ADDRESS'))
//            ->to('nomangul934@gmail.com')
//            ->subject('New Campaign Available')
//            ->view('mails.campaign_available')
//            ->with(
//                [
//                    'testVarOne' => '1',
//                    'testVarTwo' => '2',
//                ]
//            ));
        return  $this->from('no-reply@slabcar.com','Slab Car')
            ->to( $this->details['receiver_email'])
            ->subject('New Campaign Available')
            ->view('mails.campaign_available')
           ;
//         dd($this);
//        return $this;
    }
}
