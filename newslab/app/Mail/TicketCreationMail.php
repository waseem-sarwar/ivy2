<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TicketCreationMail extends Mailable
{
use Queueable, SerializesModels;

    /**
     * TSS
     * DFDFhe demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($details)
    {
        $this->details = $details;
    }

    /**
     * BUild the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->details['from-email'])
            ->subject('New support Ticket is created')
            ->view('mails.ticket_created')
           ;
    }
}
