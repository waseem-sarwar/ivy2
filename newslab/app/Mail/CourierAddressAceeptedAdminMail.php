<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourierAddressAceeptedAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin_email,$admin_name,$camp_id,$driver_name,$driver_email)
    {
        $this->admin_email =$admin_email;
        $this->camp_id =$camp_id ;
        $this->admin_name =$admin_name;
        $this->driver_name =$driver_name ;
        $this->driver_email =$driver_email ;
    }
    public function build()
    {

//        dd();
//        dd($this->from(env('MAIL_FROM_ADDRESS'))
//            ->to('nomangul934@gmail.com')
//            ->subject('New Campaign Available')
//            ->view('mails.campaign_available')
//            ->with(
//                [
//                    'testVarOne' => '1',
//                    'testVarTwo' => '2',
//                ]
//            ));
        return  $this->from($this->driver_email, $this->driver_name)
            ->to($this->admin_email)
//            ->subject('Courier acknowledged by driver '. $this->driver_name)
            ->subject('Courier address confirmed by driver '. $this->driver_name)
            ->view('mails.courier_campaign_admin')->with(['name' => $this->driver_name,'camp_id' => $this->camp_id,'admin'=>$this->admin_name,]);
//         dd($this);
//        return $this;
    }
}
