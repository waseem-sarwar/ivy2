<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CourierRejectedAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin_email, $admin_name, $camp_id, $driver_name, $driver_email, $comment)
    {
        $this->admin_email =$admin_email;
        $this->camp_id =$camp_id ;
        $this->admin_name =$admin_name;
        $this->driver_name =$driver_name ;
        $this->driver_email =$driver_email ;
        $this->comment =$comment ;
    }
    public function build()
    {
        return  $this->from($this->driver_email, $this->driver_name)
            ->to($this->admin_email)
            ->subject('Courier rejected by driver '. $this->driver_name)
            ->view('mails.courier_campaign_rejected_admin')->with(['name' => $this->driver_name,'camp_id' => $this->camp_id,'admin'=>$this->admin_name,'comment'=>$this->comment]);
//         dd($this);
//        return $this;
    }
}
