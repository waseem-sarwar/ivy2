<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RequstForApprovalAdminMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The demo object instance.
     *
     * @var Demo
     */
    public $details;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($adminame,$applier_name,$slug,$to,$camp_id)
    {
        $this->admin_name =$adminame ;
        $this->applier_name = $applier_name;
        $this->camp_id = $camp_id;
        $this->admin_email = $to;
        $this->slug = $slug;
    }

    public function build()
    {

//        dd();
//        dd($this->from(env('MAIL_FROM_ADDRESS'))
//            ->to('nomangul934@gmail.com')
//            ->subject('New Campaign Available')
//            ->view('mails.campaign_available')
//            ->with(
//                [
//                    'testVarOne' => '1',
//                    'testVarTwo' => '2',
//                ]
//            ));
        return  $this->from('no-reply@slabcar.com','Slab Car')
            ->subject('New Driver Applied')
            ->view('mails.request_for_campaign_approved')->with(['admin_name' => $this->admin_name,'applier_name' => $this->applier_name,'camp_id' => $this->camp_id,'url' => $this->slug]);
//         dd($this);
//        return $this;
    }
}
