<?php

namespace App\Imports;

use App\UserBank;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
class PaymentImport implements ToModel,WithHeadingRow
{

    public function model(array $row)
    {
        return new UserBank([
            'expiry'     => $row['expiry'],
            'email_address'    => $row['email_address'],
            'card_number'    => $row['card_number']
        ]);
    }
}
