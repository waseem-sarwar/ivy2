<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotification extends Model
{
   protected $table='notifications';

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    function notification()
    {
        return $this->belongsTo('App\Notification','notification_type_id','id');
    }


}
