<?php

namespace App\Exports;

use App\CampaignCar;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class CampaignExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithTitle
{
    public function __construct($status)
    {

        $this->status=$status;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
//        dd($this->state_id);

        if($this->status!=''){
            $prices=CampaignCar::with('campaign','status','state','city')->where('status_id',$this->status)->orderBy('id','desc')->get();
        }
        else{
            $prices=CampaignCar::with('campaign','status','state','city')->orderBy('id','desc')->get();
        }


        return $prices;
    }

    public function map($prices): array
    {
//dd($this->high_percentage);
        return [
            'CMP-'.$prices->id,
            $prices->user->name,
            $prices->user->email,
            $prices->campaign->title,
            $prices->state->state,
            $prices->city->primary_city,
            $prices->zip,
            $prices->campaign->campaign_total_days,
            $prices->campaign->interval,
            $prices->status->name,
            $prices->payment_status,
            $prices->no_of_cars,
            ($prices->completed_drivers==null)? '0':$prices->completed_drivers,
            $prices->mileage .' Miles',
            '$ '.$prices->price_per_mile,
            '$ '.$prices->total_price,
        ];
    }
    public function headings(): array
    {
        return [
            '#',
            'Advertisor',
            'Email',
            'Title',
            'State',
            'City',
            'Zip',
            'Duration',
            'Period',
            'Status',
            'Payment status',
            'Drivers Required',
            'Completed Drivers',
            'Miles',
            'Price per Mile',
            'Price',

        ];
    }

    public function title(): string
    {
        return 'Campaigns';
    }


}
