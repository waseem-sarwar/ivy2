<?php

namespace App\Exports;
use App\AdvertisorPayment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
class AdvertisorPaymentnExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithTitle{
    public function __construct($status)
    {

        $this->status=$status;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection(){
        if($this->status!=''){
            $prices = AdvertisorPayment::with('user')->where('status',$this->status)->orderBy('id','desc')->get();
        }
        else{
            $prices = AdvertisorPayment::with('user')->orderBy('id','desc')->get();
        }
        return $prices;
    }

    public function map($prices): array
    {
//dd($this->high_percentage);

        return [
            $prices->id,
            $prices->user->name,
            $prices->user->email,
            'CMP-'.$prices->campaign_id,
            '$'.$prices->amount,
            $prices->company_name,
            'Stripe',
            $prices->status,
            $prices->created_at->format('m-d-Y'),

        ];
    }
    public function headings(): array
    {
        return [
            '#',
            'Advertisor',
            'Advertisor Email',
            'Campaign',
            'Amount',
            'Company',
            'Medium',
            'Status',
            'Payment date',
        ];
    }

    public function title(): string
    {
        return 'Advertisor Payment Export';
    }


}
