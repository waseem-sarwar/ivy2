<?php

namespace App\Exports;
use App\AdvertisorPayment;
use App\DriverPayment;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
class DriverPaymentExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize, WithTitle{
    public function __construct($status)
    {

        $this->status=$status;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        if($this->status!=''){
            $prices = DriverPayment::with('user')->where('user_id',$this->status)->orderBy('id','desc')->get();
        }
        else{
            $prices = DriverPayment::with('user')->orderBy('id','desc')->get();
        }
        return $prices;
    }

    public function map($prices): array
    {
//dd($this->high_percentage);

        return [
            $prices->id,
            $prices->user->name,
            $prices->user->email,
            'CMP-'.$prices->car_id,
            '$'.$prices->amount,
            $prices->created_at->format('m-d-Y'),

        ];
    }
    public function headings(): array
    {
        return [
            '#',
            'Driver',
            'Email',
            'Campaign',
            'Amount',
            'Date',

        ];
    }

    public function title(): string
    {
        return 'Driver Payment Export';
    }


}
