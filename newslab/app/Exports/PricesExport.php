<?php

namespace App\Exports;

use App\PriceRange;
use App\SystemSetting;
use App\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PricesExport implements FromCollection, WithHeadings, WithMapping
{
    public function __construct($high_percentage,$state_id)
    {

        $this->high_percentage=$high_percentage;
        $this->state_id=$state_id;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
//        dd($this->state_id);

        if($this->state_id!='null'){
            $prices=PriceRange::where('state_id',$this->state_id)->with('state')->get();
        }
        else{
            $prices=PriceRange::with('state')->get();
        }


        return $prices;
    }

    public function map($prices): array
    {
//dd($this->high_percentage);

        return [
            $prices->State->id,
            $prices->state->state,
            ($prices->min_range=='0')? '0':$prices->min_range,
            $prices->max_range,
            $prices->price,
            ($prices->is_set_high=='1')? 'Yes':'-',
            ($prices->is_set_high)? $prices->price* $this->high_percentage:'-',
        ];
    }

    public function headings(): array
    {
        return [
            'State Id',
            'Name',
            'From',
            'To',
            'Price',
            'High Price status',
            'High Price'

        ];
    }


}
