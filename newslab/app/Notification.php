<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
   protected $table='notifications_listing';

    public function role()
    {
        return $this->belongsTo('App\Role','role_id','id');
    }

    public function setFieldNameAttribute(){
        $this->attributes['field_name'] = \Str::slug($this->name , "_");
    }
}
