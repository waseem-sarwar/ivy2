<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserNotificationSetting extends Model
{
    protected $table='user_notification_settings';
}
