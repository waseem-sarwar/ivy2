<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarModels extends Model
{
    protected $with=['maker','model','carName','carType'];
    protected $table='car_models';
    protected $fillable=['company_id','car_name_id','car_type','car_model_id','status'];

    public function maker(){
        return $this->hasOne(CarCompany::class,'id','company_id');
    }
    public function model(){
        return $this->hasOne(MakeModel::class,'id','car_model_id');
    }
    public function carName(){
        return $this->hasOne(CarName::class,'id','car_name_id');
    }

    public function carType(){
        return $this->hasOne(CarType::class,'id','car_type');
    }

}
