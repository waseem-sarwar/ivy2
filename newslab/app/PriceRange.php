<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceRange extends Model
{
    protected $table='price_ranges';

    public function state()
    {
        return $this->belongsTo('App\State','state_id','id');
    }
}
