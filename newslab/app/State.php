<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $table='state';

   public function priceRanges()
{
    return  $this->hasMany('App\PriceRange','state_id','id');
}

}
