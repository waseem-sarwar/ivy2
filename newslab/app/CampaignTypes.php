<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignTypes extends Model
{
    protected $table ='campaign_types';
     protected $fillable=['name','status'];
    public function type(){
        return $this->belongsTo(Campaign::class,'type','name');
    }
}
