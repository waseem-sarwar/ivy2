<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketThread extends Model
{
    protected $table ='ticket_threads';


    public function fromUser()
    {
        return  $this->belongsTo('App\User','from_id','id');
    }
    public function toUser()
    {
        return  $this->belongsTo('App\User','to_id','id');
    }
}
