<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AppliedCampaigns extends Model
{
    protected $table='applied_campaigns';
    protected $with = ['driverDetails'];
    protected $guarded=[''];
    public function driverDetail()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function driverDetails()
    {
        return $this->belongsTo('App\UserProfile','user_id','id');
    }


    public function users(){
        return $this->belongsToMany(User::class,'user_id','id');
    }

//    public function AssignedCampaign(){
//    return $this->hasOne(Campaign::class,'id','campaign_id');
//    }
    public function campaign(){
        return $this->belongsTo(CampaignCar::class,'car_id','id');
    }

    public function courier(){
        return $this->belongsTo(CourierDetail::class,'id','applied_campaigns_id');
    }
    public function location(){
        return $this->belongsTo(CampaignLocation::class,'camp_location_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
//    public function payment(){
//        return $this->belongsTo(DriverPayment::class,'car_id','car_id');
//    }




}
