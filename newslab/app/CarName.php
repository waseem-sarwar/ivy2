<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarName extends Model
{
    protected $with=['Company'];
    protected  $table='car_name';
    protected $fillable=['company_id','car_name','status'];
    public function Company(){
        return $this->belongsTo(CarCompany::class,'company_id','id');
    }
}
