<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profiles';
    protected $guarded=['id'];
    public function country()
    {
        return $this->belongsTo('App\Country');
    }


}
