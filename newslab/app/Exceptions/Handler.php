<?php

namespace App\Exceptions;

use App\Http\Middleware\VerifyCsrfToken;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception){
        if ($exception instanceof \Illuminate\Session\TokenMismatchException) {
            return redirect()->back()->with('error', 'Please try again');
        }
        if ($exception instanceof \Illuminate\Routing\Exceptions\InvalidSignatureException) {
            return redirect('/')->with('error', 'Link Expire');
        }
//        if ($exception instanceof \Illuminate\Validation\ValidationException) {
//            dd($exception);
//            return redirect('/')->with('error', ' Please send email again');
//        }
        return parent::render($request, $exception);

    }
}
