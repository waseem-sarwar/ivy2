<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StickerTemplate extends Model
{
    public function typ()
    {
        return $this->belongsTo('App\StickerTemplateType','type','id');
    }
    public function color()
    {
        return $this->hasOne('App\StickerTemplateLogoDetail','template_id','id');
    }
    public function activeColor()
    {
        return $this->hasOne('App\StickerTemplateLogoDetail','template_id','id')->where('status','1');
    }
}
