<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdvertisorPayment extends Model
{
    public function campaign(){
        return $this->belongsTo('App\CampaignCar','campaign_id','id');
    }
    public function user(){
        return $this->belongsTo('App\User','user_id','id');
    }
}
