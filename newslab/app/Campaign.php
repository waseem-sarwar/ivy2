<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table ='campaigns';
    protected $with = ['typee'];
//    protected $with = ['status','user','cars','locations','type'];


    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
    public function cars()
    {
        return $this->hasMany('App\CampaignCar','campaign_id','id');
    }
    public function locations()
    {
        return $this->hasMany('App\CampaignLocation','campaign_id','id');
    }

    public function camp_location()
    {
        return $this->hasOne('App\CampaignLocation','campaign_id','id');
    }
    public function sticker()
    {
        return $this->hasOne('App\CampaignSticker','campaign_id','id');
    }
    public function appliedDrivers()
    {
        return $this->hasMany('App\AppliedCampaigns','campaign_id','id');
    }
    public function campaign(){
        return $this->belongsTo('App\AppliedCampaigns','user_id','user_id');
    }
    public function status_count()
    {
        return $this->belongsTo('App\Status','status_id','id')->where('name','pending');
    }
    public function typee(){
        return $this->belongsTo(CampaignTypes::class,'type','id');
    }

    public function payment(){
        return $this->hasOne('App\AdvertisorPayment','campaign_id','id');
    }

}
