<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampaignLocation extends Model
{
    protected $table ='campaign_locations';



    protected $with = ['state','city','zips'];

    public function state()
    {
        return $this->belongsTo('App\State','state_id','id');
    }
    public function city()
    {
        return $this->belongsTo('App\City','city_id','id');
    }
    public function zips()
    {
        return $this->hasMany('App\CampaignZip','camp_location_id','id');
    }
    public function drivers()
    {
        return $this->hasMany('App\DriverEnrollment','driving_state','state_id');
    }



}
