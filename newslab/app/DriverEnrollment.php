<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpKernel\Profiler\Profile;

class DriverEnrollment extends Model
{
    protected $with = ['state'];

   protected $table="driver_enrollments";

public function appliedcampaigns(){
    return $this->hasMany(AppliedCampaigns::class,'user_id','user_id');
}


public function country(){
    return $this->belongsTo(Country::class,'car_drive_country','id');
}
public function state(){
    return $this->belongsTo(State::class,'driving_state','id');
}

public function user(){
    return $this->belongsTo(User::class,'user_id','id');
}


public function city(){
    return $this->belongsTo(City::class,'car_drive_city','id');
}
    public function car_yearr()
    {
        return $this->belongsTo('App\MakeModel','car_year','id');
    }
    public function car_namee(){
        return $this->belongsTo(CarName::class,'car_model','id');
    }
    public function car_typee(){
        return $this->belongsTo(CarType::class,'car_type','id');
    }
    public function car_company(){
        return $this->belongsTo(CarCompany::class,'car_make','id');
    }
    public function payments(){
        return $this->hasMany(DriverPayment::class,'user_id','user_id');
    }




}
