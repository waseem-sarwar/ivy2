{{--@extends('user.layout.auth')--}}

{{--<!-- Main Content -->--}}
{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-8 col-md-offset-2">--}}
{{--            <div class="panel panel-default">--}}
{{--                <div class="panel-heading">Reset Password</div>--}}
{{--                <div class="panel-body">--}}
{{--                    @if (session('status'))--}}
{{--                        <div class="alert alert-success">--}}
{{--                            {{ session('status') }}--}}
{{--                        </div>--}}
{{--                    @endif--}}

{{--                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/user/password/email') }}">--}}
{{--                        {{ csrf_field() }}--}}

{{--                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">--}}

{{--                                @if ($errors->has('email'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('email') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="col-md-6 col-md-offset-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    Send Password Reset Link--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}



@extends('pages.site.layout.login')
@section('content')
    <main class="main">
        <section class="enquirySection">

            <h1>Forgot Password?</h1>
            <p>We’ll help you reset it and get back on track.</p>
            <form method="POST" action="{{ url('/user/password/email')}}">
                @csrf
                @if (session('status'))
                    <div class="form-group alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="form-group">
                    <input  id="email" type="email"  placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <i class="flaticon-user icon"></i>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <div class="formFlex form-group">
                    <p></p>
                    <a href="{{route('login')}}">Signin?</a>

                </div>
                <button type="submit" class="btn primaryBtn">RESET PASSWORD</button>
            </form>
        </section>
    </main>
@endsection
