@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout='layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout='layout.driver'; ?>
@elseif($role=='2')
    <?php $layout='layout.default'; ?>
@elseif($role=='1')
    <?php $layout='layout.admin'; ?>
@endif
{{-- Content --}}
@extends($layout)
@section('content')
    <style>
        textarea {
            resize: none;
        }
    </style>
{{--{{dd(Auth::user())}}--}}
{{--    @php--}}
{{--if(Auth::guard('admin')->check()){--}}
{{--    Auth::guard()->logout();--}}
{{--}--}}
{{--if(Auth::guard('user')->check()){--}}
{{--    Auth::guard('user')->logout();--}}
{{--    dd('sdfdsf');--}}
{{--}--}}

{{--?    @endphp--}}
{{--    @if(Auth::guard('admin'))--}}
{{--        {{dd(Auth()->guard())}}@elseif(Auth::guard('user')->check())  {{dd('user')}} @endif--}}
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Mobile Toggle-->
                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">
                        <span></span>
                    </button>
                    <!--end::Mobile Toggle-->
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Support Tickets</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
{{--                            <li class="breadcrumb-item">--}}
{{--                                <a href="" class="text-muted">Apps</a>--}}
{{--                            </li>--}}
{{--                            <li class="breadcrumb-item">--}}
{{--                                <a href="" class="text-muted">Inbox</a>--}}
{{--                            </li>--}}
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>

            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Inbox-->

                <div class="modal fade" id="checkout" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
                     aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">

                                <h5 class="modal-title">Create Ticket</h5>

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <i aria-hidden="true" class="ki ki-close"></i>
                                </button>
                            </div>
                            <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                          <div class="modal-body">
                              <form action="{{route('user.ticket.store')}}"  method="post" id="ticket_create" enctype="multipart/form-data">
                                  @csrf
                                    <div class="border-bottom">
                                      <input class="form-control border-0 px-8 min-h-45px" name="title" required  placeholder="Ticket Subject">
                                  </div>
{{--                                  <div class="border-bottom">--}}
{{--                                      <input class="form-control border-0 px-8 min-h-45px" name="c_n" placeholder="Contact Number" value="{{@Auth::user()->profile->contact_no}}">--}}
{{--                                  </div>--}}
{{--                                  <div class="border-bottom">--}}
{{--                                      <input class="form-control border-0 px-8 min-h-45px" name="f_n" placeholder="First Name" value="{{@Auth::user()->profile->first_name}}">--}}
{{--                                  </div>--}}
{{--                                  <div class="border-bottom">--}}
{{--                                      <input class="form-control border-0 px-8 min-h-45px" name="l_n" placeholder="Last Name" value="{{@Auth::user()->profile->last_name}}">--}}
{{--                                  </div>--}}
{{--                                  <div class="border-bottom">--}}
{{--                                      <input class="form-control border-0 px-8 min-h-45px" name="email" placeholder="Email Address" value="{{@Auth::user()->email}}">--}}
{{--                                  </div>--}}
                                  <div class="form-group mt-5">
                                      <select name="type"  class="form-control select2" required onchange="typee(this.value);" data-placeholder="Choose Ticket Type">
                                          <option  value="" disabled="" selected>Choose Ticket Type</option>
                                          @foreach($ticket_types as $type)
                                              @if($type->status=='1')
                                                  <option value="{{$type->id}}">{{$type->name}}</option>
                                              @endif
                                          @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group mt-5">
                                      <select name="campaign_id"  class="form-control sec2" required onchange="getData();" data-placeholder="Choose Campaign">
                                          <option  value="" disabled="" selected>Choose campaign</option>
                                          @if(Auth::user()->role_id=='2')
                                              @foreach($data['campaigns'] as $campaign)
                                                 <option value="{{$campaign->id}}" data-status = "{{$campaign->status_id}}">CMP#{{$campaign->id}}&nbsp {{$campaign->campaign->title}} &nbsp {{@$campaign->status->name}}</option>
                                              @endforeach
                                          @elseif(Auth::user()->role_id=='3')
                                              @foreach($data['campaigns'] as $campaign)
                                                 <option value="{{@$campaign->campaign->id}}">CMP#{{@$campaign->campaign->id}}&nbsp {{@$campaign->campaign->title}}</option>
                                              @endforeach
                                        @elseif(Auth::user()->role_id=='4')
                                              @foreach($data['campaigns'] as $campaign1)
                                                    @foreach($campaign1->appliedcampaigns as $campaign)
                                                         <option value="{{$campaign->campaign->id}}">CMP#{{$campaign->campaign->id}}&nbsp {{$campaign->campaign->campaign->title}}</option>
                                                     @endforeach
                                              @endforeach
                                          @endif
                                      </select>
                                  </div>
                                  <div class="form-group mt-5 stat ">
                                      <select name="selected_camp_status"  class="form-control camp_status  " required >
                                          <option  value="" disabled="" selected>Status of campaign</option>
                                      @foreach($campaign_statuses as $status)
                                             <option value="{{$status->id}}">{{$status->name}}</option>
                                       @endforeach
                                      </select>

                                  </div>
{{--                                  <div class="form-group ">--}}

{{--                                      <select name="assigne_id"   class="form-control ">--}}
{{--                                          <option value="" selected="" disabled="">Assign Ticket</option>--}}
{{--                                          <option value="1">Assign 1</option>--}}
{{--                                          <option value="2">Assign 2</option>--}}
{{--                                          <option value="3">Assign 3</option>--}}
{{--                                      </select>--}}
{{--                                  </div>--}}
{{--                                  <div class="form-group ">--}}
{{--                                      <select name="priority" class="form-control ">--}}
{{--                                          <option value="" selected="" disabled="">Select Priority</option>--}}
{{--                                          <option value="2">Urgent</option>--}}
{{--                                          <option value="1">Normal</option>--}}
{{--                                          <option value="3">High</option>--}}
{{--                                      </select>--}}
{{--                                  </div>--}}
{{--                                  <div class="form-group ">--}}
{{--                                      <select name="ticket_type" class="form-control ">--}}
{{--                                          <option value="" selected="" disabled="">Select Ticket Type</option>--}}
{{--                                          <option value="1">Ticket 1</option>--}}
{{--                                          <option value="2">Ticket 2</option>--}}
{{--                                          <option value="3">Ticket 3</option>--}}
{{--                                      </select>--}}
{{--                                  </div>--}}
                                  <div class="form-group ">
                                      <label for="">Ticket Detail </label>
                                      <textarea name="detail" required class="form-control" rows="5"></textarea>
                                  </div>
                                  <div class="form-group">
                                      <label for="fil">Attachment </label>
                                      <input type="file" name="image" accept="image/*" id="fil" class="form-control">
                                  </div>
                                  <input type="submit" class="btn btn-success float-right">

                              </form>
                          </div>

                        </div>
                    </div>
                </div>
                <div class="d-flex flex-row">
                    <div class="flex-row-fluid ml-lg-8 d-block" id="kt_inbox_list">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch">
                            <div class="row">
                                <!--begin::Toolbar-->
                                @if(Auth::user()->department_id==null)
                                <div class="col-12 mt-1">
                                    <button data-toggle="modal" data-target="#checkout" class="btn mb-n25 float-right mr-8 mt-5 btn-default btn-primary btn-hover-primary  ">
                                       Create Ticket
                                    </button>
                                </div>
                             @endif
                            </div>

                            <!--begin::Body-->
                            <div class="row pt-10">
                                <div class="col-md-2 offset-3 pr-0 ">
                                    <form action="#" id="frm">
                                       @if($data['tickets_all']->count()>0)
                                        <select name="ticket"  class="form-control select2" required data-placeholder="Ticket">
                                            <option  value="" disabled="" selected>Ticket</option>
                                            @foreach( $data['tickets_all'] as $ticket)
                                                <option value="{{$ticket}}" {{(isset($_GET['ticket'])&& $ticket==$_GET['ticket'])?'selected':''}}>{{$ticket}}</option>
                                            @endforeach
                                        </select>
                                     @endif
                                </div>
                                <div class="col-md-3 pl-1">

                                    <select name="type"  class="form-control select2" required data-placeholder="Ticket Type">
                                        <option  value="" disabled="" selected>Ticket Type</option>
                                        @foreach($ticket_types as $type)
                                            <option value="{{$type->id}}" {{(isset($_GET['type'])&& $type->id==$_GET['type'])?'selected':''}}>{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                    </form>
                                </div>
                                <div class="col-md-1 pr-0">
                                    <button type="submit" class="btn btn-success" onclick="document.getElementById('frm').submit()">search</button>
                                </div>
                                <div class="col-md-1 pt-5 pl-0">
                                    @if(isset($_GET['type']) || isset($_GET['ticket']))
                                        <a href="#" onClick="window.location.href='./tickets'"
                                           style="text-decoration: underline; padding-left:5px;">
                                            reset
                                        </a>
                                    @endif
                                </div>
                            </div>


                            <table class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center {{($data['tickets']->count()==0)?'teable-fluid':'table-responsive'}} px-3 pt-5">
                                <thead>
                                <tr class="text-left text-uppercase">
                                    <th >#</th>
                                    @if($role=='1') <th>User</th><th>Role</th> @endif
                                    <th>Campaign</th>
                                    <th >Title</th>
                                    <th >Type</th>
                                    <th >Status</th>
                                    <th style="max-width: 150px">Choosed Campaign Status</th>
                                    <th  style="max-width: 150px">Current Campaign Status</th>
                                    <th >Replies </th>
                                    <th >Last update</th>
                                    <th> </th>
                                </tr>
                                </thead>
                                <tbody>
                                @forelse ($data['tickets'] as $ticket)
                                <tr>
                                    <td class="text-center py-8">
                                        <a class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$ticket->id}}</a>
                                    </td>
                                    @if($role=='1')
                                    <td class="text-center py-8">
                                        <a class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$ticket->user->name}}</a>
                                    </td>
                                        <td class="text-center py-8">
                                        <a class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{($ticket->user->role_id=='4')?'Ent Driver':(($ticket->user->role_id=='3')?'Driver':'Advetrtisor')}}</a>
                                    </td>

                                    @endif
                                    <td class="text-center">
                                        <span class="font-weight-bolder text-info"><a href="{{(is_null($ticket->campaign))?'':'./campaign-detail/'.$ticket->campaign->slug}}">{{(is_null($ticket->campaign_id))?'':"CMP-$ticket->campaign_id" }}</a></span>
                                    </td>
                                    <td class="w-350px">
                                        <span class="font-weight-bolder">{{$ticket->title}}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="font-weight-bolder">{{(is_null($ticket->gettype))?'':$ticket->gettype->name}}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="font-weight-bolder">{{(is_null($ticket->ticketStatus))?'':$ticket->ticketStatus->name}}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="font-weight-bolder">{{(is_null($ticket->choosedStatus))?'':$ticket->choosedStatus->name}}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="font-weight-bolder">{{(is_null($ticket->campaign))?'':$ticket->campaign->status->name}}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="font-weight-bolder">{{$ticket->threads_count}}</span>
                                    </td>
                                    <td class="text-center">
                                        <span class="font-weight-bolder">{{$ticket->updated_at->diffForHumans()}}</span>
                                    </td>
                                    <td>
                                        <span class="text-dark-75 text-center font-weight-bolder d-block font-size-lg"><i class="fa fa-eye cursor-pointer" onclick="details({{$ticket}},'{{$data['admin']}}')"></i></span>
                                    </td>
                                </tr>
                                    @empty
                                    <tr>
                                        <td colspan="9" >
                                            <div class="col-md">
                                            <span class="text-dark-75 text-center font-weight-bolder d-block font-size-lg">No Ticket found</span>
                                            </div>
                                        </td>
                                    </tr>

                                @endforelse


                                </tbody>
                            </table>

                            @if($data['tickets']->total()>10)
                                <nav aria-label="Page navigation example ">
                                    <ul class="pagination float-right mt-4 mr-10">
                                        <h6 class="pt-3">Total tickets:{{$data['tickets']->total()}}  </h6>  &nbsp &nbsp&nbsp  {{$data['tickets']->appends(['type' => @$_GET['type'],'ticket' => @$_GET['ticket']])->links()}}
                                    </ul>
                                </nav>
                            @endif

                            <div class="card-body table-responsive px-0 d-none">
                                <!--begin::Items-->

                                <div class="list list-hover min-w-500px" data-inbox="list">
                                    @forelse ($data['tickets'] as $ticket)
                                    <!--begin::Item-->
                                    <div class="d-flex align-items-start list-item card-spacer-x py-3"  onclick="showDetails({{$ticket}},'{{$ticket->created_at->diffForHumans()}}','{{$data['admin']}}')" data-inbox="message">
                                        <!--begin::Toolbar-->
                                        <div class="d-flex align-items-center">
                                            <!--begin::Actions-->
                                            <div class="d-flex align-items-center mr-3" data-inbox="actions">
{{--                                                <label class="checkbox checkbox-inline checkbox-primary flex-shrink-0 mr-3">--}}
{{--                                                    <input type="checkbox" />--}}
{{--                                                    <span></span>--}}
{{--                                                </label>--}}
{{--                                                <a href="#" class="btn btn-icon btn-xs btn-hover-text-warning active" data-toggle="tooltip" data-placement="right" title="Star">--}}
{{--                                                    <i class="flaticon-star text-muted"></i>--}}
{{--                                                </a>--}}
                                                <a href="#" class="btn btn-icon btn-xs text-hover-warning" data-toggle="tooltip" data-placement="right" title="Mark as important">
                                                    <i class="flaticon-add-label-button text-muted"></i>
                                                </a>
                                            </div>
                                            <!--end::Actions-->
                                            <!--begin::Author-->
                                            <div class="d-flex align-items-center flex-wrap w-xxl-20px" data-toggle="view">
                                                <a href="#" class="font-weight-bold text-dark-75 text-hover-primary">{{$ticket->id}}</a>
                                            </div>
                                            <div class="d-flex align-items-center flex-wrap w-xxl-50px mr-3" data-toggle="view">
                                                <span class="symbol symbol-15">
                                              CMP#{{@$ticket->campaign->id}}
                                                </span>
                                            </div>
                                            <div class="d-flex align-items-center flex-wrap w-xxl-100px mr-3">
                                                <span class="symbol symbol-15">
                                             {{@$ticket->gettype->name}}
                                                </span>
                                            </div>
                                            @if(Auth::user()->role_id=='1')
                                            <div class="d-flex align-items-center flex-wrap w-xxl-2 00px mr-3" data-toggle="view">
                                                <span class="symbol symbol-35 mr-3">
                                                        @if(@$ticket->user->profile->profile_pic)
                                                        @php $profile_pic='/images/profile/'.$ticket->user->profile->profile_pic;  @endphp
                                                        @else
                                                            @php $profile_pic='assets/media/users/default.jpg'  @endphp
                                                        @endif
{{--                                                                    <span class="symbol-label" style="background-image: url({{asset($profile_pic)}})"></span>--}}
                                                    <img id='preview-imge-thums' class='symbol-label' src='{{asset($profile_pic)}}' alt=''>
                                                </span>
                                                <a  class="font-weight-bold text-dark-75 text-hover-primary">{{ucfirst($ticket->user->name)}}</a>
                                            </div>
                                            @endif
                                            <div class="d-flex align-items-center flex-wrap w-xxl-100px mr-3" data-toggle="view">
                                                <span class="label label-lg w-60 label-light-info label-inline">
                                              {{@$ticket->campaign->status->name}}
                                                </span>
                                            </div>


                                            <!--end::Author-->
                                        </div>
                                        <!--end::Toolbar-->
                                        <!--begin::Info-->
                                        <div class="d-flex align-items-center flex-wrap w-xxl-500px mr-3" data-toggle="view">
                                            <div>
                                                <span class="font-weight-bolder font-size-lg mr-2">{{$ticket->title}} -</span>
                                                <span class="text-muted">{{$ticket->detail}}</span>
                                            </div>
{{--                                            <div class="mt-2">--}}
{{--                                                <span class="label label-light-danger font-weight-bold label-inline">new</span>--}}
{{--                                            </div>--}}
                                        </div>
                                        <!--end::Info-->
                                        <!--begin::Datetime-->
                                        <div class="mt-2 mr-3 font-weight-normal w-100px text-right text-muted" data-toggle="view">{{\Carbon\Carbon::parse($ticket->created_at)->format('M d')}}</div>
                                        <!--end::Datetime-->
                                    </div>
                                    <!--end::Item-->
                                    @empty
                                            <div class="col-2 offset-5" >No Ticket Found</div>
                                        @endforelse
                                        @if($data['tickets']->total()>12)
)
                                            <nav aria-label="Page navigation example ">
                                                <ul class="pagination float-right mt-4 mr-10">
                                                    <h6 class="pt-3">Total tickets:{{$data['tickets']->total()}}  </h6>  &nbsp &nbsp&nbsp  {{$data['tickets']->appends(['type' => @$_GET['type'],'ticket' => @$_GET['ticket']])->links()}}
                                                </ul>
                                            </nav>
                                        @endif



                                </div>
                                <!--end::Items-->

                            </div>
                            <!--end::Body-->

                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::List-->
                    <!--begin::View-->
                    <div class="flex-row-fluid ml-lg-8 d-none" id="kt_inbox_view">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch">
                            <!--begin::Header-->
                            <div class="card-header align-items-center flex-wrap justify-content-between py-5 h-auto">
                                <!--begin::Left-->
                                <div class="d-flex align-items-center my-2">
                                    <a href="#" class="btn btn-clean btn-icon btn-sm mr-6" data-inbox="back">
                                        <i class="flaticon2-left-arrow-1"></i>
                                    </a>
{{--                                    <span class="btn btn-default btn-icon btn-sm mr-2" data-toggle="tooltip" title="Archive">--}}
{{--														<span class="svg-icon svg-icon-md">--}}
{{--															<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->--}}
{{--															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																	<rect x="0" y="0" width="24" height="24" />--}}
{{--																	<path d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z" fill="#000000" opacity="0.3" />--}}
{{--																	<path d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z" fill="#000000" />--}}
{{--																</g>--}}
{{--															</svg>--}}
{{--                                                            <!--end::Svg Icon-->--}}
{{--														</span>--}}
{{--													</span>--}}
{{--                                    <span class="btn btn-default btn-icon btn-sm mr-2" data-toggle="tooltip" title="Spam">--}}
{{--														<span class="svg-icon svg-icon-md">--}}
{{--															<!--begin::Svg Icon | path:assets/media/svg/icons/Code/Warning-1-circle.svg-->--}}
{{--															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																	<rect x="0" y="0" width="24" height="24" />--}}
{{--																	<circle fill="#000000" opacity="0.3" cx="12" cy="12" r="10" />--}}
{{--																	<rect fill="#000000" x="11" y="7" width="2" height="8" rx="1" />--}}
{{--																	<rect fill="#000000" x="11" y="16" width="2" height="2" rx="1" />--}}
{{--																</g>--}}
{{--															</svg>--}}
{{--                                                            <!--end::Svg Icon-->--}}
{{--														</span>--}}
{{--													</span>--}}
{{--                                    <span class="btn btn-default btn-icon btn-sm mr-2" data-toggle="tooltip" title="Delete">--}}
{{--														<span class="svg-icon svg-icon-md">--}}
{{--															<!--begin::Svg Icon | path:assets/media/svg/icons/General/Trash.svg-->--}}
{{--															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																	<rect x="0" y="0" width="24" height="24" />--}}
{{--																	<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />--}}
{{--																	<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />--}}
{{--																</g>--}}
{{--															</svg>--}}
{{--                                                            <!--end::Svg Icon-->--}}
{{--														</span>--}}
{{--													</span>--}}
{{--                                    <span class="btn btn-default btn-icon btn-sm mr-2" data-toggle="tooltip" title="Mark as read">--}}
{{--														<span class="svg-icon svg-icon-md">--}}
{{--															<!--begin::Svg Icon | path:assets/media/svg/icons/General/Duplicate.svg-->--}}
{{--															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																	<rect x="0" y="0" width="24" height="24" />--}}
{{--																	<path d="M15.9956071,6 L9,6 C7.34314575,6 6,7.34314575 6,9 L6,15.9956071 C4.70185442,15.9316381 4,15.1706419 4,13.8181818 L4,6.18181818 C4,4.76751186 4.76751186,4 6.18181818,4 L13.8181818,4 C15.1706419,4 15.9316381,4.70185442 15.9956071,6 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--																	<path d="M10.1818182,8 L17.8181818,8 C19.2324881,8 20,8.76751186 20,10.1818182 L20,17.8181818 C20,19.2324881 19.2324881,20 17.8181818,20 L10.1818182,20 C8.76751186,20 8,19.2324881 8,17.8181818 L8,10.1818182 C8,8.76751186 8.76751186,8 10.1818182,8 Z" fill="#000000" />--}}
{{--																</g>--}}
{{--															</svg>--}}
{{--                                                            <!--end::Svg Icon-->--}}
{{--														</span>--}}
{{--													</span>--}}
{{--                                    <span class="btn btn-default btn-icon btn-sm mr-2" data-toggle="tooltip" title="Move">--}}
{{--														<span class="svg-icon svg-icon-md">--}}
{{--															<!--begin::Svg Icon | path:assets/media/svg/icons/Files/Media-folder.svg-->--}}
{{--															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--																	<rect x="0" y="0" width="24" height="24" />--}}
{{--																	<path d="M3.5,21 L20.5,21 C21.3284271,21 22,20.3284271 22,19.5 L22,8.5 C22,7.67157288 21.3284271,7 20.5,7 L10,7 L7.43933983,4.43933983 C7.15803526,4.15803526 6.77650439,4 6.37867966,4 L3.5,4 C2.67157288,4 2,4.67157288 2,5.5 L2,19.5 C2,20.3284271 2.67157288,21 3.5,21 Z" fill="#000000" opacity="0.3" />--}}
{{--																	<path d="M10.782158,17.5100514 L15.1856088,14.5000448 C15.4135806,14.3442132 15.4720618,14.0330791 15.3162302,13.8051073 C15.2814587,13.7542388 15.2375842,13.7102355 15.1868178,13.6753149 L10.783367,10.6463273 C10.5558531,10.489828 10.2445489,10.5473967 10.0880496,10.7749107 C10.0307022,10.8582806 10,10.9570884 10,11.0582777 L10,17.097272 C10,17.3734143 10.2238576,17.597272 10.5,17.597272 C10.6006894,17.597272 10.699033,17.566872 10.782158,17.5100514 Z" fill="#000000" />--}}
{{--																</g>--}}
{{--															</svg>--}}
{{--                                                            <!--end::Svg Icon-->--}}
{{--														</span>--}}
{{--													</span>--}}
                                </div>
                                <!--end::Left-->
                                <!--begin::Right-->
{{--                                <div class="d-flex align-items-center justify-content-end text-right my-2">--}}
{{--                                    <span class="text-muted font-weight-bold mr-4" data-toggle="dropdown">1 - 50 of 235</span>--}}
{{--                                    <span class="btn btn-default btn-icon btn-sm mr-2" data-toggle="tooltip" title="Previose message">--}}
{{--														<i class="ki ki-bold-arrow-back icon-sm"></i>--}}
{{--													</span>--}}
{{--                                    <span class="btn btn-default btn-icon btn-sm mr-2" data-toggle="tooltip" title="Next message">--}}
{{--														<i class="ki ki-bold-arrow-next icon-sm"></i>--}}
{{--													</span>--}}
{{--                                    <div class="dropdown" data-toggle="tooltip" title="Settings">--}}
{{--														<span class="btn btn-default btn-icon btn-sm" data-toggle="dropdown">--}}
{{--															<i class="ki ki-bold-more-hor icon-1x"></i>--}}
{{--														</span>--}}
{{--                                        <div class="dropdown-menu dropdown-menu-right p-0 m-0 dropdown-menu-md">--}}
{{--                                            <!--begin::Navigation-->--}}
{{--                                            <ul class="navi navi-hover py-5">--}}
{{--                                                <li class="navi-item">--}}
{{--                                                    <a href="#" class="navi-link">--}}
{{--																		<span class="navi-icon">--}}
{{--																			<i class="flaticon2-drop"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="navi-text">New Group</span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                                <li class="navi-item">--}}
{{--                                                    <a href="#" class="navi-link">--}}
{{--																		<span class="navi-icon">--}}
{{--																			<i class="flaticon2-list-3"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="navi-text">Contacts</span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                                <li class="navi-item">--}}
{{--                                                    <a href="#" class="navi-link">--}}
{{--																		<span class="navi-icon">--}}
{{--																			<i class="flaticon2-rocket-1"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="navi-text">Groups</span>--}}
{{--                                                        <span class="navi-link-badge">--}}
{{--																			<span class="label label-light-primary label-inline font-weight-bold">new</span>--}}
{{--																		</span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                                <li class="navi-item">--}}
{{--                                                    <a href="#" class="navi-link">--}}
{{--																		<span class="navi-icon">--}}
{{--																			<i class="flaticon2-bell-2"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="navi-text">Calls</span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                                <li class="navi-item">--}}
{{--                                                    <a href="#" class="navi-link">--}}
{{--																		<span class="navi-icon">--}}
{{--																			<i class="flaticon2-gear"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="navi-text">Settings</span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                                <li class="navi-separator my-3"></li>--}}
{{--                                                <li class="navi-item">--}}
{{--                                                    <a href="#" class="navi-link">--}}
{{--																		<span class="navi-icon">--}}
{{--																			<i class="flaticon2-magnifier-tool"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="navi-text">Help</span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                                <li class="navi-item">--}}
{{--                                                    <a href="#" class="navi-link">--}}
{{--																		<span class="navi-icon">--}}
{{--																			<i class="flaticon2-bell-2"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="navi-text">Privacy</span>--}}
{{--                                                        <span class="navi-link-badge">--}}
{{--																			<span class="label label-light-danger label-rounded font-weight-bold">5</span>--}}
{{--																		</span>--}}
{{--                                                    </a>--}}
{{--                                                </li>--}}
{{--                                            </ul>--}}
{{--                                            <!--end::Navigation-->--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <!--end::Right-->
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body p-0">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center justify-content-between flex-wrap card-spacer-x py-5">
                                    <!--begin::Title-->
                                    <div class="d-flex align-items-center mr-2 py-2">
{{--                                        <div class="font-weight-bold font-size-h3 mr-3 " id="ticket_title"></div>--}}
{{--                                        <span class="label label-light-primary font-weight-bold label-inline mr-2">inbox</span>--}}
{{--                                        <span class="label label-light-danger font-weight-bold label-inline">important</span>--}}
                                    </div>
                                    <!--end::Title-->
                                    <!--begin::Toolbar-->
{{--                                    <div class="d-flex py-2">--}}
{{--                                        <span class="btn btn-default btn-sm btn-icon mr-2">--}}
{{--                                            <i class="flaticon2-sort"></i>--}}
{{--                                        </span>--}}
{{--                                       <span class="btn btn-default btn-sm btn-icon" data-dismiss="modal">--}}
{{--                                            <i class="flaticon2-fax"></i>--}}
{{--                                        </span>--}}
{{--                                    </div>--}}
                                    <!--end::Toolbar-->
                                </div>
                                <!--end::Header-->
                                <!--begin::Messages-->
                                <div class="mb-3">
                                    <div class=" shadow-xs toggle-on" >
                                        <div class="d-flex align-items-center card-spacer-x py-6">
                                                <span class="symbol symbol-50 mr-4">
                                                    <img id="preview-imge-thums" class="symbol-label" id="imgeshowerr" src="{{asset('assets/media/users/default.jpg')}}" alt="">
                                                </span>
                                            <div class="d-flex flex-column flex-grow-1 flex-wrap mr-2">
                                                <div class="d-flex">
                                                    <span  class="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2" id="nam"></span>
                                                    <div class="font-weight-bold text-muted">
                                                       <span id="created_date"></span></div>
                                                </div>
{{--                                                <div class="d-flex flex-column">--}}
{{--                                                    <div class="toggle-off-item">--}}
{{--																		<span class="font-weight-bold text-muted cursor-pointer" data-toggle="dropdown">to me--}}
{{--																		<i class="flaticon2-down icon-xs ml-1 text-dark-50"></i></span>--}}
{{--                                                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-left p-5">--}}
{{--                                                            <table>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted min-w-75px py-2">From</td>--}}
{{--                                                                    <td>Mark Andre</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted py-2">Date:</td>--}}
{{--                                                                    <td>Jul 30, 2019, 11:27 PM</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted py-2">Subject:</td>--}}
{{--                                                                    <td>Trip Reminder. Thank you for flying with us!</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted py-2">Reply to:</td>--}}
{{--                                                                    <td></td>--}}
{{--                                                                </tr>--}}
{{--                                                            </table>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="text-muted font-weight-bold toggle-on-item" data-inbox="toggle" id="det"></div>--}}
{{--                                                </div>--}}
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <div class="font-weight-bold text-muted mr-2" id="created_datee"></div>
{{--                                                <div class="d-flex align-items-center" data-inbox="toolbar">--}}
{{--																	<span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top" title="Star">--}}
{{--																		<i class="flaticon-star icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                    <span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top" title="Mark as important">--}}
{{--																		<i class="flaticon-add-label-button icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                    <span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top" title="Reply">--}}
{{--																		<i class="flaticon2-reply-1 icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                    <span class="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" title="Settings">--}}
{{--																		<i class="flaticon-more icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                </div>--}}
                                            </div>
                                        </div>
                                        <div class="card-spacer-x py-5  toggle-off-item my-2 mx-1" style="border:1px solid #f3f6f9">
                                            <div class="row">
                                                <div class="col-md-1 offset-4">Title:</div> <div class="col-md-6" id="ticket_title"></div>
                                                <div class="col-md-1 offset-4">Detail:</div> <div class="col-md-6" id="ticket_detail"></div>  </div>
{{--                                            <div class="font-weight-bold font-size-h3 mr-3  text-center" id="ticket_detail"></div>--}}

                                            {{--                                            <p>Ticket Details:</p>--}}
{{--                                            <p  id="ticket_detail" class="pl-0 pt-9 text-center text-muted "></p>--}}
                                            <br>
{{--                                            <p class="mb-0">By:</p>--}}
                                            <p class="d-none" id="username"></p>
                                            <div class="row">
                                                <div class="col-4  offset-4 ">
                                                    <p id="ticket_img"></p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div id="datas"></div>
{{--                                    <div class="cursor-pointer shadow-xs toggle-off" data-inbox="message">--}}
{{--                                        <div class="d-flex align-items-center card-spacer-x py-6">--}}
{{--															<span class="symbol symbol-50 mr-4" data-toggle="expand">--}}
{{--																<span class="symbol-label" style="background-image: url('assets/media/users/100_14.jpg')"></span>--}}
{{--															</span>--}}
{{--                                            <div class="d-flex flex-column flex-grow-1 flex-wrap mr-2">--}}
{{--                                                <div class="d-flex" data-toggle="expand">--}}
{{--                                                    <a href="#" class="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">Sean Stone</a>--}}
{{--                                                    <div class="font-weight-bold text-muted">--}}
{{--                                                        <span class="label label-success label-dot mr-2"></span>1 Day ago</div>--}}
{{--                                                </div>--}}
{{--                                                <div class="d-flex flex-column">--}}
{{--                                                    <div class="toggle-off-item">--}}
{{--																		<span class="font-weight-bold text-muted cursor-pointer" data-toggle="dropdown">to me--}}
{{--																		<i class="flaticon2-down icon-xs ml-1 text-dark-50"></i></span>--}}
{{--                                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-left p-5">--}}
{{--                                                            <table>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted w-75px py-2">From</td>--}}
{{--                                                                    <td>Mark Andre</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted py-2">Date:</td>--}}
{{--                                                                    <td>Jul 30, 2019, 11:27 PM</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted py-2">Subject:</td>--}}
{{--                                                                    <td>Trip Reminder. Thank you for flying with us!</td>--}}
{{--                                                                </tr>--}}
{{--                                                                <tr>--}}
{{--                                                                    <td class="text-muted py-2">Reply to:</td>--}}
{{--                                                                    <td>mark.andre@gmail.com</td>--}}
{{--                                                                </tr>--}}
{{--                                                            </table>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="text-muted font-weight-bold toggle-on-item" data-toggle="expand">With resrpect, i must disagree with Mr.Zinsser. We all know the most part of important part....</div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="d-flex align-items-center">--}}
{{--                                                <div class="font-weight-bold text-muted mr-2" data-toggle="expand">Jul 15, 2019, 11:19AM</div>--}}
{{--                                                <div class="d-flex align-items-center">--}}
{{--																	<span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top" title="Star">--}}
{{--																		<i class="flaticon-star icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                    <span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top" title="Mark as important">--}}
{{--																		<i class="flaticon-add-label-button icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                    <span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top" title="Reply">--}}
{{--																		<i class="flaticon2-reply-1 icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                    <span class="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" title="Settings">--}}
{{--																		<i class="flaticon-more icon-1x"></i>--}}
{{--																	</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="card-spacer-x py-3 toggle-off-item">--}}
{{--                                            <p>Hi Bob,</p>--}}
{{--                                            <p>With resrpect, i must disagree with Mr.Zinsser. We all know the most part of important part of any article is the title.Without a compelleing title, your reader won't even get to the first sentence.After the title, however, the first few sentences of your article are certainly the most important part.</p>--}}
{{--                                            <p>Jornalists call this critical, introductory section the "Lede," and when bridge properly executed, it's the that carries your reader from an headine try at attention-grabbing to the body of your blog post, if you want to get it right on of these 10 clever ways to omen your next blog posr with a bang</p>--}}
{{--                                            <p>Best regards,</p>--}}
{{--                                            <p>Jason Muller</p>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                                <!--end::Messages-->
                                <!--begin::Reply-->
                                <div class="card-spacer mb-3" id="kt_inbox_reply">
                                    <div class="card card-custom shadow-sm">
                                        <div class="card-body p-0">
                                            <!--begin::Form-->
                                            @php  $url= (Request::segment(1) == "admin")?'ticket.thread.create':'user.ticket.thread.create' @endphp
                                            <form action="{{route($url)}}" id="kt_inbox_reply_form" method="post" enctype="multipart/form-data">
                                                @csrf
                                                <!--begin::Body-->
                                                <div class="d-block">
                                                    <div class="border-bottom">
                                                        <input class="form-control border-0 px-8 min-h-45px" required name="subject" placeholder="Message" />
{{--                                                        <input type="hidden" name="ticket_id" id="ticket_id"/>--}}
{{--                                                        <input type="hidden" name="department_id" id="department_id"/>--}}
{{--                                                        <input type="hidden" name="creator_id" id="creator_id"/>--}}
                                                    </div>
                                                    <!--end::Subject-->
                                                    <!--begin::Message-->
                                                    <textarea name="detail" class="form-control"  required placeholder="Detail ...." rows="8" ></textarea>
{{--                                                    <div id="kt_inbox_reply_editor" class="border-0" style="height: 250px"></div>--}}
                                                    <div class="dropzone dropzone-multi px-8 py-4" id="kt_inbox_reply_attachments">
                                                        <div class="dropzone-items">
                                                            <div class="dropzone-item" style="display:none">
                                                                <div class="dropzone-file">
                                                                    <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                                        <span data-dz-name="">some_image_file_name.jpg</span>
                                                                        <strong>(
                                                                            <span data-dz-size="">340kb</span>)</strong>
                                                                    </div>
                                                                    <div class="dropzone-error" data-dz-errormessage=""></div>
                                                                </div>
                                                                <div class="dropzone-progress">
                                                                    <div class="progress">
                                                                        <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                                    </div>
                                                                </div>
                                                                <div class="dropzone-toolbar">
                                                                    <span class="dropzone-delete" data-dz-remove="">
                                                                        <i class="flaticon2-cross"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Attachments-->
                                                </div>
                                                <!--end::Body-->
                                                <!--begin::Footer-->
                                                <div class="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top">
                                                    <!--begin::Actions-->
                                                    <div class="d-flex align-items-center mr-3">
                                                        <!--begin::Send-->
                                                        <div class="btn-group mr-4">
                                                            <button class="btn btn-primary font-weight-bold px-6">Send</button>
                                                        </div>
                                                        <!--end::Send-->
                                                        <!--begin::Other-->
{{--                                                        <span class="btn btn-icon btn-sm btn-clean mr-2" id="kt_inbox_reply_attachments_select">--}}
{{--																			<i class="flaticon2-clip-symbol"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="btn btn-icon btn-sm btn-clean">--}}
{{--																			<i class="flaticon2-pin"></i>--}}
{{--														</span>--}}
                                                        <!--end::Other-->
                                                    </div>
                                                    <!--end::Actions-->
                                                    <!--begin::Toolbar-->
{{--                                                    <div class="d-flex align-items-center">--}}
{{--																		<span class="btn btn-icon btn-sm btn-clean mr-2" data-toggle="tooltip" title="More actions">--}}
{{--																			<i class="flaticon2-settings"></i>--}}
{{--																		</span>--}}
{{--                                                        <span class="btn btn-icon btn-sm btn-clean" data-inbox="dismiss" data-toggle="tooltip" title="Dismiss reply">--}}
{{--																			<i class="flaticon2-rubbish-bin-delete-button"></i>--}}
{{--																		</span>--}}
{{--                                                    </div>--}}
                                                    <!--end::Toolbar-->
                                                </div>
                                                <!--end::Footer-->
                                            </form>
                                            <!--end::Form-->
                                        </div>
                                    </div>
                                </div>
                                <!--end::Reply-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::View-->
                </div>
                <!--end::Inbox-->
                <!--begin::Compose-->
                <div class="modal modal-sticky modal-sticky-lg modal-sticky-bottom-right" id="kt_inbox_compose" role="dialog" data-backdrop="false">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <!--begin::Form-->
                            <form id="kt_inbox_compose_form">
                                <!--begin::Header-->
                                <div class="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-bottom">
                                    <h5 class="font-weight-bold m-0">Compose</h5>
                                    <div class="d-flex ml-2">
														<span class="btn btn-clean btn-sm btn-icon mr-2">
															<i class="flaticon2-arrow-1 icon-1x"></i>
														</span>
                                        <span class="btn btn-clean btn-sm btn-icon" data-dismiss="modal">
															<i class="ki ki-close icon-1x"></i>
										</span>
                                    </div>
                                </div>
                                <!--end::Header-->
                                <!--begin::Body-->
                                <div class="d-block">
                                    <!--begin::To-->
                                    <div class="d-flex align-items-center border-bottom inbox-to px-8 min-h-45px">
                                        <div class="text-dark-50 w-75px">To:</div>
                                        <div class="d-flex align-items-center flex-grow-1">
                                            <input type="text" class="form-control border-0" name="compose_to" value="Chris Muller, Lina Nilsons" />
                                        </div>
                                        <div class="ml-2">
                                            <span class="text-muted font-weight-bold cursor-pointer text-hover-primary mr-2" data-inbox="cc-show">Cc</span>
                                            <span class="text-muted font-weight-bold cursor-pointer text-hover-primary" data-inbox="bcc-show">Bcc</span>
                                        </div>
                                    </div>
                                    <!--end::To-->
                                    <!--begin::CC-->
                                    <div class="d-none align-items-center border-bottom inbox-to-cc pl-8 pr-5 min-h-45px">
                                        <div class="text-dark-50 w-75px">Cc:</div>
                                        <div class="flex-grow-1">
                                            <input type="text" class="form-control border-0" name="compose_cc" value="" />
                                        </div>
                                        <span class="btn btn-clean btn-xs btn-icon" data-inbox="cc-hide">
                                        <i class="la la-close"></i>
                                    </span>
                                    </div>
                                    <!--end::CC-->
                                    <!--begin::BCC-->
                                    <div class="d-none align-items-center border-bottom inbox-to-bcc pl-8 pr-5 min-h-45px">
                                        <div class="text-dark-50 w-75px">Bcc:</div>
                                        <div class="flex-grow-1">
                                            <input type="text" class="form-control border-0" name="compose_bcc" value="" />
                                        </div>
                                        <span class="btn btn-clean btn-xs btn-icon" data-inbox="bcc-hide">
															<i class="la la-close"></i>
														</span>
                                    </div>
                                    <!--end::BCC-->
                                    <!--begin::Subject-->
                                    <div class="border-bottom">
                                        <input class="form-control border-0 px-8 min-h-45px" name="compose_subject" placeholder="Subject" />
                                    </div>
                                    <!--end::Subject-->
                                    <!--begin::Message-->
                                    <div id="kt_inbox_compose_editor" class="border-0" style="height: 250px"></div>
                                    <!--end::Message-->
                                    <!--begin::Attachments-->
                                    <div class="dropzone dropzone-multi px-8 py-4" id="kt_inbox_compose_attachments">
                                        <div class="dropzone-items">
                                            <div class="dropzone-item" style="display:none">
                                                <div class="dropzone-file">
                                                    <div class="dropzone-filename" title="some_image_file_name.jpg">
                                                        <span data-dz-name="">some_image_file_name.jpg</span>
                                                        <strong>(
                                                            <span data-dz-size="">340kb</span>)</strong>
                                                    </div>
                                                    <div class="dropzone-error" data-dz-errormessage=""></div>
                                                </div>
                                                <div class="dropzone-progress">
                                                    <div class="progress">
                                                        <div class="progress-bar bg-primary" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0" data-dz-uploadprogress=""></div>
                                                    </div>
                                                </div>
                                                <div class="dropzone-toolbar">
                                                    <span class="dropzone-delete" data-dz-remove="">
                                                        <i class="flaticon2-cross"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Attachments-->
                                </div>
                                <!--end::Body-->
                                <!--begin::Footer-->
                                <div class="d-flex align-items-center justify-content-between py-5 pl-8 pr-5 border-top">
                                    <!--begin::Actions-->
                                    <div class="d-flex align-items-center mr-3">
                                        <!--begin::Send-->
                                        <div class="btn-group mr-4">
                                            <span class="btn btn-primary font-weight-bold px-6">Send</span>
                                            <span class="btn btn-primary font-weight-bold dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="button"></span>
                                            <div class="dropdown-menu dropdown-menu-sm dropup p-0 m-0 dropdown-menu-right">
                                                <ul class="navi py-3">
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																			<span class="navi-icon">
																				<i class="flaticon2-writing"></i>
																			</span>
                                                            <span class="navi-text">Schedule Send</span>
                                                        </a>
                                                    </li>
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																			<span class="navi-icon">
																				<i class="flaticon2-medical-records"></i>
																			</span>
                                                            <span class="navi-text">Save &amp; archive</span>
                                                        </a>
                                                    </li>
                                                    <li class="navi-item">
                                                        <a href="#" class="navi-link">
																			<span class="navi-icon">
																				<i class="flaticon2-hourglass-1"></i>
																			</span>
                                                            <span class="navi-text">Cancel</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <!--end::Send-->
                                        <!--begin::Other-->
                                        <span class="btn btn-icon btn-sm btn-clean mr-2" id="kt_inbox_compose_attachments_select">
                                        <i class="flaticon2-clip-symbol"></i>
                                    </span>
                                    <span class="btn btn-icon btn-sm btn-clean">
                                        <i class="flaticon2-pin"></i>
                                    </span>
                                        <!--end::Other-->
                                    </div>
                                    <!--end::Actions-->
                                    <!--begin::Toolbar-->
                                    <div class="d-flex align-items-center">
                                        <span class="btn btn-icon btn-sm btn-clean mr-2" data-toggle="tooltip" title="More actions">
                                            <i class="flaticon2-settings"></i>
                                        </span>
                                        <span class="btn btn-icon btn-sm btn-clean" data-inbox="dismiss" data-toggle="tooltip" title="Dismiss reply">
															<i class="flaticon2-rubbish-bin-delete-button"></i>
										</span>
                                    </div>
                                    <!--end::Toolbar-->
                                </div>
                                <!--end::Footer-->
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                </div>
                <!--end::Compose-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--end::Content-->

    <!--end::Content-->

<div class="modal fade show" id="detailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border: none;padding-bottom:0px;">
                <h5 class="modal-title">Ticket:  <span class="ticket_id"></span></h5>
                <div style="float: right">
                <span type="button" class="close pt-3 pl-2" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </span>
                    <span  class="text-right text-dark float-right btn btn-sm btn-success" id="reso" data-dismiss="modal" data-target="#resolveModal"  data-toggle="modal" >
                   Resolve Ticket
                </span>
                    </div>
            </div>
            <div class="modal-body">
                <div class="card card-custom">
                    <div class="card-body">
                        <!--begin::Invoice-->
                        <div class="row justify-content-center pl-4 p-1 px-md-0">
                         <div class="col-lg-8 offset-1 text-dark">
                            <h3 class="">Title: <span class="ticket-title"></span></h3>
                            <h3 class="">Description: <span class="ticket-description"></span></h3>
                        </div>
                            <div class="col-lg-3">
                                <div class="ticket_img">
                                    <!--end::Invoice-->
                                </div>
                            </div>
                        </div>
                        <div class="card-footer align-items-center">
                            <form action="{{route($url)}}" id="kt_inbox_reply_form" method="post" enctype="multipart/form-data">
                            @csrf
                            <textarea class="form-control" name="detail" rows="2" required  id='mxg' placeholder="Message"></textarea>
                            <div class="d-flex align-items-center justify-content-between mt-5">
                                <input type="hidden" name="ticket_id" id="ticket_id"/>
                                <input type="hidden" name="department_id" id="department_id"/>
                                <input type="hidden" name="creator_id" id="creator_id"/>
                                <div class="mr-3"></div>
                                <div>
                                    <input type="submit" id="send" value="Send" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6"/>
                                </div>
                            </div>
                                </form>

                            <!--begin::Compose-->
                        </div>
                        <br>
                        <table class=" col-md-8 offset-1 table-fluid mb-5" id="jqueryTable">
                            <thead>
                            <th >From</th>
                            <th >To</th>
                            <th >Message</th>
                            <th >Time</th>
{{--                            <th >Status</th>--}}
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade show" id="resolveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border: none;padding-bottom:0px;">
                <h5 class="modal-title">Ticket close feedback</h5>
                <div style="float: right">
                <span type="button" class="close pt-3 pl-2" data-dismiss="modal" aria-label="Close" data-target="#detailModal" data-toggle="modal">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </span>
                   &nbsp
                    </div>
            </div>
            <div class="modal-body">
                <div class="card card-custom">
                    <div class="card-body">
                        <!--begin::Invoice-->
                        <div class="card-footer align-items-center">
                            <textarea class="form-control" name="message" rows="2" id="mesg" required placeholder="Message"></textarea>
                            <span class="text-danger errr d-none">Field required </span>
                            <div class="d-flex align-items-center justify-content-between mt-5">
                                <div class="mr-3"></div>
                                <div>
                                    <input onclick="formsubmit()"  value="save" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6"/>
                                </div>
                            </div>
                            <!--begin::Compose-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>















@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/inbox/inbox.js') }}" type="text/javascript"></script>
{{--    <script src="assets/js/pages/custom/inbox/inbox.js"></script>--}}
    {{--    <script src="{{ asset('js/pages/custom/wizard/wizard-3.js') }}" type="text/javascript"></script>--}}
    {{--    <script src="assets/js/pages/custom/wizard/wizard-3.js"></script>--}}
<script>

    {{--function showDetails(ticket,created_date,user_name) {--}}
    {{--    var str1 = '<div class="accordian_container"><a href="#" class="accordian_trigger"><h4>Co-Borrower Information</h4></a><hr/><div class="accordian_item" id="accord_item_2"><label> First Name</label><br/><input type="text"/><br/><label>Middle Name</label><br/> <input type="text"/><br/> <label>Last Name</label><br/> <input type="text" /><br/> <label>Home Number</label><br/> <input type="text"/><br> <label>Work Number</label><br/><input type="text"/><br> <label>Cell Number</label><br/> <input type="text"/><br> </div> </div>"';--}}
    {{--    $('#ticket_title').html(ticket.title);--}}
    {{--    $('#ticket_detail').text(ticket.detail);--}}
    {{--    $('#det').text(ticket.detail);--}}
    {{--    $('#username').text(ticket.user.name);--}}
    {{--    $('#created_date').text(created_date);--}}
    {{--    $('#created_datee').text(moment(ticket.created_at).format('MMM DD, YYYY, h:mmA'));--}}
    {{--    $('#nam').text(ticket.user.name);--}}
    {{--    $('#ticket_id').val(ticket.id);--}}
    {{--    $('#department_id').val(ticket.department_id);--}}
    {{--    $('#creator_id').val(ticket.user_id);--}}
    {{--    var img_url= ticket.img_url;--}}
    {{--    $('#compose_to').html('');--}}
    {{--    var urll = '{{asset('/images/tickets/')}}';--}}
    {{--    var urlll = '{{asset('/images/profile/')}}';--}}
    {{--    var defal_imge = '{{asset('/assets/media/users/default.jpg')}}';--}}
    {{--    if(ticket.user.profile.profile_pic==null){--}}
    {{--        var imges = defal_imge;--}}
    {{--        $('#preview-imge-thums').attr('src', imges);--}}
    {{--    }else{--}}
    {{--        var imges = urlll + '/'+ticket.user.profile.profile_pic;--}}
    {{--        $('#preview-imge-thums').attr('src', imges);--}}
    {{--    }--}}
    {{--    --}}
    {{--    if(img_url){--}}
    {{--    $('#ticket_img').html('<img class="img-thumbnail" src="'+urll+'/'+img_url+'" >');--}}

    {{--    }--}}
    {{--    $('#datas').html('')--}}
    {{--    if({{Auth::user()->id}}!=ticket.user.id){--}}

    {{--    var user_id =ticket.user.id ;--}}
    {{--    var name = ticket.user.name;--}}
    {{--    var newOption = new Option(name, user_id, false, false);--}}
    {{--    $('#compose_to').append(newOption).val(user_id).trigger('change');--}}
    {{--    }--}}
    {{--    function capitalizeFirstLetter(string) {--}}
    {{--        return string.charAt(0).toUpperCase() + string.slice(1);--}}
    {{--    }--}}
    {{--    var1=0;--}}
    {{--    $.each(ticket.supporters, function (index, value) {--}}
    {{--        var defal_imge = '{{asset('assets/media/users/default.jpg')}}';--}}
    {{--        var user_id = value['id'];--}}
    {{--        var name = capitalizeFirstLetter(value['name']) ;--}}
    {{--        if({{Auth::user()->id}}!=user_id){--}}
    {{--            var newOption = new Option(name, user_id, false, false);--}}
    {{--            $('#compose_to').append(newOption).trigger('change');--}}
    {{--            var1++;--}}

    {{--        }--}}
    {{--    });--}}

    {{--    $.each(ticket.threads, function (index, value) {--}}

    {{--        var fromUser ='';--}}
    {{--        var toUser ='';--}}
    {{--        var readUser='';--}}
    {{--        if(value.from_user==null){--}}
    {{--            fromUser= user_name;--}}
    {{--        }else{--}}
    {{--            fromUser= value.from_user.name--}}
    {{--            if(value.from_user.profile!=null && value.from_user.profile.profile_pic){--}}
    {{--                defal_imge= urlll + '/'+ticket.user.profile.profile_pic;--}}
    {{--            }--}}
    {{--            else{--}}
    {{--                defal_imge = '{{asset('assets/media/users/default.jpg')}}';--}}
    {{--            }--}}
    {{--        }--}}
    {{--        if(value.to_user==null){--}}
    {{--            toUser=user_name--}}
    {{--        }else{--}}
    {{--            toUser= value.to_user.name--}}
    {{--        }--}}
    {{--        var str=' <div class="cursor-pointer shadow-xs toggle-off" data-inbox="message"><div class="d-flex align-items-center card-spacer-x py-6"><span class="symbol symbol-50 mr-4" data-toggle="expand"><img id=\'preview-imge-thums\' class=\'symbol-label\' src=\''+defal_imge+'\' alt=\'\'></span><div class="d-flex flex-column flex-grow-1 flex-wrap mr-2"><div class="d-flex" data-toggle="expand"><a  class="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">'+capitalizeFirstLetter(fromUser)+'</a><div class="font-weight-bold text-muted"><span class="label label-success label-dot mr-2"></span>'+ moment(value.created_at).fromNow()+'</div></div><div class="d-flex flex-column"><div class="toggle-off-item"><span class="font-weight-bold text-muted cursor-pointer" data-toggle="dropdown">to me<i class="flaticon2-down icon-xs ml-1 text-dark-50"></i></span><div class="dropdown-menu dropdown-menu-md dropdown-menu-left p-5"><table><tr><td class="text-muted w-75px py-2">From</td><td>'+fromUser+'</td></tr><tr><td class="text-muted py-2">Date</td><td>'+moment(value.created_at).format('MMMM DD YYYY')+'</td></tr><tr><td class="text-muted py-2">Message:</td><td>'+value.subject+'</td></tr><tr><td class="text-muted py-2">Reply to:</td><td>'+toUser+'</td></tr></table></div></div><div class="text-muted font-weight-bold toggle-on-item" data-toggle="expand">'+value.subject+'</div></div></div><div class="d-flex align-items-center"><div class="font-weight-bold text-muted mr-2" data-toggle="expand">'+moment(value.created_at).format('MMM DD, YYYY, h:mmA')+'</div></div></div><div class="card-spacer-x  text-center py-3 toggle-off-item"><p>'+value.message+'</p></div></div>';--}}
    {{--        $('#datas').append(str);--}}
    {{--    });--}}
    {{--}--}}


    function details(ticket,user_name){
        if(ticket.status_id==3){
            $('#reso').hide();
            $('#send').hide();
            $('#mxg').val(ticket.close_message);
            $('#mxg').attr('readonly','readonly');
            }
        else{
            $('#reso').show();
            $('#mxg').val('');
            $('#send').show();
            $('#mxg').removeAttr('readonly','readonly');
        }


        $('.img-thumbnail').attr('src','')
        $('#jqueryTable tbody').html('');
        var img_url= ticket.img_url;
        var urll = '{{asset('/images/tickets/')}}';
        if(img_url){
            $('.ticket_img').html('<img class="img-thumbnail" src="'+urll+'/'+img_url+'" >');

        }
        console.log('sssssss'+ticket)
        var threads='';
        if(ticket.threads.length==0){
            $('#jqueryTable tbody').append("<tr><td colspan='5' class='text-center'>No message</td></tr>");
        }

        $.each(ticket.threads, function (index, value) {
            $('#jqueryTable tbody').html('');
            var fromUser ='';
            var toUser ='';
            var readUser='';
            if(value.from_user==null){
                fromUser= user_name;
            }else{
                fromUser= value.from_user.name
                if(value.from_user.profile!=null && value.from_user.profile.profile_pic){
                    defal_imge= urlll + '/'+ticket.user.profile.profile_pic;
                }
                else{
                    defal_imge = '{{asset('assets/media/users/default.jpg')}}';
                }
            }
            console.log(value)
            if(value.to_user==null){
                toUser=user_name
            }else{
                toUser= value.to_user.name
            }
            threads += "<tr class='border-bottom' ><td>" + fromUser + "</td><td >" +  toUser  + "</td><td >" +value.message + "</td><td >" +moment(value.created_at).format('M-DD-YY, h:mmA') + "</td></tr>"
      });
        $('#jqueryTable').append(threads);


        $('.ticket_id').text(ticket.id);
        $('.ticket-title').text(ticket.title);
        $('.ticket-description').text(ticket.detail);
        $('#detailModal').modal();
        $('#nam').text(ticket.user.name);
        $('#ticket_id').val(ticket.id);
        // $('#department_id').val(ticket.department_id);
        $('#creator_id').val(ticket.user_id);
        return;

        // $('#datas').html(str);
        // $('#ticket_title').html(ticket.title);
        $('#ticket_detail').text(ticket.detail);
        $('#det').text(ticket.detail);
        $('#username').text(ticket.user.name);
        $('#created_date').text(created_date);
        $('#created_datee').text(moment(ticket.created_at).format('MMM DD, YYYY, h:mmA'));
        $('#nam').text(ticket.user.name);
        $('#ticket_id').val(ticket.id);
        $('#department_id').val(ticket.department_id);
        $('#creator_id').val(ticket.user_id);
        var img_url= ticket.img_url;
        $('#compose_to').html('');
        var urll = '{{asset('/images/tickets/')}}';
        var urlll = '{{asset('/images/profile/')}}';
        var defal_imge = '{{asset('/assets/media/users/default.jpg')}}';
        // console.log(ticket.user.profile.profile_pic)
        if(ticket.user.profile.profile_pic==null){
            var imges = defal_imge;
            $('#preview-imge-thums').attr('src', imges);
         // console.log('imge not found')
        }else{
            var imges = urlll + '/'+ticket.user.profile.profile_pic;
            $('#preview-imge-thums').attr('src', imges);
            // console.log('imge found')
        }

        // console.log(urll);
        if(img_url){
        $('#ticket_img').html('<img class="img-thumbnail" src="'+urll+'/'+img_url+'" >');

        }
        $('#datas').html('')
        if({{Auth::user()->id}}!=ticket.user.id){

        var user_id =ticket.user.id ;
        var name = ticket.user.name;
        var newOption = new Option(name, user_id, false, false);
        $('#compose_to').append(newOption).val(user_id).trigger('change');
        }
        function capitalizeFirstLetter(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        var1=0;
        $.each(ticket.supporters, function (index, value) {
            var defal_imge = '{{asset('assets/media/users/default.jpg')}}';
            var user_id = value['id'];
            var name = capitalizeFirstLetter(value['name']) ;
            if({{Auth::user()->id}}!=user_id){
                var newOption = new Option(name, user_id, false, false);
                $('#compose_to').append(newOption).trigger('change');
                var1++;

            }
        });

        // console.log(ticket.threads)
        $.each(ticket.threads, function (index, value) {

            var fromUser ='';
            var toUser ='';
            var readUser='';
            if(value.from_user==null){
                fromUser= user_name;
            }else{
                fromUser= value.from_user.name
                if(value.from_user.profile!=null && value.from_user.profile.profile_pic){
                    defal_imge= urlll + '/'+ticket.user.profile.profile_pic;
                }
                else{
                    defal_imge = '{{asset('assets/media/users/default.jpg')}}';
                }


            }
            if(value.to_user==null){
                toUser=user_name
            }else{
                toUser= value.to_user.name
            }

            var str=' <div class="cursor-pointer shadow-xs toggle-off" data-inbox="message"><div class="d-flex align-items-center card-spacer-x py-6"><span class="symbol symbol-50 mr-4" data-toggle="expand"><img id=\'preview-imge-thums\' class=\'symbol-label\' src=\''+defal_imge+'\' alt=\'\'></span><div class="d-flex flex-column flex-grow-1 flex-wrap mr-2"><div class="d-flex" data-toggle="expand"><a  class="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">'+capitalizeFirstLetter(fromUser)+'</a><div class="font-weight-bold text-muted"><span class="label label-success label-dot mr-2"></span>'+ moment(value.created_at).fromNow()+'</div></div><div class="d-flex flex-column"><div class="toggle-off-item"><span class="font-weight-bold text-muted cursor-pointer" data-toggle="dropdown">to me<i class="flaticon2-down icon-xs ml-1 text-dark-50"></i></span><div class="dropdown-menu dropdown-menu-md dropdown-menu-left p-5"><table><tr><td class="text-muted w-75px py-2">From</td><td>'+fromUser+'</td></tr><tr><td class="text-muted py-2">Date</td><td>'+moment(value.created_at).format('MMMM DD YYYY')+'</td></tr><tr><td class="text-muted py-2">Message:</td><td>'+value.subject+'</td></tr><tr><td class="text-muted py-2">Reply to:</td><td>'+toUser+'</td></tr></table></div></div><div class="text-muted font-weight-bold toggle-on-item" data-toggle="expand">'+value.subject+'</div></div></div><div class="d-flex align-items-center"><div class="font-weight-bold text-muted mr-2" data-toggle="expand">'+moment(value.created_at).format('MMM DD, YYYY, h:mmA')+'</div></div></div><div class="card-spacer-x  text-center py-3 toggle-off-item"><p>'+value.message+'</p></div></div>';

            // var str1='<div class="cursor-pointer shadow-xs toggle-off" data-inbox="message"><div class="d-flex align-items-center card-spacer-x py-6"> <span class="symbol symbol-50 mr-4" data-toggle="expand"> <span class="symbol-label"></span></span><div class="d-flex flex-column flex-grow-1 flex-wrap mr-2"><div class="d-flex flex-column flex-grow-1 flex-wrap mr-2"> <div class="d-flex" data-toggle="expand"> <a class="font-size-lg font-weight-bolder text-dark-75 text-hover-primary mr-2">'+value.from_user.name+'</a><div class="font-weight-bold text-muted"><span class="label label-success label-dot mr-2"></span>'+ moment(value.created_at).fromNow()+'</div></div><div class="d-flex flex-column"><div class="toggle-off-item"><span class="font-weight-bold text-muted cursor-pointer" data-toggle="dropdown">to me<i class="flaticon2-down icon-xs ml-1 text-dark-50"></i></span><div class="dropdown-menu dropdown-menu-md dropdown-menu-left p-5">  <table><tbody><tr> <td class="text-muted w-75px py-2">From</td><td>'+value.from_user.name+'</td></tr><tr><td class="text-muted py-2">Date</td><td>Jul 30</td></tr><tr><td class="text-muted py-2">Subject</td><td>Trip Reminder Thank you for flying with us</td></tr><tr><td class="text-muted py-2">Reply to</td><td>gffdm</td></tr> </tbody></table></div> </div> <div class="text-muted font-weight-bold toggle-on-item" data-toggle="expand">'+value.subject+'</div> </div> </div> <div class="d-flex align-items-center"> <div class="font-weight-bold text-muted mr-2" data-toggle="expand">Jul 15</div><div class="d-flex align-items-center"> <span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top"  data-original-title="Star"><i class="flaticon-star icon-1x"></i></span><span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top" title=" data-original-title="Mark as important"><i class="flaticon-add-label-button icon-1x"></i> </span><span class="btn btn-clean btn-sm btn-icon mr-2" data-toggle="tooltip" data-placement="top"  data-original-title="Reply"> <i class="flaticon2-reply-1 icon-1x"></i></span><span class="btn btn-clean btn-sm btn-icon" data-toggle="tooltip" data-placement="top" data-original-title="Settings"><i class="flaticon-more icon-1x"></i> </span> </div></div></div> <div class="card-spacer-x py-3 toggle-off-item"> <p >'+value.message+'</p>  </div> </div>';
            $('#datas').append(str);

            // console.log('secod data',newOption);
        });

    }




    function typee(val) {
        if(val==4){
        // $('campaign_id').html('');
       $('#ticket_create .sec2').hide('3000'    );
       $('#ticket_create .sec2').removeAttr('required');
       $('#ticket_create .camp_status').removeAttr('required');
       $('#ticket_create .camp_status').hide('3000');

    }else{
            $('#ticket_create .camp_status').attr('required','required');
            $('#ticket_create .sec2').attr('required','required');
            $('#ticket_create .sec2').show('3000');
            $('#ticket_create .camp_status').show('3000');
        }
    }

    function getData() {

        var current_status=$('.sec2 option:selected').attr('data-status')
        $('#ticket_create .stat').show('3000');
       if(current_status==3){
           $('.camp_status').children('option[value="1"]').hide();
           $('.camp_status').children('option[value="3"]').show();
           $('.camp_status').children('option[value="4"]').hide();
           $('.camp_status').children('option[value="2"]').show();
           $('.camp_status').children('option[value="6"]').show();
       }
       if(current_status==6){
               $('.camp_status').children('option[value="1"]').hide();
               $('.camp_status').children('option[value="3"]').hide();
               $('.camp_status').children('option[value="4"]').hide();
               $('.camp_status').children('option[value="2"]').hide();
               $('.camp_status').children('option[value="6"]').show();
       }
       if(current_status==2){
               $('.camp_status').children('option[value="1"]').hide();
               $('.camp_status').children('option[value="3"]').hide();
               $('.camp_status').children('option[value="4"]').hide();
               $('.camp_status').children('option[value="2"]').show();
               $('.camp_status').children('option[value="6"]').show();
       }   if(current_status==1){
               $('.camp_status').children('option[value="1"]').hide();
               $('.camp_status').children('option[value="3"]').show();
               $('.camp_status').children('option[value="4"]').hide();
               $('.camp_status').children('option[value="2"]').show();
               $('.camp_status').children('option[value="6"]').show();
       }
       if(current_status==4){
               $('.camp_status').children('option[value="1"]').hide();
               $('.camp_status').children('option[value="3"]').hide();
               $('.camp_status').children('option[value="4"]').show();
               $('.camp_status').children('option[value="2"]').hide();
               $('.camp_status').children('option[value="6"]').show();
       }
        // alert($('.sec2').attr('data-status_16'));
    }
    function formsubmit(){
        var msg=$('#mesg').val();
        if (msg == '') {
            $('.errr').removeClass('d-none');
            return;
        }
        $('.errr').addClass('d-none');
        $('#loader').removeClass('d-none');
        var data = {
            'ticket_id': $('#ticket_id').val(),
            'mesg':msg ,
        };
        $.ajax({
            url: '{{route('user.ticket.close')}}',
            type: "GET",
            data: data,
            success: function (result) {
                if (result == 1) {
                    $('#mxg').val(msg);
                    $('#mxg').attr('readonly','readonly');
                    $('#detailModal').modal('show');
                    $('#resolveModal').modal('hide');
                    $('#loader').addClass('d-none');
                    $('#send').hide();
                    $('#reso').hide();
                    $('#detail').hide();
                    toastr.success('Ticket closed successfully');
                } else {
                    toastr.error('Please contact to support', 'Error Occured');
                }
            }
        })

    }


    $('.select2').select2({
        // placeholder: "Choose Support user",
        allowClear: true,
        width: '100%'
    });

    @if (\Session::has('created'))
    toastr.success('{!! \Session::get('created') !!}', 'Message sent Sucessfull');
    @endif
</script>
@endsection
