<label>Select year:</label>
<select name="car_year" class="form-control garcartype" id="car_year_1" required >
    @if(isset($driver))
        {{-- @if(isset($modelsss))--}}
        {{--    <option value="" selected disabled>Select Model</option>--}}
        {{--        @foreach($car_companies as $model)--}}
        @if($driver->car_yearr)
            <option value="{{$driver->car_yearr->id}}" selected>{{$driver->car_yearr->model_name}}</option>
        @endif

{{--    <option value="" selected disabled>Making Year</option>--}}
{{--        <option  disabled>Choose Year </option>--}}
  @elseif(isset($years))
            <option selected value="" disabled>Choose Year</option>
    @foreach($years as $year)
        <option value="{{$year->car_model_id}}">{{$year->model->model_name}}</option>
    @endforeach
@else
            <option selected disabled value=""> Choose Year</option>
        @endif




</select>
<span class="form-text text-danger d-none">This field is required.</span>
