<label>Car Name  : </label>
<select name="car_model" id="car_model_1"
        class="form-control car_models" required>
@if(isset($driver))

            @if($driver->car_namee)
            <option value="" selected disabled>Choose Car </option>
             <option value="{{$driver->car_namee->id}}" selected>{{$driver->car_namee->car_name}}</option>
        @endif

    @elseif(isset($modelsss))
        <option selected disabled>Choose Car </option>
        <option value="" selected disabled>Choose Car </option>
            @foreach($modelsss as $model)
                <option value="{{$model->car_name_id}}">{{$model->carName->car_name}}</option>
            @endforeach
    @else
            <option value="" selected disabled>Choose Car </option>
    @endif

</select>
<span class="form-text text-danger d-none" id="interval_error">Car model field is required.</span>

{{--<script>--}}
{{--    function saveName() {--}}
{{--        window.localStorage.setItem('car_model_1', $('.car_models option:selected').text());--}}
{{--    }--}}

{{--    @if(old('car_model'))--}}
{{--     $('.car_models').html('');--}}
{{--      $('.car_models').html('<option selected  value="{{old('car_model')}}">'+window.localStorage.getItem('car_model_1')+'</option>');--}}
{{--    @endif--}}

{{--</script>--}}
