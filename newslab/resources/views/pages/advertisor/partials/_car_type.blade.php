<label>Car Type:</label>
<select name="car_type[]" id="car_type_1" class="form-control  cartyped" notify="Country" required>
    @if(isset($driver))

        {{-- @if(isset($modelsss))--}}
        {{--    <option value="" selected disabled>Select Model</option>--}}
        {{--        @foreach($car_companies as $model)--}}
        @if($driver->car_typee)
            <option value="" selected >Choose Car Type</option>
            <option value="{{$driver->car_typee->id}}" selected>{{$driver->car_typee->name}}</option>
        @endif
    @elseif(isset($types))
        <option value="" selected disabled >Choose Car Type</option>
         @foreach($types as $type)
            <option value="{{$type->car_type}}">{{$type->carType->name}}</option>
        @endforeach


        @else
        <option value=""  disabled  selected >Choose Car Type</option>
       @endif

</select>
<span class="form-text text-danger d-none" id="interval_error">Car Type is required.</span>
