<label>Car Name  : </label>
<select name="car_model[]" id="car_model_1"
        class="form-control car_models"  >
@if(isset($driver))

            @if($driver->car_namee)
            <option value="" selected >Choose Car </option>
             <option value="{{$driver->car_namee->id}}" selected>{{$driver->car_namee->car_name}}</option>
        @endif

    @elseif(isset($modelsss))
        <option value="" selected >Choose Car </option>
            @foreach($modelsss as $model)
                <option value="{{$model->car_name_id}}">{{$model->carName->car_name}}</option>
            @endforeach
    @else
        <option value="" selected >Choose Car </option>
    @endif

</select>
<span class="form-text text-danger d-none" id="interval_error">Car model field is required.</span>
