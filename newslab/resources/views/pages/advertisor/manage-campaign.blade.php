@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout='layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout='layout.driver'; ?>
@elseif($role=='2')
    <?php $layout='layout.default'; ?>
@elseif($role=='1')
    <?php $layout='layout.admin'; ?>
@endif
@extends($layout)
@section('content')
    <style>

        .StripeElement {
            box-sizing: border-box;
            width:100%;
            height: 40px;
            padding: 10px 12px;
            border: 1px solid #E4E6EF;;
            border-radius: 4px;
            background-color: white;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }
        #payment-form button{
            margin-top:15px;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
    </style>


    <!-- The Modal -->


    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Manage Campaign
                    <div class="text-muted pt-2 font-size-sm">Management of Campaign</div>
                </h3>
            </div>
            <div class="card-toolbar">
{{--                <select class="data-table-10list float-right">--}}
{{--                    <option value="">10</option>--}}
{{--                    <option value="">20</option>--}}
{{--                </select>--}}
                <a class="btn btn-light-primary mr-2" href="javascript:;" id="toggleSearchForm">
                     <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                             <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                                  version="1.1">
                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                     <rect x="0" y="0" width="24" height="24"/>
                                     <path
                                         d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
                                         fill="#000000" opacity="0.3"/>
                                     <path
                                         d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
                                         fill="#000000"/>
                                 </g>
                             </svg>
                         <!--end::Svg Icon-->
                   </span>
                    Search
                </a>



            </div>
        </div>

        <div class="card-body">

            <!--begin::Search Form-->
            <div class="mt-2 mb-5 mt-lg-5 mb-lg-10" id="table-search-form" style="display: <?php  echo ($check=='1')?'block':'none'; ?>" >

                {{--<option   <?php  echo (null!=($request->meal_type_id)&&($meal_type->id ==$request->meal_type_id))?'selected':''; ?> value="{{$meal_type->id}}">{{$meal_type->name}}</option>--}}
                <form action="#" id="frm">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-xl-12  ">
                            <div class="row align-items-center">
                                <div class="col-md-3 my-2 my-md-0  offset-2">
                                    <div class="input-icon">
                                        <input type="text" name="search" class="form-control" placeholder="Search..."
                                               value="{{(isset($_GET['search']))?$_GET['search']:''}}"
                                               id="kt_datatable_search_query"/>
                                        <span><i class="flaticon2-search-1 text-muted"></i></span>
                                    </div>
                                </div>

                                <div class="col-md-3 my-2 my-md-0 ">
                                    <div class="d-flex align-items-center">
                                        <label class=" mb-0 d-none d-md-block">Campaign Status:</label>&nbsp
{{--                                        {{dd($status)}}--}}
                                        <select class="form-control" name='campaign_status' id="kt_datatable_search_status">
                                            {{--                                        <option selected disabled>Choose Campaign status</option>--}}
                                            @foreach($status as $stat)
                                                @if($stat->id!=1)
                                                <option value="{{$stat->id}}" {{(isset($_GET['campaign_status'])&& $stat->id==$_GET['campaign_status'])?'selected':''}} >{{$stat->name}}</option>
                                               @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
{{--                                <div class="col-md-2 my-2 my-md-0">--}}
{{--                                    <div class="d-flex align-items-center">--}}
{{--                                        <label class=" mb-0 d-none d-md-block">Payment Status:</label>&nbsp--}}
{{--                                        <select class="form-control"  name='payment_status' id="kt_datatable_search_type">--}}
{{--                                            <option value="">All</option>--}}
{{--                                            <option value="1">Paid</option>--}}
{{--                                            <option value="2">Refunded</option>--}}
{{--                                            <option value="2">On Hold</option>--}}
{{--                                            <option value="2">pending</option>--}}
{{--                                            --}}{{--                                        <option value="3">Direct</option>--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" onclick="document.getElementById('frm').submit();" class="btn btn-light-primary px-6 font-weight-bold">
                                        Search
                                    </a>
                                    @if(isset($_GET['campaign_status']))
                                        &nbsp<a href="#" onClick="window.location.href='../user/campaign-listing'"  style="text-decoration: underline">
                                            reset
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

            <!-- HTML Table Begin Here -->
            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">Id</th>
                            <th style="max-width: 80px" >
                               Sticker
                            </th>
                            <th style="min-width: 200px" >
                                <span >Title</span>
                            </th>
                            <th>Location</th>
                            <th style="max: 130px;text-align:center">Zip</th>
                            <th style="min-width: 100px">Amount</th>
                            <th >Refunded Amount</th>
                            <th >Drivers required</th>
                            <th style="min-width: 150px">Campaign Status</th>
                            <th style="min-width: 180px">Payment Status</th>
                            <th >Days</th>
                            <th style="min-width: 130px">Duration</th>
                            <th >Created</th>
                            @if(Auth::user()->role_id!='2' )
{{--                            <th class="fix-table-head" style="min-width: 135px;">Action</th>--}}
                                @endif
                        </tr>
                        </thead>
                        <tbody>

                        @if(count($campaigns)==0)
                            <tr class="text-center">
                                <td colspan="13"><h4>Sorry, No campaign found</h4></td>
                            </tr>
                        @endif
                        @foreach($campaigns as $campaign)
                            <tr>
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                        <a href="{{route('user.camp.detail',$campaign->slug)}}">CMP-{{$campaign->id}}</a>
                                    </span>
                                    {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                                </td>
                                <td class="pl-0 ">
                                    <div class=" align-items-center">
                                        <div class="symbol-light ">
                                            <span class="symbol-label">
                                         @if(@$campaign->sticker->image_url!=null)
                                                    @php if(@$campaign->sticker->image_url!=null){
                                                        $url='/images/campaign-sticker/'.$campaign->sticker->image_url;
                                                        }else{
                                                          $url='/media/uploadimge.png';
                                                        } @endphp
                                                    @if($url)
                                                        <img width="100%" height="100%" src="{{asset($url)}}" class=" align-self-end" />
                                                    @endif
                                                @endif
                                            </span>
                                        </div>
                                        <div>

                                            {{--                                            <span class="text-muted font-weight-bold d-block">more infor sample </span>--}}
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center py-8">
                                    <a  class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$campaign->campaign->title}}</a>
                                </td>    <td class="text-center">
                                    <span class="font-weight-bolder " >{{@$campaign->state->state}} {{@$campaign->city->primary_city}} </span>
                                </td>
                                <td class="text-center">
                                    <span class="font-weight-bolder" >{{@$campaign->zip}} </span>
                                </td>
                                <td>
                                    <span class="text-dark-75 text-center font-weight-bolder d-block font-size-lg">${{$campaign->total_price}}</span>
                                </td>
                                <td>
                                    <span class="text-dark-75 text-center font-weight-bolder d-block font-size-lg">${{$campaign->refund_amount->sum('amount')}}</span>
                                </td>
                                <td>
                                    <span class="text-center text-dark-75 font-weight-bolder d-block font-size-lg">{{$campaign->no_of_cars}}</span>
                                </td>
                                <td>
                                    <span   style="min-width:135px !important" class="label label-lg  label-light-{{@$campaign->status->class}} label-inline">{{@$campaign->status->name}}</span>
                                </td>


                                <td class="text-center">
                                    @if(@$campaign->status->id==3 && $campaign->payment_status=='Paid')
                                        <span   style="min-width:145px !important" class="label label-lg w-50 label-light-success label-inline">Paid
                                    @else
{{--                                                {{ dd($campaign)}}--}}
                                            @if($campaign->status->id=='3' &&  $campaign->payment_status=='Pending')
                                                <span  class="example-tools justify-content-center cursor-auto " onclick="getdata({{$campaign}})" data-toggle="modal" data-target="#myModal">
                                                <a style="min-width:145px !important" class=" btn btn-sm btn-default btn-text-primary btn-hover-success btn-icon cursor-pointer"
                                                    data-toggle="tooltip" title="Payment Pending">
                                                 Pay now</a>
                                            </span>
                                            @endif

                                    </span>
                                    @endif
                                        @if($campaign->status->id==6 && $campaign->payment_status )
                                            <span   style="min-width:145px !important" class="label label-lg w-50 label-light-success label-inline">{{$campaign->payment_status}}
                                       @endif
                                        @if($campaign->status->id==4 || $campaign->status->id==2)
                                            <span   style="min-width:145px !important" class="label label-lg w-50 label-light-success label-inline">{{$campaign->payment_status}}
                                       @endif
                                </td>
                                <td class="text-center">
                                    <span class="font-weight-bolder " >{{@$campaign->campaign->campaign_total_days}}</span>
                                </td>
                                <td>
                                    @php  $interval=explode("-",$campaign->campaign->interval)@endphp
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                        {{ Carbon\Carbon::parse($interval[0])->format('M d ') }} -{{ Carbon\Carbon::parse($interval[1])->format('M d') }}</span>
                                    <span class="text-muted font-weight-bold"></span>
                                </td>

{{--                                <td>--}}
{{--                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">--}}
{{--                                     {{ Carbon\Carbon::parse($interval[1])->format('d M Y') }}</span>--}}
{{--                                    <span class="text-muted font-weight-bold"> </span>--}}
{{--                                </td>--}}
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                     {{ Carbon\Carbon::parse($campaign->created_at)->format('d/M/Y') }}</span>
                                    <span class="text-muted font-weight-bold"> </span>
                                </td>

{{--                                @if(Auth::user()->role_id!='2' )--}}
                                {{--                                <td class="pr-0 fix-table-column">--}}
                                {{--                                    <a href="javascript:;" data-toggle="modal" data-target="#viewModal" onclick="viewDetail('{{$campaign}}')"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">--}}
                                {{--                                        <i class="la la-eye"></i>--}}
                                {{--                                    </a>--}}
                                {{--                                    <a href="#" data-toggle="modal" data-target="#editModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">--}}
                                {{--                                        <i class="la la-upload"></i>--}}
                                {{--                                    </a>--}}

                                {{--                                    <a href="javascript:;" data-toggle="modal" data-target="#statusUpdate" onclick="status('{{$campaign->status_id}}','{{$campaign->id}}')" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">--}}
                                {{--                                        <i class="la la-wrench"></i>--}}
                                {{--                                    </a>--}}
                                {{--                                    <a href="javascript:;" data-toggle="modal" onclick="feedback('{{$campaign->id}}')" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">--}}
                                {{--                                        <i class="la la-comments-o"></i>--}}
                                {{--                                    </a>--}}
                                {{--                                    <a href="javascript:;" onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">--}}
                                {{--                                        <i class="la la-trash"></i>--}}
                                {{--                                    </a>--}}


                                {{--                                </td>--}}

                            </tr>
                        @endforeach

                        </tbody>
                    </table>

                </div>
                <nav aria-label="Page navigation example ">
                    <ul class="pagination float-right mt-5">
                        <h6 class="pt-3">Total records: {{$campaigns->total()}}  </h6>  &nbsp &nbsp&nbsp  {{$campaigns->appends(['search' => @$_GET['search'],'campaign_status' => @$_GET['campaign_status']])->links()}}
                    </ul>
                </nav>
                <!--end::Table-->
                <!-- HTML Table End Here -->
            </div>
        </div>
    </div>


    <div class="modal fade show" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Choose payment options</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2 pt-5" >
                                       <br><br>
                                        <div class="col-lg-6 offset-4">
                                            <div class="form-group" >
                                                <h4>Campaign: CMp-<span id="campen-id"></span></h4>
                                                <h4>
                                                    Amount to be paid: <span class="cmp-amount"></span>
                                                </h4>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group text-center border" style=" background: #f6f6f9 !important;border-radius: 20px;">
                                                <span class="paypalA" title="Pay now">
                                                    <i class="fab fa-paypal  m-12" style="color: #2222ca !important; font-size: 50px; "></i>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6" >
                                            <a  href="javascript:;" data-dismiss="modal" data-toggle="modal"  data-target="#stripeMpodal">
                                            <div class="form-group text-center border" style=" background: #f6f6f9 !important;border-radius: 20px;">
                                                <span  class="paypalA" title="Pay now">
                                                    <i class="la la-cc-stripe  m-10" style="color: #4db9fe; !important; font-size:65px;"></i>
                                                </span>
                                            </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Back</button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade show" id="stripeMpodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2 pt-5" >
                                       <br><br>
                                        <div class="col-lg-10 p-5 offset-1"  >
                                            <form action="{{route('user.stripeCharge')}}" method="post" id="payment-form">
                                                <div class="form-row ">
                                                    <div class="col-12 pb-1" >
                                                    <label for="card-element  ">Card company name</label>
                                                    <input type="text" name="company_name"  required class="form-control" placeholder="Company Name">
                                                    </div>
                                                </div>
                                                <div class="form-row pb-1 pt-2 ">
                                                    <label for="card-element  ">Credit or debit card</label>
                                                    <div id="card-element"></div>
                                                    <div id="card-errors" role="alert" class="text-danger"></div>
                                                    {{ csrf_field() }}
                                                </div>
                                                <input type="hidden" name="campaign_id" id="campeign_id">
                                                <button class="btn btn btn-success float-right"  >Pay now <small class="cmp-amount" ></small></button>
                                            </form>
                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal"  data-toggle="modal" data-target="#myModal"> Back</button>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="card-body p-4">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center pt-4 px-4 pt-md-4 ">

                                    <div class="col-md-12">
                                        <div id="kt_repeater_1">
                                            <div class="form-group row" id="kt_repeater_1">
                                                <div data-repeater-list="">
                                                    <div data-repeater-item="" class="form-group row align-items-center">
                                                        <div class="col-md-6">
                                                            <label>Campaign completed Video:</label>
                                                            <input type="file" class="form-control" name="state" placeholder="Select state">
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Photos:</label>
                                                            <input type="file" class="form-control" name="[0][city]">
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-6 mt-4">
                                                            <label>Miles :</label>
                                                            <input type="text" class="form-control" placeholder="Enter Miles" name="[0][zip]">
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <!--end::Invoice-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Submit
                    </button><button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" ></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-1">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pt-5  px-md-12">


                                <div class="col-md-10 p-1">
                                    <div id="kt_repeater_1">
                                        <div class="form-group row">
                                                    <div class="col-md-6 pr-2">
                                                        <label >Campaign :</label>&nbsp
                                                        <span class="d-m "id="campaign"></span>
                                                    </div>
                                                    <div class="col-md-6 pr-2">
                                                        <label >Amount :</label>
                                                        <span class="d-m pr-4 ">540</span>
                                                    </div>

                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>
                    <button type="button" class="btn btn-light-success font-weight-bold" data-dismiss="modal">Next</button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade show" id="feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block"><h5 class="d-inline">Campaign Id:  </h5> <span  id='cmp_id' class="float-right mr-30"></span></span>
                                        </div><br><br><br>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Write feedback about campaign:</label>
                                                <textarea name="s_u1" class="w-100" type="textarea" rows="10"></textarea>
                                                <span class="form-text text-muted"> feedback.</span>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>



                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="statusUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Campaign Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-4">
                                        <div class="row">
                                            <div class="col-md-12 mt-4 ">
                                                <span class="text-center d-block"><h5 class="d-inline">Campaign Id:  </h5> <span id="camp_id"></span></span>
                                            </div><br><br><br>


                                            <div class="col-lg-12">
                                                <form id="form-id" cmethod="POST" action="{{ route('campaign.status.update') }}" >
                                                    @csrf
                                                    <input type="hidden" name="camp_id" id="camp-id">
                                                    <div class="form-check">
                                                        <label>Status Update:</label>
                                                        <select name="s_u" class=" form-control" id="stat1" required>
                                                            <option value="" selected="" disabled="">
                                                                Choose one option
                                                            </option>
                                                            @foreach($status as $stats)
                                                                <option value="{{$stats->id}}">{{$stats->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        {{--                                                <span class="form-text text-muted"> Choose status.</span>--}}
                                                    </div>
                                                </form>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create="" onclick="document.getElementById('form-id').submit();" class="btn btn-sm font-weight-bolder btn-light-primary">
                        Update
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>



@endsection

{{-- Styles Section --}}
@section('styles')
{{--    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>--}}
{{--            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--}}

@endsection




{{-- Scripts Section --}}
@section('scripts')
{{--            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
{{--            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>--}}
{{--            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>--}}

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- vendors --}}
    <script>



       // function formSbt() {
       //     $('.loadr2').removeClass('d-none');
       //     $('#stripeForm').submit();
       //     $('.loadr2').addClass('d-none');
       // }
        // document.getElementById('video_form').submit();

        function status(status_id,camp_id) {
            $("#camp_id").html('CMP-'+camp_id);
            $("#stat1").val(status_id)
            $("#camp-id").val(camp_id)
        }
        function feedback(camp_id) {
            $("#cmp_id").html('CMP-'+camp_id);
        }
        function viewDetail(campaign) {
            console.log(campaign);
            // $("#cmp_id").html('CMP-'+camp_id);
        }


        function del()
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }
$(document).on('click', '#toggleSearchForm', function(){
            $('#table-search-form').slideToggle();
        });

function getdata(campaign){
   $('#campen-id').text(campaign.id)
   $('.cmp-amount').text('$'+campaign.total_price)
   $('#campeign_id').val(campaign.id)
}

        @if (\Session::has('error'))
            toastr.error('{!! \Session::get('error') !!}', 'Transaction not successfull');
        @endif
        @if (\Session::has('created'))
            toastr.error('{!! \Session::get('created') !!}', 'Campaign created successfull');
        @endif
        @if (\Session::has('success'))
            toastr.success('{!! \Session::get('success') !!}', 'Payment successfull');
        @endif
        @if (\Session::has('updated'))
        toastr.success('{!! \Session::get('updated') !!}', 'Updated');
        @endif

{{--        @if(isset($_GET['id']))--}}
{{--        toastr.success( 'Campaign <?php echo $_GET['id'] ?> Created Successfully','Campaign Created');--}}
{{--        @endif--}}
        {{--    @if (\Session::has('deleted'))--}}
        {{--        toastr.warning('{!! \Session::get('deleted') !!}', 'User Deleted');--}}
        {{--    @endif--}}




    </script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    //start stripe
    // Create a Stripe client.
    {{--var stripe = Stripe('{{env('STRIPE_PUBLIC_KEY')}}');--}}
    var stripe = Stripe('pk_test_51HlYDGK7l7e9CDWNxOVqBen7Z7PYhRL6K0sUVS0OGEDFiH7l6Qg204EfEh2ju40UFsY13pbCLZzwPvugzt6D4eCy006htkwlIZ');
    // Create an instance of Elements.
    var elements = stripe.elements();

    // Custom styling can be passed to options when creating an Element.
    // (Note that this demo uses a wider set of styles than the guide below.)
    var style = {
        base: {
            color: '#32325d',
            fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
            fontSmoothing: 'antialiased',
            fontSize: '16px',
            '::placeholder': {
                color: '#aab7c4'
            }
        },
        invalid: {
            color: '#fa755a',
            iconColor: '#fa755a'
        }
    };

    // Create an instance of the card Element.
    var card = elements.create('card', {style: style});

    // Add an instance of the card Element into the `card-element` <div>.
    card.mount('#card-element');

    // Handle real-time validation errors from the card Element.
    card.addEventListener('change', function(event) {
        var displayError = document.getElementById('card-errors');
        if (event.error) {
            displayError.textContent = event.error.message;
        } else {
            displayError.textContent = '';
        }
    });
    // Handle form submission.
    var form = document.getElementById('payment-form');
    form.addEventListener('submit', function(event) {
        event.preventDefault();

        stripe.createToken(card).then(function(result) {
            if (result.error) {
                // Inform the user if there was an error.
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server.
                stripeTokenHandler(result.token);
            }
        });
    });
    // Submit the form with the token ID.
    function stripeTokenHandler(token) {
        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);
        // Submit the form
        form.submit();
    }
    //end stripe
</script>






@endsection
