{{-- Extends layout --}}
@extends('layout.default')
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-3.css') }}" rel="stylesheet" type="text/css"/>
@endsection
{{-- Content --}}
@section('content')
    <?php $count = 0;?>
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Campaign</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Pages</a>
                            </li>
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Campaign</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Actions-->
                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>
                    <!--end::Actions-->
                    <!--begin::Dropdown-->
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions"
                         data-placement="left">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                  <span class="svg-icon svg-icon-success svg-icon-2x">
                     <!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px"
                          height="24px" viewBox="0 0 24 24" version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <polygon points="0 0 24 0 24 24 0 24"/>
                           <path
                               d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z"
                               fill="#000000" fill-rule="nonzero" opacity="0.3"/>
                           <path
                               d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z"
                               fill="#000000"/>
                        </g>
                     </svg>
                      <!--end::Svg Icon-->
                  </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover">
                                <li class="navi-header font-weight-bold py-4">
                                    <span class="font-size-lg">Choose Label:</span>
                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip"
                                       data-placement="right" title="Click to learn more..."></i>
                                </li>
                                <li class="navi-separator mb-3 opacity-70"></li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                        <span class="navi-text">
                        <span class="label label-xl label-inline label-light-success">Customer</span>
                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                        <span class="navi-text">
                        <span class="label label-xl label-inline label-light-danger">Partner</span>
                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                        <span class="navi-text">
                        <span class="label label-xl label-inline label-light-warning">Suplier</span>
                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                        <span class="navi-text">
                        <span class="label label-xl label-inline label-light-primary">Member</span>
                        </span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
                        <span class="navi-text">
                        <span class="label label-xl label-inline label-light-dark">Staff</span>
                        </span>
                                    </a>
                                </li>
                                <li class="navi-separator mt-3 opacity-70"></li>
                                <li class="navi-footer py-4">
                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">
                                        <i class="ki ki-plus icon-sm"></i>Add new</a>
                                </li>
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body p-0">
                        <!--begin: Wizard-->
                        <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first"
                             data-wizard-clickable="true">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav">
                                <div class="wizard-steps px-8 py-8 px-lg-15 py-lg-3">
                                    <!--begin::Wizard Step 1 Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span>  </span>Campaigns Creation
                                            </h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Wizard Nav-->
                            <!--begin: Wizard Body-->
                            <div class="mb-2 wizard-body  py-lg-1 px-lg-15 border-right">
                                <form class="form" id="kt_form">
                                    <!--begin: Wizard Step 1-->
                                    <div class="" data-wizard-type="step-content" data-wizard-state="current">
                                        <h4 class="camp font-weight-bold text-dark">Create Campaign</h4>
                                        <div class="camp" >

                                            <hr/>
                                        </div>
                                        <br>
                                    </div>
                                    <div class="camp row">
                                        <div class="col-md-8 border-right ">
                                            <div class="row">
                                                <!--begin::Input-->
                                                <div class="form-group col-md-3">
                                                    <label>Campaign Title</label>
                                                    <input type="text" class="form-control " name="fname"
                                                           placeholder="Campaign Title" value="" />
                                                    <span
                                                        class="form-text text-muted"> Enter Title.</span>
                                                </div>
                                                <div class="form-group col-md-5">
                                                    <label>Campaign Type:</label>
                                                    <select name="type" class="form-control ">
                                                        <option value="" selected disabled>Select Campaign Type</option>
                                                        <option value="">Type 1</option>
                                                        <option value="overnight">Type 2</option>
                                                        <option value="express">Type 3</option>
                                                        {{--                                                        <option value="basic">more than months</option>--}}
                                                    </select>
                                                    <span class="form-text text-muted">Choose    duration .</span>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label>Time Duration:</label>
                                                    <select name="duration" class="form-control ">
                                                        <option value="" selected disabled>Time duration</option>
                                                        <option value="">days</option>
                                                        <option value="overnight">weeks</option>
                                                        <option value="express">months</option>
                                                        <option value="basic">more than months</option>
                                                    </select>
                                                    <span class="form-text text-muted">Choose    duration .</span>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Start date*</label>
                                                        <input class="form-control" name="s_d" type="date">
                                                        <span
                                                            class="form-text text-muted">Please enter start date.</span>
                                                    </div>
                                                </div>
{{--                                                <hr/>--}}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card">
                                                <h4 class="font-weight-bold text-dark text-center py-2"
                                                    style="background-color: #ebedf3;"> Campaign Detail</h4>
                                                <div class="card-body">
                                                    <div class="form-group row ">
                                                        <div class=" col-sm-7">
                                                            <b>Campaign Title</b>
                                                        </div>
                                                        <div class="  col-sm-5">
                                                            <p>Titl name xyz </p>
                                                        </div>
                                                        <div class="  col-sm-7">
                                                            <b>Campaign Type</b>
                                                        </div>
                                                        <div class="  col-sm-5">
                                                            <p>Type1</p>
                                                        </div>
                                                        <div class="  col-sm-7">
                                                            <b>Campaign Duration</b>
                                                        </div>
                                                        <div class="  col-sm-5">
                                                            <p>7 days </p>
                                                        </div>
                                                        <div class="  col-sm-7">
                                                            <b>Campaign Start date</b>
                                                        </div>
                                                        <div class="  col-sm-5">
                                                            <p>01-11-2020</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <h4 style="display: none"
                                        class="location_info mb-2 mt-2 font-weight-bold text-dark">
                                        Location info</h4>
                                    <hr />
                                    <div style="display: none" class="location_info row">
                                        <div class="col-md-8 border-right ">
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-check">
                                                        <label>Country:</label>
                                                        <select name="country"
                                                                class="select2 form-control ">
                                                            <option value="" selected disabled>Country
                                                            </option>
                                                            <option value="12">England</option>
                                                            <option value="92">UK</option>
                                                            <option value="11">Australia</option>
                                                            <option value="1">India/option>
                                                        </select>
                                                        <span class="form-text text-muted">Please select compaign country.</span>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>State:</label>
                                                        <select name="state"
                                                                class=" select2 form-control ">
                                                            <option value="" selected disabled>Choose
                                                                State
                                                            </option>
                                                            <option value="12">England</option>
                                                            <option value="92">UK</option>
                                                            <option value="11">Australia</option>
                                                            <option value="1">India/option>
                                                        </select>
                                                        <span class="form-text text-muted">Please select compaign country.</span>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-check">
                                                        <label>City:</label>
                                                        <select name="city"
                                                                class=" select2 form-control ">
                                                            <option value="" selected disabled>choose
                                                                City
                                                            </option>
                                                            <option value="12">lahore>
                                                            <option value="92">london</option>
                                                            <option value="11">islamabad</option>
                                                        </select>
                                                        <span
                                                            class="form-text text-muted">Please select compaign city.</span>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Zip code:</label>
                                                        <input type="text" class="form-control "
                                                               name="address1"
                                                               placeholder="zip code" value="ZXC111"/>
                                                        <span class="form-text text-muted">City zip code.</span>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
{{--                                                <hr/>--}}
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="card-body p-2   ">
                                                <div id="accordion">
                                                    <div class="card">
                                                        <div class="card-header p-2" id="headingOne">
                                                            <h5 class="mb-0 d-flex">
                                                                <span class="btn btn-link text-decoration-none border-0 bg-transparent" onclick="onClickback();">
                                                                           <span
                                                                               class="d-flex">
                                                                               <i  class="fa fa-edit"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                                </span> &nbsp
                                                                <span onclick="onClickAccord();"
                                                                        class="btn btn-link collapsed w-100 text-decoration-none "
                                                                        data-toggle="collapse"
                                                                        data-target="#collapseOne"
                                                                        aria-expanded="false"
                                                                        aria-controls="collapseOne">
                                                                           <span
                                                                               class="d-flex justify-content-between w-100">
                                                                               <span>Campaign Info</span>
                                                                               <i id="accord1" class="fa fa-angle-down"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                                </span>
                                                            </h5>
                                                        </div>

                                                        <div id="collapseOne" class="collapse "
                                                             aria-labelledby="headingOne"
                                                             data-parent="#accordion">
                                                            <div class="card-body p-4">
                                                                <div class="form-group row ">
                                                                    <div class=" col-sm-7">
                                                                        <b>Campaign Title</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>Titl name xyz </p>
                                                                    </div>
                                                                    <div class="  col-sm-7">
                                                                        <b>Campaign Type</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>Type1</p>
                                                                    </div>
                                                                    <div class="  col-sm-7">
                                                                        <b>Campaign Duration</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>12hrs</p>
                                                                    </div>
                                                                    <div class="  col-sm-7">
                                                                        <b>Campaign Start date</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>12hrs</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card-body m-0 p-0">
                                                    <div id="googleMap" class="w-100" style="height:270px;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <h4 style="display: none" class="car-info mb-2 mt-2 font-weight-bold text-dark">Car
                                        Info</h4>
                                    <div class="car-info">

                                        <hr/>
                                    </div>
                                    <div style="display: none" class=" car-info row">
                                        <div class="col-md-8">
                                            <div>
                                                <div class="form-group row">
                                                    <div id="kt_repeater_3">
                                                        <div class="form-group row">
                                                            <div data-repeater-list="car_info" class="col-lg-12">
                                                                <div data-repeater-item class="form-group row">
                                                                    <div class="col-lg-4">
                                                                        <div class="form-check">
                                                                            <label>Car Make:</label>
                                                                            <select name="car_make" onchange="showValues();"
                                                                                    class="form-control">
                                                                                <option value="" selected disabled>
                                                                                    Select Make
                                                                                </option>
                                                                                <option value="hyundai">Hyundai</option>
                                                                                <option value="suzuki">Suzuki</option>
                                                                                <option value="Toyota">Toyota</option>
                                                                            </select>
                                                                            <span class="form-text text-muted"> Choose car make company.</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-4">
                                                                        <div class="form-group">
                                                                            <label>Car Type:</label>
                                                                            <select name="car-type" onchange="showValues();"
                                                                                    class="form-control ">
                                                                                <option value="" selected disabled>
                                                                                    Select Type
                                                                                </option>
                                                                                <option value="Hatchback">Hatchback</option>
                                                                                <option value="Sedan">Sedan</option>
                                                                                <option value="SUV">SUV</option>
                                                                                <option value="Mini Van">Mini Van</option>
                                                                                <option value="Crossover">Crossover</option>
                                                                                <option value="Van">Van</option>
                                                                            </select>
                                                                            <span class="form-text text-muted">Choose car type.</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-4">
                                                                        <div class="form-check">
                                                                            <label>Car Model:</label>
                                                                            <select name="car_model"
                                                                                    class="form-control ">
                                                                                <option value="" selected disabled>
                                                                                    Select Model
                                                                                </option>
                                                                                <option value="12">Corolla</option>
                                                                                <option value="92">Civic</option>
                                                                                <option value="11">
                                                                                    Land Cruiser
                                                                                </option>
                                                                            </select>
                                                                            <span class="form-text text-muted">Manufactored year.</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xl-4">
                                                                        <div class="form-check">
                                                                            <label>Select year:</label>
                                                                            <select name=car_year
                                                                                    class="form-control ">
                                                                                <option value="" selected disabled>
                                                                                    Making Year
                                                                                </option>
                                                                                <option value="2020">2020</option>
                                                                                <option value="2019">2019</option>
                                                                                <option value="2018">2018</option>
                                                                                <option value="2017">2017</option>
                                                                                <option value="2016">2016</option>
                                                                                <option value="2015">2015</option>
{{--                                                                                <option value="11">2014</option>--}}
{{--                                                                                <option value="11">2013</option>--}}
{{--                                                                                <option value="11">2012</option>--}}
{{--                                                                                <option value="11">2011</option>--}}
{{--                                                                                <option value="11">2010</option>--}}
                                                                            </select>
                                                                            <span class="form-text text-muted">Manufactored year.</span>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-xl-4">
                                                                        <div class="form-group">
                                                                            <label>How Often miles Drived:</label>
                                                                            <input type="text" class="form-control "
                                                                                   name="car_miles"
                                                                                   placeholder="Miles Drived"
                                                                                   />
                                                                            <span class="form-text text-muted">Number of miles.</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-xl-4">
                                                                        <div class="form-group">
                                                                            <label>Number of Cars :</label>
                                                                            <input type="text" class="form-control "
                                                                                   name="no_of_cars"
                                                                                   />
                                                                            <span
                                                                                class="form-text text-muted">cars</span>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-xl-12">
                                                                        <div class="row">
                                                                            <div class="col-xl-4 ">
                                                                                <div class="form-check">
                                                                                    <label>How often drives a
                                                                                        car:</label>
                                                                                    <select name="how_often"
                                                                                            class="form-control"
                                                                                            onchange="yesnoCheck(this);">
                                                                                        <option value="" selected>choose
                                                                                            one option
                                                                                        </option>
                                                                                        <option value="d">Daily</option>
                                                                                        <option value="w">Weekly
                                                                                        </option>
                                                                                        <option value="m">Monthly
                                                                                        </option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="day-picker col-xl-4"
                                                                                 style="display: none">
                                                                                <div class="form-check">
                                                                                    <label>Select Start Time :</label>
                                                                                    <input type="time"
                                                                                           class="form-control "
                                                                                           name="starttime"
                                                                                           />
                                                                                    <span class="form-text text-muted">cars</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="day-picker col-xl-4"
                                                                                 style="display: none">
                                                                                <div class="form-check">
                                                                                    <label>Select End Time :</label>
                                                                                    <input  type="time"
                                                                                           class="form-control "
                                                                                           name="endtime"
                                                                                          />
                                                                                    <span class="form-text text-muted">cars</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="week-picker col-xl-4"
                                                                                 style="display: none">
                                                                                <div class="form-check">
                                                                                    <label>Select Start Week :</label>
                                                                                    <input type="week"
                                                                                           class="form-control "
                                                                                           name="startweek"
                                                                                           />
                                                                                    <span class="form-text text-muted">cars</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="week-picker col-xl-4"
                                                                                 style="display: none">
                                                                                <div class="form-check">
                                                                                    <label>Select End Week :</label>
                                                                                    <input type="week"
                                                                                           class="form-control "
                                                                                           name="endweek"
                                                                                    />
                                                                                    {{--                                                        <input type="text">--}}
                                                                                    <span class="form-text text-muted">cars</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="month-picker col-xl-4"
                                                                                 style="display: none">
                                                                                <div class="form-check">
                                                                                    <label>Select Month Start :</label>
                                                                                    <input type="month"
                                                                                           class="form-control "
                                                                                           name="monthstart"
                                                                                          />
                                                                                    {{--                                                        <input type="text">--}}
                                                                                    <span class="form-text text-muted">cars</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="month-picker col-xl-4"
                                                                                 style="display: none">
                                                                                <div class="form-check">
                                                                                    <label>Select Month End :</label>
                                                                                    <input type="month"
                                                                                           class="form-control "
                                                                                           name="endmonth"
                                                                                           />
                                                                                    <span class="form-text text-muted">cars</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-1 offset-2">
                                                                                <div class="form-group">
                                                                                    <label>Mon</label>
                                                                                    <div class="ml-2">
                                                                                        <input type="checkbox"
                                                                                               name="days">
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <div class="form-group">
                                                                                    <label>Tue</label>
                                                                                    <div class="ml-2">
                                                                                        <input type="checkbox"
                                                                                               name="days">
                                                                                        {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <div class="form-group">
                                                                                    <label>Wed</label>
                                                                                    <div class="ml-2">
                                                                                        <input type="checkbox"
                                                                                               name="days">
                                                                                        {{--                                                            <label class="custom-control-label"></label>--}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <div class="form-group">
                                                                                    <label>Thur</label>
                                                                                    <div class="ml-2">
                                                                                        <input type="checkbox"
                                                                                               name="days">
                                                                                        {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <div class="form-group">
                                                                                    <label>Fri</label>
                                                                                    <div class="">
                                                                                        <input type="checkbox"
                                                                                               name="days">
                                                                                        {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <div class="form-group">
                                                                                    <label>Sat</label>
                                                                                    <div class="ml-2">
                                                                                        <input type="checkbox"
                                                                                               name="days">
                                                                                        {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-sm-1">
                                                                                <div class="form-group">
                                                                                    <label>Sun</label>
                                                                                    <div class="ml-2">
                                                                                        <input type="checkbox"
                                                                                               name="days">
                                                                                        {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-lg-2 offset-10">
                                                                        <a href="javascript:;" data-repeater-delete=""
                                                                           class="btn font-weight-bold btn-sm w-75  float-right  btn-danger btn-icon">
                                                                            <i class="la la-trash"></i> delete
                                                                        </a>

                                                                    </div>
                                                                </div>
                                                            </div>
{{--                                                            <div class="row">--}}
                                                                <div class="col-lg-2 ml-2">
                                                                    <a data-repeater-create=""
                                                                       class="btn font-weight-bold btn-sm w-75  float-right  btn-success btn-icon">
                                                                        <i class="la la-plus"></i> Add
                                                                    </a>

                                                                </div>
{{--                                                            </div>--}}
                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row pb-3">
                                                <div class="col-xl-4 pb-6">
                                                    {{--                                                    <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">--}}
                                                    {{--                                                        <i class="la la-plus"></i>Add--}}
                                                    {{--                                                    </a>--}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div id="accordion">
                                                <div class="card ">
                                                    <div class="card-header p-2" id="headingOne">
                                                        <h5 class="mb-0 d-flex">
                                                           <span class="btn btn-link text-decoration-none border-0 bg-transparent" onclick="onClickback1();">
                                                                           <span
                                                                               class="d-flex">
                                                                               <i  class="fa fa-edit"
                                                                                   aria-hidden="true"></i>
                                                                           </span>
                                                            </span>


                                                            <span onclick="onClickAccord();"
                                                                    class="btn btn-link collapsed w-100 text-decoration-none "
                                                                    data-toggle="collapse"
                                                                    data-target="#collapseOne"
                                                                    aria-expanded="false"
                                                                    aria-controls="collapseOne">
                                                                           <span
                                                                               class="d-flex justify-content-between w-100">
                                                                               <span>Campaign Info</span>
                                                                               <i id="accord1" class="fa fa-angle-down"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                        </h5>
                                                    </div>

                                                    <div id="collapseOne" class="collapse "
                                                         aria-labelledby="headingOne"
                                                         data-parent="#accordion">
                                                        <div class="card-body p-4">
                                                                <div class="form-group row ">
                                                                    <div class=" col-sm-7">
                                                                        <b>Campaign Title</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>Titl name xyz </p>
                                                                    </div>
                                                                    <div class="  col-sm-7">
                                                                        <b>Campaign Type</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>Type1</p>
                                                                    </div>
                                                                    <div class="  col-sm-7">
                                                                        <b>Campaign Duration</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>12hrs</p>
                                                                    </div>
                                                                    <div class="  col-sm-7">
                                                                        <b>Campaign Start date</b>
                                                                    </div>
                                                                    <div class="  col-sm-5">
                                                                        <p>12hrs</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header p-2" id="headingTwo">
                                                        <h5 class="mb-0 d-flex">
                                                           <span class="btn btn-link text-decoration-none border-0 bg-transparent"  onclick="onClickback2();">
                                                                           <span
                                                                               class="d-flex">
                                                                               <i  class="fa fa-edit"
                                                                                   aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                            <span onclick="onClickAccord();"
                                                                    class="btn btn-link collapsed w-100 text-decoration-none "
                                                                    data-toggle="collapse"
                                                                    data-target="#collapseTwo"
                                                                    aria-expanded="false"
                                                                    aria-controls="collapseTwo">
                                                                           <span
                                                                               class="d-flex justify-content-between w-100">
                                                                               <span>Location Details</span>
                                                                               <i id="accord1" class="fa fa-angle-down"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                        </h5>
                                                    </div>

                                                    <div id="collapseTwo" class="collapse "
                                                         aria-labelledby="headingTwo"
                                                         data-parent="#accordion">
                                                        <div class="card-body p-4 d-flex">
                                                             <div id="googleMap2" class="w-100" style="height:270px;"></div>
{{--                                                            <img id="img-default"--}}
{{--                                                                 src="{{ asset('/media/cinqueterre.jpg') }}"--}}
{{--                                                                 class="img-rounded" alt="Cinque Terre"--}}
{{--                                                                 width="100%"--}}
{{--                                                                 height="236">--}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="card">
                                                    <div class="card-header p-2" id="headingThree">
                                                        <h5 class="mb-0 d-flex">
{{--                                                           <span class="btn btn-link text-decoration-none border-0 bg-transparent"  onclick="onClickback21();">--}}
{{--                                                                           <span--}}
{{--                                                                               class="d-flex">--}}
{{--                                                                               <i  class="fa fa-edit"--}}
{{--                                                                                   aria-hidden="true"></i>--}}
{{--                                                                           </span>--}}
{{--                                                            </span>--}}
                                                            <span onclick="onClickAccord();"
                                                                    class="btn btn-link collapsed w-100 text-decoration-none "
                                                                    data-toggle="collapse"
                                                                    data-target="#collapseThree"
                                                                    aria-expanded="false"
                                                                    aria-controls="collapseThree">
                                                                           <span
                                                                               class="d-flex justify-content-between w-100">
                                                                               <span>Car 1 Details</span>
                                                                               <i id="accord1" class="fa fa-angle-down"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                        </h5>
                                                    </div>

                                                    <div id="collapseThree" class="collapse "
                                                         aria-labelledby="headingThree"
                                                         data-parent="#accordion">
                                                        <div class="card-body p-4 d-flex">
                                                            <div class="form-group row  p-4  ">

                                                                <div class=" col-sm-5 " >
                                                                   <p>Car Make</p>
                                                                </div>
                                                                <div class="  col-sm-7" id="0">
                                                                </div>
                                                                <div class="  col-sm-5">
                                                                    <p> Car Type </p>
                                                                </div>
                                                                <div class="  col-sm-7">
                                                                </div>
                                                                <div class="  col-sm-5">
                                                                    <p>Year</p>
                                                                </div>
                                                                <div class="  col-sm-7"> </div>
                                                                <div class="  col-sm-5">
                                                                    <p>Miles</p>
                                                                </div>


                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

{{--                                            <div class="card">--}}
{{--                                                <h4 class="font-weight-bold text-dark text-center py-2"--}}
{{--                                                    style="background-color: #ebedf3;">Car 1 detail</h4>--}}
{{--                                                <div class="card-body">--}}
{{--                                                    <div class="form-group row ">--}}
{{--                                                        <div class=" col-sm-5 ">--}}
{{--                                                            Duration--}}
{{--                                                        </div>--}}
{{--                                                        <div class="  col-sm-7">--}}
{{--                                                            <p>10 days </p>--}}
{{--                                                            <p>01-11-2020 to 22-11-2020</p>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="  col-sm-5">--}}
{{--                                                            Costing--}}
{{--                                                        </div>--}}
{{--                                                        <div class="  col-sm-7">--}}
{{--                                                            <p>100 X 10= 1000</p>--}}
{{--                                                        </div>--}}
{{--                                                        <div class=" col-sm-7 offset-5">--}}
{{--                                                            <p>Total: 10000</p>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}



<div class="car"></div>

                                        </div>
                                    </div>
                                    <hr/>


                                    <div class="sticker-info row" style="display: none;">
                                        <div class="col-8">
                                            <form class="form" id="kt_form">
                                                <!--begin: Wizard Step 1-->
                                                <div class="card card-custom">
                                                    <div class="card-header">
                                                        <h3 class="card-title">
                                                            Sticker Section
                                                        </h3>
                                                    </div>
                                                    <div class="col-10  d-flex justify-content-end ">
                                                            <span class="switch switch-success switch-sm">
                                                                    <input type="checkbox" checked="checked"
                                                                           name="select"
                                                                           onchange="onChangeHandler(this);"/>
                                                            </span>
                                                    </div>
                                                    <div class="form-group mb-0 row align-items-center">

                                                        <div class="col-10 offset-2 d-flex justify-content-end">
                                                            <label class="col-form-label" id="switch-text">Click to request
                                                                sticker from admin? &nbsp</label>
                                                            <span class="switch switch-success mr-5 pr-4 switch-sm">

                                                                    <label>
                                                                        <input type="checkbox" checked="checked"
                                                                               name="select"
                                                                               onchange="onChangeHandler(this);"/>
                                                                        <span></span>
                                                                    </label>
                                                                </span>
                                                        </div>
                                                    </div>

                                                    <div class="card-body text_area1">
                                                        <textarea name="editor1"></textarea>
                                                        <button type="button" id="btn-preview" class="btn btn-primary mt-3">
                                                            Preview
                                                        </button>
                                                    </div>
                                                    <div class="card-body text_area2 d-none">
                                                        <div class="row">
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <label>Company:</label>
                                                                    <input type="text" class="form-control " name="l_title"
                                                                           placeholder="Write title here"/>
                                                                    <span class="form-text text-muted">Title of logo.</span>
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <label>Slogan:</label>
                                                                    <input type="text" class="form-control " name="l_title2"
                                                                           placeholder="Write sub title here"/>
                                                                    <span
                                                                        class="form-text text-muted">Sub title of logo.</span>
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <label>Contact</label>
                                                                    <input type="text" class="form-control " name="l_title2"
                                                                           placeholder="Contact "/>
                                                                    <span class="form-text text-muted">Contact</span>
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                            <div class="col-xl-6">
                                                                <!--begin::Input-->
                                                                <div class="form-group">
                                                                    <label>Sticker Details </label>
                                                                    <input type="text" class="form-control " name="l_title2"
                                                                           placeholder="Write Sticker Details here"/>
                                                                    <span
                                                                        class="form-text text-muted">Sticker Details</span>
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>
                                                            {{--                                                        <div class="col-xl-6">--}}
                                                            {{--                                                            <!--begin::Input-->--}}
                                                            {{--                                                            <div class="form-group">--}}
                                                            {{--                                                                <label>Upload Brand Image </label>--}}
                                                            {{--                                                                <input type="file" class="form-control " name="l_title2" placeholder="Write Sticker Details here"  />--}}
                                                            {{--                                                                <span class="form-text text-muted">Upload</span>--}}
                                                            {{--                                                            </div>--}}
                                                            {{--                                                            <!--end::Input-->--}}
                                                            {{--                                                        </div>--}}
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Upload Image</label>
                                                                    <div class="input-group">
                                                                    <span class="input-group-btn">
                                                                        <span class="btn btn-default btn-file">
                                                                            Browse… <input type="file" id="imgInp">
                                                                        </span>
                                                                    </span>
                                                                        <input type="text" class="form-control" readonly>
                                                                    </div>
                                                                    {{--                                                                <img id='img-upload'/>--}}
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-2  offset-10">
                                                                <!--begin::Input-->
                                                                <div class="form-group ">
                                                                    {{--                                                                <input type="submit"  value='Next' class="form-control bg-primary" />--}}
                                                                </div>
                                                                <!--end::Input-->
                                                            </div>


                                                        </div>


                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="col-4">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="card shadow  ">
                                                        <div class="card-body p-2   ">
                                                            <div id="accordion">
                                                                <div class="card ">
                                                                    <div class="card-header p-2" id="headingOne">
                                                                        <h5 class="mb-0 d-flex">
                                                           <span class="btn btn-link text-decoration-none border-0 bg-transparent" onclick="onClickback1();">
                                                                           <span
                                                                               class="d-flex">
                                                                               <i  class="fa fa-edit"
                                                                                   aria-hidden="true"></i>
                                                                           </span>
                                                            </span>


                                                                            <span onclick="onClickAccord();"
                                                                                  class="btn btn-link collapsed w-100 text-decoration-none "
                                                                                  data-toggle="collapse"
                                                                                  data-target="#collapseOne"
                                                                                  aria-expanded="false"
                                                                                  aria-controls="collapseOne">
                                                                           <span
                                                                               class="d-flex justify-content-between w-100">
                                                                               <span>Campaign Info</span>
                                                                               <i id="accord1" class="fa fa-angle-down"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                                        </h5>
                                                                    </div>

                                                                    <div id="collapseOne" class="collapse "
                                                                         aria-labelledby="headingOne"
                                                                         data-parent="#accordion">
                                                                        <div class="card-body p-4">
                                                                            <div class="form-group row ">
                                                                                <div class=" col-sm-7">
                                                                                    <b>Campaign Title</b>
                                                                                </div>
                                                                                <div class="  col-sm-5">
                                                                                    <p>Titl name xyz </p>
                                                                                </div>
                                                                                <div class="  col-sm-7">
                                                                                    <b>Campaign Type</b>
                                                                                </div>
                                                                                <div class="  col-sm-5">
                                                                                    <p>Type1</p>
                                                                                </div>
                                                                                <div class="  col-sm-7">
                                                                                    <b>Campaign Duration</b>
                                                                                </div>
                                                                                <div class="  col-sm-5">
                                                                                    <p>12hrs</p>
                                                                                </div>
                                                                                <div class="  col-sm-7">
                                                                                    <b>Campaign Start date</b>
                                                                                </div>
                                                                                <div class="  col-sm-5">
                                                                                    <p>12hrs</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-header p-2" id="headingTwo">
                                                                        <h5 class="mb-0 d-flex">
                                                           <span class="btn btn-link text-decoration-none border-0 bg-transparent"  onclick="onClickback2();">
                                                                           <span
                                                                               class="d-flex">
                                                                               <i  class="fa fa-edit"
                                                                                   aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                                            <span onclick="onClickAccord();"
                                                                                  class="btn btn-link collapsed w-100 text-decoration-none "
                                                                                  data-toggle="collapse"
                                                                                  data-target="#collapseTwo"
                                                                                  aria-expanded="false"
                                                                                  aria-controls="collapseTwo">
                                                                           <span
                                                                               class="d-flex justify-content-between w-100">
                                                                               <span>Location Details</span>
                                                                               <i id="accord1" class="fa fa-angle-down"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                                        </h5>
                                                                    </div>

                                                                    <div id="collapseTwo" class="collapse "
                                                                         aria-labelledby="headingTwo"
                                                                         data-parent="#accordion">
                                                                        <div class="card-body p-4 d-flex">
                                                                            <div id="googleMap2" class="w-100" style="height:270px;"></div>
                                                                            {{--                                                            <img id="img-default"--}}
                                                                            {{--                                                                 src="{{ asset('/media/cinqueterre.jpg') }}"--}}
                                                                            {{--                                                                 class="img-rounded" alt="Cinque Terre"--}}
                                                                            {{--                                                                 width="100%"--}}
                                                                            {{--                                                                 height="236">--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="card">
                                                                    <div class="card-header p-2" id="headingThree">
                                                                        <h5 class="mb-0 d-flex">
                                                                            {{--                                                           <span class="btn btn-link text-decoration-none border-0 bg-transparent"  onclick="onClickback21();">--}}
                                                                            {{--                                                                           <span--}}
                                                                            {{--                                                                               class="d-flex">--}}
                                                                            {{--                                                                               <i  class="fa fa-edit"--}}
                                                                            {{--                                                                                   aria-hidden="true"></i>--}}
                                                                            {{--                                                                           </span>--}}
                                                                            {{--                                                            </span>--}}
                                                                            <span onclick="onClickAccord();"
                                                                                  class="btn btn-link collapsed w-100 text-decoration-none "
                                                                                  data-toggle="collapse"
                                                                                  data-target="#collapseThree"
                                                                                  aria-expanded="false"
                                                                                  aria-controls="collapseThree">
                                                                           <span
                                                                               class="d-flex justify-content-between w-100">
                                                                               <span>Car 1 Details</span>
                                                                               <i id="accord1" class="fa fa-angle-down"
                                                                                  aria-hidden="true"></i>
                                                                           </span>
                                                            </span>
                                                                        </h5>
                                                                    </div>

                                                                    <div id="collapseThree" class="collapse "
                                                                         aria-labelledby="headingThree"
                                                                         data-parent="#accordion">
                                                                        <div class="card-body p-4 d-flex">
                                                                            <div class="form-group row  p-4  ">

                                                                                <div class=" col-sm-5 " >
                                                                                    <p>Car Make</p>
                                                                                </div>
                                                                                <div class="  col-sm-7" id="0">
                                                                                </div>
                                                                                <div class="  col-sm-5">
                                                                                    <p> Car Type </p>
                                                                                </div>
                                                                                <div class="  col-sm-7">
                                                                                </div>
                                                                                <div class="  col-sm-5">
                                                                                    <p>Year</p>
                                                                                </div>
                                                                                <div class="  col-sm-7"> </div>
                                                                                <div class="  col-sm-5">
                                                                                    <p>Miles</p>
                                                                                </div>


                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            {{--                                                            <div id="accordion">--}}
{{--                                                                <div class="card">--}}
{{--                                                                    <div class="card-header p-2" id="headingOne">--}}
{{--                                                                        <h5 class="mb-0">--}}
{{--                                                                            <span onclick="onClickAccord();"--}}
{{--                                                                                    class="btn btn-link collapsed w-100 text-decoration-none "--}}
{{--                                                                                    data-toggle="collapse"--}}
{{--                                                                                    data-target="#collapseOne"--}}
{{--                                                                                    aria-expanded="false"--}}
{{--                                                                                    aria-controls="collapseOne">--}}
{{--                                                                           <span--}}
{{--                                                                               class="d-flex justify-content-between w-100">--}}
{{--                                                                               <span>Campaign Info</span>--}}
{{--                                                                               <i id="accord1" class="fa fa-angle-down"--}}
{{--                                                                                  aria-hidden="true"></i>--}}
{{--                                                                           </span>--}}
{{--                                                                            </span>--}}
{{--                                                                        </h5>--}}
{{--                                                                    </div>--}}

{{--                                                                    <div id="collapseOne" class="collapse"--}}
{{--                                                                         aria-labelledby="headingOne"--}}
{{--                                                                         data-parent="#accordion">--}}
{{--                                                                        <div class="card-body p-4">--}}
{{--                                                                            --}}{{--                                                                                                                                            <h2 class=" py-2 text-center bg-light ">Campaign Detail</h2>--}}
{{--                                                                            <div class="form-group row ">--}}
{{--                                                                                <div class=" col-sm-7">--}}
{{--                                                                                    <b>Campaign Title</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>Titl name xyz </p>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-7">--}}
{{--                                                                                    <b>Campaign Type</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>Type1</p>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-7">--}}
{{--                                                                                    <b>Campaign Duration</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>7 days </p>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-7">--}}
{{--                                                                                    <b>Campaign Start date</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>01-11-2020</p>--}}
{{--                                                                                </div>--}}
{{--                                                                            </div>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}


{{--                                                                <div class="card">--}}
{{--                                                                    <div class="card-header p-2" id="headingTwo">--}}
{{--                                                                        <h5 class="mb-0 d-flex">--}}

{{--                                                                        <span onclick="onClickAccord2();"--}}
{{--                                                                              class="btn btn-link collapsed w-100 text-decoration-none "--}}
{{--                                                                              data-toggle="collapse"--}}
{{--                                                                              data-target="#collapseTwo"--}}
{{--                                                                              aria-expanded="false"--}}
{{--                                                                              aria-controls="collapseTwo">--}}
{{--                                                                           <span--}}
{{--                                                                               class="d-flex justify-content-between w-100">--}}
{{--                                                                               <span>Location Details</span>--}}
{{--                                                                               <i id="accord2" class="fa fa-angle-down"--}}
{{--                                                                                  aria-hidden="true"></i>--}}
{{--                                                                           </span>--}}
{{--                                                            </span>--}}
{{--                                                                        </h5>--}}
{{--                                                                    </div>--}}

{{--                                                                    <div id="collapseTwo" class="collapse "--}}
{{--                                                                         aria-labelledby="headingTwo"--}}
{{--                                                                         data-parent="#accordion">--}}
{{--                                                                        <div class="card-body p-4 d-flex">--}}
{{--                                                                            <div id="googleMap" class="w-100" style="height:270px;"></div>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                                <div class="card">--}}
{{--                                                                    <div class="card-header p-3" id="headingThree">--}}
{{--                                                                        <h5 class="mb-0">--}}
{{--                                                                            <span--}}
{{--                                                                                class=" w-100 text-decoration-none  btn btn-link collapsed"--}}
{{--                                                                                data-toggle="collapse"--}}
{{--                                                                                data-target="#collapseThree"--}}
{{--                                                                                aria-expanded="false"--}}
{{--                                                                                aria-controls="collapseThree">--}}
{{--                                                                          <span onclick="onClickAccord3();"--}}
{{--                                                                                class="d-flex justify-content-between w-100">--}}
{{--                                                                               <span>Cars Detail</span>--}}
{{--                                                                               <i id="accord3" class="fa fa-angle-down"--}}
{{--                                                                                  aria-hidden="true"></i>--}}
{{--                                                                           </span>--}}
{{--                                                                            </span>--}}
{{--                                                                        </h5>--}}
{{--                                                                    </div>--}}
{{--                                                                    <div id="collapseThree" class="collapse"--}}
{{--                                                                         aria-labelledby="headingThree"--}}
{{--                                                                         data-parent="#accordion">--}}
{{--                                                                        <div class="card-body">--}}

{{--                                                                            <div class="form-group row ">--}}
{{--                                                                                <h2 class="text-center">Car1 Details</h2>--}}


{{--                                                                                <div class=" col-sm-7">--}}
{{--                                                                                    <b>Car Make</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>Hyundai </p>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-7">--}}
{{--                                                                                    <b>Car Type</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>Mehran</p>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-7">--}}
{{--                                                                                    <b>Car Model</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>2020</p>--}}
{{--                                                                                </div>--}}
{{--                                                                            </div>--}}


{{--                                                                            <div class="form-group row ">--}}
{{--                                                                                <h2 class="text-center">Car2 Details</h2>--}}

{{--                                                                                <div class=" col-sm-7">--}}
{{--                                                                                    <b>Car Make</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>Toyota </p>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-7">--}}
{{--                                                                                    <b>Car Type</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>Hyundai</p>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-7">--}}
{{--                                                                                    <b>Car Model</b>--}}
{{--                                                                                </div>--}}
{{--                                                                                <div class="  col-sm-5">--}}
{{--                                                                                    <p>2020</p>--}}
{{--                                                                                </div>--}}
{{--                                                                            </div>--}}
{{--                                                                        </div>--}}
{{--                                                                    </div>--}}
{{--                                                                </div>--}}
{{--                                                            </div>--}}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="card shadow  " style="">
                                                        <div class="card-body p-2   ">
                                                            <h2 class=" py-2 text-center bg-light ">Preview </h2>
                                                            <div class="form-group row ">
                                                                <div class="col-md-12">
                                                                    <img id="img-default"
                                                                         src="{{ asset('/media/logos/aa.jpg') }}"
                                                                         class="img-rounded d-none" alt="Cinque Terre"
                                                                         width="100%"
                                                                         height="236">
                                                                    <img id="img-upload">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>
                                        <!--end: Wizard-->
                                    </div>
                                </form>
                            </div>
                            <!--end: Wizard body -->
                        </div>
                    </div>
                    <!--end: Wizard-->
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-2 offset-1 ">
        </div>
        <div class="col-2  offset-6 ">
            <input id="next-div" onclick="process_1();" type="button" value="Next1" name="Next"
                   class=" float-right btn btn-sm  font-weight-bold btn-success w-100 btn-shadow ">
            <input id="next-div2" style="display: none" onclick="process_2();" type="button" value="Next2" name="Next"
                   class=" float-right btn btn-sm  font-weight-bold btn-success w-100 btn-shadow ">
            <input id="next-div3" style="display: none" onclick="process_3();" type="button" value="Next3" name="Next"
                   class=" float-right btn btn-sm  font-weight-bold btn-success w-100 btn-shadow ">
{{--            <input id="next-div4" style="display: none" type="submit"--}}
{{--                   onclick="window.location.href='{{url('/Advertiser/Dashboard/create-campaign-step2')}}';" value="Next"--}}
{{--                   name="submit" class=" float-right btn btn-sm  font-weight-bold btn-success w-50 btn-shadow ">--}}


            <a  id="next-div4"  href="javascript:void(0);" data-toggle="modal" data-target="#viewModal"
               class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
               title="edit details">
                <i class="la la-eye"></i>
            </a>
        </div>
    </div>

    <div class="modal fade" id="modalCart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <!--Header-->
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Your Campaigns</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!--Body-->
                <div class="modal-body">

                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Campaign name</th>
                            <th>Location</th>
                            <th>Cost</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Campgin 1</td>
                            <td>Taxsas USA</td>
                            <td>100$</td>
                            {{--                            <td><a><i class="fas fa-times"></i></a></td>--}}
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Campaign 2</td>
                            <td>California USA</td>
                            <td>200$</td>
                            {{--                            <td><a><i class="fas fa-times"></i></a></td>--}}
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Campaign 3</td>
                            <td>New York USA</td>
                            <td>400$</td>
                        </tr>
                        {{--                        <tr>--}}
                        {{--                            <th scope="row">4</th>--}}
                        {{--                            <td>Product 4</td>--}}
                        {{--                            <td>100$</td>--}}
                        {{--                            <td><a><i class="fas fa-times"></i></a></td>--}}
                        {{--                        </tr>--}}
                        <tr class="total">
                            <th scope="row"></th>
                            <td>Total</td>
                            <td></td>
                            <td>$700</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
                <!--Footer-->
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary">Checkout</button>
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="checkout" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button class="btn btn-light-primary font-weight-bold text-right" style="margin-left: 267px;"><i
                            class="fa fa-download" aria-hidden="true"></i>download
                    </button>
                    <button class="btn btn-light-primary font-weight-bold" style="margin-left: -68px;"><i
                            class="fa fa-print" aria-hidden="true"></i>print
                    </button>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center py-10 px-8 px-md-0">
                            <div class="col-md-10">
                                <!-- begin: Invoice header-->
                                <div
                                    class="d-flex justify-content-between pb-5 pb-md-10 flex-column flex-md-row">
                                    <h2 class="display-4 font-weight-boldest mb-10">Driver details</h2>
                                    {{--                                            <span>Duration : 20m </span>--}}

                                    <div class="d-flex flex-column align-items-md-end px-0">
                                        <!--begin::Logo-->
                                        <a href="#" class="mb-5 max-w-200px">
                                            <span class="svg-icon svg-icon-full">
                                                <!--begin::Svg Icon | path:assets/media/svg/logos/duolingo.svg-->
                                                <svg xmlns="http://www.w3.org/2000/svg" width="113"
                                                     height="31" viewBox="0 0 113 31" fill="none">
                                                    <path
                                                        d="M26.6802 13.33V13.485C26.6802 14.9575 26.6087 15.8875 26.4659 16.275C26.2517 16.895 25.966 17.36 25.4661 17.67C25.0376 17.98 24.5377 18.1358 23.8949 18.1358C23.2529 18.1358 22.7523 17.9808 22.3245 17.67C21.8953 17.3608 21.6103 16.895 21.3954 16.275C21.1811 15.7325 21.1104 14.8025 21.1104 13.4075V5.425H17.3254V13.485C17.3968 15.345 17.5396 16.6625 17.7539 17.515C18.1824 18.9108 18.8965 19.995 19.8963 20.8483C20.8962 21.6233 22.2531 22.0108 23.8242 22.0108C25.324 22.0108 26.538 21.7008 27.5379 21.0025C28.537 20.3058 29.2511 19.375 29.7518 18.0575C30.1088 17.205 30.3231 15.655 30.3231 13.485V5.425H26.6087V13.33H26.6802ZM15.6107 0H11.897V6.2775C11.8256 6.2775 11.7542 6.2 11.6828 6.2C10.4687 5.425 9.11182 5.0375 7.75492 5.0375C5.82669 5.0375 4.1127 5.735 2.61297 7.2075C0.898984 8.9125 0.0419922 11.005 0.0419922 13.485C0.0419922 15.81 0.827568 17.825 2.3273 19.4525C3.82704 21.0808 5.68314 21.9325 7.75492 21.9325C9.18324 21.9325 10.5401 21.545 11.7542 20.8483C12.9683 20.0733 13.8967 19.065 14.6109 17.7483C15.2536 16.5858 15.54 15.345 15.6107 14.0283V13.4858V0ZM10.6823 16.895C9.89668 17.7483 8.96899 18.2125 7.7542 18.2125C6.61155 18.2125 5.61172 17.7475 4.89756 16.895C4.11199 16.0425 3.75491 14.88 3.75491 13.485C3.75491 12.1675 4.11199 11.005 4.89756 10.1525C5.68314 9.3 6.61155 8.835 7.7542 8.835C8.96828 8.835 9.89668 9.3 10.6823 10.1525C11.4685 11.005 11.8256 12.1675 11.8256 13.485C11.8256 14.88 11.4685 16.0425 10.6823 16.895ZM46.2481 9.3C45.534 7.9825 44.6049 6.975 43.3915 6.2C42.1774 5.425 40.8205 5.0375 39.4636 5.0375C37.5354 5.0375 35.8207 5.735 34.3217 7.2075C32.6077 8.9125 31.75 10.9275 31.75 13.485C31.75 15.81 32.5363 17.825 34.0353 19.4525C35.5357 21.0808 37.3918 21.9325 39.4636 21.9325C40.8919 21.9325 42.2481 21.545 43.4629 20.8483C44.677 20.0733 45.6054 19.065 46.3195 17.7483C47.0337 16.4308 47.3908 15.0358 47.3908 13.4858C47.248 12.0125 46.9623 10.6175 46.2481 9.3ZM42.3917 16.895C41.6061 17.7483 40.6777 18.2125 39.4636 18.2125C38.2495 18.2125 37.3211 17.7475 36.607 16.895C35.8214 16.0425 35.4643 14.88 35.4643 13.485C35.4643 12.1675 35.8214 11.005 36.6077 10.1525C37.3933 9.3 38.321 8.835 39.4643 8.835C40.6777 8.835 41.6068 9.3 42.3924 10.1525C43.178 11.005 43.535 12.1675 43.535 13.485C43.5336 14.88 43.1058 16.0425 42.3917 16.895ZM60.2457 21.7H64.0307V6.1225H60.2457V21.7ZM53.8182 17.515C53.3897 17.2058 53.0326 16.74 52.8898 16.12C52.747 15.655 52.6756 14.725 52.6756 13.33V0H48.9619V13.2525C49.0334 15.4225 49.1762 16.895 49.5333 17.825C50.0332 19.065 50.7473 20.0725 51.7472 20.77C52.747 21.4675 53.9618 21.7775 55.4608 21.7775H58.3174V17.98H55.3894C54.7466 17.98 54.246 17.825 53.8182 17.515ZM78.7424 9.145C78.2425 7.905 77.5283 6.8975 76.5278 6.2C75.5287 5.5025 74.3139 5.1925 72.8142 5.1925C71.1716 5.1925 69.8861 5.58 68.8863 6.355C67.8872 7.13 67.173 8.215 66.7438 9.6875C66.5296 10.4625 66.3867 11.8575 66.3153 13.7175V21.7H70.1003V13.95V13.64C70.1003 12.3225 70.1725 11.315 70.386 10.7725C70.6003 10.1525 70.8859 9.765 71.3144 9.3775C71.7429 9.0675 72.2428 8.9125 72.8856 8.9125C73.5276 8.9125 74.0282 9.0675 74.4567 9.3775C74.8852 9.6875 75.2423 10.1525 75.4558 10.7725C75.5987 11.2375 75.6701 12.1675 75.6701 13.5625V21.7H79.3837V14.88V13.64C79.3137 11.5475 79.0995 9.9975 78.7424 9.145ZM62.0311 0.31C60.9598 0.31 60.0314 1.3175 60.0314 2.48C60.0314 3.6425 60.8884 4.65 62.0311 4.65C63.1023 4.65 64.0307 3.72 64.0307 2.48C64.0307 1.24 63.1023 0.31 62.0311 0.31ZM111.951 9.455C111.237 8.1375 110.308 7.13 109.094 6.355C107.88 5.58 106.595 5.27 105.094 5.27C102.952 5.27 101.166 6.1225 99.6666 7.75C98.1661 9.3775 97.3813 11.3925 97.3813 13.7175C97.3813 16.1975 98.2383 18.2892 99.9522 19.995C101.453 21.4675 103.166 22.165 105.094 22.165C106.522 22.165 107.808 21.7775 109.022 21.0025C110.236 20.2275 111.237 19.22 111.879 17.9025C112.593 16.5858 112.95 15.19 112.95 13.7183C113.022 12.1675 112.665 10.7725 111.951 9.455ZM109.236 13.7175C109.236 15.1125 108.879 16.1975 108.094 17.05C107.308 17.9025 106.38 18.3675 105.166 18.3675C104.022 18.3675 103.023 17.9017 102.309 17.05C101.523 16.1975 101.166 15.035 101.166 13.7175C101.166 12.3225 101.523 11.16 102.309 10.3075C103.095 9.455 104.022 8.99 105.166 8.99C106.308 8.99 107.308 9.455 108.094 10.3075C108.879 11.16 109.236 12.3225 109.236 13.7175C109.236 13.64 109.236 13.64 109.236 13.7175ZM96.0965 13.02C96.0244 11.7025 95.6673 10.54 95.0967 9.3775C94.3825 8.06 93.4541 7.0525 92.3122 6.2775C91.0981 5.5025 89.8126 5.1925 88.3843 5.1925C86.2418 5.1925 84.4564 5.9675 82.9559 7.6725C81.4562 9.3 80.7421 11.315 80.7421 13.64C80.7421 16.12 81.599 18.2125 83.2416 19.84C84.7413 21.3125 86.3832 22.0108 88.3121 22.0108C89.669 22.0108 91.026 21.6233 92.24 20.8483C92.3122 20.77 92.3829 20.77 92.4543 20.6933V22.5525C92.4543 23.9483 92.0258 25.0325 91.3116 25.8858C90.526 26.7383 89.5976 27.2025 88.455 27.2025C87.3123 27.2025 86.3839 26.815 85.5983 25.9625L83.1702 28.7525L83.2416 28.83C84.7413 30.3017 86.4553 31 88.3121 31C89.669 31 91.026 30.6125 92.24 29.8375C93.4541 29.0625 94.3825 28.055 95.0967 26.7375C95.8108 25.42 96.0965 24.025 96.0965 22.5525V14.105V13.5625C96.1679 13.4075 96.1679 13.175 96.0965 13.02ZM92.4536 13.5625C92.4536 14.9575 92.0965 16.0425 91.3109 16.895C90.5253 17.7483 89.5969 18.2125 88.4543 18.2125C87.3116 18.2125 86.3832 17.7475 85.5976 16.895C84.812 16.0425 84.455 14.9575 84.455 13.5625C84.455 12.1675 84.812 11.0825 85.5976 10.1525C86.3832 9.3 87.3116 8.835 88.4543 8.835C89.5969 8.835 90.5967 9.3 91.3109 10.1525C92.0258 11.0825 92.4536 12.1675 92.4536 13.5625Z"
                                                        fill="#1CB0F6"></path>
                                                </svg>
                                                <!--end::Svg Icon-->
                                            </span>
                                        </a>
                                        <!--end::Logo-->
                                        <span
                                            class="d-flex flex-column text-center align-items-md-end font-size-h6 font-weight-bold text-muted">
{{--															<span class="text-center">Start Campaign  </span>--}}
                                            {{--															<span>12 May 2020  </span>--}}

                                        </span>
                                    </div>
                                </div>

                                <div class="row font-size-lg mb-3">
                                    <span class="col-md-2 font-weight-bold pr-1">First Name:</span>
                                    <span class="col-md-4 text-right ">Joe
{{--                                                    	<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        </span>--}}
                                    </span>
                                    <span class="col-md-2 font-weight-bold pl-3 pr-1">Last Name:</span>
                                    <span class="col-md-4 text-right ">Doe</span>
                                </div>

                                <div class="row font-size-lg mb-3">
                                    <span class="col-md-2 font-weight-bold pr-1">Status:</span>
                                    <span class="col-md-4 text-right ">Activated</span>
                                    <span class="col-md-2 font-weight-bold pl-3 pr-1"></span>
                                    <span class="col-md-4 text-right "></span>
                                </div>
                            </div>

                        </div>

                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left text-decoration-none"
                                                type="button" data-toggle="collapse" data-target="#collapseOne"
                                                aria-expanded="true" aria-controls="collapseOne">Car Info
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="d-flex flex-column w-100">
                                            <div class="d-flex flex-column w-100">
                                                <div
                                                    class=" font-size-h6 pb-5 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                    Vehicle Info
                                                </div>
                                                <div class="d-flex flex-column mb-10 mb-md-0 ml-2  ">
                                                    <div class="d-flex justify-content-between font-size-lg mb-3">
                                                        <span class="font-weight-bold mr-15">Vehicle Type:</span>
                                                        <span class="text-right">Car</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-3">
                                                        <span class="font-weight-bold mr-15">Vehicle Make:</span>
                                                        <span class="text-right ">Honda</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg">
                                                        <span class="font-weight-bold mr-15">Vehicle Model:</span>
                                                        <span class="text-right">2020</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="border-bottom  my-8"></div>

                                            <div class="d-flex flex-column w-100">
                                                <div
                                                    class=" font-size-h6 pb-5 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                    Car Pictures
                                                </div>
                                                <div class="d-flex flex-column mb-10 mb-md-0 ml-2  ">
                                                    <div class="p-2 d-flex align-items-end">
                                                        <span class="pr-1" style="max-width:190px">
                                                            <img class="img-fluid" alt=""
                                                                 src="https://upload.wikimedia.org/wikipedia/commons/a/a4/2016_Toyota_Corolla_%28ZRE172R%29_Ascent_sedan_%282017-01-15%29_01.jpg"/>
                                                        </span>
                                                        <span style="max-width:190px">
                                                            <img class="img-fluid" alt=""
                                                                 src="https://upload.wikimedia.org/wikipedia/commons/2/25/2018_Toyota_Corolla_%28ZRE172R%29_Ascent_sedan_%282018-11-02%29_01.jpg"/>
                                                        </span>
                                                    </div>
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                                    {{--                                                            <span class="text-right">Pic1</span>--}}
                                                    {{--                                                        </div>--}}

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed text-decoration-none"
                                                type="button" data-toggle="collapse" data-target="#collapseTwo"
                                                aria-expanded="false" aria-controls="collapseTwo">Drive a car location
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="py-2 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        Country
                                                    </th>
                                                    <th class="py-2 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        State
                                                    </th>

                                                    <th class="py-2 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        City
                                                    </th>
                                                    <th class="py-2 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        Zip
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class=" font-size-lg">
                                                    <td class="border-top-0 pl-0 py-3 d-flex align-items-center">
                                                        England
                                                    </td>
                                                    <td class="pr-0 py-3 font-size-h6  text-right">
                                                        London
                                                    </td>
                                                    <td class="text-right py-3">Ohio</td>
                                                    <td class="text-right py-3">564</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed text-decoration-none"
                                                type="button" data-toggle="collapse" data-target="#collapseThree"
                                                aria-expanded="false" aria-controls="collapseThree">Uploaded Documents
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="document-uploaded d-flex flex-column mb-10 mb-md-0 ml-2  ">
                                            <div class="d-flex flex-column font-size-lg mb-3">
                                                <span class="font-weight-bold mb-5">Driving License </span>
                                                <span class="">
                                                    <span class="pr-1" style="max-width:190px">
                                                        <img class="img-fluid" alt=""
                                                             src="https://upload.wikimedia.org/wikipedia/commons/a/a4/2016_Toyota_Corolla_%28ZRE172R%29_Ascent_sedan_%282017-01-15%29_01.jpg"/>
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="d-flex flex-column font-size-lg mb-3">
                                                <span class="font-weight-bold mb-5">Government CNIC/Passport</span>
                                                <span class=" ">
                                                    <span class="pr-1" style="max-width:190px">
                                                        <img class="img-fluid" alt=""
                                                             src="https://upload.wikimedia.org/wikipedia/commons/a/a4/2016_Toyota_Corolla_%28ZRE172R%29_Ascent_sedan_%282017-01-15%29_01.jpg"/>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed text-decoration-none"
                                                type="button"
                                                data-toggle="collapse" data-target="#collapseFour"
                                                aria-expanded="false" aria-controls="collapseFour">
                                            Become a Driver
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="d-flex flex-column mb-10 mb-md-0 ml-2  ">
                                            <div class="d-flex justify-content-between font-size-lg mb-3">
                                                <span
                                                    class="font-weight-bold mr-15">How many hours drive a car? :</span>
                                                <span class="text-right">Weekly</span>
                                            </div>
                                            <div class="d-flex justify-content-between font-size-lg mb-3">
                                                <span class="font-weight-bold mr-15">How often drive a car?:</span>
                                                <span class="text-right ">Daily</span>
                                            </div>
                                            <div class="d-flex justify-content-between font-size-lg">
                                                <span class="font-weight-bold pr-5"> How often mileage a car? :</span>
                                                <span class="text-right">Monthly</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

@endsection
{{-- Scripts Section --}}
@section('scripts')
    <script>


      function  showValues(){
// console.log('dfdfd');
          // console.log($('#kt_repeater_3').repeaterVal());
         var car_info=$('#kt_repeater_3').repeaterVal();
         var val= car_info.car_info[0]["car_make"];
          $('#0').text(val);
console.log(val);
        }

        var KTFormRepeater = function () {

            // Private functions
            var demo3 = function () {
                $('#kt_repeater_3').repeater({
                    initEmpty: false,

                    defaultValues: {
                        'text-input': 'foo'
                    },

                    show: function () {
                        $(this).slideDown(1000);
                        // console.log(this);
                        console.log($('#kt_repeater_3').repeaterVal());
                        var m = $('#kt_repeater_3').repeaterVal();
                        var n= m.car_info.length;
                        $(".car").append(
						"  <br><div class='card' id="+n+">\n" +
                            "              <h4 class='font-weight-bold text-dark text-center py-2'\n" +
                            "             style='background-color: #ebedf3;'>Car "+n+" Detail</h4>\n" +
                            "             <div class='card-body'>\n" +
                            "             <div class='form-group row  p-4  '>"+
                            "             <div class=' col-sm-5' >"+
                            "             <p>Car Make</p>"+
                            "             </div>"+
                            "             <div class='  col-sm-7' id='0'>"+
                            "             </div>"+
                            "             <div class=' col-sm-5'>"+
                            "              <p> Car Type </p>"+
                            "             </div>"+
                            "             <div class=' col-sm-7'>"+
                            "               </div>"+
                            "                <div class='col-sm-5'>"+
                            "                 <p>Year</p>"+
                            "                 </div>"+
                            "                  <div class='  col-sm-7'> </div>"+
                            "                 <p>Miles</p>"+
                            "                </div>"+












                                                            "</div>\n" +
                            "              </div>\n" +
                            "             </div>" );
                    },
                    isFirstItemUndeletable: true,
                    hide: function (deleteElement) {
                        // if(confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                        // }
                    }
                });
            };

            return {
                // public functions
                init: function () {
                    demo3();
                }
            };
        }();

        jQuery(document).ready(function () {
            KTFormRepeater.init();
        });
    </script>
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/wizard/wizard-3.js') }}" type="text/javascript"></script>
    {{--    <script src="assets/js/pages/custom/wizard/wizard-3.js"></script>--}}
    <script>

    </script>


    <script>

        function yesnoCheck(that) {
            if (that.value == "d") {
                // alert("check");
                $('.week-picker').hide();
                $('.day-picker').show();
                $('.month-picker').hide();

            } else if (that.value == "w") {
                // alert("check");
                $('.week-picker').show();
                $('.day-picker').hide();
                $('.month-picker').hide();


            } else if (that.value == "m") {
                // alert("check");
                $('.week-picker').hide();
                $('.day-picker').hide();
                $('.month-picker').show();

            } else {

            }
        }

        function process_1() {
            // alert("check");
            $('.camp').hide(1000);
            $('.sticker-info').hide(1000);
            $('.location_info').show(1000);
            $('#next-div').hide();
            $('#next-div2').show();

        }
        function onClickback() {
            // alert("check");
            $('.camp').show(1000);
            $('.location_info').hide(1000);
            $('.sticker-info').hide(1000);
            $('#next-div').show();
            $('#next-div2').hide();
            // $('#next-div3').hide();

        }
        function onClickback1() {
            // alert("check");
            $('.camp').show(1000);
            $('.location_info').hide(1000);
            $('.sticker-info').hide(1000);
            $('#next-div').show();
            $('#next-div2').hide();
            $('#next-div3').hide();
            $('.car-info').hide(1000)

        }   function onClickback2() {
            // alert("check");
            $('.camp').hide(1000);
            $('.location_info').show(1000);
            $('.sticker-info').hide(1000);
            $('#next-div').hide();
            $('#next-div2').show();
            $('#next-div3').hide();
            $('.car-info').hide(1000)

        }

        function process_2() {
            // alert("check");
            $('.location_info').hide(1000);
            $('.car-info').show(1000);
            $('.sticker-info').hide(1000);
            $('#next-div2').hide();
            $('#next-div3').show();

        }function process_3() {
            // alert("check");
            $('.location_info').hide(1000);
            $('.car-info').hide(1000);
            $('#next-div2').hide();
            $('#next-div3').show();
            $('.camp').hide(1000);
            $('.sticker-info').show(1000);

        }

        showdelete = function () {
            console.log('here');

        };

        function myMap() {
            var myLatLng = {lat: 51.50874, lng: -0.120850};
            var mapProp = {
                center: new google.maps.LatLng(51.508742, -0.120850),
                zoom: 5,
                zoomControl: false,
                mapTypeControl: false,

            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
            var map2 = new google.maps.Map(document.getElementById("googleMap2"), mapProp);
            var marker2 = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }

        function onClickAccord() {
            if ($("#accord1").hasClass("fa-angle-down")) {
                // alert($(this));

                $("#accord1").removeClass("fa-angle-down");
                $("#accord1").addClass("fa-angle-up");
            } else {

                $("#accord1").removeClass("fa-angle-up");
                $("#accord1").addClass("fa-angle-down");
            }
        }

        $(document).ready(function () {
            $("#datepicker").datepicker();
            $("#datepicker2").datepicker();
            $('.select2').select2();
        });
    </script>
    <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor1');
    </script>

    <script>
        function onClickAccord() {
            if ($("#accord1").hasClass("fa-angle-down")) {
                // alert($(this));

                $("#accord1").removeClass("fa-angle-down");
                $("#accord1").addClass("fa-angle-up");
            } else {

                $("#accord1").removeClass("fa-angle-up");
                $("#accord1").addClass("fa-angle-down");
            }
            // $( "#accord1" ).hasClass("fa-angle-down");

            // if(cb.checked==true) {
            //     $( ".text_area1" ).removeClass( "d-none" );
            //     $( ".text_area2" ).addClass( "d-none" );
            //     $( "#switch-text" ).html( "Click to request sticker from admin &nbsp" );
            //
            //     // alert("Clicked, new value = " + cb.checked);
            // }
            // else{
            //     $( "#switch-text" ).html( "Click to create your own sticker     &nbsp   " );
            //     $( ".text_area1" ).addClass( "d-none" );
            //     $( ".text_area2" ).removeClass( "d-none" );
            //     // alert("false");
            // }
        }

        function onClickAccord2() {
            if ($("#accord2").hasClass("fa-angle-down")) {
                // alert($(this));

                $("#accord2").removeClass("fa-angle-down");
                $("#accord2").addClass("fa-angle-up");
            } else {

                $("#accord2").removeClass("fa-angle-up");
                $("#accord2").addClass("fa-angle-down");
            }
            // $( "#accord1" ).hasClass("fa-angle-down");

            // if(cb.checked==true) {
            //     $( ".text_area1" ).removeClass( "d-none" );
            //     $( ".text_area2" ).addClass( "d-none" );
            //     $( "#switch-text" ).html( "Click to request sticker from admin &nbsp" );
            //
            //     // alert("Clicked, new value = " + cb.checked);
            // }
            // else{
            //     $( "#switch-text" ).html( "Click to create your own sticker     &nbsp   " );
            //     $( ".text_area1" ).addClass( "d-none" );
            //     $( ".text_area2" ).removeClass( "d-none" );
            //     // alert("false");
            // }
        }

        function onClickAccord3() {
            if ($("#accord3").hasClass("fa-angle-down")) {
                // alert($(this));

                $("#accord3").removeClass("fa-angle-down");
                $("#accord3").addClass("fa-angle-up");
            } else {

                $("#accord3").removeClass("fa-angle-up");
                $("#accord3").addClass("fa-angle-down");
            }
            // $( "#accord1" ).hasClass("fa-angle-down");

            // if(cb.checked==true) {
            //     $( ".text_area1" ).removeClass( "d-none" );
            //     $( ".text_area2" ).addClass( "d-none" );
            //     $( "#switch-text" ).html( "Click to request sticker from admin &nbsp" );
            //
            //     // alert("Clicked, new value = " + cb.checked);
            // }
            // else{
            //     $( "#switch-text" ).html( "Click to create your own sticker     &nbsp   " );
            //     $( ".text_area1" ).addClass( "d-none" );
            //     $( ".text_area2" ).removeClass( "d-none" );
            //     // alert("false");
            // }
        }

        function onChangeHandler(cb) {

            if (cb.checked == true) {
                $(".text_area1").removeClass("d-none");
                $(".text_area2").addClass("d-none");
                $("#img-upload").addClass("d-none");
                $("#switch-text").html("Click to request sticker from admin &nbsp");

                // alert("Clicked, new value = " + cb.checked);
            } else {
                $("#switch-text").html("Click to create your own sticker     &nbsp   ");
                $(".text_area1").addClass("d-none");
                $("#img-upload").removeClass("d-none");
                $(".text_area2").removeClass("d-none");
                $("#img-default").addClass("d-none");
                // $("#img-default").addClass("d-none");
                // alert("false");
            }
        }

        $("#btn-preview").on('click', function () {
            if ($("#img-default").hasClass('d-none')) {
                $("#img-default").removeClass('d-none');
            }
        });
        function myMap() {
            var myLatLng = {lat: 51.50874, lng: -0.120850};
            var mapProp = {
                center: new google.maps.LatLng(51.508742, -0.120850),
                zoom: 5,
                zoomControl: false,
                mapTypeControl: false,

            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }

        $(document).ready(function () {
            $("#datepicker").datepicker();
            $('#select2').select2();
        });

    </script>
    <script>
        $(document).ready(function () {
            $(document).on('change', '.btn-file :file', function () {
                var input = $(this),
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [label]);
            });

            $('.btn-file :file').on('fileselect', function (event, label) {

                var input = $(this).parents('.input-group').find(':text'),
                    log = label;

                if (input.length) {
                    input.val(log);
                } else {
                    if (log) alert(log);
                }

            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#img-upload').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#imgInp").change(function () {
                readURL(this);
            });
        });


        function myMap() {
            var myLatLng = {lat: 51.50874, lng: -0.120850};
            var mapProp = {
                center: new google.maps.LatLng(51.508742, -0.120850),
                zoom: 5,
                zoomControl: false,
                mapTypeControl: false,

            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });

        }

    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&callback=myMap"></script>
@endsection
