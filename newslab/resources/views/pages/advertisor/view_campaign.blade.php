@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout='layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout='layout.driver'; ?>
@elseif($role=='2')
    <?php $layout='layout.default'; ?>
@elseif($role=='1')
    <?php $layout='layout.admin'; ?>
@endif
@extends($layout)
@section('content')

    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
        .asdfasdf {
            border: 1px solid #ffffff;
            margin-top: 18px;
            padding: 14px 19px;
            border-radius: 6px;
            box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
        }
        ul.compainlist li {
            margin-top: 8px;
            font-size: 15px;
        }
        .owpierjpwoq {
            font-size: 52px;
            color: #3c9bd1;
            margin-top: 11px;
            margin-right: 19px;
        }
    </style>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <!--begin::Details-->
                    <div class="d-flex mb-9">
                        <!--begin: Pic-->
                        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                            <div class="symbol symbol-50 symbol-lg-120">
{{--                                <img src="/metronic/theme/html/demo1/dist/assets/media/users/300_1.jpg" alt="image">--}}
                            </div>
                            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                            </div>
                        </div>
                        <!--end::Pic-->
                        <!--begin::Info-->
                        <div class="flex-grow-1">
                            <!--begin::Title-->
                            <div class="d-flex justify-content-between flex-wrap mt-1">
                                <div class="d-flex mr-3">
                                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">Sample Tille</a>
                                    <a href="#">
                                        <i class="flaticon2-correct text-success font-size-h5"></i>
                                    </a>
                                </div>
                                <div class="my-lg-0 my-3">
                                    <button class="btn btn-light-primary font-weight-bold text-right" style=""><i class="fa fa-download" aria-hidden="true"></i>download</button>
                                    <button class="btn btn-light-primary font-weight-bold" style="  "><i class="fa fa-print" aria-hidden="true"></i>print</button>


                                </div>
                            </div>
                            <!--end::Title-->
                            <!--begin::Content-->
                            <div class="d-flex flex-wrap justify-content-between mt-1">
                                <div class="d-flex flex-column flex-grow-1 pr-8">
                                    <div class="d-flex flex-wrap mb-4">
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                            <i class="flaticon2-new-email mr-2 font-size-lg"></i>jason@siastudio.com</a>
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                            <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>PR Manager</a>
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold">
                                            <i class="flaticon2-placeholder mr-2 font-size-lg"></i>Melbourne</a>
                                    </div>
{{--                                    <span class="font-weight-bold text-dark-50">I distinguish three main text objectives could be merely to inform people.</span>--}}
{{--                                    <span class="font-weight-bold text-dark-50">A second could be persuade people.You want people to bay objective</span>--}}
                                </div>
                                <div class="d-flex align-items-center w-25 flex-fill float-right mt-lg-12 mt-8">
                                    <span class="font-weight-bold text-dark-75">Progress</span>
                                    <div class="progress progress-xs mx-3 w-100">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 63%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <span class="font-weight-bolder text-dark">78%</span>
                                </div>
                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Details-->
                    <div class="separator separator-solid"></div>
                    <!--begin::Items-->
                    <div class="d-flex align-items-center flex-wrap mt-8">
                        <!--begin::Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
												<span class="mr-4">
													<i class="icon-xl fas fa-car-alt display-4 text-muted font-weight-bold"></i>
												</span>
                            <div class="d-flex flex-column text-dark-75">
                                <span class="font-weight-bolder font-size-sm">Number Of Cars</span>
                                <span class="font-weight-bolder font-size-h5">
													<span class="text-dark-50 font-weight-bold"></span>24</span>
                            </div>
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
												<span class="mr-4">
													<i class="flaticon-piggy-bank display-4 text-muted font-weight-bold"></i>
												</span>
                            <div class="d-flex flex-column text-dark-75">
                                <span class="font-weight-bolder font-size-sm">Total Cost</span>
                                <span class="font-weight-bolder font-size-h5">
													<span class="text-dark-50 font-weight-bold">$</span>164,700</span>
                            </div>
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
												<span class="mr-4">
													<i class="icon-xl far fa-calendar-minus display-4 text-muted font-weight-bold"></i>
												</span>
                            <div class="d-flex flex-column text-dark-75">
                                <span class="font-weight-bolder font-size-sm">Start Date</span>
                                <span class="font-weight-bolder font-size-h5">
													<span class="text-dark-50 font-weight-bold"></span>10/1/2010</span>
                            </div>
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
												<span class="mr-4">
													<i class="icon-xl far fa-calendar-minus display-4 text-muted font-weight-bold"></i>
												</span>
                            <div class="d-flex flex-column flex-lg-fill">
                                <span class="text-dark-75 font-weight-bolder font-size-sm">End Date</span>
                                <span class="font-weight-bolder font-size-h5">
													<span class="text-dark-50 font-weight-bold"></span>10/1/2010</span>
                            </div>
                        </div>
                        <!--end::Item-->
{{--                        <!--begin::Item-->--}}
{{--                        <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">--}}
{{--												<span class="mr-4">--}}
{{--													<i class="flaticon-chat-1 display-4 text-muted font-weight-bold"></i>--}}
{{--												</span>--}}
{{--                            <div class="d-flex flex-column">--}}
{{--                                <span class="text-dark-75 font-weight-bolder font-size-sm">648 Comments</span>--}}
{{--                                <a href="#" class="text-primary font-weight-bolder">View</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <!--end::Item-->
                        <!--begin::Item-->
{{--                        <div class="d-flex align-items-center flex-lg-fill mb-2 float-left">--}}
{{--												<span class="mr-4">--}}
{{--													<i class="flaticon-network display-4 text-muted font-weight-bold"></i>--}}
{{--												</span>--}}
{{--                            <div class="symbol-group symbol-hover">--}}
{{--                                <div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="" data-original-title="Mark Stone">--}}
{{--                                    <img alt="Pic" src="/metronic/theme/html/demo1/dist/assets/media/users/300_25.jpg">--}}
{{--                                </div>--}}
{{--                                <div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="" data-original-title="Charlie Stone">--}}
{{--                                    <img alt="Pic" src="/metronic/theme/html/demo1/dist/assets/media/users/300_19.jpg">--}}
{{--                                </div>--}}
{{--                                <div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="" data-original-title="Luca Doncic">--}}
{{--                                    <img alt="Pic" src="/metronic/theme/html/demo1/dist/assets/media/users/300_22.jpg">--}}
{{--                                </div>--}}
{{--                                <div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="" data-original-title="Nick Mana">--}}
{{--                                    <img alt="Pic" src="/metronic/theme/html/demo1/dist/assets/media/users/300_23.jpg">--}}
{{--                                </div>--}}
{{--                                <div class="symbol symbol-30 symbol-circle" data-toggle="tooltip" title="" data-original-title="Teresa Fox">--}}
{{--                                    <img alt="Pic" src="/metronic/theme/html/demo1/dist/assets/media/users/300_18.jpg">--}}
{{--                                </div>--}}
{{--                                <div class="symbol symbol-30 symbol-circle symbol-light">--}}
{{--                                    <span class="symbol-label font-weight-bold">5+</span>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <!--end::Item-->
                    </div>
                    <!--begin::Items-->
                </div>
            </div>
            <!--end::Card-->
            <!--begin::Row-->
            <div class="row">
                <div class="col-lg-4">
                    <!--begin::Mixed Widget 14-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title font-weight-bolder">Campaign Details </h3>

                        </div>
                        <!--end::Header-->
                        <div class="asdfasdf">
                            <div class="col-lg-12 ">
                                <div class=" ueytui " >
{{--                                    <h4>Campaign Metrics1</h4>--}}
                                    <div class="d-flex border-bottom mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b>Potential Reach</b></small>
                                          <h4>8,090 People</h4>
                                     </span>
                                        <span>
                                            <span class="uwpoms"> <i class="fas fa-tachometer-alt owpierjpwoq"></i></span>
                                     </span>
                                    </div>
                                    <span>
                                           <small class="mb-1 mt-2"><b>Campaign Type</b></small>
                                          <h5 class="mb-5">Commercial</h5>
                                     </span>
                                    <span>
                                           <small class="mb-1 mt-2"><b>Campaign Start</b></small>
                                          <h5 class="mb-5">22-jan-2020</h5>
                                     </span>
                                    <span>
                                           <small class="mb-1 mt-4"><b>Campaign Ends</b></small>
                                          <h5 class="mb-5">30-jan-2020</h5>
                                     </span>

                                </div>
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 14-->
                </div>

                <div class="col-lg-8">
                    <!--begin::Advance Table Widget 2-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label font-weight-bolder text-dark">Car Details</span>
{{--                                <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span>--}}
                            </h3>
{{--                            <div class="card-toolbar">--}}
{{--                                <ul class="nav nav-pills nav-pills-sm nav-dark-75">--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_11_1">Month</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="#kt_tab_pane_11_2">Week</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link py-2 px-4 active" data-toggle="tab" href="#kt_tab_pane_11_3">Day</a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-2 pb-0 mt-n3">
                            <div class="tab-content mt-5" id="myTabTables11">
                                <!--begin::Tap pane-->
                                <div class="tab-pane fade" id="kt_tab_pane_11_1" role="tabpanel" aria-labelledby="kt_tab_pane_11_1">
                                    <!--begin::Table-->
                                    <div class="table-responsive">
                                        <table class="table table-borderless table-vertical-center">
                                            <thead>
                                            <tr>
                                                <th class="p-0 w-40px"></th>
                                                <th class="p-0 min-w-200px"></th>
                                                <th class="p-0 min-w-100px"></th>
                                                <th class="p-0 min-w-125px"></th>
                                                <th class="p-0 min-w-110px"></th>
                                                <th class="p-0 min-w-150px"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="pl-0 py-4">
                                                    <div class="symbol symbol-50 symbol-light">
																				<span class="symbol-label">
																					<img src="/metronic/theme/html/demo1/dist/assets/media/svg/misc/003-puzzle.svg" class="h-50 align-self-center" alt="">
																				</span>
                                                    </div>
                                                </td>
                                                <td class="pl-0">
                                                    <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Payrol Application</a>
                                                    <div>
                                                        <span class="font-weight-bolder">Email:</span>
                                                        <a class="text-muted font-weight-bold text-hover-primary" href="#">company@dev.com</a>
                                                    </div>
                                                </td>
                                                <td class="text-right">
                                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$560,000</span>
                                                    <span class="text-muted font-weight-bold">Paid</span>
                                                </td>
                                                <td class="text-right">
                                                    <span class="text-muted font-weight-500">Laravel, Metronic</span>
                                                </td>
                                                <td class="text-right">
                                                    <span class="label label-lg label-light-success label-inline">Success</span>
                                                </td>
                                                <td class="text-right pr-0">
                                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
																				<span class="svg-icon svg-icon-md svg-icon-primary">
																					<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Settings-1.svg-->
																					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																							<rect x="0" y="0" width="24" height="24"></rect>
																							<path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
																							<path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
																						</g>
																					</svg>
                                                                                    <!--end::Svg Icon-->
																				</span>
                                                    </a>
                                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm mx-3">
																				<span class="svg-icon svg-icon-md svg-icon-primary">
																					<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Write.svg-->
																					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																							<rect x="0" y="0" width="24" height="24"></rect>
																							<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)"></path>
																							<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
																						</g>
																					</svg>
                                                                                    <!--end::Svg Icon-->
																				</span>
                                                    </a>
                                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
																				<span class="svg-icon svg-icon-md svg-icon-primary">
																					<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Trash.svg-->
																					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																							<rect x="0" y="0" width="24" height="24"></rect>
																							<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
																							<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
																						</g>
																					</svg>
                                                                                    <!--end::Svg Icon-->
																				</span>
                                                    </a>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end::Table-->
                                </div>
                                <!--end::Tap pane-->
                                <!--begin::Tap pane-->
                                <div class="tab-pane fade" id="kt_tab_pane_11_2" role="tabpanel" aria-labelledby="kt_tab_pane_11_2">
                                    <!--begin::Table-->
                                    <div class="table-responsive">
                                        <table class="table table-borderless table-vertical-center">
                                            <thead>
                                            <tr>
                                                <th class="p-0 w-40px"></th>
                                                <th class="p-0 min-w-200px"></th>
                                                <th class="p-0 min-w-100px"></th>
                                                <th class="p-0 min-w-125px"></th>
                                                <th class="p-0 min-w-110px"></th>
                                                <th class="p-0 min-w-150px"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td class="pl-0 py-4">
                                                    <div class="symbol symbol-50 symbol-light">
																				<span class="symbol-label">
																					<img src="/metronic/theme/html/demo1/dist/assets/media/svg/misc/015-telegram.svg" class="h-50 align-self-center" alt="">
																				</span>
                                                    </div>
                                                </td>
                                                <td class="pl-0">
                                                    <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Application Development</a>
                                                    <div>
                                                        <span class="font-weight-bolder">Email:</span>
                                                        <a class="text-muted font-weight-bold text-hover-primary" href="#">app@dev.com</a>
                                                    </div>
                                                </td>
                                                <td class="text-right">
                                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$4,600,000</span>
                                                    <span class="text-muted font-weight-bold">Paid</span>
                                                </td>
                                                <td class="text-right">
                                                    <span class="text-muted font-weight-500">Python, MySQL</span>
                                                </td>
                                                <td class="text-right">
                                                    <span class="label label-lg label-light-warning label-inline">In Progress</span>
                                                </td>
                                                <td class="text-right pr-0">
                                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
																				<span class="svg-icon svg-icon-md svg-icon-primary">
																					<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Settings-1.svg-->
																					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																							<rect x="0" y="0" width="24" height="24"></rect>
																							<path d="M7,3 L17,3 C19.209139,3 21,4.790861 21,7 C21,9.209139 19.209139,11 17,11 L7,11 C4.790861,11 3,9.209139 3,7 C3,4.790861 4.790861,3 7,3 Z M7,9 C8.1045695,9 9,8.1045695 9,7 C9,5.8954305 8.1045695,5 7,5 C5.8954305,5 5,5.8954305 5,7 C5,8.1045695 5.8954305,9 7,9 Z" fill="#000000"></path>
																							<path d="M7,13 L17,13 C19.209139,13 21,14.790861 21,17 C21,19.209139 19.209139,21 17,21 L7,21 C4.790861,21 3,19.209139 3,17 C3,14.790861 4.790861,13 7,13 Z M17,19 C18.1045695,19 19,18.1045695 19,17 C19,15.8954305 18.1045695,15 17,15 C15.8954305,15 15,15.8954305 15,17 C15,18.1045695 15.8954305,19 17,19 Z" fill="#000000" opacity="0.3"></path>
																						</g>
																					</svg>
                                                                                    <!--end::Svg Icon-->
																				</span>
                                                    </a>
                                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm mx-3">
																				<span class="svg-icon svg-icon-md svg-icon-primary">
																					<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Communication/Write.svg-->
																					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																							<rect x="0" y="0" width="24" height="24"></rect>
																							<path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)"></path>
																							<path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
																						</g>
																					</svg>
                                                                                    <!--end::Svg Icon-->
																				</span>
                                                    </a>
                                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">
																				<span class="svg-icon svg-icon-md svg-icon-primary">
																					<!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/General/Trash.svg-->
																					<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																						<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																							<rect x="0" y="0" width="24" height="24"></rect>
																							<path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"></path>
																							<path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"></path>
																						</g>
																					</svg>
                                                                                    <!--end::Svg Icon-->
																				</span>
                                                    </a>
                                                </td>
                                            </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end::Table-->
                                </div>
                                <!--end::Tap pane-->
                                <!--begin::Tap pane-->
                                <div class="tab-pane fade show active" id="kt_tab_pane_11_3" role="tabpanel" aria-labelledby="kt_tab_pane_11_3">
                                    <!--begin::Table-->
                                    <div class="table-responsive">
                                        <table class="table text-center  table-vertical-center">
                                            <thead>
                                            <tr>
                                                <th >Zip Code</th>
                                                <th >Car Make</th>
                                                <th >Number Of Cars</th>
                                                <th >Model</th>
                                                <th >Year</th>
                                                <th >Milage</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                           <tr>
                                               <th >2032</th>
                                               <th >Suzuki</th>
                                               <th >2</th>
                                               <th >Wagon R</th>
                                               <th >2020</th>
                                               <th >1018,000</th>
                                           </tr>
                                           <tr>
                                               <th >2032</th>
                                               <th >Honda</th>
                                               <th >72</th>
                                               <th >Car</th>
                                               <th >2010</th>
                                               <th >102,000</th>
                                           </tr>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!--end::Table-->
                                </div>
                                <!--end::Tap pane-->
                            </div>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Advance Table Widget 2-->
                </div>

            </div>

            <!--end::Row-->
            <div class="row">
                <div class="col-lg-4">
                                    <!--begin::Mixed Widget 14-->
                                    <div class="card card-custom card-stretch gutter-b">
                                        <!--begin::Header-->
                                        <div class="card-header border-0 pt-5">
                                            <h3 class="card-title font-weight-bolder">Location Details</h3>

                                        </div>
                                        <!--end::Header-->
                                        <!--begin::Body-->
                                        <div class="asdfasdf mt-n5">
{{--                                            <p class="border-bottom">Location Details </p>--}}
                                            <div class="d-flex border-bottom mb-1 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b>Manchester</b></small>
                                          <h4>USA</h4>
                                     </span>
                                                <span>

                                            <span class="uwpoms"> <i class="fas fa-map-marked-alt owpierjpwoq"></i></span>
                                     </span>
                                            </div>
                                            <div class="w-100">
                                                <img src="{{asset('/media/mapimge.png')}}" id="loadimgedd" class=" img-thumbnail" alt="">
                                            </div>
                                        </div>
                                        <!--end::Body-->
                                    </div>
                                    <!--end::Mixed Widget 14-->
                                </div>
                <div class="col-lg-4">
                    <!--begin::Mixed Widget 14-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5 mb-3">
                            <h3 class="card-title font-weight-bolder">Sticker Details</h3>
                            <div class="mt-n5">
                                <div class=" ueytui mt-n5" >
{{--                                    <h4>Sticker Detials</h4>--}}
                                    <div class="d-flex  mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b></b></small>
                                          <h4></h4>
                                     </span>
                                        <span>

                                            <span class="uwpoms"> <i class="fas fa-image owpierjpwoq"></i></span>
                                     </span>
                                    </div>
                                    <table class="table">
                                        <tbody><tr>
                                            <th>Company:</th>
                                            <td>20thFloor</td>
                                        </tr>
                                        <tr>
                                            <th>Slogan:</th>
                                            <td>20thTech</td>
                                        </tr>
                                        <tr>
                                            <th>Contact:</th>
                                            <td>+12-165159806</td>
                                        </tr>
                                        <tr>
                                            <th>Sticker Details:</th>
                                            <td>Sample sticker information here</td>
                                        </tr>
                                        </tbody></table>



                                </div>
                                <p class="mb-0">Sticker Picture:</p>
                                <img src="{{asset('/media/uploadimge.png')}}" class=" img-thumbnail" alt="">
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->

                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 14-->
                </div>
                <div class="col-lg-4">
                    <!--begin::Mixed Widget 14-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5 mb-3">
                            <h3 class="card-title font-weight-bolder">Detail Summary</h3>
                            <i class="far fa-chart-bar float-right mt-3 owpierjpwoq"></i>
                        </div>
                        <table class="table text-center">
                            <tr>
                                <th>Interval</th>
                                <td>20/2/2020-03/06/2020</td>
                            </tr>
                            <tr>
                                <th>Locations</th>
                                <td>UK,Londan</td>
                            </tr>
                            <tr>
                                <th>Total Cars</th>
                                <td>22</td>
                            </tr>
                            <tr>
                                <th>Total Cost</th>
                                <td>$30,992</td>
                            </tr>
                        </table>
                        <!--end::Header-->
                        <!--begin::Body-->

                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 14-->
                </div>

            </div>
            <!--begin::Row-->

            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>


@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection


{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- vendors --}}
    <script>
        function del()
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }


        $('#kt_datatable').dataTable({
            "scrollX": true
        });
    </script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

    {{-- page scripts --}}
    <script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/contacts/list-datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

    <script>

        $(document).on('click', '#toggleSearchForm', function(){

            $('#table-search-form').slideToggle();

        });







    </script>

@endsection
