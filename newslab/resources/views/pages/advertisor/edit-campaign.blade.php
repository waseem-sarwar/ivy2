{{-- Extends layout --}}
@extends('layout.default')
@section('styles')

    <link href="{{ asset('css/pages/wizard/wizard-3.css') }}" rel="stylesheet" type="text/css" />

@endsection
{{-- Content --}}
@section('content')
    <?php $count=0;?>
    <div id="kt_repeater_1">
        <div class="form-group row" id="kt_repeater_1" data-val="{{$count}}">
            <div data-repeater-list="" >
                <div data-repeater-item >
                    <!--begin::Content-->
                    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                        <!--begin::Subheader-->
                        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
                            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                                <!--begin::Info-->
                                <div class="d-flex align-items-center flex-wrap mr-1">
                                    <!--begin::Page Heading-->
                                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                                        <!--begin::Page Title-->
                                        <h5 class="text-dark font-weight-bold my-1 mr-5">Campaign</h5>
                                        <!--end::Page Title-->
                                        <!--begin::Breadcrumb-->
                                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                                            <li class="breadcrumb-item">
                                                <a href="" class="text-muted">Pages</a>
                                            </li>
                                            <li class="breadcrumb-item">
                                                <a href="" class="text-muted">Campaign</a>
                                            </li>
                                        </ul>
                                        <!--end::Breadcrumb-->
                                    </div>
                                    <!--end::Page Heading-->
                                </div>
                                <!--end::Info-->
                                <!--begin::Toolbar-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Actions-->
                                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>
                                    <!--end::Actions-->
                                    <!--begin::Dropdown-->
                                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">
                                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="svg-icon svg-icon-success svg-icon-2x">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24" />
														<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
														<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">
                                            <!--begin::Navigation-->
                                            <ul class="navi navi-hover">
                                                <li class="navi-header font-weight-bold py-4">
                                                    <span class="font-size-lg">Choose Label:</span>
                                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
                                                </li>
                                                <li class="navi-separator mb-3 opacity-70"></li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-success">Customer</span>
														</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-danger">Partner</span>
														</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-warning">Suplier</span>
														</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-primary">Member</span>
														</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-dark">Staff</span>
														</span>
                                                    </a>
                                                </li>
                                                <li class="navi-separator mt-3 opacity-70"></li>
                                                <li class="navi-footer py-4">
                                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">
                                                        <i class="ki ki-plus icon-sm"></i>Add new</a>
                                                </li>
                                            </ul>
                                            <!--end::Navigation-->
                                        </div>
                                    </div>
                                    <!--end::Dropdown-->
                                </div>
                                <!--end::Toolbar-->
                            </div>
                        </div>
                        <!--end::Subheader-->
                        <!--begin::Entry-->
                        <div class="d-flex flex-column-fluid">
                            <!--begin::Container-->
                            <div class="container">
                                <div class="card card-custom">
                                    <div class="card-body p-0">
                                        <!--begin: Wizard-->
                                        <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first" data-wizard-clickable="true">
                                            <!--begin: Wizard Nav-->
                                            <div class="wizard-nav">
                                                <div class="wizard-steps px-8 py-8 px-lg-15 py-lg-3">
                                                    <!--begin::Wizard Step 1 Nav-->
                                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                                        <div class="wizard-label">
                                                            <h3 class="wizard-title">
                                                                <span>  </span>Campaigns Edit</h3>
                                                            <div class="wizard-bar"></div>
                                                        </div>
                                                    </div>
                                                    <!--end::Wizard Step 1 Nav-->
                                                    <!--begin::Wizard Step 2 Nav-->
                                                {{--                                    <div class="wizard-step" data-wizard-type="step">--}}
                                                {{--                                        <div class="wizard-label">--}}
                                                {{--                                            <h3 class="wizard-title">--}}
                                                {{--                                                <span>2.</span>Enter Details</h3>--}}
                                                {{--                                            <div class="wizard-bar"></div>--}}
                                                {{--                                        </div>--}}
                                                {{--                                    </div>--}}
                                                <!--end::Wizard Step 2 Nav-->
                                                    <!--begin::Wizard Step 3 Nav-->
                                                {{--                                    <div class="wizard-step" data-wizard-type="step">--}}
                                                {{--                                        <div class="wizard-label">--}}
                                                {{--                                            <h3 class="wizard-title">--}}
                                                {{--                                                <span>3.</span>Select Services</h3>--}}
                                                {{--                                            <div class="wizard-bar"></div>--}}
                                                {{--                                        </div>--}}
                                                {{--                                    </div>--}}
                                                <!--end::Wizard Step 3 Nav-->
                                                    <!--begin::Wizard Step 4 Nav-->
                                                {{--                                    <div class="wizard-step" data-wizard-type="step">--}}
                                                {{--                                        <div class="wizard-label">--}}
                                                {{--                                            <h3 class="wizard-title">--}}
                                                {{--                                                <span>4.</span>Delivery Address</h3>--}}
                                                {{--                                            <div class="wizard-bar"></div>--}}
                                                {{--                                        </div>--}}
                                                {{--                                    </div>--}}
                                                <!--end::Wizard Step 4 Nav-->
                                                </div>
                                            </div>
                                            <!--end: Wizard Nav-->
                                            <!--begin: Wizard Body-->
                                            <div class="mb-2 wizard-body  py-lg-1 px-lg-15 border-right">
                                                <form class="form" id="kt_form">
                                                    <!--begin: Wizard Step 1-->
                                                    <div class="" data-wizard-type="step-content" data-wizard-state="current">
                                                        <h4 class=" font-weight-bold text-dark">Edit Campaign</h4>
                                                    </div>
                                                    <!--begin: Wizard Form-->
                                                    <div class="row">
                                                        <div class="col-md-8 border-right ">
                                                            <div class="row">
                                                                <!--begin::Input-->
                                                                <div class="form-group col-md-4">
                                                                    <label>Campaign Title</label>
                                                                    <input type="text" class="form-control " name="fname" placeholder="Campaign Title" value="" required />
                                                                    <span class="form-text text-muted">Please Enter your Campaign Title.</span>
                                                                </div>
                                                                <!--end::Input-->
                                                                <!--begin::Input-->
                                                                {{--                                                <div class="form-group col-md-4">--}}
                                                                {{--                                                    <label>Compaign type:</label>--}}
                                                                {{--                                                    <select name="delivery" class="form-control ">--}}
                                                                {{--                                                        <option value="" selected disabled>Choose compaign type</option>--}}
                                                                {{--                                                        <option value="2" >type1</option>--}}
                                                                {{--                                                        <option value="1">type2</option>--}}
                                                                {{--                                                        <option value="3">type3</option>--}}
                                                                {{--                                                    </select>--}}
                                                                {{--                                                    <span class="form-text text-muted">Choose service type of compaign</span>--}}
                                                                {{--                                                </div>--}}
                                                                <div class="form-group col-md-4">
                                                                    <label>Time Duration:</label>
                                                                    <select name="delivery"  class="form-control ">
                                                                        <option value="" selected disabled>Time duration</option>
                                                                        <option value="">days</option>
                                                                        <option value="overnight" >weeks</option>
                                                                        <option value="express">months</option>
                                                                        <option value="basic">more than months</option>
                                                                    </select>
                                                                    <span class="form-text text-muted">Choose total duration of compaign.</span>
                                                                </div>


                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label>Start date*</label>
                                                                        <input class="form-control" name="s_d" type="date" >
                                                                        <span class="form-text text-muted">Please enter start date.</span>
                                                                    </div>
                                                                </div>

                                                                {{--                                                <div class="col-md-6">--}}
                                                                {{--                                                    <div class="form-group">--}}
                                                                {{--                                                        <label>End date*</label>--}}
                                                                {{--                                                        <input class="form-control " name="e_d" type="text" id="datepicker2">--}}
                                                                {{--                                                        <span class="form-text text-muted">Please end date.</span>--}}
                                                                {{--                                                    </div>--}}
                                                                {{--                                                </div>--}}


                                                                <hr/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">

                                                            <div class="card">
                                                                <h4 class="font-weight-bold text-dark text-center py-2" style="background-color: #ebedf3;">     Campaign OverView</h4>
                                                                <div class="card-body"> <br>
                                                                    <div class="form-group row">
                                                                        <div class="  col-sm-5">
                                                                            Duration
                                                                        </div>
                                                                        <div class="  col-sm-7">
                                                                            <p>10   days </p><p> 01-11-2020  to 22-11-2020</p>
                                                                        </div>
                                                                        <div class="  col-sm-5">
                                                                            Costing
                                                                        </div>
                                                                        <div class="  col-sm-7">
                                                                            <p>100 X  10= 1000</p>
                                                                        </div>
                                                                        <div class="  col-sm-6 offset-7">
                                                                            <p>Total: 10000</p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                        </div>
                                                    </div>
                                                    <hr/>


                                                    <h4 class="mb-2 mt-2 font-weight-bold text-dark">Location info</h4>
                                                    <div class="row">
                                                        <div class="col-md-8 border-right ">
                                                            <div class="row">

                                                                <div class="col-xl-6" >
                                                                    <!--begin::Input-->
                                                                    <div class="form-check">
                                                                        <label>Country:</label>
                                                                        <select name="delivery " class="select3 form-control ">
                                                                            <option value="" selected disabled>Country</option>
                                                                            <option value="12">England</option>
                                                                            <option value="92">UK</option>
                                                                            <option value="11">Australia</option>
                                                                            <option value="1">India/option>
                                                                        </select>
                                                                        <span class="form-text text-muted">Please select compaign country.</span>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                </div>
                                                                <div class="col-xl-6">
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <label>State:</label>
                                                                        <select name="delivery" class="form-control ">
                                                                            <option value="" selected disabled>Choose State</option>
                                                                            <option value="12">England</option>
                                                                            <option value="92" >UK</option>
                                                                            <option value="11">Australia</option>
                                                                            <option value="1">India/option>
                                                                        </select>
                                                                        <span class="form-text text-muted">Please select compaign country.</span>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                </div>


                                                                <div class="col-xl-6">
                                                                    <!--begin::Input-->
                                                                    <div class="form-check">
                                                                        <label>City:</label>
                                                                        <select name="delivery" class="form-control ">
                                                                            <option value="" selected disabled>choose City</option>
                                                                            <option value="12">lahore>
                                                                            <option value="92" >london</option>
                                                                            <option value="11">islamabad</option>

                                                                        </select>
                                                                        <span class="form-text text-muted">Please select compaign city.</span>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                </div>

                                                                <div class="col-xl-6">
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <label>Zip code:</label>
                                                                        <input type="text" class="form-control " name="address1" placeholder="zip code" value="ZXC111" />
                                                                        <span class="form-text text-muted">City zip code.</span>
                                                                    </div>
                                                                    <!--end::Input-->
                                                                </div>

                                                                <hr/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="card">
                                                                <div class="card-body m-0 p-0">
                                                                    <div id="googleMap" style="width:100%;height:270px;"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <hr/>




                                                    <h4 class="mb-2 mt-2 font-weight-bold text-dark">Car info</h4>
                                                    <div class="row">
                                                        <div class="col-md-8 border-right ">
                                                            <div class="row">
                                                                <div class="col-xl-4">
                                                                    <div class="form-check">
                                                                        <label>Car Make:</label>
                                                                        <select name="delivery" class="form-control">
                                                                            <option value="" selected disabled>Select Make</option>
                                                                            <option value="12">Hyundai</option>
                                                                            <option value="92" >Suzuki</option>
                                                                            <option value="11">Toyota</option>
                                                                        </select>
                                                                        <span class="form-text text-muted"> Choose car make company.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <div class="form-group">
                                                                        <label>Car Type:</label>
                                                                        <select name="delivery" class="form-control ">
                                                                            <option value="" selected disabled> Select Type</option>
                                                                            <option value="12">Hatchback</option>
                                                                            <option value="92" >Sedan</option>
                                                                            <option value="11">SUV</option>
                                                                            <option value="11">Mini Van</option>
                                                                            <option value="11">Crossover</option>
                                                                            <option value="11">Van</option>
                                                                            <option value="11">Micro Van</option>
                                                                            <option value="11">MPV</option>
                                                                            <option value="11">Double Cabin</option>
                                                                            <option value="11">Station Wagon</option>
                                                                            <option value="11">Pick Up</option>
                                                                            <option value="11">High Roof</option>
                                                                        </select>
                                                                        <span class="form-text text-muted">Choose car type.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <div class="form-group">
                                                                        <label>Car Model:</label>
                                                                        <select name="delivery" class="form-control ">
                                                                            <option value="" selected disabled>Select Model</option>
                                                                            <option value="12">Corolla</option>
                                                                            <option value="92" >Civic</option>
                                                                            <option value="11">
                                                                                Land Cruiser</option>
                                                                        </select>
                                                                        <span class="form-text text-muted">Manufactored year.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <div class="form-group">
                                                                        <label>Select year:</label>
                                                                        <select name="delivery" class="form-control ">
                                                                            <option value="" selected disabled>Year</option>
                                                                            <option value="12">2020</option>
                                                                            <option value="92" >2019</option>
                                                                            <option value="11">2018</option>
                                                                            <option value="11">2017</option>
                                                                            <option value="11">2016</option>
                                                                            <option value="11">2015</option>
                                                                            <option value="11">2014</option>
                                                                            <option value="11">2013</option>
                                                                            <option value="11">2012</option>
                                                                            <option value="11">2011</option>
                                                                            <option value="11">2010</option>
                                                                        </select>
                                                                        <span class="form-text text-muted">Manufactored year.</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4">
                                                                    <div class="form-group">
                                                                        <label>How Often miles Drived:</label>
                                                                        <input type="number" class="form-control " name="address1" placeholder="Miles Drived" value="12 " />
                                                                        <span class="form-text text-muted">Number of miles.</span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-xl-4">
                                                                    <div class="form-check">
                                                                        <label>Number of Cars :</label>
                                                                        <input type="number" class="form-control " name="address1" placeholder="Number of cars" value="3" />
                                                                        {{--                                                        <input type="text">--}}
                                                                        <span class="form-text text-muted">cars</span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-xl-4 ">
                                                                    <div class="form-group">
                                                                        <label>How often drives a car:</label>
                                                                        <select name="delivery"  class="form-control" onchange="yesnoCheck(this);">
                                                                            <option value="" selected>choose one</option>
                                                                            <option value="d">Daily</option>
                                                                            <option value="w" >Weekly</option>
                                                                            <option value="m">Monthly</option>

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="day-picker col-xl-4" style="display: none">
                                                                    <div class="form-check">
                                                                        <label>Select Start  Day :</label>
                                                                        <input type="date" class="form-control " name="address1" placeholder="Number of cars" value="3" />
                                                                        {{--                                                        <input type="text">--}}
                                                                        <span class="form-text text-muted">cars</span>
                                                                    </div>
                                                                </div>
                                                                <div class="day-picker col-xl-4" style="display: none">
                                                                    <div class="form-check">
                                                                        <label>Select End Day :</label>
                                                                        <input type="date" class="form-control " name="address1" placeholder="Number of cars" value="3" />
                                                                        {{--                                                        <input type="text">--}}
                                                                        <span class="form-text text-muted">cars</span>
                                                                    </div>
                                                                </div>
                                                                <div class="week-picker col-xl-4" style="display: none">
                                                                    <div class="form-check">
                                                                        <label>Select Start  Week :</label>
                                                                        <input type="week" class="form-control " name="address1" placeholder="Number of cars" value="3" />
                                                                        {{--                                                        <input type="text">--}}
                                                                        <span class="form-text text-muted">cars</span>
                                                                    </div>
                                                                </div>
                                                                <div class="week-picker col-xl-4" style="display: none">
                                                                    <div class="form-check">
                                                                        <label>Select End Week :</label>
                                                                        <input type="week" class="form-control " name="address1" placeholder="Number of cars" value="3" />
                                                                        {{--                                                        <input type="text">--}}
                                                                        <span class="form-text text-muted">cars</span>
                                                                    </div>
                                                                </div>
                                                                <div class="month-picker col-xl-4" style="display: none">
                                                                    <div class="form-check">
                                                                        <label>Select Month Start :</label>
                                                                        <input type="month" class="form-control " name="address1" placeholder="Number of cars" value="3" />
                                                                        {{--                                                        <input type="text">--}}
                                                                        <span class="form-text text-muted">cars</span>
                                                                    </div>
                                                                </div>
                                                                <div class="month-picker col-xl-4" style="display: none">
                                                                    <div class="form-check">
                                                                        <label>Select Month End  :</label>
                                                                        <input type="month" class="form-control " name="address1" placeholder="Number of cars" value="3" />
                                                                        {{--                                                        <input type="text">--}}
                                                                        <span class="form-text text-muted">cars</span>
                                                                    </div>
                                                                </div>

                                                                {{--                                                <clearfix>--}}



                                                                {{--                                                <div class="col-xl-4">--}}
                                                                {{--                                                    <div class="form-group">--}}
                                                                {{--                                                        <input type="checkbox"  name="days" >--}}
                                                                {{--                                                        <label>hiii</label>--}}
                                                                {{--                                                    </div>--}}
                                                                {{--                                                </div>--}}

                                                            </div>
                                                            <div class="row">
                                                                <div class="col-sm-1 offset-2">
                                                                    <div class="form-group">
                                                                        <label >Mon</label>
                                                                        <div class="ml-2">
                                                                            <input type="checkbox" name="days">
                                                                            {{--                                                            <label class="custom-control-label"></label>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div class="form-group">
                                                                        <label>Tue</label>
                                                                        <div class="ml-2">
                                                                            <input type="checkbox"  name="days" >
                                                                            {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div class="form-group">
                                                                        <label  >Wed</label>
                                                                        <div class="ml-2">
                                                                            <input type="checkbox"  name="days" >
                                                                            {{--                                                            <label class="custom-control-label"></label>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div class="form-group">
                                                                        <label >Thur</label>
                                                                        <div class="ml-2">
                                                                            <input type="checkbox"name="days" >
                                                                            {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div class="form-group">
                                                                        <label   >Fri</label>
                                                                        <div class="">
                                                                            <input type="checkbox" name="days" >
                                                                            {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div class="form-group">
                                                                        <label >Sat</label>
                                                                        <div class="ml-2">
                                                                            <input type="checkbox" name="days" >
                                                                            {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-1">
                                                                    <div class="form-group">
                                                                        <label  >Sun</label>
                                                                        <div class="ml-2">
                                                                            <input type="checkbox" name="days"  >
                                                                            {{--                                                            <label class="custom-control-label" ></label>--}}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--                                        <div class="col-md-4">--}}
                                                        {{--                                            <div class="card">--}}
                                                        {{--                                                <div class="card-body m-0 p-0">--}}
                                                        {{--                                                    <div  style="width:100%;height:270px;"></div>--}}
                                                        {{--                                                </div>--}}
                                                        {{--                                            </div>--}}
                                                        {{--                                        </div>--}}
                                                        <div class="col-md-4">

                                                            <div class="card">
                                                                {{--                                                <h4 class=" font-weight-bold text-dark">Create a compaign</h4>--}}
                                                                <h4 class="font-weight-bold text-dark text-center py-2" style="background-color: #ebedf3;">     Campaign OverView</h4>
                                                                <div class="card-body"> <br>
                                                                    <div class="form-group row">
                                                                        <div class="  col-sm-5">
                                                                            Duration
                                                                        </div>
                                                                        <div class="  col-sm-7">
                                                                            <p>10   days </p><p> 01-11-2020  to 22-11-2020</p>
                                                                        </div>
                                                                        <div class="  col-sm-5">
                                                                            Costing
                                                                        </div>
                                                                        <div class="  col-sm-7">
                                                                            <p>100 X  10= 1000</p>
                                                                        </div>
                                                                        <div class="  col-sm-6 offset-7">
                                                                            <p>Total: 10000</p>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>



                                                        </div>
                                                    </div>
                                                    <hr/>


                                                </form>
                                                <div id="trash" class="trash float-right mb-4 mr-2">
                                                    <a href="javascript:;" data-repeater-delete="" class=" btn btn-sm font-weight-bolder btn-light-danger">
                                                        <i class="la la-trash-o"></i>
                                                    </a>
                                                </div>
{{--                                                <div class="float-left mb-4 mr-2">--}}
{{--                                                    <a href="javascript:;" onclick="showdelete();" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">--}}
{{--                                                        <i class="la la-plus"></i> Add more--}}
{{--                                                    </a>--}}
{{--                                                </div>--}}
                                            </div>





                                            <!--end: Wizard body -->
                                        </div>
                                        {{--                        <div class="form-group">--}}
                                        {{--                            <input  id="add_more"   name="submit" value="add more" style="--}}
                                        {{--    margin-left: 50px;--}}
                                        {{--"  class=" float-left btn font-weight-bold btn-success btn-shadow mr-2" >--}}
                                        {{--                        </div>--}}
                                    </div>
                                {{--                    <div class="col-md-6 mt-4">--}}
                                {{--                        <a href="javascript:;" data-repeater-delete="" class=" btn btn-sm font-weight-bolder btn-light-danger">--}}
                                {{--                            <i class="la la-trash-o"></i>--}}
                                {{--                        </a>--}}
                                {{--                    </div>--}}


                                {{--                    <div class="form-group">--}}
                                {{--                        <input onclick="window.location.href='{{url('/Advertiser/Dashboard/create-campaign-step2')}}';" value="Next" name="submit"  class=" float-right btn font-weight-bold btn-primary btn-shadow mr-2" >--}}
                                {{--                    </div>--}}
                                <!--end: Wizard-->
                                </div>

                                <!--end: Wizard Form-->
                            </div>
                            <!--end: Wizard Body-->
                            <!--begin: Wizard Nav-->

                            <!--end: Wizard Nav-->
                            <!--end: Wizard-->

                            <!--end: Wizard Body-->
                        </div>

                        <!--end: Wizard-->
                    </div>





                </div>
            </div>

        </div>
        <div  class="row">
            <div class="col-2 offset-1 ">
                {{--                <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">--}}
                {{--                    <i class="la la-plus"></i> Add more--}}
                {{--                </a>--}}
            </div>
            <div class="col-2  offset-6 ">
                <input type="submit" onclick="window.location.href='{{url('/Advertiser/Dashboard/create-campaign-step2')}}';" value="Next" name="submit"  class=" float-right btn btn-sm  font-weight-bold btn-success w-50 btn-shadow " >
            </div>
        </div>
    </div>




@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/wizard/wizard-3.js') }}" type="text/javascript"></script>
    {{--    <script src="assets/js/pages/custom/wizard/wizard-3.js"></script>--}}
    <script>

        function yesnoCheck(that) {
            if (that.value == "d") {
                // alert("check");
                $('.week-picker').hide();
                $('.day-picker').show();
                $('.month-picker').hide();
                // document.getElementById("ifYes").style.display = "block";
            }else if (that.value == "w") {
                // alert("check");
                $('.week-picker').show();
                $('.day-picker').hide();
                $('.month-picker').hide();

                // document.getElementById("ifYes").style.display = "block";
            } else if (that.value == "m") {
                // alert("check");
                $('.week-picker').hide();
                $('.day-picker').hide();
                $('.month-picker').show();
                // document.getElementById("ifYes").style.display = "block";
            } else {
                // document.getElementById("ifYes").style.display = "none";
            }
        }
        $(document).ready(function(){
            // $("button").click(function(){
            $("#trash{{$count}}").hide();
            $(".trash").hide();
            // });
        });
        showdelete = function () {
            console.log('here');
            $("#trash{{$count}}").show();
            $(".trash").show();
        };
        var KTFormRepeater = function() {
            // Private functions
            var demo1 = function() {
                $('#kt_repeater_1').repeater({
                    initEmpty: false,
                    defaultValues: {
                        'statew': 'foo'
                    },
                    show: function () {
                        $(this).slideDown();
                    },
                    hide: function (deleteElement) {
                        $(this).slideUp(deleteElement);
                    }
                });
            }
            return {
                // public functions
                init: function() {
                    demo1();
                }
            };
        }();
        jQuery(document).ready(function() {
            KTFormRepeater.init();
        });





        getGuardsDeploymentDataForUnpaid = function (parwest) {
            var globalPublicPath = '{{ URL('/') }}';
            console.log('herfe');
            // var parwest  = $('#parwest_id').val();
            if(parwest){
                // $("#month_deployment").css("border-color", "gray");
                var data = {
                    'parwest_id': parwest,
                    // 'month': month,
                };
                // let globalPublicPath;
                var object = {
                    'url': globalPublicPath + '/ajaxGetCountries',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                // if (result.data.inventory > 0) {
                //     warningNotificationCustom('Selected Guard   Inventory Must be revoked before clearance   ');
                // }
                // if (result.data.status_name == 'present' ) {
                //     warningNotificationCustom('Selected Guard must be revoked before clearance ');
                // }
                // if (result.data.status_name == 'absent' ||result.data.status_name == 'default' ||result.data.status_name == 'present') {
                //     warningNotificationCustom('Selected Guard Is  '+ result.data.status_name);
                // }else{
                if (result.responseCode == 1) {

                    $('#unpaid_guard_name').val(result.data.name);
                    $('#unpaid_status').val(result.data.status_name);
                    $('#unpaid_type').val(result.data.designation_name);
                    $('#unpaid_current_location').val(result.data.location);
                    $('#unpaid_loan').val(result.data.loan);

                    successNotificationCustom(result.message);
                }if(result.responseCode == 2){
                    warningNotificationCustom(result.message);
                }

                // }


            }else{
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        };
        getGuardsStatusDataForUnpaid = function (month) {
            var parwest  = $('#parwest_id').val();
            if(month){
                console.log('month',month);
                // $("#month_deployment").css("border-color", "gray");
                var data = {
                    'month': month,
                    'parwest_id': parwest,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetailsUnpaidSalaryExport',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                // if (result.data.inventory > 0) {
                //     warningNotificationCustom('Selected Guard   Inventory Must be revoked before clearance   ');
                // }
                // if (result.data.status_name == 'present' ) {
                //     warningNotificationCustom('Selected Guard must be revoked before clearance ');
                // }
                // if (result.data.status_name == 'absent' ||result.data.status_name == 'default' ||result.data.status_name == 'present') {
                //     warningNotificationCustom('Selected Guard Is  '+ result.data.status_name);
                // }else{
                if (result.responseCode == 1) {

                    // $('#unpaid_guard_name').val(result.data.name);
                    $('#salary_status').val(result.data.status);
                    if(result.data.status == 'Paid'){

                        $('#export_unpaid').attr('disabled', 'disabled');

                    }
                    // $('#unpaid_type').val(result.data.designation_name);
                    // $('#unpaid_current_location').val(result.data.location);
                    // $('#unpaid_loan').val(result.data.loan);

                    // successNotificationCustom(result.message);
                }
                // if(result.responseCode == 2){
                //     warningNotificationCustom(result.message);
                // }

                // }


            }else{
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        };
        getSupervisorsAgainstManagerOnExport = function (manager_id) {

            if (manager_id != 0) {

                var data = {
                    'manager_id': manager_id,
                };
                var object = {
                    'url': globalPublicPath + '/client/getSupervisorsAgainstManager',
                    'type': 'POST',
                    'data': data
                };



                var result = centralAjax(object);

                var html = '<option value="0" selected>--Select Supervisor--</option>' ;
                if(result.responseCode == 1 )
                {
                    $.each(result.data,function (key,value) {
                        html += '<option value="'+value.id+'">'+value.name+'</option>';
                    });

                }
                // $('#supervisor').removeAttr('disabled');
                $('#supervisor').empty().append(html);
                $('#supervisor').selectpicker('refresh');

                if (result.responseCode == 1) {
                    console.log(result.data[0].id);


                    //append a select supervisor dropdown
                    // $('#supervisor').html('');
                    // // $('#edit_supervisor_number_container').val('No Phone Number Found');
                    // $('#supervisor').append($('<option>', {
                    //     value: 0,
                    //     text: '--Select Supervisor--',

                    // }));


                    //append all the supervisors against a manager in dropdown
                    // $.each(result.data, function (key, value) {
                    // $('#supervisor').append('<option id="' + value.id + '">' + value.name + '</option>');
                    // var newOption = new Option(value.name, value.id, false, false);
                    // $('#supervisor').append(newOption);
                    // $('#supervisor').selectpicker('val',  value.id );
                    // $('#supervisor').selectpicker('text',  value.id );
//                         console.log(value.id);
//
//                         $('#edit_selected_client').selectpicker('val', client_id);
//                         $('#supervisor').append($('<option >', {
//                             value: value.id,
//                             text: value.name,
//
//                         }));
//
//                         //to select the value of current supervisor
// //                     if (currenstSupervisor.id == value.id) {
// //                         $('#edit_branch_supervisor_id').val(currenstSupervisor.id).change();
// // //                        getSupervisorDetailOnEditPage();
// //                     }
//
//                     });
                }
                else {
                    // errorNotificationCustom(result.message);
                }

            }


            else {
                errorNotificationCustom('please select a manager before continue');
            }

        };
        getManagersAgainstRegionOnExport = function (region_id) {

            if (region_id != 0) {

                var data = {
                    'region_id': region_id,
                };
                var object = {
                    'url': globalPublicPath + '/client/getManagersAgainstRegion',
                    'type': 'POST',
                    'data': data
                };



                var result = centralAjax(object);

                var html = '<option value="0" selected>--Select Manager--</option>' ;
                if(result.responseCode == 1 )
                {
                    $.each(result.data,function (key,value) {
                        html += '<option value="'+value.id+'">'+value.name+'</option>';
                    });

                }
                // $('#supervisor').removeAttr('disabled');
                $('#managers').empty().append(html);
                $('#managers').selectpicker('refresh');

                if (result.responseCode == 1) {
                    console.log(result.data[0].id);


                    //append a select supervisor dropdown
                    // $('#supervisor').html('');
                    // // $('#edit_supervisor_number_container').val('No Phone Number Found');
                    // $('#supervisor').append($('<option>', {
                    //     value: 0,
                    //     text: '--Select Supervisor--',

                    // }));


                    //append all the supervisors against a manager in dropdown
                    // $.each(result.data, function (key, value) {
                    // $('#supervisor').append('<option id="' + value.id + '">' + value.name + '</option>');
                    // var newOption = new Option(value.name, value.id, false, false);
                    // $('#supervisor').append(newOption);
                    // $('#supervisor').selectpicker('val',  value.id );
                    // $('#supervisor').selectpicker('text',  value.id );
//                         console.log(value.id);
//
//                         $('#edit_selected_client').selectpicker('val', client_id);
//                         $('#supervisor').append($('<option >', {
//                             value: value.id,
//                             text: value.name,
//
//                         }));
//
//                         //to select the value of current supervisor
// //                     if (currenstSupervisor.id == value.id) {
// //                         $('#edit_branch_supervisor_id').val(currenstSupervisor.id).change();
// // //                        getSupervisorDetailOnEditPage();
// //                     }
//
//                     });
                }
                else {
                    // errorNotificationCustom(result.message);
                }

            }


            else {
                errorNotificationCustom('please select a manager before continue');
            }

        };

        $('#extrahour_guards_client').change(function () {
            // $this.val();
            var client_id = $('#extrahour_guards_client').val();
            console.log(client_id);
            var data = {
                'client_id': client_id,
            };
            var object = {
                'url': globalPublicPath + '/guard/getClientBranchesLoan',
                'type': 'GET',
                'data': data,
            };
            $('#extrahour_guards_branch').html('');
            $('#extrahour_guards_branch').append($('<option>', {
                value: 0,
                text: '--Select Branch--',
            }));
            var result = centralAjax(object);
            console.log(result);
            $.each(result.data.branches, function (keys, values) {
                console.log(values);
                // $.each(values, function (key, value) {

                if (values.is_active == 1) {
                    $('#extrahour_guards_branch').append($('<option >', {
                        value: values.id,
                        text: values.name,

                    }));

                }
                // });


                // if(value.id == branchId){
                //     $("#branch_id_on_user_profile").val(value.id);
                //     console.log("branched value selected through jquery ajax");
                // }


            });
        });
        // populate managers list before salary genrate
        $("#genrate_salary").click(function(e) {
            e.preventDefault();
            var region = $('#regions').val();
            // var month = $('#salary_month').val();
            var markup = "";
            markup += '<tr >' +
                '<td ></td>' +
                '<td ></td>' +

                '</tr>';
            $("#salaryfinalize").append(markup);
            $("#finalizelist").append(markup);
            if(region == "" ){
                warningNotificationCustom('please select region ');
            }
            if(region != "" ){
                successNotificationCustom(" calculating loans ......",1000);
                var data = {
                    'region': region,
                    // 'month': month,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getFinalisedStatus',
                    'type': 'POST',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                if(result.post_status == true){
                    errorNotificationCustom(" Region already posted for this month ");

                }
                if(result.post_status == false){
                    warningNotificationCustom("Please Finalised loans by the following Users   ",1000);
                    $.each(result.users, function (index, value) {
                        console.log(index + ": " + value);
                        var markup = "";
                        markup += '<tr id="history">' +
                            '<td >' + value.name + '</td>' +
                            '<td >' + 'Pending !!' + '</td>' +

                            '</tr>';
                        $("#salaryfinalize").append(markup);
                    });
                    $.each(result.finalUsers, function (index, value) {
                        console.log(index + ": " + value);
                        var markup = "";
                        markup += '<tr id="finalizelistitem">' +
                            '<td >' + value.name + '</td>' +
                            '<td >' + 'Finalised :' + '</td>' +

                            '</tr>';
                        $("#finalizelist").append(markup);
                    });
                }
                if(result.genrate_status == false){
                    $("#salary-form").submit();
                }

            }

            // if(month == "" ){
            //     warningNotificationCustom('please select month ');
            // }

            console.log('slary genrated button clicked ',"region "+region+"   " + month);

        });
        $('#clearance-status').change(function () {
            // $this.val();
            var sel = $('#clearance-status').val();
            console.log(sel);
            if( sel == 2){

                $('#clearance-image-div').css('display','none');
            }else{
                $('#clearance-image-div').css('display','block');
            }

        });

        getGuardsData = function (parwest_id) {
            var month = $("#extrahour_month").val();
            console.log('month ',month)
            if(month){
                var data = {
                    'parwest_id': parwest_id,
                    'month': month,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetails',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                if (result.responseCode == 1) {

                    // $('#guard_name').val(result.data.guard[0].name);
                    // $('#guards_phone').val(result.data.guard[0].contact_no);
                    // $('#guards_current_supervisor').val(result.data.branch_supervisor);
                    // $('#current_deployment').val(result.data.branch_name + ''+result.data.client_name);
                    // $('#deployment_days').val(result.data.days);
                    // console.log(result.data.guard);
                    // $('#guardLoanHistory').css('display', 'block');
                    // console.log('loan data ', result.data.loan[0]);
                    // $.each(result.data.loan, function (index, value) {
                    //     console.log(index + ": " + value);
                    //     var markup = "";
                    //     markup += '<tr id="history">' +
                    //         '<td >' + value.parwest_id + '</td>' +
                    //         '<td >' + value.guard_name + '</td>' +
                    //         '<td >' + value.current_deployment + '</td>' +
                    //         '<td >' + value.deployment_days + '</td>' +
                    //         '<td >' + value.guards_phone + '</td>' +
                    //         '<td >' + value.guards_current_supervisor + '</td>' +
                    //         '<td >' + value.amount_paid + '</td>' +
                    //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                    //         '<td >' + value.payment_location + '</td>' +
                    //         '<td >' + value.slip_number_loan + '</td>' +
                    //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                    //         '<td class="table-heading2 finalize">'+
                    //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                    //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                    //         '</a> </button> '+ ' </td>' +
                    //         '</tr>';
                    //     $("#deploymentDetailsTableBody").append(markup);
                    // });
                    //
                    // $('#guards_supervisor_loaned').html('');
                    // $('#guards_supervisor_loaned').append($('<option>', {
                    //     value: 0,
                    //     text: 'ffffff',
                    // }));
                    //
                    // $.each(result.data.supervisors.data, function (key, value) {
                    //
                    //     $('#guards_supervisor_loaned').append($('<option >', {
                    //         value: value.status,
                    //         value: value.id,
                    //         text: value.name,
                    //
                    //     }));
                    //
                    // });
                    successNotificationCustom(result.message);
                }
            }else {
                $("#extrahour_month").css("border-color", "#f56942");
                warningNotificationCustom('Guard Number Not Exists ');
            }

        };


        removeGuardDetails = function () {
            // console.log('hamza');
            //
            // $('#guard_name').val('');
            // $('#guards_supervisor_loaned').html('');
            // $('#guards_supervisor_loaned').empty().append($('<option>', {
            //     value: 0,
            //     text: '--Select Branch--',
            // }));
        };
        getGuardsDeploymentData = function (month) {
            var parwest  = $('#extrahour_parwest_id').val();
            if(parwest){
                console.log('i am here in ');
                $("#extrahour_month").css("border-color", "gray");
                var data = {
                    'parwest_id': parwest,
                    'month': month,
                }
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetailsForExtraHours',
                    'type': 'GET',
                    'data': data,
                }

                var result = centralAjax(object);
                console.log(result);
                if (result.responseCode == 1) {
                    $('#extrahour_guard_name').val(result.data.guard.name);
                    $('#extrahour_status').val(result.data.guard.status_name);
                    // $('#guards_phone').val(result.data.guard[0].contact_no);
                    // $('#guards_current_supervisor').val(result.data.branch_supervisor);
                    $('#extrahour_current_location').val(result.data.branch_name + ''+result.data.client_name);
                    $('#extrahour_type').val(result.data.guard_type.name);
                    // console.log(result.data.guard);
                    // $('#guardLoanHistory').css('display', 'block');
                    // console.log('loan data ', result.data.loan[0]);
                    // $.each(result.data.loan, function (index, value) {
                    //     console.log(index + ": " + value);
                    //     var markup = "";
                    //     markup += '<tr id="history">' +
                    //         '<td >' + value.parwest_id + '</td>' +
                    //         '<td >' + value.guard_name + '</td>' +
                    //         '<td >' + value.current_deployment + '</td>' +
                    //         '<td >' + value.deployment_days + '</td>' +
                    //         '<td >' + value.guards_phone + '</td>' +
                    //         '<td >' + value.guards_current_supervisor + '</td>' +
                    //         '<td >' + value.amount_paid + '</td>' +
                    //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                    //         '<td >' + value.payment_location + '</td>' +
                    //         '<td >' + value.slip_number_loan + '</td>' +
                    //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                    //         '<td class="table-heading2 finalize">'+
                    //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                    //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                    //         '</a> </button> '+ ' </td>' +
                    //         '</tr>';
                    //     $("#deploymentDetailsTableBody").append(markup);
                    // });
                    //
                    // $('#guards_supervisor_loaned').html('');
                    // $('#guards_supervisor_loaned').append($('<option>', {
                    //     value: 0,
                    //     text: 'ffffff',
                    // }));
                    //
                    // $.each(result.data.supervisors.data, function (key, value) {
                    //
                    //     $('#guards_supervisor_loaned').append($('<option >', {
                    //         value: value.status,
                    //         value: value.id,
                    //         text: value.name,
                    //
                    //     }));
                    //
                    // });
                    successNotificationCustom(result.message);
                }
                // var data = {
                //     'month': month,
                //     'parwest_id': parwest,
                // };
                // var object = {
                //     'url': globalPublicPath + '/guard/getGuardsDeploymentData',
                //     'type': 'GET',
                //     'data': data,
                // };
                //
                // var result = centralAjax(object);
                // console.log(result);
                // if (result.responseCode == 1) {
                //
                //     // $('#guard_name').val(result.data.guard[0].name);
                //     // $('#guards_phone').val(result.data.guard[0].contact_no);
                //     // $('#guards_current_supervisor').val(result.data.guard[0].guard_supervisor_name);
                //     $('#current_deployment').val('meezan bank');
                //     $('#deployment_days').val('5');
                //     console.log(result);
                //     // $('#guardLoanHistory').css('display', 'block');
                //     // console.log('loan data ', result.data.loan[0]);
                //     // $.each(result.data.loan, function (index, value) {
                //     //     console.log(index + ": " + value);
                //     //     var markup = "";
                //     //     markup += '<tr id="history">' +
                //     //         '<td >' + value.parwest_id + '</td>' +
                //     //         '<td >' + value.guard_name + '</td>' +
                //     //         '<td >' + value.current_deployment + '</td>' +
                //     //         '<td >' + value.deployment_days + '</td>' +
                //     //         '<td >' + value.guards_phone + '</td>' +
                //     //         '<td >' + value.guards_current_supervisor + '</td>' +
                //     //         '<td >' + value.amount_paid + '</td>' +
                //     //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                //     //         '<td >' + value.payment_location + '</td>' +
                //     //         '<td >' + value.slip_number_loan + '</td>' +
                //     //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                //     //         '<td class="table-heading2 finalize">'+
                //     //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                //     //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                //     //         '</a> </button> '+ ' </td>' +
                //     //         '</tr>';
                //     //     $("#deploymentDetailsTableBody").append(markup);
                //     // });
                //
                //     // $('#guards_supervisor_loaned').html('');
                //     // $('#guards_supervisor_loaned').append($('<option>', {
                //     //     value: 0,
                //     //     text: 'ffffff',
                //     // }));
                //
                //     // $.each(result.data.supervisors.data, function (key, value) {
                //     //
                //     //     $('#guards_supervisor_loaned').append($('<option >', {
                //     //         value: value.status,
                //     //         value: value.id,
                //     //         text: value.name,
                //     //
                //     //     }));
                //     //
                //     // });
                //     successNotificationCustom(result.message);
                // }

            }else{
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        };


        removeGuardDeploymentDetails = function () {
            // console.log('hamza');

            // $('#guard_name').val('');
            // $('#guards_supervisor_loaned').html('');
            // $('#guards_supervisor_loaned').empty().append($('<option>', {
            //     value: 0,
            //     text: '--Select Branch--',
            // }));
        };

        function myMap() {
            var myLatLng = {lat: 51.50874, lng: -0.120850};
            var mapProp= {
                center:new google.maps.LatLng(51.508742,-0.120850),
                zoom:5,
                zoomControl: false,
                mapTypeControl: false,

            };
            var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });
        }

        $(document).ready(function() {
            $( "#datepicker" ).datepicker();
            $( "#datepicker2" ).datepicker();
            $('.select3').select2(); });
    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&callback=myMap"></script>


@endsection
