{{-- Extends layout --}}
@extends('layout.default')
@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <?php $count = 0;?>
    <!--begin::Content-->
    <style>
        .card-header:hover .etiedhovee {
            color: #3c9bd1;
        }

        .fghfh {
            margin: 1px -149px;
            width: 124%;
        }

        .sdfds {
            /*background: #3c9bd1;*/
            padding: 5px 0px;
            font-weight: 500;
            border-radius: 3px;
            color: white;
            margin: -2px;
            height: 100%;
        }

        span.sdfsfw {
            /* width: 24px !important; */
            background: white;
            margin-right: 12px;
            padding: 1px 8px;
            border-radius: 50%;
            color: #1e1e2d;
            font-weight: bolder;
        }

        span.adiwe {
            width: 20%;
            background: #3c9bd1;
            height: 6px;
            margin: 4px;
            border-radius: 10px;
        }

        .sdfq {
            background: #c5c5c5 !important;
        }

        .mgko {
            margin: 5px 66px;
        }

        .asdfasdf {
            background: #c5c5c505;
            padding: 13px 14px;
            border-radius: 7px;
            border: 1px solid #8b8b981f;
            box-shadow: 0px 1px 5px 0px #10101029;
            margin-top: 14px;
        }

        .owpierjpwoq {
            font-size: 52px;
            color: #3c9bd1;
            margin-top: 11px;
            margin-right: 19px;
        }

        button.asdfy {
            background: #3c9bd1;
            border: none;
            color: white;
            padding: 6px 25px;
            border-radius: 3px;
            margin-top: 65px;

            margin-bottom: 23px;
            margin-right: 12px;
            font-weight: bold;
        }

        button.osijdfow {
            background: no-repeat;
            border: 1px solid #1e1e2d;
            padding: 11px 52px;
            font-weight: bold;
        }

        button.osijdfow:hover {
            background: #1e1e2d;
            border: 1px solid #1e1e2d;
            padding: 11px 52px;
            font-weight: bold;
            color: white;
        }

        ul.sdfsfuu li {
            margin-top: 5px;
        }

        ul.sdfsfuu {
            margin-bottom: 13px !important;
            margin-top: 5px;
            list-style: none;
        }

        span.addcarcs {
            border: 1px solid #3c9bd1;
            padding: 10px 18px;
            color: #3c9bd1;
            font-weight: bold;
            margin-left: 16px;
            height: 42px;
            width: 123px !important;
            display: table;
            text-align: center;
        }

        .hicarscol {
            overflow: hidden;
            overflow-x: auto;
            overflow-y: hidden;
        }

        .hicarscol::-webkit-scrollbar {
            height: 5px;
        }

        .hicarscol::-webkit-scrollbar-thumb {
            background: #3c9bd1;
            border-radius: 7px;
        }

        .table::-webkit-scrollbar {
            height: 5px;
        }

        .table::-webkit-scrollbar-thumb {
            background: #3c9bd1;
            border-radius: 7px;
        }

        .sidecomdet {
            color: #3c9bd1;
            font-size: 16px;
            margin-top: 6px;
            margin-right: 8px;
        }

        span.companginfo {
            font-size: 13px;
            margin-top: 4px !important;

            font-weight: bold;
            color: #1e1e2d;
        }

        button.btn.slectdays {
            border: 1px solid #dad3d3;
            background: white;
        }

        .activesidebar {
            background: #1e1e2d26;
            padding: 5px 9px;
            border-radius: 6px;
        }

        img.checkoutimge {
            width: 134px;
            margin-left: 1px;
            margin-bottom: 19px;
            border: 2px solid #f9f9f9;
            border-radius: 5px;
            box-shadow: 1px 1px 4px #888484;
            padding: 9px;
            height: 77px;
        }

        .topbuttons {
            background: white;
            border: none;
            border-bottom: 4px solid #3c9bd1;
            color: #3c9bd1;
            font-weight: bold;
            font-size: 12px;
            height: 50px;
            margin: 0px 5px;
            margin-top: 2px;
        }

        .topbuttons-active {
            border: none;
            color: #ffffff;
            font-weight: bold;
            font-size: 12px;
            height: 50px;
            margin: 0px 5px;
            background-image: linear-gradient(#3c9bd1, #3c9bd166);
            margin-top: 2px;
            border-top-right-radius: 5px;
            border-top-left-radius: 5px;
        }


        /*car information css*/
        #carstabs li {
            border: 1px solid #3c9bd1;
            margin-right: 5px;
            min-width: 106px;
        }

        #carstabs li a.active {
            color: #ffffff !important;
            text-decoration: none;
            background: #3c9bd1;
        }

        #carstabs {
            flex-wrap: nowrap !important;
        }

        #carstabs li a {
            color: #3c9bd1;
            text-decoration: none;
        }

        .deletcars-icon {
            position: absolute;
            right: -6px;
            top: -7px;
            background: #fefefe;
            padding: 4px;
            border-radius: 50%;
            color: #3c9bd1;
            cursor: pointer;
        }


        /*!* Handle *!*/
        /*::-webkit-scrollbar-thumb {*/
        /*    background: red;*/
        /*    border-radius: 15px;*/
        /*}*/

        /*!* Handle on hover *!*/
        /*::-webkit-scrollbar-thumb:hover {*/
        /*    background: #b30000;*/
        /*}*/

        /*p.sdfds:after {*/
        /*    left: 94%;*/
        /*    top: 48.5%;*/
        /*    -webkit-transform: translateY(-50%);*/
        /*    transform: translateY(-50%);*/
        /*    content: " ";*/
        /*    height: 0;*/
        /*    width: 0;*/
        /*    border: solid transparent;*/
        /*    position: absolute;*/
        /*    border-left-color: #ebedf3;*/
        /*    border-width: 0.9rem;*/
        /*}*/
        .tick-contorler .fa-check {
            color: green !important;
        }

        .showatsm {
            display: none;
        }

        @media screen and (max-width: 1199px) {
            .hideatsm {
                display: none;
            }

            .showatsm {
                display: block;
            }

        }
    </style>

    <div id="overlay" class="container-fluid bg-white    rounded px-0  "
         style="justify-content: center; padding: 254px 0px;text-align: center;">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>

    {{--    <form name="photo" id="imageUploadForm" enctype="multipart/form-data" action="/upload-img" method="post">--}}
    {{--        @csrf--}}
    {{--        <input type="file" style="widows:0; height:0" id="ImageBrowse" name="image" size="30"/>--}}
    {{--        <input type="submit" name="upload" value="Upload"  id="upload"/>--}}
    {{--        <img width="100" style="border:#000; z-index:1;position: relative; border-width:2px; float:left" height="100px" src="<?php echo @$upload_path.$large_image_name.$_SESSION['user_file_ext'];?>" id="thumbnail"/>--}}
    {{--    </form>--}}

    {{--    <script>--}}

    {{--        $(document).ready(function (e) {--}}
    {{--            $('#upload').on('click',(function(e) {--}}
    {{--                e.preventDefault();--}}
    {{--                var formData = $('ImageBrowse').val();--}}
    {{--                // var formData = new FormData(this);--}}

    {{--                $.ajax({--}}
    {{--                    type:'POST',--}}
    {{--                    url: globalPublicPath +'/img',--}}
    {{--                    data:formData,--}}
    {{--                    cache:false,--}}
    {{--                    contentType: false,--}}
    {{--                    processData: false,--}}
    {{--                    success:function(data){--}}
    {{--                        console.log("success");--}}
    {{--                        console.log(data);--}}
    {{--                    },--}}
    {{--                    error: function(data){--}}
    {{--                        console.log("error");--}}
    {{--                        console.log(data);--}}
    {{--                    }--}}
    {{--                });--}}
    {{--            }));--}}

    {{--            $("#ImageBrowse").on("change", function() {--}}
    {{--                $("#imageUploadForm").submit();--}}
    {{--            });--}}
    {{--        });--}}




    <div id="allshowcontainer" class="container-fluid bg-white d-none rounded ">
        <div class="row  ">
            <div class="col-lg-12 showatsm p-0 ">
                <div class="d-flex w-100">
                    <button type="button" onclick="editecompaign()" class="px-0 topbuttons topbuttons-active  w-100">
                        Compaign Info
                    </button>
                    <button type="button" onclick="editelocaiton()" class="px-0 topbuttons w-100">Location Info</button>
                    <button type="button" onclick="editeacrs()" class="px-0 topbuttons  w-100">Car Info (<span
                            class="totalcarprevew">0</span>)
                    </button>
                    <button type="button" onclick="stickeredite()" class="px-0 topbuttons w-100">Sticker Info</button>
                    <button type="button" onclick="previwcomdetils()" class="px-0 topbuttons w-100">Preview</button>
                </div>
            </div>
        </div>
        <div class="row mb-n1">
            <div class="col-xl-2 hideatsm   px-n1" style="background: #3c9bd1;margin-left: -13px;">
                <div class="sdfds tick-contorler  pt-5">
                    <p id="sidebara1" class="sidebardivss activesidebar cursor-pointer d-flex"
                       onclick="editecompaign()">
                        <i class="fas fa-exclamation-circle sidecomdet text-white"></i>
                        <span class="companginfo text-white sdsdsdw">Compaign Details</span>
                    </p>
                    <ul class="sdfsfuu sdfsyty pb-2 mb-n1">
                        <li>Overview</li>
                        <li>Objective</li>
                    </ul>
                    <p id="sidebara2" class="sidebardivss d-flex  cursor-pointer" onclick="editelocaiton()">
                        <i class="fas fa-exclamation-circle sidecomdet text-white"></i>
                        <span class="companginfo text-white">Location Details</span></p>
                    <ul class="sdfsfuu  pb-2 mb-n1">
                        <li>Locaiton info</li>

                    </ul>
                    <p id="sidebara3" class="sidebardivss d-flex  cursor-pointer" onclick="editeacrs()">
                        <i class="fas fa-exclamation-circle sidecomdet text-white"></i><span
                            class="companginfo text-white">Car info</span></p>
                    <ul class="sdfsfuu pb-2 mb-n1">
                        <li>Car Overview</li>
                        <li>Cars (<span class="totalcarprevew">0</span>)</li>
                    </ul>
                    <p id="sidebara4" class="sidebardivss d-flex cursor-pointer" onclick="stickeredite()">
                        <i class="fas fa-exclamation-circle  sidecomdet text-white"></i><span
                            class="companginfo text-white">Sticker Detials</span></p>
                    <ul class="sdfsfuu pb-2 mb-n1">
                        <li>Sticker Overview</li>
                        <li>Sticker Details</li>
                    </ul>
                    <p id="sidebara5" class="sidebardivss d-flex cursor-pointer" onclick="previwcomdetils()">
                        <i class="fas fa-exclamation-circle sidecomdet text-white"></i><span
                            class="companginfo text-white">Preview</span></p>
                    <ul class="sdfsfuu pb-2 mb-n1">
                        <li>All details</li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-10 pl-2 bg-white">
                <div class="row ">
                    <div class="col-lg-12 mt-2">
                        <h1 class="mt-3">Create Campaign</h1>
                        <hr>
                        {{--                        <h4 class="mt-5 pt-2">Campaign Objective</h4>--}}
                    </div>

                </div>

                <div class="row ">
                    <div class="col-lg-12">
                        <div class="d-flex " style="justify-content: center;">
                            <span id="progresbb1" class="adiwe"></span>
                            <span id="progresbb2" class="adiwe sdfq"></span>
                            <span id="progresbb3" class="adiwe sdfq"></span>
                            <span id="progresbb4" class="adiwe sdfq"></span>
                            <span id="progresbb5" class="adiwe sdfq"></span>
                        </div>
                    </div>
                </div>
                <div id="stepdiv1" style="min-height: 490px" class="row mt-3 allstepsdivs">
                    <div class="col-lg-8 ">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="mt-5 pt-2 ">Campaign Overview</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="asdfasdf">
                                    <div class="row">
                                        <!--begin::Input-->
                                        @csrf
                                        <div class="form-group col-md-12">
                                            <label>Campaign Title:</label>
                                            <input type="text" class="form-control " name="Campaign_tittle"
                                                   maxlength="35"
                                                   id="Campaign_tittle" notify="Campaign Title"
                                                   onkeyup="getpassvalue(this.value,'showtittlecard1','')"
                                                   placeholder="Campaign Tittle" value="">
                                            <span class="form-text text-danger d-none " id="tittle_error">This field is required.</span>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Campaign Type:</label>
                                            <select name="type" class="form-control" id="campaign_type"
                                                    notify="Campaign Type"
                                                    onchange="getpassvalue(this.value,'compaintype','')">
                                                <option value="" selected disabled>Choose Campaign Type</option>
                                                @foreach($data['campaign_types'] as $campaign_type)
                                                    <option  value="{{$campaign_type->id}}">{{$campaign_type->name}}</option>
                                                @endforeach

                                            </select>
                                            <span class="form-text text-danger d-none" id="campaign_type_error">This field is required.</span>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Duration:</label>
                                            <select name="duration" class="form-control" id="campaign_duration"
                                                    notify="Duration"
                                                    onchange="getpassvalue(this.value,'timedurationcom','')">
                                                <option value="" selected="">Select Time Duration</option>
                                                <option value="Days">Days</option>
                                                <option value="Week">Week</option>
                                                <option value="Months">Months</option>
                                            </select>
                                            <span class="form-text text-danger d-none" id="campaign_duration_error">This field is required.</span>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Interval:</label>
                                                <input name="Time_interval" class="form-control" notify="Interval"
                                                       onchange="getpassvalue(this.value,'timeintervalcom','')"
                                                       id="kt_daterangepicker_1" readonly="readonly"
                                                       placeholder="Set time interval" type="text">
                                                <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="asdfasdf ueytui " style="margin-top: 50px;">
                            <h4>Campaign Detail</h4>
                            <div class="d-flex   pb-1" style="justify-content: space-between;">
                                     <span class="pt-2">
                                              <small class="mb-1 mt-5"><b></b></small>
{{--                                          <h4>8,090 People</h4>--}}
                                     </span>
                                <span>
                                            <span class="uwpoms"> <i
                                                    class="fas fa-tachometer-alt mr-n1 owpierjpwoq"></i></span>
                                     </span>
                            </div>
                            <table class="table table-responsive">
                                <tr>
                                    <td colspan="2"><b class="pr-3">Campaign Title:</b> <span
                                            class="showtittlecard1"></span>
                                    </td>

                                </tr>
                                <tr>
                                    <th>Type:</th>
                                    <td class="compaintype"></td>
                                    <script>

                                    </script>
                                </tr>
                                <tr>
                                    <th>Time Duration:</th>
                                    <td class="timedurationcom"></td>
                                </tr>
                                <tr>
                                    <th>Time Interval:</th>
                                    <td class="timeintervalcom"></td>
                                </tr>
                                <tr>
                                    <th>Total Days:</th>
                                    <td class="totaldays"></td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>
                <div id="stepdiv2" style="min-height: 490px" class="row mt-3 allstepsdivs ">
                    <div class="col-lg-8 ">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="clearfix">
                                    <h5 class="mt-5 pt-2 float-left">Location info</h5>
                                    <input type="hidden" value="2" id="addinglocationvalue">
                                    <input type="hidden" value="2" id="detectlocationdelet">
                                    <input type="hidden" value="2" id="firstaddlocaitonscheck">
                                    <button id="btnAddLocationTab" class="float-right btn btn-primary mt-5 mr-2">ADD
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="asdfasdf">
                                    <ul class="nav nav-tabs " id="tabLocationInfo" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home"
                                               role="tab" aria-controls="home" aria-selected="true">Location 1</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="tabContenttabLocationInfo">
                                        <div class="tab-pane fade show active" id="home" role="tabpanel"
                                             aria-labelledby="home-tab">
                                            <div class="tab-inner">
                                                <div class="row border-bottom ">
                                                    <p class="px-3 font-weight-bold pt-2 mb-0 w-100 ">Location 1 </p>
                                                </div>
                                                <div class="row mt-n2">
                                                    <!--begin::Input-->
                                                    <div class="col-lg-6">
                                                        <div class="form-check pl-0">
                                                            <label class="mt-5">Country:</label>
                                                            <select disabled name="countries[]" id="country_name_1"
                                                                    notify="Country"
                                                                    onchange="showValues();"
                                                                    class="form-control contrydisable drope"
                                                                    id="contrydisableid">
                                                                <option value="" disabled>
                                                                    Select Country
                                                                </option>
                                                                <option value="1" selected>America</option>
                                                                <option value="2">Australia</option>
                                                                <option value="3">England</option>
                                                            </select>
                                                            <span class="form-text text-danger d-none mb-3"
                                                                  id="country_name_error">Select Country.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-check pl-0">
                                                            <label class="mt-5">State:</label>
                                                            <select name="states[]" id="states_1"
                                                                    class="form-control  statedisable drope"
                                                                    notify="Country"
                                                                    id="statedisableid" onchange="getCitiess('1');">
                                                                <option value="" selected disabled>
                                                                    Select state
                                                                </option>
                                                                @foreach($data['states'] as $state)
                                                                    <option
                                                                        value="{{$state->id}}">{{$state->state}}</option>
                                                                @endforeach

                                                            </select>
                                                            <span class="form-text text-danger d-none mb-3"
                                                                  id="state_error">This field is required.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="form-check pl-0">
                                                            <label class="d-block mt-5">City:</label>
                                                            <select name="cities[]" id="city_1"
                                                                    class="form-control cityecolect cities-drop drope"
                                                                    notify="Country"
                                                                    onchange="getZips('1')">
                                                                <option value="" selected disabled>
                                                                    Select City
                                                                </option>
                                                            </select>
                                                            <span class="form-text text-danger d-none mb-3"
                                                                  id="city_error">This field is required.</span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 w-100">
                                                        <div class="form-check pl-0 w-100">
                                                            <label class="d-block mt-5">Zip:</label>
                                                            <select onchange="slectallzips()" id="zips_1"
                                                                    class="select2 form-control findzips drope"
                                                                    notify="Country"
                                                                    style="width:200px !important;" name="zips[]"
                                                                    multiple>
                                                            </select>
                                                            <span class="form-text text-danger d-none mb-3"
                                                                  id="findzips_error">Select Zip code</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-4 ">
                        <div class="asdfasdf ueytui " style="margin-top: 61px;">
                            <h4 onclick="locationDetails()">Locaiton Details</h4>
                            <div class="d-flex border-bottom mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b></b></small>
                                          <h4></h4>
                                     </span>
                                <span>
                                    <span class="uwpoms"> <i
                                            class="fas fa-map-marked-alt mr-n2 owpierjpwoq"></i></span>
                                     </span>
                            </div>

                            <div class="w-100">
                                <table>
                                    <tr>
                                        <th>Total Locations:</th>
                                        <td class="totallocaitionprevew">1</td>
                                    </tr>
                                </table>
                                <div id="dvMap" style="width: 100%; height: 250px"><b></b></div>
                                {{-- <img src="{{asset('/media/mapimge.png')}}" id="loadimgedd" class=" img-thumbnail"  alt="">--}}
                                <script>
                                    function slectallzips() {
                                        var allzips = [];
                                        var zipget = $('.findzips').select2('data');
                                        // console.log(zipget);
                                        // console.log(allzips);
                                        refrashmap(allzips);
                                    }

                                </script>
                            </div>

                        </div>
                    </div>
                </div>
                <div id="stepdiv3" style="min-height: 490px" class="row mt-3 allstepsdivs">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="mt-5 pt-2 ">Car Info</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" value="2" id="carListHandler">
                                <div class="asdfasdf hicarscol">
                                    <ul class="nav " id="carstabs" role="tablist">
                                        <li class="nav-item" id="tabdeletid1" role="presentation">
                                            {{--                                            <i class="fas fa-times deletcars-icon"></i>--}}
                                            <a class="nav-link active position-relative" id="car-tab1" data-toggle="tab"
                                               href="#carbtn1"
                                               role="tab" aria-controls="carbtn1" aria-selected="false">Car 001 </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="asdfasdf">
                                    <div class="tab-content" id="car-tabs-warap">
                                        <div class="tab-pane fade  show active" id="carbtn1" role="tabpanel"
                                             aria-labelledby="car-tab1">
                                            <div class="row border-bottom">
                                                <div class="col-lg-12">
                                                    <div class="clearfix">
                                                        <span
                                                            class="mb-1 float-left  pb-2"><b>Car 001 Details</b></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        {{--                                                        onchange="getZipss('1')"--}}
                                                        <label class="d-block">Choose Location:</label>
                                                        <select class="form-control w-100 getlocations"
                                                                notify="Country" name="location[]" id="car_location_1">
                                                        </select>

                                                        <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                        <input type="hidden" class="hiddenstatefinder"
                                                               id="getstateaa_1">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="d-block">Zips:</label>
                                                        <select
                                                            class="select2 form-control  w-100  selected_zips"
                                                            multiple style="width:200px !important;" name="carZip[]"
                                                            notify="Country"
                                                            id="selected_zips_1">
                                                            <option value="" selected>
                                                                No Zip codes
                                                            </option>
                                                        </select>
                                                        <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Number of Cars :</label>
                                                        <input type="number" min="1"
                                                               class="form-control getvals numberofcars ui-autocomplete-input"
                                                               name="carQuntity[]" id="no_of_cars_1" placeholder="Cars"
                                                               autocomplete="off">
                                                        <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Car Maker:</label>
                                                        <select name="carMake[]" id="car_make_1"
                                                                class="form-control carMake" onnotify="Country">
                                                            <option value="" selected>Select Maker</option>
                                                            @foreach($Cars as $Car)
                                                                <option
                                                                    value="{{$Car->company_id}}">{{$Car->maker->company}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                    </div>
                                                </div>


                                                <div class="col-lg-6">
                                                    <div class="form-check pl-0">
                                                        @include('pages.advertisor.partials1._car_models')
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-check pl-0">
                                                        @include('pages.advertisor.partials1._car_year')
                                                    </div>
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group ">
                                                        @include('pages.advertisor.partials1._car_type')
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 pt-5 mb-0">
                                                    <label>Mileage Drive:</label>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <div class="ion-range-slider">
                                                            <input type="hidden" name="mileage[]" data-id='1'
                                                                   data-validation='0' id="mileage_1"
                                                                   class="kt_slider_a getvals form-control"/>
                                                            <span> </span>
                                                            <span class="form-text text-success milage-notification mt-2">Select Mileage to get your cost.</span>
                                                            <input type="hidden" name="price_per_mile[]"
                                                                   class="mailage-rate getvals form-control"
                                                                   value="">
                                                            <span class="form-text text-danger d-none">This field is required.</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <label for="">How Often Drive A Car:</label>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="btn-group mr-2" role="group"
                                                         style="display: contents !important;" aria-label="First group">
                                                        <button type="button" class="btn  slectdays ">
                                                            <input type="checkbox" name="days_1" value="Monday"
                                                                   class="d-none form-control carinputs">Mon
                                                        </button>
                                                        <button type="button" class="btn  slectdays  ">
                                                            <input type="checkbox" name="days_1" value="Tuesday"
                                                                   class="d-none form-control carinputs">Tue
                                                        </button>
                                                        <button type="button" class="btn  slectdays ">
                                                            <input type="checkbox" name="days_1" value="Wednesday"
                                                                   class="d-none form-control carinputs">Wed
                                                        </button>
                                                        <button type="button" class="btn  slectdays ">
                                                            <input type="checkbox" name="days_1" value="Thursday"
                                                                   class="d-none form-control carinputs">Thu
                                                        </button>
                                                        <button type="button" class="btn  slectdays ">
                                                            <input type="checkbox" name="days_1" value="Friday"
                                                                   class="d-none form-control carinputs">Fri
                                                        </button>
                                                        <button type="button" class="btn  slectdays ">
                                                            <input type="checkbox" name="days_1"
                                                                   value="Saturday"
                                                                   class="d-none form-control carinputs">Sat
                                                        </button>
                                                        <button type="button" class="btn  slectdays ">
                                                            <input type="checkbox" name="days_1" value="Sunday"
                                                                   class="d-none form-control carinputs">Sun
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <input type="hidden" class="statedetails getvals form-control" name="stateid[]">
                                                <input type="hidden" class="citydeils getvals form-control" name="cityid[]">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-lg-12">
                                            <button id="addmorecars" class="asdfy m-4 float-right">Add more cars
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="asdfasdf ueytui " style="margin-top: 61px;">
                            <h4 onclick="carDetailss()">Car Details</h4>
                            <div class="d-flex  mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
{{--                                           <small class="mb-1 mt-5"><b>Car Make</b></small>--}}
                                         {{--                                          <h4>Suzuki</h4>--}}
                                     </span>
                                <span><span class="uwpoms"> <i class="fas fa-car owpierjpwoq mr-n2"></i></span>
                                     </span>
                            </div>
                            <table class="table w-100 table-responsive">

                                <tr>
                                    <th>Total Cars:</th>
                                    <td class="totalcarprevew">1</td>
                                </tr>

                            </table>


                        </div>
                    </div>
                </div>
                <div id="stepdiv4" style="min-height: 490px" class="row mt-3 allstepsdivs">
                    <div class="col-lg-8 ">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="mt-5 pt-2 ">Create A Sticker</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="asdfasdf">

                                    <div class="alert alert-custom alert-light-danger fade show mb-10 d-none"
                                         role="alert">
                                        <div class="alert-text font-weight-bold">
                                            <ul class="float-left" id="error"></ul>

                                        </div>
                                        {{--                                        <div class="alert-close">--}}
                                        {{--                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">--}}
                                        {{--                                                <span aria-hidden="true">--}}
                                        {{--                                                    <i class="ki ki-close"></i>--}}
                                        {{--                                                </span>--}}
                                        {{--                                            </button>--}}
                                        {{--                                        </div>--}}

                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <label>Company:</label>
                                                <input type="text" onkeyup="getpassvalue(this.value,'campanysts','')"
                                                       class="form-control " notify="Country"
                                                       id="company_details"
                                                       name="l_title" placeholder="Enter Company Name" maxlength="45"/>
                                                <span class="form-text text-danger d-none " id="tittle_error">This field is required.</span>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <div class="col-xl-6">
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <label>Slogan:</label>
                                                <input type="text" class="form-control "
                                                       onkeyup="getpassvalue(this.value,'slogans','')" notify="Country"
                                                       id="slogan"
                                                       name="l_title54"
                                                       placeholder="Enter Slogan Name" maxlength="45"/>
                                                <span class="form-text text-danger d-none " id="tittle_error">This field is required.</span>
                                                {{--                                                                    <span--}}
                                                {{--                                                                        class="form-text text-muted">Sub title of logo.</span>--}}
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <div class="col-xl-6">
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <label>Contact:</label>
                                                <input type="number" class="form-control "
                                                       onkeyup="getpassvalue(this.value,'contectstc','')"
                                                       notify="Country" id="contact"
                                                       name="l_title2"
                                                       placeholder="Enter Contact Details" maxlength="15"/>
                                                <span class="form-text text-danger d-none " id="tittle_error">This field is required.</span>
                                                {{--                                                                    <span class="form-text text-muted">Contact</span>--}}
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="form-group">
                                                <label>Sticker Details: </label>
                                                <input type="text" class="form-control "
                                                       onkeyup="getpassvalue(this.value,'stcdetailss','')"
                                                       id="sticker_details"
                                                       name="l_title2"
                                                       placeholder="Enter Sticker Details" maxlength="205"/>
                                                <span class="form-text text-danger d-none " id="tittle_error">This field is required.</span>
                                                {{--                                                                    <span--}}
                                                {{--                                                                        class="form-text text-muted">Sticker Details</span>--}}
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Upload Imaged</label>
                                                <br>
                                                <form name="photo" id="imageUploadForm" enctype="multipart/form-data"
                                                      action="{{route('user.uploadSticker')}}" method="post">
                                                    @csrf
                                                    <img src="{{asset('/media/uploadsticker.jpg')}}" id="loadimgedds"
                                                         class="img-thumbnail" onclick="slctstiker()" alt="">
                                                    <input type="file" class="d-none form-control" name="img"
                                                           id="uploadstickerimge" accept="image/*">
                                                    <input type="hidden" name="cmp_id" id="cmp-id">
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 ">
                        <div class="asdfasdf ueytui " style="margin-top:61px;">
                            <h4>Sticker Details</h4>
                            <div class="d-flex  mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b></b></small>
                                          <h4></h4>
                                     </span>
                                <span>
                                            <span class="uwpoms"> <i class="fas fa-image owpierjpwoq"></i></span>
                                     </span>
                            </div>
                            <table class="table table-responsive">
                                <tr>
                                    <th>Company:</th>
                                    <td class="campanysts">--</td>
                                </tr>
                                <tr>
                                    <th>Slogan:</th>
                                    <td class="slogans">--</td>
                                </tr>
                                <tr>
                                    <th>Contact:</th>
                                    <td class="contectstc">--</td>
                                </tr>
                                <tr>
                                    <th>Sticker Details:</th>
                                    <td class="stcdetailss">--</td>
                                </tr>
                            </table>


                        </div>
                    </div>
                </div>
                <div id="stepdiv5" style="min-height: 490px" class="row mt-3 allstepsdivs">
                    <div class="col-lg-12 ">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5 class="mt-5 pt-2 ">Compaign Preview</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="asdfasdf" style=" min-height: 592px !important;">
                                    <p class="border-bottom">Compaign Details <i onclick="editecompaign()"
                                                                                 title="Edit"
                                                                                 class="fas fa-edit float-right cursor-pointer"></i>
                                    </p>
                                    <div class="col-lg-12 ">
                                        <div class=" ueytui " style="margin-top: 38px;margin-right: 16px;">
                                            <h4>Campaign Metrics</h4>
                                            <div class="d-flex border-bottom mb-4 pb-2"
                                                 style="justify-content: space-between;">
                                     <span class="pt-5">
{{--                                           <small class="mb-1 mt-5"><b>Potential Reach</b></small>--}}
                                         {{--                                          <h4>8,090 People</h4>--}}
                                     </span>
                                                <span>
                                            <span class="uwpoms"> <i
                                                    class="fas fa-tachometer-alt owpierjpwoq"></i></span>
                                     </span>
                                            </div>
                                            <span>
                                           <small class="mb-1 mt-2"><b>Campaign Title:</b></small>
                                          <h5 class="mb-5 showtittlecard1">Commercial</h5>
                                     </span>
                                            <span>
                                           <small class="mb-1 mt-2"><b>Campaign Type:</b></small>
                                          <h5 class="mb-5 compaintype">Weak</h5>
                                     </span>
                                            {{--                                                   <span>--}}
                                            {{--                                           <small class="mb-1 mt-4"><b>Campaign Ends</b></small>--}}
                                            {{--                                          <h5 class="mb-5">30-jan-2020</h5>--}}
                                            {{--                                     </span>--}}
                                            <span>
                                           <small class="mb-1 mt-4"><b>Duration:</b></small>
                                          <h5 class="mb-5 timedurationcom"></h5>
                                     </span>
                                            <span>
                                           <small class="mb-1 mt-4"><b>Interval:</b></small>
                                          <h5 class="mb-5 totaldays">30-jan-2020/20-02-2020</h5>
                                     </span>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="asdfasdf" style=" min-height: 592px !important;">
                                    <p class="border-bottom">Location Details <i onclick="editelocaiton()" title="Edite"
                                                                                 class="fas fa-edit float-right cursor-pointer"></i>
                                    </p>
                                    <div class="d-flex border-bottom mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b></b></small>
                                          <h4>Location Details</h4>
                                     </span>
                                        <span>

                                            <span class="uwpoms"> <i
                                                    class="fas fa-map-marked-alt owpierjpwoq"></i></span>
                                     </span>
                                    </div>
                                    <div class="w-100">
                                        <div class="accordion" id="locationprevdetails">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="asdfasdf" style=" min-height: 592px !important;">
                                    <p class="border-bottom">Car Details <i onclick="editeacrs()" title="Edite"
                                                                            class="fas fa-edit float-right cursor-pointer"></i>
                                    </p>
                                    <div class=" ueytui " style="margin-top: 38px;margin-right: 16px;">
                                        <h4>Car Details</h4>
                                        <div class="d-flex  mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b></b></small>
                                          <h4></h4>
                                     </span>
                                            <span>
                                            <span class="uwpoms"> <i class="fas fa-car owpierjpwoq"></i></span>
                                     </span>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="accordion" id="carpreviewdetails">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="asdfasdf" style=" min-height: 592px !important;">
                                    <p class="border-bottom">Sticker Details <i onclick="stickeredite()" title="Edite"
                                                                                class="fas fa-edit float-right cursor-pointer"></i>
                                    </p>
                                    <div class=" ueytui " style="margin-top: 38px;margin-right: 16px;">
                                        <h4>Sticker Detais</h4>
                                        <div class="d-flex  mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-5">
                                           <small class="mb-1 mt-5"><b></b></small>
                                          <h4></h4>
                                     </span>
                                            <span>

                                            <span class="uwpoms"> <i class="fas fa-image owpierjpwoq"></i></span>
                                     </span>
                                        </div>
                                        <table class="table">
                                            <tbody>
                                            <tr>
                                                <th>Company:</th>
                                                <td class="campanysts"></td>
                                            </tr>
                                            <tr>
                                                <th>Slogan:</th>
                                                <td class="slogans"></td>
                                            </tr>
                                            <tr>
                                                <th>Contact:</th>
                                                <td class="contectstc"></td>
                                            </tr>
                                            <tr>
                                                <th>Sticker Details:</th>
                                                <td class="stcdetailss">---</td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>
                                    <p class="mb-0">Sticker Picture:</p>
                                    <img src="{{asset('/media/uploadimge.png')}}"
                                         class="img-fluid img-thumbnail previewstckerimge" alt="">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="asdfasdf" style=" min-height: 100px !important;">
                                    <p class="border-bottom">Summary <i onclick="editeacrs()" title="Edite"
                                                                        class="fas fa-edit float-right cursor-pointer"></i>
                                    </p>
                                    <div class=" ueytui " style="margin-top: 38px;margin-right: 16px;">
                                        <div class="d-flex  mb-4 pb-2" style="justify-content: space-between;">
                                     <span class="pt-2">
                                           <small class="mb-1 mt-2"><b></b></small>
                                          <h4>Payment Summary</h4>
                                     </span>
                                        </div>
                                        <table class="table text-center table-fluid">
                                            <thead>
                                            <tr>
                                                <th>Car #</th>
                                                <th>State / City</th>
                                                <th>Car Name</th>
                                                <th>Car Make</th>
                                                <th>Cars Quantity</th>
                                                <th>Miles</th>
                                                <th>Miles Rate</th>
                                                <th>Days</th>
                                                <th>Sub Total</th>
                                            </tr>
                                            </thead>
                                            <tbody class="carCostappend">

                                            </tbody>

                                        </table>

                                        <table class="table table-fluid">
                                            <tbody>
                                            <tr>
                                                <td style='margin-left: 130px;' class="float-right"><b
                                                        class="font-size-h4-lg ">Total:</b><span
                                                        class="ml-4 font-size-h4-md totalamount carTotalCost">0</span>
                                                </td>
                                                <td></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div id="stepbtn1" class="row stepsbtnr">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-end">
                            {{--                              <button class="asdfy float-right">Nill</button>--}}
                            <button onclick="tabformvalidaton(1)" class="asdfy float-right">Save & Continue</button>
                            <script>
                                // stepshow1();
                                function stepshow1() {
                                    if ($("#Campaign_tittle").val() == '') {
                                        $("#tittle_error").removeClass('d-none');
                                    } else {
                                        $("#tittle_error").addClass('d-none');
                                    }
                                    if ($("#campaign_type").val() == null) {
                                        // console.log('tittle is empty');
                                        $("#campaign_type_error").removeClass('d-none');
                                    } else {
                                        $("#campaign_type_error").addClass('d-none');
                                    }
                                    if ($("#campaign_duration").val() == null) {
                                        // console.log('tittle is empty');
                                        $("#campaign_duration_error").removeClass('d-none');
                                    } else {
                                        $("#campaign_duration_error").addClass('d-none');
                                    }
                                    if ($("#kt_daterangepicker_1").val() == '') {
                                        // alert($("#kt_daterangepicker_1").val());
                                        $("#interval_error").removeClass('d-none');
                                    } else {
                                        $("#interval_error").addClass('d-none');
                                    }

                                    if ($("#Campaign_tittle").val() != '' && $("#campaign_type").val() != null && $("#campaign_duration").val() != null) {
                                        activetab(2);
                                    }

                                }
                            </script>
                        </div>
                    </div>
                </div>
                <div id="stepbtn2" class="row stepsbtnr">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-end">
                            <button onclick="back1()" class="asdfy float-right">Back</button>
                            <button onclick="tabformvalidaton(2)" class="asdfy float-right">Save & Continue</button>
                            <script>
                                function stepshow2() {
                                    if ($("#country_name").val() == null) {
                                        $("#country_name_error").removeClass('d-none');
                                    } else {
                                        $("#country_name_error").addClass('d-none');
                                    }
                                    if ($("#state").val() == null) {
                                        $("#state_error").removeClass('d-none');
                                    } else {
                                        $("#state_error").addClass('d-none');
                                    }
                                    if ($("#city").val() == null) {
                                        $("#city_error").removeClass('d-none');
                                    } else {
                                        $("#city_error").addClass('d-none');
                                    }

                                    if ($(".findzips").val() == null) {
                                        $("#findzips_error").removeClass('d-none');
                                    } else {
                                        $("#findzips_error").addClass('d-none');
                                    }

                                    checkzipscount();
                                    var totaltabs = $("#addinglocationvalue").val();
                                    var totaltabs = parseInt(totaltabs).toFixed(2);
                                    // totaltabs=totaltabs-1;
                                    for (var i = 1; i <= totaltabs; i++) {
                                        getlocations(i);
                                    }
                                    activetab(3);
                                }

                                function back1() {
                                    activetab(1);
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <div id="stepbtn3" class="row stepsbtnr">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-end">
                            <button onclick="back2()" class="asdfy float-right">Back</button>
                            <button onclick="tabformvalidaton(3)" class="asdfy float-right btn-danger" id="step-3">Save
                                &
                                Continue
                            </button>
                            <script>
                                function stepshow3() {
                                    var carcountarry = [];
                                    // var carTabsCount = parseInt($("#carListHandler").val());
                                    // var carTabsCount = $("#car-tabs-warap .tab-pane").length;
                                    // $("#car-tabs-warap .tab-pane").each(function(){
                                    //   var carcountval =   $(this).find('.numberofcars').val();
                                    //     carcountarry.push(carcountval);
                                    // });
                                    // console.log("counttabs",carTabsCount);
                                    // console.log(varisdf)
                                    // selected_zips_
                                    // var carListCount = parseInt($("#carListHandler").val());
                                    // console.log("car count arryss",carcountarry);
                                    var locationTotalZips = checkzipscount();
                                    carCount = parseInt($("#carListHandler").val())

                                    // var locationTotalZips = checkzipscount();
                                    // carCount = parseInt($("#carListHandler").val())

                                    // locationTotalZips = parseInt($("#carListHandler").val())
                                    var cartotalzips = [];
                                    var carzips = [];
                                    for (i = 1; i < carCount; i++) {
                                        carzips = $('#selected_zips_' + i).val();
                                        for (j = 0; j < carzips.length; j++) {
                                            cartotalzips.push(carzips[j]);
                                        }
                                    }
                                    var totalcheck = 0;
                                    // console.log('carzips' + cartotalzips);
                                    locationsZipCount = locationTotalZips.length;
                                    for (i = 0; i < locationsZipCount; i++) {
                                        for (j = 0; j < cartotalzips.length; j++) {
//                              console.log('car zip'+cartotalzips[j]+'loc zips'+locationTotalZips[i]);
                                            if (cartotalzips[j] == locationTotalZips[i]) {
                                                ++totalcheck;
                                                carzips.push(cartotalzips[j]);
                                            }

                                        }
                                    }

                                    var carzipscount = removeDuplicates(carzips);
                                    // console.log('carzipsss unique', carzipscount);
                                    carzipscount = carzipscount.length;
                                    // console.log('carzipscount', carzipscount);
                                    // console.log('locationsZipCount' + locationsZipCount);
//                                     console.log('locationTotalZips' + locationTotalZips);
//                                     console.log('totl chk' + totalcheck);
                                    if (carzipscount >= locationsZipCount) {
                                        activetab(4);
                                    } else {
                                        toastr.error('Please choose all the zips to proceed to next step', 'All zips not selected');
                                    }

                                    function removeDuplicates(array) {
                                        const result = [];
                                        const map = {};

                                        for (let i = 0; i < array.length; i++) {
                                            if (map[array[i]]) {
                                                continue;
                                            } else {
                                                result.push(array[i]);
                                                map[array[i]] = true;
                                            }
                                        }
                                        return result;
                                    }


                                    // console.log('carzips' + carzips);
                                    // console.log('locationTotalZips' + locationTotalZips);
                                    // console.log( totalcheck);
                                }

                                function back2() {
                                    activetab(2);
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <div id="stepbtn4" class="row stepsbtnr">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-end">
                            <button onclick="back3()" class="asdfy float-right">Back</button>
                            <button onclick="tabformvalidaton(4)" class="asdfy float-right summary-btn">Save &
                                Continue
                            </button>
                            <script>
                                function stepshow5() {
                                    $('.alert').removeClass('d-none');
                                    if ($("input[name=l_title]").val() == '') {
                                        $('#error').text('Company field is required');
                                        $('.alert').show();
                                        $('.summary-btn').addClass('btn-danger')
                                        return;
                                    }
                                    if ($("input[name=l_title54]").val() == '') {
                                        $('.alert').show();
                                        $('#error').text('Title field is required');
                                        $('.summary-btn').addClass('btn-danger')
                                        return;
                                    }
                                    if ($("input[name=l_title2]").val() == '') {

                                        $('#error').text('Contact number is required');
                                        $('.summary-btn').addClass('btn-danger')
                                        return;
                                    }
                                    $('.summary-btn').removeClass('btn-danger')
                                    $('.summary-btn').addClass('asdfy')
                                    let sum = 0;
                                    $('.add_cars_1 .testsum').each(function () {
                                        var dataLayer = $(this).data('subtotal');
                                        sum += dataLayer;
                                    });
                                    // alert(sum.toFixed(2))
                                    $('.totalamount').text(sum.toFixed(2));

                                    // var total_locations=$("#addinglocationvalue").val();
                                    // for(var i=1;i<=total_locations;i++) {
                                    //     console.log();
                                    //   alert($('#selected_zips_'+i).val());
                                    // }
                                    // }
                                    activetab(5);

                                }


                                function back3() {
                                    activetab(3);
                                }
                            </script>
                        </div>
                    </div>
                </div>
                <div id="stepbtn5" class="row stepsbtnr">
                    <div class="col-lg-12">
                        <div class="d-flex justify-content-end">
                            <button onclick="back4()" class="asdfy  float-right">Back</button>

                            <button class="float-right asdfy" data-toggle="modal" data-target="#nextterms">Next
                            </button>

                            <script>
                                // function stepshow4(){
                                //     // $(".allstepsdivs").hide();
                                //     // $(".stepsbtnr").hide();
                                //     // // $("#stepbtn4").show();
                                //     // $("#stepdiv5").show();
                                //     // $("#progresbb5").removeClass("sdfq");
                                //     alert("You are going to checkout")
                                // }
                                function back4() {
                                    activetab(4);
                                }
                            </script>


                            <!-- Modal -->

                            <div class="modal fade" id="nextterms" tabindex="-1"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            {{--                                            <h5 class="modal-title" id="exampleModalLabel">CheckOut</h5>--}}
                                            <h6 class="modal-title" id="exampleModalLabel">Terms and conditions</h6>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{--                                        *****************************************************************************--}}
                                            <div class="row">

                                                <div class="col-md-10 order-md-1">
                                                    <h5 class="terms_error text-center text-danger"></h5>
                                                    <h4 class="mb-3">Please read our Terms and conditions below:</h4>
                                                    <div>
                                                        <div class="row">
                                                            <div class="col-10 offset-1">
                                                                <div class="form-group">
                                                                    <li>All provided information is correct</li>
                                                                    <li>The Customer shall ensure that such third
                                                                        parties access to and/or use of the Software
                                                                        complies with all the terms and conditions of
                                                                        the Agreement, and any violation of these terms
                                                                        and
                                                                        conditions by such third parties shall be deemed
                                                                        to be a violation by the Customer.
                                                                    </li>
                                                                    <li>Company has a right to cancel your campaign at
                                                                        any step
                                                                    </li>
                                                                    <li>All data is valid</li>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-10 offset-1">
                                                                <div class="form-group">
                                                                    {{--                                                                    <label>Large Size</label>--}}
                                                                    <div class="checkbox-inline">
                                                                        <label class="checkbox checkbox-lg">
                                                                            <input id="termss" type="checkbox" value="1"
                                                                                   name="termss"><span></span>
                                                                            I have read and agreed above all terms and
                                                                            conditions</label>
                                                                    </div>
                                                                    {{--                                                                    <span class="form-text text-muted">Some help text goes here</span>--}}
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            {{--                                            <button class="btn btn-danger btn-terms" disabled data-dismiss="modal"--}}
                                            {{--                                                    data-toggle="modal" data-target="#checkoutpreview">Agree Terms & Checkout--}}
                                            {{--                                            </button>   --}}
                                            <button class="btn btn-danger btn-terms" disabled id="sbmitt"
                                                    onclick="thankyou()"> Create Campaign
                                            </button>
                                            {{--                                            <button class="asdfy float-right" data-toggle="modal" data-target="#checkoutpreview">Check--}}
                                            {{--                                                Out--}}
                                            {{--                                            </button>--}}
                                            {{--                                            <button type="button" class="btn btn-primary" onclick="thankyou()">Continue--}}
                                            {{--                                                to checkout--}}
                                            {{--                                            </button>--}}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal fade" id="checkoutpreview" tabindex="-1"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-xl">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">CheckOut</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{--                                        *****************************************************************************--}}
                                            <div class="row">
                                                <div class="col-md-4 order-md-2 mb-4">
                                                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                                                        <span class="text-muted">Summary</span>
                                                        <span class="badge badge-secondary badge-pill"></span>
                                                    </h4>
                                                    <ul class="list-group mb-3">
                                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                            <div>
                                                                <h6 class="my-0">Campaign Tittle</h6>
                                                                <small class="text-muted">Sample tittle</small>
                                                            </div>
                                                            {{--                                                            <span class="text-muted">$12</span>--}}
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                                                            <div>
                                                                <h6 class="my-0">Cars</h6>
                                                                <small class="text-muted"></small>
                                                            </div>
                                                            <span class="text-muted totalcarprevew ">1</span>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between bg-light">
                                                            <div class="text-success">
                                                                <h6 class="my-0">Promo code</h6>
                                                                <small></small>
                                                            </div>
                                                            <span class="text-success">PROMO-1111</span>
                                                        </li>
                                                        <li class="list-group-item d-flex justify-content-between">
                                                            <span>Total (USD)</span>
                                                            <strong class="carTotalCost"></strong>
                                                        </li>
                                                    </ul>

                                                    <div class="card p-2">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                   placeholder="Promo code">
                                                            <div class="input-group-append">
                                                                <button type="submit" class="btn btn-secondary">Redeem
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 order-md-1">
                                                    <h4 class="mb-5">Chose Billing Option</h4>
                                                    <form class="needs-validation" novalidate="">
                                                        <div class="row">
                                                            <div class="col-6"><img
                                                                    src="{{asset('/media/paypal-logo.png')}}"
                                                                    class="checkoutimge cursor-pointer" alt=""></div>
                                                            <div class="col-6"><img src="{{asset('/media/Stripe.png')}}"
                                                                                    class="checkoutimge  cursor-pointer"
                                                                                    alt=""></div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-md-6 mb-3">
                                                                <label for="cc-name">Name on card</label>
                                                                <input type="text" class="form-control" id="cc-name"
                                                                       value="Mical Jon" placeholder="" required="">
                                                                <small class="text-muted">Full name as displayed on
                                                                    card</small>
                                                                <div class="invalid-feedback">
                                                                    Name on card is required
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 mb-3">
                                                                <label for="cc-number">Credit card number</label>
                                                                <input type="text" class="form-control" id="cc-number"
                                                                       value="36784565389759" placeholder=""
                                                                       required="">
                                                                <div class="invalid-feedback">
                                                                    Credit card number is required
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-3 mb-3">
                                                                <label for="cc-expiration">Expiration</label>
                                                                <input type="text" class="form-control"
                                                                       id="cc-expiration" value="10-23" placeholder=""
                                                                       required="">
                                                                <div class="invalid-feedback">
                                                                    Expiration date required
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 mb-3">
                                                                <label for="cc-expiration">CVV</label>
                                                                <input type="text" class="form-control" id="cc-cvv"
                                                                       value="368" placeholder="" required="">
                                                                <div class="invalid-feedback">
                                                                    Security code required
                                                                </div>
                                                            </div>
                                                        </div>
                                                        {{--                                                        <hr class="mb-4">--}}
                                                        {{--                                                        <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>--}}
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal"
                                                    data-toggle="modal" data-target="#nextterms">Back
                                            </button>
                                            <button type="button" class="btn btn-primary" onclick="thankyou()">Continue
                                                to checkout
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal fade" id="thankyou" aria-labelledby="exampleModalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-lg modal-dialog-scrollable">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Thankyou</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            {{--                                        *****************************************************************************--}}
                                            <div class="d-flex flex-row flex-column-fluid container">
                                                <!--begin::Content Wrapper-->
                                                <div class="main d-flex flex-column flex-row-fluid">
                                                    <div class="content flex-column-fluid" id="kt_content">
                                                        <!--begin::Section-->
                                                        <div class="text-center pt-15">
                                                            <h1 class="h2 font-weight-bolder text-dark mb-6">Thankyou
                                                                For Your Request</h1>
                                                            <div class="h4 text-dark-50 font-weight-normal">Contact us
                                                                for more details or queries!
                                                            </div>
                                                            <div class="row">
                                                                <div class="offset-md-3 col-md-6">
											<span class="svg-icon svg-icon-full">
												<!--begin::Svg Icon | path:/metronic/theme/html/demo8/dist/assets/media/svg/illustrations/contact.svg-->
												<svg xmlns="http://www.w3.org/2000/svg"
                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                     id="ae37f038-3a9e-4b82-ad68-fc94ba16af2a" data-name="Layer 1"
                                                     width="1096" height="574.74" viewBox="0 0 1096 574.74">
													<g opacity="0.1">
														<ellipse cx="479.42" cy="362.12" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M540.43,461a18,18,0,0,0,2.38-9.11c0-8.23-5.1-14.9-11.39-14.9S520,443.68,520,451.91a18,18,0,0,0,2.38,9.11,18.61,18.61,0,0,0,0,18.21,18.61,18.61,0,0,0,0,18.21,17.94,17.94,0,0,0-2.38,9.11c0,8.22,5.1,14.9,11.38,14.9s11.39-6.68,11.39-14.9a17.94,17.94,0,0,0-2.38-9.11,18.61,18.61,0,0,0,0-18.21,18.61,18.61,0,0,0,0-18.21Z"
                                                            transform="translate(-52 -162.63)" fill="#3f3d56"></path>
														<ellipse cx="479.42" cy="271.07" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<ellipse cx="479.42" cy="252.86" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M488.82,290.86a53.08,53.08,0,0,1-4.24-6.24l29.9-4.91-32.34.24a54.62,54.62,0,0,1-1-43.2l43.39,22.51-40-29.42a54.53,54.53,0,1,1,90,61,54.54,54.54,0,0,1,6.22,9.94L541.92,321l41.39-13.89a54.53,54.53,0,0,1-8.79,51.2,54.52,54.52,0,1,1-85.7,0,54.52,54.52,0,0,1,0-67.42Z"
                                                            transform="translate(-52 -162.63)" fill="#6c63ff"></path>
														<path
                                                            d="M586.19,324.57a54.27,54.27,0,0,1-11.67,33.71,54.52,54.52,0,1,1-85.7,0C481.51,349,586.19,318.45,586.19,324.57Z"
                                                            transform="translate(-52 -162.63)" opacity="0.1"></path>
													</g>
													<g opacity="0.1">
														<ellipse cx="612.28" cy="330.26" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M671,445.26a13.43,13.43,0,0,0,1.77-6.8c0-6.15-3.81-11.14-8.5-11.14s-8.51,5-8.51,11.14a13.33,13.33,0,0,0,1.78,6.8,13.9,13.9,0,0,0,0,13.61,13.9,13.9,0,0,0,0,13.61,13.33,13.33,0,0,0-1.78,6.8c0,6.15,3.81,11.14,8.51,11.14s8.5-5,8.5-11.14a13.43,13.43,0,0,0-1.77-6.8,14,14,0,0,0,0-13.61,14,14,0,0,0,0-13.61Z"
                                                            transform="translate(-52 -162.63)" fill="#3f3d56"></path>
														<ellipse cx="612.28" cy="262.22" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<ellipse cx="612.28" cy="248.61" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M632.44,318.11a39,39,0,0,1-3.17-4.66l22.35-3.67-24.17.18a40.84,40.84,0,0,1-.78-32.29L659.1,294.5l-29.91-22a40.75,40.75,0,1,1,67.29,45.6,41.2,41.2,0,0,1,4.65,7.43l-29,15.07,30.93-10.38a40.76,40.76,0,0,1-6.57,38.26,40.74,40.74,0,1,1-64,0,40.74,40.74,0,0,1,0-50.38Z"
                                                            transform="translate(-52 -162.63)" fill="#6c63ff"></path>
														<path
                                                            d="M705.2,343.3a40.57,40.57,0,0,1-8.72,25.19,40.74,40.74,0,1,1-64,0C627,361.56,705.2,338.73,705.2,343.3Z"
                                                            transform="translate(-52 -162.63)" opacity="0.1"></path>
													</g>
													<g opacity="0.1">
														<ellipse cx="1038.58" cy="322.12" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M1081.57,421a18,18,0,0,1-2.38-9.11c0-8.23,5.1-14.9,11.39-14.9s11.38,6.67,11.38,14.9a18,18,0,0,1-2.38,9.11,18.61,18.61,0,0,1,0,18.21,18.61,18.61,0,0,1,0,18.21,17.94,17.94,0,0,1,2.38,9.11c0,8.22-5.1,14.9-11.38,14.9s-11.39-6.68-11.39-14.9a17.94,17.94,0,0,1,2.38-9.11,18.61,18.61,0,0,1,0-18.21,18.61,18.61,0,0,1,0-18.21Z"
                                                            transform="translate(-52 -162.63)" fill="#3f3d56"></path>
														<ellipse cx="1038.58" cy="231.07" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<ellipse cx="1038.58" cy="212.86" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M1133.18,250.86a53.08,53.08,0,0,0,4.24-6.24l-29.9-4.91,32.34.24a54.62,54.62,0,0,0,1-43.2l-43.39,22.51,40-29.42a54.53,54.53,0,1,0-90,61,54.54,54.54,0,0,0-6.22,9.94L1080.08,281l-41.39-13.89a54.53,54.53,0,0,0,8.79,51.2,54.52,54.52,0,1,0,85.7,0,54.52,54.52,0,0,0,0-67.42Z"
                                                            transform="translate(-52 -162.63)" fill="#6c63ff"></path>
														<path
                                                            d="M1035.81,284.57a54.27,54.27,0,0,0,11.67,33.71,54.52,54.52,0,1,0,85.7,0C1140.49,309,1035.81,278.45,1035.81,284.57Z"
                                                            transform="translate(-52 -162.63)" opacity="0.1"></path>
													</g>
													<g opacity="0.1">
														<ellipse cx="928.72" cy="324.26" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M974,439.26a13.43,13.43,0,0,1-1.77-6.8c0-6.15,3.81-11.14,8.5-11.14s8.51,5,8.51,11.14a13.33,13.33,0,0,1-1.78,6.8,13.9,13.9,0,0,1,0,13.61,13.9,13.9,0,0,1,0,13.61,13.33,13.33,0,0,1,1.78,6.8c0,6.15-3.81,11.14-8.51,11.14s-8.5-5-8.5-11.14a13.43,13.43,0,0,1,1.77-6.8,14,14,0,0,1,0-13.61,14,14,0,0,1,0-13.61Z"
                                                            transform="translate(-52 -162.63)" fill="#3f3d56"></path>
														<ellipse cx="928.72" cy="256.22" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<ellipse cx="928.72" cy="242.61" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M1012.56,312.11a39,39,0,0,0,3.17-4.66l-22.35-3.67,24.17.18a40.84,40.84,0,0,0,.78-32.29L985.9,288.5l29.91-22a40.75,40.75,0,1,0-67.29,45.6,41.2,41.2,0,0,0-4.65,7.43l29,15.07L942,324.23a40.76,40.76,0,0,0,6.57,38.26,40.74,40.74,0,1,0,64,0,40.74,40.74,0,0,0,0-50.38Z"
                                                            transform="translate(-52 -162.63)" fill="#6c63ff"></path>
														<path
                                                            d="M939.8,337.3a40.57,40.57,0,0,0,8.72,25.19,40.74,40.74,0,1,0,64,0C1018,355.56,939.8,332.73,939.8,337.3Z"
                                                            transform="translate(-52 -162.63)" opacity="0.1"></path>
													</g>
													<g opacity="0.1">
														<ellipse cx="61.59" cy="322.12" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M122.59,421a18,18,0,0,0,2.38-9.11c0-8.23-5.1-14.9-11.38-14.9s-11.38,6.67-11.38,14.9a18,18,0,0,0,2.37,9.11,18.67,18.67,0,0,0,0,18.21,18.67,18.67,0,0,0,0,18.21,17.93,17.93,0,0,0-2.37,9.11c0,8.22,5.09,14.9,11.38,14.9S125,474.77,125,466.55a17.94,17.94,0,0,0-2.38-9.11,18.61,18.61,0,0,0,0-18.21,18.61,18.61,0,0,0,0-18.21Z"
                                                            transform="translate(-52 -162.63)" fill="#3f3d56"></path>
														<ellipse cx="61.59" cy="231.07" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<ellipse cx="61.59" cy="212.86" rx="11.38" ry="14.9"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M71,250.86a54.33,54.33,0,0,1-4.24-6.24l29.91-4.91L64.3,240a54.62,54.62,0,0,1-1-43.2l43.4,22.51-40-29.42a54.52,54.52,0,1,1,90,61,54.54,54.54,0,0,1,6.22,9.94L124.08,281l41.4-13.89a54.59,54.59,0,0,1-8.8,51.2,54.52,54.52,0,1,1-85.7,0,54.52,54.52,0,0,1,0-67.42Z"
                                                            transform="translate(-52 -162.63)" fill="#6c63ff"></path>
														<path
                                                            d="M168.35,284.57a54.27,54.27,0,0,1-11.67,33.71,54.52,54.52,0,1,1-85.7,0C63.67,309,168.35,278.45,168.35,284.57Z"
                                                            transform="translate(-52 -162.63)" opacity="0.1"></path>
													</g>
													<g opacity="0.1">
														<ellipse cx="171.44" cy="324.26" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M230.17,439.26a13.43,13.43,0,0,0,1.77-6.8c0-6.15-3.8-11.14-8.5-11.14s-8.51,5-8.51,11.14a13.43,13.43,0,0,0,1.78,6.8,13.9,13.9,0,0,0,0,13.61,13.9,13.9,0,0,0,0,13.61,13.43,13.43,0,0,0-1.78,6.8c0,6.15,3.81,11.14,8.51,11.14s8.5-5,8.5-11.14a13.43,13.43,0,0,0-1.77-6.8,14,14,0,0,0,0-13.61,14,14,0,0,0,0-13.61Z"
                                                            transform="translate(-52 -162.63)" fill="#3f3d56"></path>
														<ellipse cx="171.44" cy="256.22" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<ellipse cx="171.44" cy="242.61" rx="8.51" ry="11.13"
                                                                 fill="#3f3d56"></ellipse>
														<path
                                                            d="M191.6,312.11a40.21,40.21,0,0,1-3.17-4.66l22.35-3.67-24.17.18a40.84,40.84,0,0,1-.78-32.29l32.43,16.83-29.91-22a40.75,40.75,0,1,1,67.29,45.6,40.12,40.12,0,0,1,4.65,7.43l-29,15.07,30.93-10.38a40.76,40.76,0,0,1-6.57,38.26,40.74,40.74,0,1,1-64,0,40.74,40.74,0,0,1,0-50.38Z"
                                                            transform="translate(-52 -162.63)" fill="#6c63ff"></path>
														<path
                                                            d="M264.36,337.3a40.57,40.57,0,0,1-8.72,25.19,40.74,40.74,0,1,1-64,0C186.14,355.56,264.36,332.73,264.36,337.3Z"
                                                            transform="translate(-52 -162.63)" opacity="0.1"></path>
													</g>
													<ellipse cx="548" cy="493.13" rx="548" ry="8.86" fill="#6c63ff"
                                                             opacity="0.1"></ellipse>
													<ellipse cx="548" cy="565.88" rx="548" ry="8.86" fill="#6c63ff"
                                                             opacity="0.1"></ellipse>
													<ellipse cx="548" cy="341.3" rx="548" ry="8.86" fill="#6c63ff"
                                                             opacity="0.1"></ellipse>
													<ellipse cx="548" cy="417.21" rx="548" ry="8.86" fill="#6c63ff"
                                                             opacity="0.1"></ellipse>
													<path
                                                        d="M860.79,273a18.3,18.3,0,0,0-10.6,1.16,15.65,15.65,0,0,1-12.74,0,17.88,17.88,0,0,0-15,.29,9.24,9.24,0,0,1-4.31,1.08c-6.08,0-11.13-6.12-12.18-14.19a11.88,11.88,0,0,0,3-3.27c3.56-5.74,9.07-9.43,15.27-9.43s11.64,3.64,15.2,9.32a11.68,11.68,0,0,0,10.09,5.54h.16C854.57,263.45,858.76,267.33,860.79,273Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"
                                                        opacity="0.1"></path>
													<path
                                                        d="M879.3,247.65l-9.82,6.22,6-10.84a9.7,9.7,0,0,0-5.94-2.11h-.16a11.35,11.35,0,0,1-2-.15L864,242.88l1.43-2.6a11.79,11.79,0,0,1-5.83-4.42l-6,3.78,3.76-6.84c-3.48-4.18-8.18-6.74-13.34-6.74-6.2,0-11.71,3.68-15.28,9.42a11.41,11.41,0,0,1-10.09,5.44h-.33c-6.84,0-12.38,7.75-12.38,17.31s5.54,17.32,12.38,17.32a9.39,9.39,0,0,0,4.31-1.08,17.86,17.86,0,0,1,15-.3,15.55,15.55,0,0,0,12.74,0,17.92,17.92,0,0,1,14.86.29,9.3,9.3,0,0,0,4.26,1.06c6.84,0,12.38-7.76,12.38-17.32A21.93,21.93,0,0,0,879.3,247.65Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"
                                                        opacity="0.1"></path>
													<path
                                                        d="M443.26,267.59a12.84,12.84,0,0,0-7.43.81,10.92,10.92,0,0,1-8.91,0,12.48,12.48,0,0,0-10.49.21,6.62,6.62,0,0,1-3,.75c-4.25,0-7.79-4.28-8.53-9.93a8.32,8.32,0,0,0,2.13-2.29c2.49-4,6.35-6.6,10.69-6.6s8.15,2.55,10.64,6.52a8.19,8.19,0,0,0,7.07,3.88h.11C438.9,260.92,441.83,263.64,443.26,267.59Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"
                                                        opacity="0.1"></path>
													<path
                                                        d="M456.21,249.86l-6.87,4.36,4.17-7.59a6.75,6.75,0,0,0-4.15-1.48h-.12a7.49,7.49,0,0,1-1.42-.11l-2.33,1.48,1-1.82a8.3,8.3,0,0,1-4.08-3.09l-4.17,2.64,2.64-4.78a12.21,12.21,0,0,0-9.34-4.73c-4.34,0-8.2,2.58-10.69,6.6a8,8,0,0,1-7.07,3.81h-.23c-4.79,0-8.67,5.42-8.67,12.12s3.88,12.12,8.67,12.12a6.5,6.5,0,0,0,3-.76,12.5,12.5,0,0,1,10.48-.2,11.1,11.1,0,0,0,4.49,1,11,11,0,0,0,4.43-.94,12.54,12.54,0,0,1,10.4.2,6.48,6.48,0,0,0,3,.74c4.78,0,8.66-5.43,8.66-12.12A15.33,15.33,0,0,0,456.21,249.86Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"
                                                        opacity="0.1"></path>
													<path
                                                        d="M321.59,346a12.82,12.82,0,0,1,7.42.81,10.94,10.94,0,0,0,8.92,0,12.52,12.52,0,0,1,10.49.2,6.47,6.47,0,0,0,3,.76c4.25,0,7.79-4.29,8.52-9.94a8.15,8.15,0,0,1-2.12-2.29c-2.5-4-6.36-6.59-10.69-6.59s-8.15,2.54-10.65,6.52a8.19,8.19,0,0,1-7.06,3.88h-.11C325.94,339.37,323,342.08,321.59,346Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"
                                                        opacity="0.1"></path>
													<path
                                                        d="M308.63,328.3l6.88,4.36-4.18-7.58a6.79,6.79,0,0,1,4.16-1.49h.11a8.52,8.52,0,0,0,1.43-.1l2.33,1.47-1-1.81a8.29,8.29,0,0,0,4.07-3.09l4.17,2.64L324,317.91a12.2,12.2,0,0,1,9.34-4.72c4.33,0,8.2,2.58,10.69,6.6a8,8,0,0,0,7.06,3.81h.24c4.78,0,8.66,5.43,8.66,12.12s-3.88,12.12-8.66,12.12a6.49,6.49,0,0,1-3-.75,12.48,12.48,0,0,0-10.49-.21,10.86,10.86,0,0,1-4.48,1,11,11,0,0,1-4.44-.94,12.52,12.52,0,0,0-10.39.2,6.48,6.48,0,0,1-3,.74c-4.79,0-8.67-5.42-8.67-12.12A15.44,15.44,0,0,1,308.63,328.3Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"
                                                        opacity="0.1"></path>
													<path
                                                        d="M716.31,652.89c2.61-4.84-.35-10.76-3.75-15.07s-7.56-8.8-7.47-14.29c.13-7.89,8.51-12.56,15.2-16.74a74.3,74.3,0,0,0,13.65-11,20.13,20.13,0,0,0,4.19-5.62c1.39-3.08,1.35-6.6,1.26-10q-.43-16.89-1.67-33.76"
                                                        transform="translate(-52 -162.63)" fill="none" stroke="#3f3d56"
                                                        stroke-miterlimit="10" stroke-width="4"></path>
													<path
                                                        d="M750.45,545.85a12.31,12.31,0,0,0-6.15-10.09l-2.76,5.45.09-6.6a12.31,12.31,0,1,0,8.82,11.24Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M728.49,629.17a12.31,12.31,0,0,1-23.24-5,12,12,0,0,1,.8-5,12.32,12.32,0,0,1,23,.13l-7.69,6.26,8.46-2A12.24,12.24,0,0,1,728.49,629.17Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M722.41,605.27a12.31,12.31,0,0,1-3.9-24.15l-.07,5.07,2.79-5.52h0a12.31,12.31,0,1,1,1.15,24.6Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M752.3,585.38a12.31,12.31,0,1,1,5.44-23l-2.17,6L760,564a12.31,12.31,0,0,1-7.74,21.37Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M748.79,549.13c-2.84.31-5.6,1.19-8.46,1.37s-6-.51-7.78-2.72a39.48,39.48,0,0,1-2.28-4,8.76,8.76,0,0,0-3.1-2.92,12.31,12.31,0,1,0,23,8.18C749.72,549.05,749.25,549.08,748.79,549.13Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M752.3,585.38a12.31,12.31,0,0,1-11.71-17.56,9.11,9.11,0,0,1,2.47,2.48,41.72,41.72,0,0,0,2.44,4.07c1.92,2.25,5.2,3,8.17,2.85s5.84-1,8.8-1.25c.41,0,.82-.06,1.24-.07A12.31,12.31,0,0,1,752.3,585.38Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M722.41,605.27a12.31,12.31,0,0,1-11.81-17.33,10,10,0,0,1,2.61,2.5,41.23,41.23,0,0,0,2.67,4.15c2.07,2.31,5.57,3.13,8.71,3s6-.81,9-1A12.33,12.33,0,0,1,722.41,605.27Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M728.49,629.17a12.31,12.31,0,0,1-23.24-5,12,12,0,0,1,.8-5,12.29,12.29,0,0,1,2.7,2.41c1.17,1.42,1.94,3,3.3,4.37,2.51,2.47,6.58,3.49,10.19,3.58A51.7,51.7,0,0,0,728.49,629.17Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M106.31,580.89c2.61-4.84-.35-10.76-3.75-15.07S95,557,95.09,551.53c.13-7.89,8.51-12.56,15.2-16.74a74.3,74.3,0,0,0,13.65-11,20.13,20.13,0,0,0,4.19-5.62c1.39-3.08,1.35-6.6,1.26-10q-.44-16.89-1.67-33.76"
                                                        transform="translate(-52 -162.63)" fill="none" stroke="#3f3d56"
                                                        stroke-miterlimit="10" stroke-width="4"></path>
													<path
                                                        d="M140.45,473.85a12.31,12.31,0,0,0-6.15-10.09l-2.76,5.45.09-6.6a12.31,12.31,0,1,0,8.82,11.24Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M118.49,557.17a12.31,12.31,0,0,1-23.24-5,12,12,0,0,1,.8-5,12.32,12.32,0,0,1,23,.13l-7.69,6.26,8.46-2A12.24,12.24,0,0,1,118.49,557.17Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M112.41,533.27a12.31,12.31,0,0,1-3.9-24.15l-.07,5.07,2.79-5.52h0a12.31,12.31,0,1,1,1.15,24.6Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M142.3,513.38a12.31,12.31,0,1,1,5.44-23l-2.17,6L150,492a12.31,12.31,0,0,1-7.74,21.37Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M138.79,477.13c-2.84.31-5.6,1.19-8.46,1.37s-6-.51-7.78-2.72a39.48,39.48,0,0,1-2.28-4,8.76,8.76,0,0,0-3.1-2.92,12.31,12.31,0,1,0,23,8.18C139.72,477.05,139.25,477.08,138.79,477.13Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M142.3,513.38a12.31,12.31,0,0,1-11.71-17.56,9.11,9.11,0,0,1,2.47,2.48,41.72,41.72,0,0,0,2.44,4.07c1.92,2.25,5.2,3,8.17,2.85s5.84-1,8.8-1.25c.41,0,.82-.06,1.24-.07A12.31,12.31,0,0,1,142.3,513.38Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M112.41,533.27a12.31,12.31,0,0,1-11.81-17.33,10,10,0,0,1,2.61,2.5,41.23,41.23,0,0,0,2.67,4.15c2.07,2.31,5.57,3.13,8.71,3s6-.81,9-1A12.33,12.33,0,0,1,112.41,533.27Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M118.49,557.17a12.31,12.31,0,0,1-23.24-5,12,12,0,0,1,.8-5,12.29,12.29,0,0,1,2.7,2.41c1.17,1.42,1.94,3,3.3,4.37,2.51,2.47,6.58,3.49,10.19,3.58A51.7,51.7,0,0,0,118.49,557.17Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M512.52,498.52c-2.61-4.83.35-10.75,3.76-15.06s7.55-8.8,7.46-14.29c-.12-7.9-8.5-12.56-15.2-16.74a74,74,0,0,1-13.64-11,19.78,19.78,0,0,1-4.2-5.61c-1.38-3.09-1.34-6.6-1.26-10q.45-16.89,1.67-33.76"
                                                        transform="translate(-52 -162.63)" fill="none" stroke="#3f3d56"
                                                        stroke-miterlimit="10" stroke-width="4"></path>
													<path
                                                        d="M478.39,391.49a12.3,12.3,0,0,1,6.14-10.09l2.76,5.45-.08-6.6a12.62,12.62,0,0,1,4.05-.49,12.31,12.31,0,1,1-12.87,11.73Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M500.34,474.81a12.31,12.31,0,1,0-.59-9.91l7.69,6.26-8.46-2A12.24,12.24,0,0,0,500.34,474.81Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M506.42,450.91a12.31,12.31,0,0,0,3.91-24.15l.06,5.07-2.79-5.52h0A12.31,12.31,0,0,0,494.7,438a12.16,12.16,0,0,0,.53,4.2A12.3,12.3,0,0,0,506.42,450.91Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M476.54,431a12.31,12.31,0,1,0-5.45-23l2.18,6-4.48-4.29a12.21,12.21,0,0,0-4,8.5,11.91,11.91,0,0,0,.31,3.39A12.3,12.3,0,0,0,476.54,431Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M480.05,394.77c2.84.31,5.6,1.19,8.45,1.37s6-.51,7.79-2.72a39.4,39.4,0,0,0,2.27-4,8.79,8.79,0,0,1,3.11-2.92,12.31,12.31,0,1,1-23,8.17C479.12,394.68,479.58,394.72,480.05,394.77Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M476.54,431a12.32,12.32,0,0,0,11.71-17.56,9.15,9.15,0,0,0-2.48,2.48,41.72,41.72,0,0,1-2.44,4.07c-1.91,2.25-5.19,3-8.16,2.85s-5.84-1-8.8-1.25c-.41,0-.83-.06-1.24-.07A12.3,12.3,0,0,0,476.54,431Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M506.42,450.91a12.3,12.3,0,0,0,11.81-17.33,9.83,9.83,0,0,0-2.6,2.5,41.23,41.23,0,0,1-2.67,4.15c-2.08,2.31-5.57,3.13-8.72,3s-6-.81-9-1A12.3,12.3,0,0,0,506.42,450.91Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M500.34,474.81a12.31,12.31,0,0,0,22.45-10,11.82,11.82,0,0,0-2.7,2.41c-1.17,1.42-2,3-3.3,4.37-2.52,2.47-6.58,3.49-10.2,3.58A53.94,53.94,0,0,1,500.34,474.81Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<circle cx="779.73" cy="238.18" r="99.96" fill="#3f3d56"></circle>
													<path
                                                        d="M874.64,426.59c-3.48-3.48-11.85-8.73-16-10.69-4.79-2.3-6.55-2.26-9.94.19-2.82,2-4.65,3.93-7.89,3.22s-9.66-5.55-15.87-11.74S813.91,395,813.22,391.71s1.21-5.08,3.23-7.9c2.44-3.39,2.51-5.15.19-9.94-2-4.15-7.19-12.5-10.7-16s-4.27-2.73-6.19-2a35.8,35.8,0,0,0-5.67,3c-3.48,2.33-5.43,4.27-6.8,7.19s-2.92,8.34,5.05,22.53a125.69,125.69,0,0,0,22.1,29.47l0,0,0,0A125.88,125.88,0,0,0,844,440.2c14.18,8,19.61,6.41,22.53,5.05s4.86-3.29,7.18-6.8a35.33,35.33,0,0,0,3-5.67C877.37,430.86,878.15,430.08,874.64,426.59Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M831.73,300.86a100,100,0,1,0,99.95,99.95A100,100,0,0,0,831.73,300.86Zm0,186.62a86.67,86.67,0,1,1,86.67-86.67A86.67,86.67,0,0,1,831.73,487.48Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<circle cx="550.08" cy="390.01" r="99.96" fill="#3f3d56"></circle>
													<path
                                                        d="M658.93,522.44,629,552.89a.54.54,0,0,0,0,.78L650,576a3.62,3.62,0,0,1-2.55,6.17,3.64,3.64,0,0,1-2.56-1.06L624,558.86a.57.57,0,0,0-.8,0L618.11,564a22.37,22.37,0,0,1-16,6.73,22.86,22.86,0,0,1-16.28-6.92l-4.89-5a.57.57,0,0,0-.8,0l-20.84,22.2a3.61,3.61,0,0,1-5.11,0,3.6,3.6,0,0,1,0-5.11l20.92-22.28a.6.6,0,0,0,0-.78l-29.93-30.45a.55.55,0,0,0-.94.39v60.93a8.92,8.92,0,0,0,8.89,8.89H651a8.92,8.92,0,0,0,8.89-8.89V522.83A.55.55,0,0,0,658.93,522.44Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M602.08,563.5A15.14,15.14,0,0,0,613,559l43.59-44.37a8.7,8.7,0,0,0-5.5-2H553.15a8.64,8.64,0,0,0-5.5,2L591.25,559A15.08,15.08,0,0,0,602.08,563.5Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M600.82,452.69a100,100,0,1,0,99.95,99.95A100,100,0,0,0,600.82,452.69Zm0,186.62a86.67,86.67,0,1,1,86.67-86.67A86.67,86.67,0,0,1,600.82,639.31Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<circle cx="312.85" cy="315.36" r="99.96" fill="#3f3d56"></circle>
													<path
                                                        d="M364.85,430.16,325,522.1l3.72,3.72,36.14-15.94L401,525.82l3.72-3.72Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M364.85,378A100,100,0,1,0,464.8,478,100,100,0,0,0,364.85,378Zm0,186.62A86.67,86.67,0,1,1,451.52,478,86.67,86.67,0,0,1,364.85,564.66Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M840.93,603.88q-1.36-3.32-2.79-6.62a19.65,19.65,0,0,0-3.41-5.89,6.24,6.24,0,0,0-2-1.5,8.53,8.53,0,0,0-2.61-.52,20.83,20.83,0,0,0-4.1-.11c-.53-.16-1.13-.37-1.72-.61a14.28,14.28,0,0,0,10.64-13.25c0-.2,0-.41,0-.61a14.25,14.25,0,0,0-14.19-14.31,14,14,0,0,0-7.45,2.13,13.37,13.37,0,0,0-15,7.23,16.53,16.53,0,0,0-5.1,12.3c0,.21,0,.43,0,.63.26,7.77,5.28,14,11.62,14.63a6.66,6.66,0,0,0-1.84,1.23c-1.94,1.87-2.26,4.84-2.47,7.54l-.15,2L797,609.35a.62.62,0,0,0-.15,1.09l3.21,2.29c0,.19,0,.38,0,.57s0,.43.06.65a4.59,4.59,0,0,0-1.24,1.77,8.18,8.18,0,0,0-.29,3.24l.12,2.79a21.91,21.91,0,0,0,.85,6.21c.42,1.24,2.24,4.23,3.55,4.25-1.21,5.5-1.53,17.1-1.53,17.1-.15.15.27.29,1,.41.72,5.52,3.07,21,7.24,27.81a58.64,58.64,0,0,1,1.57,6.76s1.19,14.71,2.76,19.08c1.34,3.72,2.4,12.9,2.69,15.56a1.53,1.53,0,0,0-1.11,1.14c-.79,2.38-5.91,6-5.91,6a24.76,24.76,0,0,0-4.42,2.83c-1.61,1.45-1.55,2.76,5.6,1.94,13.8-1.59,16.16-6,16.16-6s-.22-.74-.55-1.7a18.46,18.46,0,0,0-1.81-4.27c-.2-.19-.4.13-.64.63a85.63,85.63,0,0,1-.94-19.31s.39-6.76-1.58-11.53a21.83,21.83,0,0,1,0-7.55c.79-3.18-3.15-18.69-3.15-18.69L816.23,651a3.28,3.28,0,0,1,.63,1.33l.06.23.06.33a.62.62,0,0,0,.16.31c1.54,6.4,5.76,24.13,5.76,26.73,0,3.18,3.94,11.13,3.94,11.13s1.57,13.12,4.33,18.28c2.1,3.93,2.15,9.69,2.05,12.23h0s-.47,3.27-1.26,4.06a9.66,9.66,0,0,0-1.49,2.59l-.09.19s-1.18,2.39-.39,2.79a19.37,19.37,0,0,0,9.46,1.19s1.58-2,1.58-2.78a12.52,12.52,0,0,0-.18-1.42c-.29-1.88-.83-4.84-.84-4.87.33-1.28-1-4.84-1-4.84l-.4-11.13a51.38,51.38,0,0,0-2.76-22.27s-2-7.15-1.57-9.14-.79-23.06-.79-23.06l-.12-.3a22.78,22.78,0,0,1,.12-4.47c.39-4.77-1.18-14.71-1.58-15.1a13.1,13.1,0,0,1-.56-3.29c.09-.83.36-3.26.73-6.28a112.91,112.91,0,0,0,11.62-1,1.45,1.45,0,0,0,1-.4,1.4,1.4,0,0,0,.23-.81C845.39,615.24,843.2,609.42,840.93,603.88ZM833,616.49c.41-2.85.85-5.61,1.29-7.66l.06.18a55.65,55.65,0,0,1,1.9,6.3A21.43,21.43,0,0,1,833,616.49Z"
                                                        transform="translate(-52 -162.63)"
                                                        fill="url(#eb6c86d6-45fa-49e0-9a60-1b0612516196)"></path>
													<path
                                                        d="M802.37,648.68s2.36,21.23,7.47,29.48a57.21,57.21,0,0,1,1.57,6.68s1.18,14.54,2.75,18.86,2.75,16.12,2.75,16.12,5.5,6.28,7.47,1.18a82.82,82.82,0,0,1-1.18-20.44s.39-6.68-1.57-11.4a21.32,21.32,0,0,1,0-7.46c.78-3.15-3.15-18.47-3.15-18.47l-3.14-15.33Z"
                                                        transform="translate(-52 -162.63)" fill="#be6f72"></path>
													<path
                                                        d="M827.13,724.93s-2.36,4.32-16.11,5.89c-7.14.81-7.19-.48-5.58-1.92a24.9,24.9,0,0,1,4.4-2.8s5.11-3.53,5.89-5.89,3.54-.39,3.54-.39c3.93,5.89,4.72-1.58,5.5-.79a17.73,17.73,0,0,1,1.81,4.21C826.91,724.19,827.13,724.93,827.13,724.93Z"
                                                        transform="translate(-52 -162.63)" fill="#ff6f61"></path>
													<path
                                                        d="M827.13,724.93s-2.36,4.32-16.11,5.89c-7.14.81-7.19-.48-5.58-1.92,1.12.71,3.38,1,7.93-.83,6.55-2.68,11.3-4.23,13.21-4.83C826.91,724.19,827.13,724.93,827.13,724.93Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M823.2,700.56s.39-6.68-1.57-11.4a21.32,21.32,0,0,1,0-7.46c.78-3.15-3.15-18.47-3.15-18.47l-3.14-15.33-2.72.16,3.11,15.17s3.93,15.32,3.15,18.47a21.32,21.32,0,0,0,0,7.46c2,4.72,1.57,11.4,1.57,11.4A82.82,82.82,0,0,0,821.63,721a3.44,3.44,0,0,1-1.14,1.62c1.46.66,3,.63,3.89-1.62A82.82,82.82,0,0,1,823.2,700.56Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M820.64,562.42a14,14,0,0,0-7.42,2.11,13.35,13.35,0,0,0-15,7.15,16.28,16.28,0,0,0-5.09,12.16c0,8.36,5.63,15.13,12.57,15.13,5,0,9.38-3.56,11.39-8.71a13.8,13.8,0,0,0,3.54.46,14.15,14.15,0,0,0,0-28.3Z"
                                                        transform="translate(-52 -162.63)" fill="#3f3d56"></path>
													<path
                                                        d="M832.24,650.65l1.18,3.14s1.18,20.83.78,22.8,1.58,9,1.58,9a50.31,50.31,0,0,1,2.75,22l.39,11s1.57,4.32.79,5.11S833,723,833,723s.79-8.25-2-13.36-4.32-18.08-4.32-18.08-3.93-7.86-3.93-11-6.29-28.69-6.29-28.69Z"
                                                        transform="translate(-52 -162.63)" fill="#be6f72"></path>
													<path
                                                        d="M801.58,650.26s.4-14.15,2-18.47a29.33,29.33,0,0,0,1.75-6.27s23.79,4.3,25.76,2.73c0,0,.39,5.5.79,5.89s2,10.22,1.57,14.94,0,4.71,0,4.71-16.12,2.36-16.51,0-1.57-2.35-1.57-2.35S800.8,651,801.58,650.26Z"
                                                        transform="translate(-52 -162.63)" fill="#3f3d56"></path>
													<g opacity="0.1">
														<path
                                                            d="M814.55,650.26s-9.94-.27-12.94-.81c0,.51,0,.81,0,.81-.78.78,13.76,1.18,13.76,1.18a1,1,0,0,1,.54.22C815.38,650.26,814.55,650.26,814.55,650.26Z"
                                                            transform="translate(-52 -162.63)"></path>
														<path
                                                            d="M831.85,634.14c-.4-.39-.79-5.89-.79-5.89a1.69,1.69,0,0,1-.67.24c.14,1.66.41,4.21.67,4.47s2,10.22,1.57,14.94,0,4.71,0,4.71-12.75,1.87-15.84.62c0,.17.09.36.12.56.39,2.36,16.51,0,16.51,0s-.4,0,0-4.71S832.24,634.54,831.85,634.14Z"
                                                            transform="translate(-52 -162.63)"></path>
														<path
                                                            d="M814.55,650.26s-9.94-.27-12.94-.81c0,.51,0,.81,0,.81-.78.78,13.76,1.18,13.76,1.18a1,1,0,0,1,.54.22C815.38,650.26,814.55,650.26,814.55,650.26Z"
                                                            transform="translate(-52 -162.63)"
                                                            fill="url(#ad310e25-2b04-44c8-bb7b-982389166780)"></path>
														<path
                                                            d="M831.85,634.14c-.4-.39-.79-5.89-.79-5.89a1.69,1.69,0,0,1-.67.24c.14,1.66.41,4.21.67,4.47s2,10.22,1.57,14.94,0,4.71,0,4.71-12.75,1.87-15.84.62c0,.17.09.36.12.56.39,2.36,16.51,0,16.51,0s-.4,0,0-4.71S832.24,634.54,831.85,634.14Z"
                                                            transform="translate(-52 -162.63)"
                                                            fill="url(#ad310e25-2b04-44c8-bb7b-982389166780)"></path>
													</g>
													<path
                                                        d="M818.9,587.18a5.21,5.21,0,0,0,1.5,1.94,3.36,3.36,0,0,0,1.41.61c-.88.51-1.84.89-2.72,1.4a48.36,48.36,0,0,0-4,3,8.14,8.14,0,0,1-4.59,1.78,5.62,5.62,0,0,0,.64-5.59,5.22,5.22,0,0,1-.7-1.94c0-1.13,1-2,1.93-2.6.75-.51,1.52-1,2.3-1.49.61-.38,1.53-1.18,2.3-1.15s.86.71,1,1.41A11.09,11.09,0,0,0,818.9,587.18Z"
                                                        transform="translate(-52 -162.63)" fill="#be6f72"></path>
													<path
                                                        d="M818.9,587.18a5.21,5.21,0,0,0,1.5,1.94,3.36,3.36,0,0,0,1.41.61c-.88.51-1.84.89-2.72,1.4a48.36,48.36,0,0,0-4,3,8.14,8.14,0,0,1-4.59,1.78,5.62,5.62,0,0,0,.64-5.59,5.22,5.22,0,0,1-.7-1.94c0-1.13,1-2,1.93-2.6.75-.51,1.52-1,2.3-1.49.61-.38,1.53-1.18,2.3-1.15s.86.71,1,1.41A11.09,11.09,0,0,0,818.9,587.18Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<circle cx="776.11" cy="438.7" r="9.04" fill="#be6f72"></circle>
													<path
                                                        d="M840.88,729.64c0,.79-1.57,2.75-1.57,2.75a19.46,19.46,0,0,1-9.43-1.18c-.79-.39.39-2.75.39-2.75l.09-.19a9.54,9.54,0,0,1,1.49-2.56c.78-.78,1.25-4,1.25-4,2.44-4,6.77,1.73,6.77,1.73s.55,3,.84,4.82C840.81,728.91,840.88,729.44,840.88,729.64Z"
                                                        transform="translate(-52 -162.63)" fill="#ff6f61"></path>
													<path
                                                        d="M840.88,729.64c0,.79-1.57,2.75-1.57,2.75a19.46,19.46,0,0,1-9.43-1.18c-.79-.39.39-2.75.39-2.75l.09-.19a19,19,0,0,0,8.56,1.76l1.79-1.79C840.81,728.91,840.88,729.44,840.88,729.64Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<circle cx="758.03" cy="419.83" r="8.84" fill="#be6f72"></circle>
													<path
                                                        d="M802.9,600.14c-1.94,1.85-2.26,4.78-2.46,7.45l-.3,3.91a19.72,19.72,0,0,0-.07,3.17c0,.65.16,1.31.22,2s.08,1.56.09,2.35l0,5.78a6.82,6.82,0,0,0,.64,3.47,2.36,2.36,0,0,0,3,1.08,3.42,3.42,0,0,0,1.24-1.92,29.85,29.85,0,0,0,2-7.32c.08-.87.09-1.74.17-2.61s.23-1.78.36-2.66c.56-3.68.81-7.4,1.06-11.11.07-1.1,1.06-5.43-.38-5.91-.7-.23-2.15.49-2.82.73A7.94,7.94,0,0,0,802.9,600.14Z"
                                                        transform="translate(-52 -162.63)" fill="#be6f72"></path>
													<path
                                                        d="M802.9,600.14c-1.94,1.85-2.26,4.78-2.46,7.45l-.3,3.91a19.72,19.72,0,0,0-.07,3.17c0,.65.16,1.31.22,2s.08,1.56.09,2.35l0,5.78a6.82,6.82,0,0,0,.64,3.47,2.36,2.36,0,0,0,3,1.08,3.42,3.42,0,0,0,1.24-1.92,29.85,29.85,0,0,0,2-7.32c.08-.87.09-1.74.17-2.61s.23-1.78.36-2.66c.56-3.68.81-7.4,1.06-11.11.07-1.1,1.06-5.43-.38-5.91-.7-.23-2.15.49-2.82.73A7.94,7.94,0,0,0,802.9,600.14Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M821.23,588.16s-6.68,5.11-10.61,6.29c0,0-2.33,2.66-2.73,3.3s-1.2,7.7-2,9.28-2.75,5.11,0,8.64c0,0,.78,3.54.39,4.72s-1.82,5.5-.52,5.7,5.24,2.94,8.77,2.16,16.71,3,16.71,3S833.42,611,835,607.42l-3.54-1.18s-9.82-5.89-3.93-14.93C827.52,591.31,822,590.13,821.23,588.16Z"
                                                        transform="translate(-52 -162.63)" fill="#ff6f61"></path>
													<path
                                                        d="M796.84,611.83l8.12,5.75a.6.6,0,0,0,.71,0l2.2-1.65a.62.62,0,0,0,0-.94l-6.25-5.55a.6.6,0,0,0-.61-.12L797,610.76A.61.61,0,0,0,796.84,611.83Z"
                                                        transform="translate(-52 -162.63)" fill="#3f3d56"></path>
													<path
                                                        d="M799.57,629.15a21.76,21.76,0,0,1-.85-6.13l-.12-2.77a8,8,0,0,1,.29-3.2,5.23,5.23,0,0,1,3.37-2.95,11.64,11.64,0,0,1,4.59-.33,1.18,1.18,0,0,1,.59.16,1.1,1.1,0,0,1,.32.54,3.62,3.62,0,0,1,.18,2.89,3,3,0,0,1-.92.95c-1.09.78-2.36,1.3-3.39,2.15a1.23,1.23,0,0,0-.46.58,1.35,1.35,0,0,0,0,.63,10.08,10.08,0,0,0,.52,1.61,1.59,1.59,0,0,0,.45.68c.19.16.44.22.64.37.43.35,1.33.81,1.28,1.36-.15,2-1.35,4.24-1.82,6.22C803.34,635.78,800.15,630.84,799.57,629.15Z"
                                                        transform="translate(-52 -162.63)" fill="#be6f72"></path>
													<g opacity="0.1">
														<path
                                                            d="M812.2,628.25l-.22,0a6.73,6.73,0,0,0,2.57,0l.25,0A9,9,0,0,0,812.2,628.25Z"
                                                            transform="translate(-52 -162.63)"></path>
														<path
                                                            d="M832.83,606.83c-1.41,3.17-3.49,20.34-3.86,23.75,1.38.36,2.29.62,2.29.62S833.42,611,835,607.42Z"
                                                            transform="translate(-52 -162.63)"></path>
													</g>
													<path
                                                        d="M828.42,601.61a3,3,0,0,0,1.29,1.95,17.31,17.31,0,0,1,2.29,2.32,16.43,16.43,0,0,1,2.27,4.55,55.75,55.75,0,0,1,1.89,6.22c-5,2.32-10.68,2.21-16,3.74a12.77,12.77,0,0,0-6,3.55,8.27,8.27,0,0,0-2.21,6.51.85.85,0,0,0,.12.39.84.84,0,0,0,.7.28,6,6,0,0,0,3.67-1.26,17.46,17.46,0,0,0,2.81-2.76,4.52,4.52,0,0,1,2-1.48c2.89-.88,6-.83,9-.88a113.42,113.42,0,0,0,13.41-1.06,1.08,1.08,0,0,0,1.2-1.2c.44-5.9-1.75-11.66-4-17.13q-1.35-3.29-2.78-6.55a19.4,19.4,0,0,0-3.4-5.82,6,6,0,0,0-2-1.48A8.46,8.46,0,0,0,830,591a19,19,0,0,0-4.39-.08,9.36,9.36,0,0,0-.42,2.27c.08,2.7,1.26,5.23,2.42,7.67C827.76,601.23,828.19,601.25,828.42,601.61Z"
                                                        transform="translate(-52 -162.63)" fill="#be6f72"></path>
													<path
                                                        d="M812.41,606.67a7.33,7.33,0,0,0-1,3.82,5.14,5.14,0,0,0,.25,1.56,5.06,5.06,0,0,0,3.19,3"
                                                        transform="translate(-52 -162.63)" fill="none" stroke="#000"
                                                        stroke-miterlimit="10" opacity="0.1"></path>
													<path
                                                        d="M817.5,573.82a4.59,4.59,0,0,0-1.45.23c-1.31-2-4.39-3.38-8-3.38-4.78,0-8.65,2.47-8.65,5.51s3.87,5.5,8.65,5.5a12.17,12.17,0,0,0,5.19-1.1,4.71,4.71,0,1,0,4.24-6.76Z"
                                                        transform="translate(-52 -162.63)" fill="#3f3d56"></path>
													<path
                                                        d="M817.89,582.07a4.7,4.7,0,0,1-4.24-2.67,12.17,12.17,0,0,1-5.19,1.1c-4.77,0-8.64-2.46-8.64-5.5a3.31,3.31,0,0,1,0-.55,3.73,3.73,0,0,0-.44,1.73c0,3,3.87,5.5,8.65,5.5a12.17,12.17,0,0,0,5.19-1.1,4.71,4.71,0,0,0,8.87-1.17A4.69,4.69,0,0,1,817.89,582.07Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M798.23,574a13.35,13.35,0,0,1,15-7.15,14.14,14.14,0,0,1,21.52,10.86c0-.39,0-.78,0-1.18a14.14,14.14,0,0,0-21.57-12,13.35,13.35,0,0,0-15,7.15,16.28,16.28,0,0,0-5.09,12.16c0,.4,0,.79,0,1.18A16,16,0,0,1,798.23,574Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M888.39,577c2.61-4.83-.35-10.76-3.76-15.07s-7.55-8.79-7.46-14.29c.12-7.89,8.5-12.55,15.2-16.74a73.9,73.9,0,0,0,13.64-11,19.93,19.93,0,0,0,4.2-5.61c1.38-3.09,1.34-6.6,1.26-10q-.44-16.9-1.67-33.76"
                                                        transform="translate(-52 -162.63)" fill="none" stroke="#3f3d56"
                                                        stroke-miterlimit="10" stroke-width="4"></path>
													<path
                                                        d="M922.52,469.93a12.29,12.29,0,0,0-6.14-10.08l-2.76,5.45.08-6.6a12.08,12.08,0,0,0-4.05-.49,12.31,12.31,0,1,0,12.87,11.72Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M900.57,553.26a12.31,12.31,0,1,1,.59-9.92l-7.69,6.26,8.46-2A12.24,12.24,0,0,1,900.57,553.26Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M894.49,529.35a12.31,12.31,0,0,1-3.91-24.15l-.06,5.07,2.79-5.51h0a12.32,12.32,0,0,1,12.87,11.73,12.3,12.3,0,0,1-11.72,12.87Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M924.37,509.47a12.31,12.31,0,1,1,5.45-23l-2.18,6,4.48-4.3a12.24,12.24,0,0,1,4,8.5,11.88,11.88,0,0,1-.31,3.39A12.31,12.31,0,0,1,924.37,509.47Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M920.87,473.21c-2.84.32-5.61,1.2-8.46,1.38s-6-.51-7.79-2.73a38.2,38.2,0,0,1-2.27-4,8.85,8.85,0,0,0-3.1-2.92,12.31,12.31,0,1,0,23,8.17C921.79,473.13,921.33,473.16,920.87,473.21Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M924.37,509.47a12.32,12.32,0,0,1-11.71-17.56,9.11,9.11,0,0,1,2.48,2.47,39.47,39.47,0,0,0,2.44,4.07c1.91,2.26,5.19,3,8.16,2.86s5.84-1,8.8-1.25c.41,0,.83-.07,1.24-.08A12.31,12.31,0,0,1,924.37,509.47Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M894.49,529.35A12.3,12.3,0,0,1,882.68,512a10.15,10.15,0,0,1,2.6,2.5c1,1.35,1.55,2.91,2.67,4.15,2.08,2.32,5.57,3.13,8.72,3.06s6-.82,9-1.05A12.31,12.31,0,0,1,894.49,529.35Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M900.57,553.26a12.32,12.32,0,0,1-22.45-10,12.06,12.06,0,0,1,2.7,2.41c1.18,1.43,1.95,3,3.3,4.38,2.52,2.47,6.58,3.48,10.2,3.58A53.94,53.94,0,0,0,900.57,553.26Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M1047.39,729c2.61-4.83-.35-10.76-3.76-15.07s-7.55-8.79-7.46-14.29c.12-7.89,8.5-12.55,15.2-16.74a73.9,73.9,0,0,0,13.64-11,19.93,19.93,0,0,0,4.2-5.61c1.38-3.09,1.34-6.6,1.26-10q-.43-16.9-1.67-33.76"
                                                        transform="translate(-52 -162.63)" fill="none" stroke="#3f3d56"
                                                        stroke-miterlimit="10" stroke-width="4"></path>
													<path
                                                        d="M1081.52,621.93a12.29,12.29,0,0,0-6.14-10.08l-2.76,5.45.08-6.6a12.08,12.08,0,0,0-4-.49,12.31,12.31,0,1,0,12.87,11.72Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M1059.57,705.26a12.31,12.31,0,1,1,.59-9.92l-7.69,6.26,8.46-2A12.24,12.24,0,0,1,1059.57,705.26Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M1053.49,681.35a12.31,12.31,0,0,1-3.91-24.15l-.06,5.07,2.79-5.51h0a12.32,12.32,0,0,1,12.87,11.73,12.3,12.3,0,0,1-11.72,12.87Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M1083.37,661.47a12.31,12.31,0,1,1,5.45-23l-2.18,6,4.48-4.3a12.24,12.24,0,0,1,4,8.5,11.88,11.88,0,0,1-.31,3.39A12.31,12.31,0,0,1,1083.37,661.47Z"
                                                        transform="translate(-52 -162.63)" fill="#6c63ff"></path>
													<path
                                                        d="M1079.87,625.21c-2.84.32-5.61,1.2-8.46,1.38s-6-.51-7.79-2.73a38.2,38.2,0,0,1-2.27-4,8.85,8.85,0,0,0-3.1-2.92,12.31,12.31,0,1,0,23,8.17C1080.79,625.13,1080.33,625.16,1079.87,625.21Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M1083.37,661.47a12.32,12.32,0,0,1-11.71-17.56,9.11,9.11,0,0,1,2.48,2.47,39.47,39.47,0,0,0,2.44,4.07c1.91,2.26,5.19,3,8.16,2.86s5.84-1,8.8-1.25c.41,0,.83-.07,1.24-.08A12.31,12.31,0,0,1,1083.37,661.47Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M1053.49,681.35A12.3,12.3,0,0,1,1041.68,664a10.15,10.15,0,0,1,2.6,2.5c1,1.35,1.55,2.91,2.67,4.15,2.08,2.32,5.57,3.13,8.72,3.06s6-.82,9-1.05A12.31,12.31,0,0,1,1053.49,681.35Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M1059.57,705.26a12.32,12.32,0,0,1-22.45-10,12.06,12.06,0,0,1,2.7,2.41c1.18,1.43,2,3,3.3,4.38,2.52,2.47,6.58,3.48,10.2,3.58A53.94,53.94,0,0,0,1059.57,705.26Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M530,587.91a9.81,9.81,0,0,0-3.91-7.92,11.5,11.5,0,0,0-9.61-11.28,11.48,11.48,0,0,0-7.65-8.55h0a9.76,9.76,0,0,0-1.42-.33l-.14,0-.25,0-.2,0-.22,0-.27,0h-.61a11.26,11.26,0,0,0-10.88,9.62c-3.51-.81-6.72.24-7.31,2.46a3,3,0,0,0,.1,1.78c.6,1.72,2.55,3.37,5.16,4.18l.34.1.51.13.11,0h0a8.84,8.84,0,0,0-.84,3.79,8.74,8.74,0,0,0,4.71,7.8l-.23.14a3.12,3.12,0,0,0-.76,1.06,15.86,15.86,0,0,0-1.47,3.54c-.25,1.06-.29,2.16-.6,3.21-.58,1.93-2.06,3.56-2.23,5.57-.08.9.07,2-.57,2.58a3.11,3.11,0,0,1-.51.36,4.13,4.13,0,0,0-1.11,1.17,3.88,3.88,0,0,1-.78,1.12c-.58.43-1.53.39-1.81,1.06s.45,1.46.28,2.19-.79.87-1.24,1.28a4.45,4.45,0,0,0-1.07,2,6.56,6.56,0,0,1-5.4-1.38,10.09,10.09,0,0,1,.56,2,8.76,8.76,0,0,0-2.91-1.46,9.19,9.19,0,0,1-3.1-2.3l1.66-.23L469,600.41l-2.17-.45,1.94,3.73a1.19,1.19,0,0,0-.66.79,3.39,3.39,0,0,0-.07,1.27,22.73,22.73,0,0,0,.5,3l.56,2.62c.05.24.11.49.18.73a6.89,6.89,0,0,0-2.17.24,18.88,18.88,0,0,0-4.51-1.07,2.5,2.5,0,0,0-1,0,2.59,2.59,0,0,0-.95.77,30.76,30.76,0,0,0-3.35,4.72c-4,6.86-9.21,12.85-14.14,19.05-3.4,1.38-6.83,2.69-10.26,4l-7.38,2.82a11.19,11.19,0,0,1-4.58,1c-.7,0-1.57-.15-2,.45a4.21,4.21,0,0,0-.45,1l-.76.08h0a8.74,8.74,0,0,0-1.77-3.26,1.12,1.12,0,0,0-.83.64c-.18.33-.29.69-.5,1-.39.58-1.08.86-1.57,1.35s-.8,1.25-1.34,1.75a5.3,5.3,0,0,1-2.51,1,15.84,15.84,0,0,0-3.93,1.49,6.33,6.33,0,0,1-1.45.67,5.45,5.45,0,0,1-1.71.06l-3.9-.29a3.08,3.08,0,0,0-1.05.05,2,2,0,0,0-1.24,2,3.15,3.15,0,0,0,1.24,2.15,7.94,7.94,0,0,0,2.22,1.15A65.15,65.15,0,0,0,409.79,658v.79a17.75,17.75,0,0,0,8,1.9,1.24,1.24,0,0,0,.6-.11,1.32,1.32,0,0,0,.5-.78,32.17,32.17,0,0,0,1.56-7.66h0a4,4,0,0,0,1.41-.36c1.34-.37,2.68.55,4.07.7,1.71.19,3.29-.81,4.91-1.42l.7-.23a7.68,7.68,0,0,1-1.62,1.55c-.67.48-1.42.84-2.07,1.34a14.26,14.26,0,0,1-1.76,1.36,11.05,11.05,0,0,1-1.35.52,8.91,8.91,0,0,0-1.93,1,3.91,3.91,0,0,0-1.31,1.28,1.92,1.92,0,0,0-.11,1.8,1.9,1.9,0,0,0,1.29.9,5.29,5.29,0,0,0,1.59,0c2.12-.16,4.23-.35,6.34-.54a16.92,16.92,0,0,0,2.65-.38c.46-.11.92-.26,1.38-.39a12.31,12.31,0,0,1,2.41-.36,2,2,0,0,1,.7.07c.49.16.84.66,1.34.76a2,2,0,0,0,1-.13,22.3,22.3,0,0,1,4.69-.5c.69,0,1.5-.17,1.82-.81s-.26-1.67-.52-2.52a7.71,7.71,0,0,1-.22-1.66,10.39,10.39,0,0,0-1.16-3.94,10.29,10.29,0,0,1-.87-1.8,45.47,45.47,0,0,0,8.63-4.38,15.43,15.43,0,0,1,4.92-2.37,13.38,13.38,0,0,1,6.18.72,140.61,140.61,0,0,1,14.17,4.94c2.21.91,4.41,1.87,6.69,2.56a10.43,10.43,0,0,0,2.38,2.31,11,11,0,0,0,5.15,1.41,67.48,67.48,0,0,0,7.25.27,12.23,12.23,0,0,0,5.4-1,5.55,5.55,0,0,0,1.13-.75,4.48,4.48,0,0,0,3.56-1.31A8.63,8.63,0,0,0,512,646.5c.59-2.28.14-5,1.54-6.82.37-.5.86-.9,1.23-1.4a3.94,3.94,0,0,0,.64-2.31.47.47,0,0,1,.06-.09,8.47,8.47,0,0,0,.6-2.3,13.14,13.14,0,0,1,.65-1.82,56.19,56.19,0,0,0,1.86-5.39,31.49,31.49,0,0,0,.86-3.7c.15-1,.24-2,.32-3a18.63,18.63,0,0,1,.91-4.63,37.68,37.68,0,0,0,2.1-11.08v-.22a7.43,7.43,0,0,0,6.58-7.56,7.89,7.89,0,0,0-.69-3.23A10,10,0,0,0,530,587.91Zm-35.78,43.47c.11.39.24.77.37,1.15-.59-.57-1.16-1.16-1.75-1.74.23.08.47.15.69.24A3.73,3.73,0,0,1,494.19,631.38Zm-2.83-1.92c-3.46-3-7.46-5.33-11-8.24,1.93.95,3.81,2.06,5.67,3.12q3.31,1.89,6.71,3.63A5.49,5.49,0,0,1,491.36,629.46Zm1.47-7.56.17-.17c0,.43,0,.82,0,1.19l-.93-.32A8.28,8.28,0,0,0,492.83,621.9ZM462.43,630a28.63,28.63,0,0,0,3.68-5.25c1.37,2.39,3.6,4.12,5.49,6.13C468.55,630.49,465.49,630.16,462.43,630Z"
                                                        transform="translate(-52 -162.63)"
                                                        fill="url(#a964f849-fa65-4178-8cc4-fb8fb10b3617)"></path>
													<polygon
                                                        points="415.35 438.25 417.53 438.69 424.96 449.68 421.69 450.12 415.35 438.25"
                                                        fill="#2f2e41"></polygon>
													<path
                                                        d="M414.49,645.46a42.78,42.78,0,0,0,2.18,5.67,54,54,0,0,0,7.25-2.07,22.86,22.86,0,0,0-1.51-2,7.46,7.46,0,0,0-2.05-1.57,5.64,5.64,0,0,0-2.49,0C416.75,645.46,415.62,645.48,414.49,645.46Z"
                                                        transform="translate(-52 -162.63)" fill="#fbbebe"></path>
													<path
                                                        d="M420.37,648.39a4.52,4.52,0,0,1-2.48-2.82A9.08,9.08,0,0,0,416,642.2a1.08,1.08,0,0,0-.84.63c-.18.32-.29.68-.49,1-.4.57-1.1.85-1.6,1.33s-.8,1.23-1.34,1.72a5.32,5.32,0,0,1-2.53,1,16.41,16.41,0,0,0-4,1.47,6.9,6.9,0,0,1-1.47.66,5.82,5.82,0,0,1-1.72,0l-3.94-.28a3.08,3.08,0,0,0-1,0,1.91,1.91,0,0,0-1.25,2,3,3,0,0,0,1.25,2.12,8.3,8.3,0,0,0,2.24,1.14,65.9,65.9,0,0,0,10.56,3v.78a18.28,18.28,0,0,0,8.05,1.87,1.36,1.36,0,0,0,.6-.1,1.32,1.32,0,0,0,.5-.78,31.2,31.2,0,0,0,1.65-8.56,6.82,6.82,0,0,0-.17-2.21c-.22-.71.52-.41-.16-.69"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<path
                                                        d="M495.93,633.54c-11.68.7-23.22-2.72-34.92-3.08a17.05,17.05,0,0,0-4.34.27,21.87,21.87,0,0,0-4.3,1.67c-6.27,2.92-12.76,5.34-19.24,7.75l-7.45,2.77a11.28,11.28,0,0,1-4.61,1c-.71,0-1.58-.15-2,.44-1.21,1.8-.64,4.43.16,6.45.26.65.7,1.36,1.4,1.43a4.07,4.07,0,0,0,1.42-.36c1.35-.36,2.7.55,4.1.7,1.72.18,3.32-.8,4.95-1.4,2.7-1,5.64-1,8.48-1.44,4.75-.79,9.16-3,13.28-5.51a15.5,15.5,0,0,1,4.95-2.34,13.7,13.7,0,0,1,6.23.71,143,143,0,0,1,14.29,4.86,66.61,66.61,0,0,0,7.46,2.73,27,27,0,0,0,16.73-1c1.87-.74,3.9-2,4-4.06.07-1.15-.83-2.16-1.42-3.15-.38-.63-.77-1.26-1.18-1.87C501.92,637.21,499.48,633.33,495.93,633.54Z"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<path
                                                        d="M495.93,633.54c-11.68.7-23.22-2.72-34.92-3.08a17.05,17.05,0,0,0-4.34.27,21.87,21.87,0,0,0-4.3,1.67c-6.27,2.92-12.76,5.34-19.24,7.75l-7.45,2.77a11.28,11.28,0,0,1-4.61,1c-.71,0-1.58-.15-2,.44-1.21,1.8-.64,4.43.16,6.45.26.65.7,1.36,1.4,1.43a4.07,4.07,0,0,0,1.42-.36c1.35-.36,2.7.55,4.1.7,1.72.18,3.32-.8,4.95-1.4,2.7-1,5.64-1,8.48-1.44,4.75-.79,9.16-3,13.28-5.51a15.5,15.5,0,0,1,4.95-2.34,13.7,13.7,0,0,1,6.23.71,143,143,0,0,1,14.29,4.86,66.61,66.61,0,0,0,7.46,2.73,27,27,0,0,0,16.73-1c1.87-.74,3.9-2,4-4.06.07-1.15-.83-2.16-1.42-3.15-.38-.63-.77-1.26-1.18-1.87C501.92,637.21,499.48,633.33,495.93,633.54Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M436.92,646q2.74.93,5.53,1.72c1-1.71,3-2.9,3.43-4.86a.46.46,0,0,0,0-.26.48.48,0,0,0-.21-.19,10.48,10.48,0,0,0-4.34-1.27c-1.2-.07-1.75,1.18-2.29,2.06C438.83,643.59,437.41,646.19,436.92,646Z"
                                                        transform="translate(-52 -162.63)" fill="#fbbebe"></path>
													<path
                                                        d="M439.48,644.78a4,4,0,0,1-2.69-1.77c-.25-.45-.53-1.06-1.05-1s-.69.6-.8,1.07a30,30,0,0,1-1.51,5.19,9.74,9.74,0,0,1-3.23,4.25c-.68.47-1.43.82-2.1,1.32a14.43,14.43,0,0,1-1.77,1.34,10.51,10.51,0,0,1-1.36.5,9.43,9.43,0,0,0-1.94,1,3.85,3.85,0,0,0-1.33,1.27,1.83,1.83,0,0,0-.1,1.77,1.89,1.89,0,0,0,1.29.88,5.44,5.44,0,0,0,1.61,0c2.13-.15,4.26-.34,6.39-.53a18.44,18.44,0,0,0,2.67-.37l1.39-.38a11.06,11.06,0,0,1,2.43-.36,1.89,1.89,0,0,1,.71.07c.49.16.84.65,1.35.75a2.26,2.26,0,0,0,1-.13,22.08,22.08,0,0,1,4.73-.49c.7,0,1.52-.18,1.84-.8.4-.78-.26-1.65-.53-2.49a8,8,0,0,1-.22-1.63,9.94,9.94,0,0,0-1.17-3.88c-.54-1-1.28-2.1-.9-3.16a3,3,0,0,0,.36-1.09C444.4,644.39,440.46,645,439.48,644.78Z"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<path
                                                        d="M478.42,614.8a9.12,9.12,0,0,1-4.66-4.62c-.19-.44-.35-.91-.54-1.35a10.26,10.26,0,0,0-2.78-3.87,1.37,1.37,0,0,0-1-.43,1.09,1.09,0,0,0-.75.81,3.09,3.09,0,0,0-.08,1.25,22.27,22.27,0,0,0,.5,2.92l.57,2.58a5.66,5.66,0,0,0,.35,1.15,4,4,0,0,0,.95,1.13c.52.48,1.05.94,1.58,1.4a5.43,5.43,0,0,0,1,.69,5.81,5.81,0,0,0,1.77.52,15.06,15.06,0,0,1,6.64,2.56s0-1.73,0-1.92a2.54,2.54,0,0,0-.5-1.39A8.87,8.87,0,0,0,478.42,614.8Z"
                                                        transform="translate(-52 -162.63)" fill="#fbbebe"></path>
													<path
                                                        d="M478.42,614.8a9.12,9.12,0,0,1-4.66-4.62c-.19-.44-.35-.91-.54-1.35a10.26,10.26,0,0,0-2.78-3.87,1.37,1.37,0,0,0-1-.43,1.09,1.09,0,0,0-.75.81,3.09,3.09,0,0,0-.08,1.25,22.27,22.27,0,0,0,.5,2.92l.57,2.58a5.66,5.66,0,0,0,.35,1.15,4,4,0,0,0,.95,1.13c.52.48,1.05.94,1.58,1.4a5.43,5.43,0,0,0,1,.69,5.81,5.81,0,0,0,1.77.52,15.06,15.06,0,0,1,6.64,2.56s0-1.73,0-1.92a2.54,2.54,0,0,0-.5-1.39A8.87,8.87,0,0,0,478.42,614.8Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<ellipse cx="450.2" cy="429.57" rx="2.84" ry="2.86"
                                                             fill="#2f2e41"></ellipse>
													<path
                                                        d="M500.44,590.48a3.07,3.07,0,0,0-2.24.32,3.11,3.11,0,0,0-.77,1.05,15.25,15.25,0,0,0-1.48,3.48c-.25,1-.29,2.13-.61,3.16-.58,1.9-2.07,3.51-2.24,5.49-.08.89.07,1.93-.58,2.53a3.68,3.68,0,0,1-.51.37,3.84,3.84,0,0,0-1.12,1.15,3.77,3.77,0,0,1-.79,1.09c-.58.43-1.54.39-1.82,1s.45,1.44.28,2.16-.8.86-1.25,1.26a4.37,4.37,0,0,0-1.08,2,6.67,6.67,0,0,1-5.44-1.35,17.32,17.32,0,0,1,.85,6.22,2.54,2.54,0,0,1-.5,1.92,34.64,34.64,0,0,1,6.67,1.16,4.6,4.6,0,0,0,2.91,0,4.86,4.86,0,0,0,1.34-1.06c1.2-1.22,2.46-2.55,2.75-4.24a24.91,24.91,0,0,0,.05-2.87,16.18,16.18,0,0,1,.73-3.49,41.53,41.53,0,0,0,1.27-7.18c.13-1.5,1.43-2.7,2.11-4,1.28-2.53,2.6-5.16,2.69-8C501.7,591.73,501.38,590.58,500.44,590.48Z"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<path
                                                        d="M500.44,590.48a3.07,3.07,0,0,0-2.24.32,3.11,3.11,0,0,0-.77,1.05,15.25,15.25,0,0,0-1.48,3.48c-.25,1-.29,2.13-.61,3.16-.58,1.9-2.07,3.51-2.24,5.49-.08.89.07,1.93-.58,2.53a3.68,3.68,0,0,1-.51.37,3.84,3.84,0,0,0-1.12,1.15,3.77,3.77,0,0,1-.79,1.09c-.58.43-1.54.39-1.82,1s.45,1.44.28,2.16-.8.86-1.25,1.26a4.37,4.37,0,0,0-1.08,2,6.67,6.67,0,0,1-5.44-1.35,17.32,17.32,0,0,1,.85,6.22,2.54,2.54,0,0,1-.5,1.92,34.64,34.64,0,0,1,6.67,1.16,4.6,4.6,0,0,0,2.91,0,4.86,4.86,0,0,0,1.34-1.06c1.2-1.22,2.46-2.55,2.75-4.24a24.91,24.91,0,0,0,.05-2.87,16.18,16.18,0,0,1,.73-3.49,41.53,41.53,0,0,0,1.27-7.18c.13-1.5,1.43-2.7,2.11-4,1.28-2.53,2.6-5.16,2.69-8C501.7,591.73,501.38,590.58,500.44,590.48Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M497.78,636c-2.43-1.16-4.21-3.31-6.18-5.16-3.75-3.53-8.34-6-12.29-9.33-2.62-2.19-5-4.74-7.83-6.58a19.57,19.57,0,0,0-8.39-2.93,2.23,2.23,0,0,0-1,0,2.3,2.3,0,0,0-.95.75,30.22,30.22,0,0,0-3.39,4.65c-5,8.5-12.11,15.64-18,23.54a1.5,1.5,0,0,0-.29.56,1.39,1.39,0,0,0,.22.87l1.2,2.34c.67,1.32,1.75,2.85,3.19,2.59a3.49,3.49,0,0,0,1.67-1.07l5.38-5.15,5.64-5.39c3.3-3.15,6.66-6.38,8.82-10.42,1.68,2.92,4.67,4.83,6.71,7.5.78,1,1.42,2.13,2.12,3.21,1.11,1.72,2.38,3.34,3.64,5l6.47,8.25a12.38,12.38,0,0,0,3,3,11.45,11.45,0,0,0,5.19,1.39,68.59,68.59,0,0,0,7.31.26,12.6,12.6,0,0,0,5.45-1,4.94,4.94,0,0,0,2.95-4.39,6.44,6.44,0,0,0-.93-2.73C505.15,641.59,502.13,638.07,497.78,636Z"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<ellipse cx="451.95" cy="431.55" rx="2.19" ry="2.2"
                                                             fill="#e8e8f0"></ellipse>
													<path
                                                        d="M503.47,592.53a4.21,4.21,0,0,1,.24,3.16,12.82,12.82,0,0,1-1.41,2.91,13.85,13.85,0,0,1-1.83,2.6c3.38.16,6.65-1.16,9.62-2.76,2.4-1.29,4.79-2.88,6.07-5.29a2.8,2.8,0,0,1-2.24-.42,8,8,0,0,1-1.71-1.59l-1.69-1.9a7.61,7.61,0,0,1-1-1.37,3.78,3.78,0,0,1-.45-2.21,12.87,12.87,0,0,1-2.24,1.86c-1.21.79-2.54,1.36-3.76,2.14-.42.28-1.38.72-1.13,1.28S503.13,591.93,503.47,592.53Z"
                                                        transform="translate(-52 -162.63)" fill="#fbbebe"></path>
													<path
                                                        d="M503.47,592.53a4.21,4.21,0,0,1,.24,3.16,12.82,12.82,0,0,1-1.41,2.91,13.85,13.85,0,0,1-1.83,2.6c3.38.16,6.65-1.16,9.62-2.76,2.4-1.29,4.79-2.88,6.07-5.29a2.8,2.8,0,0,1-2.24-.42,8,8,0,0,1-1.71-1.59l-1.69-1.9a7.61,7.61,0,0,1-1-1.37,3.78,3.78,0,0,1-.45-2.21,12.87,12.87,0,0,1-2.24,1.86c-1.21.79-2.54,1.36-3.76,2.14-.42.28-1.38.72-1.13,1.28S503.13,591.93,503.47,592.53Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M503.51,592.86a2.85,2.85,0,0,0,0,3.3c.1.12.46.87.61.88a.55.55,0,0,0,.38-.2,25.1,25.1,0,0,0,2.14-2.55,4.76,4.76,0,0,1,2.72-1.78,5.88,5.88,0,0,1,3.75,1.17,3.56,3.56,0,0,1,1.29,1,3.49,3.49,0,0,1,.42,1.43,68.9,68.9,0,0,1,.38,9.09,35.82,35.82,0,0,1-.43,6.55c-.22,1.11-.54,2.2-.69,3.33a21.24,21.24,0,0,0-.14,3v4.18a11,11,0,0,0,.16,2.29c.2.91.63,1.76.92,2.65a17.33,17.33,0,0,1,.61,3.13l.57,4.32c.18,1.36.32,2.85-.51,4-.37.49-.86.89-1.24,1.38-1.41,1.84-1,4.47-1.55,6.72a8.38,8.38,0,0,1-2.74,4.2,4.53,4.53,0,0,1-3.61,1.29c-1.76-.33-2.81-2.16-3.29-3.89s-.65-3.61-1.69-5.07a13.45,13.45,0,0,0-3.34-2.79,5.77,5.77,0,0,1-2.61-3.39c-.11-.63,0-1.28-.11-1.92a4.53,4.53,0,0,0-2.79-3.67c-.9-.35-2-.48-2.52-1.3a6.07,6.07,0,0,0,2.63-3.53,18.13,18.13,0,0,0,.64-4.46l.3-5c.14-2.25.2-4.47,1.19-6.49.44-.9,1-1.75,1.45-2.64a28.66,28.66,0,0,0,1.55-3.87c.72-2.06,1.44-4.12,2.08-6.2a11.68,11.68,0,0,1,1.14-2.89C501.72,594.19,502.5,593.06,503.51,592.86Z"
                                                        transform="translate(-52 -162.63)" fill="#e8e8f0"></path>
													<path
                                                        d="M492.36,625.5l1.33,0,.28-2a4.17,4.17,0,0,1,.28-1.17,7,7,0,0,1,.77-1.12c1.3-1.85,1.21-4.3,1.53-6.55a33.33,33.33,0,0,1,2.92-8.56l2.58-5.75a7.94,7.94,0,0,0,.56-1.47,8.82,8.82,0,0,0,.16-1.7c0-.89.28-4.13.31-5,0-.33-.73-.15-.88-.44-.32-.64-.76,1.09-1.37,1.47a3.36,3.36,0,0,0-1.19,1.74c-.56,1.44-.9,3-1.42,4.43a9.05,9.05,0,0,1-2.41,3.92c-.53.46-1.14.83-1.64,1.33-1.6,1.58-1.76,4.09-1.81,6.35C492.32,612.36,492.06,625.5,492.36,625.5Z"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<path
                                                        d="M511.5,590.47a4.54,4.54,0,0,0-2.37,3.25,16.44,16.44,0,0,1-1.2,4c-1.62,2.82-5.75,3.64-6.9,6.69a12.3,12.3,0,0,0-.46,3.15c-.2,2.19-.92,4.3-1.14,6.48a28.87,28.87,0,0,0,.12,5.09L501,636.35a23,23,0,0,1-.43,7.58,7.3,7.3,0,0,0,4.34-.72c1.35-.62,2.59-1.47,3.9-2.19,2.24-1.24,4.72-2.11,6.69-3.76a4.09,4.09,0,0,0,.9-1A8.65,8.65,0,0,0,517,634a12.2,12.2,0,0,1,.65-1.78,52.45,52.45,0,0,0,1.88-5.32,30.53,30.53,0,0,0,.86-3.64c.16-1,.25-2,.33-3a17.6,17.6,0,0,1,.92-4.55,36.65,36.65,0,0,0,2.11-10.92,5.71,5.71,0,0,0-.11-1.6,8.56,8.56,0,0,0-.88-1.83c-.6-1.13-1-2.37-1.52-3.52a17.31,17.31,0,0,0-5.91-6.21C514.17,590.84,512.81,590,511.5,590.47Z"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<path
                                                        d="M481.29,618.57a10.43,10.43,0,0,1-2.75-2.09c-.36-.42-.66-.89-1-1.31a6.81,6.81,0,0,0-3.46-2,16.9,16.9,0,0,0-4-.39,7.26,7.26,0,0,0-2.67.32,2.72,2.72,0,0,0-1.77,1.9,3.15,3.15,0,0,0,1.35,2.8,6.36,6.36,0,0,0,2.69,1.3,21.5,21.5,0,0,0,2.43.29c4.78.5,9.05,3.1,13.24,5.47q5.52,3.14,11.26,5.84a23.4,23.4,0,0,0,.19-3.45c0-.86-.07-2.18-.88-2.69a17.66,17.66,0,0,0-3.58-1.07,56.34,56.34,0,0,1-7.21-2.92C483.81,620,482.54,619.3,481.29,618.57Z"
                                                        transform="translate(-52 -162.63)" fill="#fbbebe"></path>
													<ellipse cx="450.2" cy="420.35" rx="8.52" ry="8.57"
                                                             fill="#fbbebe"></ellipse>
													<path
                                                        d="M513.93,600.18a13.66,13.66,0,0,0-1.65,3.09c-1,2.16-2.7,4-3.53,6.29s-.84,5.23-2.55,7.1a2.49,2.49,0,0,0-.75,1.08,5.16,5.16,0,0,1-.08.78c-.25.68-1.36.71-1.57,1.41-.06.2,0,.42-.1.62-.17.45-.77.52-1.16.79-.83.56-.66,1.86-1.25,2.67a2.25,2.25,0,0,1-2.13.75,7.5,7.5,0,0,1-2.21-.83,2.13,2.13,0,0,0-.8-.25,1.65,1.65,0,0,0-.92.32,5.67,5.67,0,0,0-2.3,4.85,15.84,15.84,0,0,0,1.32,5.43,2.18,2.18,0,0,0,.43.81,1.91,1.91,0,0,0,2.12.16,10,10,0,0,1,2.07-.93c1.32-.24,2.61.67,3.95.59a5.49,5.49,0,0,0,2.66-1.17,14.7,14.7,0,0,0,3.58-3c.58-.77,1-1.64,1.52-2.46a31.54,31.54,0,0,1,2.15-2.87l4.92-6a13.83,13.83,0,0,0,1.32-1.82,12.83,12.83,0,0,0,1-2.75,21.07,21.07,0,0,0,1-4.89,18.79,18.79,0,0,0-.36-4.1,14.49,14.49,0,0,0-1-3.91C518.54,599.68,515.94,597.84,513.93,600.18Z"
                                                        transform="translate(-52 -162.63)" fill="#2f2e41"></path>
													<path
                                                        d="M531.05,589a9.6,9.6,0,0,0-3.94-7.8,11.41,11.41,0,0,0-9.69-11.11,11.07,11.07,0,0,0-21.81.59c-3.54-.8-6.78.24-7.37,2.42s1.91,5,5.64,6a10.51,10.51,0,0,0,3.62.36,11.08,11.08,0,0,0,7.62,4.75,11.37,11.37,0,0,0,8.32,8.58,9.55,9.55,0,0,0,2.57,3.55c0,.27,0,.55,0,.83a7.21,7.21,0,1,0,14.42,0,7.7,7.7,0,0,0-.69-3.19A9.64,9.64,0,0,0,531.05,589Z"
                                                        transform="translate(-52 -162.63)" fill="#f86d70"></path>
													<path
                                                        d="M490.64,573.56c.59-2.18,3.83-3.22,7.37-2.42a11.26,11.26,0,0,1,11-9.48l.73,0a10.84,10.84,0,0,0-3.14-.47,11.26,11.26,0,0,0-11,9.48c-3.54-.8-6.78.24-7.37,2.42s1.91,5,5.64,6a11,11,0,0,0,2.14.37C492.44,578.39,490.05,575.79,490.64,573.56Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<path
                                                        d="M518.37,597.59a7.52,7.52,0,0,1,0-.82,9.34,9.34,0,0,1-2.57-3.55,11.34,11.34,0,0,1-8.32-8.59,11.05,11.05,0,0,1-7.63-4.74,9.6,9.6,0,0,1-2.13-.06,11.11,11.11,0,0,0,7.35,4.37,11.37,11.37,0,0,0,8.32,8.58,9.55,9.55,0,0,0,2.57,3.55c0,.27,0,.55,0,.83a7.34,7.34,0,0,0,7.17,7.46A7.46,7.46,0,0,1,518.37,597.59Z"
                                                        transform="translate(-52 -162.63)" opacity="0.1"></path>
													<ellipse cx="505.04" cy="584.74" rx="1.31" ry="1.75"
                                                             transform="translate(-302.82 261.95) rotate(-37.22)"
                                                             fill="#fbbebe"></ellipse>
												</svg>
                                                <!--end::Svg Icon-->
											</span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <!--begin::Callout-->
                                                                <div
                                                                    class="card card-custom mb-2 bg-diagonal bg-diagonal-light-primary">
                                                                    <div class="card-body">
                                                                        <div
                                                                            class="d-flex align-items-center justify-content-between p-4">
                                                                            <!--begin::Content-->
                                                                            <div class="d-flex flex-column mr-5">
                                                                                <a href="#"
                                                                                   class="h4 text-dark text-hover-primary mb-5">Or
                                                                                    Reach Us by Live Chat</a>
                                                                            </div>
                                                                            <!--end::Content-->
                                                                            <!--begin::Button-->
                                                                            <div class="ml-6 flex-shrink-0">
                                                                                <a href="#" data-toggle="modal"
                                                                                   data-target="#kt_chat_modal"
                                                                                   class="btn font-weight-bolder text-uppercase font-size-lg btn-primary py-3 px-6">Contact
                                                                                    Us</a>
                                                                            </div>
                                                                            <!--end::Button-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Callout-->
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <!--begin::Callout-->
                                                                <div
                                                                    class="card card-custom mb-2 bg-diagonal bg-diagonal-light-success">
                                                                    <div class="card-body">
                                                                        <div
                                                                            class="d-flex align-items-center justify-content-between p-4">
                                                                            <!--begin::Content-->
                                                                            <div class="d-flex flex-column mr-5">
                                                                                <a href="#"
                                                                                   class="h4 text-dark text-hover-primary mb-5">Phone
                                                                                    Call</a>
                                                                                {{--                                                                                <p class="text-dark-50">Windows 10 automatically installs updates--}}
                                                                                {{--                                                                                    <br>to make for sure</p>--}}
                                                                            </div>
                                                                            <!--end::Content-->
                                                                            <!--begin::Button-->
                                                                            <div class="ml-6 flex-shrink-0">
                                                                                <a href="#" data-toggle="modal"
                                                                                   data-target="#kt_chat_modal"
                                                                                   class="btn font-weight-bolder text-uppercase font-size-lg btn-success py-3 px-6">Submit
                                                                                    a Request</a>
                                                                            </div>
                                                                            <!--end::Button-->
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!--end::Callout-->
                                                            </div>
                                                        </div>
                                                        <!--end::Row-->
                                                    </div>
                                                    <!--end::Content-->
                                                </div>
                                                <!--begin::Content Wrapper-->
                                            </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                                            </button>
                                            <a href="{{url('/Advertiser/Dashboard/Manage-Campaign')}}" type="button"
                                               class="btn btn-primary">Continue</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





    {{--    <div class="col-lg-3 ">--}}
    {{--        <div class="asdfasdf ueytui " style="margin-top: 38px;margin-right: 16px;">--}}
    {{--            <p><i class="far fa-check-circle sidecomdet "></i><span  class="companginfo text-wrap">Compaign Details</span></p>--}}
    {{--            <p><i class="far fa-times-circle sidecomdet text-danger"></i><span class="companginfo text-wrap">Location Details</span></p>--}}
    {{--            <p><i class="far fa-times-circle sidecomdet text-danger"></i><span class="companginfo text-wrap">Car info</span></p>--}}
    {{--            <p><i class="far fa-times-circle sidecomdet text-danger"></i><span class="companginfo text-wrap">Sticker Detials</span></p>--}}
    {{--        </div>--}}
    {{--    </div>--}}


    {{--   @include('pages.advertisor.checkout')--}}

@endsection

{{-- Scripts Section --}}
@section('scripts')
    {{--    <script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>--}}
    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
    <script>


        $('.btn-terms').css('cursor', ' auto');
        $("#btnAddLocationTab").click(function () {
            $("#detectlocationdelet").val(2)
            var check = 1;
            $('#tabContenttabLocationInfo .form-control').each(function (index) {
                // console.log($(this).val())
                if ($(this).val() == "" || $(this).val() == null) {
                    $(this).next().removeClass("d-none");
                    check = 2;
                    toastr.error('', 'Empty field');
                } else {
                    $(this).next().addClass('d-none');
                }

            });
            if (check == 1) {

                $("#tabLocationInfo .nav-item .nav-link").removeClass('active');
                $("#tabContenttabLocationInfo .tab-pane").removeClass('active show');
                var totaltabs = parseInt($("#addinglocationvalue").val());

                var elemTab = "  <li class=\"nav-item\" role=\"presentation\" id=\"location-tab-btn" + totaltabs + "\">" +
                    "<a class=\"nav-link active \" id=\"location-tab" + totaltabs + "\" data-toggle=\"tab\" href=\"#location" + totaltabs + "\" role=\"tab\" aria-controls=\"contact" + totaltabs + "\" aria-selected=\"false\">Location " + totaltabs + "</a>\n" +
                    "</li>";

                var elemTabContent = " <div class=\"tab-pane fade active show\" id=\"location" + totaltabs + "\" role=\"tabpanel\" aria-labelledby=\"location-tab" + totaltabs + "\">\n" +
                    "                                           <div class=\"tab-inner\">\n" +
                    "                                               <div class=\"row border-bottom \">\n" +

                    " <p class=\"px-3 font-weight-bold pt-2 mb-0 w-100 \">Location " + totaltabs + "\  <i class=\"fas fa-trash mr-2 cursor-pointer float-right\" onclick=\"deletcartabs(" + totaltabs + ")\"></i>   </p>" +
                    "                                               </div>\n" +
                    "                                               <div class=\"row mt-n2\">\n" +
                    "                                                   <!--begin::Input-->\n" +
                    "                                                   <div class=\"col-lg-6\">\n" +
                    "                                                       <div class=\"form-check pl-0\">\n" +
                    "                                                           <label class=\"mt-5\">Country:</label>\n" +
                    "                                                           <select disabled name=\"countries[]\" id=\"country_name" + totaltabs + "\" onchange=\"showValues();\" class=\"form-control drope contrydisable\" id=\"contrydisableid\">\n" +
                    "                                                               <option selected value=\"1\">America</option>\n" +
                    "\n" +
                    "                                                           </select>\n" +
                    "                                                           <span class=\"form-text text-danger d-none mb-3\" id=\"country_name_error\">Select Country.</span>\n" +
                    "                                                       </div>\n" +
                    "                                                   </div>\n" +
                    "                                                   <div class=\"col-lg-6\">\n" +
                    "                                                       <div class=\"form-check pl-0\">\n" +
                    "                                                           <label class=\"mt-5\">State:</label>\n" +
                    "                                                           <select name=\"states[]\" id=\"states_" + totaltabs + "\" onChange=\"getCitiess(" + totaltabs + ")\" class=\"form-control   statedisable  drope\" >\n" +
                    "                                                               <option selected value=''>Select state </option>\n" +
                    "                                                              @foreach($data['states'] as $state)\n" +
                    "                                                               <option value=\"{{$state->id}}\">{{$state->state}}</option>\n" +
                    "                                                               @endforeach \n" +
                    "\n" +
                    "                                                           </select>\n" +
                    "                                                           <span class=\"form-text text-danger d-none mb-3\" id=\"state_error\"> This field is required.</span>\n" +
                    "                                                       </div>\n" +
                    "                                                   </div>\n" +
                    "                                                   <div class=\"col-lg-6\">\n" +
                    "                                                       <div class=\"form-check pl-0\">\n" +
                    "                                                           <label class=\"d-block mt-5\">City:</label>\n" +
                    "                                                           <select onChange=\"getZips(" + totaltabs + ")\" name=\"cities[]\" id=\"city_" + totaltabs + "\" class=\"form-control cityecolect  drope\">\n" +
                    "                                                               <option selected value=''>Select city</option>\n" +
                    "                                                           </select>\n" +
                    "                                                           <span class=\"form-text text-danger d-none mb-3\" id=\"city_error\">This field is required.</span>\n" +
                    "                                                       </div>\n" +
                    "                                                   </div>\n" +
                    "                                                   <div class=\"col-lg-6 w-100\">\n" +
                    "                                                       <div class=\"form-check pl-0 w-100\">\n" +
                    "                                                           <label class=\"d-block mt-5\">Zip:</label>\n" +
                    "                                                           <select id=\"zips_" + totaltabs + "\" class=\"select2 form-control drope findzips\" style=\"width:200px !important;\" name=\"zips[]\" multiple>\n" +
                    "                                                               <option value=\"\" disabled > Zip code</option>\n" +
                    "                                                           </select>\n" +
                    "                                                           <span class=\"form-text text-danger d-none mb-3\" id=\"findzips_error\">Select Zip code</span>\n" +
                    "                                                       </div>\n" +
                    "                                                   </div>\n" +
                    "                                               </div>\n" +
                    "                                           </div>\n" +
                    "                                       </div>";


                $("#tabLocationInfo").append(elemTab);
                $("#tabContenttabLocationInfo").append(elemTabContent);
                // $("#location-tab" + totaltabs).addClass("active");
                var totatCares = $("#tabLocationInfo .nav-item").length; //geting carent car count
                console.log(totatCares);
                $(".totallocaitionprevew").text(totatCares); //showing user his total cars
                var nextabtext = totaltabs + 1;
                $("#addinglocationvalue").val(nextabtext);
            }
            // calling select2
            runslect2()
        });

        // car details
        let days = [];
        var cararrrydetails = [];
        let testAry = [];
        function carDetailss() {
            $("#carpreviewdetails").html('');
            cararrrydetails = [];
            testAry = [];
            var testData = {};

            days = [];
            var namae;
            var value;
            var value2;
            var Totalcars = 1;
            $('#car-tabs-warap .tab-pane').each(function (index) {
                var cardetail = [];
                testData = {days: []};
                $(this).find('.form-control').each(function (index) {
                    index = $(this).attr('name');
                    if ($(this).hasClass('selected_zips')) {
                        value = $(this).val();
                        valuerr = $(this).val();
                    } else if ($(this).hasClass('drope')) {
                        value = $(this).find('option:selected').text();
                        valuerr = $(this).find('option:selected').text();
                    } else if ($(this).hasClass('carinputs')) {
                        if ($(this).is(":checked")) {
                            value = $(this).val();
                            valuerr = $(this).val();
                        } else {
                            value = ""
                            valuerr = ""
                        }
                    } else if ($(this).hasClass('getvals')) {
                            value = $(this).val();
                            valuerr = $(this).val();
                    } else {
                        value = $(this).val();
                        valuerr = $(this).find('option:selected').text();
                    }

                    // console.log("your car colected values is = ",valuerr)
                    cardetail.push({
                        name: index,
                        value: valuerr,
                    });


                    if (index == "days_1") {
                        testData.days.push(value)
                    } else {
                        testData[index] = value
                    }

                });


                var value10 = cardetail[0]['value'];
                var value11 = cardetail[1]['value'];
                var value12 = cardetail[2]['value'];
                var value13 = cardetail[3]['value'];
                var value14 = cardetail[4]['value'];
                var value15 = cardetail[5]['value'];
                // console.log(value15);
                var value16 = cardetail[6]['value'];
                var value17 = cardetail[7]['value'];
                var value18 = cardetail[8]['value'];
                var value19 = cardetail[9]['value'];
                var value20 = cardetail[10]['value'];
                var value21 = cardetail[11]['value'];
                var value22 = cardetail[12]['value'];
                var value23 = cardetail[13]['value'];
                var value24 = cardetail[14]['value'];
                var value25 = cardetail[15]['value'];

                cararrrydetails.push(cardetail);
                testAry.push(JSON.stringify(testData));

                var locationdd = `
<div class="card">
                                                <div class="card-header" id="headingOnea${Totalcars}">
                                                    <h2 class="mb-0">
                                                        <button class="btn text-white  btn-block text-left"  type="button" data-toggle="collapse"
                                                         data-target="#collapseOnea${Totalcars}" style="background: #3c9bd1" aria-expanded="true" aria-controls="collapseOnea${Totalcars}">
                                                          Car Details  ${Totalcars}
                                                        </button>
                                                    </h2>
                                                </div>
                                                <div id="collapseOnea${Totalcars}" class="collapse " aria-labelledby="headingOnea${Totalcars}" data-parent="#carpreviewdetails">
                                                    <div class="card-body">


    <table class="table table-responsive">
                                            <tbody >


   </tr><tr>
                                        <th>State / City:</th>
                                        <td>${value10}</td>
                                    </tr>
                                    <tr>
                                        <th>Zips:</th>
                                        <td>${value11}</td>
                                    </tr>
                                    <tr>
                                        <th>Number of Cars:</th>
                                        <td>${value12}</td>
                                    </tr>
                                    <tr>
                                        <th>Car Make:</th>
                                        <td>${value13}</td>
                                    </tr>
                                    <tr>
                                        <th>Car Name:</th>
                                        <td>${value14}</td>
                                    </tr>
                                    <tr>
                                        <th>year:</th>
                                        <td>${value15}</td>
                                    </tr>
  <tr>
                                        <th>Car Type:</th>
                                        <td>${value16}</td>
                                    </tr>
                                    <tr>
                                        <th>Mileage Drive:</th>
                                        <td>${value17}</td>
                                    </tr>
                                    <tr>
                                        <th>Mileage Rate:</th>
                                        <td>${value18}</td>
                                    </tr>
                                    <tr>
                                        <th>Days:</th>
                                        <td>${value19},${value20},${value21},${value22},${value23},${value24},${value25}</td>
                                    </tr>
         </tbody>
                                        </table>

</div>
                                                </div>
                                            </div>`;


                $("#carpreviewdetails").append(locationdd);
                Totalcars++;
            });
            console.log("car all details ", testAry);
        }

        let locationdbs = [];
        var locationdetails = [];
        var testLocation = [];
        let testTwo = {};
        let locationdbdetails = [];

        function locationDetails() {
            locationdbs = [];
            testTwo = {}
            locationdbdetails = [];
            testLocation = [];
            $("#locationprevdetails").html('');
            // locationdetails = [];
            var namae;
            var value;
            var value2;
            var Totalcars = 1;
            $('#tabContenttabLocationInfo .tab-pane').each(function (index) {
                var cardetail = [];
                var locationdbinerarry = {};
                testTwo = {}
                $(this).find('.form-control').each(function (index) {
                    index = $(this).attr('name');
                    if ($(this).hasClass('findzips')) {
                        value = $(this).val();
                    } else if ($(this).hasClass('drope')) {
                        value9 = $(this).find('option:selected').text();
                        value = $(this).find('option:selected').val();
                        value2 = $(this).find('option:selected').val();
                    } else {
                        value = $(this).val();
                    }

                    cardetail.push({
                        name: index,
                        value: value,
                        text: value9,
                    });


                    testTwo[index] = value2
                    locationdbinerarry[index] = value
                    // side appending cars

                });

                var Country = cardetail[0]['text'];
                var State = cardetail[1]['text'];
                var City = cardetail[2]['value'];
                var Zip = cardetail[3]['value'];
                console.log("carmake", Country);
                console.log("numberofcars", State);
                console.log("milages", City);
                console.log("milages", Zip);
                locationdetails.push(cardetail);
                locationdbdetails.push(cardetail);
                testLocation.push(testTwo);

                locationdbs.push(JSON.stringify(locationdbinerarry));

                var locationdd =`
<div class="card">
                                                <div class="card-header" id="headingOne${Totalcars}">
                                                    <h2 class="mb-0">
                                                        <button class="btn text-white  btn-block text-left"  type="button" data-toggle="collapse"
                                                         data-target="#collapseOne${Totalcars}" style="background: #3c9bd1" aria-expanded="true" aria-controls="collapseOne${Totalcars}">
                                                            Location ${Totalcars}
                                                        </button>
                                                    </h2>
                                                </div>

                                                <div id="collapseOne${Totalcars}" class="collapse " aria-labelledby="headingOne${Totalcars}" data-parent="#locationprevdetails">
                                                    <div class="card-body">


    <table class="table">
                                            <tbody id="locationprevdetails">


    <tr>
                                        <th>Country:</th>
                                        <td>${Country}</td>
                                    </tr>
                                    <tr>
                                        <th>State:</th>
                                        <td>${State}</td>
                                    </tr>
                                    <tr>
                                        <th>City:</th>
                                        <td>${City}</td>
                                    </tr>
                                    <tr>
                                        <th>Zip:</th>
                                        <td>${Zip}</td>
                                    </tr>
         </tbody>
                                        </table>

</div>
                                                </div>
                                            </div>`;

                $("#locationprevdetails").append(locationdd);
                Totalcars++;
            });
            console.log("locationssss detailsaa ", locationdbs);
        }

        // car side preview details

        // cars cost details
        /*function carcost() {
            $(".carCostappend").html('');

            var Totalcars = 1;
            var TotalCost = 0;
            $('#car-tabs-warap .tab-pane').each(function (index) {
                var carCountew
                var milages;
                var mailagerate;
                var carMake;
                var getlocations;
                $(this).find('.form-control').each(function (index) {
                    if ($(this).hasClass('numberofcars')) {
                        carCountew = parseInt($(this).val());
                        // console.log("numberofcars", carCountew);
                    }

                    if ($(this).hasClass('kt_slider_a')) {
                        milages = parseInt($(this).val());
                        // console.log("milages", milages)
                    }

                    if ($(this).hasClass('mailage-rate')) {
                        mailagerate = $(this).val();
                        // console.log("mailagerate", mailagerate)
                    }

                    if ($(this).hasClass('carMake')) {
                        carMake = $(this).find('option:selected').text();
                        // console.log("carMake", carMake)
                    }



                });
                var GrandCost = carCountew * mailagerate;
                var html = `<tr>
                                    <th>${Totalcars}</th>
                                    <th>${getlocations}</th>
                                    <th>${carMake}</th>
                                    <th>${carCountew}</th>
                                    <th>${milages}</th>
                                    <th>${mailagerate}</th>
                                    <th>${GrandCost}</th>
                                </tr>`;
                $(".carCostappend").append(html);
                TotalCost += GrandCost;
                Totalcars++;

            });

            $(".carTotalCost").text(TotalCost);

        }*/

        // car details 2


        //*******************************


        $(".topbuttons").click(function () {
            $(".topbuttons").removeClass('topbuttons-active');
            $(this).addClass('topbuttons-active');
            var oky = $(this).attr('id');
            var oky = oky.substr(oky.length - 1);
            var oky = parseInt(oky);
            // console.log(oky)
            activetab(oky);


        })


        // geting loacation after also deleting the locations
        function updatelocations() {
            if ($("#firstaddlocaitonscheck").val() == 1) {
                $(".getlocations").find('option').remove() //making empty location input
                // var newOption = new Option('Choose Locations', "nills", true, true);
                $('.getlocations').append("<option selected disabled='true'>Choose Location</option>");  //adding empty record
                console.log($("#tabContenttabLocationInfo .tab-pane").length);
                $("#tabContenttabLocationInfo .tab-pane").each(function (index) {
                    var locationName = $(this).find('.statedisable').find(":selected").text();
                    var cityesname = $(this).find('.cityecolect').find(":selected").text();
                    console.log("location Name", locationName);
                    var locationWithState = locationName + " / " + cityesname;
                    var newOption = new Option(locationWithState, index, true, false);
                    $('.getlocations').append(newOption);
                });
                $("#firstaddlocaitonscheck").val(2);
            }
        }


        // rest cars data
        function restcarsdetails() {
            if ($("#detectlocationdelet").val() == 2) {
                //**********************************************
                console.log("Car reseting.....")

                $("#car-tabs-warap").find('.tab-pane').not(":eq(0)").remove();
                $("#carstabs").find('li').not(":eq(0)").remove();
                $('#carstabs li:first-child a').tab('show') // Select first tab
                // resetting form at first tab
                $(".milage-notification").text('Select Mileage to get your cost.');
                $(".milage-notification").removeClass('text-danger');
                $('#car-tabs-warap .tab-pane').each(function (index) {
                    $(this).find('.form-control').each(function (index) {

                        if ($(this).hasClass('carinputs')) {
                            // not to change valueat this poing
                        } else {
                            $(this).val('');
                        }
                        if ($(this).hasClass('selected_zips')) {
                            $(this).find("option").remove();
                        }
                        if ($(this).hasClass('kt_slider_a')) {
                            var ranger = $(this).data("ionRangeSlider");
                            ranger.update({
                                min: 0,
                            });
                        }

                    });
                });
                //**********************************************
                // resetting  location delet check
                $("#detectlocationdelet").val(1);
            }

        }


        // geting zips after also deleting the locations
        $(document).on('change', '.getlocations', function (e) {
            var locationid = $(this).val();
            if (locationid != "nills") {
                console.log(locationid)
                $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.selected_zips').find('option').remove()//making empty location input
                var arry = $("#tabContenttabLocationInfo .findzips:eq(" + locationid + ")").val();
                var cityesname = $("#tabContenttabLocationInfo .cityecolect:eq(" + locationid + ")").find(":selected").text();
                var cityesid = $("#tabContenttabLocationInfo .cityecolect:eq(" + locationid + ")").find(":selected").val();
                var stateid = $("#tabContenttabLocationInfo .statedisable:eq(" + locationid + ")").find(":selected").val();
                console.log('your city name', cityesname);
                console.log("appending arry", locationid)
                $.each(arry, function (index, value) {
                    console.log('your index is %s and your value is %s', index, value);
                    var newOption = new Option(value, value, true, true);
                    console.log("parent element", newOption)
                    $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.selected_zips').append(newOption);

                });

                console.log(arry);
                $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.citydeils').val(cityesid);
                $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.statedetails').val(stateid);
                // alert($(this).val());

            } else {
                $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.selected_zips').find('option').remove();
            }

        });


        $('.checkoutimge').click(function () {
            $('.checkoutimge').css('border', '2px solid white');
            $(this).css('border', '2px solid #0099df');

        })
        // loding scrips
        // hiding scrpts
        $(".allstepsdivs").hide();
        // hiding buttons
        $(".stepsbtnr").hide();
        // showing first ones
        $("#stepdiv1").show();
        $("#stepbtn1").show();
        // hideing edite car div
        $("#editecarsdive").hide();
        $(".finaledtebtns").hide();


        // calling select2
        $('.select2').select2({
            placeholder: "Zip Code",
            allowClear: true,
            width: '100%'
        });

        $(".sidebardivss").click(function () {
            if ($(this).find('i').hasClass('fa-check')) {
                $(".sidebardivss").removeClass('activesidebar');
                $(this).addClass('activesidebar');
            } else {
                console.log("Un active");
            }


        })

        // changing add car buttons
        //  $(".addcarcs").click(function () {
        //      alert("clicked")
        //
        //
        //  })
        // edite compain funtions

        function runslect2() {
            $('.select2').select2({
                placeholder: "Zip Code",
                // allowClear: true,
                width: '100%'
            });
        }


        function activetab(id) {
            $(".allstepsdivs").hide();
            $("#stepdiv" + id).show(1400);
            $(".stepsbtnr").hide();
            $("#stepbtn" + id).show();
            $(".adiwe").addClass("sdfq");
            for (var i = 1; i <= id; i++) {
                $("#progresbb" + i).removeClass("sdfq");
            }
            $(".sidebardivss").removeClass("activesidebar");
            $("#sidebara" + id).addClass("activesidebar");
            // active top buttons
            $(".topbuttons").removeClass("topbuttons-active");
            $("#topbuttons" + id).addClass("topbuttons-active");
        }


        function tabformvalidaton(tabid) {
            var activebtn = 0;
            console.log(tabid);
            $('#stepdiv' + tabid + ' .form-control').each(function (index) {
                console.log($(this).val())
                if ($(this).val() == "" || $(this).val() == null) {
                    if ($(this).hasClass('carinputs')) {

                    } else {
                        $(this).next().removeClass("d-none");
                        console.log($(this).attr('name'));
                        activebtn = 2;
                        toastr.error('', 'Empty field');
                        $('#carstabs li:last-child a').tab('show');
                    }
                } else {
                    $(this).next().addClass('d-none');
                }

            });

            console.log("validte btn", activebtn)
            if (activebtn == 0) {
                activetab(tabid + 1);
                $('.tick-contorler .sidebardivss').each(function (index) {
                    if (tabid - 1 == index) {
                        $(this).find('.sidecomdet').addClass('fa-check').removeClass('fa-exclamation-circle');
                    }
                });


                if (tabid + 1 == 3) {

                    // reseting cars details if any changes found in location details
                    restcarsdetails();
                    updatelocations();
                }


                if (tabid + 1 == 5) {
                    let sum = 0;
                    $('.add_cars_1 .testsum').each(function () {
                        var dataLayer = $(this).data('subtotal');
                        sum += dataLayer;
                    });
                    $('.totalamount').text(sum.toFixed(2));

                    // showing cars and locations
                    locationDetails()
                    carDetailss()
                    carcost();
                }


                console.log("your are in")
            }
            runslect2()


        }


        function getpassvalue(input, pasvalue, ifempty) {
            no_0f_days = '';
            if (input == '') {
                $("." + pasvalue).text(ifempty);
            } else {
                if (pasvalue == 'timeintervalcom' || pasvalue == 'timedurationcom') {
                    // dte=("#kt_daterangepicker_1").val();
                    var intervals = $('#kt_daterangepicker_1').val().split(' - ');
                    var start = moment(intervals[0], "MM-DD-YYYY");
                    var end = moment(intervals[1], "MM-DD-YYYY");

                    var no_0f_days = moment.duration(start.diff(end)).asDays();
                    no_0f_days = no_0f_days.toString();

                    if (no_0f_days != 0 && no_0f_days != '') {
                        days = no_0f_days.substring(1);

                        days++;
                        if ($('#campaign_duration').val() == 'Week') {
                            var weeks = days / 7;
                            var day = days % 7;
                            $(".totaldays").html(days + '<small> (' + Math.floor(weeks) + 'Weeks & ' + day + 'days)</small>');

                        } else if ($('#campaign_duration').val() == 'Months') {
                            var month = days / 30;
                            var day = days % 30;
                            $(".totaldays").html(days + '<small> (' + Math.floor(month) + 'Month & ' + day + 'days)</small>');

                        } else if (days != '') {
                            $(".totaldays").text(days + ' days');

                        }

                    } else if (no_0f_days == 0) {
                        $(".totaldays").text('1day');
                    }
                }
                $("." + pasvalue).text(input);
                if (isNaN(no_0f_days)) {
                    $(".totaldays").text('');
                }
                if (pasvalue == 'compaintype') {
                    $("." + pasvalue).text($("#campaign_type option:selected").text())
                }


            }
        }


        // custom prefix
        runmilage()

        function editecompaign() {
            activetab(1);
        }

        function editelocaiton() {
            tabformvalidaton(1);
        }

        function editeacrs() {
            tabformvalidaton(2);


        }

        function previwcomdetils() {
            tabformvalidaton(4);

        }

        function stickeredite() {
            tabformvalidaton(3);

        }

        //
        // shiwing imge in sticker dive
        function slctstiker() {
            $("#uploadstickerimge").click();
            // alert("sdf")
        }

        $(document).on('click', '.slectdays', function (e) {
            var color = $(this).css("background-color");
            if (color == 'rgb(60, 155, 209)') {
                $(this).css('background', 'white');
                $(this).css('color', 'black');
                $(this).find('input').prop("checked", false);
            } else {
                $(this).css('background', '#3c9bd1');
                $(this).css('color', 'white');
                $(this).find('input').prop("checked", true);
            }
        })

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#loadimgedds').attr('src', e.target.result);
                    $('.previewstckerimge').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#uploadstickerimge").change(function () {
            readURL(this);
        });
        var today = new Date();
        var tomorrow = new Date();
        tomorrow.setDate(today.getDate() + 1)
        $('#kt_daterangepicker_1, #kt_daterangepicker_1_modal').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            minDate: moment(tomorrow),

            maxYear: parseInt(moment().add(1, 'years').format('YYYY'), 10),

        });
        // $('#kt_daterangepicker_1').val('');


        // map configration starts

        function refrashmap(allzips) {
            var markers = [];
            var zips = allzips;
            for (i = 0; i < zips.length; i++) {
                $.ajax({
                    'async': false,
                    'type': "GET",
                    'global': false,
                    'dataType': 'json',
                    'url': "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=" + zips[i] + "&key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI",
                    'success': function (data) {
                        var title = data.results[0].formatted_address
                        var lat = data.results[0]['geometry']['location']['lat']
                        var lng = data.results[0]['geometry']['location']['lng']
                        console.log('latitude', data.results[0]['geometry']['location']['lat']);
                        markers.push({
                            "title": title,
                            "lat": lat,
                            "lng": lng,
                            "description": title
                        })

                    }
                });
            }

            LoadMap(markers);
        }

        function LoadMap(markers) {
            var last_location = markers.length - 1;
            var mapOptions = {
                center: new google.maps.LatLng(markers[last_location].lat, markers[last_location].lng),
                // center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 6,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
                mapTypeControl: false,
                scrollwheel: true,
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
            //Create and open InfoWindow.
            var infoWindow = new google.maps.InfoWindow();
            for (var i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title

                });
                //Attach click event to the marker.
                (function (marker, data) {
                    google.maps.event.addListener(marker, "mouseover", function (e) {
                        infoWindow.setContent("<div style = 'width:200px;min-height:40px'>" + data.description + "</div>");
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }

        $(document).ready(function () {
            // PAGE IS FULLY LOADED
            // FADE OUT YOUR OVERLAYING DIV
            $('#overlay').addClass('d-none');
            $('#allshowcontainer').removeClass('d-none');
        });
        // map configration ends
        // submit all data
        // function thankyou() {
        //     var carTabsCount = $("#car-tabs-warap .tab-pane").length;
        //     var cars_data = [];
        //     for (var start = 1; start <= carTabsCount; start++) {
        //         var daysarry = []
        //         $("input:checkbox[name=days_" + start + "]:checked").each(function () {
        //             daysarry.push($(this).val());
        //         });
        //         let data = {
        //             'car_location': $("#car_location_" + start).val(),
        //             'no_of_cars': $("#no_of_cars_" + start).val(),
        //             'car_make': $("#car_make_" + start).val(),
        //             'car_type': $("#car_type_" + start).val(),
        //             'car_model': $("#car_model_" + start).val(),
        //             'car_year': $("#car_year_" + start).val(),
        //             'mile_age': $("#mileage_" + start).val(),
        //             'days': daysarry,
        //         }
        //
        //         cars_data.push(data);
        //     }
        //     console.log('car  dataa', cars_data);
        //     var totaltabs = $("#addinglocationvalue").val();
        //     var totaltabs = parseInt(totaltabs);
        //     totaltabs = totaltabs - 1;
        //     var campaign_locations = [];
        //
        //     for (var start = 1; start <= totaltabs; start++) {
        //         let data = {
        //             'location_number': start,
        //             'state_id': $("#states_" + start).val(),
        //             'city_id': $("#city_" + start).val(),
        //             'zips': $("#zips_" + start).val()
        //         }
        //         campaign_locations.push(data);
        //     }
        //     console.log('location dta', campaign_locations);
        //     return;
        //     // console.log('after loop ', campaign_locations);
        //     // getting all data
        //     var cam_title = $("#Campaign_tittle").val();
        //     console.log('title: ', cam_title);
        //     var campaign_type = $("#campaign_type").val();
        //     console.log('campaign_type: ', campaign_type);
        //     var campaign_duration = $("#campaign_duration").val();
        //     console.log('campaign_duration: ', campaign_duration);
        //     var interval = $("#kt_daterangepicker_1").val();
        //     console.log('interval: ', interval);
        //     var country_name = $("#country_name_1").val();
        //     console.log('country_name: ', country_name);
        //     var state = $("#state").val();
        //     console.log('state: ', state);
        //     var city = $("#city").val();
        //     console.log('city: ', city);
        //
        //     var carsdata = cars_data;
        //     console.log('car details :', carsdata);
        //     var company_details = $("#company_details").val();
        //     console.log('company_details: ', company_details);
        //     var slogan = $("#slogan").val();
        //     console.log('slogan: ', slogan);
        //     var contact = $("#contact").val();
        //     console.log('contact: ', contact);
        //     var sticker_details = $("#sticker_details").val();
        //     var car_location = $("#location").val();
        //     var csrf = $("input[name=_token]").val();
        //     // getting fist car details
        //     var locationa = $("#location").val();
        //
        //     var numberOfCarsa = $("#numberOfCars").val();
        //     var carmakeida = $("#carmakeid").val();
        //     var cartypesa = $("#cartypes").val();
        //     var carmodalsa = $("#carmodals").val();
        //     var slectyearsa = $("#slectyears").val();
        //     var milageid = $("#milageid").val();
        //
        //
        //     // var img  = $('#ImageBrowse').val();
        //     // var newcardetails = {'locationa': locationa,'numberOfCarsa': numberOfCarsa,'carmakeida': carmakeida,'cartypesa': cartypesa,'carmodalsa': carmodalsa, 'slectyearsa': slectyearsa, 'milageid': milageid, 'newcardetails': newcardetails,};
        //     // carlist.push(newcardetails);
        //
        //     // $('#checkoutpreview').modal('hide');
        //     // $('#thankyou').modal('show');
        //
        //     console.log('sticker_details: ', sticker_details);
        //     // console.log('csrf: ', csrf);
        //     var data = {
        //         'cam_title': cam_title,
        //         'campaign_type': campaign_type,
        //         'campaign_duration': campaign_duration,
        //         'interval': interval,
        //         'country_name': country_name,
        //         'state': state,
        //         'city': city,
        //         'campaign_locations': campaign_locations,
        //         'car_location': car_location,
        //         'cars_data': cars_data,
        //         'sticker_slogan': slogan,
        //         'sticker_contact': contact,
        //         'sticker_details': sticker_details,
        //         '_token': csrf,
        //     };
        //     var response = createCampaign(data);
        //     console.log('rr');
        //
        //     if (response == '1') {
        //         window.location.replace(globalPublicPath + "/Advertiser/Dashboard/Manage-Campaign?id=q");
        //     }
        //
        // }

        // waseen functions
        /* function thankyou() {
             var Totalcars = 1;
             var TotalCost = 0;
             var html;
             $('#car-tabs-warap .tab-pane').each(function (index) {
                 var carCountew
                 var milages;
                 var mailagerate;
                 var carMake;
                 $(this).find('.form-control').each(function (index) {
                     if ($(this).hasClass('numberofcars')) {
                         carCountew = parseInt($(this).val());
                         console.log("numberofcars", carCountew);
                     }
                     if ($(this).hasClass('kt_slider_a')) {
                         milages = parseInt($(this).val());
                         console.log("milages", milages)
                     }

                     if ($(this).hasClass('mailage-rate')) {
                         mailagerate = parseInt($(this).val());
                         console.log("mailagerate", mailagerate)
                     }

                     if ($(this).hasClass('carMake')) {
                         carMake = $(this).find('option:selected').text();
                         console.log("carMake", carMake)
                     }

                 });
                 var GrandCost = carCountew * milages * mailagerate;

                 var cars_data = [];
                     for (var start = 1; start <= carTabsCount; start++) {
                         var daysarry = []
                         $("input:checkbox[name=days_" + start + "]:checked").each(function () {
                             daysarry.push($(this).val());
                         });
                         let data = {
                             'car_location': $("#car_location_" + start).val(),
                             'no_of_cars': carCountew,
                             'car_make': carMake,
                             'car_type': $("#car_type_" + start).val(),
                             'car_model': $("#car_model_" + start).val(),
                             'car_year': $("#car_year_" + start).val(),
                             'mile_age': milages,
                             'days': daysarry,
                         }

                         cars_data.push(data);
                     }

                 html = `<tr>
                                     <th>${Totalcars}</th>
                                     <th>${carMake}</th>
                                     <th>${carCountew}</th>
                                     <th>${milages}</th>
                                     <th>${mailagerate}</th>
                                     <th>${GrandCost}</th>
                                 </tr>`;
                 // $(".carCostappend").append(html);
                 console.log(html);

                 TotalCost += GrandCost;
                 Totalcars++;

             });

             // console.log(html);
         }*/


        function carcost() {
            $(".carCostappend").html('');

            var Totalcars = 1;
            var TotalCost = 0;
            $('#car-tabs-warap .tab-pane').each(function (index) {
                var carCountew
                var milages;
                var mailagerate;
                var carMake;
                var getlocations;
                var carname;
                $(this).find('.form-control').each(function (index) {
                    if ($(this).hasClass('numberofcars')) {
                        carCountew = parseInt($(this).val());
                        console.log("numberofcars", carCountew);
                    }
                    if ($(this).hasClass('kt_slider_a')) {
                        milages = parseInt($(this).val());
                        console.log("milages", milages)
                    }

                    if ($(this).hasClass('mailage-rate')) {
                        mailagerate = $(this).val();
                        console.log("mailagerate", mailagerate)
                    }

                    if ($(this).hasClass('carMake')) {
                        carMake = $(this).find('option:selected').text();
                        // console.log("carMake", carMake)
                    }
                    if ($(this).hasClass('getlocations')) {
                        getlocations = $(this).find('option:selected').text();

                        // console.log("getlocations aaaaaaaaaaaaa", getlocations)

                    }
                    if ($(this).hasClass('car_models')) {
                        carname = $(this).find('option:selected').text();



                    }

                });
                var intervals = $('#kt_daterangepicker_1').val().split(' - ');
                var start = moment(intervals[0], "MM-DD-YYYY");
                var end = moment(intervals[1], "MM-DD-YYYY");

                var no_0f_days = moment.duration(end.diff(start)).asDays();
                no_0f_days += 1;
                no_0f_days = no_0f_days.toString();
                var GrandCost = carCountew * mailagerate * no_0f_days;
                var html = `<tr>
                                    <th>${Totalcars}</th>
                                      <th>${getlocations}</th>
                                      <th>${carname}</th>
                                    <th>${carMake}</th>
                                    <th>${carCountew}</th>
                                    <th>${milages}</th>
                                    <th>${mailagerate}</th>
                                     <th>${no_0f_days}</th>
                                    <th>${GrandCost.toFixed(2)}</th>
                                </tr>`;


                $(".carCostappend").append(html);
                TotalCost += GrandCost;
                Totalcars++;

            });


            $(".carTotalCost").text(TotalCost.toFixed(2));

        }

        function createCampaign(data) {
            var object = {
                'url': '{{route('user.create.campaign.data')}}',
                'type': 'POST',
                'enctype': 'multipart/form-data',
                'processData': false,
                'contentType': false,
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'cache': false,
                'data': data,

            };
            var result = centralAjax(object);
            return result;
            // console.log('result', result);

            // $('#statesDropdown').html('');
            // $.each(result, function (index, value) {
            //     var sub_id = value['id'] ;
            //     var sub_name = value['name'] ;
            //
            //     var newOption = new Option(sub_name, sub_id, true, true);
            //     $('#statesDropdown').append(newOption).trigger('change');
            //
            // });
        }


        function getCitiess(id) {

            $("#detectlocationdelet").val(2);
            $("#firstaddlocaitonscheck").val(1);
            console.log(id);
            var state_id = $('#states_' + id).val();
            $('#zips_' + id).find('option').remove();
            // $('#states_' + id).find("option").remove();
            console.log(state_id);

            var data = {
                'state_id': state_id,
            };
            var object = {
                'url': globalPublicPath + '/get-cities',
                'processData': false,
                'contentType': false,
                'cache': false,
                'data': data,
            };
            var result2 = centralAjax(object);
            $('#city_' + id).html('');
            var newOption = new Option('Select City', '', false, false);
            $('#city_' + id).append(newOption);
            $.each(result2, function (index, value) {
                var city_id = value['id'];
                var name = value['primary_city'];
                var newOption = new Option(name, city_id, false, false);
                $('#city_' + id).append(newOption);
            });
            if (result2.length == 0) {
                var newOption = new Option('No city found', '', false, false);
                $('#city_' + id).append(newOption);
            }

        }


        function getZips(id) {
            $("#detectlocationdelet").val(2);
            $("#firstaddlocaitonscheck").val(1);
            // var state_id = $('#states_' + id + ' option:selected').val();
            $('#zips_' + id).find('option').remove();
            var city_id = $("#city_" + id).val();
            // alert(id);
            var data = {
                'city_id': city_id,

            };
            var object = {
                'url': globalPublicPath + '/get-zips',
                'processData': false,
                'contentType': false,
                'cache': false,
                'data': data,
            };
            var result2 = centralAjax(object);
            console.log('ajax zipsss', result2);
            // $('#zips_'+id).html('');
            $.each(result2, function (index, value) {
                var zip_code = value['zip'];
                var newOption = new Option(zip_code, zip_code, false, false);
                $('#zips_' + id).prepend(newOption);
            });
            if (result2.length == 0) {
                var newOption = new Option('No zip found', '', false, false);
                $('#zips_' + id).prepend(newOption);
            }
        }


        function getZipss(id) {
            var location_id = $('#car_location_' + id).val();
            var zips = $('#zips_' + location_id).val();
            // console.log('zip_id',zips);
            // console.log('location_id',location_id);
            $('#selected_zips_' + id).html('');
            $.each(zips, function (zip, value) {
                var newOption = new Option(value, value, true, true);
                $('#selected_zips_' + id).prepend(newOption);
            });
            var state = $('#states_' + location_id).val();

            $('#getstateaa_' + id).attr('data-name', $('#states_' + location_id + ' option:selected').text());
            $('#getstateaa_' + id).val(state);
            // console.log(state);


        }

        // $(document).on('change', '.getlocations', function (e) {
        //     alert();
        //   var value =  $(this).val();
        //  console.log(value)
        // $this.parents().find('.select2').
        // });
        function getlocations(tab_no) {
            tab_no = tab_no - 1;
            var totaltabs = $("#addinglocationvalue").val();
            var totaltabs = parseInt(totaltabs);
            var j = 1;
            var prev_selected = $('#car_location_' + tab_no).val();
            $('#car_location_' + tab_no).html('');
            for (var i = 1; i < totaltabs; i++) {
                var newOption = new Option('Location ' + j, j, false, false);
                $('#car_location_' + tab_no).append(newOption);
                ++j;
            }
            if (prev_selected != null) {
                $('#car_location_' + tab_no).val(prev_selected)
            }
            // console.log(' tab no_',tab_no);
            getZipss(tab_no);
        }


        $(document).on('click', '#addmorecars', function (e) {
            var check = 1;
            $('#car-tabs-warap .form-control').each(function (index) {
                console.log($(this).val())
                if ($(this).val() == "" || $(this).val() == null) {
                    if ($(this).hasClass('carinputs')) {

                    } else {
                        $(this).next().removeClass("d-none");
                        console.log($(this).attr('name'));
                        check = 2;
                        toastr.error('', 'Empty field');
                        $('#carstabs li:last-child a').tab('show');
                    }

                } else {
                    $(this).next().addClass('d-none');
                }

            });

            if (check == 1) {
                var carListCount = parseInt($("#carListHandler").val());
                var carsListAdd = " <li class=\"nav-item active position-relative \"  id=\"tabdeletid" + carListCount + "\" role=\"presentation\">\n" +
                    "<a class=\"nav-link\" id=\"car-tab" + carListCount + "\" data-toggle=\"tab\" href=\"#carbtn" + carListCount + "\" role=\"tab\" aria-controls=\"carbtn" + carListCount + "\" aria-selected=\"false\">Car 00" + carListCount + "<i class=\"fas fa-times deletcars-icon\"  tittle=\"Delete\" onclick=\"deletcartabsss(" + carListCount + ")\"></i></a>\n" +
                    "</li>";

                var carListWarp = `<div class="tab-pane fade " id="carbtn${carListCount}" role="tabpanel" aria-labelledby="car-tab${carListCount}">
                                            <div class="row border-bottom">
                                                <div class="col-lg-12">
                                                    <div class="clearfix">
                                                        <span class="mb-1 float-left  pb-2"><b>Car 00${carListCount} Details</b></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">

                                                        <label class="d-block">Choose Location:</label>
                                                        <select class="form-control w-100  getlocations" notify="Country" name="location[]" id="car_location_1">
                                                        <option value="nills" selected="">Choose Location</option><option value="0" selected="">Location 1</option></select>

                                                        <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                        <input type="hidden" class="hiddenstatefinder" id="getstateaa_1">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="d-block">Zips:</label>
                                                         <select
                                                            class="select2 form-control  w-100  selected_zips"
                                                            multiple style="width:200px !important;" name="carZip[]"
                                                            notify="Country" >
                                                            <option value="" selected>
                                                                No Zip codes
                                                            </option>
                                                        </select>
<span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Number of Cars :</label>
                                                        <input type="number" min="1" value="" class="form-control getvals  numberofcars ui-autocomplete-input" name="carQuntity[]" id="no_of_cars" placeholder="Cars" autocomplete="off">
                                                        <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label>Car Maker:</label>
                                                        <select name="carMake[]" id="car_make_1" class="form-control carMake" onnotify="Country">
                                                         <option value="" value='' selected disabled> Choose maker</option>
                                                         @foreach($Cars as $Car)
                <option value="{{$Car->company_id}}">{{$Car->maker->company}}</option>
                                                          @endforeach
                </select>
             <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
         </div>
     </div>
     <div class="col-lg-6">
         <div class="form-check pl-0">
             <label>Car Model:</label>
<select name="car_model[]" id="car_model_1" class="form-control   car_models" notify="Country">
<option value="" selected="" disabled="">Select Model</option>
</select>
<span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
         </div>
     </div>

     <div class="col-lg-6">
         <div class="form-check pl-0">
             <label>Select year:</label>
<select name="car_year[]" class="form-control garcartype" id="car_year_1">
<option value="" selected="" disabled="">Making Year</option>
</select>
<span class="form-text text-danger d-none">This field is required.</span>
         </div>
     </div>

     <div class="col-lg-6">
         <div class="form-group ">
             <label>Car Type:</label>
<select name="car_type[]" id="car_type_1" class="form-control   cartyped" notify="Country">
<option value="" selected="" disabled="">Select Type</option>
</select>
<span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
         </div>
     </div>

     <div class="col-lg-12 pt-5 mb-0">
         <label>Mileage Drive:</label>
     </div>
     <div class="col-lg-12">
         <div class="form-group">
              <div class="ion-range-slider">
                 <input type="hidden" name="mileage[]" data-id='1'
                        data-validation='0' id="mileage_1"
                        class="kt_slider_a getvals form-control"/>
                 <span> </span>
                 <span class="form-text text-success milage-notification mt-2">Select Mileage to get your cost.</span>
                 <input type="hidden" name="price_per_mile[]" class="mailage-rate getvals form-control"
                        value="">
                 <span class="form-text text-danger d-none">This field is required.</span>
             </div>
         </div>
     </div>
     <div class="col-lg-12">
         <label for="">How Often Drive A Car:</label>
     </div>
     <div class="col-lg-12">
          <div class="btn-group mr-2" role="group"
              style="display: contents !important;" aria-label="First group">
             <button type="button" class="btn  slectdays ">
                 <input type="checkbox" name="days_1" value="Monday"
                        class="d-none form-control carinputs">Mon
             </button>
             <button type="button" class="btn  slectdays  ">
                 <input type="checkbox" name="days_1" value="Tuesday"
                        class="d-none form-control carinputs">Tue
             </button>
             <button type="button" class="btn  slectdays ">
                 <input type="checkbox" name="days_1" value="Wednesday"
                        class="d-none form-control carinputs">Wed
             </button>
             <button type="button" class="btn  slectdays ">
                 <input type="checkbox" name="days_1" value="Thursday"
                        class="d-none form-control carinputs">Thu
             </button>
             <button type="button" class="btn  slectdays ">
                 <input type="checkbox" name="days_1" value="Friday"
                        class="d-none form-control carinputs">Fri
             </button>
             <button type="button" class="btn  slectdays ">
                 <input type="checkbox" name="days_1"
                        value="Saturday"
                        class="d-none form-control carinputs">Sat
             </button>
             <button type="button" class="btn  slectdays ">
                 <input type="checkbox" name="days_1" value="Sunday"
                        class="d-none form-control carinputs">Sun
             </button>
         </div>
     </div>
 </div>
  <div class="row">
                                                <input type="hidden" class="statedetails form-control" name="stateid[]">
                                                <input type="hidden" class="citydeils form-control" name="cityid[]">
                                            </div>
</div>`;


                $("#carstabs").append(carsListAdd);//appending car btn
                // $("#car-tab").append(carsListAdd);//appending car btn

                $("#car-tabs-warap").append(carListWarp); //appending car container
                var totatCares = $("#carstabs .nav-item").length; //geting carent car count

                $(".totalcarprevew").text(totatCares); //showing user his total cars
                updatelocations2();

                var carliscontadd = carListCount + 1;
                $("#carListHandler").val(carliscontadd);
            }

            runslect2();
            runmilage();

            window.scrollTo(0, 0);
        });


        function runmilage() {
            $('.kt_slider_a').ionRangeSlider({
                // type: "double",
                grid: true,
                // min: 0,
                max: 2000,
                min: 0,
                // from: 0,
                // to: 0,
                prefix: "Miles",
            });
        }

        $(document).on('change', '#termss', function (e) {
            if ($('#termss').is(":checked")) {
                $('.terms_error').text('');
                $('.btn-terms').removeClass('btn-danger').removeAttr('disabled ').addClass('btn-primary').css('cursor', 'pointer');

            } else {
                $('.terms_error').text('Please agree our terms and conditions');
                $('.btn-terms').removeClass('btn-primary').attr('disabled', 'disabled').addClass('btn-danger').css('cursor', ' auto');
            }
        });

        $(document).on('change', '.kt_slider_a', function (e) {
            $(this).parent().find('.milage-notification').addClass('text-danger');
            var rangevalue = $(this).val();
            var numberCont = $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.numberofcars ').val();
            var locationindex = parseInt($("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.getlocations ').val()) + 1;
            // console.log("locationindex", locationindex);
            if (locationindex >= 0) {
                if (numberCont == "") {
                    toastr.error('Car Count is empty.');
                } else {
                    var statevalue = $("#tabContenttabLocationInfo .tab-pane:nth-child(" + locationindex + ")").find(".statedisable ").val();
                    // console.log("numberCont", numberCont);
                    // console.log("statevalue", statevalue);
                    // alert(name);
                    var data = {
                        'state_id': statevalue,
                        'range': rangevalue,
                    };
                    var object = {
                        'url': "{{route('user.miles.get.price')}}",
                        'type': 'GET',
                        'data': data
                    };
                    var result = centralAjax(object);
                    // console.log('resultt', result);
                    var car_counts = $('#carListHandler').val();
                    car_counts = car_counts - 1
                    if (result.length == '0') {
                        toastr.error('Mileage price is not available against ' + numberCont + ' cars');
                        $(this).parent().find('.milage-notification').text('Mileage price not available. Please choose other mileage.');
                        $(this).parent().find('.milage-notification').addClass('text-success');
                        // $(this).parent().find('.milage-notification').addClass('Mileage price not available against car number: ' + numberCont, 'Choose other Mileage please');
                        $(this).parent().find('.mailage-rate').val('');
                    } else {
                        var price_per_mile = result.price;
                        var totalCost = price_per_mile * parseInt(numberCont) * parseInt(rangevalue);
                        toastr.success('Mileage price is available against ' + numberCont + ' cars.');
                        $(this).parent().find('.milage-notification').text('Price at this point is: ' + price_per_mile);
                        $(this).parent().find('.milage-notification').removeClass('text-danger');
                        $(this).parent().find('.milage-notification').removeClass('d-none');
                        $(this).parent().find('.mailage-rate').val(0);
                        price_per_mile = price_per_mile;
                        $(this).parent().find('.mailage-rate').val(price_per_mile);

                        // // console.log('totl',rangevalue*result);
                    }
                }

            } else {
                toastr.error('Choose Location.');
            }
            // .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {color:#da4675}
            // .irs--flat .irs-bar {color:#da4675}

            // console.log('counter of car',$(this).attr('data-id'));
            // // console.log('location choosed :',value);


        });


        // change car btn color onclick
        $(document).on('click', '#carstabs li', function (e) {
            // alert("sdf")
            // $(this).tab('show')
            $("#carstabs li").find('a').removeClass('active');
            $(this).find('a').addClass('active')
        })

        function deletcartabs(deletid) {
            console.log(deletid)
            $("#detectlocationdelet").val(2);
            $("#firstaddlocaitonscheck").val(1);
            $("#location-tab-btn" + deletid).remove()
            $("#location" + deletid).remove();
            toastr.error('Location has been deleted.');
            // $("#deletCarNotification").removeClass('d-none');
            // $("#deletCarNotification").find('span').text("00" + deletid);
            $('#tabLocationInfo li:nth-child(1) a').tab('show')
            var totatCares = $("#tabLocationInfo .nav-item").length; //geting carent car count
            console.log(totatCares);
            $(".totallocaitionprevew").text(totatCares); //showing user his total cars
        }

        function deletcartabsss(deletid) {
            console.log(deletid)
            $("#tabdeletid" + deletid).remove()
            $("#carbtn" + deletid).remove();
            toastr.error('Car has been deleted.');

            var totatCares = $("#carstabs .nav-item").length; //geting carent car count
            console.log(totatCares);
            $(".totalcarprevew").text(totatCares);
            $('#carstabs li:last-child a').tab('show');
            // var carListCount = parseInt($("#carListHandler").val());
            // var rem = --carListCount;
            // $("#totalcarprevew").text(rem);
            runslect2();
        }

        // update locations while adding addmore cars
        function updatelocations2() {
            $("#car-tabs-warap").find(".tab-pane:last-child").find('.getlocations').find('option').remove()//making empty location input
            var newOption = new Option('Choose Location', "nills", true, true);
            $("#car-tabs-warap").find(".tab-pane:last-child").find('.getlocations').append(newOption);  //adding empty record
            console.log($("#tabContenttabLocationInfo .tab-pane").length);
            var locaitonlist = []
            $("#car-tabs-warap .tab-pane").each(function (index) {
                var locationName = $(this).find('.getlocations').val();
                locaitonlist.push(locationName)
            });
            console.log(locaitonlist);
            //gettign value form locaitons
            $("#tabContenttabLocationInfo .tab-pane").each(function (index) {
                var locationName = $(this).find('.statedisable').find(":selected").text();
                var cityesname = $(this).find('.cityecolect').find(":selected").text();
                console.log("location Namess", locationName);
                var locationWithState = locationName + " / " + cityesname;
                var newOption = new Option(locationWithState, index, true, false);
                $("#car-tabs-warap").find(".tab-pane:last-child").find('.getlocations').append(newOption);
            });
        }


        function checkzipscount() {
            var totallocations = $('#tabContenttabLocationInfo .tab-pane').length;
            var totalzips = 0;
            var zipsarry = [];
            $('#tabContenttabLocationInfo .tab-pane').each(function () {
                var zipsarrr = $(this).find('.findzips').val();
                for (i = 0; i < zipsarrr.length; i++) {

                }
                var zip = zipsarrr.length;
                totalzips += zip;
            });
            return zipsarry
            // console.log(zipsarry);
            // $('#totlzips').val(zipsarry);
            // console.log("total zips",totalzips);
            // console.log("total locations",totallocations);
        }

        $('#kt_daterangepicker_1').val('');
        $('.timeintervalcom').html('');

        // muhamad ali js

        $(document).on('change', '.carMake', function (e) {
            $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.car_models').find('option').remove() //making empty location input
            var company_id = $(this).val();
            let url = "{{route('user.get.cars.for.compaigns',':id')}}"
            url = url.replace(':id', company_id)
            $.get(url, function (response) {
                // console.log(response);
                $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.car_models').html(response);//making empty location input

            });
        })

        $(document).on('change', '.car_models', function (e) {
            $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.garcartype').find('option').remove() //making empty location input
            var car_id = $(this).val();
            let url = "{{route('user.get.year.for.compaigns',':id')}}"
            url = url.replace(':id', car_id)
            $.get(url, function (response) {
                $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.garcartype').html(response) //making empty location input
            });
        })

        $(document).on('change', '.garcartype', function (e) {
            $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.cartyped').find('option').remove() //making empty location input
            var car_id = $(this).val();

            let url = "{{route('user.get.type.for.compaigns',':id')}}"
            url = url.replace(':id', car_id)
            $.get(url, function (response) {
                // console.log(response);
                $("#car-tabs-warap").find(".tab-pane.fade.show.active").find('.cartyped').html(response); //making empty location input
            });
        });


        // let data_image = null
        // let reader;
        // $('#uploadstickerimge').change(function (event) {
        //   data_image = event.target.files[0];
        //     reader = new FileReader();
        //     reader.onloadend = function() {
        //         data_image = reader.result;
        //     }
        //
        //     // reader.readAsDataURL(data_image);
        //
        // });


        function thankyou() {
            $("#sbmitt").addClass('btn-danger');
            $('.loadr').removeClass('d-none');
            var campaign_title = $("#Campaign_tittle").val();
            var campaign_type = $("#campaign_type").val();
            var campaign_duration = $("#campaign_duration").val();
            var daterange = $("#kt_daterangepicker_1").val();

            // sticker details
            var company_details = $("#company_details").val();
            var slogan = $("#slogan").val();
            var contact = $("#contact").val();
            var sticker_details = $("#sticker_details").val();
            var sticker_imge = $("#uploadstickerimge").val();

            // var whole_data = ['slogan',slogan,'contact',contact,'sticker_details'
            //     ,sticker_details,'sticker_imge',sticker_imge,'Campaign_tittle'
            //     ,Campaign_tittle,'campaign_type',campaign_type,'campaign_duration'
            //     ,campaign_duration,'daterange',daterange,'company_details',company_details];

            // console.log(whole_data);
            // console.log(cararrrydetails);
            // console.log('locationdetails:',locationdetails);
            // console.log('testLocation:',testLocation);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            let campaign_detail = {
                'campaign_title': campaign_title,
                'campaign_type': campaign_type,
                'campaign_duration': campaign_duration,
                'daterange': daterange
            }
            let sticker_detail = {
                'company_details': company_details,
                'slogan': slogan,
                'contact': contact,
                'sticker_details': sticker_details,
            }


            let data = {
                campaign_detail: campaign_detail,
                locations: locationdbs,
                cars: testAry,
                sticker_detail: sticker_detail,
                // image: reader,
            };

            console.log('fff', data);
            let alldate = JSON.stringify(data)
            console.log(alldate)
            // return;

            // let formData = new FormData();
            // formData.append('cars',testAry);
            // for ( var key in testAry ) {
            //     formData.append('cars', testAry[key]);
            // }
            // formData.append('location',testLocation);
            // formData.append('campaign_detail',campaign_detail);
            // formData.append('sticker_detail',sticker_detail);
            // formData.append('image',reader);
            $.post("{{route('user.create.campaign.data')}}",data , function (response) {
                // console.log('resp',response);
                // return
                   if(response != '0') {
                           let url = "{{route('user.campaigns',':id')}}"
             url = url.replace(':id', 'id=q')
            $("#cmp-id").val(response);
            $("#sbmitt").attr('disabled');
            $("#imageUploadForm").submit();
           $('.loadr').addClass('d-none');
           // window.location.replace(url);
        }
});

        }

        $('.totaldays').text('');
        // $('.numberofcars').val(1);
        // $('#no_of_cars_1').text('1');
    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&callback=myMap"></script>

    {{--    $("#updatetimge").on('submit', function(e){--}}
    {{--    e.preventDefault();--}}
    {{--    $.ajax({--}}
    {{--    type: 'POST',--}}
    {{--    url: '../submitfolder/students/update_student_imge.php',--}}
    {{--    data:,--}}
    {{--    contentType: false,--}}
    {{--    cache: false,--}}
    {{--    processData:false,--}}




@endsection
