{{-- Extends layout --}}
@extends('layout.ent_driver')

{{-- Content --}}
@section('content')

    {{-- Dashboard 1 --}}

    <div class="row">
        <div class="col-lg-6 col-xxl-4">
            @include('pages.widgets._widget-1', ['class' => 'card-stretch gutter-b'])
        </div>
        {{--        <div class="col-md-4 col-xs-12 col-sm-4">--}}
        {{--                                <div class="general-information-form_section-one">--}}
        {{--                                    <label>Parwest ID <span class="steric-color">*</span></label>--}}
        {{--                                    <input type="text" name="parwest_id" id="parwest_id"--}}
        {{--                                           onchange="getGuardsDeploymentDataForUnpaid(this.value) "--}}
        {{--                                           placeholder="Parwest ID">--}}
        {{--                                </div>--}}
        {{--                            </div>--}}

        <div class="col-lg-6 col-xxl-4">
            @include('pages.widgets._widget-2', ['class' => 'card-stretch gutter-b'])
        </div>

        <div class="col-lg-6 col-xxl-4">
            @include('pages.widgets._widget-3', ['class' => 'card-stretch card-stretch-half gutter-b'])
            @include('pages.widgets._widget-4', ['class' => 'card-stretch card-stretch-half gutter-b'])
        </div>

        <div class="col-lg-6 col-xxl-4 order-1 order-xxl-1">
            @include('pages.widgets._widget-5', ['class' => 'card-stretch gutter-b'])
        </div>

        <div class="col-xxl-8 order-2 order-xxl-1">
            @include('pages.widgets._widget-6', ['class' => 'card-stretch gutter-b'])
        </div>

        {{--        <div class="col-lg-6 col-xxl-4 order-1 order-xxl-2">--}}
        {{--            @include('pages.widgets._widget-7', ['class' => 'card-stretch gutter-b'])--}}
        {{--        </div>--}}

        {{--        <div class="col-lg-6 col-xxl-4 order-1 order-xxl-2">--}}
        {{--            @include('pages.widgets._widget-8', ['class' => 'card-stretch gutter-b'])--}}
        {{--        </div>--}}

        {{--        <div class="col-lg-12 col-xxl-4 order-1 order-xxl-2">--}}
        {{--            @include('pages.widgets._widget-9', ['class' => 'card-stretch gutter-b'])--}}
        {{--        </div>--}}
    </div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>

    <script>
        getGuardsDeploymentDataForUnpaid = function (parwest) {
            var globalPublicPath = '{{ URL('/') }}';
            console.log('herfe');
            // var parwest  = $('#parwest_id').val();
            if(parwest){
                // $("#month_deployment").css("border-color", "gray");
                var data = {
                    'parwest_id': parwest,
                    // 'month': month,
                };
                // let globalPublicPath;
                var object = {
                    'url': globalPublicPath + '/ajaxGetCountries',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                // if (result.data.inventory > 0) {
                //     warningNotificationCustom('Selected Guard   Inventory Must be revoked before clearance   ');
                // }
                // if (result.data.status_name == 'present' ) {
                //     warningNotificationCustom('Selected Guard must be revoked before clearance ');
                // }
                // if (result.data.status_name == 'absent' ||result.data.status_name == 'default' ||result.data.status_name == 'present') {
                //     warningNotificationCustom('Selected Guard Is  '+ result.data.status_name);
                // }else{
                if (result.responseCode == 1) {

                    $('#unpaid_guard_name').val(result.data.name);
                    $('#unpaid_status').val(result.data.status_name);
                    $('#unpaid_type').val(result.data.designation_name);
                    $('#unpaid_current_location').val(result.data.location);
                    $('#unpaid_loan').val(result.data.loan);

                    successNotificationCustom(result.message);
                }if(result.responseCode == 2){
                    warningNotificationCustom(result.message);
                }

                // }


            }else{
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        };
        getGuardsStatusDataForUnpaid = function (month) {
            var parwest  = $('#parwest_id').val();
            if(month){
                console.log('month',month);
                // $("#month_deployment").css("border-color", "gray");
                var data = {
                    'month': month,
                    'parwest_id': parwest,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetailsUnpaidSalaryExport',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                // if (result.data.inventory > 0) {
                //     warningNotificationCustom('Selected Guard   Inventory Must be revoked before clearance   ');
                // }
                // if (result.data.status_name == 'present' ) {
                //     warningNotificationCustom('Selected Guard must be revoked before clearance ');
                // }
                // if (result.data.status_name == 'absent' ||result.data.status_name == 'default' ||result.data.status_name == 'present') {
                //     warningNotificationCustom('Selected Guard Is  '+ result.data.status_name);
                // }else{
                if (result.responseCode == 1) {

                    // $('#unpaid_guard_name').val(result.data.name);
                    $('#salary_status').val(result.data.status);
                    if(result.data.status == 'Paid'){

                        $('#export_unpaid').attr('disabled', 'disabled');

                    }
                    // $('#unpaid_type').val(result.data.designation_name);
                    // $('#unpaid_current_location').val(result.data.location);
                    // $('#unpaid_loan').val(result.data.loan);

                    // successNotificationCustom(result.message);
                }
                // if(result.responseCode == 2){
                //     warningNotificationCustom(result.message);
                // }

                // }


            }else{
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        };
        getSupervisorsAgainstManagerOnExport = function (manager_id) {

            if (manager_id != 0) {

                var data = {
                    'manager_id': manager_id,
                };
                var object = {
                    'url': globalPublicPath + '/client/getSupervisorsAgainstManager',
                    'type': 'POST',
                    'data': data
                };



                var result = centralAjax(object);

                var html = '<option value="0" selected>--Select Supervisor--</option>' ;
                if(result.responseCode == 1 )
                {
                    $.each(result.data,function (key,value) {
                        html += '<option value="'+value.id+'">'+value.name+'</option>';
                    });

                }
                // $('#supervisor').removeAttr('disabled');
                $('#supervisor').empty().append(html);
                $('#supervisor').selectpicker('refresh');

                if (result.responseCode == 1) {
                    console.log(result.data[0].id);


                    //append a select supervisor dropdown
                    // $('#supervisor').html('');
                    // // $('#edit_supervisor_number_container').val('No Phone Number Found');
                    // $('#supervisor').append($('<option>', {
                    //     value: 0,
                    //     text: '--Select Supervisor--',

                    // }));


                    //append all the supervisors against a manager in dropdown
                    // $.each(result.data, function (key, value) {
                    // $('#supervisor').append('<option id="' + value.id + '">' + value.name + '</option>');
                    // var newOption = new Option(value.name, value.id, false, false);
                    // $('#supervisor').append(newOption);
                    // $('#supervisor').selectpicker('val',  value.id );
                    // $('#supervisor').selectpicker('text',  value.id );
//                         console.log(value.id);
//
//                         $('#edit_selected_client').selectpicker('val', client_id);
//                         $('#supervisor').append($('<option >', {
//                             value: value.id,
//                             text: value.name,
//
//                         }));
//
//                         //to select the value of current supervisor
// //                     if (currenstSupervisor.id == value.id) {
// //                         $('#edit_branch_supervisor_id').val(currenstSupervisor.id).change();
// // //                        getSupervisorDetailOnEditPage();
// //                     }
//
//                     });
                }
                else {
                    // errorNotificationCustom(result.message);
                }

            }


            else {
                errorNotificationCustom('please select a manager before continue');
            }

        };
        getManagersAgainstRegionOnExport = function (region_id) {

            if (region_id != 0) {

                var data = {
                    'region_id': region_id,
                };
                var object = {
                    'url': globalPublicPath + '/client/getManagersAgainstRegion',
                    'type': 'POST',
                    'data': data
                };



                var result = centralAjax(object);

                var html = '<option value="0" selected>--Select Manager--</option>' ;
                if(result.responseCode == 1 )
                {
                    $.each(result.data,function (key,value) {
                        html += '<option value="'+value.id+'">'+value.name+'</option>';
                    });

                }
                // $('#supervisor').removeAttr('disabled');
                $('#managers').empty().append(html);
                $('#managers').selectpicker('refresh');

                if (result.responseCode == 1) {
                    console.log(result.data[0].id);


                    //append a select supervisor dropdown
                    // $('#supervisor').html('');
                    // // $('#edit_supervisor_number_container').val('No Phone Number Found');
                    // $('#supervisor').append($('<option>', {
                    //     value: 0,
                    //     text: '--Select Supervisor--',

                    // }));


                    //append all the supervisors against a manager in dropdown
                    // $.each(result.data, function (key, value) {
                    // $('#supervisor').append('<option id="' + value.id + '">' + value.name + '</option>');
                    // var newOption = new Option(value.name, value.id, false, false);
                    // $('#supervisor').append(newOption);
                    // $('#supervisor').selectpicker('val',  value.id );
                    // $('#supervisor').selectpicker('text',  value.id );
//                         console.log(value.id);
//
//                         $('#edit_selected_client').selectpicker('val', client_id);
//                         $('#supervisor').append($('<option >', {
//                             value: value.id,
//                             text: value.name,
//
//                         }));
//
//                         //to select the value of current supervisor
// //                     if (currenstSupervisor.id == value.id) {
// //                         $('#edit_branch_supervisor_id').val(currenstSupervisor.id).change();
// // //                        getSupervisorDetailOnEditPage();
// //                     }
//
//                     });
                }
                else {
                    // errorNotificationCustom(result.message);
                }

            }


            else {
                errorNotificationCustom('please select a manager before continue');
            }

        };

        $('#extrahour_guards_client').change(function () {
            // $this.val();
            var client_id = $('#extrahour_guards_client').val();
            console.log(client_id);
            var data = {
                'client_id': client_id,
            };
            var object = {
                'url': globalPublicPath + '/guard/getClientBranchesLoan',
                'type': 'GET',
                'data': data,
            };
            $('#extrahour_guards_branch').html('');
            $('#extrahour_guards_branch').append($('<option>', {
                value: 0,
                text: '--Select Branch--',
            }));
            var result = centralAjax(object);
            console.log(result);
            $.each(result.data.branches, function (keys, values) {
                console.log(values);
                // $.each(values, function (key, value) {

                if (values.is_active == 1) {
                    $('#extrahour_guards_branch').append($('<option >', {
                        value: values.id,
                        text: values.name,

                    }));

                }
                // });


                // if(value.id == branchId){
                //     $("#branch_id_on_user_profile").val(value.id);
                //     console.log("branched value selected through jquery ajax");
                // }


            });
        });
        // populate managers list before salary genrate
        $("#genrate_salary").click(function(e) {
            e.preventDefault();
            var region = $('#regions').val();
            // var month = $('#salary_month').val();
            var markup = "";
            markup += '<tr >' +
                '<td ></td>' +
                '<td ></td>' +

                '</tr>';
            $("#salaryfinalize").append(markup);
            $("#finalizelist").append(markup);
            if(region == "" ){
                warningNotificationCustom('please select region ');
            }
            if(region != "" ){
                successNotificationCustom(" calculating loans ......",1000);
                var data = {
                    'region': region,
                    // 'month': month,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getFinalisedStatus',
                    'type': 'POST',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                if(result.post_status == true){
                    errorNotificationCustom(" Region already posted for this month ");

                }
                if(result.post_status == false){
                    warningNotificationCustom("Please Finalised loans by the following Users   ",1000);
                    $.each(result.users, function (index, value) {
                        console.log(index + ": " + value);
                        var markup = "";
                        markup += '<tr id="history">' +
                            '<td >' + value.name + '</td>' +
                            '<td >' + 'Pending !!' + '</td>' +

                            '</tr>';
                        $("#salaryfinalize").append(markup);
                    });
                    $.each(result.finalUsers, function (index, value) {
                        console.log(index + ": " + value);
                        var markup = "";
                        markup += '<tr id="finalizelistitem">' +
                            '<td >' + value.name + '</td>' +
                            '<td >' + 'Finalised :' + '</td>' +

                            '</tr>';
                        $("#finalizelist").append(markup);
                    });
                }
                if(result.genrate_status == false){
                    $("#salary-form").submit();
                }

            }

            // if(month == "" ){
            //     warningNotificationCustom('please select month ');
            // }

            console.log('slary genrated button clicked ',"region "+region+"   " + month);

        });
        $('#clearance-status').change(function () {
            // $this.val();
            var sel = $('#clearance-status').val();
            console.log(sel);
            if( sel == 2){

                $('#clearance-image-div').css('display','none');
            }else{
                $('#clearance-image-div').css('display','block');
            }

        });

        getGuardsData = function (parwest_id) {
            var month = $("#extrahour_month").val();
            console.log('month ',month)
            if(month){
                var data = {
                    'parwest_id': parwest_id,
                    'month': month,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetails',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                if (result.responseCode == 1) {

                    // $('#guard_name').val(result.data.guard[0].name);
                    // $('#guards_phone').val(result.data.guard[0].contact_no);
                    // $('#guards_current_supervisor').val(result.data.branch_supervisor);
                    // $('#current_deployment').val(result.data.branch_name + ''+result.data.client_name);
                    // $('#deployment_days').val(result.data.days);
                    // console.log(result.data.guard);
                    // $('#guardLoanHistory').css('display', 'block');
                    // console.log('loan data ', result.data.loan[0]);
                    // $.each(result.data.loan, function (index, value) {
                    //     console.log(index + ": " + value);
                    //     var markup = "";
                    //     markup += '<tr id="history">' +
                    //         '<td >' + value.parwest_id + '</td>' +
                    //         '<td >' + value.guard_name + '</td>' +
                    //         '<td >' + value.current_deployment + '</td>' +
                    //         '<td >' + value.deployment_days + '</td>' +
                    //         '<td >' + value.guards_phone + '</td>' +
                    //         '<td >' + value.guards_current_supervisor + '</td>' +
                    //         '<td >' + value.amount_paid + '</td>' +
                    //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                    //         '<td >' + value.payment_location + '</td>' +
                    //         '<td >' + value.slip_number_loan + '</td>' +
                    //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                    //         '<td class="table-heading2 finalize">'+
                    //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                    //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                    //         '</a> </button> '+ ' </td>' +
                    //         '</tr>';
                    //     $("#deploymentDetailsTableBody").append(markup);
                    // });
                    //
                    // $('#guards_supervisor_loaned').html('');
                    // $('#guards_supervisor_loaned').append($('<option>', {
                    //     value: 0,
                    //     text: 'ffffff',
                    // }));
                    //
                    // $.each(result.data.supervisors.data, function (key, value) {
                    //
                    //     $('#guards_supervisor_loaned').append($('<option >', {
                    //         value: value.status,
                    //         value: value.id,
                    //         text: value.name,
                    //
                    //     }));
                    //
                    // });
                    successNotificationCustom(result.message);
                }
            }else {
                $("#extrahour_month").css("border-color", "#f56942");
                warningNotificationCustom('Guard Number Not Exists ');
            }

        };


        removeGuardDetails = function () {
            // console.log('hamza');
            //
            // $('#guard_name').val('');
            // $('#guards_supervisor_loaned').html('');
            // $('#guards_supervisor_loaned').empty().append($('<option>', {
            //     value: 0,
            //     text: '--Select Branch--',
            // }));
        };
        getGuardsDeploymentData = function (month) {
            var parwest  = $('#extrahour_parwest_id').val();
            if(parwest){
                console.log('i am here in ');
                $("#extrahour_month").css("border-color", "gray");
                var data = {
                    'parwest_id': parwest,
                    'month': month,
                }
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetailsForExtraHours',
                    'type': 'GET',
                    'data': data,
                }

                var result = centralAjax(object);
                console.log(result);
                if (result.responseCode == 1) {
                    //
                    $('#extrahour_guard_name').val(result.data.guard.name);
                    $('#extrahour_status').val(result.data.guard.status_name);
                    // $('#guards_phone').val(result.data.guard[0].contact_no);
                    // $('#guards_current_supervisor').val(result.data.branch_supervisor);
                    $('#extrahour_current_location').val(result.data.branch_name + ''+result.data.client_name);
                    $('#extrahour_type').val(result.data.guard_type.name);
                    // console.log(result.data.guard);
                    // $('#guardLoanHistory').css('display', 'block');
                    // console.log('loan data ', result.data.loan[0]);
                    // $.each(result.data.loan, function (index, value) {
                    //     console.log(index + ": " + value);
                    //     var markup = "";
                    //     markup += '<tr id="history">' +
                    //         '<td >' + value.parwest_id + '</td>' +
                    //         '<td >' + value.guard_name + '</td>' +
                    //         '<td >' + value.current_deployment + '</td>' +
                    //         '<td >' + value.deployment_days + '</td>' +
                    //         '<td >' + value.guards_phone + '</td>' +
                    //         '<td >' + value.guards_current_supervisor + '</td>' +
                    //         '<td >' + value.amount_paid + '</td>' +
                    //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                    //         '<td >' + value.payment_location + '</td>' +
                    //         '<td >' + value.slip_number_loan + '</td>' +
                    //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                    //         '<td class="table-heading2 finalize">'+
                    //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                    //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                    //         '</a> </button> '+ ' </td>' +
                    //         '</tr>';
                    //     $("#deploymentDetailsTableBody").append(markup);
                    // });
                    //
                    // $('#guards_supervisor_loaned').html('');
                    // $('#guards_supervisor_loaned').append($('<option>', {
                    //     value: 0,
                    //     text: 'ffffff',
                    // }));
                    //
                    // $.each(result.data.supervisors.data, function (key, value) {
                    //
                    //     $('#guards_supervisor_loaned').append($('<option >', {
                    //         value: value.status,
                    //         value: value.id,
                    //         text: value.name,
                    //
                    //     }));
                    //
                    // });
                    successNotificationCustom(result.message);
                }
                // var data = {
                //     'month': month,
                //     'parwest_id': parwest,
                // };
                // var object = {
                //     'url': globalPublicPath + '/guard/getGuardsDeploymentData',
                //     'type': 'GET',
                //     'data': data,
                // };
                //
                // var result = centralAjax(object);
                // console.log(result);
                // if (result.responseCode == 1) {
                //
                //     // $('#guard_name').val(result.data.guard[0].name);
                //     // $('#guards_phone').val(result.data.guard[0].contact_no);
                //     // $('#guards_current_supervisor').val(result.data.guard[0].guard_supervisor_name);
                //     $('#current_deployment').val('meezan bank');
                //     $('#deployment_days').val('5');
                //     console.log(result);
                //     // $('#guardLoanHistory').css('display', 'block');
                //     // console.log('loan data ', result.data.loan[0]);
                //     // $.each(result.data.loan, function (index, value) {
                //     //     console.log(index + ": " + value);
                //     //     var markup = "";
                //     //     markup += '<tr id="history">' +
                //     //         '<td >' + value.parwest_id + '</td>' +
                //     //         '<td >' + value.guard_name + '</td>' +
                //     //         '<td >' + value.current_deployment + '</td>' +
                //     //         '<td >' + value.deployment_days + '</td>' +
                //     //         '<td >' + value.guards_phone + '</td>' +
                //     //         '<td >' + value.guards_current_supervisor + '</td>' +
                //     //         '<td >' + value.amount_paid + '</td>' +
                //     //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                //     //         '<td >' + value.payment_location + '</td>' +
                //     //         '<td >' + value.slip_number_loan + '</td>' +
                //     //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                //     //         '<td class="table-heading2 finalize">'+
                //     //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                //     //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                //     //         '</a> </button> '+ ' </td>' +
                //     //         '</tr>';
                //     //     $("#deploymentDetailsTableBody").append(markup);
                //     // });
                //
                //     // $('#guards_supervisor_loaned').html('');
                //     // $('#guards_supervisor_loaned').append($('<option>', {
                //     //     value: 0,
                //     //     text: 'ffffff',
                //     // }));
                //
                //     // $.each(result.data.supervisors.data, function (key, value) {
                //     //
                //     //     $('#guards_supervisor_loaned').append($('<option >', {
                //     //         value: value.status,
                //     //         value: value.id,
                //     //         text: value.name,
                //     //
                //     //     }));
                //     //
                //     // });
                //     successNotificationCustom(result.message);
                // }

            }else{
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        };


        removeGuardDeploymentDetails = function () {
            // console.log('hamza');

            // $('#guard_name').val('');
            // $('#guards_supervisor_loaned').html('');
            // $('#guards_supervisor_loaned').empty().append($('<option>', {
            //     value: 0,
            //     text: '--Select Branch--',
            // }));
        };




    </script>
@endsection
