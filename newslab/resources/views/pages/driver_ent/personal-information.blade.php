@php
    $role=Auth::user()->role_id;

@endphp
@if($role=='4')
    <?php $layout = 'layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout = 'layout.driver'; ?>
@elseif($role=='2')
    <?php $layout = 'layout.default'; ?>
@elseif($role=='1')
    <?php $layout = 'layout.admin'; ?>
@endif

@extends($layout)
@section('styles')
    <style>
        /*#loader1{*/
        /*    position:fixed;*/
        /*    z-index:999999999999;*/
        /*    width: 100%;*/
        /*    padding-top:30%;*/
        /*    background:rgb(72 68 68 / 75%);*/
        /*    height:100%*/
        /*}*/



        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        .dob:read-only {
            background-color: #f3f6f9;
        }


    </style>

@endsection
{{-- Content --}}
@section('content')
    <div id="loader1" class="d-none">
        <div style="display:flex;justify-content: center;">
            <div class="spinner-border text-primary" role="status">
                <span class="sr-only">Loading...</span>
            </div></div>
    </div>
    @if(@$user->profile->profile_pic)
        @php $profile_pic='/images/profile/'.$user->profile->profile_pic;  @endphp
    @else
        @php $profile_pic='assets/media/users/default.jpg'  @endphp
    @endif

    <!--begin::Content-->
    <!--begin::Content-->
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Mobile Toggle-->
                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none"
                            id="kt_subheader_mobile_toggle">
                        <span></span>
                    </button>
                    <!--end::Mobile Toggle-->
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Profile </h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">

                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Personal Information</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
            {{--                <div class="d-flex align-items-center">--}}
            {{--                    <!--begin::Actions-->--}}
            {{--                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>--}}
            {{--                    <!--end::Actions-->--}}
            {{--                    <!--begin::Dropdown-->--}}
            {{--                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">--}}
            {{--                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
            {{--											<span class="svg-icon svg-icon-success svg-icon-2x">--}}
            {{--												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
            {{--													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
            {{--														<polygon points="0 0 24 0 24 24 0 24" />--}}
            {{--														<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
            {{--														<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />--}}
            {{--													</g>--}}
            {{--												</svg>--}}
            {{--                                                <!--end::Svg Icon-->--}}
            {{--											</span>--}}
            {{--                        </a>--}}
            {{--                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">--}}
            {{--                            <!--begin::Navigation-->--}}
            {{--                            <ul class="navi navi-hover">--}}
            {{--                                <li class="navi-header font-weight-bold py-4">--}}
            {{--                                    <span class="font-size-lg">Choose Label:</span>--}}
            {{--                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-separator mb-3 opacity-70"></li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-success">Customer</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-danger">Partner</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-warning">Suplier</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-primary">Member</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-dark">Staff</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-separator mt-3 opacity-70"></li>--}}
            {{--                                <li class="navi-footer py-4">--}}
            {{--                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">--}}
            {{--                                        <i class="ki ki-plus icon-sm"></i>Add new</a>--}}
            {{--                                </li>--}}
            {{--                            </ul>--}}
            {{--                            <!--end::Navigation-->--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                    <!--end::Dropdown-->--}}
            {{--                </div>--}}
            <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Profile Personal Information-->
                <div class="d-flex flex-row">
                    <!--begin::Aside-->
                    <div class="flex-row-auto offcanvas-mobile w-250px w-xxl-350px" id="kt_profile_aside">
                        <!--begin::Profile Card-->
                        <div class="card card-custom card-stretch">
                            <!--begin::Body-->
                            <div class="card-body pt-4">
                                <!--begin::Toolbar-->
                                <div class="d-flex justify-content-end">
                                    <div class="dropdown dropdown-inline">
                                        {{--                                        <a href="#" class="btn btn-clean btn-hover-light-primary btn-sm btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                                        {{--                                            <i class="ki ki-bold-more-hor"></i>--}}
                                        {{--                                        </a>--}}
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <!--begin::Navigation-->
                                            <ul class="navi navi-hover py-5">
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
																		<span class="navi-icon">
																			<i class="flaticon2-drop"></i>
																		</span>
                                                        <span class="navi-text">New Group</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
																		<span class="navi-icon">
																			<i class="flaticon2-list-3"></i>
																		</span>
                                                        <span class="navi-text">Contacts</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
																		<span class="navi-icon">
																			<i class="flaticon2-rocket-1"></i>
																		</span>
                                                        <span class="navi-text">Groups</span>
                                                        <span class="navi-link-badge">
																			<span
                                                                                class="label label-light-primary label-inline font-weight-bold">new</span>
																		</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
																		<span class="navi-icon">
																			<i class="flaticon2-bell-2"></i>
																		</span>
                                                        <span class="navi-text">Calls</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
																		<span class="navi-icon">
																			<i class="flaticon2-gear"></i>
																		</span>
                                                        <span class="navi-text">Settings</span>
                                                    </a>
                                                </li>
                                                <li class="navi-separator my-3"></li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
																		<span class="navi-icon">
																			<i class="flaticon2-magnifier-tool"></i>
																		</span>
                                                        <span class="navi-text">Help</span>
                                                    </a>
                                                </li>
                                                <li class="navi-item">
                                                    <a href="#" class="navi-link">
																		<span class="navi-icon">
																			<i class="flaticon2-bell-2"></i>
																		</span>
                                                        <span class="navi-text">Privacy</span>
                                                        {{--                                                        <span class="navi-link-badge">--}}
                                                        {{--																			<span class="label label-light-danger label-rounded font-weight-bold">5</span>--}}
                                                        {{--														</span>--}}
                                                    </a>
                                                </li>
                                            </ul>
                                            <!--end::Navigation-->
                                        </div>
                                    </div>
                                </div>
                                <!--end::Toolbar-->
                                <!--begin::User-->
                                <div class="d-flex align-items-center">
                                    <div
                                        class="symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center">
                                        {{--                                        <div class="symbol-label" style="background-image:url({{$profile_pic}})">--}}
                                        <div class="symbol-label img-wrapper">
                                            @if(@$user->profile->profile_pic)
                                                <img class="img-fluid" src="{{asset($profile_pic)}}"  alt="" style="max-width: 100px;max-height: 100px" alt="">

                                            @else
                                                <img class="img-fluid" symbol symbol-60 symbol-xxl-100 mr-5 align-self-start align-self-xxl-center'assets/media/users/default.jpg')}}" alt="">
                                            @endif


                                        </div>
                                        <i class="symbol-badge bg-success"></i>
                                    </div>
                                    <div>
                                        <p
                                           class="font-weight-bolder font-size-h5 text-dark-75 text-hover-primary">{{$user->name}}</p>
                                        <div class="text-muted">{{@Auth::user()->profile->job_designation}}</div>
                                        <div class="mt-2">
                                        </div>
                                    </div>
                                </div>
                                <!--end::User-->
                                <!--begin::Contact-->
                                <div class="py-9">
                                    <div class="d-flex align-items-center justify-content-between mb-2">
                                        <span class="font-weight-bold mr-2">Email:</span>
                                        <a href="#" class="text-muted text-hover-primary">{{\Auth::user()->email}}</a>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-between mb-2">
                                        <span class="font-weight-bold mr-2">Phone:</span>
                                        <span class="text-muted">{{@Auth::user()->profile->contact_no}}</span>
                                    </div>
                                    <div class="d-flex align-items-center justify-content-between">
                                        <span class="font-weight-bold mr-2">Location:</span>
                                        <span class="text-muted">{{@Auth::user()->profile->country->name}}</span>
                                    </div>
                                </div>
                                <!--end::Contact-->
                                <!--begin::Nav-->
                                <div class="navi navi-bold navi-hover navi-active navi-link-rounded">
                                    <div class="navi-item mb-2" style="background-color: #f3f6f9;">
                                        <a href="{{route('user.personal.info')}}" class="navi-link py-4">

															<span class="navi-icon mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/General/User.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<polygon points="0 0 24 0 24 24 0 24"/>
																			<path
                                                                                d="M12,11 C9.790861,11 8,9.209139 8,7 C8,4.790861 9.790861,3 12,3 C14.209139,3 16,4.790861 16,7 C16,9.209139 14.209139,11 12,11 Z"
                                                                                fill="#000000" fill-rule="nonzero"
                                                                                opacity="0.3"/>
																			<path
                                                                                d="M3.00065168,20.1992055 C3.38825852,15.4265159 7.26191235,13 11.9833413,13 C16.7712164,13 20.7048837,15.2931929 20.9979143,20.2 C21.0095879,20.3954741 20.9979143,21 20.2466999,21 C16.541124,21 11.0347247,21 3.72750223,21 C3.47671215,21 2.97953825,20.45918 3.00065168,20.1992055 Z"
                                                                                fill="#000000" fill-rule="nonzero"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
															</span>
                                            <span class="navi-text font-size-lg">Personal Information</span>
                                        </a>
                                    </div>
                                    @if( $role=='4' || $role=='2')
                                        <div class="navi-item mb-2">
                                            <a href="{{route('user.bussiness.info')}}" class="navi-link py-4">
															<span class="navi-icon mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Code/Compiling.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24"/>
																			<path
                                                                                d="M2.56066017,10.6819805 L4.68198052,8.56066017 C5.26776695,7.97487373 6.21751442,7.97487373 6.80330086,8.56066017 L8.9246212,10.6819805 C9.51040764,11.267767 9.51040764,12.2175144 8.9246212,12.8033009 L6.80330086,14.9246212 C6.21751442,15.5104076 5.26776695,15.5104076 4.68198052,14.9246212 L2.56066017,12.8033009 C1.97487373,12.2175144 1.97487373,11.267767 2.56066017,10.6819805 Z M14.5606602,10.6819805 L16.6819805,8.56066017 C17.267767,7.97487373 18.2175144,7.97487373 18.8033009,8.56066017 L20.9246212,10.6819805 C21.5104076,11.267767 21.5104076,12.2175144 20.9246212,12.8033009 L18.8033009,14.9246212 C18.2175144,15.5104076 17.267767,15.5104076 16.6819805,14.9246212 L14.5606602,12.8033009 C13.9748737,12.2175144 13.9748737,11.267767 14.5606602,10.6819805 Z"
                                                                                fill="#000000" opacity="0.3"/>
																			<path
                                                                                d="M8.56066017,16.6819805 L10.6819805,14.5606602 C11.267767,13.9748737 12.2175144,13.9748737 12.8033009,14.5606602 L14.9246212,16.6819805 C15.5104076,17.267767 15.5104076,18.2175144 14.9246212,18.8033009 L12.8033009,20.9246212 C12.2175144,21.5104076 11.267767,21.5104076 10.6819805,20.9246212 L8.56066017,18.8033009 C7.97487373,18.2175144 7.97487373,17.267767 8.56066017,16.6819805 Z M8.56066017,4.68198052 L10.6819805,2.56066017 C11.267767,1.97487373 12.2175144,1.97487373 12.8033009,2.56066017 L14.9246212,4.68198052 C15.5104076,5.26776695 15.5104076,6.21751442 14.9246212,6.80330086 L12.8033009,8.9246212 C12.2175144,9.51040764 11.267767,9.51040764 10.6819805,8.9246212 L8.56066017,6.80330086 C7.97487373,6.21751442 7.97487373,5.26776695 8.56066017,4.68198052 Z"
                                                                                fill="#000000"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
															</span>
                                                <span class="navi-text font-size-lg">Bussiness Information</span>
                                            </a>
                                        </div>
                                    @endif
                                    <div class="navi-item mb-2">
                                        <a href="{{route('user.change.password')}}" class="navi-link py-4">
															<span class="navi-icon mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Shield-user.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24"/>
																			<path
                                                                                d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z"
                                                                                fill="#000000" opacity="0.3"/>
																			<path
                                                                                d="M12,11 C10.8954305,11 10,10.1045695 10,9 C10,7.8954305 10.8954305,7 12,7 C13.1045695,7 14,7.8954305 14,9 C14,10.1045695 13.1045695,11 12,11 Z"
                                                                                fill="#000000" opacity="0.3"/>
																			<path
                                                                                d="M7.00036205,16.4995035 C7.21569918,13.5165724 9.36772908,12 11.9907452,12 C14.6506758,12 16.8360465,13.4332455 16.9988413,16.5 C17.0053266,16.6221713 16.9988413,17 16.5815,17 C14.5228466,17 11.463736,17 7.4041679,17 C7.26484009,17 6.98863236,16.6619875 7.00036205,16.4995035 Z"
                                                                                fill="#000000" opacity="0.3"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
															</span>
                                            <span class="navi-text font-size-lg">Change Password</span>
                                            {{--                                            <span class="navi-label">--}}
                                            {{--																<span class="label label-light-danger label-rounded font-weight-bold">5</span>--}}
                                            {{--															</span>--}}
                                        </a>
                                    </div>
                                    <div class="navi-item mb-2">
                                        <a href="{{route('user.notification.setting')}}" class="navi-link py-4">															<span
                                                class="navi-icon mr-2">
																<span class="svg-icon">
																	<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Mail-opened.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24"/>
																			<path
                                                                                d="M6,2 L18,2 C18.5522847,2 19,2.44771525 19,3 L19,12 C19,12.5522847 18.5522847,13 18,13 L6,13 C5.44771525,13 5,12.5522847 5,12 L5,3 C5,2.44771525 5.44771525,2 6,2 Z M7.5,5 C7.22385763,5 7,5.22385763 7,5.5 C7,5.77614237 7.22385763,6 7.5,6 L13.5,6 C13.7761424,6 14,5.77614237 14,5.5 C14,5.22385763 13.7761424,5 13.5,5 L7.5,5 Z M7.5,7 C7.22385763,7 7,7.22385763 7,7.5 C7,7.77614237 7.22385763,8 7.5,8 L10.5,8 C10.7761424,8 11,7.77614237 11,7.5 C11,7.22385763 10.7761424,7 10.5,7 L7.5,7 Z"
                                                                                fill="#000000" opacity="0.3"/>
																			<path
                                                                                d="M3.79274528,6.57253826 L12,12.5 L20.2072547,6.57253826 C20.4311176,6.4108595 20.7436609,6.46126971 20.9053396,6.68513259 C20.9668779,6.77033951 21,6.87277228 21,6.97787787 L21,17 C21,18.1045695 20.1045695,19 19,19 L5,19 C3.8954305,19 3,18.1045695 3,17 L3,6.97787787 C3,6.70173549 3.22385763,6.47787787 3.5,6.47787787 C3.60510559,6.47787787 3.70753836,6.51099993 3.79274528,6.57253826 Z"
                                                                                fill="#000000"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
															</span>
                                            <span class="navi-text font-size-lg">Notification settings</span>
                                        </a>
                                    </div>
                                </div>                                <!--end::Nav-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Profile Card-->
                    </div>
                    <!--end::Aside-->
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8">
                        <!--begin::Card-->
                        <div class="card card-custom card-stretch">
                            <!--begin::Header-->
                            <div class="card-header py-3">
                                <div class="card-title align-items-start flex-column">
                                    <h3 class="card-label font-weight-bolder text-dark">Personal Information</h3>
                                    <span class="text-muted font-weight-bold font-size-sm mt-1">Update your personal informaiton</span>
                                </div>
                                <div class="card-toolbar">
                                    <button onclick="document.getElementById('form-id').submit();"
                                            class="btn btn-success mr-2">Save Changes
                                    </button>
{{--                                    <button type="reset" onclick='location.reload()' class="btn btn-secondary">Cancel--}}
{{--                                    </button>--}}
                                </div>
                            </div>
                            <!--end::Header-->
                            <!--end::Header-->
                            <!--begin::Form-->
                            <form id="form-id" class="form" method="POST" action="{{ route('user.update.personal') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="card-body">
                                    <!--begin::Alert-->
{{--                                   @if( $errors->first('email')=='')--}}
                                    @if($errors->any())
                                        <div class="alert alert-custom alert-light-danger fade show mb-10" role="alert">
                                            <div class="alert-icon">
															<span class="svg-icon svg-icon-3x svg-icon-danger">
																<!--begin::Svg Icon | path:assets/media/svg/icons/Code/Info-circle.svg-->
																<svg xmlns="http://www.w3.org/2000/svg"
                                                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                     width="24px" height="24px" viewBox="0 0 24 24"
                                                                     version="1.1">
																	<g stroke="none" stroke-width="1" fill="none"
                                                                       fill-rule="evenodd">
																		<rect x="0" y="0" width="24" height="24"/>
																		<circle fill="#000000" opacity="0.3" cx="12"
                                                                                cy="12" r="10"/>
																		<rect fill="#000000" x="11" y="10" width="2"
                                                                              height="7" rx="1"/>
																		<rect fill="#000000" x="11" y="7" width="2"
                                                                              height="2" rx="1"/>
																	</g>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                            </div>
                                            <div class="alert-text font-weight-bold">
{{--                                                @if($errors->has('dob'))--}}
{{--                                                    <li class="float-left">--}}
{{--                                                        Your Date of birth must be greater than 18  </li>@endif--}}
                                                @foreach ($errors->all() as $error)
                                                    <li class="float-left">{{ $error }}</li><br>

                                                @endforeach

                                            </div>
                                            <div class="alert-close">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-label="Close">
																<span aria-hidden="true">
																	<i class="ki ki-close"></i>
																</span>
                                                </button>
                                            </div>

                                        </div>
{{--                                        @endif--}}
                                @endif
                                <!--begin::Body-->
                                    <div class="card-body">
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Profile Picture </label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="image-input image-input-outline img-wrapper"
                                                     id="kt_profile_avatar">
                                                    <img class="image-input-wrapper img-fluid" id="preview-imge" src="{{asset($profile_pic)}}"  alt="" style="max-width: 200px;max-height: 100px">
                                                    <label
                                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                        id="slect-imge" data-action="change" data-toggle="tooltip"
                                                        title="" data-original-title="Change profile picture">
                                                        <i class="fa fa-pen icon-sm text-muted"></i>
                                                        <input type="file" id="imge-profile_avatar"
                                                               name="image" accept=".png, .jpg, .jpeg"/>
                                                        <input type="hidden" name="profile_avatar_remove"/>
                                                    </label>
                                                    <span
                                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                                        data-action="cancel" data-toggle="tooltip"
                                                        title="Cancel avatar">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
                                                    </span>
{{--                                                    <span--}}
{{--                                                        class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"--}}
{{--                                                        id="remove-imge" data-action="remove" data-toggle="tooltip"--}}
{{--                                                        title="Remove picture">--}}
{{--                                                        <i class="ki ki-bold-close icon-xs text-muted"></i>--}}

{{--                                                    </span>--}}
                                                </div>
                                                <span
                                                    class="form-text text-muted">Allowed file types: png, jpg, jpeg.</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">First Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control form-control-lg form-control-solid" required
                                                       name="fname" type="text"
                                                       value="{{old('fname',@$user->name)}}"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Last Name</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <input class="form-control form-control-lg form-control-solid"
                                                       name="lname" type="text"
                                                       value="{{old('lname',@$user->profile->last_name)}}"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">DOB</label>
                                            <div class="col-lg-9 col-xl-6">
{{--                                                @php @$date=Carbon\Carbon::parse(@$user->profile->dob)->format('Y-d-m') @endphp--}}
                                                <input class="form-control form-control-lg form-control-solid dob" readonly  name="dob" type=text" id="doba"
                                                       value="{{old('dob',@$user->profile->dob)}}" />
                                                {{--                                            <span class="form-text text-muted">If you want your invoices addressed to a company. Leave blank to use your full name.</span>--}}
                                            </div>
                                        </div>
                                        <div class="row">
                                            <label class="col-xl-3"></label>
                                            <div class="col-lg-9 col-xl-6">
                                                <h5 class="font-weight-bold mt-10 mb-6">Contact Info</h5>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Contact Number</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la la-phone"></i>
                                                    </span>
                                                    </div>
                                                    <input type="tel"
                                                           class="form-control form-control-lg form-control-solid" minlength="7"
                                                           name="contact_no"
                                                           value="{{old('contact_no',@$user->profile->contact_no)}}"
                                                           placeholder="Phone"/>
                                                </div>
                                                <span class="form-text text-muted">We'll never share your number with anyone else.</span>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Email Address</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la la-at"></i>
                                                    </span>
                                                    </div>
                                                    <input type="email" disabled
                                                           class="form-control form-control-lg form-control-solid"
                                                     value="{{$user->email}}" placeholder="Email"/>

                                                </div>
                                                <a href="#" onclick="updateemail({{$user->id}});" class=" w-100 text-right float-right">
                                                       change email
                                                    </a>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <label class="col-xl-3"></label>
                                            <div class="col-lg-9 col-xl-6">
                                                <h5 class="font-weight-bold mt-10 mb-6">Location Info</h5>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Country</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <label for="country_id" class="sr-only"></label>
                                                    <select class="form-control form-control-lg form-control-solid"
                                                            name="country_id" id="country_id">
                                                        <option selected disabled>Select country</option>
                                                        @foreach($countries  as $country)
                                                            @if($country->id   =='2')
                                                                <option
                                                                    <?php  echo (null != (@$user->profile->country_id) && ($user->profile->country_id == $country->id)) ? 'selected' : ''; ?> value="{{$country->id}}">{{$country->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>

                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">State</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <select class="form-control form-control-lg form-control-solid"
                                                            id="statesDropdown" onchange="getCitiess('2')"
                                                            name="state_id">
                                                        <option value="" disabled="disabled">Choose State</option>
                                                        {{--                                                    {{dd($states)}}--}}
                                                        @foreach($states  as $state)
                                                            <option
                                                                <?php  echo (null != (@$user->profile->state_id) && ($user->profile->state_id == $state->id)) ? 'selected' : ''; ?> value="{{$state->id}}">{{$state->state}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">City</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <select name="city_id" id="cities" class="form-control"
                                                            onchange="getZips();">
                                                        <option value="">Select City</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Street</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <input type="text"
                                                           class="form-control form-control-lg form-control-solid"
                                                           name="street_address"
                                                           value='{{@$user->profile->street_address}}'
                                                           placeholder="abc street"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Zip code</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <select name="zip_code" id="zips" class="form-control">
                                                        <option
                                                            value="{{@$user->profile->zip_code}}">{{@$user->profile->zip_code}}</option>
                                                    </select>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <label class="col-xl-3 col-lg-3 col-form-label">Address</label>
                                            <div class="col-lg-9 col-xl-6">
                                                <div class="input-group input-group-lg input-group-solid">
                                                    <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la la-at"></i>
                                                    </span>
                                                    </div>
                                                    <input type="text"
                                                           class="form-control form-control-lg form-control-solid"
                                                           name="address" value='{{@$user->profile->address}}'/>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!--end::Body-->
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Profile Personal Information-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->
    <!--end::Content-->
    <!--end::Content-->

    <!--end::Content-->
    <div class="modal fade" id="emailUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <form action="#" method="post" >
                    <div class="modal-header">
                        <h5 class="modal-title">Email update</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="card-body p-4">
                                    <!--begin::Invoice-->
                                    <div class="row justify-content-center pt-4 px-4 pt-md-4 ">
                                        <div class="col-md-12">
                                            <div id="kt_repeater_1">
                                                <div class="form-group row" id="kt_repeater_1">
                                                    <div data-repeater-list="">
                                                        <div data-repeater-item=""
                                                             class="form-group row align-items-center">
                                                            <div class="col-md-11 mt-4 offset-3">
                                                                <label>Updated email * </label>
                                                                @csrf
                                                                <input type="email" class="form-control"
                                                                       placeholder="Enter email" id="email"
                                                                       name="updated_email" required >
                                                                <input type="hidden" name="id"
                                                                       id="idd">
                                                                <div class="d-m text-danger mb-2 error"></div>
                                                            </div>
                                                            {{--                                                            <div class="col-md-6 mt-4">--}}
                                                            {{--                                                                <label>Latest Miles of car* </label>--}}
                                                            {{--                                                                @csrf--}}
                                                            {{--                                                                <input type="number" class="form-control" placeholder="Enter current miles of car" name="miles" required>--}}
                                                            {{--                                                                <input type="hidden"  name="campaign_id" id="campaign-id">--}}
                                                            {{--                                                                <div class="d-md-none mb-2"></div>--}}
                                                            {{--                                                            </div>--}}

                                                            <div class="col-5 offset-9 pt-4">
                                                                <div class="form-group">
                                                                    {{--                                                                    <label>Large Size</label>--}}
                                                                    <div class="checkbox-inline">
                                                                        <label class="checkbox checkbox-lg">
                                                                            <input id="termss" type="checkbox" value="1" required
                                                                                   name="termss" /><span></span>
                                                                            Are you sure this is a valid and your personal email</label>
                                                                    </div>
                                                                    <span class="terms_error text-danger pl-2"></span>
                                                                    {{--                                                                    <span class="form-text text-muted">Some help text goes here</span>--}}
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                        <!--end::Invoice-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                        </button>
                        <button type="button" class="btn btn-light font-weight-bold btn-terms"
                                value="Submit" onclick="update_email()" >Submit<span class="btn-terms1"></span></button>
                        {{--                        <input  type="submit" class="btn btn-light font-weight-bold"  value="Submit">--}}
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

{{-- Scripts Section --}}
@section('scripts')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

    <script>

            function updateemail(id) {
                $('#emailUpdate').modal('show');
                $('#idd').val(id);
            }
            function validateEmail(email) {
                const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }

            function update_email(id) {

                if ($('#termss').is(":checked")) {
                }else{
                    $('.terms_error').text('Please tick the checkbox');
                    return;
                }
                if(!validateEmail($('#email').val())){
                    $('.error').text('Email is invalid');
                    return;
                }

                var id = $('#idd').val();
                var email = $('#email').val();
                $('.btn-terms1').addClass('spinner-border');
                var data = {
                    'id': id,
                    'email': email,
                };

                $.ajax({
                    url: '{{route('user.email_update')}}',
                    type: "GET",
                    data: data,
                    success: function (result) {
                        // console.log(result);
                        // console.log('message' + result);
                        // console.log('updated_email'+result.errors[0].updated_email);

                        if (result == '1') {
                            toastr.success('Please verify your email', 'Email updated successfully');
                            setTimeout(function(){ location.reload(); },2000);
                            $('.btn-terms1').removeClass('spinner-border');

                        } else {
                            toastr.error('Try later', 'Error occured');
                        }
                    }, error: function (data) {
                        if (data.status === 422) {
                            var errors = $.parseJSON(data.responseText);
                            $.each(errors, function (key, value) {
                               $('.error').text(value.email);
                                $('.btn-terms1').removeClass('spinner-border');
                            });
                        }

                    }
                })
            }

        // $('#dob').datepicker(
        //     {
        //         format: 'mm/dd/yyyy',
        //         startDate: '01/01/1949',
        //
        //     }
        // );
        $(function(){
            // document.getElementById("dob").disabled = true;
            var dtToday = new Date();
            var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
            var day = dtToday.getDate();
            var year = dtToday.getFullYear() - 18;
            if(month < 10)
                month = '0' + month.toString();
            if(day < 10)
                day = '0' + day.toString();
            var minDate = year + '-' + month + '-' + day;
            var maxDate = year + '-' + month + '-' + day;
            $('#dob').attr('max', maxDate);
        });




        // $('#cities').attr('disabled','disabled');
        getCitiess('1');
        function getZips() {
            // var state_id = $('#states_' + id + ' option:selected').val();
            var city_id = $("#cities").val();
            // alert(id);
            var data = {
                'city_id': city_id,

            };
            var object = {
                'url': globalPublicPath + '/get-zips',
                'processData': false,
                'contentType': false,
                'cache': false,
                'data': data,
            };
            var result2 = centralAjax(object);

            $('#zips').html('');
            $.each(result2, function (index, value) {
                var zip_code = value['zip'];
                var newOption = new Option(zip_code, zip_code, false, false);
                $('#zips').append(newOption);
            });
            $('#zips').prepend("<option selected disabled='disabled'>Choose zip please</option>");

            // var newOption = new Option('Select Zip', '', true,false);
            $('#zips').append(newOption);
            if (result2.length == 0) {
                var newOption = new Option('No zip found', '', false, false);
                $('#zips').prepend(newOption);
            }
        }

        function getCitiess(check) {
            var state_id = $('#statesDropdown').val();
            console.log(state_id);

            var data = {
                'state_id': state_id,
            };
            var object = {
                'url': globalPublicPath + '/get-cities',
                'processData': false,
                'contentType': false,
                'cache': false,
                'data': data,
            };

            var result2 = centralAjax(object);
            $('#cities').html('');
            $.each(result2, function (index, value) {
                var city_id = value['id'];
                var name = value['primary_city'];
                var newOption = new Option(name, city_id, false, false);
                $('#cities').append(newOption);

            });
            $('#cities').append("<option selected disabled='disabled'>Choose City</option>");
            if (check == '1') {
                var profile_city_id = {{ @$user->profile->city_id }}
                $('#cities').val(profile_city_id);
            }
        }

        function getStates(id) {
            var data = {
                'id': id,
            };
            var object = {
                'url': globalPublicPath + '/getCities',
                'type': 'GET',
                'data': data,
            };
            var result = centralAjax(object);
            console.log('result', result);
            $('#statesDropdown').html('');
            $.each(result, function (index, value) {
                var sub_id = value['id'];
                var sub_name = value['name'];

                var newOption = new Option(sub_name, sub_id, true, true);
                $('#statesDropdown').append(newOption).trigger('change');

            });
        };


        function readURLa(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview-imge').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
        }

        $("#imge-profile_avatar").on('change', function () {
            readURLa(this);
        })
        $("#remove-imge").on('click', function () {
            // var name =  $("#imge-profile_avatar").val();
            $('#preview-imge').attr('src', 'assets/media/users/default.jpg');
            // console.log(name)
        })




        function validateEmail(email) {
            const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        }

            var dtToday = new Date();
            var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
            var day = dtToday.getDate();
            var year = dtToday.getFullYear() - 18;
            var year1 = dtToday.getFullYear() - 25;
            if(month < 10)
                month = '0' + month.toString();
            if(day < 10)
                day = '0' + day.toString();
            var maxDate =month+'/'+day+'/'+'/'+year;

            $('#doba').datepicker({
                format: 'mm/dd/yyyy',
                startDate: '01/01/1949',
                endDate: maxDate,
                defaultDate: '01/01/1959',
                startView:2,
                autoclose: true,
                readOnly:true,

            })



        {{--    @if (\Session::has('success'))--}}
        {{--        toastr.success('{!! \Session::get('success') !!}', 'User Created');--}}
        {{--    @endif--}}
        {{--    @if (\Session::has('error'))--}}
        {{--        toastr.error(' Sorry. User not created', 'User Not Created');--}}
        {{--    @endif--}}
        @if (\Session::has('updated'))
        toastr.info('{!! \Session::get('updated') !!}', 'Updated');
        @endif
        {{--    @if (\Session::has('deleted'))--}}
        {{--        toastr.warning('{!! \Session::get('deleted') !!}', 'User Deleted');--}}
        {{--    @endif--}}
    </script>
@endsection
