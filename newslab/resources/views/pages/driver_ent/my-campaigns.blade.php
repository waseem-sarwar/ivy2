@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout = 'layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout = 'layout.driver'; ?>
@elseif($role=='2')
    <?php $layout = 'layout.default'; ?>
@elseif($role=='1')
    <?php $layout = 'layout.admin'; ?>
@endif
@extends($layout)
@section('content')

    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }

        .fix-table-column-old2 {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 25px;
            background-color: #ffffff;
        }

        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head {
            position: sticky;
            right: 0;
        }
    </style>

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Campaigns
                    <div class="text-muted pt-2 font-size-sm">Campaign Listing</div>
                </h3>
            </div>
            <div class="card-toolbar">
                {{--                <select class="data-table-10list float-right">--}}
                {{--                    <option value="">10</option>--}}
                {{--                    <option value="">20</option>--}}
                {{--                </select>--}}
                {{--                <a class="btn btn-light-primary mr-2" href="javascript:;" id="toggleSearchForm">--}}
                {{--                     <span class="svg-icon svg-icon-md">--}}
                {{--                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->--}}
                {{--                                                 <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"--}}
                {{--                                                      version="1.1">--}}
                {{--                                                     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
                {{--                                                         <rect x="0" y="0" width="24" height="24"/>--}}
                {{--                                                         <path--}}
                {{--                                                             d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"--}}
                {{--                                                             fill="#000000" opacity="0.3"/>--}}
                {{--                                                         <path--}}
                {{--                                                             d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"--}}
                {{--                                                             fill="#000000"/>--}}
                {{--                                                     </g>--}}
                {{--                                                 </svg>--}}
                {{--                         <!--end::Svg Icon-->--}}
                {{--                                             </span>--}}
                {{--                    Search--}}
                {{--                </a>--}}
            </div>
        </div>

        <div class="card-body">
            <!--begin::Search Form-->
            <div class="mt-2 mb-5 mt-lg-5 mb-lg-10" id="table-search-form"
                 style="display: <?php  echo ($check == '1') ? 'block' : 'none'; ?>">
                <form action="#" id="frm">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-xl-12  ">
                            <div class="row align-items-center">
                                <div class="col-md-3 my-2 my-md-0  offset-2">
                                    <div class="input-icon">
                                        <input type="text" name="search" class="form-control" placeholder="Search..."
                                               value="{{(isset($_GET['search']))?$_GET['search']:''}}"
                                               id="kt_datatable_search_query"/>
                                        <span><i class="flaticon2-search-1 text-muted"></i></span>
                                    </div>
                                </div>

                                <div class="col-md-3 my-2 my-md-0 ">
                                    <div class="d-flex align-items-center">
                                        <label class=" mb-0 d-none d-md-block">Campaign Status:</label>&nbsp
                                        <select class="form-control" name='campaign_status'
                                                id="kt_datatable_search_status">
                                            {{--                                        <option selected disabled>Choose Campaign status</option>--}}
                                            @foreach($status as $stat)
                                                <option
                                                    value="{{$stat->id}}" {{(isset($_GET['campaign_status'])&& $stat->id==$_GET['campaign_status'])?'selected':''}} >{{$stat->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" onclick="document.getElementById('frm').submit();"
                                       class="btn btn-light-primary px-6 font-weight-bold">
                                        Search
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <!-- HTML Table Begin Here -->
            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table
                        class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">Campaign Id</th>
                            <th style="min-width: 250px" class="pl-7">
                                <span>Campaign</span>
                            </th>
                            <th style="min-width: 130px">Working Status</th>
                            <th >Earned Amount</th>
                            <th style="max-width: 50px">Start Date</th>
                            <th style="max-width: 50px">End Date</th>
                            <th class="fix-table-head" style="min-width: 220px;"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(count($campaigns)==0)
                            <tr class="text-center">
                                <td colspan="9"><h4>Sorry, No campaign exist </h4></td>
                            </tr>
                        @endif
                        @foreach($campaigns as $campaign)
                            <tr>
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                         <a href="{{route('user.camp.detail',$campaign->campaign->slug)}}">CMP-{{$campaign->campaign->id}}</a>
                                    </span>
                                    {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                                </td>
                                <td class="pl-0 py-8">
                                    <div class="d-flex align-items-center">
                                        <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                 @if(@$campaign->campaign->sticker->image_url!=null)
                                                    @php $url='/images/campaign-sticker/'.$campaign->campaign->sticker->image_url; @endphp
                                                        <img width="100%" height="100%" src="{{asset($url)}}" class=" align-self-end" />
                                                 @endif

                                             </span>

                                        </div>
                                        <div>
                                            <a href="#"
                                               class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{@$campaign->campaign->campaign->title}}</a>
                                            {{--                                            <span class="text-muted font-weight-bold d-block">more infor sample </span>--}}
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span class=" p-4 label label-lg label-light-primary label-inline">{{$campaign->status}}</span>
                                </td>
                                <td class="text-center ">
                                    <span class="text-dark-75 font-weight-bolder  mb-1 font-size-lg">{{(!is_null($campaign->amount_gives))?"$".$campaign->amount_gives:'-'}}</span>
                                </td>
                                <td>
                                    @php $interval=explode("-",$campaign->campaign->campaign->interval)@endphp
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                        {{ Carbon\Carbon::parse($interval[0])->format('d M Y') }}
                                    </span>
                                    <span class="text-muted font-weight-bold"></span>
                                </td>
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                     {{ Carbon\Carbon::parse(@$interval[1])->format('d M Y') }} {{$campaign->availability}}</span>
                                    <span class="text-muted font-weight-bold"> </span>
                                </td>
                                <td class="pr-0 fix-table-column float-left" style="min-width: 230px;">
                                    @if($campaign->availability==0)
                                        <span class="example-tools justify-content-center cursor-auto">
                                                <a class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                   data-toggle="tooltip" title="Waiting for availabilty confirmation">
                                                <i class="far fa-pause-circle"></i></a>
                                      </span>
                                    @endif
                                        @if($campaign->availability==1 || $campaign->availability==2)
                                        <span class="example-tools justify-content-center cursor-auto">
                                                <a class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                   data-toggle="tooltip" title="{{($campaign->availability==1)?'Available'?:($campaign->availability===0)?:'Waiting for availabilty confirmation':'Not available'}}">
                                                <i class="{{($campaign->availability==1)?'far fa-check-circle text-success'?:($campaign->availability==0)?:'far fa-pause-circle':' far fa-times-circle'}}"></i></a>
                                      </span>
                                      @endif
                                        @if($campaign->address_acknowledged==1)
                                            <span class="example-tools justify-content-center cursor-auto">
                                        <a class=" btn btn-sm btn-default btn-text-success btn-hover-info btn-icon cursor-pointer mr-1" data-toggle="tooltip" title="" data-original-title="Address Confirmed">
                                            <i class="far fa-envelope text-success"></i></a>
                                            </span>
                                      @endif
                                    @if($campaign->status=='Waiting for documents' || $campaign->status=='Assigned' )
                                        @if($campaign->courier_status=='Mail Acknowledged' && $campaign->documents_status!=null)
                                            {{--                                        <a href="#" data-toggle="modal"--}}
                                            {{--                                           onclick="getCampaignId({{$campaign->campaign_id}});" data-target="#editModal"--}}
                                            {{--                                           class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"--}}
                                            {{--                                           title="Upload Campaign Start documents">--}}
                                            {{--                                            <i class="la la-video"></i>--}}
                                            {{--                                        </a>--}}
                                            @if($campaign->documents_status=='Waits for sticker video upload')
                                                <span class="example-tools justify-content-center cursor-auto ">
                                                    <a onclick="getCampaignId({{$campaign->campaign_id}});"
                                                       class=" btn btn-sm btn-default btn-text-primary btn-hover-info btn-icon cursor-pointer"
                                                       data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                      <i class="la la-video"></i></a>
                                                </span>
                                            @endif
                                            @if($campaign->documents_status=='Waiting for video sticker approval')
                                                <span class="example-tools justify-content-center cursor-auto" onclick="get_start_details({{$campaign->car_id}},{{$campaign->user_id}});">
                                                    <a
                                                        class=" btn btn-sm btn-default btn-text-primary btn-hover-info btn-icon cursor-pointer"
                                                        data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                      <i class="la la-video"></i></a>
                                                </span>
                                            @endif
                                            @if($campaign->documents_status=='Waiting for starting miles approval')
                                                <span class="example-tools justify-content-center cursor-auto">
                                                    <a onclick="get_start_details({{$campaign->car_id}},{{$campaign->user_id}});"
                                                       class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                       data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                      <i class="la la-video"></i></a>
                                                </span>

                                              @elseif( $campaign->documents_status=='Waiting for resend sticker' )
                                                <span class="example-tools justify-content-center cursor-auto">
                                                    <a onclick="get_start_details({{$campaign->car_id}},{{$campaign->user_id}});"
                                                       class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                       data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                      <i class="la la-video"></i></a>
                                                </span>
                                              @endif


                                        @endif


                                        @if($campaign->courier_status=='Waiting for address confirmation' || $campaign->courier_status=='Address Acknowledged'|| $campaign->courier_status=='Courier resend request')
                                            <span class="example-tools justify-content-center "
                                                  onclick="courier_details('{{$campaign->courier}}','{{$campaign->car_id}}','{{$campaign}}')">
                                                <a class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon"
                                                   data-toggle="tooltip" title=""
                                                   data-original-title="{{($campaign->courier_status=='Address Acknowledged')?'Waiting for courier details':(($campaign->courier_status=='Waiting for address confirmation')?'Waiting for address confirmation':$campaign->courier_status)}}"><i
                                                        class="fa fa-envelope-open-text"></i></a>
                                               </span>
                                                @elseif($campaign->courier_status=='Waits for courier received confirmation')
                                                    <span class="example-tools justify-content-center "
                                                          onclick="courier_details1('{{$campaign->courier}}','{{$campaign->car_id}}','{{$campaign}}');">
                                                        <a class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon"
                                                           data-toggle="tooltip" title=""
                                                           data-original-title="{{($campaign->courier_status)}}"><i
                                                                class="fa fa-envelope-open-text"></i></a>
                                                   </span>
                                                   @elseif($campaign->courier_status=='Courier Received')
                                                        <span class="example-tools justify-content-center "
                                                              onclick="courier_details('{{$campaign->courier}}','{{$campaign->car_id}}','{{$campaign}}')">
                                                            <a class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon"
                                                               data-toggle="tooltip" title=""
                                                               data-original-title="{{($campaign->courier_status)}}"><i
                                                                    class="fa fa-envelope-open-text text-success"></i></a>
                                                       </span>
                                                    @if($campaign->documents_status=='Waits for sticker video upload')
                                                            <span class="example-tools justify-content-center cursor-auto ">
                                                                <a onclick="getCampaignId({{$campaign->car_id}});"
                                                                   class=" btn btn-sm btn-default btn-text-primary btn-hover-info btn-icon cursor-pointer"
                                                                   data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                                  <i class="la la-video"></i></a>
                                                            </span>
                                                        @elseif($campaign->documents_status=='Miles approved')
                                                            <span class="example-tools justify-content-center cursor-auto">
                                                            <a onclick="get_start_details({{$campaign->car_id}},{{$campaign->user_id}});"
                                                               class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                               data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                              <i class="la la-video text-success"></i></a>
                                                        </span>
                                                           @else
                                                            <span class="example-tools justify-content-center cursor-auto">
                                                            <a onclick="get_start_details({{$campaign->car_id}},{{$campaign->user_id}});"
                                                               class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                               data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                              <i class="la la-video"></i></a>
                                                        </span>
                                                        @endif
                                            @elseif($campaign->courier_status=='Courier not received')
                                                        <span class="example-tools justify-content-center "
                                                              onclick="courier_details('{{$campaign->courier}}','{{$campaign->car_id}}','{{$campaign}}')">
                                                            <a class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon"
                                                               data-toggle="tooltip" title=""
                                                               data-original-title="{{($campaign->courier_status)}}"><i
                                                                    class="fa fa-envelope-open-text"></i></a>
                                                       </span>
                                            @elseif($campaign->availability==1)
                                                <span class="example-tools justify-content-center cursor-auto ">
                                                    <a style="cursor: default"
                                                       class="btn btn-sm btn-default btn-text-primary btn-hover-info btn-icon cursor-pointer"
                                                       data-toggle="tooltip"
                                                       data-original-title="Waiting for address"><i
                                                            class="flaticon2-pin"></i></a>
                                                </span>
                                            @endif

{{--                                            @if( $campaign->documents_status=='Waiting for resend miles' )--}}
{{--                                                <span class="example-tools justify-content-center cursor-auto">--}}
{{--                                                    <a onclick="get_start_details({{$campaign->campaign_id}},{{$campaign->user_id}});"--}}
{{--                                                       class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"--}}
{{--                                                       data-toggle="tooltip" title="{{$campaign->documents_status}}">--}}
{{--                                                      <i class="la la-video"></i></a>--}}
{{--                                                </span>--}}
{{--                                            @endif--}}
                                            @if($campaign->status=='Assigned' )
{{--                                                <span class="example-tools justify-content-center cursor-auto">--}}
{{--                                                    <a onclick="get_start_details({{$campaign->campaign_id}},{{$campaign->user_id}});"--}}
{{--                                                       class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"--}}
{{--                                                       data-toggle="tooltip" title="{{$campaign->documents_status}}">--}}
{{--                                                      <i class="la la-video"></i></a>--}}
{{--                                                </span>--}}
                                              <span class="example-tools justify-content-center cursor-auto"
                                                  data-target="#finshCampaign"
                                                  onclick="finshCampaign({{$campaign->car_id}})" data-toggle="modal">
                                                    <a class=" btn btn-sm btn-default btn-text-primary btn-hover-danger btn-icon cursor-pointer"
                                                        data-toggle="tooltip" title="{{$campaign->status}}">
                                                      <i class="la la-road"></i></a>
                                                </span>

                                         @endif

                                    @endif
                                    @if($campaign->status=='Finished' || $campaign->status=='Completed' )
                                            <span class="example-tools justify-content-center "
                                                  onclick="courier_details('{{$campaign->courier}}','{{$campaign->car_id}}','{{$campaign}}')">
                                                <a class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon"
                                                   data-toggle="tooltip" title=""
                                                   data-original-title="{{($campaign->courier_status=='Address Acknowledged')?'Courier Acknowledged':(($campaign->courier_status=='Mail sent')?'Courier Mail received':$campaign->courier_status)}}"><i
                                                        class="fa fa-envelope-open-text text-success"></i></a>
                                        </span>
                                        <span class="example-tools justify-content-center cursor-auto">
                                                    <a onclick="get_start_details({{$campaign->car_id}},{{$campaign->user_id}});"
                                                       class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                       data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                      <i class="la la-video text-success"></i></a>
                                                </span>


                                        <span class="example-tools justify-content-center cursor-auto">
                                            <a class=" btn btn-sm btn-default btn-text-primary btn-hover-danger btn-icon cursor-pointer" onclick="documentsCompleteModel({{$campaign->car_id}},{{$campaign->user_id}},'{{$campaign->status}}');"
                                                data-toggle="tooltip" title="Status:{{$campaign->status}} Amount Earned: {{($campaign->amount_gives)?"$".$campaign->amount_gives:'N/A'}}">
                                              <i class="la la-car text-success"></i></a>
                                         </span>
                                    @endif
{{--                                    @if($campaign->status=='Applied' && $campaign->availability==1)--}}
{{--                                    <span class="example-tools justify-content-center cursor-auto">--}}
{{--                                        <a class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"--}}
{{--                                           data-toggle="tooltip" title="Waiting for request approval">--}}
{{--                                          <i class="la la-car"></i></a>--}}
{{--                                      </span>--}}
{{--                                    @endif--}}


                                        @if($campaign->documents_status=='Waiting for miles')
                                            <span class="example-tools justify-content-center cursor-auto">
                                                    <a onclick="getCampaignId1({{$campaign->car_id}});"
                                                       class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                                                       data-toggle="tooltip" title="{{$campaign->documents_status}}">
                                                      <i class="la la-tachometer"></i></a>
                                                </span>
                                        @endif



                                    {{--                                    <a href="javascript:;" data-toggle="modal" data-target="#statusUpdatestatusUpdate" onclick="status('{{$campaign->status_id}}','{{$campaign->id}}')" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">--}}
                                    {{--                                        <i class="la la-wrench"></i>--}}
                                    {{--                                    </a>--}}
                                    {{--                                    <a href="javascript:;" data-toggle="modal" onclick="feedback('{{$campaign->id}}')" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">--}}
                                    {{--                                        <i class="la la-comments-o"></i>--}}
                                    {{--                                    </a>--}}
                                    {{--                                    <a href="javascript:;" onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">--}}
                                    {{--                                        <i class="la la-trash"></i>--}}
                                    {{--                                    </a>--}}


                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>

                </div>
                <nav aria-label="Page navigation example ">
                    <ul class="pagination float-right mt-5">
                        {{--                        <h6 class="pt-3">Total records:{{$campaigns->total()}}  </h6>  &nbsp &nbsp&nbsp  {{$campaigns->links()}}--}}
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Title</h5>
                    <button class="btn btn-light-primary font-weight-bold text-right" style="
    margin-left: 267px;
"><i class="fa fa-download" aria-hidden="true"></i>download
                    </button>
                    <button class="btn btn-light-primary font-weight-bold" style="
    margin-left: -68px;
"><i class="fa fa-print" aria-hidden="true"></i>print
                    </button>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">
                    <div class="container">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">
                                    <div class="col-md-9">
                                        <!-- begin: Invoice header-->
                                        <div
                                            class="d-flex justify-content-between pb-5 pb-md-10 flex-column flex-md-row">
                                            <h1 class="display-4 font-weight-boldest mb-10">Sample Campaign Title</h1>
                                            {{--                                            <span>Duration : 20m </span>--}}

                                            <div class="d-flex flex-column align-items-md-end px-0">
                                                <!--begin::Logo-->
                                                <a href="#" class="mb-5 max-w-200px">
															<span class="svg-icon svg-icon-full">
																<!--begin::Svg Icon | path:assets/media/svg/logos/duolingo.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" width="113"
                                                                     height="31" viewBox="0 0 113 31" fill="none">
																	<path
                                                                        d="M26.6802 13.33V13.485C26.6802 14.9575 26.6087 15.8875 26.4659 16.275C26.2517 16.895 25.966 17.36 25.4661 17.67C25.0376 17.98 24.5377 18.1358 23.8949 18.1358C23.2529 18.1358 22.7523 17.9808 22.3245 17.67C21.8953 17.3608 21.6103 16.895 21.3954 16.275C21.1811 15.7325 21.1104 14.8025 21.1104 13.4075V5.425H17.3254V13.485C17.3968 15.345 17.5396 16.6625 17.7539 17.515C18.1824 18.9108 18.8965 19.995 19.8963 20.8483C20.8962 21.6233 22.2531 22.0108 23.8242 22.0108C25.324 22.0108 26.538 21.7008 27.5379 21.0025C28.537 20.3058 29.2511 19.375 29.7518 18.0575C30.1088 17.205 30.3231 15.655 30.3231 13.485V5.425H26.6087V13.33H26.6802ZM15.6107 0H11.897V6.2775C11.8256 6.2775 11.7542 6.2 11.6828 6.2C10.4687 5.425 9.11182 5.0375 7.75492 5.0375C5.82669 5.0375 4.1127 5.735 2.61297 7.2075C0.898984 8.9125 0.0419922 11.005 0.0419922 13.485C0.0419922 15.81 0.827568 17.825 2.3273 19.4525C3.82704 21.0808 5.68314 21.9325 7.75492 21.9325C9.18324 21.9325 10.5401 21.545 11.7542 20.8483C12.9683 20.0733 13.8967 19.065 14.6109 17.7483C15.2536 16.5858 15.54 15.345 15.6107 14.0283V13.4858V0ZM10.6823 16.895C9.89668 17.7483 8.96899 18.2125 7.7542 18.2125C6.61155 18.2125 5.61172 17.7475 4.89756 16.895C4.11199 16.0425 3.75491 14.88 3.75491 13.485C3.75491 12.1675 4.11199 11.005 4.89756 10.1525C5.68314 9.3 6.61155 8.835 7.7542 8.835C8.96828 8.835 9.89668 9.3 10.6823 10.1525C11.4685 11.005 11.8256 12.1675 11.8256 13.485C11.8256 14.88 11.4685 16.0425 10.6823 16.895ZM46.2481 9.3C45.534 7.9825 44.6049 6.975 43.3915 6.2C42.1774 5.425 40.8205 5.0375 39.4636 5.0375C37.5354 5.0375 35.8207 5.735 34.3217 7.2075C32.6077 8.9125 31.75 10.9275 31.75 13.485C31.75 15.81 32.5363 17.825 34.0353 19.4525C35.5357 21.0808 37.3918 21.9325 39.4636 21.9325C40.8919 21.9325 42.2481 21.545 43.4629 20.8483C44.677 20.0733 45.6054 19.065 46.3195 17.7483C47.0337 16.4308 47.3908 15.0358 47.3908 13.4858C47.248 12.0125 46.9623 10.6175 46.2481 9.3ZM42.3917 16.895C41.6061 17.7483 40.6777 18.2125 39.4636 18.2125C38.2495 18.2125 37.3211 17.7475 36.607 16.895C35.8214 16.0425 35.4643 14.88 35.4643 13.485C35.4643 12.1675 35.8214 11.005 36.6077 10.1525C37.3933 9.3 38.321 8.835 39.4643 8.835C40.6777 8.835 41.6068 9.3 42.3924 10.1525C43.178 11.005 43.535 12.1675 43.535 13.485C43.5336 14.88 43.1058 16.0425 42.3917 16.895ZM60.2457 21.7H64.0307V6.1225H60.2457V21.7ZM53.8182 17.515C53.3897 17.2058 53.0326 16.74 52.8898 16.12C52.747 15.655 52.6756 14.725 52.6756 13.33V0H48.9619V13.2525C49.0334 15.4225 49.1762 16.895 49.5333 17.825C50.0332 19.065 50.7473 20.0725 51.7472 20.77C52.747 21.4675 53.9618 21.7775 55.4608 21.7775H58.3174V17.98H55.3894C54.7466 17.98 54.246 17.825 53.8182 17.515ZM78.7424 9.145C78.2425 7.905 77.5283 6.8975 76.5278 6.2C75.5287 5.5025 74.3139 5.1925 72.8142 5.1925C71.1716 5.1925 69.8861 5.58 68.8863 6.355C67.8872 7.13 67.173 8.215 66.7438 9.6875C66.5296 10.4625 66.3867 11.8575 66.3153 13.7175V21.7H70.1003V13.95V13.64C70.1003 12.3225 70.1725 11.315 70.386 10.7725C70.6003 10.1525 70.8859 9.765 71.3144 9.3775C71.7429 9.0675 72.2428 8.9125 72.8856 8.9125C73.5276 8.9125 74.0282 9.0675 74.4567 9.3775C74.8852 9.6875 75.2423 10.1525 75.4558 10.7725C75.5987 11.2375 75.6701 12.1675 75.6701 13.5625V21.7H79.3837V14.88V13.64C79.3137 11.5475 79.0995 9.9975 78.7424 9.145ZM62.0311 0.31C60.9598 0.31 60.0314 1.3175 60.0314 2.48C60.0314 3.6425 60.8884 4.65 62.0311 4.65C63.1023 4.65 64.0307 3.72 64.0307 2.48C64.0307 1.24 63.1023 0.31 62.0311 0.31ZM111.951 9.455C111.237 8.1375 110.308 7.13 109.094 6.355C107.88 5.58 106.595 5.27 105.094 5.27C102.952 5.27 101.166 6.1225 99.6666 7.75C98.1661 9.3775 97.3813 11.3925 97.3813 13.7175C97.3813 16.1975 98.2383 18.2892 99.9522 19.995C101.453 21.4675 103.166 22.165 105.094 22.165C106.522 22.165 107.808 21.7775 109.022 21.0025C110.236 20.2275 111.237 19.22 111.879 17.9025C112.593 16.5858 112.95 15.19 112.95 13.7183C113.022 12.1675 112.665 10.7725 111.951 9.455ZM109.236 13.7175C109.236 15.1125 108.879 16.1975 108.094 17.05C107.308 17.9025 106.38 18.3675 105.166 18.3675C104.022 18.3675 103.023 17.9017 102.309 17.05C101.523 16.1975 101.166 15.035 101.166 13.7175C101.166 12.3225 101.523 11.16 102.309 10.3075C103.095 9.455 104.022 8.99 105.166 8.99C106.308 8.99 107.308 9.455 108.094 10.3075C108.879 11.16 109.236 12.3225 109.236 13.7175C109.236 13.64 109.236 13.64 109.236 13.7175ZM96.0965 13.02C96.0244 11.7025 95.6673 10.54 95.0967 9.3775C94.3825 8.06 93.4541 7.0525 92.3122 6.2775C91.0981 5.5025 89.8126 5.1925 88.3843 5.1925C86.2418 5.1925 84.4564 5.9675 82.9559 7.6725C81.4562 9.3 80.7421 11.315 80.7421 13.64C80.7421 16.12 81.599 18.2125 83.2416 19.84C84.7413 21.3125 86.3832 22.0108 88.3121 22.0108C89.669 22.0108 91.026 21.6233 92.24 20.8483C92.3122 20.77 92.3829 20.77 92.4543 20.6933V22.5525C92.4543 23.9483 92.0258 25.0325 91.3116 25.8858C90.526 26.7383 89.5976 27.2025 88.455 27.2025C87.3123 27.2025 86.3839 26.815 85.5983 25.9625L83.1702 28.7525L83.2416 28.83C84.7413 30.3017 86.4553 31 88.3121 31C89.669 31 91.026 30.6125 92.24 29.8375C93.4541 29.0625 94.3825 28.055 95.0967 26.7375C95.8108 25.42 96.0965 24.025 96.0965 22.5525V14.105V13.5625C96.1679 13.4075 96.1679 13.175 96.0965 13.02ZM92.4536 13.5625C92.4536 14.9575 92.0965 16.0425 91.3109 16.895C90.5253 17.7483 89.5969 18.2125 88.4543 18.2125C87.3116 18.2125 86.3832 17.7475 85.5976 16.895C84.812 16.0425 84.455 14.9575 84.455 13.5625C84.455 12.1675 84.812 11.0825 85.5976 10.1525C86.3832 9.3 87.3116 8.835 88.4543 8.835C89.5969 8.835 90.5967 9.3 91.3109 10.1525C92.0258 11.0825 92.4536 12.1675 92.4536 13.5625Z"
                                                                        fill="#1CB0F6"></path>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                                </a>
                                                <!--end::Logo-->
                                                <span
                                                    class="d-flex flex-column text-center align-items-md-end font-size-h6 font-weight-bold text-muted">
{{--															<span class="text-center">Start Campaign  </span>--}}
                                                    {{--															<span>12 May 2020  </span>--}}

                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="d-flex  font-size-lg mb-3">
                                                <span class="font-weight-bold mr-12">Campaign Status:</span>
                                                <span class="text-right ">
                                                    	<span class="navi-icon mr-2">
													<i class="fa fa-genderless text-primary font-size-h2"></i>
                                                            																		</span>
                                                    In review
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="d-flex  font-size-lg mb-3">
                                                <span class="font-weight-bold mr-15">Payment Status:</span>
                                                <span class="text-right ">
                                                    	<span class="navi-icon mr-2"><i
                                                                class="fa fa-genderless text-success font-size-h2"></i>
                                                            																		</span>
                                                   Paid
                                                </span>
                                            </div>
                                        </div>


                                        <!--end: Invoice header-->
                                        <!--begin: Invoice body-->
                                        <div class="row border-bottom pb-10">
                                            <div class="col-md-7 py-md-10 pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Location
                                                            </th>

                                                            <th class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Zipcode
                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Cost
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">
                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-center">
																		<span class="navi-icon mr-2">
																			<i class="fa fa-genderless text-danger font-size-h2"></i>
																		</span>England London
                                                            </td>

                                                            <td class="text-right pt-7">564</td>
                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                $3200.00
                                                            </td>
                                                        </tr>
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                                        {{--																		</span>Front-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $4800.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                        {{--																		</span>Back-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $12600.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                <div class="d-flex flex-column flex-md-row">
                                                    <div class="d-flex flex-column mb-10 mb-md-0">
                                                        <div
                                                            class=" font-size-h6 mb-3 pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                            Vehicle Info
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-3">
                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>
                                                            <span class="text-right">Car</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-3">
                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>
                                                            <span class="text-right">Honda</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg">
                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>
                                                            <span class="text-right">2020</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 border-left-md pl-md-10 py-md-10">
                                                <!--begin::Total Amount-->
                                                <!--begin::Total Amount-->
                                                <div class="font-size-h6 font-weight-bolder text-muted mb-3">Grand
                                                    Amount
                                                </div>
                                                <div class="font-size-h6 font-weight-boldest">$20,60</div>
                                            {{--                                                <div class="text-muted font-weight-bold mb-16">Taxes included</div>--}}
                                            <!--end::Total Amount-->
                                                <div class="border-bottom w-100 mb-16"></div>
                                                <!--begin::Invoice To-->
                                            {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Campaign Status--}}
                                            {{----}}
                                            {{--                                                </div>--}}
                                            {{--                                                <div class="font-size-lg font-weight-bold mb-10">Iris Watson.--}}
                                            {{--                                                    <br>In review--}}
                                            {{--                                                </div>--}}
                                            <!--end::Invoice To-->
                                                <!--begin::Invoice No-->
                                            {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Payment Status--}}
                                            {{--                                                </div>--}}
                                            {{--                                                <div class="font-size-lg font-weight-bold mb-10">Pending</div>--}}
                                            <!--end::Invoice No-->
                                                <!--begin::Invoice Date-->
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Start Date
                                                    12 May, 2020
                                                </div>
                                                <div class="font-size-lg font-weight-bold"></div>
                                                <!--end::Invoice Date--><!--begin::Invoice Date-->
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">End Date 18
                                                    May, 2020
                                                </div>
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Duration 7
                                                    Days
                                                </div>
                                                <div class="font-size-lg font-weight-bold"></div>
                                                <!--end::Invoice Date--><!--begin::Invoice Date-->
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="d-flex flex-column flex-md-row">
                                                    <div class="d-flex flex-column mb-10 mb-md-0">
                                                        {{--                                                        <div class=" font-size-h6 mb-3 pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                                        {{--                                                            Vehicle Info--}}
                                                        {{--                                                        </div>--}}
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span class="font-weight-bold mr-15">Number of Cars:</span>
                                                            <span class="text-right">12</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span
                                                                class="font-weight-bold mr-15">How often miles drived:</span>
                                                            <span class="text-right">12</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span
                                                                class="font-weight-bold mr-1">How often drives a car:</span>
                                                            <span class="text-right">Weekly</span>
                                                        </div>
                                                        {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                        {{--                                                            <span class="font-weight-bold mr-15">How often miles drived:</span>--}}
                                                        {{--                                                            <span class="text-right">34</span>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                                        {{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                                        {{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                                        {{--                                                        </div>--}}
                                                    </div>
                                                </div>
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Number of Cars : 3 </div>--}}
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">How often miles drived: 20 </div>--}}
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">How often drives a car: Weekly </div>--}}
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <!--end::Invoice Date-->--}}
                                            </div>
                                        </div>
                                        <!--end: Invoice body-->
                                        <div class="row border-bottom pb-1">
                                            <div class="col-md-6  pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                My Sticker Info
                                                            </th>

                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">

                                                            {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                                            {{--																		<span class="navi-icon mr-2">--}}
                                                            {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                            {{--																		</span>in progress--}}
                                                            {{--                                                            </td>--}}

                                                            {{--                                                            <td class="text-right pt-7">564</td>--}}
                                                            {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                                            {{--                                                                $3200.00--}}
                                                            {{--                                                            </td>--}}
                                                        </tr>

                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                                        {{--																		</span>Front-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $4800.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                        {{--																		</span>Back-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $12600.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        </tbody>
                                                    </table>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Company:</span>
                                                        <span class="text-right">20thFloor Technologies</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15"> Slogan</span>
                                                        <span class="text-right">Technologies</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Contact</span>
                                                        <span class="text-right">+166855869</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>
                                                        <span class="text-right"></span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        {{--                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>--}}
                                                        <span class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Logo</span>
                                                        <span class="text-right"></span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        {{--                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>--}}
                                                        <span class="text-left">
{{--                                                            <img src="{{ url('/media/cinqueterre.jpg') }}"--}}
{{--                                                                 class="img-rounded" alt="Cinque Terre" width="150px"--}}
{{--                                                                 height="150px" style="--}}
{{--   border-radius: 20px;--}}
{{--">--}}
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6  pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Preview
                                                            </th>

                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">

                                                            {{--                                                            <td class="text-right pt-7">564</td>--}}
                                                            {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                                            {{--                                                                $3200.00--}}
                                                            {{--                                                            </td>--}}
                                                        </tr>
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                                        {{--																		</span>Front-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $4800.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                        {{--																		</span>Back-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $12600.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        </tbody>
                                                    </table>
                                                    <img src="http://127.0.0.1:8000/media/cinqueterre.jpg"
                                                         class="img-rounded" alt="Cinque Terre" width="233" height="236"
                                                         style="
    width: 100%;
">
                                                    <br>
                                                    <br>
                                                    <br>
                                                    <br>
                                                    {{--<br>--}}
                                                    {{--<br>--}}
                                                </div>

                                            </div>

                                        </div>
                                        {{--                                        <div class="row border-bottom pb-2">--}}
                                        {{--                                            <div class="col-md-6 py-md-10 pr-md-10">--}}
                                        {{--                                                <div class="table-responsive">--}}
                                        {{--                                                    <table class="table">--}}
                                        {{--                                                        <thead>--}}
                                        {{--                                                        <tr>--}}
                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
                                        {{--                                                                Campaign Status--}}
                                        {{--                                                            </th>--}}

                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        </thead>--}}
                                        {{--                                                        <tbody>--}}
                                        {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
                                        {{--																		</span>in review--}}
                                        {{--                                                            </td>--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--																		</span>in progress--}}
                                        {{--                                                            </td>--}}

                                        {{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
                                        {{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                            --}}{{--                                                                $3200.00--}}
                                        {{--                                                            --}}{{--                                                            </td>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Front-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $4800.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Back-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $12600.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        </tbody>--}}
                                        {{--                                                    </table>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
                                        {{--                                                <div class="d-flex flex-column flex-md-row">--}}
                                        {{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                        {{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="col-md-6 py-md-10 pr-md-10">--}}
                                        {{--                                                <div class="table-responsive">--}}
                                        {{--                                                    <table class="table">--}}
                                        {{--                                                        <thead>--}}
                                        {{--                                                        <tr>--}}
                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
                                        {{--                                                                Payment Status--}}
                                        {{--                                                            </th>--}}

                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        </thead>--}}
                                        {{--                                                        <tbody>--}}
                                        {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
                                        {{--																		</span>in review--}}
                                        {{--                                                            </td>--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--																		</span>in progress--}}
                                        {{--                                                            </td>--}}

                                        {{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
                                        {{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                            --}}{{--                                                                $3200.00--}}
                                        {{--                                                            --}}{{--                                                            </td>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Front-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $4800.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Back-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $12600.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        </tbody>--}}
                                        {{--                                                    </table>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
                                        {{--                                                <div class="d-flex flex-column flex-md-row">--}}
                                        {{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                        {{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}

                                        {{--                                        </div>--}}
                                        {{--                                        <div class="row border-bottom pb-2">--}}
                                        {{--                                            <div class="col-md-12 py-md-10 pr-md-10">--}}
                                        {{--                                                <div class="table-responsive">--}}
                                        {{--                                                    <table class="table">--}}
                                        {{--                                                        <thead>--}}
                                        {{--                                                        <tr>--}}
                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
                                        {{--                                                                Payment Status--}}
                                        {{--                                                            </th>--}}

                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        </thead>--}}
                                        {{--                                                        <tbody>--}}
                                        {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
                                        {{--																		</span>1 -pending--}}
                                        {{--                                                            </td>--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--																		</span>2  - paid--}}
                                        {{--                                                            </td>--}}

                                        {{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
                                        {{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                            --}}{{--                                                                $3200.00--}}
                                        {{--                                                            --}}{{--                                                            </td>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Front-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $4800.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Back-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $12600.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        </tbody>--}}
                                        {{--                                                    </table>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
                                        {{--                                                <div class="d-flex flex-column flex-md-row">--}}
                                        {{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                        {{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}

                                        {{--                                        </div>--}}
                                    </div>
                                    <div class="row mt-n5">
                                        <div class="col-sm-12">
                                            <iframe width="560" height="315"
                                                    src="https://www.youtube.com/embed/U_lTuI8Ck0I" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>

                                </div>

                                <!--end::Invoice-->

                            </div>


                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" style="z-index: 99999999"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <form action="{{route('user.campaign.start.documents')}}" id="video_form" method="post"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title">Campaign Start Documents</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="card-body p-4">
                                    <div class="row justify-content-center pt-4 px-4 pt-md-4 ">
                                        <div class="col-md-12">
                                            <div id="kt_repeater_1">
                                                <div class="form-group row" id="kt_repeater_1">
                                                    <div data-repeater-list="">
                                                        <div data-repeater-item=""
                                                             class="form-group row align-items-center">
                                                            {{--                                                            <div class="col-md-6">--}}
                                                            {{--                                                                <label>Mileage picture* </label>--}}
                                                            {{--                                                                <input type="file" class="form-control"--}}
                                                            {{--                                                                       name="mileage_pic" required>--}}
                                                            {{--                                                                <div class="d-md-none mb-2"></div>--}}
                                                            {{--                                                            </div>--}}
                                                            @csrf
                                                            <div class="col-md-6">
                                                                <label>Sticker pasted picture* </label>
                                                                <input type="file" class="form-control sticker-pic"
                                                                       name="sticker_pic" id="sticker_pic"
                                                                       accept="image/*" required>
                                                                <div class="d-m  mb-2 sticker_pic_error"><small>picture
                                                                        must not be greater thn 5mb's</small></div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Sticker pasted video*</label>
                                                                <input type="file" class="form-control sticker-video"
                                                                       name="sticker_video" id="sticker_video"
                                                                       accept="video/*" required>
                                                                <div class="d-m  mb-2 sticker_video_error"><small>video
                                                                        must not be greater thn 20mb's</small></div>
                                                            </div>
                                                            <input type="hidden" name="campaign_id" class="campaign_idd">
                                                            <div class="d-md-none mb-2"></div>
                                                            {{--                                                            </div>--}}
                                                        </div>
                                                    </div>


                                                </div>

                                            </div>
                                        </div>

                                        <!--end::Invoice-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                        </button>

                        <input type="button" id="video_btn" class="btn btn-light font-weight-bold" onclick="formsbmt();"
                               value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="editModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" style="z-index: 9999999"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <form action="{{route('user.campaign.start.documents.miles')}}" id="video_form1" method="post"
                      enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title">Campaign Mile Form</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="card-body p-4">
                                    <div class="row justify-content-center pt-4 px-4 pt-md-4 ">
                                        <div class="col-md-12">
                                            <div id="kt_repeater_1">

                                                <div data-repeater-list="">
                                                    <div class=" row align-items-center">
                                                        {{--                                                            <div class="col-md-6">--}}
                                                        {{--                                                                <label>Mileage picture* </label>--}}
                                                        {{--                                                                <input type="file" class="form-control"--}}
                                                        {{--                                                                       name="mileage_pic" required>--}}
                                                        {{--                                                                <div class="d-md-none mb-2"></div>--}}
                                                        {{--                                                            </div>--}}
                                                        @csrf
                                                        <div class="col-md-6 ">
                                                            <label>Current Miles picture* </label>
                                                            <input type="file" class="form-control"
                                                                   name="mileage_pic" id="mileage_pic" accept="image/*"
                                                                   required>
                                                            {{--                                                                <div class="d-m  mb-2 sticker_pic_error" ><small></small></div>--}}
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Current Miles of car* </label>
                                                            <input type="number" class="form-control"
                                                                   name="mileage_input" id="mileage_input" required
                                                                   min="1">
                                                            {{--                                                                <div class="d-m  mb-2 sticker_pic_error" ><small> </small></div>--}}
                                                            <input type="hidden"  class="campaien_id" name="campaign_id" id="campain-id">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!--end::Invoice-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                        </button>


                        <input type="button" id="video_btn" class="btn btn-light font-weight-bold"
                               onclick="formsbmt1();" value="Submit">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="courierfeedback" tabindex="-1" role="dialog" style="z-index: 99999;opacity:1"
         aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Courier Address </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-9 pb-0">
                                    <div class="form-group row pl-2 pr-2">
                                        <br><br>

                                        <div class="col-lg-12">
                                            <div class="form-group pt-1">
                                                <label>Write your correct mailing address:</label>&nbsp &nbsp &nbsp &nbsp<span
                                                    id="reject_feedback_error" class="text-danger text-success"></span>
                                                <textarea name="s_u1" id='reject_feedback_field' class="w-100 "
                                                          type="textarea" rows="3"></textarea>
{{--                                                <span class="form-text text-muted">Please send modification request</span>--}}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="reject_feedback()" data-repeater-create=""
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="couriernotreceivedfeedback" tabindex="-1" role="dialog" style="z-index:99999"  aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Courier Not Received Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-9 pb-0">
                                    <div class="form-group row pl-2 pr-2">
                                        <br>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <form action="{{route('user.driverRejectCourierDeleivery')}}" enctype="multipart/form-data" id="form-rejected" method="post">
                                                @csrf
                                                <input name="campaign_id" class='campeee_id' type="hidden" />
                                                <label class="pt-1">Write detail here</label>&nbsp &nbsp &nbsp &nbsp<span
                                                    id="reject_courier_field_error" class="text-danger text-success"></span>
                                                <textarea name="coment" id='reject_courier_field' class="w-100 form-control"
                                                          type="textarea" rows="5" required></textarea>
                                                <label class="pt-3">Relevent screenshot</label>&nbsp &nbsp &nbsp &nbsp<span
                                                    id="reject_feedback_error" class="text-danger text-success"></span>
                                                <input name="not_received_file" id='not_received_file' class="w-100 form-control" required accept="image/*,application/pdf"  type="file" />
                                                &nbsp &nbsp &nbsp &nbsp<span id="not_received_file_error" class="text-danger"></span>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="reject_feedback1()" data-repeater-create=""
                       class="btn btn-sm font-weight-bolder btn-light-primary btn33">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">I am Compaign Title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">


                                <div class="col-md-12">
                                    <div id="kt_repeater_1">
                                        <div class="form-group row" id="kt_repeater_1">
                                            <div data-repeater-list="">
                                                <div data-repeater-item=""
                                                     class="form-group row align-items-center">
                                                    <div class="col-md-6">
                                                        <label>State:</label>
                                                        <input type="text" class="form-control" name="[0][state]"
                                                               placeholder="Select state" onkeyup="showdetail();">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>City:</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Select city" name="[0][city]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Zip:</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Enter zip code" name="[0][zip]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6 mt-4">
                                                        <a href="javascript:;" data-repeater-delete=""
                                                           class=" btn btn-sm font-weight-bolder btn-light-danger">
                                                            <i class="la la-trash-o"></i>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 ">
                                                <a href="javascript:;" data-repeater-create=""
                                                   class="btn btn-sm font-weight-bolder btn-light-primary">
                                                    <i class="la la-plus"></i> Add more
                                                </a>
                                            </div>


                                        </div>
                                        <div class="col-md-4 border text-center p-1">
                                            <div class="form-group">
                                                <h3>Details</h3>
                                                <div class="inner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade show" id="feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                                <span class=" d-block"><h5 class="d-inline">Campaign Id:  </h5> <span
                                                        id='cmp_id' class="float-right mr-30"></span></span>
                                        </div>
                                        <br><br><br>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Write feedback about campaign:</label>
                                                <textarea name="s_u1" class="w-100" type="textarea"
                                                          rows="10"></textarea>
                                                <span class="form-text text-muted"> feedback.</span>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create=""
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>


                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="statusUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Campaign Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-4">
                                        <div class="row">
                                            <div class="col-md-12 mt-4 ">
                                                    <span class="text-center d-block"><h5
                                                            class="d-inline">Campaign Id:  </h5> <span
                                                            id="camp_id"></span></span>
                                            </div>
                                            <br><br><br>


                                            <div class="col-lg-12">
                                                <form id="form-id" method="POST"
                                                      action="{{ route('campaign.status.update') }}">
                                                    @csrf
                                                    <input type="hidden" name="camp_id" id="camp-id">
                                                    <div class="form-check">
                                                        <label>Status Update:</label>
                                                        <select name="s_u" class=" form-control" id="stat1"
                                                                required>
                                                            <option value="" selected="" disabled="">
                                                                Choose one option
                                                            </option>
                                                            @foreach($status as $stats)
                                                                <option
                                                                    value="{{$stats->id}}">{{$stats->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        {{--                                                <span class="form-text text-muted"> Choose status.</span>--}}
                                                    </div>
                                                </form>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create=""
                       onclick="document.getElementById('form-id').submit();"
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Update
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="CourierModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Courier Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center  px-md-0 pt-3">

                                        <div class="row border-bottom pb-5 ">
                                            <div class="col-md-12 p-0 text-center pb-2">Campaign Id: &nbsp <span
                                                    id="campen_id"></span></div>
                                            <div class="col-md-6 p-0 text-center address_confiramation">Courier company</div>
                                            <div class="col-md-6 p-0  text-left courier_company"></div>
                                            <div class="col-md-6  p-0  text-center address_confiramation">Courier tracking id</div>
                                            <div class="col-md-6  p-0 text-left tracking_id"></div>
                                            <div class="col-md-6   p-0  text-center ">Address</div>
                                            <div class="col-md-6  p-0  text-left address"></div>
                                            <div class="col-md-6  p-0 text-center address_confiramation">Courier url</div>
                                            <div class="col-md-6  p-0 text-left url address_confiramation"></div>


                                                <div class="col-md-6  p-0 text-center pt-5 feed">My feedback:<br></div>
                                                <div class="col-md-5  pt-5 text-left  p-0"><span class="feed_text"></span> </div>

                                            <div class="d-none couier_not_received col-12">
                                                <div class="col-md-6 offset-1 p-0 text-center pt-5  pl-1">Not received Comment:<br> <span class="not_received_comment"></span></div>
{{--                                                <div class="col-md-5  pt-5 text-left  p-0 not_received_comment "></div>--}}
                                                <div class="col-md-6   offset-1 p-0 text-center pt-5  ">Not received file:<span class="not_received_pic"></span></div>
{{--                                                <div class="col-md-4  pt-5   p-0 not_received_pic ">3</div>--}}
                                            </div>


                                            <div class="col-md-2 offset-6 p-0 pt-4 text-center ">
                                                <button href="javascript;"  id='reject_btn' data-toggle="modal"
                                                   data-target="#courierfeedback"
                                                   class="btn-danger btn font-weight-bold mr-10">Reject</button>
                                            </div>
                                            <div class="col-md-1 p-0 pt-4 text-center">
                                                <button type="button" id="accept_courier"
                                                        class="btn btn-light-primary font-weight-bold"
                                                        onclick="accept_courier();">
                                                    Accept
                                                </button>
                                            </div>
                                            <!--end: Invoice body-->
                                        </div>


                                    <!--end::Invoice-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                        </button>
                        <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="CourierReceiveModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Courier Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center  px-md-0">
                                    <div class="col-md-12 pt-5">
                                        <div class="row border-bottom pb-5">
                                            <div class="col-md-12 p-0 text-center pb-2">Campaign ID: &nbsp <span class="campen_id2" id="campen_id2" ></span></div>
                                            <div class="col-md-6 p-0 text-center">Courier company</div>
                                            <div class="col-md-6 p-0  text-left courier_company"></div>
                                            <div class="col-md-6  p-0  text-center">Courier tracking id</div>
                                            <div class="col-md-6  p-0 text-left tracking_id"></div>
                                            <div class="col-md-6   p-0  text-center">Address</div>
                                            <div class="col-md-6  p-0  text-left address"></div>
                                            <div class="col-md-6  p-0 text-center ">Courier url</div>
                                            <div class="col-md-6  p-0 text-left url"></div>

                                            <div class="col-md-6  p-0 text-center pt-5 feed">My feedback:<br></div>
                                            <div class="col-md-5  pt-5 text-left  p-0 "><span class="feed_text"></span>
                                            </div>

                                            <div class="col-md-3 offset-4 p-0  text-center mt-8">
                                                <button type="button" href="javascript;" id='reject_courier_btn' data-toggle="modal"
                                                        data-target="#couriernotreceivedfeedback"
                                                        class="btn btn-light-danger font-weight-bold">Courier didnt received</button>
                                            </div>

                                            <div class="col-md-3 p-0 mt-8 text-center">
                                                <button type="button" id="courier_received"
                                                        class="btn btn-light-primary font-weight-bold"
                                                        onclick="accept_courier1();">
                                                    Courier received
                                                </button>
                                            </div>


                                            <!--end: Invoice body-->
                                        </div>
                                    </div>

                                    <!--end::Invoice-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                        </button>
                        <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade show" id="get_details_modal1" tabindex="-1" role="dialog" style="z-index: 99999"
         aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog  modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver campaign documents</h5>

                    <div classormsbmt1="float-right">
                    <span class="example-tools cursor-auto" id="mile_resend_button_click"   onclick="getCampaignId10();" >
                        <a  class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                           data-toggle="tooltip" title="" >
                          <i class="la la-upload"></i></a>
                    </span>
                        <span class="example-tools cursor-auto id1" id="sticker_resend_button_click" onclick="getCampaignId12()">
                        <a  class="btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer"
                           data-toggle="tooltip" title="" >
                          <i class="la la-upload"></i></a>
                    </span>

                        <small class="badge badge-pill badge-light  ml-3" id="stats"></small>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-9 pb-0 ">
                                    <div class="form-group row pl-2 pr-2">
                                        <br><br>
                                        <div class="col-lg-12 pt-5">
                                            <table
                                                class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center table-bordered">
                                                <tr>
                                                    <td>Campaign Id</td>
                                                    <td class='campee_id' id="campee_id"></td>
                                                    <td>Start Miles</td>
                                                    <td id="strt_miles"></td>
                                                    <td>Capmaign start date</td>
                                                    <td id="strt_date"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-lg-12 pt-0" id='miles-comment'>

                                        </div>
                                        <div class="col-lg-6">
                                            <video width="400" height="340" controls id="sticker_video1">
                                                <source src="">
                                            </video>
                                        </div>
                                        <div class="col-lg-6 mt-1 pl-10">
                                            <img width="400" height="300" id="sticker_img1" alt="sticker image" src=""/>
                                        </div>
                                        <div class="col-lg-12 mt-1 mile_pic">
                                            <img width="800" height="300" id="mile_pic" alt="Mile image" src=""/>
                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="finshCampaign" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <form action="{{route('user.campaign.finish.documents')}}" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title">Finish Campaign Documents</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="card-body p-4">
                                    <!--begin::Invoice-->
                                    <div class="row justify-content-center pt-4 px-4 pt-md-4 ">
                                        <div class="col-md-12">
                                            <div id="kt_repeater_1">
                                                <div class="form-group row" id="kt_repeater_1">
                                                    <div data-repeater-list="">
                                                        <div data-repeater-item=""
                                                             class="form-group row align-items-center">
                                                            <div class="col-md-6">
                                                                <label>Latest Mileage picture* </label>
                                                                <input type="file" class="form-control"
                                                                       name="mileage_pic" required  accept="image/*">
                                                                <div class="d-md-none mb-2"></div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Sticker pasted fresh picture* </label>
                                                                <input type="file" class="form-control"
                                                                       name="sticker_pic" required accept="image/*">
                                                                <div class="d-md-none mb-2"></div>
                                                            </div>
                                                            <div class="col-md-6 mt-4">
                                                                <label>Latest Miles of car* </label>
                                                                @csrf
                                                                <input type="number" class="form-control"
                                                                       placeholder="Enter current miles of car"
                                                                       name="miles" required min="1">
                                                                <input type="hidden" name="campaign_id"
                                                                       id="campaign-iidd">
                                                                <div class="d-md-none mb-2"></div>
                                                            </div>
                                                            {{--                                                            <div class="col-md-6 mt-4">--}}
                                                            {{--                                                                <label>Latest Miles of car* </label>--}}
                                                            {{--                                                                @csrf--}}
                                                            {{--                                                                <input type="number" class="form-control" placeholder="Enter current miles of car" name="miles" required>--}}
                                                            {{--                                                                <input type="hidden"  name="campaign_id" id="campaign-id">--}}
                                                            {{--                                                                <div class="d-md-none mb-2"></div>--}}
                                                            {{--                                                            </div>--}}

                                                            <div class="col-10 offset-1 pt-4">
                                                                <div class="form-group">
                                                                    {{--                                                                    <label>Large Size</label>--}}
                                                                    <div class="checkbox-inline">
                                                                        <label class="checkbox checkbox-lg">
                                                                            <input id="termss" type="checkbox" value="1"
                                                                                   name="termss"><span></span>
                                                                            I verify that above upoladed record are true
                                                                            and completed the required miles of campaign</label>
                                                                    </div>
                                                                    <span class="terms_error text-danger pl-2"></span>
                                                                    {{--                                                                    <span class="form-text text-muted">Some help text goes here</span>--}}
                                                                </div>
                                                            </div>


                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                        <!--end::Invoice-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                        </button>
                        <input type="submit" class="btn btn-light font-weight-bold btn-terms" style="cursor:default;"
                               disabled="disabled" value="Submit">
                        {{--                        <input  type="submit" class="btn btn-light font-weight-bold"  value="Submit">--}}
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade show" id="DriverCompleteDocuments" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign  <small class="badge badge-pill badge-light" id="camp_id12"></small></h5>


                    <div class="float-right">
                        <small class="badge badge-pill badge-light ml-3" id="drriver"></small>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--                        <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{--                    </button>--}}
                </div>
                <div class="modal-body ">
                    <div class="card card-custom">
                        <div class="card-body p-1 ">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-6  ">
                                    <div class="row ">
                                        <div class="col-md-9  pb-3 pl-4">
                                            Start miles of car
                                        </div>
                                        <div class="col-md-3 pb-3" id="start_miles_no"></div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-9  pb-3 pl-4">
                                            Campaign start date
                                        </div>
                                        <div class="col-md-3 pb-3" id="start_date"></div>
                                    </div>
                                    <div class="">
                                        Sticker Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img id="start_sticker_pic" height="280px" alt="Sticker picture">
                                    </div>

                                    <div class="">
                                        Mileage Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img height="280px" id="start_mileage_pic" alt="mileage picture">
                                    </div>

                                </div>
                                <div class="col-md-6  ">
                                    <div class="row ">
                                        <div class="col-md-6  pb-3 pl-4">
                                            Completed miles of car
                                        </div>
                                        <div class="col-md-3 pb-3" id="completed_miles_no"></div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-6  pb-3 pl-4">
                                            Campaign completed date
                                        </div>
                                        <div class="col-md-3 pb-3" id="completed_date"></div>
                                    </div>
                                    <div >
                                        Sticker Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img id="completed_sticker_pic" height="280px" alt="sticker picture">
                                    </div>
                                    <div class="">
                                        Mileage Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img height="280px" id="completed_mileage_pic" alt="mileage picture">
                                    </div>
                                </div>

                                <div class="col-md-6 mt-4 ">
                                    <label>Covered Miles* </label>
                                    <input type="number" class="form-control" type="number" id="miles_covered" disabled
                                           name="miles_covered">

                                </div>

                                <div class="col-md-6 mt-4 ">
                                    <label>Days </label>
                                    <input type="text" disabled class="form-control" type="number" id="days_covered">

                                </div>
{{--                                <div class="col-md-6 mt-4 ">--}}
{{--                                    <label>Campaign amount </label>--}}
{{--                                    <input type="text" disabled class="form-control" type="number" id="actual_amount">--}}

{{--                                </div>--}}
                                <div class="col-md-12 mt-4 ">
                                    <label>Amount Earned</label>
                                    <input type="text"  disabled class="form-control" type="number" id="giving_amount">

                                </div>
                                {{--                                <div class="col-md-6 mt-4 ">--}}
                                {{--                                    <label>Actual Amount </label>--}}
                                {{--                                    <span id="actual_amount"></span>--}}
                                {{--                                    <div class="d-md-none mb-2"></div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="col-md-6 mt-4 ">--}}
                                {{--                                    <label>Giving Amount </label>--}}
                                {{--                                    <span id="giving_amount"></span>--}}
                                {{--                                    <div class="d-md-none mb-2"></div>--}}
                                {{--                                </div>--}}


                            </div>

                            <!--end::Invoice-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">


                    {{--                    <button type="button" class="btn btn-light-success font-weight-bold">Request to resend documents again</button>--}}
                    <a href="#" class="btn  btn-light-primary font-weight-bold" data-dismiss="modal" data-toggle="modal" >Back</a>


                </div>
            </div>
        </div>
    </div>


@endsection

@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    {{-- vendors --}}
    <script>

        var globalPublicPath = globalPublicPath+'/images/driver-documents';
        var globalPublicPath2 = '<?php echo e(url('/').env('APP_URL_LIVE_SEGMENT')); ?>';
        var globalPublicPath2 = globalPublicPath2+'/images/driver/courierreject';
        $(document).on('change', '#termss', function (e) {
            if ($('#termss').is(":checked")) {
                $('.terms_error').text('');
                $('.btn-terms').removeClass('btn-light');
                $('.btn-terms').removeAttr('disabled');
                $('.btn-terms').addClass('btn-primary');
                $('.btn-terms').css('cursor', 'pointer');
            } else {
                $('.terms_error').text('Please agree our terms and conditions');
                $('.btn-terms').removeClass('btn-primary');
                $('.btn-terms').attr('disabled', 'disabled');
                $('.btn-terms').addClass('btn-light');
                $('.btn-terms').css('cursor', ' auto');
            }
        });
        function finshCampaign(id) {
            // alert(id)
            $('#campaign-iidd').val(id);
        }

        //     alert( globalPublicPath)
{{--        var globalPublicPath = '{{ URL('/SlabCar/images/driver-documents/') }}';--}}
        $('#sticker_video').bind('change', function () {
            if ((this.files[0].size / 1024 / 1024) > 20) {
                $('#sticker_video').val('');
                $('.sticker_video_error').addClass('text-danger');
            } else {
                $('.sticker_video_error').removeClass('text-danger');
            }
        });
        $('#sticker_pic').bind('change', function () {
            if ((this.files[0].size / 1024 / 1024) > 5) {
                $('#sticker_pic').val('');
                $('.sticker_pic_error').addClass('text-danger');
            } else {
                $('.sticker_pic_error').removeClass('text-danger');
            }
        });

        function formsbmt() {
            var extensions = ["mp4", "webm","m4v","3gp","flv","mkv"];

            // return;
            if ($('.sticker-pic').val() == '') {
                $(".sticker-pic").css("border-color", 'red');
                return;
            }else{
                $(".sticker-pic").css("border-color", '#E4E6EF');
            }

            if ($('.sticker-video').val() == ''){
                $(".sticker-video").css("border-color", 'red');
                return;
            }
            else{
                $(".sticker-video").css("border-color", '#E4E6EF');
            }
            var extension=$('.sticker-video').val().split('.').pop();
            if (extensions.includes(extension)==false){
                $(".sticker-video").css("border-color", 'red');
                $('.sticker_video_error').addClass('text-danger');
                $('.sticker_video_error').text('Allowed formats are: mp4,webm,m4v,3gp,flv,mkv');
                return;
            }
            else{
                $(".sticker-video").css("border-color", '#E4E6EF');
            }
            // alert(extensions.includes(extension));
            // return;
            $('#loader1').removeClass('d-none');
            document.getElementById('video_form').submit();
        }

        function formsbmt1() {
            if ($('#mileage_input').val() == '') {
                $("#mileage_input").css("border-color", 'red');
                return;
            } else if ($('#mileage_input').val() < 1) {
                $("#mileage_input").css("border-color", 'red');
                return;
            } else {
                $("#mileage_input").css("border-color", '#e4e6ef');
            }
            if ($('#mileage_pic').val() == '') {
                $("#mileage_pic").css("border-color", 'red');
                return;
            } else {
                $("#mileage_pic").css("border-color", '#e4e6ef');
            }

            $('#loader1').removeClass('d-none');
            document.getElementById('video_form1').submit();
        }


        function get_start_details(campaign_id, driver_id) {
            // $(".campee_id").text(campaign_id)
            // $('.btn-text-primary').attr('onclick=getCampaignId1('+campaign_id+')');
            // $('#getCampaignId1').attr('onclick=getCampaignId1('+campaign_id+')');

            $("#resend_mile_btn_show").addClass('d-none')
            $(".campaign_idd").val(campaign_id)
            // alert(campaign_id)

            var data = {
                'campaign_id': campaign_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('user.getStickerDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != 0) {
                        $("#miles-comment").html('');
                        $("#sticker_img1").attr('src', globalPublicPath + '/' + result.sticker_picture)
                        $("#sticker_video1").attr('src', globalPublicPath + '/' + result.sticker_video)
                        if (result.campain_start_date == null) {
                            $("#strt_date").text('Not available')
                        } else {
                            $("#strt_date").text(result.campain_start_date)
                        }
                        if (result.mileage_picture == null) {
                            $(".mile_pic").addClass('d-none')
                        } else {
                            $(".mile_pic").removeClass('d-none')
                            $("#mile_pic").attr('src', globalPublicPath + '/' + result.mileage_picture)
                        }
                        if (result.starting_miles == null) {
                            $("#strt_miles").text('Not available');
                        } else {
                            $("#strt_miles").text(result.starting_miles)
                        }
                        if (result.applied_campaign.documents_status != null) {
                            $("#stats").text(result.applied_campaign.documents_status);
                        }
                        if (result.applied_campaign.documents_feedback != null || result.applied_campaign.document_sticker_feedback != null) {
                            var milesfeeedback='';
                            var stickerfeeedback='';
                            if(result.applied_campaign.documents_feedback==null){
                                milesfeeedback='Not available';
                            }else{
                                milesfeeedback=result.applied_campaign.documents_feedback
                            }
                            if(result.applied_campaign.document_sticker_feedback==null){
                                stickerfeeedback='Not available';
                            }else{
                                stickerfeeedback=result.applied_campaign.document_sticker_feedback
                            }
                            html = "<table\n" +
                                " class=\"table main-table table-head-custom table-head-bg table-borderless table-vertical-center table-bordered\">\n" +
                                "   <tr>\n" +
                                " <td>Sticker Feedback</td>\n" +
                                " <td >" + stickerfeeedback+ "</td>\n" +
                                "  <td>Miles Feedback</td>\n" +
                                " <td >" + milesfeeedback + "</td>\n" +
                                " </tr>\n" +
                                "</table>"
                            $("#miles-comment").html(html)
                        }

                        if (result.applied_campaign.documents_status == 'Waiting for resend miles') {

                            $("#mile_resend_button_click").removeClass('d-none')
                        }else{
                            $("#mile_resend_button_click").addClass('d-none')
                        }
                        if (result.applied_campaign.documents_status == 'Waiting for resend sticker') {
                            // $('#getCampaignId1').attr('onclick=getCampaignId12('+campaign_id+')');
                            // $('#mile_resend_button_click').attr('onclick=getCampaignId1('+campaign_id+')');
                            $("#sticker_resend_button_click").removeClass('d-none')

                        }else{
                            $("#sticker_resend_button_click").addClass('d-none')
                        }

                        // $("#drvwr_name").text(driver_name)
                        $("#campee_id").text(result.car_id)
                        $('#get_details_modal1').modal('show');
                    } else {
                        toastr.error('No record found', 'Error Occured');
                    }
                }
            })
        }


        {{--function get_start_details(campaign_id,driver_id){--}}
        {{--    var data = {--}}
        {{--        'campaign_id': campaign_id,--}}
        {{--        'driver_id': driver_id,--}}
        {{--    };--}}
        {{--    $.ajax({--}}
        {{--        url: '{{route('user.getStickerDetails')}}',--}}
        {{--        type: "GET",--}}
        {{--        data: data,--}}
        {{--        success: function (result) {--}}
        {{--            // console.log(result);--}}
        {{--            if (result != 0) {--}}
        {{--                $("#sticker_img1").attr('src',globalPublicPath+'/'+result.sticker_picture)--}}
        {{--                $("#sticker_video1").attr('src',globalPublicPath+'/'+result.sticker_video)--}}
        {{--                // $("#mile_pic").attr('src',globalPublicPath+'/'+result.mileage_picture)--}}
        {{--                // alert(result.strt_date)--}}
        {{--                if(result.campain_start_date==null){--}}
        {{--                    $("#strt_date").text('Not available')--}}
        {{--                }--}}
        {{--                else{--}}
        {{--                    $("#strt_date").text(result.campain_start_date)--}}
        {{--                }--}}
        {{--                if(result.mileage_picture==null){--}}
        {{--                    $(".mile_pic").addClass('d-none')--}}
        {{--                }--}}
        {{--                else{--}}
        {{--                    $(".mile_pic").removeClass('d-none')--}}
        {{--                    $("#mile_pic").attr('src',globalPublicPath+'/'+result.mileage_picture)--}}
        {{--                }--}}
        {{--                if(result.starting_miles==null){--}}
        {{--                    $("#strt_miles").text('Not available');--}}
        {{--                }--}}
        {{--                else{--}}

        {{--                    $("#strt_miles").text(result.starting_miles)--}}
        {{--                }--}}


        {{--                // $("#drvwr_name").text(driver_name)--}}
        {{--                $("#campee_id").text('CMP-'+result.campaign_id)--}}

        {{--                $('#get_details_modal1').modal('show');--}}
        {{--            } else {--}}
        {{--                toastr.error('No record found', 'Error Occured');--}}
        {{--            }--}}
        {{--        }--}}
        {{--    })--}}
        {{--}--}}






        function getCampaignId(id) {
            $('.campaign_idd').val(id);
            $('.sticker-pic').css('border-color','#E4E6EF');
            $('.sticker-video').css('border-color','#E4E6EF');
            $('.sticker_pic_error').css('border-color','#E4E6EF');
            $('.sticker_video_error').css('border-color','#E4E6EF');
            $('.sticker-video').val('')
            $('.sticker-pic').val('')
            $("#editModal").modal('show');
            // $("#get_details_modal1").modal('hide');
        }
        function getCampaignId12(id) {

            $('.sticker-pic').css('border-color','#E4E6EF');
            $('.sticker-video').css('border-color','#E4E6EF');
            $('.sticker_pic_error').css('border-color','#E4E6EF');
            $('.sticker_video_error').css('border-color','#E4E6EF');
            $('.sticker-video').val('')
            $('.sticker-pic').val('')
            $("#editModal").modal('show');
            $("#get_details_modal1").modal('hide');
        }

        function getCampaignId1(id) {
            $('#campain-id').val(id);
            $("#editModal1").modal('show');
            $("#get_details_modal1").modal('hide');
        }
        function getCampaignId10() {
           // alert($('#campee_id').text());
            $('.campaien_id').val($('#campee_id').text());
            $("#editModal1").modal('show');
            $("#get_details_modal1").modal('hide');
        }

        function status(status_id, camp_id) {
            $("#camp_id").html('CMP-' + camp_id);
            $("#stat1").val(status_id)
            $("#camp-id").val(camp_id)
        }

        function feedback(camp_id) {
            $("#cmp_id").html('CMP-' + camp_id);
        }

        function viewDetail(campaign) {
            console.log(campaign);
            // $("#cmp_id").html('CMP-'+camp_id);
        }


        function del() {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }
        function isEmptyOrSpaces(str){
            return str === null || str.match(/^ *$/) !== null;
        }
        function reject_feedback() {
            if ($('#reject_feedback_field').val() == '') {
                $('#reject_feedback_error').text('Please fill this field');
                return;
            }if ($('#reject_feedback_field').val().length<9) {
                $('#reject_feedback_error').text('Address is too short');
                return;
            }if (isEmptyOrSpaces($('#reject_feedback_field').val())) {
                $('#reject_feedback_error').text('Invalid message');
                return;
            } else {

                $('#loader1').removeClass('d-none');
                var camp_id = $('#campen_id').text()
                $('#reject_feedback_error').text('');
                var data = {
                    'campaign_id': camp_id,
                    'comment': $('#reject_feedback_field').val(),
                };
                // $('#loader1').removeClass('d-none');
                $.ajax({
                    url: '{{route('user.driverRejectCourier')}}',
                    type: "GET",
                    data: data,
                    success: function (result) {
                        if (result == '1') {
                            $('#accept_courier').hide('2000');
                            $('#reject_btn').hide('2000');
                            toastr.success('Message sent successfully', 'Action successfull');
                            $('#loader1 ').addClass('d-none');
                            setTimeout(function () {
                                location.reload();
                                ;
                            }, 700);
                        } else {
                            $('#loader1').removeClass('d-none');
                            toastr.error('Try later', 'Error occured');
                        }
                    },
                })
                return;
            }
        }

        $('#not_received_file').bind('change', function () {
            if ((this.files[0].size / 1024 / 1024) > 5) {
                $('#not_received_file').val('');
                $('#not_received_file_error').text('Max file size must not be greater than 5mb\'s');
            } else {
                $('#not_received_file_error').text('');
            }
        });

        function reject_feedback1() {
            if ($('#reject_courier_field').val() == '') {
                $('#reject_courier_field_error').text('Please fill this field');
                return;
            }else if($('#not_received_file').val() == ''){
                $('#not_received_file_error').text('Please fill this field');
                return;
            }
            else {
                // $('.campen_id2').val($('#campen_id2').text())

                $('#loader1').removeClass('d-none');
                var camp_id = $('#campen_id2').text()
                $('#reject_courier_field_error').text('');
                $('#not_received_file_error').text('');
                $('.btn33').hide();
                $('#loader1').removeClass('d-none');
                document.getElementById('form-rejected').submit();

            }
        }


        function courier_details(courier_data, camp_id, campaign) {

            campaign=JSON.parse(campaign)
            courier_data=JSON.parse(courier_data)
// console.log('camp_id',camp_id);
            $('.couier_not_received').addClass('d-none')
            $('.address_confiramation').removeClass('d-none')
            $('.feed').text('');
            $('.feed_text').text('');
            $('#reject_feedback_error').text('');
            $('#reject_btn').addClass('id_' + camp_id);
            $('#accept_courier').addClass('id_' + camp_id);

            if (campaign.address_acknowledged == '1' ){
                 $('.id_' + camp_id).hide();
                if (campaign.courier_driver_feedback !== null) {
                    $('.feed').text('My feedback:');
                    $('.feed_text').text(campaign.courier_driver_feedback);
                }
            }   if (campaign.courier_status == 'Courier Received'){
                $('.id_' + camp_id).hide();
            }  else if (campaign.courier_status == 'Courier not received'){
                  $('.id_' + camp_id).hide();
                  $('.couier_not_received').show();
                  $('.couier_not_received').removeClass('d-none');
                  $('.not_received_comment').text(campaign.courier_reject_message);
                var re = /(?:\.([^.]+))?$/;
                if(re.exec(campaign.courier_rejected_image)[1]=='pdf'){
                    $('.not_received_pic').html("<a target='_blank' href='"+globalPublicPath2+'/'+campaign.courier_rejected_image+"' ><img src='"+globalPublicPath2+'/courrier_reject.png'+"' height='200px'/></a>");
                }else{
                    var img = globalPublicPath2+'/'+campaign.courier_rejected_image;
                    $('.not_received_pic').html("<img src='"+img+"' height='250px'/>");
                }

            } else if (campaign.courier_status === 'Waiting for address confirmation') {
                $('.address_confiramation').addClass('d-none')
                $('.id_' + camp_id).show();
                if (campaign.courier_driver_feedback !== null) {
                    $('.feed').text('My feedback:');
                    $('.feed_text').text(campaign.courier_driver_feedback);
                }
            } else if (campaign.courier_status === 'Address resend request') {
                $('.address_confiramation').addClass('d-none')
                $('.id_' + camp_id).hide();
                $('.feed').text('My feedback:');
                $('.feed_text').text(campaign.courier_driver_feedback);
            }

            $('.url').text(courier_data.url);
            $('#campen_id').text(camp_id);
            $('.address').text(courier_data.address);
            $('.courier_company').text(courier_data.courier_company);
            $('.tracking_id').text(courier_data.tracking_id);
            $("#CourierModal").modal('show');
        }
        function courier_details1(courier_data, camp_id, campaign) {
            campaign=JSON.parse(campaign)
            courier_data=JSON.parse(courier_data)
            $('.feed').text('');
            $('.campeee_id').val(camp_id);
            $('.feed_text').text('');
            $('#reject_feedback_error').text('');
            $('#reject_courier_btn').addClass('id_' + camp_id);
            $('#courier_received').addClass('id_' + camp_id);

            if (campaign.courier_status == 'Mail Acknowledged') {
                $('.id_' + camp_id).hide();
                if(campaign.courier_reject_message !== null) {
                    $('.feed').text('My feedback:');
                    $('.feed_text').text(campaign.courier_reject_message);
                }

            } else if (campaign.courier_status === 'Waiting for address confirmation') {
                $('.id_' + camp_id).show();
                if (campaign.courier_driver_feedback !== null) {
                    $('.feed').text('My feedback:');
                    $('.feed_text').text(campaign.courier_driver_feedback);
                }
            } else if (campaign.courier_status === 'Courier resend request') {
                $('.id_' + camp_id).hide();
                $('.feed').text('My feedback:');
                $('.feed_text').text(campaign.courier_driver_feedback);
            }

            $('.url').text(courier_data.url);
            $('.campen_id2').text(camp_id);
            $('.address').text(courier_data.address);
            $('.courier_company').text(courier_data.courier_company);
            $('.tracking_id').text(courier_data.tracking_id);
            $("#CourierReceiveModal").modal('show');
        }


        function accept_courier() {
            var camp_id = $('#campen_id').text();
            var data = {
                'campaign_id': camp_id,
            };
            $('#loader1').removeClass('d-none');
            $.ajax({
                url: '{{route('user.driverApproveCourier')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {

                        $('#accept_courier').hide('2000');
                        $('#accept_courier').hide('2000');
                        toastr.success('Acknowledged successfully', 'Action successfull');
                        $('#loader1 ').addClass('d-none');
                        location.reload();
                    } else {
                        toastr.error('Try later', 'Errror occured');
                    }
                },

            })

        }

        function accept_courier1() {

            var camp_id = $('.campen_id2').text();
            // alert(camp_id);
            // return;
            var data = {
                'campaign_id': camp_id,
            };
            $('#loader1').removeClass('d-none');
            $.ajax({
                url: '{{route('user.driverApproveCourierReceived')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {
                        $('#courier_received').hide('2000');
                        $('#reject_courier_btn').hide('2000');
                        toastr.success('Updated successfully', 'Action successfull');
                        $('#loader1 ').addClass('d-none');
                        location.reload();
                    } else {
                        toastr.error('Try later', 'Errror occured');
                    }
                },

            })

        }


        $(document).on('click', '#toggleSearchForm', function () {
            $('#table-search-form').slideToggle();
        });


        @if (\Session::has('success'))
        toastr.success('{!! \Session::get('success') !!}', 'Submitted successfull');
        @endif
        {{--    @if (\Session::has('error'))--}}
        {{--        toastr.error(' Sorry. User not created', 'User Not Created');--}}
        {{--    @endif--}}
        @if (\Session::has('updated'))
        toastr.success('{!! \Session::get('updated') !!}', 'Updated');
        @endif
        {{--    @if (\Session::has('deleted'))--}}
        {{--        toastr.warning('{!! \Session::get('deleted') !!}', 'User Deleted');--}}
        {{--    @endif--}}


        function documentsCompleteModel(camp_id, driver_id,name) {

            $('#loader').removeClass('d-none');

            var data = {
                'campaign_id': camp_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('user.getDriverCompletedDocuments')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != '0') {

                        // console.log('result',result);
                        $('#driverrr_id').val(driver_id);
                        // $('#cmpp').val(camp_id);
                        $('#drriver').text(name);
                        $('#camp_id12').text(camp_id);

                        // sticker_pic='/images/driver-documents/'+result['start'].sticker_picture;
                        sticker_pic = "{{url('/images/driver-documents/')}}/" + result['start'].sticker_picture;
                        $('#start_sticker_pic').attr('src', sticker_pic);
                        mileage_pic = "{{url('/images/driver-documents/')}}/" + result['start'].mileage_picture;
                        $('#start_mileage_pic').attr('src', mileage_pic);
                        $('#start_miles_no').text(result['start'].starting_miles);
                        $('#start_date').text(result['start'].campain_start_date);

                        sticker_pic = "{{url('/images/driver-documents/')}}/" + result['complete'].sticker_picture;
                        $('#completed_sticker_pic').attr('src', sticker_pic);
                        mileage_pic = "{{url('/images/driver-documents/')}}/" + result['complete'].mileage_picture;
                        $('#completed_mileage_pic').attr('src', mileage_pic);
                        $('#completed_miles_no').text(result['complete'].mileage);
                        $('#completed_date').text(result['complete'].campaign_finish_date);
                        $('#miles_covered').val(result['covered_miles']);
                        $('#days_covered').val(result['total_days']);
                        if(result['campaign'].status=="Completed"){


                            if(result['complete'].admin_miles!=null){
                                $('#miles_covered').val(result['complete'].admin_miles);
                            }
                        }
                        // console.log('acttt,',(result['price'].price_per_mile*0.8).toFixed(2));
                        if (result['price']) {
                            // $('#actual_amount').val(result['price'].price_per_mile);
                            $('#giving_amount').val((result['price'].price_per_mile*0.8).toFixed(2));
                        } else {
                            $('#approvedd').attr('disabled', 'disabled');
                        }
                        if (result['campaign']!=null) {
                            if (result['campaign'].amount_gives!=null) {
                                $('#giving_amount').val(result['campaign'].amount_gives);
                            }



                        }
                    } else {
                        toastr.error('No data exist against this driver', 'No data found');
                    }
                },
            })
            $('#loader').addClass('d-none');
            $("#DriverCompleteDocuments").modal();

        }



    </script>

@endsection
