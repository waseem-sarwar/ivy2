@php
    $role=Auth::user()->role_id;

@endphp
@if($role=='4')
    <?php $layout = 'layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout = 'layout.driver'; ?>
@elseif($role=='2')
    <?php $layout = 'layout.default'; ?>
@elseif($role=='1')
    <?php $layout = 'layout.admin'; ?>
@endif
@extends($layout)
@section('styles')
    <link href="{{ asset('css/pages/wizard/wizard-3.css') }}" rel="stylesheet" type="text/css"/>
@endsection
{{-- Content --}}
@section('content')

    @if ($errors->any())
        <div class="alert alert-danger" style="background-color: #e0afb4; ">
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="float-left">{{ $error }}</li><br>
                @endforeach
            </ul>
        </div>
    @endif
    {{-- Dashboard 1 --}}
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Become A Driver </h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a class="text-muted">Enrollment</a>
                            </li>

                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->

                <!--end::Toolbar-->
            </div>
        </div>
        <!--end::Subheader-->
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <div class="card card-custom">
                    <div class="card-body p-0">
                        <!--begin: Wizard-->
                        <div class="wizard wizard-3" id="kt_wizard_v3" data-wizard-state="step-first"
                             data-wizard-clickable="true">
                            <!--begin: Wizard Nav-->
                            <div class="wizard-nav">
                                <div class="wizard-steps px-8 py-8 px-lg-15 py-lg-3">
                                    <!--begin::Wizard Step 1 Nav-->
                                    <div class="wizard-step" data-wizard-type="step" data-wizard-state="current">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span></span>Driver Personal Info</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 1 Nav-->
                                    <!--begin::Wizard Step 2 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span></span>Car Info</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 2 Nav-->
                                    <!--begin::Wizard Step 3 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span></span>Drive a car location</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 3 Nav-->
                                    <!--begin::Wizard Step 4 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span></span>Upload Documents</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 4 Nav-->
                                    <!--begin::Wizard Step 5 Nav-->
                                    <div class="wizard-step" data-wizard-type="step">
                                        <div class="wizard-label">
                                            <h3 class="wizard-title">
                                                <span></span>Become a Driver</h3>
                                            <div class="wizard-bar"></div>
                                        </div>
                                    </div>
                                    <!--end::Wizard Step 4 Nav-->
                                </div>
                            </div>
                            <!--end: Wizard Nav-->
                            <!--begin: Wizard Body-->
                            <div class="row justify-content-center py-10 px-8 py-lg-12 px-lg-10">
                                <div class="col-xl-12 col-xxl-7">
                                    <!--begin: Wizard Form-->
                                    {{--                                    <form class="form" action="createenrollement" id="kt_form">--}}
                                    <form method="POST" action="{{ route('user.enrollment.create') }}"
                                          enctype="multipart/form-data" class="form multi_step_form" id="kt_forma">
                                    @csrf

                                    <!--begin::Wizard Step 1-->
                                        <div class="pb-5 step_1" data-wizard-type="step-content"
                                             data-wizard-state="current">
                                            <h3 class="mb-10 font-weight-bold text-dark">Driver Personal Info</h3>
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <label>First Name*</label>
                                                <input type="text" required
                                                       class="form-control form-control-solid required  form-control-lg"
                                                       value="{{old('first_name')}}" name="first_name" required
                                                       placeholder="First Name"/>
                                                @if($errors->has('first_name'))
                                                    <span class="form-text text-danger"> {{$errors->first('first_name')}}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Last Name*</label>
                                                <input type="text"
                                                       class="form-control form-control-solid form-control-lg"
                                                       value="{{old('last_name')}}" name="last_name" required
                                                       placeholder="Last Name" value=""/>
                                                @if($errors->has('last_name'))
                                                    <span
                                                        class="form-text text-danger"> {{$errors->first('last_name')}}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Email*</label>
                                                <input type="email"
                                                       class="form-control form-control-solid form-control-lg"
                                                       value="{{old('email')}}" name="email" placeholder="Email" required autocomplete="off"
                                                />
                                                @if($errors->has('email'))
                                                    <span
                                                        class="form-text text-danger"> {{$errors->first('email')}}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>Password*</label>
                                                <input type="password"
                                                       class="form-control form-control-solid form-control-lg"
                                                       value="{{old('passsword')}}" name="passsword"
                                                       placeholder="Password"  required autocomplete="off"/>
                                                @if($errors->has('passsword'))
                                                    <span
                                                        class="form-text text-danger"> {{$errors->first('passsword')}}</span>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label>DOB</label>
                                                <input type="text" data-date-end-date="-18Y"
                                                       class="form-control form-control-solid form-control-lg" autocomplete="off" onkeydown="return false;"
                                                       value="{{old('dob')}}" name="dob" id="dob"
                                                       placeholder="Date of Birth"/>
{{--                                                <input type="date"--}}
{{--                                                       class="form-control form-control-solid form-control-lg dob3"--}}
{{--                                                       value="{{old('dob')}}" name="dob" placeholder="Date of Birth" required--}}
{{--                                                       min="01-01-1955" max="{{date('Y-m-d')}}"/>--}}
                                                {{--                                                <span class="form-text text-muted">Enter your DOB</span>--}}
                                            </div>
                                        {{--                                                <span class="form-text text-muted">Enter your DOB</span>--}}



                                        <!--end::Input-->
                                            <div class="row">

                                                {{--                                                <div class="col-xl-6">--}}
                                                {{--                                                    <!--begin::Select-->--}}
                                                {{--                                                    <div class="form-group">--}}
                                                {{--                                                        <label>Country</label>--}}
                                                {{--                                                        <select name="country_personal" class="form-control  form-control-lg">--}}
                                                {{--                                                            <option value="" disabled="disabled">Choose country</option>--}}
                                                {{--                                                            @foreach($countries as $country)--}}
                                                {{--                                                               <option {{($country->id=='2')?'selected':'disabled' }} value="{{$country->id}}">{{$country->name}}</option>--}}
                                                {{--                                                            @endforeach--}}
                                                {{--                                                        </select>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                    <!--end::Select-->--}}
                                                {{--                                                </div>--}}

                                                {{--                                                <div class="col-xl-6">--}}
                                                {{--                                                    <!--begin::Input-->--}}
                                                {{--                                                    <div class="form-group">--}}
                                                {{--                                                        <label>Zip Code</label>--}}
                                                {{--                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="zip_code" placeholder="Zip Code" value="{{old('zip_code')}}"  />--}}
                                                {{--                                                        --}}{{--                                                        <span class="form-text text-muted">Please enter your Zip Code.</span>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                    <!--end::Input-->--}}
                                                {{--                                                </div>--}}
                                                {{--                                                <div class="col-xl-6">--}}
                                                {{--                                                    <!--begin::Input-->--}}
                                                {{--                                                    <div class="form-group">--}}
                                                {{--                                                        <label>City</label>--}}
                                                {{--                                                        <select name="driver[city]" class="form-control form-control-solid form-control-lg">--}}
                                                {{--                                                            <option value="">Select</option>--}}
                                                {{--                                                            <option value="Washington">Washington</option>--}}
                                                {{--                                                            <option value="North America">North America</option>--}}
                                                {{--                                                            <option value="canada" selected >canada</option>--}}
                                                {{--                                                        </select>--}}
                                                {{--                                                    </div>--}}
                                                {{--                                                    <!--end::Input-->--}}
                                                {{--                                                </div>--}}
                                                {{--                <div class="col-xl-12">
                                                                    <!--begin::Input-->
                                                                    <div class="form-group">
                                                                        <label>Street Address</label>
                                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="street" placeholder="Street address"  value="{{old('street')}}" />
                --}}{{--                                                        <span class="form-text text-muted">Please enter your street address.</span>--}}{{--
                                                                    </div>
                                                                    <!--end::Input-->
                                                                </div>--}}

                                                <div class="col-xl-12">
                                                    <!--begin::Input-->
                                                    <div class="form-group">
                                                        <label>Mailing Address*</label>
                                                        <textarea
                                                            class="form-control form-control-solid form-control-lg"
                                                            name="billing_address" autocomplete="off"
                                                            placeholder="Mailling address">{{old('billing_address')}} </textarea>
                                                        {{--                                                        <input type="textarea" class="form-control form-control-solid form-control-lg" name="billing_address" placeholder="Mailling address" value="{{old('billing_address')}}" />--}}
                                                        @if($errors->has('billing_address'))
                                                            <span class="form-text text-danger"> Mailing address is required</span>
                                                        @endif
                                                    </div>
                                                    <!--end::Input-->
                                                </div>

                                            </div>
                                            {{--                                            <div class="row">--}}
                                            {{--                                                <div class="col-xl-12">--}}
                                            {{--                                                    <!--begin::Input-->--}}
                                            {{--                                                    <div class="form-group">--}}
                                            {{--                                                        <label>Location</label>--}}
                                            {{--                                                        <div class="card-body m-0 p-0">--}}
                                            {{--                                                            <div id="googleMap" class="w-100" style="height:270px;"></div>--}}
                                            {{--                                                        </div>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                    <!--end::Input-->--}}
                                            {{--                                                </div>--}}

                                            {{--                                            </div>--}}
                                        </div>
                                        <!--end::Wizard Step 1-->

                                        <!--begin::Wizard Step 2-->
                                        <div class="pb-5 step_2" data-wizard-type="step-content">
                                        {{--                                            <h4 class="mb-10 font-weight-bold text-dark">Car Info</h4>--}}
                                        <!--begin::Input-->
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Car Maker</label>
                                                        <select name="car_make" class="form-control  form-control-lg"
                                                                onchange="getcar(this.value)" required>
                                                            <option value="" selected disabled>select maker</option>
                                                            @foreach($Cars as $Car)
                                                                <option
                                                                    {{(old('car_make')==@$Car->company_id)?'selected':''}}  value="{{@$Car->company_id}}">{{@$Car->maker->company}}</option>
                                                            @endforeach

                                                        </select>
                                                        @if($errors->has('car_make'))
                                                            <span
                                                                class="form-text text-danger"> {{$errors->first('car_make')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group">
                                                        <label>Car Name:</label>
                                                        <select name="car_model" id="car_model_1" required
                                                                class="form-control car_models" disabled
                                                                onchange="getyear(this.value)">
                                                            <option value="" selected disabled>Select Name</option>
                                                        </select>
                                                        @if($errors->has('car_model'))
                                                            <span class="form-text text-danger " id="interval_error">Car name field is required.</span>
                                                        @endif
                                                        {{----}}
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <div class="form-group car_name">
                                                        <label>Select year:</label>
                                                        <select name=car_year class="form-control garcartype" disabled required
                                                                id="car_year_1" onchange="gettype(this.value)"> >
                                                            <option value="" selected disabled>Select Car Year</option>
                                                        </select>

                                                        @if($errors->has('car_year'))
                                                            <span
                                                                class="form-text text-danger"> {{$errors->first('car_year')}}</span>
                                                        @endif
                                                    </div>

                                                </div>
                                                <div class="col-xl-6">
                                                    <div class="form-group car_model">
                                                        <label>Car Type:</label>
                                                        <select name="car_type" id="car_type_1" class="form-control cartyped" required>
                                                            @if(isset($driver))
                                                                @if($driver->car_typee)
                                                                    <option value="" selected >Choose Car Type</option>
                                                                    <option value="{{$driver->car_typee->id}}" selected>{{$driver->car_typee->name}}</option>
                                                                @endif
                                                            @elseif(isset($types))
                                                                <option value="" selected disabled >Choose Car Type</option>
                                                                @foreach($types as $type)
                                                                    <option value="{{$type->car_type}}">{{$type->carType->name}}</option>
                                                                @endforeach
                                                            @else
                                                                <option value=""  disabled  selected >Choose Car Type</option>
                                                            @endif

                                                        </select>
                                                        <span class="form-text text-danger d-none" id="interval_error">This field is required.</span>
                                                        @if($errors->has('car_type'))
                                                            <span
                                                                class="form-text text-danger"> {{$errors->first('car_type')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label>Upload car photo*</label>
                                                <input type="file"
                                                       class="form-control form-control-solid form-control-lg"
                                                       name="car_photo"/>
                                                <span class="form-text text-muted">Please upload picture.</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload car video*</label>
                                                <input type="file"
                                                       class="form-control form-control-solid form-control-lg"
                                                       name="car_video" placeholder="Car Video"/>
                                                <span class="form-text text-muted">Please upload video.</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload car mileage proof*</label>
                                                <input type="file"
                                                       class="form-control form-control-solid form-control-lg"
                                                       name="mileage_proof"/>
                                                <span class="form-text text-muted">Please upload file.</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Upload car registration proof*</label>
                                                <input type="file"
                                                       class="form-control form-control-solid form-control-lg"
                                                       name="car_registration_proof"
                                                       placeholder="Car registration proof"/>
                                                <span class="form-text text-muted">Please upload file.</span>
                                            </div>
                                        </div>
                                        <!--end::Wizard Step 2-->

                                        <!--begin::Wizard Step 3-->
                                        <div class="pb-5 step_3" data-wizard-type="step-content">
                                            <h4 class="mb-10 font-weight-bold text-dark">Drive a car location</h4>
                                            <!--begin::Select-->
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Select-->
                                                    <div class="form-group">
                                                        <label>Country</label>
                                                        <select name="car_drive_country"
                                                                class="form-control form-control-solid form-control-lg">
                                                            @foreach($countries as $country)
                                                                @if($country->id=='2')
                                                                    <option
                                                                        {{($country->id=='2')?'selected':'disabled' }} value="{{$country->id}}">{{$country->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <!--end::Select-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Select-->
                                                    <div class="form-group">
                                                        <label>State</label>
                                                        <select name="driving_state" required
                                                                class="form-control form-control-solid form-control-lg"
                                                                onchange="getCitiess(this.value);">
                                                            <option selected disabled value="">Choose state</option>
                                                            @foreach($states as $state)
                                                                <option
                                                                    value="{{$state->id}}" {{(old("driving_state") == $state->id ? "selected":"") }}>{{$state->state}}</option>
                                                            @endforeach
                                                        </select>

                                                        @if($errors->has('driving_state'))
                                                            <span
                                                                class="form-text text-danger"> State is required</span>
                                                        @endif
                                                    </div>
                                                    <!--end::Select-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Select-->
                                                    <div class="form-group">
                                                        <label>City</label>
                                                        <select name="car_drive_city" id='cities' required
                                                                class="form-control form-control-solid form-control-lg"
                                                                onchange="getZips(this.value);">
                                                            <option selected disabled value="">Choose City</option>
                                                        </select>

                                                        @if($errors->has('car_drive_city'))
                                                            <span class="form-text text-danger">City is required</span>
                                                        @endif
                                                    </div>
                                                    <!--end::Select-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Select-->
                                                    <div class="form-group">
                                                        <label>Zip</label>
                                                        <select name="car_drive_zip" id='zips' required class="form-control form-control-solid form-control-lg">
                                                            <option selected disabled value="">Choose Zip</option>
                                                        </select>
                                                        @if($errors->has('car_drive_zip'))
                                                            <span class="form-text text-danger">Zip is required</span>
                                                        @endif
                                                    </div>
                                                    <!--end::Select-->
                                                </div>

                                            </div>
                                            {{--                                            <div class="row">--}}
                                            {{--                                                <div class="col-xl-6">--}}
                                            {{--                                                    <!--begin::Input-->--}}
                                            {{--                                                    <div class="form-group">--}}
                                            {{--                                                        <label>Drive location(Zip Code)</label>--}}
                                            {{--                                                        <input type="text" class="form-control form-control-solid form-control-lg" name="drive[zipCode]" placeholder="Zip Code"  />--}}
                                            {{--                                                        <span class="form-text text-muted">Please enter your Zip Code.</span>--}}
                                            {{--                                                    </div>--}}
                                            {{--                                                    <!--end::Input-->--}}
                                            {{--                                                </div>--}}

                                            {{--                                            </div>--}}
                                            <div class="form-group">
                                                <label>Address</label>
                                                <input type="text"
                                                       class="form-control form-control-solid form-control-lg"
                                                       value="{{old('drive[address]')}}" name="drive[address]"
                                                       placeholder="Address"/>
                                                <span class="form-text text-muted">Please enter your address.</span>
                                            </div>


                                        </div>
                                        <!--end::Wizard Step 3-->

                                        <!--begin::Wizard Step 4-->
                                        <div class="pb-5 step_4" data-wizard-type="step-content">
                                            <h4 class="mb-10 font-weight-bold text-dark">Upload Documents</h4>
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <label>Upload driving license</label>
                                                <input type="file"
                                                       class="form-control form-control-solid form-control-lg"
                                                       name="license"/>
                                                <span class="form-text text-muted">Please upload license.</span>
                                            </div>
                                            <!--end::Input-->
                                            <!--begin::Input-->
                                            <div class="form-group">
                                                <label>Upload Government CNIC/Passport</label>
                                                <input type="file"
                                                       class="form-control form-control-solid form-control-lg"
                                                       value="{{ old("cnic")}}" name="cnic"/>
                                                <span class="form-text text-muted">Please upload cnic.</span>
                                            </div>

                                        </div>
                                        <!--end::Wizard Step 4-->

                                        <!--begin::Wizard Step 5-->
                                        <div class="pb-5 step_5" data-wizard-type="step-content">
                                            <!--begin::Section-->
                                            <h4 class="mb-10 font-weight-bold text-dark">Additional Details</h4>
                                            <div class="form-group">
                                                <label>How many hours drive a car?</label>
                                                <select name="how_many_hours_drive"
                                                        class="form-control form-control-solid form-control-lg">
                                                    <option
                                                        {{ (old("how_many_hours_drive") =='7'? "selected":"") }} value="7"
                                                        selected="selected">7hrs/day
                                                    </option>
                                                    <option
                                                        {{ (old("how_many_hours_drive") =='10'? "selected":"") }}  value="10">
                                                        10hrs/day
                                                    </option>
                                                    <option
                                                        {{ (old("how_many_hours_drive") =='12'? "selected":"") }} value="12">
                                                        12hrs/day
                                                    </option>

                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label> Hours drive a month?</label>
                                                <select name="days_drive_in_month" id="days_drive_in_month"
                                                        class="form-control form-control-solid form-control-lg">
                                                    @for ($i = 30; $i > 0; $i--)
                                                        <option
                                                            value="{{$i}}" {{ (old("days_drive_in_month") ==$i? "selected":"")}}>{{$i}}
                                                            days
                                                        </option>
                                                    @endfor
                                                </select>
                                            </div>
                                            <div class="row " style="margin-top: 17px">
                                                <label class="pl-2 ml-2">How Often Drive a Car?</label><br>
                                                <div class="col-sm-1 offset-2">
                                                    <div class="form-group">
                                                        <label>Mon</label>
                                                        <div class="ml-2">
                                                            <input type="checkbox" name="car_info[days][1]" value="Mon">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="form-group">
                                                        <label>Tue</label>
                                                        <div class="ml-2">
                                                            <input type="checkbox" name="car_info[days][2]" value="Tue">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="form-group">
                                                        <label>Wed</label>
                                                        <div class="ml-2">
                                                            <input type="checkbox" name="car_info[days][3]" value="Wed">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="form-group">
                                                        <label>Thur</label>
                                                        <div class="ml-2">
                                                            <input type="checkbox" name="car_info[days][4]"
                                                                   value="Thur">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="form-group">
                                                        <label>Fri</label>
                                                        <div class="">
                                                            <input type="checkbox" name="car_info[days][5]" value="Fri">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="form-group">
                                                        <label>Sat</label>
                                                        <div class="ml-2">
                                                            <input type="checkbox" name="car_info[days][6]" value="Sat">

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-1">
                                                    <div class="form-group">
                                                        <label>Sun</label>
                                                        <div class="ml-2">
                                                            <input type="checkbox" name="car_info[days][7]" value="Sun">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>How often monthly mileage of car?</label>
                                                <select name="how_often_mileage"
                                                        class="form-control form-control-solid form-control-lg">
                                                    <option
                                                        {{ (old("how_often_mileage") =='200'? "selected":"") }} value="200">
                                                        200 Miles
                                                    </option>
                                                    <option
                                                        {{ (old("how_often_mileage") =='300'? "selected":"") }} value="300">
                                                        300 Miles
                                                    </option>
                                                    <option
                                                        {{ (old("how_often_mileage") =='1000'? "selected":"") }} value="1000"
                                                        selected>1000 Miles
                                                    </option>
                                                    <option
                                                        {{ (old("how_often_mileage") =='2000'? "selected":"") }} value="2000">
                                                        2000 Miles
                                                    </option>
                                                    <option
                                                        {{ (old("how_often_mileage") =='3000'? "selected":"") }} value="3000">
                                                        3000 Miles
                                                    </option>
                                                    <option
                                                        {{ (old("how_often_mileage") =='5000'? "selected":"") }} value="5000">
                                                        5000 Miles
                                                    </option>
                                                    <option
                                                        {{ (old("how_often_mileage") =='7000'? "selected":"") }} value="6000">
                                                        7000 Miles
                                                    </option>
                                                    <option
                                                        {{ (old("how_often_mileage") =='10000'? "selected":"") }} value="10000">
                                                        10000 Miles
                                                    </option>

                                                </select>
                                            </div>
                                            <!-- <h6 class="font-weight-bolder mb-3">Current Address:</h6>
                                            <div class="text-dark-50 line-height-lg">
                                                <div>Address Line 1</div>
                                                <div>Address Line 2</div>
                                                <div>Melbourne 3000, VIC, Australia</div>
                                            </div>
                                            <div class="separator separator-dashed my-5"></div> -->
                                            <!--end::Section-->
                                            <!--begin::Section-->


                                        </div>
                                        <!--end::Wizard Step 5-->

                                        <!--begin: Wizard Actions-->
                                        <div class="buttons-wrap d-flex justify-content-between border-top mt-5 pt-10">
                                            <div class="prev-wrap mr-2">
                                                <button type="button" data-wizard-type="action-prev"
                                                        class="btn btn-light-primary btn-prev font-weight-bolder text-uppercase px-5 py-4">
                                                    Previous
                                                </button>
                                            </div>
                                            <div class="next-wrap">
                                                <input type="submit" value="Submit"
                                                       class="btn btn-success btn-submit font-weight-bolder text-uppercase px-5 py-4"
                                                       onclick="loadr();" data-wizard-type="action-submit">
                                                <button type="button" data-wizard-type="action-next"
                                                        class="btn btn-primary btn-next font-weight-bolder text-uppercase px-5 py-4">
                                                    Next
                                                </button>
                                            </div>
                                        </div>
                                        <!--end: Wizard Actions-->

                                    </form>
                                    <!--end: Wizard Form-->
                                </div>
                            </div>
                            <!--end: Wizard Body-->
                        </div>
                        <!--end: Wizard-->
                    </div>
                </div>
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/wizard/wizard-valid-1.js') }}"></script>

    <script>

        // wizard validaiots

        // $('#loader1').removeClass('d-none');
        function loadr() {
            $('#loader1').removeClass('d-none');
            window.localStorage.setItem('city', $('#cities option:selected').text());
            window.localStorage.setItem('zip', $('#zips option:selected').text());
            document.getElementById('kt_forma').submit();
        }
        @if(old('car_drive_city'))
        // $('#cities').removeAttr('disabled')
        $('#cities').html('<option selected  value="{{old('car_drive_city')}}">' + window.localStorage.getItem('city') + '</option>');
        @endif
        @if(old('car_drive_zip'))
        // $('#zips').removeAttr('disabled')
        $('#zips').html('<option selected  value="{{old('car_drive_zip')}}">' + window.localStorage.getItem('zip') + '</option>');

        @endif


        function getCarModel() {
            $('#loader').removeClass('d-none');
            driver_id = $('#driverr_id').val();
            $('#approvedd').attr('disabled', 'disabled');
            $('#approvedd').css('cursor', 'default');
            camp_id = $('#campp_id').val();
            var data = {
                'campaign_id': camp_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: globalPublicPath + '/driverApproveStartDocuments',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {
                        console.log(result);
                        $('#approvedd').text('Approved');
                        toastr.success('Driver request approved successfully', 'Request approved');
                        $('#loader').addClass('d-none');
                    } else {
                        toastr.error('Try later', 'Errror occured');
                    }
                },

            })


        }


        getGuardsDeploymentDataForUnpaid = function (parwest) {
            var globalPublicPath = '{{ URL('/') }}';
            console.log('herfe');
            // var parwest  = $('#parwest_id').val();
            if (parwest) {
                // $("#month_deployment").css("border-color", "gray");
                var data = {
                    'parwest_id': parwest,
                    // 'month': month,
                };
                // let globalPublicPath;
                var object = {
                    'url': globalPublicPath + '/ajaxGetCountries',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                // if (result.data.inventory > 0) {
                //     warningNotificationCustom('Selected Guard   Inventory Must be revoked before clearance   ');
                // }
                // if (result.data.status_name == 'present' ) {
                //     warningNotificationCustom('Selected Guard must be revoked before clearance ');
                // }
                // if (result.data.status_name == 'absent' ||result.data.status_name == 'default' ||result.data.status_name == 'present') {
                //     warningNotificationCustom('Selected Guard Is  '+ result.data.status_name);
                // }else{
                if (result.responseCode == 1) {

                    $('#unpaid_guard_name').val(result.data.name);
                    $('#unpaid_status').val(result.data.status_name);
                    $('#unpaid_type').val(result.data.designation_name);
                    $('#unpaid_current_location').val(result.data.location);
                    $('#unpaid_loan').val(result.data.loan);

                    successNotificationCustom(result.message);
                }
                if (result.responseCode == 2) {
                    warningNotificationCustom(result.message);
                }

                // }


            } else {
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        }

        getGuardsStatusDataForUnpaid = function (month) {
            var parwest = $('#parwest_id').val();
            if (month) {
                console.log('month', month);
                // $("#month_deployment").css("border-color", "gray");
                var data = {
                    'month': month,
                    'parwest_id': parwest,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetailsUnpaidSalaryExport',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                // if (result.data.inventory > 0) {
                //     warningNotificationCustom('Selected Guard   Inventory Must be revoked before clearance   ');
                // }
                // if (result.data.status_name == 'present' ) {
                //     warningNotificationCustom('Selected Guard must be revoked before clearance ');
                // }
                // if (result.data.status_name == 'absent' ||result.data.status_name == 'default' ||result.data.status_name == 'present') {
                //     warningNotificationCustom('Selected Guard Is  '+ result.data.status_name);
                // }else{
                if (result.responseCode == 1) {

                    // $('#unpaid_guard_name').val(result.data.name);
                    $('#salary_status').val(result.data.status);
                    if (result.data.status == 'Paid') {

                        $('#export_unpaid').attr('disabled', 'disabled');

                    }
                    // $('#unpaid_type').val(result.data.designation_name);
                    // $('#unpaid_current_location').val(result.data.location);
                    // $('#unpaid_loan').val(result.data.loan);

                    // successNotificationCustom(result.message);
                }
                // if(result.responseCode == 2){
                //     warningNotificationCustom(result.message);
                // }

                // }


            } else {
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        }

        getSupervisorsAgainstManagerOnExport = function (manager_id) {

            if (manager_id != 0) {

                var data = {
                    'manager_id': manager_id,
                };
                var object = {
                    'url': globalPublicPath + '/client/getSupervisorsAgainstManager',
                    'type': 'POST',
                    'data': data
                };


                var result = centralAjax(object);

                var html = '<option value="0" selected>--Select Supervisor--</option>';
                if (result.responseCode == 1) {
                    $.each(result.data, function (key, value) {
                        html += '<option value="' + value.id + '">' + value.name + '</option>';
                    });

                }
                // $('#supervisor').removeAttr('disabled');
                $('#supervisor').empty().append(html);
                $('#supervisor').selectpicker('refresh');

                if (result.responseCode == 1) {
                    console.log(result.data[0].id);
                } else {
                    // errorNotificationCustom(result.message);
                }

            } else {
                errorNotificationCustom('please select a manager before continue');
            }

        };

        getManagersAgainstRegionOnExport = function (region_id) {

            if (region_id != 0) {

                var data = {
                    'region_id': region_id,
                };
                var object = {
                    'url': globalPublicPath + '/client/getManagersAgainstRegion',
                    'type': 'POST',
                    'data': data
                };


                var result = centralAjax(object);

                var html = '<option value="0" selected>--Select Manager--</option>';
                if (result.responseCode == 1) {
                    $.each(result.data, function (key, value) {
                        html += '<option value="' + value.id + '">' + value.name + '</option>';
                    });

                }
                // $('#supervisor').removeAttr('disabled');
                $('#managers').empty().append(html);
                $('#managers').selectpicker('refresh');

                if (result.responseCode == 1) {
                    console.log(result.data[0].id);
                } else {
                    // errorNotificationCustom(result.message);
                }

            } else {
                errorNotificationCustom('please select a manager before continue');
            }

        };

        $('#extrahour_guards_client').change(function () {
            // $this.val();
            var client_id = $('#extrahour_guards_client').val();
            console.log(client_id);
            var data = {
                'client_id': client_id,
            };
            var object = {
                'url': globalPublicPath + '/guard/getClientBranchesLoan',
                'type': 'GET',
                'data': data,
            };
            $('#extrahour_guards_branch').html('');
            $('#extrahour_guards_branch').append($('<option>', {
                value: 0,
                text: '--Select Branch--',
            }));
            var result = centralAjax(object);
            console.log(result);
            $.each(result.data.branches, function (keys, values) {
                console.log(values);
                // $.each(values, function (key, value) {

                if (values.is_active == 1) {
                    $('#extrahour_guards_branch').append($('<option >', {
                        value: values.id,
                        text: values.name,

                    }));

                }
                // });


                // if(value.id == branchId){
                //     $("#branch_id_on_user_profile").val(value.id);
                //     console.log("branched value selected through jquery ajax");
                // }


            });
        });

        // populate managers list before salary genrate

        $("#genrate_salary").click(function (e) {
            e.preventDefault();
            var region = $('#regions').val();
            // var month = $('#salary_month').val();
            var markup = "";
            markup += '<tr >' +
                '<td ></td>' +
                '<td ></td>' +

                '</tr>';
            $("#salaryfinalize").append(markup);
            $("#finalizelist").append(markup);
            if (region == "") {
                warningNotificationCustom('please select region ');
            }
            if (region != "") {
                successNotificationCustom(" calculating loans ......", 1000);
                var data = {
                    'region': region,
                    // 'month': month,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getFinalisedStatus',
                    'type': 'POST',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                if (result.post_status == true) {
                    errorNotificationCustom(" Region already posted for this month ");

                }
                if (result.post_status == false) {
                    warningNotificationCustom("Please Finalised loans by the following Users   ", 1000);
                    $.each(result.users, function (index, value) {
                        console.log(index + ": " + value);
                        var markup = "";
                        markup += '<tr id="history">' +
                            '<td >' + value.name + '</td>' +
                            '<td >' + 'Pending !!' + '</td>' +

                            '</tr>';
                        $("#salaryfinalize").append(markup);
                    });
                    $.each(result.finalUsers, function (index, value) {
                        console.log(index + ": " + value);
                        var markup = "";
                        markup += '<tr id="finalizelistitem">' +
                            '<td >' + value.name + '</td>' +
                            '<td >' + 'Finalised :' + '</td>' +

                            '</tr>';
                        $("#finalizelist").append(markup);
                    });
                }
                if (result.genrate_status == false) {
                    $("#salary-form").submit();
                }

            }

            // if(month == "" ){
            //     warningNotificationCustom('please select month ');
            // }

            console.log('slary genrated button clicked ', "region " + region + "   " + month);

        });

        $('#clearance-status').change(function () {
            // $this.val();
            var sel = $('#clearance-status').val();
            console.log(sel);
            if (sel == 2) {

                $('#clearance-image-div').css('display', 'none');
            } else {
                $('#clearance-image-div').css('display', 'block');
            }

        });

        getGuardsData = function (parwest_id) {
            var month = $("#extrahour_month").val();
            console.log('month ', month)
            if (month) {
                var data = {
                    'parwest_id': parwest_id,
                    'month': month,
                };
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetails',
                    'type': 'GET',
                    'data': data,
                };

                var result = centralAjax(object);
                console.log(result);
                if (result.responseCode == 1) {

                    // $('#guard_name').val(result.data.guard[0].name);
                    // $('#guards_phone').val(result.data.guard[0].contact_no);
                    // $('#guards_current_supervisor').val(result.data.branch_supervisor);
                    // $('#current_deployment').val(result.data.branch_name + ''+result.data.client_name);
                    // $('#deployment_days').val(result.data.days);
                    // console.log(result.data.guard);
                    // $('#guardLoanHistory').css('display', 'block');
                    // console.log('loan data ', result.data.loan[0]);
                    // $.each(result.data.loan, function (index, value) {
                    //     console.log(index + ": " + value);
                    //     var markup = "";
                    //     markup += '<tr id="history">' +
                    //         '<td >' + value.parwest_id + '</td>' +
                    //         '<td >' + value.guard_name + '</td>' +
                    //         '<td >' + value.current_deployment + '</td>' +
                    //         '<td >' + value.deployment_days + '</td>' +
                    //         '<td >' + value.guards_phone + '</td>' +
                    //         '<td >' + value.guards_current_supervisor + '</td>' +
                    //         '<td >' + value.amount_paid + '</td>' +
                    //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                    //         '<td >' + value.payment_location + '</td>' +
                    //         '<td >' + value.slip_number_loan + '</td>' +
                    //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                    //         '<td class="table-heading2 finalize">'+
                    //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                    //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                    //         '</a> </button> '+ ' </td>' +
                    //         '</tr>';
                    //     $("#deploymentDetailsTableBody").append(markup);
                    // });
                    //
                    // $('#guards_supervisor_loaned').html('');
                    // $('#guards_supervisor_loaned').append($('<option>', {
                    //     value: 0,
                    //     text: 'ffffff',
                    // }));
                    //
                    // $.each(result.data.supervisors.data, function (key, value) {
                    //
                    //     $('#guards_supervisor_loaned').append($('<option >', {
                    //         value: value.status,
                    //         value: value.id,
                    //         text: value.name,
                    //
                    //     }));
                    //
                    // });
                    successNotificationCustom(result.message);
                }
            } else {
                $("#extrahour_month").css("border-color", "#f56942");
                warningNotificationCustom('Guard Number Not Exists ');
            }

        };

        removeGuardDetails = function () {
            // console.log('hamza');
            //
            // $('#guard_name').val('');
            // $('#guards_supervisor_loaned').html('');
            // $('#guards_supervisor_loaned').empty().append($('<option>', {
            //     value: 0,
            //     text: '--Select Branch--',
            // }));
        };

        getGuardsDeploymentData = function (month) {
            var parwest = $('#extrahour_parwest_id').val();
            if (parwest) {
                console.log('i am here in ');
                $("#extrahour_month").css("border-color", "gray");
                var data = {
                    'parwest_id': parwest,
                    'month': month,
                }
                var object = {
                    'url': globalPublicPath + '/guard/getRecentGuardDetailsForExtraHours',
                    'type': 'GET',
                    'data': data,
                }

                var result = centralAjax(object);
                console.log(result);
                if (result.responseCode == 1) {
                    //
                    $('#extrahour_guard_name').val(result.data.guard.name);
                    $('#extrahour_status').val(result.data.guard.status_name);
                    // $('#guards_phone').val(result.data.guard[0].contact_no);
                    // $('#guards_current_supervisor').val(result.data.branch_supervisor);
                    $('#extrahour_current_location').val(result.data.branch_name + '' + result.data.client_name);
                    $('#extrahour_type').val(result.data.guard_type.name);
                    // console.log(result.data.guard);
                    // $('#guardLoanHistory').css('display', 'block');
                    // console.log('loan data ', result.data.loan[0]);
                    // $.each(result.data.loan, function (index, value) {
                    //     console.log(index + ": " + value);
                    //     var markup = "";
                    //     markup += '<tr id="history">' +
                    //         '<td >' + value.parwest_id + '</td>' +
                    //         '<td >' + value.guard_name + '</td>' +
                    //         '<td >' + value.current_deployment + '</td>' +
                    //         '<td >' + value.deployment_days + '</td>' +
                    //         '<td >' + value.guards_phone + '</td>' +
                    //         '<td >' + value.guards_current_supervisor + '</td>' +
                    //         '<td >' + value.amount_paid + '</td>' +
                    //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                    //         '<td >' + value.payment_location + '</td>' +
                    //         '<td >' + value.slip_number_loan + '</td>' +
                    //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                    //         '<td class="table-heading2 finalize">'+
                    //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                    //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                    //         '</a> </button> '+ ' </td>' +
                    //         '</tr>';
                    //     $("#deploymentDetailsTableBody").append(markup);
                    // });
                    //
                    // $('#guards_supervisor_loaned').html('');
                    // $('#guards_supervisor_loaned').append($('<option>', {
                    //     value: 0,
                    //     text: 'ffffff',
                    // }));
                    //
                    // $.each(result.data.supervisors.data, function (key, value) {
                    //
                    //     $('#guards_supervisor_loaned').append($('<option >', {
                    //         value: value.status,
                    //         value: value.id,
                    //         text: value.name,
                    //
                    //     }));
                    //
                    // });
                    successNotificationCustom(result.message);
                }
                // var data = {
                //     'month': month,
                //     'parwest_id': parwest,
                // };
                // var object = {
                //     'url': globalPublicPath + '/guard/getGuardsDeploymentData',
                //     'type': 'GET',
                //     'data': data,
                // };
                //
                // var result = centralAjax(object);
                // console.log(result);
                // if (result.responseCode == 1) {
                //
                //     // $('#guard_name').val(result.data.guard[0].name);
                //     // $('#guards_phone').val(result.data.guard[0].contact_no);
                //     // $('#guards_current_supervisor').val(result.data.guard[0].guard_supervisor_name);
                //     $('#current_deployment').val('meezan bank');
                //     $('#deployment_days').val('5');
                //     console.log(result);
                //     // $('#guardLoanHistory').css('display', 'block');
                //     // console.log('loan data ', result.data.loan[0]);
                //     // $.each(result.data.loan, function (index, value) {
                //     //     console.log(index + ": " + value);
                //     //     var markup = "";
                //     //     markup += '<tr id="history">' +
                //     //         '<td >' + value.parwest_id + '</td>' +
                //     //         '<td >' + value.guard_name + '</td>' +
                //     //         '<td >' + value.current_deployment + '</td>' +
                //     //         '<td >' + value.deployment_days + '</td>' +
                //     //         '<td >' + value.guards_phone + '</td>' +
                //     //         '<td >' + value.guards_current_supervisor + '</td>' +
                //     //         '<td >' + value.amount_paid + '</td>' +
                //     //         '<td >' + value.loan_paid_to_guard_date + '</td>' +
                //     //         '<td >' + value.payment_location + '</td>' +
                //     //         '<td >' + value.slip_number_loan + '</td>' +
                //     //         '<td >' + value.guards_supervisor_loaned + '</td>' +
                //     //         '<td class="table-heading2 finalize">'+
                //     //         '<button title="Export Excel File" type="button" class="btn btn-primary" onclick="editModel('+value.id+')" style="border-color: #f3f3f3;"> <a style="padding: 0px  !important;">' +
                //     //         '<i class="fa fa-edit finalize-item"  data-id="'+value.id+'" aria-hidden="true" style="font-size: 21px"></i>' +
                //     //         '</a> </button> '+ ' </td>' +
                //     //         '</tr>';
                //     //     $("#deploymentDetailsTableBody").append(markup);
                //     // });
                //
                //     // $('#guards_supervisor_loaned').html('');
                //     // $('#guards_supervisor_loaned').append($('<option>', {
                //     //     value: 0,
                //     //     text: 'ffffff',
                //     // }));
                //
                //     // $.each(result.data.supervisors.data, function (key, value) {
                //     //
                //     //     $('#guards_supervisor_loaned').append($('<option >', {
                //     //         value: value.status,
                //     //         value: value.id,
                //     //         text: value.name,
                //     //
                //     //     }));
                //     //
                //     // });
                //     successNotificationCustom(result.message);
                // }

            } else {
                $("#parwest_id").css("border-bottom-color", "#FFFFFF");
                warningNotificationCustom('Guard Number Not Exists ');

            }

        };

        removeGuardDeploymentDetails = function () {
            // console.log('hamza');

            // $('#guard_name').val('');
            // $('#guards_supervisor_loaned').html('');
            // $('#guards_supervisor_loaned').empty().append($('<option>', {
            //     value: 0,
            //     text: '--Select Branch--',
            // }));
        };

        function myMap() {
            var myLatLng = {lat: 51.50874, lng: -0.120850};
            var mapProp = {
                center: new google.maps.LatLng(51.508742, -0.120850),
                zoom: 5,
                zoomControl: false,
                mapTypeControl: false,

            };
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: 'Hello World!'
            });

        }

        function getZips(city_id) {
            var data = {
                'city_id': city_id,
            };
            var object = {
                'url': '{{route('get.zips')}}',
                'processData': false,
                'contentType': false,
                'cache': false,
                'data': data,
            };
            var result2 = centralAjax(object);
            $('#zips').html('');
            $('#zips').removeAttr('disabled');
            $.each(result2, function (index, value) {
                var zip_code = value['zip'];
                var newOption = new Option(zip_code, zip_code, false, false);
                $('#zips').append(newOption);
            });
            $('#zips').prepend("<option selected value='' disabled='disabled'>Choose zip please</option>");
            $('#zips').append(newOption);
            if (result2.length == 0) {
                $('#zips').html('');
                $('#zips').attr('disabled', 'disable');
                var newOption = new Option('No zip found', '', false, false);
                $('#zips').prepend(newOption);
            }
        }

        function getCitiess(state_id) {
            var data = {
                'state_id': state_id,
            };
            var object = {

                'url': '{{route('get.cities')}}',
                'processData': false,
                'contentType': false,
                'cache': false,
                'data': data,

            }
            var result2 = centralAjax(object);
            console.log(result2);
            $('#cities').html('');
            $('#cities').prepend("<option selected value='' disabled='disabled'>Choose City</option>");
            $.each(result2, function (index, value) {
                var city_id = value['id'];
                var name = value['primary_city'];
                var newOption = new Option(name, city_id, false, false);
                $('#cities').append(newOption);
            });

        }


        $('#dob').datepicker(
            {
                format: 'mm/dd/yyyy',
                startDate: '01/01/1949',
                minDate: '-18Y',

            }
        ).on('changeDate', function(e){
            $(this).datepicker('hide');
        });


        $(function () {
            // document.getElementById("dob").disabled = true;
            var dtToday = new Date();
            var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
            var day = dtToday.getDate();
            var year = dtToday.getFullYear() - 18;
            if (month < 10)
                month = '0' + month.toString();
            if (day < 10)
                day = '0' + day.toString();
            var minDate = year + '-' + month + '-' + day;
            var maxDate = year + '-' + month + '-' + day;
            $('.dob3').attr('max', maxDate);
        });

        function getcar(company_id) {
            let url = "{{route('user.get.cars.for.compaigns',':id')}}"
            url = url.replace(':id', company_id)
            $.get(url, function (response) {
                $("#car_model_1").html(response)
                // alert();
                $('#car_model_1').removeAttr('disabled')
            });
        }

        function getyear(car_id) {
            let url = "{{route('user.get.year.for.compaigns',':id')}}"
            url = url.replace(':id', car_id)
            $.get(url, function (response) {
                // console.log(response);
                $("#car_year_1").html(response);
                $('.garcartype').removeAttr('disabled')

            });
        }

        function gettype(car_id) {
            let url = "{{route('user.get.type.for.compaigns',':id')}}"
            url = url.replace(':id', car_id)
            $.get(url, function (response) {
                // console.log(response);
                $("#car_type_1").html(response)
                // $('#car_type_1').removeAttr('disabled')
            });
        }


    </script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&callback=myMap"></script>
@endsection
