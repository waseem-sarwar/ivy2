<label>Car Model:</label>
<select name="car_model" id="car_model_1"
        class="form-control car_models " notify="Country" onchange="getyear(this.value)">
    <option value="" selected disabled>Select Model</option>
   @if(isset($modelsss))
    @foreach($modelsss as $model)
        <option value="{{$model->car_name_id}}">{{$model->carName->car_name}}</option>
        @endforeach
@endif
</select>
<span class="form-text text-danger d-none" id="interval_error">Date Intervals required</span>
