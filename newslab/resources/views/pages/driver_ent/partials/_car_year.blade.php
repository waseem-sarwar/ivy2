<label>Select year:</label>
<select name=car_year
        class="form-control " id="car_year_1" onchange="gettype(this.value)">
    <option value="" selected disabled>Making Year</option>
    @if(isset($years))
    @foreach($years as $year)
        <option value="{{$year->car_model_id}}}}">{{$year->model->model_name}}</option>
        @endforeach
        @endif
</select>
<span class="form-text text-danger d-none">Date Intervals required</span>
