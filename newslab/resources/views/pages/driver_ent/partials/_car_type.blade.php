<label>Car Type:</label>
<select name="car_type_1" id="car_type_1" class="form-control  " notify="Country">
    <option value="" selected disabled>Select Type</option>
    @if(isset($types))
    @foreach($types as $type)
            <option value="{{$type->car_type}}">{{$type->carType->name}}</option>
        @endforeach
        @endif
</select>
<span class="form-text text-danger d-none" id="interval_error">Date Intervals required</span>
