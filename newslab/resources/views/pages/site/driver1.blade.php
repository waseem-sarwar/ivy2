@extends('pages.site.layout.login')
@section('content')
    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }
        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }
    </style>

    <h1>Driver Signup</h1>
    <form id="form-id" class="form-general" method="POST" action="{{ route('register') }}">
        @csrf

        <div class="sec1">
            @if ($errors->any())
                <div class="alert alert-danger" style="background-color: #e0afb4; ">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="float-left">{{ $error }}</li><br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="formFlex">
                <div class="form-group">
                    <label for="first_name" class="sr-only d-none">First Name</label>
                    <input type="text" name="first_name" id="first_name" placeholder="First Name" class="form-control"
                           value="{{old('first_name')}}" required>
                    <i class="fal fa-user icon"></i>
                    <span class='text-danger float-left d-none'>First Name is required</span>
                    @if($errors->has('first_name'))
                        <span class='text-danger float-left pl-4'> {{$errors->first('first_name')}}</span>
                    @endif

                </div>
                <div class="form-group">
                    <label for="last_name" class="sr-only d-none">Last Name</label>
                    <input type="text" name="last_name" id="last_name" placeholder="Last Name" class="form-control"
                           value="{{old('last_name')}}" required>
                    <i class="fal fa-user icon"></i>
                    <span class='text-danger float-left d-none'>Last Name is required</span>
                </div>
                @if($errors->has('last_name'))
                    <span class='text-danger float-left pl-4'> {{$errors->first('last_name')}}</span>
                @endif

            </div>

            <div class="formFlex">
                <div class="form-group">
                    <label for="email" class="sr-only d-none">Email</label>
                    <input type="email" name="email" id="email" placeholder="Email" autocomplete="off" required
                           class="form-control" value="{{old('email')}}">
                    <i class="flaticon-email icon"></i>
                    <span class='text-danger float-left d-none'>Email is required</span>
                    @if($errors->has('email'))
                        <span class='text-danger float-left pl-4'> {{$errors->first('email')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="contact_no" class="sr-only d-none">Contact Number</label>
                    <input type="number" name="contact_no" id="contact_no" placeholder="Contact Number" autocomplete="off"
                           class="form-control" value="{{old('contact_no')}}" required>
                    <i class="fal fa-phone icon"></i>
                    <span class='text-danger float-left d-none'>Contact Number is required</span>
                    @if($errors->has('contact_no'))
                        <span class='text-danger float-left pl-4'> {{$errors->first('contact_no')}}</span>
                    @endif
                </div>
            </div>
            <div class="formFlex">
                <div class="form-group">
                    <label for="doba" class="sr-only d-none">Date Of Birth</label>
                    <input type="text" name="dob" id="doba" placeholder="DOB" autocomplete="off"
                           required class="form-control" value="{{old('dob')}}" >
                    <i class="fal fa-clock icon"></i>
                    <span class='text-danger float-left d-none'>Date Of Birth is required</span>
                    @if($errors->has('dob'))
                        <span class='text-danger float-left pl-4'> {{$errors->first('dob')}}</span>
                    @endif
                </div>

                <div class="form-group">
                    <div class="formFlex h-100 pt-2 pt-lg-3">
                        <label class="checkLabel mb-0">
                            <input type="radio" class="tabchgers" name="driver_type" {{(old('driver_type')=='3')?'checked':''}} value="3" checked >Individual</label>
                        <label class="checkLabel mb-0">
                            <input type="radio" class="tabchgers" {{(old('driver_type')=='4')?'checked':''}}  name="driver_type" value="4">Enterprise</label>
                        <a class="formTooltip color-primary" data-toggle="tooltip" data-html="true"
                           data-placement="bottom"
                           title="<ul>
                                <li>If you are running a company then
                                    you go with Enterprise Account. </li>
                                <li>If you are individual become Vendor
                                    and Driver yourself then go with
                                    Individual</li>
                            </ul>">
                            <i class="fa fa-question-circle color-primary"></i>
                        </a>
                    </div>
                </div>

            </div>

            @include('pages.site.partials._countryState')

            <div class="form-group">
                <label for="address" class="sr-only d-none">Address</label>
                <input type="text" name="address" id="address" required placeholder="Address" value="{{old('address')}}"
                       class="form-control">
                <i class="fal fa-street-view icon"></i>
                <span class='text-danger float-left mb-3 d-none'>Address is required</span>
                @if($errors->has('address'))
                    <span class='text-danger float-left pl-4'> {{$errors->first('address')}}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="pwd" class="sr-only d-none">Password</label>
                <input type="password" name="pasword" id="pwd" autocomplete="off" placeholder="Password" required value="{{old('pasword')}}"
                       class="form-control">
                <i class="fal fa-lock-alt icon"></i>
                <span class='text-danger float-left d-none'>Password is required</span>
                @if($errors->has('password'))
                    <span class='text-danger float-left pl-4'> {{$errors->first('password')}}</span>
                @endif
            </div>
                <div class="formFlex  termssss pb-3 d-block">
                    <label class="checkLabel d-flex align-items-center mb-0" for="terms">
                        <input type="checkbox" class="form-control mr-3" name="terms" id="terms"  value="1" {{(old('terms')=='1')?'checked':''}}>I have read &amp;
                        accepted with the &nbsp <a href="{{route('terms')}}"> Terms & Conditions  </a>&nbsp  of use </label>
                    <span class='text-danger float-left mb-1 d-none'>Terms &amp; Condition is required</span>
                </div>

{{--            <div class="mb-1 text-right">--}}
{{--                <a class="d-block text-right" href="{{ route('login') }}">Signin?</a>--}}
{{--            </div>--}}


            <div>
                <span id="drivertabchanger1" onclick="formvalidater('.sec1 .form-control',1)" class="btn  btn-block btnMove btnNext">Signup</span>
                <span id="drivertabchanger2" onclick="formvalidater('.sec1 .form-control',0)" class="btn  d-none btn-block btnMove btnNext">Create My Profile</span>
                <span class="d-block text-right"><a href="../user/login"> Signin?</a></span>
            </div>

        </div>
        <div class="sec2" style="display: none">
            @include('pages.site.partials._loginStep2')

            {{--            <div class="formFlex">--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="company_name" class="sr-only d-none">Company Name</label>--}}
            {{--                    <input type="text" name="company_name" id="company_name" placeholder="Company Name"--}}
            {{--                           class="form-control" value="{{old('company_name')}}">--}}
            {{--                    <i class="fas fa-building icon"></i>--}}
            {{--                    <span class='text-danger float-left d-none'>Company Name is required</span>--}}
            {{--                </div>--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="industry" class="sr-only d-none">Industry</label>--}}
            {{--                    <select class="form-control form-control-solid" name="industry" id="industry">--}}
            {{--                        <option hidden>Industry</option>--}}
            {{--                        <option value="1">Transport</option>--}}
            {{--                        <option value="2">Media</option>--}}
            {{--                    </select>--}}
            {{--                    <i class="far fa-industry icon"></i>--}}
            {{--                    <span class='text-danger float-left d-none'>Industry is required</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="formFlex">--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="type_of_company" class="sr-only d-none">Company Type</label>--}}
            {{--                    <select name='type_of_company' id='type_of_company' class="form-control">--}}
            {{--                        <option hidden>Type of Company</option>--}}
            {{--                        <option value="it">IT</option>--}}
            {{--                        <option value="marketing">Marketing</option>--}}
            {{--                        <option value="networking">Networking</option>--}}
            {{--                    </select>--}}
            {{--                    <i class="far fa-network-wired icon"></i>--}}
            {{--                    <span class='text-danger float-left d-none'>Company Type is required</span>--}}
            {{--                </div>--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="size_of_company" class="sr-only d-none">Company Size</label>--}}
            {{--                    <select class="form-control form-control-solid" name="size_of_company" id="size_of_company">--}}
            {{--                        <option value="10">less than 10</option>--}}
            {{--                        <option value="5">less than 5</option>--}}
            {{--                    </select>--}}
            {{--                    <i class="fal fa-users icon"></i>--}}
            {{--                    <span class='text-danger float-left d-none'>Company Size is required</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="formFlex">--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="no_of_drivers" class="sr-only d-none">No. of Drivers</label>--}}
            {{--                    <input type="number" name="no_of_drivers" id="no_of_drivers" value="{{old('no_of_drivers')}}"--}}
            {{--                           placeholder="How many drivers" class="form-control">--}}
            {{--                    <i class="far fa-male icon"></i>--}}
            {{--                    <span class='text-danger float-left d-none'>No. of Drivers is required</span>--}}
            {{--                </div>--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="no_of_cars" class="sr-only d-none">No. of Cars</label>--}}
            {{--                    <input type="number" name="no_of_cars" id="no_of_cars" value="{{old('no_of_cars')}}"--}}
            {{--                           placeholder="How many Cars" class="form-control">--}}
            {{--                    <i class="far fa-car icon"></i>--}}
            {{--                    <span class='text-danger float-left d-none'>No. of Cars is required</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="form-group">--}}
            {{--                <label for="registration_no" class="sr-only d-none">Company Registration #</label>--}}
            {{--                <input type="text" name="registration_no" id="registration_no" value="{{old('registration_no')}}"--}}
            {{--                       placeholder="Company registration #" class="form-control">--}}
            {{--                <i class="flaticon-user icon"></i>--}}
            {{--                <span class='text-danger float-left d-none'>Company Registration # is required</span>--}}
            {{--            </div>--}}
            {{--            <div class="formFlex d-block form-row form-group">--}}
            {{--                <label class="checkLabel d-flex align-items-center" for="terms">--}}
            {{--                    <input type="checkbox" class="form-control mr-3" name="terms" id="terms">I have read &amp;--}}
            {{--                    accepted with the Term of use </label>--}}
            {{--                <span class='text-danger float-left d-none'>Terms &amp; Condition is required</span>--}}
            {{--                <a class="d-block text-right" href="{{route('byLogin')}}">Signin?</a>--}}
            {{--            </div>--}}
            {{--            <button class="btn primaryBtn" type="button" onclick="formvalidater('.sec2 .form-control',1)">Signup--}}
            {{--            </button>--}}
        </div>

    </form>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        // $(function(){
        //     // document.getElementById("dob").disabled = true;
        //     var dtToday = new Date();
        //     var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
        //     var day = dtToday.getDate();
        //     var year = dtToday.getFullYear() - 18;
        //     if(month < 10)
        //         month = '0' + month.toString();
        //     if(day < 10)
        //         day = '0' + day.toString();
        //     var minDate = year + '-' + month + '-' + day;
        //     var maxDate = year + '-' + month + '-' + day;
        //     $('#dob').attr('max', maxDate);
        // });

@if(old('city'))
        $('#cities').removeAttr('disabled');
        $('#cities').html('<option selected  value="{{old('city')}}">'+window.localStorage.getItem('city')+'</option>');
@endif
@if(old('zip'))
        $('#zips').removeAttr('disabled')
        $('#zips').html('<option selected  value="{{old('zip')}}">'+window.localStorage.getItem('zip')+'</option>');
@endif
        @if($errors->has('company_name')||$errors->has('registration_no')||$errors->has('type_of_company'))
            $(".sec1").hide();
            $(".sec2").show('9000');
        @endif
        @if($errors->has('email')||$errors->has('contact_no') )
            @if(old('driver_type')=='4' )
                $("#drivertabchanger2").removeClass('d-none');
                $("#drivertabchanger1").addClass('d-none');
          @endif
        @endif


       var dtToday = new Date();
        var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
        var day = dtToday.getDate();
        var year = dtToday.getFullYear() - 18;
        var year1 = dtToday.getFullYear() - 25;
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        var maxDate =month+'/'+day+'/'+'/'+year;
          $('#doba').datepicker({
                  format: 'mm/dd/yyyy',
                  startDate: '01/01/1949',
                  endDate: maxDate,
                  defaultDate: '01/01/1959',
                  startView:2,
                  autoclose: true,
                  readOnly:true,

              })


    </script>
@endsection
