<div class="topBarWrap">
    <div class="container-fluid">
        <div class="topBar">
            <div class="left">
                <a href="{{url('/')}}" class="logo">
                    <img class="img-fluid" src="{{ asset('site/images/logo.png')}}" alt="logo">
                </a>
            </div>
            <div class="right">
                @include('pages.site.partials._headerNav')

                @include('pages.site.partials._headerButtons')
            </div>
        </div>
    </div>
</div>
