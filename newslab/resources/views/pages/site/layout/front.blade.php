@include('pages.site.partials._pageHead')

<div class="home">

    <header class="header homeHeader" style="display: none;">
        <div class="topBarWrap">
            <div class="container-fluid">
                <div class="topBar">
                    <div class="left">
                        @include('pages.site.partials._headerNav')
                </div>

                @if (Auth::guest())
                    <form method="POST" action="{{ url('user/login') }}" id="form-4">
                        @csrf
                        <div class="right d-flex">

                            <div class="inputWrap">
                                <div class="inputFieldWrap">
                                    <i class="icon flaticon-user"></i>
                                    <input type="text" name="email" id="em" autocomplete="off" value="{{old('email')}}"
                                           class="form-control emaile <?php echo (isset($errors) && count($errors) > 0) ? 'border-danger mt-3' : ''; ?> "
                                           placeholder="Email">
                                    @if(Session::has('unauthorize'))
                                        <span class="text-danger">{{ Session::get('unauthorize') }}</span>
                                    @endif
                                    @if(isset($errors)&& count($errors)>0)
                                        <span class="text-danger">Credentials not match</span>
                                    @endif
                                    <span id="email_err" class="text-danger d-none"></span>

                                </div>

                                <div class="inputFieldWrap">
                                    <i class="icon flaticon-lock"></i>
                                    <input type="password" name="password"
                                           class="form-control passworde <?php echo (isset($errors) && count($errors) > 0) ? 'border-danger mt-3' : ''; ?>  <?php echo (Session::get('unauthorize')!=null) ? ' mb-4' : '';  ?> "
                                           id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                           aria-expanded="false" placeholder="Password">
                                    @if(isset($errors)&& count($errors)>0)
                                        <span >.</span>
                                    @endif

                                    <span id="pwd_err" class="text-danger d-none"></span>
                                    <div class="dropdown-menu bg-forget" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item " href="{{route('password.reset')}}">Forget password</a>
                                    </div>
                                </div>

                                <button type="submit" class="btn topLoginBtn <?php echo (Session::get('unauthorize')!=null) ? ' mb-4' : '';  ?>">Login</button>

                            </div>

                            @include('pages.site.partials._headerButtons')

                        </div>
                    </form>
                @else
                    <a onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                       class="btn topLoginBtn">Logout
                    </a>
                    <form id="logout-form" action="{{ route("logout") }}" method="POST" class="d-none">
                        @csrf
                    </form>
                @endif
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @if (Auth::guest())
            @include('pages.site.partials._headerButtons')
        @endif
        <div class="homeBannerContent">

            <a href="#" class="logo">
                <img src="{{ asset('site/images/logo.png') }}" height="" width="" alt="logo">
            </a>
            <h1>We Provide Best <span>Services</span></h1>
            <p>Get paid up to £100 a month for advertising on your car</p>
            <a href="tel:7028051854" class="btn callBtn"><i class="fa fa-flaticon-phone"></i> Call Us! 702-805-1854</a>
            <ul class="bannerSocialIcons">
                @include('pages.site.partials._socialLinks')
            </ul>
        </div>
    </div>
</header>

@yield('content')

@include('pages.site.partials._footerPage')

@include('pages.site.partials._pageFoot')
