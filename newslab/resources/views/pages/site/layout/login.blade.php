@include('pages.site.partials._pageHead')

<div class="login">
    <div class="accountWrap">
        <header class="header accountHeader">
            @include('pages.site.layout.headerPage')
        </header>
        <main class="main">
            <section class="enquirySection">
                @yield('content')
            </section>
        </main>
    </div>

@include('pages.site.partials._pageFoot')
