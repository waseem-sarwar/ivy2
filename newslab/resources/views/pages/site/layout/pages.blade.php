@include('pages.site.partials._pageHead')

@yield('content')

@include('pages.site.partials._footerPage')

@include('pages.site.partials._pageFoot')
