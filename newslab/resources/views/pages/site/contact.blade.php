@extends('pages.site.layout.pages')
@section('content')

<div class="contactPage">
    <header class="header accountHeader subPageHeader">
        @include('pages.site.layout.headerPage')
        <div class="subPageBanner">
            <img src="{{ asset('site/images/contact.png')}}" alt="contact">
            <h2>Contact Us</h2>
        </div>
    </header>
    <main class="main">
        @include('pages.site.partials._formInquiry')
    </main>

@endsection
