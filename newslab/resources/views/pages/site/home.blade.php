@extends('pages.site.layout.front')
@section('content')
{{--    @if (\Session::has('error'))--}}
{{--        {{Session::get('error')}}--}}
{{--    @endif--}}
    <main class="main" style="display: none;">
        <section class="brandSection">
            <div class="brandFlex">
                <div>
                    <h6>Earn money as you drive</h6>
                    <h1>Put your brand on the road</h1>
                    <p>Advertising platform, which enables brands to advertise ‘on
                        the road’ across a hyperlocal area, city, regionally or nationally.
                        Mobile Marketing</p>
                    <button  onclick="window.location.replace('{{route('drvSignup')}}');"  class="btn primaEarn Passive IncomeryBtn">Driver Signup</button>
                </div>
                <div>
                    <iframe width="" height=""
                            src="https://www.youtube.com/embed/Ub95uv0nX1s?showinfo=0&modestbranding=1"
                            frameborder="0" allow=""></iframe>
                </div>
            </div>
        </section>

        <section class="slabSection">
            <div class="slabWrap">
                <div class="slabTop">
                    <h6>It's Easy To</h6>
                    <h1>Car Slab Start to Finish</h1>
                </div>
                <div class="singup-home-slider mt-3">
                    <div class="slide">
                        <div class="brandFlex">
                            <div>
                                <h1>Signup Car Slab</h1>
                                <p>Application takes all of 30 seconds-ish.
                                    Tell us about your car and where
                                    you drive.</p>
                                <button  onclick="window.location.replace('{{route('drvSignup')}}');"  class="btn primaryBtn">Driver Signup!</button>
                            </div>
                            <div>
                                <img src="{{ asset('site/images/Layer 27.png') }}"
                                     alt="layer 27">
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="brandFlex">
                            <div>
                                <h1>Signup Car Slab</h1>
                                <p>Application takes all of 30 seconds-ish.
                                    Tell us about your car and where
                                    you drive.</p>
                                <button  onclick="window.location.replace('{{route('drvSignup')}}');" class="btn primaryBtn">Driver Signup!</button>
                            </div>
                            <div>
                                <img src="{{ asset('site/images/Layer 27.png') }}"
                                     alt="layer 27">
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="brandFlex">
                            <div>
                                <h1>Signup Car Slab</h1>
                                <p>Application takes all of 30 seconds-ish.
                                    Tell us about your car and where
                                    you drive.</p>
                                <button  onclick="window.location.replace('{{route('drvSignup')}}');" class="btn primaryBtn">Driver Signup!</button>
                            </div>
                            <div>
                                <img src="{{ asset('site/images/Layer 27.png') }}"
                                     alt="layer 27">
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="brandFlex">
                            <div>
                                <h1>Signup Car Slab</h1>
                                <p>Application takes all of 30 seconds-ish.
                                    Tell us about your car and where
                                    you drive.</p>
                                <button  onclick="window.location.replace('{{route('drvSignup')}}');" class="btn primaryBtn">Driver Signup!</button>
                            </div>
                            <div>
                                <img src="{{ asset('site/images/Layer 27.png') }}"
                                     alt="layer 27">
                            </div>
                        </div>
                    </div>
                    <div class="slide">
                        <div class="brandFlex">
                            <div>
                                <h1>Signup Car Slab</h1>
                                <p>Application takes all of 30 seconds-ish.
                                    Tell us about your car and where
                                    you drive.</p>
                                <button onclick="window.location.replace('{{route('drvSignup')}}');" class="btn primaryBtn">Driver Signup!</button>
                            </div>
                            <div>
                                <img src="{{ asset('site/images/Layer 27.png') }}"
                                     alt="layer 27">
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="passiveSection">
            <h6>Step 1</h6>
            <h1>Earn Passive Income</h1>
            <p>It's like collecting "rent" from a mini billboard on your car</p>
            <button   onclick="window.location.replace('{{route('advSignup')}}');" class="btn primaryBtn">Advertiser Signup!</button>
            <div class="singup-home-slider mt-4">
                <div class="slide">
                    <div class="">
                        <img class="img-fluid mx-auto" src="{{ asset('site/images/Layer 28.png') }}" alt="layer 28">
                    </div>
                    <div class="slideCont">
                        <h2>96% said:</h2>
                        <p>Vehicle graphics had much more impact than billboards.</p>
                    </div>
                </div>
                <div class="slide">
                    <div class="">
                        <img class="img-fluid mx-auto" src="{{ asset('site/images/Layer 28.png') }}" alt="layer 28">
                    </div>
                    <div class="slideCont">
                        <h2>96% said:</h2>
                        <p>Vehicle graphics had much more impact than billboards.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="aboutSlabSection">
            <div class="brandFlex">
                <div>
                    <h6>About Us</h6>
                    <h1>What is Car Slab?</h1>
                    <p>In addition to giving riders a way to get from point
                        A to point B, we're working to bring the future closer
                        with self-driving technology and urban air transport,
                        helping people order food quickly and affordably,
                        removing barriers to healthcare, creating new freight-
                        booking solutions, and helping companies provide a
                        seamless employee travel experience.</p>
                    <button onclick="window.location.replace('{{route('about')}}');" class="btn primaryBtn">Learn More</button>
                </div>
                <div class="aboutSlabImg">
                    <img src="{{ asset('site/images/Layer 51.png') }}" alt="layer 51">
                    <p>Getting diversity right</p>
                </div>
            </div>
        </section>

        <section class="benefitSection">
            <h1>How can you benefit?</h1>
            <div class="benefitFlex brandFlex">
                <div class="benefitBox">
                    <img src="{{ asset('site/images/ben1.png') }}" alt="benefit1">
                    <h3>Easy to start</h3>
                    <p>We create the designs and wrap the cars in 2 weeks in every city across the UK. The minimum
                        campaign
                        size is 10 cars.</p>
                </div>
                <div class="benefitBox">
                    <img src="{{ asset('site/images/ben2.png') }}" alt="benefit1">
                    <h3>Online control</h3>
                    <p>You can control your advertising campaign (heat maps, driving routes etc) in online cabinet.</p>
                </div>
                <div class="benefitBox">
                    <img src="{{ asset('site/images/ben3.png') }}" alt="benefit1">
                    <h3>Brand ambassadors</h3>
                    <p>We find loyal drivers, who love your brand and are ready to promote it.</p>
                </div>
                <div class="benefitBox">

                    <img src="{{ asset('site/images/ben4.png') }}" alt="benefit1">
                    <h3>Targeted ads</h3>
                    <p>We incentivise drivers to visit specific city locations so that your ads are seen by your target
                        audience.</p>
                </div>
            </div>
        </section>

        @include('pages.site.partials._partJoinas')

        @include('pages.site.partials._testimonials')

        @include('pages.site.partials._formInquiry')

    </main>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />--}}



<script>
    {{--toastr.error('{{Session::get('error')}}', 'Email link expires', {timeOut: 7000});--}}
@if (\Session::has('error'))
    toastr.error('{{Session::get('error')}}', 'Email link expires', {timeOut: 7000});
@endif
@if (\Session::has('deactivated'))
    toastr.error('{{Session::get('deactivated')}}', 'Account deactivated', {timeOut: 7000});
@endif
</script>
@endsection



{{--<script>--}}
{{--    toastr.error('Please contact to support', 'Oops Error occured');--}}
{{--</script>--}}
