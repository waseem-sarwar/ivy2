<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SLABcar</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/allstyles.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('site/style.css') }}">
</head>

<body class="">
<header class="header accountHeader subPageHeader">
    <div class="topBarWrap">
        <div class="container-fluid">
            <div class="topBar">
                <div class="left">
                    <a href="#" class="logo"><img src="{{ asset('site/images/logo.png')}}" height="" width="" alt="logo"></a>
                </div>
                <div class="right">
                    <nav class="main-menu-nav ">
                        <div class="body-overlay"></div>
                        <div class="menu-toggler"></div>
                        <div class="main-menu-div">
                            <div class="menu-header">
                                <a href="#" title="Home" class="home-icon fa fa-home"></a>
                                <span class="close-icon fa fa-times"></span>
                            </div>
                            <ul>
                                <li class="active">
                                    <a href="#" title="home">Home</a>
                                </li>
                                <li>
                                    <a href="" title="about">Business</a>
                                    <ul>
                                        <li><a href="{{ url('/terms') }}">Business Signup</a></li>
                                        <li><a href="{{ url('/terms') }}">Advertiser Benefits</a></li>
                                        <li><a href="{{ url('/how-it-works') }}">How it work?</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/about') }}" title="about">About</a>
                                </li>
                                <li>
                                    <a href="{{ url('/contact') }}" title="about">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="headerButtons">
                        <button class="btn adverBtn" onclick="window.location.href='{{url('/Signup/Advertiser-Personal')}}';">Advertiser Signup</button>
                        <button class="btn adverBtn" onclick="window.location.href='{{url('/Signup/Driver/Personal')}}';">Driver Signup</button>
                        {{--                        <button class="btn driverBtn">Driver Signup</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="subPageBanner">
        <img src="{{ asset('site/images/faq-bg.png')}}" height="" width="" alt="contact">
        <h2>Faq's</h2>
    </div>
</header>
<main class="main">
    <div class="faqTabsWrap">
        <ul class="nav nav-pills">
            <li><a data-toggle="pill" href="#home" class="active show">General</a></li>
            <li><a data-toggle="pill" href="#menu1">Pricing and Billing</a></li>
            <li><a data-toggle="pill" href="#menu2">Support</a></li>
            <li><a data-toggle="pill" href="#menu3">Integration ans Implementation</a></li>
            <li><a data-toggle="pill" href="#menu4">Security and Policy</a></li>
        </ul>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active show">
                <div class="bs-example">
                    <div class="accordion" id="accordionExample">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h2 class="mb-0">
                                    <button type="button" class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"><span class="textLine">How do you calculate impressions (OTS)?</span> <span class="icon"><i class="fa fa-plus"></i></span></button>
                                </h2>
                            </div>
                            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Our advertisers and their ad agencies have access to a bespoke online platform that provides easy access to their ad campaign, enabling them to monitor, assess, amend or extend an existing campaign. Our competitive edge lies within our advanced Telemetrics that analyses data from Adverttu’s GPS mobile application as an input along with patent pending probabilistic algorithms that combines with over 28 datasets enabling the Platform to calculate the minimal deliverable number of impressions each Adverttu vehicle generates in real time. We base the OTS outcome on the simultaneous analysis of these datasets that include parameters such as vehicle type, motoring and pedestrian location densities, time of day/month/year and weather conditions amongst others.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h2 class="mb-0">
                                    <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"><span class="textLine">How do vinyl stickers affect driver’s insurance?</span> <span class="icon"><i class="fa fa-plus"></i></span></button>
                                </h2>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>Bootstrap is a sleek, intuitive, and powerful front-end framework for faster and easier web development. It is a collection of CSS and HTML conventions. <a href="https://www.tutorialrepublic.com/twitter-bootstrap-tutorial/" target="_blank">Learn more.</a></p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h2 class="mb-0">
                                    <button type="button" class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"><span class="textLine">How long does it take for my campaign to go live?</span> <span class="icon"><i class="fa fa-plus"></i></span></button>
                                </h2>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>CSS stands for Cascading Style Sheet. CSS allows you to specify various style properties for a given HTML element such as colors, backgrounds, fonts etc. <a href="https://www.tutorialrepublic.com/css-tutorial/" target="_blank">Learn more.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
                <h3>Menu 1</h3>
                <p>Some content in menu 1.</p>
            </div>
            <div id="menu2" class="tab-pane fade">
                <h3>Menu 2</h3>
                <p>Some content in menu 2.</p>
            </div>
            <div id="menu3" class="tab-pane fade">
                <h3>Menu 3</h3>
                <p>Some content in menu 3.</p>
            </div>
            <div id="menu4" class="tab-pane fade">
                <h3>Menu 4</h3>
                <p>Some content in menu 4.</p>
            </div>
        </div>
        <button class="btn primaryBtn">Load More</button>
    </div>
    <section class="brandSection">
        <div class="brandFlex">
            <div>
                <h6>Earn money as you drive</h6>
                <h1>Put your brand on the road</h1>
                <p>Advertising platform, which enables brands to advertise ‘on
                    the road’ across a hyperlocal area, city, regionally or nationally.
                    Mobile Marketing</p>
                <button class="btn primaryBtn">Driver Signup</button>
            </div>
            <div>
                <iframe width="" height="" src="https://www.youtube.com/embed/Ub95uv0nX1s?showinfo=0&modestbranding=1" frameborder="0" allow=""></iframe>
            </div>
        </div>
    </section>
</main>
<footer class="footer">
    <div class="fooTop">
        <a href="#" class="logo"> <img src="{{ asset('site/images/logo.png')}}"></a>
        <div class="fooContacts">
            <p><a href="mailto:hello@Slabcar.co"><i class="flaticon-email"></i> hello@Slabcar.co</a></p>
            <p><a href="tel:7028051854"><i class="flaticon-phone"></i> 702.805.1854</a></p>
            <p><a href="#"><i class="flaticon-pin"></i> 57 Southwark Street, SE1 1RU London
                    Las Vegas, NV USA</a></p>
        </div>
        <div class="fooPages">
            <ul>
                <li><a href="{{url('/terms')}}">Terms and condition</a> </li>
                <li><a href="{{url('/terms')}}">Privacy Policy</a> </li>
                <li><a href="{{url('/terms')}}">Driver Terms Of Service</a> </li>
                <li><a href="{{url('/terms')}}">General Website Terms</a> </li>
                <li><a href="{{url('/faqs')}}">FAQs</a> </li>
            </ul>
        </div>
        <div class="fooSocialIcons">
            <a href="#" class="flaticon-facebook"></a>
            <a href="#" class="flaticon-twitter"></a>
            <a href="#" class="flaticon-google-plus"></a>
            <a href="#" class="flaticon-instagram"></a>
        </div>
    </div>
    <div class="fooBottom">
        <p>All rights reserved © 2020 Slab car</p>
        <div>
            <p>Design By <span>20thFloor</span> Technologies </p>
            <img src="{{ asset('site/images/20th-logo.png')}}" height="" width="" alt="logo">
        </div>
    </div>
</footer>
<script type="text/javascript " src="{{ asset('site/js/jquery.min.js')}} "></script>
<script src="{{ asset('site/js/plugins.js')}} "></script>
<script src="{{ asset('site/js/system.js')}}"></script>
</body>

</html>
