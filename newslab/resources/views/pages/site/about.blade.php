@extends('pages.site.layout.pages')
@section('content')

    <div class="">
        <header class="header accountHeader subPageHeader">
            @include('pages.site.layout.headerPage')
            <div class="subPageBanner">
                <img src="{{ asset('site/images/about-bg.png') }}" height="" width="" alt="contact">
                <h2>About Us</h2>
            </div>
        </header>
        <main class="main">
            <section class="brandSection">
                <div class="brandFlex">
                    <div>
                        <h6>Company Information</h6>
                        <h1>Isn’t it time transit media entered the digital age?</h1>
                        <p>We’re the advertising mavericks and bold creatives making this
                            happen. Work hard, advertise harder. That’s our motto. The tech
                            credentials of a supercar. The popularity of the Prius. There’s a
                            seat with your name on it. Jump on board./p>
                    </div>
                    <div>
                        <img src="{{ asset('site/images/about1.png') }}" alt="work1">
                    </div>
                </div>
            </section>
            <section class="slabSection">
                <div class="aboutSlabWrap">
                    <h6>advertising, media and technology.</h6>
                    <h1>Our diverse team hail from across the world</h1>
                    <p>We’re the advertising mavericks and bold creatives making this happen. Work hard, advertise
                        harder. That’s our motto. The tech credentials of a supercar. The popularity of the Prius.
                        There’s a seat with your name on it. Jump on board.</p>
                    <img src="{{ asset('site/images/about-car.png') }}" alt="about">
                </div>
            </section>
            <section class="passiveSection">
                <div class="aboutSlabWrap">
                    <h1>Earn Passive Income</h1>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into ele</p>
                    <button class="btn primaryBtn">Watch Video</button>
                    <img src="{{ asset('site/images/about-car2.png') }}" alt="about">
                </div>
            </section>

            @include('pages.site.partials._testimonials')

            <section class="driverSection">
                <div class="driverBox">
                    <img src="{{ asset('site/images/driver1.png') }}" height="" width="" alt="driver1">
                    <div class="driverCont">
                        <h1>Become a Driver</h1>
                        <p>It’s up to you! Start earning with Slap Car today.</p>
                        <button class="btn primaryBtn">Learn More</button>
                    </div>
                </div>
                <div class="driverBox">
                    <img src="{{ asset('site/images/driver1.png') }}" height="" width="" alt="driver1">
                    <div class="driverCont">
                        <h1>Become a Advertise</h1>
                        <p>Advertiser with Slap Car and grow your business.</p>
                        <button class="btn primaryBtn">Learn More</button>
                    </div>
                </div>
            </section>
        </main>

@endsection
