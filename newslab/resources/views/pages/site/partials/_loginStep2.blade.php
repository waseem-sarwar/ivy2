<div class="formFlex">
    <div class="form-group">
        <label for="company_name" class="sr-only d-none">Company Name</label>
        <input type="text" name="company_name" id="company_name" placeholder="Company Name"
               class="form-control" value="{{old('company_name')}}">
        <i class="fas fa-building icon"></i>
        <span class='text-danger float-left d-none'>Company Name is required</span>
        @if($errors->has('company_name'))
            <span class='text-danger float-left pl-4'> {{$errors->first('company_name')}}</span>
        @endif
    </div>
    <div class="form-group">
        <label for="industry" class="sr-only d-none">Industry</label>
        <select class="form-control form-control-solid" name="industry" id="industry">
            <option value="" disabled selected>Industry</option>
            @foreach($industries as $industry)
              <option value="{{$industry->id}}" {{(old('industry')==$industry->id)?'selected':''}} > {{$industry->name}}</option>
            @endforeach
        </select>
        <i class="far fa-industry icon"></i>
        <span class='text-danger float-left d-none'>Industry is required</span>
    </div>
</div>
{{--<div class="formFlex">--}}
{{--    <div class="form-group">--}}
{{--        <label for="type_of_company" class="sr-only d-none">Company Type</label>--}}
{{--        <select name='type_of_company' id='type_of_company' class="form-control">--}}
{{--            <option value="">Type of Company</option>--}}
{{--            <option value="it">IT</option>--}}
{{--            <option value="marketing">Marketing</option>--}}
{{--            <option value="networking">Networking</option>--}}
{{--        </select>--}}
{{--        <i class="far fa-network-wired icon"></i>--}}
{{--        <span class='text-danger float-left d-none'>Company Type is required</span>--}}
{{--    </div>--}}
{{--    <div class="form-group">--}}
{{--        <label for="size_of_company" class="sr-only d-none">Company Size</label>--}}
{{--        <select class="form-control form-control-solid" name="size_of_company" id="size_of_company">--}}
{{--            <option value="10">less than 10</option>--}}
{{--            <option value="5">less than 5</option>--}}
{{--        </select>--}}
{{--        <i class="fal fa-users icon"></i>--}}
{{--        <span class='text-danger float-left d-none'>Company Size is required</span>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="formFlex">
    <div class="form-group">
        <label for="type_of_company" class="sr-only d-none">Department</label>
        <select name='type_of_company' id='type_of_company' class="form-control">
            <option value="" disabled selected>Select Department</option>
            @foreach($profile_departments as $profile_department)
                <option value="{{$profile_department->id}}" {{(old('type_of_company')==$profile_department->id)?'selected':''}}>{{$profile_department->name}}</option>
            @endforeach
        </select>
        <i class="far fa-network-wired icon"></i>
        <span class='text-danger float-left d-none'>Department is required</span>
        @if($errors->has('type_of_company'))
            <span class='text-danger float-left pl-4'>Department is required</span>
        @endif
    </div>
    <div class="form-group">
        <label for="size_of_company" class="sr-only d-none">Company Size</label>
        <input type="number" min="1" class="form-control form-control-lg form-control-solid" name="size_of_company"  value="{{old('size_of_company')}}" placeholder="No.Of Employee" id="size_of_company">
        <i class="fal fa-users icon"></i>
        <span class='text-danger float-left d-none'>Company Size is required</span>
    </div>
</div>

<div class="formFlex">
    <div class="form-group">
        <label for="no_of_drivers" class="sr-only d-none">No. of Drivers</label>
        <input type="number" min="1" name="no_of_drivers" id="no_of_drivers" value="{{old('no_of_drivers')}}"
               placeholder="How many drivers" class="form-control">
        <i class="far fa-male icon"></i>
        <span class='text-danger float-left d-none'>No. of Drivers is required</span>
    </div>
    <div class="form-group">
        <label for="no_of_cars" class="sr-only d-none">No. of Cars</label>
        <input min="1" type="number" name="no_of_cars" id="no_of_cars" value="{{old('no_of_cars')}}"
               placeholder="How many Cars" class="form-control">
        <i class="far fa-car icon"></i>
        <span class='text-danger float-left d-none'>No. of Cars is required</span>
    </div>
</div>
<div class="form-group">
    <label for="registration_no" class="sr-only d-none">Company Registration #</label>
    <input type="text" name="registration_no" id="registration_no" value="{{old('registration_no')}}"
           placeholder="Company registration #" class="form-control">
    <i class="far fa-registered icon"></i>
    <span class='text-danger float-left d-none'>Company Registration # is required</span>
    @if($errors->has('registration_no'))
        <span class='text-danger float-left pl-4'> {{$errors->first('registration_no')}}</span>
    @endif
</div>
<div class="formFlex d-block ">
<label class="checkLabel d-flex align-items-center" for="terms">
    <input type="checkbox" class="form-control mr-3" name="terms" id="terms"  value="1" {{(old('terms')=='1')?'checked':''}} />I have read &amp;
    accepted with the &nbsp<a href="{{route('terms')}}"> Terms & Conditions  </a>&nbsp  of use </label>
<span class='text-danger float-left d-none'>Terms &amp; Condition is required</span>
{{--    @if((request()->is('adv/sign-up')))--}}

{{--    @elseif((request()->is('drv/sign-up')))--}}
{{--        <a class="d-block text-right" href="{{ route('login') }}">Signin?</a>--}}
{{--    @endif--}}
{{--    <a class="d-block text-right" href="{{ route('login') }}">Signin?</a>--}}
</div>
<button class="btn primaryBtn" id="signupSubmit" type="button" onclick="formvalidater('.sec2 .form-control',1)">Signup</button>
<span class="d-block text-right"><a href="../user/login"> Signin?</a></span>
