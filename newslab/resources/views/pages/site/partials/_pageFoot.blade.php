{{--<div id="loader" class="loader">--}}
{{--    <div class="d-flex align-items-center justify-content-center w-100 h-100">--}}
{{--        <i class="fal fa-tire fa-6x"></i>--}}
{{--        <span class="sr-only">Loading...</span>--}}
{{--    </div>--}}
{{--</div>--}}
<style>
    #loader2{
        position:fixed;
        z-index:99999999999999;
        width: 100%;
        padding-top:-30%;
        background:rgb(72 68 68 / 75%);
        height:100%;
        display: flex;
        justify-content: center;
        align-items: center;

    }
</style>

</div>


<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>
<script src="{{ asset('site/js/plugins.js') }}"></script>

<script src="{{url('https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.min.js')}}"></script>
<script src="{{url('https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/additional-methods.min.js')}}"></script>

<script src="{{ asset('site/js/system.js') }}"></script>

<script>

    @if(Session::has('message'))
    toastr.success('You have logged out successfully', '{{Session::get('message')}}');
    @endif
</script>

<script src="{{ asset('site/js/scripts.js') }}?{{ time() }}"></script>


</body>
</html>
