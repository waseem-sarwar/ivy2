<div class="formFlex">
    <div class="form-group">
        <label for="country_id" class="sr-only d-none">Country</label>
        <select name="country" id="country_id" class="form-control">
            {{--                            <option hidden value="">Country</option>--}}
            @foreach($countries  as $country)
                @if($country->id == '2')
                  <option value="{{$country->id}}" <?php echo ($country->id == '2') ? "selected" : "disabled"; ?> >{{$country->name}} </option>
                @endif
            @endforeach
        </select>
        <i class="fal fa-globe icon"></i>
        <span class='text-danger float-left d-none'>Country is required</span>
        @if($errors->has('country'))
            <span class='text-danger float-left pl-4'> {{$errors->first('country')}}</span>
        @endif
    </div>
    <div class="form-group">
        <label for="state_id" class="sr-only d-none">State</label>
        <select name="state" id="state_id" class="form-control" onchange="getCitiess();">
            <option value="" selected>Select State</option>
            @foreach($states  as $state)
                <option value="{{$state->id}}"  <?php echo ($state->id == old('state')) ? "selected" : ""; ?> >{{$state->state}}  </option>
            @endforeach
        </select>
        <i class="fal fa-globe icon"></i>
        <span class='text-danger float-left d-none'>State is required</span>
        @if($errors->has('state'))
            <span class='text-danger float-left pl-4'> {{$errors->first('state')}}</span>
        @endif
    </div>
</div>
<div class="formFlex">
    <div class="form-group">
        <label for="cities" class="sr-only d-none">City</label>
        <select name="city" id="cities" disabled class="form-control" onchange="getZips();">
            <option value="" selected>Select City</option>
        </select>
        <i class="fal fa-globe icon"></i>
        <span class='text-danger float-left d-none'>City is required</span>
        @if($errors->has('city'))
            <span class='text-danger float-left pl-4'> {{$errors->first('city')}}</span>
        @endif
    </div>
    <div class="form-group">
        {{--                        <input type="text" name="zip" placeholder="Zip code" class="form-control">--}}
        <label for="zips" class="sr-only d-none">Zip</label>
        <select name="zip" id="zips" disabled class="form-control">
            <option value="" selected disabled>Select Zip</option>
        </select>
        <i class="fal fa-globe icon"></i>
        <span class='text-danger float-left d-none'>Zipcode is required</span>
        @if($errors->has('zip'))
            <span class='text-danger float-left pl-4'> {{$errors->first('zip')}}</span>
        @endif
    </div>
</div>
