<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SLABcar</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap"
          rel="stylesheet">

    {{--    <link rel="stylesheet" type="text/css"--}}
    {{--          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--}}

    <link rel="stylesheet" type="text/css" href="{{asset('site/fonts/fawesome/css/all.min.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
    <link rel = "shortcut icon" href ="{{asset('/site/images/about-car2.png')}}" type = "image/x-icon">

    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/allstyles.css') }}?{{ time() }}">
    {{--    <link rel="stylesheet" type="text/css" href="{{ asset('site/style.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('site/custom.min.css') }}?{{ time() }}">

    <!--[if lte IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.0/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.0/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript " src="{{ asset('site/js/jquery.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("html").removeClass('no-js').addClass('js');
            $('body').addClass('loaded');
            $('header, main, footer').show();
        });
    </script>
</head>
<div id="loader2" style="display:none"  >
    <div style="justify-content: center;">
        <div class=" text-primary" >
            <img  src="{{asset('assets\media\loader.gif')}}">
        </div></div>
</div>
<body class="">
<div id="loader-wrapper" >
    <div id="loader" class="d-flex align-items-center justify-content-center">
        <i class="fal fa-tire fa-6x color-primary"></i>
    </div>
</div>
