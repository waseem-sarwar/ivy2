<section class="testimonialSection">
    <h1>Valuable Testimonial</h1>
    <div class="testSlider">
        <div class="slide">

            <img src="{{ asset('site/images/user.png') }}" alt="user">
            <p>“When I found Ms Diva, they made my job finding experience so easy. I created one profile and was
                matched with jobs that fit my personal preferences. I found the perfect job that I needed for my
                career.</p>
            <h5>Maryann Lambert</h5>
            <h6>Hazmat & Car Driver</h6>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
        </div>
        <div class="slide">

            <img src="{{ asset('site/images/user.png') }}" alt="user">
            <p>“When I found Ms Diva, they made my job finding experience so easy. I created one profile and was
                matched with jobs that fit my personal preferences. I found the perfect job that I needed for my
                career.</p>
            <h5>Maryann Lambert</h5>
            <h6>Hazmat & Car Driver</h6>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
        </div>
        <div class="slide">

            <img src="{{ asset('site/images/user.png') }}" alt="user">
            <p>“When I found Ms Diva, they made my job finding experience so easy. I created one profile and was
                matched with jobs that fit my personal preferences. I found the perfect job that I needed for my
                career.</p>
            <h5>Maryann Lambert</h5>
            <h6>Hazmat & Car Driver</h6>
            <div class="rating">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
            </div>
        </div>
    </div>
</section>
