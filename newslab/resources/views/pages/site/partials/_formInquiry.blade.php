<section class="enquirySection">
    <h1>Enquiry Form</h1>
    <p>Please fill out required information</p>
    <form id="home-contact" class="home-contact form-general">
        <div class="formFlex">
            <div class="form-group">
                <label for="contactFirstName" class="sr-only d-none">First Name</label>
                <input type="text" id="contactFirstName" name="contactFirstName" placeholder="First Name"
                       class="form-control" required>
                <i class="flaticon-user icon"></i>
                <span class='text-danger float-left d-none'>First Name is required</span>
            </div>
            <div class="form-group">
                <label for="contactLastName" class="sr-only d-none">Last Name</label>
                <input type="text" id="contactLastName" name="contactLastName" placeholder="Last Name"
                       class="form-control" required>
                <i class="flaticon-user icon"></i>
                <span class='text-danger float-left d-none'>Last Name is required</span>
            </div>
        </div>
        <div class="formFlex">
            <div class="form-group">
                <label for="contactEmail" class="sr-only d-none">Email</label>
                <input type="email" id="contactEmail" name="contactEmail" placeholder="Email"
                       class="form-control" required>
                <i class="flaticon-email icon"></i>
                <span class='text-danger float-left d-none'>Email is required</span>
            </div>
            <div class="form-group">
                <label for="contactPhone" class="sr-only d-none">Phone</label>
                <input type="tel" id="contactPhone" name="contactPhone" placeholder="Phone"
                       class="form-control" required>
                <i class="flaticon-phone icon"></i>
                <span class='text-danger float-left d-none'>Phone is required</span>
            </div>
        </div>
        <div class="form-group">
            <label for="contactDescription" class="sr-only d-none">Description</label>
            <input type="text" id="contactDescription" name="contactDescription" placeholder="Description"
                   class="form-control" required>
            <i class="flaticon-copy icon"></i>
            <span class='text-danger float-left d-none'>Description is required</span>
        </div>
        <div class="form-group">
                    <span class="btn uploadBtn d-flex align-items-center justify-content-between">
                        <span class="d-flex align-items-center">
                            <i class="flaticon-upload icon"></i>
                            <span>Upload file</span>
                        </span>
                        <label for="contactUpload" class="sr-only d-none">Upload File</label>
                        <input type="file" id="contactUpload" name="contactUpload" placeholder="Upload File"
                               class="file-upload form-control" value="" required>
                        <br>
                        <span class='text-danger d-none error-message font-weight-bold'>File Upload is required</span>
                    </span>
        </div>

        <button type="button" id="btnPrimaryAsk" class="btn primaryBtn"
                onclick="formvalidater('.home-contact .form-control', 0)">Inquiry Ask
        </button>

    </form>

</section>
