<nav class="main-menu-nav ">
    <div class="body-overlay"></div>
    <div class="menu-toggler"></div>
    <div class="main-menu-div">
        <div class="menu-header">
            <a href="{{url('/')}}" title="HOME" class="home-icon fa fa-home"></a>
            <span class="close-icon fa fa-times"></span>
        </div>
        <ul>
            <li class="active">
                <a href="{{url('/')}}" title="HOME">Home</a>
            </li>
            <li>
                <a href="javascript:void(0);" title="BUSINESS">Business</a>
                <ul>
                    <li>
                        <a href="{{url('adv/sign-up')}}" title="BUSINESS SIGNUP">Business Signup</a>
                    </li>
                    <li>
                        <a href="{{ url('/terms') }}" title="ADVERTISER BENEFITS">Advertiser Benefits</a>
                    </li>
                    <li>
                        <a href="{{ url('/how-it-works') }}" title="HOW IT WORKS">How it work?</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ url('/about') }}" title="ABOUT">About</a>
            </li>
            <li>
                <a href="{{ url('/contact') }}" title="CONTACT">Contact</a>
            </li>
        </ul>
    </div>
</nav>
