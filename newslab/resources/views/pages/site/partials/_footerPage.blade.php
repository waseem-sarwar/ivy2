<footer class="footer" style="display: none;">
    <div class="fooTop">

        <a href="{{route('hom')}}" class="logo"> <img src="{{ asset('site/images/logo.png') }}"></a>
        <div class="fooContacts">
            <p><a href="mailto:hello@Slabcar.co"><i class="flaticon-email"></i> hello@Slabcar.co</a></p>
            <p><a href="tel:7028051854"><i class="flaticon-phone"></i> 702.805.1854</a></p>
            <p><a href="#"><i class="flaticon-pin"></i> 57 Southwark Street, SE1 1RU London
                    Las Vegas, NV USA</a></p>
        </div>
        <div class="fooPages">
            <ul>
                <li><a href="{{url('/terms')}}">Terms and condition</a></li>
                <li><a href="{{url('/terms')}}">Privacy Policy</a></li>
                <li><a href="{{url('/terms')}}">Driver Terms Of Service</a></li>
                <li><a href="{{url('/terms')}}">General Website Terms</a></li>
                <li><a href="{{url('/faqs')}}">FAQs</a></li>
            </ul>
        </div>
        <div class="fooSocialIcons">
            <a href="#" class="flaticon-facebook"></a>
            <a href="#" class="flaticon-twitter"></a>
            <a href="#" class="flaticon-google-plus"></a>
            <a href="#" class="flaticon-instagram"></a>
        </div>
    </div>
    <div class="fooBottom">
        <p>All rights reserved © 2020 Slab car</p>
        <div>
            <p>Design By <span>20thFloor</span> Technologies </p>
            <img src="{{ asset('site/images/20th-logo.png') }}" height="" width="" alt="logo">

        </div>
    </div>
</footer>
