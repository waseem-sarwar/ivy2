<section class="driverSection">
    <div class="driverBox">
        <img src=" {{ asset('site/images/driver1.png') }}" alt="driver1">
        <div class="driverCont">
            <h1>Become a Driver</h1>
            <p>It’s up to you! Start earning with Slap Car today.</p>
            <button  onclick="window.location.replace('{{route('drvSignup')}}');" class="btn primaryBtn">Sign up</button>
        </div>
    </div>
    <div class="driverBox">
        <img src=" {{ asset('site/images/driver1.png') }}" alt="driver1">
        <div class="driverCont ">
            <h1 >Become a Advertiser</h1>
            <p>Advertiser with Slap Car and grow your business.</p>
            <button  onclick="window.location.replace('{{route('advSignup')}}');" class="btn primaryBtn">Sign up</button>
        </div>
    </div>
</section>
