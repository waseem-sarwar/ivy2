@extends('pages.site.layout.login')
@section('content')
    <main class="main">
        <section class="enquirySection">
            <h1>Forgot Password?</h1>
            <p>We’ll help you reset it and get back on track.</p>
            <form method="POST" action="{{ route('password.email') }}">
                @csrf
                <div class="form-group">
                    <input  id="email" type="email"  placeholder="Email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    <i class="flaticon-user icon"></i>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>

                <div class="formFlex form-group">
                    <p></p>
                    <a href="{{route('Login')}}">Signin?</a>

                </div>
                <button type="submit" class="btn primaryBtn">Reset Password</button>
            </form>
        </section>
    </main>
@endsection
