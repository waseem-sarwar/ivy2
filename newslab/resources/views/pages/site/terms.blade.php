<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SLABcar</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/allstyles.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('site/style.css') }}">
</head>

<body class="">
<header class="header accountHeader subPageHeader">
    <div class="topBarWrap">
        <div class="container-fluid">
            <div class="topBar">
                <div class="left">
                    <a href="/" class="logo"><img src="{{ asset('site/images/logo.png')}}" height="" width="" alt="logo"></a>
                </div>
                <div class="right">
                    <nav class="main-menu-nav ">
                        <div class="body-overlay"></div>
                        <div class="menu-toggler"></div>
                        <div class="main-menu-div">
                            <div class="menu-header">
                                <a href="{{ url('/') }}" title="Home" class="home-icon fa fa-home"></a>
                                <span class="close-icon fa fa-times"></span>
                            </div>
                            <ul>
                                <li class="active ">
                                    <a href="{{ url('/') }}" title="home">Home</a>
                                </li>
                                <li>
                                    <a href="" title="about">Business</a>
                                    <ul>
                                        <li><a href="{{url('adv/sign-up')}}">Business Signup</a></li>
                                        <li><a href="{{ url('/terms') }}">Advertiser Benefits</a></li>
                                        <li><a href="{{ url('/how-it-works') }}">How it work?</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/about') }}" title="about">About</a>
                                </li>
                                <li>
                                    <a href="{{ url('/contact') }}" title="about">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="headerButtons">
                        <button class="btn adverBtn" onclick="window.location.href='{{url('adv/sign-up')}}';">Advertiser Signup</button>
                        <button class="btn adverBtn" onclick="window.location.href='{{url('drv/sign-up')}}';">Driver Signup</button>
                        {{--                        <button class="btn driverBtn">Driver Signup</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="subPageBanner">
        <img src="{{ asset('site/images/term-bg.png')}}" height="" width="" alt="contact">
        <h2>Terms and conditions</h2>
    </div>
</header>
<main class="main">
    <div class="container">
        <div class="termsWrap">
            <ul class="nav nav-tabs">
                <li><a data-toggle="tab" href="#terms" class="active show">Terms and Conditions</a></li>
                <li><a data-toggle="tab" href="#privacy">Privacy Policy</a></li>
                <li><a data-toggle="tab" href="#dt">Driver Terms Of Service</a></li>
                <li><a data-toggle="tab" href="#gw">General Website Terms</a></li>
{{--                <li><a data-toggle="tab" href="#faq">FAQs</a></li>--}}
            </ul>
            <div class="tab-content">
                <div id="terms" class="tab-pane fade in active show ">
                    <h6>School Room Cookies Policy</h6>
                    <p>This is the cookies policy of FutureLearn which applies to our Website (“Cookies Policy”). You should read this Cookies Policy with the Terms and the Privacy Policy.</p>
                    <p>We use cookies to enhance your experience of FutureLearn and to better understand how people use the Website. This may also include cookies from third party social media websites. You can change your cookies settings at any time by following the instructions below in paragraph 3: “How to manage cookies”.</p>
                    <h6>1. What are Cookies?</h6>
                    <p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your
                        computer if you agree. Cookies contain information that is transferred to your computer’s hard drive.</p>
                    <p><b>We use the following cookies:</b></p>
                    <p>Strictly necessary cookies. These are cookies that are required for the operation of our Website. They include, for example, cookies that enable you to log into secure areas of our Website, use a shopping cart or make use of e-billing services.</p>
                    <p>Analytical/performance cookies. They allow us to recognise and count the number of visitors and to see how visitors move around our Website when they are using it. This helps us to improve the way our Website works, for example, by ensuring that users are finding what they are looking for easily.</p>
                    <p>Functionality cookies. These are used to recognise you when you return to our Website. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).</p>
                    <p>Targeting cookies. These cookies record your visit to our Website, the pages you have visited and the links you have followed. We will use this information to make our Website more relevant to your interests. We may also share this information with third parties for this purpose.</p>
                </div>


                <div id="privacy" class="tab-pane fade">
                    <h3>Menu 2</h3>
                    <p>Some content driver terms</p>
                </div>
                <div id="dt" class="tab-pane fade">
                    <h3>Menu 3</h3>
                    <p>Some content driver terms</p>
                </div>
                <div id="gw" class="tab-pane fade">
                    <h3>Menu 4</h3>
                    <p>Some content of gw</p>
                </div>
                <div id="faq" class="tab-pane fade">
                    <h6>School Room Cookies Policy</h6>
                    <p>This is the cookies policy of FutureLearn which applies to our Website (“Cookies Policy”). You should read this Cookies Policy with the Terms and the Privacy Policy.</p>
                    <p>We use cookies to enhance your experience of FutureLearn and to better understand how people use the Website. This may also include cookies from third party social media websites. You can change your cookies settings at any time by following the instructions below in paragraph 3: “How to manage cookies”.</p>
                    <h6>1. What are Cookies?</h6>
                    <p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your
                        computer if you agree. Cookies contain information that is transferred to your computer’s hard drive.</p>
                    <p><b>We use the following cookies:</b></p>
                    <p>Strictly necessary cookies. These are cookies that are required for the operation of our Website. They include, for example, cookies that enable you to log into secure areas of our Website, use a shopping cart or make use of e-billing services.</p>
                    <p>Analytical/performance cookies. They allow us to recognise and count the number of visitors and to see how visitors move around our Website when they are using it. This helps us to improve the way our Website works, for example, by ensuring that users are finding what they are looking for easily.</p>
                    <p>Functionality cookies. These are used to recognise you when you return to our Website. This enables us to personalise our content for you, greet you by name and remember your preferences (for example, your choice of language or region).</p>
                    <p>Targeting cookies. These cookies record your visit to our Website, the pages you have visited and the links you have followed. We will use this information to make our Website more relevant to your interests. We may also share this information with third parties for this purpose.</p>
                </div>
            </div>
        </div>
    </div>
</main>
<footer class="footer">
    <div class="fooTop">
        <a href="#" class="logo"> <img src="{{ asset('site/images/logo.png')}}"></a>
        <div class="fooContacts">
            <p><a href="mailto:hello@Slabcar.co"><i class="flaticon-email"></i> hello@Slabcar.co</a></p>
            <p><a href="tel:7028051854"><i class="flaticon-phone"></i> 702.805.1854</a></p>
            <p><a href="#"><i class="flaticon-pin"></i> 57 Southwark Street, SE1 1RU London
                    Las Vegas, NV USA</a></p>
        </div>
        <div class="fooPages">
            <ul>
                <li><a href="{{url('/terms')}}">Terms and condition</a> </li>
                <li><a href="{{url('/terms')}}">Privacy Policy</a> </li>
                <li><a href="{{url('/terms')}}">Driver Terms Of Service</a> </li>
                <li><a href="{{url('/terms')}}">General Website Terms</a> </li>
                <li><a href="{{url('/faqs')}}">FAQs</a> </li>
            </ul>
        </div>
        <div class="fooSocialIcons">
            <a href="#" class="flaticon-facebook"></a>
            <a href="#" class="flaticon-twitter"></a>
            <a href="#" class="flaticon-google-plus"></a>
            <a href="#" class="flaticon-instagram"></a>
        </div>
    </div>
    <div class="fooBottom">
        <p>All rights reserved © 2020 Slab car</p>
        <div>
            <p>Design By <span>20thFloor</span> Technologies </p>
            <img src="{{ asset('site/images/20th-logo.png')}}" height="" width="" alt="logo">
        </div>
    </div>
</footer>
<script type="text/javascript " src="{{ asset('site/js/jquery.min.js')}} "></script>
<script src="{{ asset('site/js/plugins.js')}} "></script>
<script src="{{ asset('site/js/system.js')}}"></script>
</body>

</html>
