<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/allstyles.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('site/style.css') }}">
</head>

<body class="">
<div class="accountWrap">
    <header class="header accountHeader">
        <div class="topBarWrap">
            <div class="container-fluid">
                <div class="topBar">
                    <div class="left">
                        <a href="#" class="logo"><img src="{{ asset('site/images/logo.png')}}" height="" width="" alt="logo"></a>
                    </div>
                    <div class="right">
                        <nav class="main-menu-nav ">
                            <div class="body-overlay"></div>
                            <div class="menu-toggler"></div>
                            <div class="main-menu-div">
                                <div class="menu-header">
                                    <a href="#" title="Home" class="home-icon fa fa-home"></a>
                                    <span class="close-icon fa fa-times"></span>
                                </div>
                                <ul>
                                    <li class="active">
                                        <a href="#" title="home">Home</a>
                                    </li>
                                    <li>
                                        <a href="" title="about">Business</a>
                                        <ul>
                                            <li><a href="{{ url('/terms') }}">Business Signup</a></li>
                                            <li><a href="{{ url('/terms') }}">Advertiser Benefits</a></li>
                                            <li><a href="{{ url('/how-it-works') }}">How it work?</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/about') }}" title="about">About</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/contact') }}" title="about">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="headerButtons">
                            <button class="btn adverBtn" onclick="window.location.href='{{url('/advertisor-sign-up-step-1')}}';">Advertiser Signup</button>
                            <button class="btn adverBtn" onclick="window.location.href='{{url('/driver-sign-up-step-1')}}';">Driver Signup</button>
                            {{--                        <button class="btn driverBtn">Driver Signup</button>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="main">
        <section class="enquirySection">
            <h1>Advertiser Signup</h1>
            <form>
                <div class="formFlex">
                    <div class="form-group">
                        <input type="text" name="" placeholder="Company Name" class="form-control">
                        <i class="flaticon-user icon"></i>
                    </div>
                    <div class="form-group">
                        <input type="text" name="" placeholder="Industry" class="form-control">
                        <i class="flaticon-user icon"></i>
                    </div>




                </div>

                <div class="formFlex">
                    <div class="form-group">
                        <select class="form-control">
                            <option hidden>Type of Company</option>
                            <option>abc</option>
                            <option>abc</option>
                        </select>
                        <i class="flaticon-user icon"></i>
                    </div>
                    <div class="form-group">
                        <input type="text" name="" placeholder="Size of Company" class="form-control">
                        <i class="flaticon-user icon"></i>
                    </div>
                </div>
                <div class="formFlex">
                    <div class="form-group">
                        <input type="text" name="" placeholder="How many drivers" class="form-control">
                        <i class="flaticon-user icon"></i>
                    </div>
                    <div class="form-group">
                        <input type="text" name="" placeholder="How many Cars" class="form-control">
                        <i class="flaticon-user icon"></i>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" name="" placeholder="Company registration #" class="form-control">
                    <i class="flaticon-user icon"></i>
                </div>
                <div class="formFlex form-group">
                    <label class="checkLabel"><input type="checkbox" name="">I have read and accepted with the Term of use </label>
                    <a href="#">Signin?</a>
                </div>
                <button class="btn primaryBtn">Signup</button>
            </form>
            <iframe width="" height="" src="https://www.youtube.com/embed/Ub95uv0nX1s?showinfo=0&modestbranding=1" frameborder="0" allow=""></iframe>
        </section>
    </main>
</div>
<script type="text/javascript " src="{{ asset('site/js/jquery.min.js')}} "></script>
<script src="{{ asset('site/js/plugins.js')}} "></script>
<script src="{{ asset('site/js/system.js')}}"></script>
</body>

</html>
