<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SLABcar</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/allstyles.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('site/style.css') }}">
</head>

<body class="">
<header class="header accountHeader subPageHeader">
    <div class="topBarWrap">
        <div class="container-fluid">
            <div class="topBar">
                <div class="left">
                    <a href="#" class="logo"><img src="{{ asset('site/images/logo.png')}}" height="" width="" alt="logo"></a>
                </div>
                <div class="right">
                    <nav class="main-menu-nav ">
                        <div class="body-overlay"></div>
                        <div class="menu-toggler"></div>
                        <div class="main-menu-div">
                            <div class="menu-header">
                                <a href="#" title="Home" class="home-icon fa fa-home"></a>
                                <span class="close-icon fa fa-times"></span>
                            </div>
                            <ul>
                                <li class="active">
                                    <a href="#" title="home">Home</a>
                                </li>
                                <li>
                                    <a href="" title="about">Business</a>
                                    <ul>
                                        <li><a href="{{ url('/terms') }}">Business Signup</a></li>
                                        <li><a href="{{ url('/terms') }}">Advertiser Benefits</a></li>
                                        <li><a href="{{ url('/how-it-works') }}">How it work?</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="{{ url('/about') }}" title="about">About</a>
                                </li>
                                <li>
                                    <a href="{{ url('/contact') }}" title="about">Contact</a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="headerButtons">
                        <button class="btn adverBtn" onclick="window.location.href='{{url('/Signup/Advertiser-Personal')}}';">Advertiser Signup</button>
                        <button class="btn adverBtn" onclick="window.location.href='{{url('/Signup/Driver/Personal')}}';">Driver Signup</button>
                        {{--                        <button class="btn driverBtn">Driver Signup</button>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="subPageBanner">
        <img src="{{ asset('site/images/work-bg.png')}}" height="" width="" alt="contact">
        <h2>How it Works</h2>
    </div>
</header>
<main class="main">
    <section class="brandSection">
        <div class="brandFlex">
            <div>
                <h1>The whole process is free and always will be.</h1>
                <p>We’re the advertising mavericks and bold creatives making this
                    happen. Work hard, advertise harder. That’s our motto. The tech
                    credentials of a supercar. The popularity of the Prius.  There’s a
                    seat with your name on it. Jump on board.</p>
            </div>
            <div>
                <img src="{{ asset('site/images/work1.png')}}" height="" width="" alt="work1">
            </div>
        </div>
    </section>
    <section class="workFlexSection">
        <div class="workBox">
            <img src="{{ asset('site/images/icon1.jpeg')}}" height="" width="" alt="icon1">
            <h6>Earn Money</h6>
            <p>Get up to £150 per month & cool perks from
                our clients for something you've been
                doing all along - driving.</p>
        </div>
        <div class="workBox">
            <img src="{{ asset('site/images/icon2.jpeg')}}" height="" width="" alt="icon1">
            <h6>Protect Point</h6>
            <p>Our wrap not only makes your car stand
                out during the campaign, but it also
                protects your vehicle's paint against
                chips & scratches.</p>
        </div>
        <div class="workBox">
            <img src="{{ asset('site/images/icon3.jpeg')}}" height="" width="" alt="icon1">
            <h6>Community</h6>
            <p>Become a part of a local community of drivers,
                where you can find people with common
                interests or even life-long friends.</p>
        </div>
    </section>
    <section class="slabSection">
        <div class="slabWrap">
            <div class="slabTop">
                <h6>It's Easy To</h6>
                <h1>Car Slab Start to Finish</h1>
                <ul class="slabStepsCount">
                    <li><span>1</span></li>
                    <li><span>2</span></li>
                    <li><span>3</span></li>
                    <li><span>4</span></li>
                    <li><span>5</span></li>
                </ul>
            </div>
            <div class="brandFlex">
                <div>
                    <h1>Signup Car Slab</h1>
                    <p>Application takes all of 30 seconds-ish.
                        Tell us about your car and where
                        you drive.</p>
                    <button class="btn primaryBtn">Driver Signup!</button>
                </div>
                <div>
                    <img src="{{ asset('site/images/Layer 27.png')}}" height="" width="" alt="layer 27">
                </div>
            </div>
        </div>
    </section>
    <section class="passiveSection">
        <h6>Step 1</h6>
        <h1>Earn Passive Income</h1>
        <p>It's like collecting "rent" from a mini billboard on your car</p>
        <button class="btn primaryBtn">Watch Video</button>
        <div class="passiveSlider">
            <div class="slide">
                <img src="{{ asset('site/images/Layer 28.png')}}" height="" width="" alt="layer 28">
                <div class="slideCont">
                    <h2>96% said:</h2>
                    <p>Vehicle graphics had much more impact than billboards.</p>
                </div>
            </div>
            <div class="slide">
                <img src="{{ asset('site/images/Layer 28.png')}}" height="" width="" alt="layer 28">
                <div class="slideCont">
                    <h2>96% said:</h2>
                    <p>Vehicle graphics had much more impact than billboards.</p>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonialSection">
        <h1>Valuable Testimonial</h1>
        <div class="testSlider">
            <div class="slide">
                <img src="{{ asset('site/images/user.png')}}" height="" width="" alt="user">
                <p>“When I found Ms Diva, they made my job finding experience so easy. I created one profile and was matched with jobs that fit my personal preferences. I found the perfect job that I needed for my career.</p>
                <h5>Maryann Lambert</h5>
                <h6>Hazmat & Car Driver</h6>
                <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="slide">
                <img src="{{ asset('site/images/user.png')}}" height="" width="" alt="user">
                <p>“When I found Ms Diva, they made my job finding experience so easy. I created one profile and was matched with jobs that fit my personal preferences. I found the perfect job that I needed for my career.</p>
                <h5>Maryann Lambert</h5>
                <h6>Hazmat & Car Driver</h6>
                <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
            <div class="slide">
                <img src="{{ asset('site/images/user.png')}}" height="" width="" alt="user">
                <p>“When I found Ms Diva, they made my job finding experience so easy. I created one profile and was matched with jobs that fit my personal preferences. I found the perfect job that I needed for my career.</p>
                <h5>Maryann Lambert</h5>
                <h6>Hazmat & Car Driver</h6>
                <div class="rating">
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                    <i class="fa fa-star"></i>
                </div>
            </div>
        </div>
    </section>
    <section class="benefitSection">
        <h1>How can you benefit?</h1>
        <div class="benefitFlex brandFlex">
            <div class="benefitBox">
                <img src="{{ asset('site/images/ben1.png')}}" height="" width="" alt="benefit1">
                <h3>Easy to start</h3>
                <p>We create the designs and wrap the cars in 2 weeks in every city across the UK. The minimum campaign size is 10 cars.</p>
            </div>
            <div class="benefitBox">
                <img src="{{ asset('site/images/ben2.png')}}" height="" width="" alt="benefit1">
                <h3>Online control</h3>
                <p>You can control your advertising campaign (heat maps, driving routes etc) in online cabinet.</p>
            </div>
            <div class="benefitBox">
                <img src="{{ asset('site/images/ben3.png')}}" height="" width="" alt="benefit1">
                <h3>Brand ambassadors</h3>
                <p>We find loyal drivers, who love your brand and are ready to promote it.</p>
            </div>
            <div class="benefitBox">
                <img src="{{ asset('site/images/ben4.png')}}" height="" width="" alt="benefit1">
                <h3>Targeted ads</h3>
                <p>We incentivise drivers to visit specific city locations so that your ads are seen by your target audience.</p>
            </div>
        </div>
    </section>
    <section class="driverSection">
        <div class="driverBox">
            <img src="{{ asset('site/images/driver1.png')}}" height="" width="" alt="driver1">
            <div class="driverCont">
                <h1>Become a Driver</h1>
                <p>It’s up to you! Start earning with Slap Car today.</p>
                <button class="btn primaryBtn">Learn More</button>
            </div>
        </div>
        <div class="driverBox">
            <img src="{{ asset('site/images/driver1.png')}}" height="" width="" alt="driver1">
            <div class="driverCont">
                <h1>Become a Advertise</h1>
                <p>Advertiser with Slap Car and grow your business.</p>
                <button class="btn primaryBtn">Learn More</button>
            </div>
        </div>
    </section>
</main>
<footer class="footer">
    <div class="fooTop">
        <a href="#" class="logo"> <img src="{{ asset('site/images/logo.png')}}"></a>
        <div class="fooContacts">
            <p><a href="mailto:hello@Slabcar.co"><i class="flaticon-email"></i> hello@Slabcar.co</a></p>
            <p><a href="tel:7028051854"><i class="flaticon-phone"></i> 702.805.1854</a></p>
            <p><a href="#"><i class="flaticon-pin"></i> 57 Southwark Street, SE1 1RU London
                    Las Vegas, NV USA</a></p>
        </div>
        <div class="fooPages">
            <ul>
                <li><a href="{{url('/terms')}}">Terms and condition</a> </li>
                <li><a href="{{url('/terms')}}">Privacy Policy</a> </li>
                <li><a href="{{url('/terms')}}">Driver Terms Of Service</a> </li>
                <li><a href="{{url('/terms')}}">General Website Terms</a> </li>
                <li><a href="{{url('/faqs')}}">FAQs</a> </li>
            </ul>
        </div>
        <div class="fooSocialIcons">
            <a href="#" class="flaticon-facebook"></a>
            <a href="#" class="flaticon-twitter"></a>
            <a href="#" class="flaticon-google-plus"></a>
            <a href="#" class="flaticon-instagram"></a>
        </div>
    </div>
    <div class="fooBottom">
        <p>All rights reserved © 2020 Slab car</p>
        <div>
            <p>Design By <span>20thFloor</span> Technologies </p>
            <img src="{{ asset('site/images/20th-logo.png')}}" height="" width="" alt="logo">
        </div>
    </div>
</footer>
<script type="text/javascript " src="{{ asset('site/js/jquery.min.js')}} "></script>
<script src="{{ asset('site/js/plugins.js')}} "></script>
<script src="{{ asset('site/js/system.js')}}"></script>
</body>

</html>
