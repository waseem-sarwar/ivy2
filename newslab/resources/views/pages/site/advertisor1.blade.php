@extends('pages.site.layout.login')
@section('content')
    <style>
        /* Chrome, Safari, Edge, Opera */
        input::-webkit-outer-spin-button,
        input::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        /* Firefox */
        input[type=number] {
            -moz-appearance: textfield;
        }

        .datepicker.dropdown-menu {
            z-index: 9999999 !important;
        }

        .dob:read-only {
            background-color: #fff;
        }
    </style>
    <h1>Advertiser Signup</h1>
    <form id="form-id" class="form-general" method="POST" action="{{ route('register') }}">
        @csrf


        <div class="sec1">
            @if ($errors->any())
                <div class="alert alert-danger" style="background-color: #e0afb4; ">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="float-left">{{ $error }}</li><br>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="formFlex">
                <div class="form-group">
                    <label for="first_name" class="sr-only d-none">First Name</label>
                    <input type="text" name="first_name" id="first_name" placeholder="First Name"
                           value="{{old('first_name')}}" required class="form-control">
                    <i class="fal fa-user icon"></i>
                    <span class='text-danger float-left d-none'>First Name is required</span>
                </div>
                <div class="form-group">
                    <label for="last_name" class="sr-only d-none">Last Name</label>
                    <input type="text" name="last_name" id="last_name" placeholder="Last Name"
                           value="{{old('last_name')}}" required class="form-control">
                    <i class="fal fa-user icon"></i>
                    <span class='text-danger float-left d-none'>Last Name is required</span>
                </div>
            </div>
            <div class="formFlex">

                <div class="form-group">
                    <label for="dob" class="sr-only d-none">Phone </label>
                    <input placeholder="Phone Number" type="number" autocomplete="off" name="contact_no" id="contact_no"
                           class="form-control" value="{{old('contact_no')}}">
                    <i class="fal fa-phone icon"></i>
                    <span class='text-danger float-left d-none'>Phone Number is required</span>
                </div>
                <div class="form-group">
                    <label for="email" class="sr-only d-none">Email</label>
                    <input type="email" name="email" id="email" placeholder="Email" value="{{old('email')}}"
                           required class="form-control">
                    <i class="flaticon-email icon"></i>
                    <span class='text-danger float-left d-none'>Email is required</span>
                </div>
            </div>
            <div class="formFlex">
                <div class="form-group">
                    <label for="job_designation" class="sr-only d-none">Job Designation</label>
                    <input type="text" name="job_designation" id="job_designation" placeholder="Job Designation"
                           value="{{old('job_designation')}}" required class="form-control">
                    <i class="fal fa-user-tie icon"></i>
                    <span class='text-danger float-left d-none'>Job Designation is required</span>
                </div>

                {{--                   <script> $('.datepicker').datepicker();</script>--}}
                {{--                    <div class="input-group date" data-provide="datepicker">--}}
                {{--                        <input type="text" class="form-control">--}}
                {{--                        <div class="input-group-addon">--}}
                {{--                            <span class="glyphicon glyphicon-th"></span>--}}
                {{--                        </div>--}}
                {{--                    </div>--}}

                <div class="form-group">
                    <label for="doba" class="sr-only d-none">Date Of Birth</label>
                    <input type="text" name="dob" id="doba" placeholder="DOB" autocomplete="off" required readonly
                           class="form-control dob" value="{{old('dob')}}">
                    <i class="fal fa-clock icon"></i>
                    <span class='text-danger float-left d-none'>Date Of Birth is required</span>
                    <i class="fal fa-clock icon"></i>
                    @if($errors->has('dob'))
                        <span class='text-danger float-left pl-4'> {{$errors->first('dob')}}</span>
                    @endif
                </div>

                {{--                    <div class="form-group">--}}
                {{--                        <label for="doba" class="sr-only d-none">Date Of Birth</label>--}}
                {{--                        <input type="text" name="dob" id="doba" placeholder="DOB" autocomplete="off" required class="form-control" value="{{old('dob')}}" >--}}
                {{--                        <i class="fal fa-clock icon"></i>--}}
                {{--                        <span class='text-danger float-left d-none'>Date Of Birth is required</span>--}}
                {{--                        @if($errors->has('dob'))--}}
                {{--                            <span class='text-danger float-left pl-4'> {{$errors->first('dob')}}</span>--}}
                {{--                        @endif--}}
                {{--                    </div>--}}

                {{--                <div class="form-group">--}}
                {{--                    <label for="dob" class="sr-only d-none">Date Of Birth</label>--}}
                {{--                    <input type="text" name="dob" id="dob" placeholder="DOB" autocomplete="off"--}}
                {{--                           required class="form-control" value="{{old('dob')}}" >--}}
                {{--                    <i class="fal fa-clock icon"></i>--}}
                {{--                    <span class='text-danger float-left d-none'>DOB is required</span>--}}
                {{--                </div>--}}
            </div>
            @include('pages.site.partials._countryState')

            <div class="form-group">
                <label for="pwd" class="sr-only d-none">Password</label>
                <input type="password" name="pasword" id="pwd" autocomplete="off" placeholder="Password" required
                       value="{{old('pasword')}}"
                       class="form-control">
                <i class="fal fa-lock-alt icon"></i>
                <span class='text-danger float-left d-none'>Password is required</span>
            </div>

            {{--            <div class="form-group">--}}
            {{--                <div class="form-group">--}}
            {{--                    <label for="street_address" class="sr-only d-none">Street Address</label>--}}
            {{--                    <input type="text" name="street_address" id="street_address" value="{{old('street_address')}}"--}}
            {{--                           placeholder="Street address" required class="form-control">--}}
            {{--                    <i class="fal fa-road icon"></i>--}}
            {{--                    <span class='text-danger float-left mb-3 d-none'>Street Address is required</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="form-group">--}}
            {{--                <label for="address" class="sr-only d-none">Address</label>--}}
            {{--                <input type="text" name="address" id="address" placeholder="Address" required class="form-control"--}}
            {{--                       value="{{old('address')}}">--}}
            {{--                <i class="fal fa-street-view icon"></i>--}}
            {{--                <span class='text-danger float-left d-none'>Address is required</span>--}}
            {{--            </div>--}}

            <input type="hidden" name="role" value="2">
            {{--            <div class="formFlex d-block form-row form-group">--}}
            {{--                <label class="checkLabel d-flex align-items-center" for="terms">--}}
            {{--                    <input type="checkbox" class="form-control mr-3" name="terms" id="terms">I have read &amp;--}}
            {{--                    accepted with the <a href="{{route('terms')}}">Terms & Conditions</a>  of use </label>--}}
            {{--                <span class='text-danger float-left d-none'>Terms &amp; Condition is required</span>--}}
            {{--                --}}{{--    @if((request()->is('adv/sign-up')))--}}
            {{--                <a class="d-block text-right" href="{{ route('login') }}">Signin?</a>--}}
            {{--                --}}{{--    @elseif((request()->is('drv/sign-up')))--}}
            {{--                --}}{{--        <a class="d-block text-right" href="{{ route('login') }}">Signin?</a>--}}
            {{--                --}}{{--    @endif--}}
            {{--            </div>--}}
            {{--            <button class="btn primaryBtn" id="signupSubmit" type="button" onclick="formvalidater('.sec1 .form-control',1)">Signup</button>--}}

            <span id="drivertabchanger2" onclick="formvalidater('.sec1 .form-control',0)"
                  class="btn btn-block btnMove btnNext">Create My Profile</span>
            <span class="d-block text-right"><a href="../user/login"> Signin?</a></span>
        </div>


        <div class="sec2 " style="display: none">
            {{--            @include('pages.site.partials._loginStep2')--}}
            <div class="formFlex">
                <div class="form-group">
                    <label for="company_name" class="sr-only d-none">Company Name</label>
                    <input type="text" name="company_name" id="company_name" placeholder="Company Name"
                           class="form-control" value="{{old('company_name')}}">
                    <i class="fas fa-building icon"></i>
                    <span class='text-danger float-left d-none'>Company Name is required</span>
                    @if($errors->has('company_name'))
                        <span class='text-danger float-left pl-4'> {{$errors->first('company_name')}}</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="industry" class="sr-only d-none">Industry</label>
                    <select class="form-control form-control-solid" name="industry" id="industry">
                        <option selected disabled>Industry</option>
                        @foreach($industries as $industry)
                            <option
                                {{(old('industry')==$industry->id)?'selected':''}} value="{{$industry->id}}">{{$industry->name}}</option>
                        @endforeach
                    </select>
                    <i class="far fa-industry icon"></i>
                    <span class='text-danger float-left d-none'>Industry is required</span>
                    @if($errors->has('industry'))
                        <span class='text-danger float-left pl-4'> {{$errors->first('industry')}}</span>
                    @endif
                </div>
            </div>
            <div class="formFlex">
                <div class="form-group">
                    <label for="type_of_company" class="sr-only d-none">Select Department</label>
                    <select name='type_of_company' id='type_of_company' class="form-control">
                        <option selected disabled>Department</option>
                        @foreach($profile_departments as $profile_department)
                            <option
                                value="{{$profile_department->id}}" {{(old('type_of_company')==$profile_department->id)?'selected':''}}>{{$profile_department->name}}</option>
                        @endforeach
                    </select>
                    <i class="far fa-network-wired icon"></i>
                    <span class='text-danger float-left d-none'>Department is required</span>
                    @if($errors->has('type_of_company'))
                        <span class='text-danger float-left pl-4'> Department is required</span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="size_of_company" class="sr-only d-none">Company Size</label>
                    <input type="number" value="{{old('size_of_company')}}" min="1" class="form-control form-control-lg form-control-solid" name="size_of_company"  placeholder="No.Of Employee" id="size_of_company">
                    <i class="fal fa-users icon"></i>
                    <span class="text-danger float-left d-none">Company Size is required</span>
                </div>
            </div>
            {{--                        <div class="formFlex">--}}
            {{--                            <div class="form-group">--}}
            {{--                                <label for="no_of_drivers" class="sr-only d-none">No. of Drivers</label>--}}
            {{--                                <input type="number" name="no_of_drivers" id="no_of_drivers" value="{{old('no_of_drivers')}}"--}}
            {{--                                       placeholder="How many drivers" class="form-control">--}}
            {{--                                <i class="far fa-male icon"></i>--}}
            {{--                                <span class='text-danger float-left d-none'>No. of Drivers is required</span>--}}
            {{--                            </div>--}}
            {{--                            <div class="form-group">--}}
            {{--                                <label for="no_of_cars" class="sr-only d-none">No. of Cars</label>--}}
            {{--                                <input type="number" name="no_of_cars" id="no_of_cars" value="{{old('no_of_cars')}}"--}}
            {{--                                       placeholder="How many Cars" class="form-control">--}}
            {{--                                <i class="far fa-car icon"></i>--}}
            {{--                                <span class='text-danger float-left d-none'>No. of Cars is required</span>--}}
            {{--                            </div>--}}
            {{--                        </div>--}}
            <div class="form-group">
                <label for="registration_no" class="sr-only d-none">Company Registration #</label>
                <input type="text" name="registration_no" id="registration_no" value="{{old('registration_no')}}"
                       placeholder="Company registration #" class="form-control">
                <i class="flaticon-user icon"></i>
                <span class='text-danger float-left d-none'>Company Registration # is required</span>
                @if($errors->has('registration_no'))
                    <span class='text-danger float-left pl-4'> {{$errors->first('registration_no')}}</span>
                @endif
            </div>
            <div class="formFlex d-block">
                <label class="checkLabel d-flex align-items-center" for="terms">
                    <input type="checkbox" class="form-control mr-3" name="terms" id="terms"
                           value="1" {{(old('terms')=='1')?'checked':''}}>I have read &amp;
                    accepted with the &nbsp<a href="{{route('terms')}}"> Terms & Conditions </a> &nbsp of use </label>
                <span class='text-danger float-left d-none'>Terms &amp; Condition is required</span>

            </div>
            <button class="btn primaryBtn" type="button" onclick="formvalidater('.sec2 .form-control',1)">Signup
            </button>
            <span class="d-block text-right"><a href="../user/login"> Signin?</a></span>

        </div>
    </form>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet"
          type="text/css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        // $(function(){
        //     // document.getElementById("dob").disabled = true;
        //     var dtToday = new Date();
        //     var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
        //     var day = dtToday.getDate();
        //     var year = dtToday.getFullYear() - 18;
        //     if(month < 10)
        //         month = '0' + month.toString();
        //     if(day < 10)
        //         day = '0' + day.toString();
        //     var minDate = year + '-' + month + '-' + day;
        //     var maxDate = year + '-' + month + '-' + day;
        //     $('#dob').attr('max', maxDate);
        // });
        @if($errors->has('company_name')||$errors->has('registration_no')||$errors->has('type_of_company'))
        $(".sec1").hide();
        $(".sec2").show('9000');
        @endif



        @if(old('city'))
        $('#cities').removeAttr('disabled')
        $('#cities').html('<option selected  value="{{old('city')}}">' + window.localStorage.getItem('city') + '</option>');

        @endif
        @if(old('zip'))
        $('#zips').removeAttr('disabled')
        $('#zips').html('<option selected  value="{{old('zip')}}">' + window.localStorage.getItem('zip') + '</option>');
        @endif
        @if($errors->has('company_name')||$errors->has('registration_no')||$errors->has('type_of_company'))
        $(".sec1").hide();
        $(".sec2").show('9000');
            @endif

        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;// jan=0; feb=1 .......
        var day = dtToday.getDate();
        var year = dtToday.getFullYear() - 18;
        if (month < 10)
            month = '0' + month.toString();
        if (day < 10)
            day = '0' + day.toString();
        var maxDate = month + '/' + day + '/' + '/' + year;

        $('#doba').datepicker({
            format: 'mm/dd/yyyy',
            startDate: '01/01/1949',
            endDate: maxDate,
            startView: 2,
            autoclose: true,
            readOnly: true,
            autoFill: false,

        });

        // $("").attr('autocomplete', 'off');
        $("#form-id, #form-id input, #form-id select, #form-id textarea").attr('autocomplete', 'off');

    </script>




@endsection

