


@if(Auth::check())
<script>
    window.location.replace('{{url('/')}}');

</script>
@endif





<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/allstyles.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('site/style.css') }}">
</head>

<body class="">
<div class="accountWrap">
    <header class="header accountHeader">
        <div class="topBarWrap">
            <div class="container-fluid">
                <div class="topBar">
                    <div class="left">
                        <a  href="{{url('/')}}" class="logo"><img src="{{ asset('site/images/logo.png')}}" height="" width="" alt="logo"></a>
                    </div>
                    <div class="right">
                        <nav class="main-menu-nav ">
                            <div class="body-overlay"></div>
                            <div class="menu-toggler"></div>
                            <div class="main-menu-div">
                                <div class="menu-header">
                                    <a href="#" title="Home" class="home-icon fa fa-home"></a>
                                    <span class="close-icon fa fa-times"></span>
                                </div>
                                <ul>
                                    <li class="active">
                                        <a href="{{url('/')}}" title="home">Home</a>
                                    </li>
                                    <li>
                                        <a href="" title="about">Business</a>
                                        <ul>
                                            <li><a href="{{url('/adv/sign-up')}}">Business Signup</a></li>
                                            <li><a href="{{ url('/terms') }}">Advertiser Benefits</a></li>
                                            <li><a href="{{ url('/how-it-works') }}">How it work?</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/about') }}" title="about">About</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/contact') }}" title="about">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="headerButtons">
                            <button class="btn adverBtn" onclick="window.location.href='{{url('adv/sign-up')}}';">Advertiser Signup</button>
                            <button class="btn adverBtn" onclick="window.location.href='{{url('drv/sign-up')}}';">Driver Signup</button>
                            {{--                        <button class="btn driverBtn">Driver Signup</button>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="main">
        <section class="enquirySection">
            <h1>Signin</h1>
            @if(isset($errors)&& count($errors)>0)
                <span  class="text-danger">Credentials not match</span>
            @endif
            @if(Session::has('unauthorize'))
                <span class="text-danger">{{ Session::get('unauthorize') }}</span>
            @endif
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group">
                    <input type="email" name="email" placeholder="Email" class="form-control" required>
                    <i class="flaticon-user icon"></i>
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Password" class="form-control" required>
                    <i class="flaticon-lock icon"></i>
                </div>
                <div class="formFlex form-group">
                    <a href="{{route('forgetPassword')}}">Forgot Password?</a>
                </div>
                <button type="submit" class="btn primaryBtn">Sign In</button>
            </form>
        </section>
    </main>
</div>
<script type="text/javascript " src="{{ asset('site/js/jquery.min.js')}} "></script>
<script src="{{ asset('site/js/plugins.js')}} "></script>
<script src="{{ asset('site/js/system.js')}}"></script>
</body>

</html>
