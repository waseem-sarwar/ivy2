@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout='layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout='layout.driver'; ?>
@elseif($role=='2')
    <?php $layout='layout.default'; ?>
@elseif($role=='1')
    <?php $layout='layout.admin'; ?>
@endif
@extends($layout)
@section('content')

    <style>
        .StripeElement {
            box-sizing: border-box;
            width:100%;
            height: 40px;
            padding: 10px 12px;
            border: 1px solid #E4E6EF;;
            border-radius: 4px;
            background-color: white;
            box-shadow: 0 1px 3px 0 #e6ebf1;
            -webkit-transition: box-shadow 150ms ease;
            transition: box-shadow 150ms ease;
        }
        #payment-form button{
            margin-top:15px;
        }

        .StripeElement--focus {
            box-shadow: 0 1px 3px 0 #cfd7df;
        }

        .StripeElement--invalid {
            border-color: #fa755a;
        }

        .StripeElement--webkit-autofill {
            background-color: #fefde5 !important;
        }

        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
        .asdfasdf {
            border: 1px solid #ffffff;
            margin-top: 18px;
            padding: 14px 19px;
            border-radius: 6px;
            box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
        }
        ul.compainlist li {
            margin-top: 8px;
            font-size: 15px;
        }
        .owpierjpwoq {
            font-size: 52px;
            color: #3c9bd1;
            margin-top: 11px;
            margin-right: 19px;
        }
    </style>


    @if($role=='1' || $role=='2')
        @php $totalmiles=0; @endphp
        @php $totalmiles= $detail->mileage*$detail->campaign->campaign_total_days*$detail->no_of_cars;  @endphp
    @endif


    {{--    @if(Auth::user()->role_id=='3'||Auth::user()->role_id=='4')--}}
    {{--            @php $miles=0; @endphp--}}
    {{--            @php $driverprofit=0; @endphp--}}
    {{--            @php $per_day_mile=0; @endphp--}}
    {{--            @php $id=null; @endphp--}}
    {{--            @php $applieddriverslimit=0; @endphp--}}
    {{--            @php $state1=''; @endphp--}}
    {{--            @php $city1=''; @endphp--}}
    {{--            @php $zip1=''; @endphp--}}

    {{--            @if( !is_object($claim))--}}
    {{--            @foreach($detail->locations as $location)--}}
    {{--                        @foreach($detail->cars as $car)--}}
    {{--                            @if(@$location->state_id== Auth::user()->enrolment->driving_state && $car->car_location==$location->id )--}}
    {{--                                @if($car->no_of_cars==$car->assigned_drivers)--}}
    {{--                                    @php $applieddriverslimit=1; @endphp--}}
    {{--                                @endif--}}
    {{--                                @if(Auth::user()->role_id=='3'  && $location->state->id==Auth::user()->enrolment->driving_state )--}}
    {{--                                        @php $carzips = explode(",", $car->zip); @endphp--}}
    {{--                                    @foreach ($carzips as $carzip)--}}
    {{--                                            @if($carzip == Auth::user()->enrolment->car_drive_zip )--}}
    {{--                                                @php $id=$location->id @endphp--}}
    {{--                                                @php $miles = $car->mileage*$detail->campaign_total_days @endphp--}}
    {{--                                                @php $per_day_mile= $car->mileage @endphp--}}
    {{--                                                @php $driverprofit= number_format(($detail->campaign_total_days*($car->price_per_mile*80/100)),2) @endphp--}}
    {{--                                             @endif--}}
    {{--                                    @endforeach--}}
    {{--                                 @endif--}}
    {{--                             @endif--}}
    {{--                         @endforeach--}}
    {{--            @endforeach--}}

    {{-- @else--}}
    {{--    @php $state1=$claim->location->state->state; @endphp--}}
    {{--    @php $city1=$claim->location->city->primary_city; @endphp--}}
    {{--    @php $zip1=$claim->applied_zip; @endphp--}}


    {{--    @foreach($detail->locations as $location)--}}
    {{--        @foreach($detail->cars as $car)--}}
    {{--            @if( $car->car_location==$location->id )--}}
    {{--                @if($car->no_of_cars==$car->assigned_drivers)--}}
    {{--                    @php $applieddriverslimit=1; @endphp--}}
    {{--                @endif--}}
    {{--                    @php $carzips = explode(",", $car->zip); @endphp--}}
    {{--                    @foreach ($carzips as $carzip)--}}
    {{--                        @if($carzip == $zip1)--}}
    {{--                            @php $id=$location->id @endphp--}}
    {{--                            @php $miles = $car->mileage*$detail->campaign_total_days @endphp--}}
    {{--                            @php $per_day_mile= $car->mileage @endphp--}}
    {{--                            @php $driverprofit= number_format(($detail->campaign_total_days*($car->price_per_mile*80/100)),2) @endphp--}}
    {{--                        @endif--}}
    {{--                    @endforeach--}}
    {{--            @endif--}}
    {{--        @endforeach--}}
    {{--    @endforeach--}}


    {{-- @endif--}}
    {{--@endif--}}
    {{--    @php dd($zip) @endphp--}}


    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <!--begin::Details-->
                    <div class="d-flex mb-2">
                        <!--begin: Pic-->
                        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                            <div class="symbol symbol-50 symbol-lg-120">
                            </div>
                            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                            </div>
                        </div>
                        <!--end::Pic-->
                        <!--begin::Info-->
                        <div class="flex-grow-1">
                            <!--begin::Title-->
                            <div class="d-flex justify-content-between flex-wrap ">
                                <div class="d-flex mr-3">
                                    <a href="#" style="cursor:default" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">{{$detail->campaign->title }} </a>
                                    <a href="#">
                                        <i class="@if(@$detail->status_id=='3')flaticon2-correct @else  flaticon2-cross @endif text-{{@$detail->status->class}} font-size-h5"></i>
                                    </a>
                                </div>
                                <div class="my-lg-0 my-3">
                                    {{--                                        @if(Auth::user()->role_id=='1')--}}
                                    {{--                                            @if($detail->status_id!='3')--}}
                                    {{--                                                <a href="javascript:;" data-toggle="modal" data-target="#statusUpdate" onclick="status('{{$detail->status_id}}','{{$detail->id}}')" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Update Status">--}}
                                    {{--                                                    <i class="la la-wrench"></i>--}}
                                    {{--                                                </a>--}}
                                    {{--                                                @endif--}}
                                    {{--                                        @endif--}}
                                    @if(Auth::user()->role_id == '1' || Auth::user()->role_id == '2')
                                        @if($detail->sticker->sticker_id=='0' && $detail->sticker->bespoke_status=='Waiting for sticker payment')
                                            <span class="example-tools justify-content-center cursor-auto ">
                                              <a style="min-width:145px !important" onclick="getdata1({{$detail->sticker}},{{$detail->campaign_id}});" class="btn btn-sm btn-default btn-text-primary btn-hover-success btn-icon cursor-pointer" data-toggle="tooltip" title="" data-original-title="Sticker Payment: Pending">
                                                            Pay Now</a>
                                             </span>
                                   @endif

                                        <span class="example-tools justify-content-center cursor-auto ">
                                                @if(Auth::user()->role_id == '2' && $detail->payment_status=='Pending' && $detail->status_id=='3'  )
                                                <a style="min-width:145px !important"  onclick="getdata({{$detail}});" class=" btn btn-sm btn-default btn-text-primary btn-hover-success btn-icon cursor-pointer" data-toggle="tooltip" title="" data-original-title="Payment Pending">
                                                       Pay Now</a>
                                                @endif

                                                  <a onclick="getCampaignId({{$detail->payment_status}});"
                                                     class=" btn btn-sm btn-light btn-text-primary btn-hover-success btn-icon cursor-pointer"
                                                     data-toggle="tooltip" title="{{$detail->payment_status}}">
                                                    <i class="fa fa-credit-card text-warning"></i></a
                                        </span>
                                        @if($detail->sticker->sticker_id=='0' && $detail->sticker->bespoke_status=='Paid')
                                            <span class="example-tools justify-content-center cursor-auto ">
                                                         <a class=" btn btn-sm btn-light btn-text-success btn-hover-info btn-icon cursor-pointer"
                                                            data-toggle="tooltip" title="Sticker payment status: {{$detail->sticker->bespoke_status}} | Amount: {{$detail->sticker->bespoke_price}}| Manufactured date: {{$detail->sticker->bespoke_days}}">
                                                        <i class="fa fa-credit-card text-success"></i></a>
                                                   </span>
                                        @endif

                                        <a class="btn btn-light-warning font-weight-bold text-right" style="cursor: default">{{@$detail->status->name}} </a>&nbsp
                                    @endif
                                    @php  $campaign_zips='' @endphp
                                    @if(Auth::user()->role_id=='3')
                                        @csrf
                                        @if($claim =='0' && $detail->status->id=='3' && $detail->payment_status=='Paid' )
                                            @php  $zips=explode(",",$detail->zip)@endphp
                                            @foreach($zips as $zip)
                                                @php  $campaign_zips.= ' '.$zip @endphp
                                                @if( Auth::user()->enrolment->car_drive_zip == $zip  && $detail->completed_drivers<$detail->no_of_cars && $detail->status->id!='4')
                                                    <button onclick="claim('{{$detail->id}}')" class="btn btn-light-primary font-weight-bold text-right" style="">Apply for campaign</button>
                                                @endif
                                            @endforeach
                                        @elseif(isset($claim->status) && $claim->availability=='0')
                                            <button onclick="availabilityupdate('{{$detail->id}}')" class="btn btn-light-primary font-weight-bold text-right" >Availability confirmation</button>
                                        @elseif(isset($claim->status) && $claim->availability!='2')
                                            <a disabled class="btn btn-light disabled  font-weight-bold text-right">{{$claim->status}}</a>
                                        @endif
                                        @if(isset($working_status) )
                                            <a class="btn btn-light-warning font-weight-bold text-right">{{$working_status}}{{$claim}}</a>
                                        @endif
                                        @if(@$claim->status=='Assigned')
                                            <button onclick="finshCampaign('{{$detail->id}}')" data-toggle="modal" data-target="#finshCampaign" class="btn btn-light-primary font-weight-bold text-right" style="">Finish Campaign</button>
                                        @endif
                                        @if(isset($claim->availability) && $claim->availability=='2')
                                            <span class="example-tools justify-content-center " >
                                                <a style="cursor: default" class=" btn btn-sm btn-danger btn-text-primary btn-icon " data-toggle="tooltip" title="" data-original-title="Not available">
                                                <i class=" far fa-times-circle"></i></a>
                                          </span>
                                        @endif
                                    @endif
                                    {{--                                                                                <button class="btn btn-light-primary font-weight-bold" onclick="window.print()"><i class="fa fa-print" aria-hidden="true"></i>print</button>--}}
                                    {{--                                    <button class="btn btn-light-primary font-weight-bold text-right"  onclick="window.print()"><i class="fa fa-download" aria-hidden="true"></i>download</button>--}}
                                </div>
                            </div>

                            <div class="d-flex flex-wrap justify-content-between mt-1">
                                <div class="d-flex flex-column flex-grow-1 pr-8">
                                    <div class="d-flex flex-wrap mb-2">
                                        <a href="#" class="text-dark-50  mr-10 text-hover-primary font-weight-bold">
                                            <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>CMP-{{@$detail->id }}</a>
                                        @if(Auth::user()->role_id == '1' || Auth::user()->role_id == '2')
                                            <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                                <i class="flaticon2-new-email mr-2 font-size-lg"></i>{{@$detail->user->email}}</a>
                                            {{--                                            <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">--}}
                                            {{--                                                <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>{{$detail->user->profile->job_designation}}</a>--}}
                                            {{--                                            <a href="#" class="text-dark-50 text-hover-primary font-weight-bold">--}}
                                            {{--                                                <i class="flaticon2-placeholder mr-1 font-size-lg "></i>{{@$detail->user->profile->country->name}}--}}
                                            {{--                                                &nbsp</a>--}}


                                            {{--                                            {{dd($detail->locations[0]->state)}}--}}

                                            {{--                                        {{dd($location->state->state)}}--}}

                                            <a href="#" class="text-dark-50 text-hover-primary font-weight-bold">
                                                <i class="flaticon2-placeholder mr-2 font-size-lg"></i>{{@$detail->state->state }}</a>
                                            @if($detail->payment)
                                                <a href="javascript:" data-toggle="modal" data-target="#viewPaymentDetailModal" title="Payment info"> <i class="fa fa-info-circle ml-1 text-success" style="padding-top: 3px;" ></i></a>
                                            @endif
                                        @endif
                                        {{--                                            <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">--}}
                                        {{--                                                <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i>{{$detail->user->profile->job_designation}}</a>--}}
                                        {{--                                            <a href="#" class="text-dark-50 text-hover-primary font-weight-bold">--}}
                                        {{--                                                <i class="flaticon2-placeholder mr-1 font-size-lg "></i>{{@$detail->user->profile->country->name}}--}}
                                        {{--                                                &nbsp</a>--}}
                                        {{--                                            @if($detail->payment)--}}
                                        {{--                                            <a href="javascript:" data-toggle="modal" data-target="#viewPaymentDetailModal" title="Payment info"> <i class="fa fa-info-circle ml-1 text-success" style="padding-top: 3px;" ></i></a>--}}
                                        {{--                                                @endif--}}

                                        {{--                                            {{dd($detail->locations[0]->state)}}--}}

                                        {{--                                        {{dd($location->state->state)}}--}}



                                    </div>
                                    {{--                                    <span class="font-weight-bold text-dark-50">I distinguish three main text objectives could be merely to inform people.</span>--}}
                                    {{--                                    <span class="font-weight-bold text-dark-50">A second could be persuade people.You want people to bay objective</span>--}}
                                </div>

                            </div>
                            <!--end::Content-->
                        </div>
                        <!--end::Info-->
                    </div>
                    <!--end::Details-->

                    @if($detail->rejected_comment)
                        <div class="alert alert-danger  text-center">
                            <strong>Rejected comment: {{$detail->rejected_comment}}</strong>
                        </div>
                    @endif
                    <div class="separator separator-solid"></div>
                    <!--begin::Items-->
                    <div class="d-flex align-items-center flex-wrap mt-8">
                        <!--begin::Item-->

                        @if(Auth::user()->role_id=='1'||Auth::user()->role_id=='2')
                            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                                    <span class="mr-4">
                                      <i class="icon-xl fas fa-car-alt display-4 text-muted font-weight-bold"></i>
                                    </span>
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">No. of Campaign Days</span>
                                    <span class="font-weight-bolder font-size-h5">
                                        <span class="text-dark-50 font-weight-bold"></span>  {{@$detail->campaign->campaign_total_days}} <small>days</small></span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                              <span class="mr-4">
                                  <i class="icon-xl fas fa-car-alt display-4 text-muted font-weight-bold"></i>
                              </span>
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">Number of Cars</span>
                                    <span class="font-weight-bolder font-size-h5">
                                        <span class="text-dark-50 font-weight-bold"></span>{{$detail->no_of_cars}}</span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
<span class="mr-4">
<i class="icon-xl fas fa-car-alt display-4 text-muted font-weight-bold"></i>
</span>
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">Grand Cost</span>
                                    <span class="font-weight-bolder font-size-h5">
<span class="text-dark-50 font-weight-bold"></span><td>{{$detail->total_price}}</td></span>
                                </div>
                            </div>      <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                            <span class="mr-4">
                            <i class="icon-xl fas fa-car-alt display-4 text-muted font-weight-bold"></i>
                            </span>
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">Grand Miles</span>
                                    <span class="font-weight-bolder font-size-h5">
                                <span class="text-dark-50 font-weight-bold"></span><td>{{@number_format($totalmiles)}}</td></span>
                                </div>
                            </div>
                        @endif
                        @if(Auth::user()->role_id=='3')
                            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">Miles per day</span>
                                    <span class="font-weight-bolder font-size-h5">
                                <span class="text-dark-50 font-weight-bold"></span>{{$detail->mileage}}</span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">Total miles to covered</span>
                                    <span class="font-weight-bolder font-size-h5">
                                <span class="text-dark-50 font-weight-bold"></span>{{$detail->mileage*$detail->campaign->campaign_total_days}}</span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">Duration</span>
                                    <span class="font-weight-bolder font-size-h5">
                                    <span class="text-dark-50 font-weight-bold"></span>{{$detail->campaign->campaign_total_days}}<small> days</small></span>
                                </div>
                            </div>
                            <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                                <div class="d-flex flex-column text-dark-75">
                                    <span class="font-weight-bolder font-size-sm">Approx. Profit</span>
                                    <span class="font-weight-bolder font-size-h5">
                                  <span class="text-dark-50 font-weight-bold"></span>{{($detail->price_per_mile*$detail->campaign->campaign_total_days)*0.8}}</span>
                                </div>
                            </div>
                            @if(@$claim['amount']!=0)
                                <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                                    <div class="d-flex flex-column text-dark-75">
                                        <span class="font-weight-bolder font-size-sm">Amount Earned</span>
                                        <span class="font-weight-bolder font-size-h5">
                                  <span class="text-danger-50 font-weight-bold "></span>{{$claim['amount']}}</span>
                                    </div>
                                </div>
                            @endif
                        @endif

                        @php  $interval=explode("-",$detail->campaign->interval)@endphp
                        <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
                        <span class="mr-4">
                        <i class="icon-xl far fa-calendar-minus display-4 text-muted font-weight-bold"></i>
                        </span>
                            <div class="d-flex flex-column text-dark-75">
                                <span class="font-weight-bolder font-size-sm">Start Date</span>
                                <span class="font-weight-bolder font-size-h5">
<span class="text-dark-50 font-weight-bold"></span>{{ Carbon\Carbon::parse($interval[0])->format('d M Y') }}</span>
                            </div>
                        </div>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <div class="d-flex align-items-center flex-lg-fill mr-5 mb-2">
      <span class="mr-4">
          <i class="icon-xl far fa-calendar-minus display-4 text-muted font-weight-bold"></i>
      </span>
                            <div class="d-flex flex-column flex-lg-fill">
                                <span class="text-dark-75 font-weight-bolder font-size-sm">End Date</span>
                                <span class="font-weight-bolder font-size-h5">
<span class="text-dark-50 font-weight-bold"></span>{{ Carbon\Carbon::parse($interval[1])->format('d M Y') }}</span>
                            </div>
                        </div>
                    </div>
                    <!--begin::Items-->
                </div>
            </div>
            <!--end::Card-->
            <!--begin::Row-->

            @if($role == '1' || $role == '2')
                <div class="row">
                    <div class="col-lg-4">
                        <!--begin::Mixed Widget 14-->
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title font-weight-bolder">Campaign Details </h3>
                                @if($role=='2' && $detail->status_id=='6'  &&  $detail->is_edit=='1')
                                    <i data-toggle="modal" data-target="#edit1"
                                       title="Edit"
                                       class="fas fa-edit float-right cursor-pointer pt-5"></i>
                                @endif
                            </div>
                            <!--end::Header-->
                            <div class="">
                                <div class="col-lg-12 ">
                                    <div class=" ueytui " >
                                        {{--                                    <h4>Campaign Metrics1</h4>--}}
                                        <div class="d-flex border-bottom mb-0 pb-0" style="justify-content: space-between;">

                                        <span>
                                      <span class="uwpoms"> <i class="fas fa-tachometer-alt owpierjpwoq"></i></span>
                                    </span>
                                        </div>
                                        <span>
                                         <small class="mb-1 mt-2"><b>Campaign Type</b></small>
                                        <h5 class="mb-5">{{@$detail->campaign->typee->name}}</h5>
                                        </span>
                                        <span>
                                         <small class="mb-1 mt-2"><b>Campaign Start</b></small>
                                        <h5 class="mb-5">{{ Carbon\Carbon::parse($interval[0])->format('d M Y') }}</h5>
                                        </span>
                                        <span>
                                             <small class="mb-1 mt-4"><b>Campaign Ends</b></small>
                                            <h5 class="mb-5">{{ Carbon\Carbon::parse($interval[1])->format('d M Y') }}</h5>
                                            </span>

                                    </div>
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Mixed Widget 14-->
                    </div>

                    <div class="col-lg-8">
                        <!--begin::Advance Table Widget 2-->
                        <div class="card card-custom card-stretch gutter-b">
                            <!--begin::Header-->
                            <div class="card-header border-0 pt-5">
                                <h3 class="card-title align-items-start flex-column mb-0">
                                    <span class="card-label font-weight-bolder text-dark">Car Details</span>
                                    {{--                                <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new members</span>--}}
                                </h3>

                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body  pb-0 mt-n3 pt-0">
                                <div class="tab-content mt-5" id="myTabTables11">
                                    <!--begin::Tap pane-->

                                    <!--end::Tap pane-->
                                    <!--begin::Tap pane-->
                                    <div class="tab-pane fade show active" id="kt_tab_pane_11_3" role="tabpanel" aria-labelledby="kt_tab_pane_11_3">
                                        <!--begin::Table-->
                                        <div class="table-responsive">
                                            <table class="table text-center  table-bordered table-vertical-center">
                                                <thead>
                                                <tr>
                                                    {{--                                                    @if($role=='2' && $detail->status_id=='6'&& $detail->cars->count()>1)<th class="border-0"></th>@endif--}}
                                                    <th>State</th>
                                                    <th>City</th>
                                                    {{--                                                    <th >Car Make</th>--}}
                                                    {{--                                                    <th >Model</th>--}}
                                                    {{--                                                    <th >Type</th>--}}
                                                    <th >Types</th>
                                                    <th >Models</th>
                                                    <th >Number Of Cars</th>
                                                    <th>Miles<small>/day</small></th>
                                                    @if($role=='1')<th>Miles Price</th> @endif
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                @php $subtotal=0 @endphp
                                                <tr>
                                                    <th class="d-flex py-5"><span class="pt-4">{{@$detail->state->state}} </span>
                                                        <a class=" btn btn-sm btn-default btn-text-primary  border-0  pl-0 ml-0"  style='background-color: white; padding-top: 10px !important'  data-toggle="tooltip"  data-original-title="Zips: {{($detail->zip)?$detail->zip:'N/A'}}  Drive days: {{($detail->drive_days)?$car->drive_days:'24/7'}}" >
                                                            <span class="pt-3"> <i class="fa fa-info-circle ml-1 " style="padding-top: 5px;"></i></span>
                                                        </a>
                                                    </th>
                                                    <th >{{@$detail->city->primary_city}}</th>
                                                    {{--                                                        <th >{{@ucfirst($detail->car_make->company)}}</th>--}}
                                                    {{--                                                        <th >{{@$detail->car_name->car_name}}</th>--}}
                                                    {{--                                                        <th >{{@$detail->car_type->name}}</th>--}}
                                                    {{--                                                        <th >{{@$detail->car_model->model_name}}</th>--}}
                                                    <th >{{@$car_types}}</th>
                                                    <th >{{@$detail->year_range}}</th>
                                                    <th >{{@$detail->no_of_cars}}</th>
                                                    <th >{{$detail->mileage}}</th>
                                                    @if($role=='1' ) <th >{{$detail->price_per_mile}}</th> @endif
                                                    @if($role=='1' )<th>${{$detail->total_price}}</th>@endif
                                                    @if($role=='2' )<th >${{$detail->total_price}}</th>@endif
                                                    {{--                                                        @php $subtotal=($detail->no_of_cars*$detail->price_per_mile) @endphp--}}
                                                </tr>

                                                {{--                                                @if($role=='1' )--}}
                                                {{--                                                    <tr>--}}
                                                {{--                                                        <th style="border:0px" colspan="8" ></th>--}}
                                                {{--                                                        <th >Cost <small>per day</small></th>--}}
                                                {{--                                                        <th >${{@number_format($subtotal,2)}}</th></tr>--}}
                                                {{--                                                @endif    @if( $role=='2' )--}}
                                                {{--                                                    <tr>--}}
                                                {{--                                                        <th style="border:0px" colspan="7" ></th>--}}
                                                {{--                                                        <th >Cost <small>per day</small></th>--}}
                                                {{--                                                        <th >${{@number_format($subtotal,2)}}</th></tr>--}}
                                                {{--                                                @endif--}}
                                                {{--                                --}}
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--end::Table-->
                                    </div>
                                    <!--end::Tap pane-->
                                </div>
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Advance Table Widget 2-->
                    </div>

                </div>
        @endif
        <!--end::Row-->
            <div class="row">
                <div class="col-lg-4">
                    <!--begin::Mixed Widget 14-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5">
                            <h3 class="card-title font-weight-bolder">Location Details</h3>

                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="asdfasdf "style="box-shadow:none !important">
                            {{--                                            <p class="border-bottom">Location Details </p>--}}
                            <div class="d-flex border-bottom mb-1 pb-2" style="justify-content: space-between;">
                        <span class="pt-5">


                         <small class="mb-1 mt-5"><b>
                             {{$detail->state->state}}
                             </b></small>
                        <h4>{{@$detail->country}}</h4>
                        </span>
                                <span>
                              <span class="uwpoms"> <i class="fas fa-map-marked-alt owpierjpwoq"></i></span>
                        </span>
                            </div>
                            <div  class="w-100 " id="dvMap" style=" height: 280px"> </div>
                        </div>

                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 14-->
                </div>
                <div class="col-lg-4">
                    <!--begin::Mixed Widget 14-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5 mb-3">
                            <h3 class="card-title font-weight-bolder">Sticker Details</h3>

                            <div class="mt-n5">
                                @if($role=='2' && $detail->status_id=='6' &&  $detail->is_edit=='1')
                                    &nbsp <span data-toggle="modal" data-target="#edit2"
                                                title="Edit"
                                                class="fas fa-edit float-right cursor-pointer " style="color: #b5b5c3"></span>

                                @endif
                                @if($role=='2'|| $role=='1')
                                    <a href="{{asset('/images/campaign-sticker/'.$detail->sticker->image_url)}}" download><span class="fas fa-download float-right cursor-pointer pr-1"></span></a>
                                @endif
                                <div class=" ueytui mt-n5" >
                                    {{--                                    <h4>Sticker Detials</h4>--}}
                                    <div class="d-flex  mb-4 pb-2" style="justify-content: space-between;">
<span class="pt-5">
<small class="mb-1 mt-5"><b></b></small>
<h4></h4>
</span>
                                        <span>
         <span class="uwpoms"> <i class="fas fa-image owpierjpwoq"></i></span>
       </span>
                                    </div>
                                    <table class="table">
                                        <tbody><tr>
                                            <th>Company:</th>
                                            <td>{{@$detail->sticker->detail}}</td>
                                        </tr>
                                        <tr>
                                            <th>Slogan:</th>
                                            <td>{{@$detail->sticker->slogan}}</td>
                                        </tr>
                                        <tr>
                                            <th>Contact:</th>
                                            <td>{{@$detail->sticker->contact}}</td>
                                        </tr>
                                        <tr>
                                            <th>Detail:</th>
                                            <td>{{@$detail->sticker->sticker_details}}</td>
                                        </tr>

                                        @if((int)$detail->sticker->sticker_id==0)
                                            {{$detail->sticker_id}}
                                            <tr>
                                                <th>Bespoke Status:</th>
                                                <td>Yes</td>
                                            </tr>
                                        @endif
                                        </tbody></table>
                                </div>
                                {{--                                    <p class="mb-0">Sticker Picture:</p>--}}
                                @php if(@$detail->sticker->image_url!=null){
   $url='/images/campaign-sticker/'.$detail->sticker->image_url;
   }else{
   $url='/media/uploadimge.png';
   } @endphp

                                <img src="{{asset($url)}}" class=" img-thumbnail"  width="100%" alt="" style="max-height: 150px;cursor: pointer" data-toggle="modal" data-target="#img-preview">
                                {{--                                    <img src="{{storage_path().'/app/stickers/1613482956.png'}}" class=" img-thumbnail"  width="100%" alt="" style="max-height: 230px;cursor: pointer" data-toggle="modal" data-target="#img-preview">--}}
                                {{--                                    <img src="{{url('/storage/app/stickers/1613482956.png')}}" class=" img-thumbnail"  width="100%" alt="" style="max-height: 230px;cursor: pointer" data-toggle="modal" data-target="#img-preview">--}}
                                @if((int)$detail->sticker->sticker_id>0)
                                    <img src="{{asset('/storage/images/stickers/'.$detail->sticker->sticker_url)}}" class=" img-thumbnail"  width="100%" alt="" style="max-height: 150px;cursor: pointer" data-toggle="modal" data-target="#img-preview1">
                                @endif
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->

                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 14-->
                </div>
                <div class="col-lg-4">
                    <!--begin::Mixed Widget 14-->
                    <div class="card card-custom card-stretch gutter-b">
                        <!--begin::Header-->
                        <div class="card-header border-0 pt-5 mb-3">
                            <h3 class="card-title font-weight-bolder">Locations</h3>
                            <i class="far fa-chart-bar float-right mt-3 owpierjpwoq"></i>
                        </div>
                        <table class="table text-center ">
                            <tr class="">
                                <th>State</th>
                                <th>City</th>
                                <th>Zip</th>
                            </tr>
                            <tr>
                                <td>{{$detail->state->state}}</td>
                                <td>{{$detail->city->primary_city}}</td>
                                <td>
                                    {{ $detail->zip}}
                                </td>
                            </tr>




                            {{--                            @if(Auth::user()->role_id=='3' )--}}

                            {{--                                        <tr>--}}
                            {{--                                            <td>{{$i++}}</td>--}}
                            {{--                                            <td>{{$state1}}</td>--}}
                            {{--                                            <td>{{$city1}}</td>--}}
                            {{--                                            <td>--}}
                            {{--                                                {{ $zip1}}--}}
                            {{--                                            </td>--}}
                            {{--                                        </tr>--}}

                            {{--                            @endif--}}




                        </table>
                        <!--end::Header-->
                        <!--begin::Body-->

                        <!--end::Body-->
                    </div>
                    <!--end::Mixed Widget 14-->
                </div>

            </div>
            <!--begin::Row-->

            <!--end::Row-->
        </div>
        <!--end::Container-->
    </div>
    <div class="modal fade show" id="statusUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Campaign Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-4">
                                        <div class="row">
                                            <div class="col-md-12 mt-4 ">
                                                <span class="text-center d-block"><h5 class="d-inline">Campaign Id: </h5> <span id="camp_id"></span></span>
                                            </div><br><br><br>


                                            <div class="col-lg-12">
                                                <form id="form-id2" method="POST" action="{{ route('campaign.status.update') }}" >
                                                    @csrf
                                                    <input type="hidden" name="car_id" id="camp-id" value="{{$detail->id}}">
                                                    <input type="hidden" name="campaigner_email" value="{{$detail->user->email}}">
                                                    <input type="hidden" name="campaigner_name" value="{{$detail->user->name}}">
                                                    <div class="form-check">
                                                        <label>Status Update:</label>
                                                        <select name="s_u" class=" form-control" id="stat1" required>
                                                            <option selected="" disabled="">
                                                                Choose one option
                                                            </option>
                                                            @foreach($status as $stats)
                                                                <option <?php ($detail->status_id==$stats->id)?'selected':'' ?>value="{{$stats->id}}">{{$stats->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="form-text text-danger status_error"> </span>
                                                    </div>
                                                </form>
                                            </div>

                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create="" onclick="formCheck();" class="btn btn-sm font-weight-bolder btn-light-primary">
                        Update
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="edit1" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Campaign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-4">
                                        <form id="form-id4" method="POST" action="{{ route('user.campaign.update')}}" >
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12 mt-4 ">
                                                    {{--                                                    <span class="text-center d-block"><h5 class="d-inline">Campaign Id: </h5> <span id="camp_id"></span></span>--}}
                                                </div><br><br>
                                                <div class="col-lg-11">
                                                    <input type="hidden" name="camp_id"  value="{{$detail->campaign->id}}">
                                                    <input type="hidden" name="car_id"  value="{{$detail->id}}">
                                                    <div class="form-check">
                                                        <label>Campaign Title:</label>
                                                        <input name="camp_title" id="camp_titlee" class="form-control" value="{{$detail->campaign->title}}" required/>

                                                        <span class="form-text text-danger camp_titlee_error"> </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-11">
                                                    <div class="form-check">
                                                        <label>Campaign type</label>
                                                        <select name="camp_type" id="camp_typee" class=" form-control"  required>
                                                            <option selected  disabled="">
                                                                Choose one option
                                                            </option>
                                                            @foreach($campaign_types_dropdown as $campaign_type)
                                                                <option {{($detail->campaign->type==$campaign_type->id)?'selected':''}} value="{{$campaign_type->id}}">{{$campaign_type->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="form-text text-danger camp_typee_error"> </span>
                                                    </div>
                                                    <div class="col-lg-12 pt-1 pr-0">
                                                        <div class="pl-1">
                                                            <label>Duration:</label>
                                                            <input name="time_interval" class="form-control" notify="Interval"
                                                                   id="kt_daterangepicker_1" readonly="readonly"
                                                                   placeholder="Set time interval" type="text">
                                                            <span class="form-text text-danger" id="interval_error"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" id="frm-btn" onclick="formCheck1();" class="btn btn-sm font-weight-bolder btn-light-primary">
                        Update
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="edit2" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Sticker details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-4">
                                        <form id="form-id5" method="POST" action="{{ route('user.campaign.sticker.update')}}" enctype="multipart/form-data" >
                                            @csrf
                                            <div class="row">
                                                <div class="col-md-12 mt-3 ">
                                                    {{--                                                    <span class="text-center d-block"><h5 class="d-inline">Campaign Id: </h5> <span id="camp_id"></span></span>--}}
                                                </div>
                                                <div class="col-lg-11">
                                                    <input type="hidden" name="camp_id"  value="{{$detail->campaign_id}}">
                                                    <div class="form-check">
                                                        <label>Company:</label>
                                                        <input name="company" id="company" class="form-control" value="{{$detail->sticker->detail}}" required/>
                                                        <span class="form-text text-danger company_error"> </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-11">
                                                    <div class="form-check">
                                                        <label>Slogan:</label>
                                                        <input name="slogan" id="slogan" class="form-control" value="{{$detail->sticker->slogan}}" required/>
                                                        <span class="form-text text-danger slogan_error"> </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-11">
                                                    <div class="form-check">
                                                        <label>Contact:</label>
                                                        <input name="contact" id="contact" class="form-control" type="tel" value="{{$detail->sticker->contact}}" required/>
                                                        <span class="form-text text-danger contact_error"> </span>
                                                    </div>
                                                </div>       <div class="col-lg-11">
                                                    <div class="form-check">
                                                        <label>Sticker Details:</label>
                                                        <textarea name="sticker_details" id="sticker_details" class="form-control" >{{$detail->sticker->sticker_details}}</textarea>
                                                        <span class="form-text text-danger sticker_details_error"> </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-11">
                                                    <div class="form-check">
                                                        <label>Sticker:</label>
                                                        <input name="sticker" id="sticker" class="form-control"  type="file" accept="image/*" required/>
                                                        <span class="form-text text-danger sticker_error"> </span>
                                                    </div>
                                                </div>

                                            </div>
                                        </form>
                                    </div>

                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" id="frm-btn1" onclick="formCheck2();" class="btn btn-sm font-weight-bolder btn-light-primary">
                        Update
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="img-preview" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: none;padding-bottom:0px;">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="col-lg-12">
                                        <img src="{{asset($url)}}" class="img-thumbnail"  width="100%" alt=""  height="100%" >
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade show" id="img-preview1" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: none;padding-bottom:0px;">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="col-lg-12">
                                        <img src="{{asset('/storage/images/stickers/'.$detail->sticker->sticker_url)}}" class=" img-thumbnail"  width="100%" alt=""  height="100%" >
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="finshCampaign" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <form action="{{route('user.campaign.finish.documents')}}" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title">Finish Campaign Documents</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <i aria-hidden="true" class="ki ki-close"></i>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="card-body p-4">
                                    <!--begin::Invoice-->
                                    <div class="row justify-content-center pt-4 px-4 pt-md-4 ">
                                        <div class="col-md-12">
                                            <div id="kt_repeater_1">
                                                <div class="form-group row" id="kt_repeater_1">
                                                    <div data-repeater-list="">
                                                        <div data-repeater-item="" class="form-group row align-items-center">
                                                            <div class="col-md-6">
                                                                <label>Latest Mileage picture* </label>
                                                                <input type="file" class="form-control" name="mileage_pic" required accept="image/*">
                                                                <div class="d-md-none mb-2"></div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <label>Sticker pasted picture* </label>
                                                                <input type="file" class="form-control" name="sticker_pic" required accept="image/*">
                                                                <div class="d-md-none mb-2"></div>
                                                            </div>
                                                            <div class="col-md-6 mt-4">
                                                                <label>Latest Miles of car* </label>
                                                                @csrf
                                                                <input type="number" class="form-control" placeholder="Enter current miles of car" name="miles" required>
                                                                <input type="hidden"  name="campaign_id" id="campaign-id">
                                                                <div class="d-md-none mb-2"></div>
                                                            </div>
                                                            {{--                                                            <div class="col-md-6 mt-4">--}}
                                                            {{--                                                                <label>Latest Miles of car* </label>--}}
                                                            {{--                                                                @csrf--}}
                                                            {{--                                                                <input type="number" class="form-control" placeholder="Enter current miles of car" name="miles" required>--}}
                                                            {{--                                                                <input type="hidden"  name="campaign_id" id="campaign-id">--}}
                                                            {{--                                                                <div class="d-md-none mb-2"></div>--}}
                                                            {{--                                                            </div>--}}

                                                            <div class="col-10 offset-1 pt-4">
                                                                <div class="form-group">
                                                                    {{--                                                                    <label>Large Size</label>--}}
                                                                    <div class="checkbox-inline">
                                                                        <label class="checkbox checkbox-lg">
                                                                            <input id="termss" type="checkbox" value="1"
                                                                                   name="termss"><span></span>
                                                                            I verify that above upoladed record are true and complete the required miles of campaign</label>
                                                                    </div>
                                                                    <span class="terms_error text-danger pl-2"></span>
                                                                    {{--                                                                    <span class="form-text text-muted">Some help text goes here</span>--}}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                        <!--end::Invoice-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>
                        <input  type="submit" class="btn btn-light font-weight-bold btn-terms"  style="cursor:default;" disabled="disabled"  value="Submit">
                        {{--                        <input  type="submit" class="btn btn-light font-weight-bold"  value="Submit">--}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade show" id="stripeMpodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title headr"></h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2 pt-5" >
                                        <br><br>
                                        <div class="col-lg-10 p-5 offset-1"  >
                                            <form action="{{route('user.stripeCharge')}}" method="post" id="payment-form">
                                                <div class="form-row delivery_date1 d-none">
                                                    <div class="col-12 pb-1" >
                                                        <label for="card-element">Sticker Delivery date</label>
                                                        <input type="text" name="company_name" required class="form-control delivery_date" readonly>
                                                    </div>
                                                </div>
                                                <div class="form-row ">
                                                    <div class="col-12 pb-1" >
                                                        <label for="card-element  ">Card company name</label>
                                                        <input type="text" name="company_name"  required class="form-control" placeholder="Company Name">
                                                    </div>
                                                </div>
                                                <div class="form-row pb-1 pt-2 ">
                                                    <label for="card-element  ">Credit or debit card</label>
                                                    <div id="card-element"></div>
                                                    <div id="card-errors" role="alert" class="text-danger"></div>
                                                    {{ csrf_field() }}
                                                </div>
                                                <input type="hidden" name="campaign_id" id="campeign_id">
                                                <button class="btn btn btn-success float-right"  >Pay now <small class="cmp-amount" ></small></button>
                                            </form>
                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    {{--                    <button type="button" class="btn btn-secondary" data-dismiss="modal"  data-toggle="modal" > Back</button>--}}
                </div>

            </div>
        </div>
    </div>

    @if($detail->payment)
        <div class="modal fade show" id="viewPaymentDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-modal="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Payment details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body">

                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center pl-4 p-1 px-md-0">
                                    <div class="col-md-12">
                                        <div class="form-group row pl-2 pr-2 pt-5" >
                                            <br><br>
                                            <div class="col-lg-9 p-5 offset-3" >
                                                <div class="form-row ">
                                                    <div class="col-4 pb-1 " >
                                                        <h6 class=" ">Card Company Name</h6>
                                                    </div>
                                                    <div class="col-4 pb-1 text-left" >
                                                        <label for="card-element  ">{{@$detail->payment->company_name}}</label>
                                                    </div>
                                                </div>
                                                <div class="form-row ">
                                                    <div class="col-4 pb-1 " >
                                                        <h6>Payment Method</h6>
                                                    </div>
                                                    <div class="col-4 pb-1 text-left" >
                                                        <label for="card-element">{{($detail->payment->type=='Paid')?'Stripe':''}}</label>
                                                    </div>
                                                </div>
                                                <div class="form-row ">
                                                    <div class="col-4 pb-1 " >
                                                        <h6 class=" ">Amount</h6>
                                                    </div>
                                                    <div class="col-4 pb-1 text-left" >
                                                        <label for="card-element  ">{{@number_format($detail->total_price,2)}}</label>
                                                    </div>
                                                </div>
                                                <div class="form-row ">
                                                    <div class="col-4 pb-1 " >
                                                        <h6 >Status</h6>
                                                    </div>
                                                    <div class="col-4 pb-1 text-left" >
                                                        <label for="card-element  ">{{@$detail->payment->status}}</label>
                                                    </div>
                                                </div>

                                                <div class="form-row ">
                                                    <div class="col-4 pb-1 " >
                                                        <h6 class=" ">Created at</h6>
                                                    </div>
                                                    <div class="col-4 pb-1 text-left" >
                                                        <label for="card-element  ">{{@$detail->payment->created_at}}</label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <!--end::Invoice-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal footer -->
                    <div class="modal-footer">
                        {{--                    <button type="button" class="btn btn-secondary" data-dismiss="modal"  data-toggle="modal" > Back</button>--}}
                    </div>

                </div>
            </div>
        </div>
    @endif

@endsection

{{-- Styles Section --}}
@section('styles')
    {{--    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>--}}
@endsection


{{-- Scripts Section --}}
@section('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    {{-- vendors --}}
    <script>
        function getdata(campaign){
// alert(campaign.total_price);
            $('#campen-id').text(campaign.id)
            $('.cmp-amount').text('$'+campaign.total_price)
            $('#campeign_id').val(campaign.id);

            $('#stripeMpodal').modal('show');
        }
        function getdata1(sticker,camp_id){
// alert(camp_id);
            $('#campen-id').text(camp_id)
            $('.cmp-amount').text('$'+sticker.bespoke_price)
            $('#campeign_id').val(camp_id);
            $('#payment-form').attr('action','{{route('user.stripeChargeStricker')}}');
            $('.delivery_date1').removeClass('d-none');
            $('.delivery_date').val(sticker.bespoke_days);
            $('.headr').text('Sticker Payment');
            $('#stripeMpodal').modal('show');
        }





        function finshCampaign(id){
            $('#campaign-id').val(id);
        }

        function del() {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }

        function claim(camp_id) {
            Swal.fire({
                title: 'Are you sure?',
                text: "You want to apply for this campaign!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {

                if (result.isConfirmed) {
                    $('#loader1').removeClass('d-none');
                    var data = {
                        'car_id': camp_id,
                        '_token': $('input[name="_token"]').val(),
                    };
                    var object = {
                        'url': '{{route('user.claim')}}',
                        'type': 'POST',
                        'data': data,

                    };
                    var result = centralAjax(object);
                    if(result=='1'){
                        $('#loader1').addClass('d-none');
                        Swal.fire(
                            'Claim Request Sent!',
                            'Your request hs been received. Please wait for admin approval.',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                toastr.success('Applied Successfully');
                                location.reload();
                            }
                        });

                    }else{
                        toastr.error('Please contact to support', 'Oops Error occured');
                    }
                }
            });
        }
        function availabilityupdate(camp_id){
            Swal.fire({
                title: 'Are you sure?',
                text: "You are available for this campaign from: {{ Carbon\Carbon::parse($interval[0])->format('d M Y') }}  to {{ Carbon\Carbon::parse($interval[1])->format('d M Y') }}. Total campaign days are: {{$detail->campaign->campaign_total_days}}",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                cancelButtonText: 'Not Available',
                confirmButtonText: 'Yes Available',
                closeOnCancel: false,
                allowOutsideClick: false
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#loader').removeClass('d-none');
                    var data = {
                        'campaign_id': camp_id,
                        'availability':1
                    };
                    var object = {
                        'url': '{{route('user.availabilityStatusUpdate')}}',
                        'type': 'GET',
                        'data': data,
                    };
                    var result = centralAjax(object);
                    if(result=='1'){
                        $('#loader').addClass('d-none');
                        Swal.fire(
                            'Availability confirmed!',
                            'We will contact you soon.',
                            'success'
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });

                    }else{
                        toastr.error('Please contact to support', 'Oops Error occured');
                    }
                }else{
                    $('#loader').removeClass('d-none');
                    var data = {
                        'campaign_id': camp_id,
                        'availability': 2
                    };
                    var object = {
                        'url': '{{route('user.availabilityStatusUpdate')}}',
                        'type': 'GET',
                        'data': data,
                    };
                    var result = centralAjax(object);
                    if(result=='1'){
                        $('#loader').addClass('d-none');
                        Swal.fire(
                            'Availability confirmed!',
                        ).then((result) => {
                            if (result.isConfirmed) {
                                location.reload();
                            }
                        });

                    }
                }
            });
        }





        @if (\Session::has('created'))
        toastr.success('{!! \Session::get('created') !!}', 'Campaign created');
        @endif
        $(document).on('click', '#toggleSearchForm', function(){
            $('#table-search-form').slideToggle();
        });

        var i;
        var markers = [ ];
            {{--var locations= <?php echo $detail->locations ?>;--}}
            {{--            @php $carzips = explode(",", $detail->zip); ;@endphp--}}
        var locations= ['lahore'];
        var zip =[];
        for(i = 0; i < locations.length; i++) {

            console.log(locations[i]);
            zip.push(locations[i]);

        }
        // var zips = ['14544','14540','14545','14542'];
        var html='';
        var j=0;
        for (i = 0; i < zip.length; i++) {
            $.ajax({
                'async': false,
                'type': "GET",
                'global': false,
                'dataType': 'json',
                'url': "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&address="+zip[i]+"&key=AIzaSyAjEWlKLj1LeT9v8eO2EsZDd8LtSK3szxk",
                'success': function (data) {
                    console.log('api',data);
                    var title= data.results[0].formatted_address;
                    var lat= data.results[0]['geometry']['location']['lat']
                    var lng= data.results[0]['geometry']['location']['lng']
// console.log('latitude',data.results[0]['geometry']['location']['lat']);
                    html+="<span>"+ ++j+": "+title+'</span><br>';
                    markers.push({
                        "title": title,
                        "lat": lat,
                        "lng": lng,
                        "description":  title
                    })
                }
            });
        }
        $('#places').html('');
        $('#places').html(html);
        // console.log('titles',html);
        window.onload = function () {
            LoadMap();
        }

        function LoadMap() {
            var mapOptions = {
                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                zoom: 3,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                zoomControl: true,
                mapTypeControl: false,
                scrollwheel: false,
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
//Create and open InfoWindow.
            var infoWindow = new google.maps.InfoWindow();
            for (var i = 0; i < markers.length; i++) {
                var data = markers[i];
                var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,
                    title: data.title

                });
//Attach click event to the marker.
                (function (marker, data) {
                    google.maps.event.addListener(marker, "click", function (e) {
                        infoWindow.setContent("<div style = 'width:200px;min-height:100px'>" + data.description + "</div>");
                        infoWindow.open(map, marker);
                    });
                })(marker, data);
            }
        }
        function status(status_id,camp_id) {
            $("#camp_id").html('CMP-'+camp_id);
            var val =status_id;
            $('#stat1 option[value='+val+']').remove();
            $("#camp-id").val(camp_id)
        }
        function carDel(car_id, camp_id) {
            swal({
                title: "Are you sure?",
                text: "You want to delete this car?",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        var data = {
                            'id':car_id,
                            'camp_id':camp_id,
                        }
                        $.ajax({
                            url: '{{route('user.campaign.car.update')}}',
                            type: "GET",
                            data: data,
                            success: function (result) {
                                if (result) {
                                    toastr.success('Car deleted successfully');
                                    location.reload();
                                } else{
                                    Swal.fire({title:'Car not deleted', icon: 'warning'});
                                }
                            }
                        })
                    }
                });
        }
        // $('#loader').removeClass('d-none');

        function formCheck() {
            var val= $("#stat1").val();

            if(val==null){
                $(".status_error").text('Please choose option to proceed')
                return ;
            }
            $('#loader').removeClass('d-none');
            document.getElementById('form-id2').submit();
        }



        $('#sticker').bind('change', function () {
            if ((this.files[0].size / 1024 / 1024) > 10) {
                $('#sticker').val('');
                $('.sticker_error').text('File size is too large.');
            } else {
                $('.sticker_error').text('');
            }
        });


        function formCheck1() {

            var range= $("#kt_daterangepicker_1").val();

            var title= $("#camp_titlee").val();
            var camp_typee= $("#camp_typee").val();

            if(range==''){
                $("#interval_error").text('Please fill this field')

                return ;
            }else{
                $("#interval_error").text('')
            }
            if(title==''){
                $(".camp_titlee_error").text('Please fill this field')
                return ;
            }else{
                $(".camp_titlee_error").text('')
            }
            if(title.length>50){
                $(".camp_titlee_error").text('Title too long')
                return ;
            }else{
                $(".camp_titlee_error").text('')
            }
            if(camp_typee==''){
                $("#camp_typee_error").text('Please fill this field')
                return ;
            }else{
                $("#camp_typee_error").text('')
            }

            $('#frm-btn').attr('disabled', 'disabled');
            document.getElementById('form-id4').submit();
        }

        function formCheck2() {

            var sticker= $("#sticker").val();

            var slogan= $("#slogan").val();
            var company= $("#company").val();
            var contact= $("#contact").val();
            var sticker_details= $("#sticker_details").val();

            if(sticker==''){
                $(".sticker_error").text('Please fill this field')

                return ;
            }else{
                $(".sticker_error").text('')
            }
            if(slogan==''){
                $(".slogan_error").text('Please fill this field')
                return ;
            }else{
                $(".slogan_error").text('')
            }
            if(slogan.length>40){
                $(".slogan_error").text('Slogan too long')
                return ;
            }else{
                $(".slogan_error").text('')
            }
            if(company.length>40){
                $(".company_error").text('Company name too long')
                return ;
            }else{
                $(".company_error").text('')
            }
            if(company==''){
                $(".company_error").text('Please fill this field')
                return ;
            }else{
                $(".company_error").text('')
            }
            if(contact==''){
                $(".contact_error").text('Please fill this field')
                return ;
            }else{
                $(".contact_error").text('')
            }
            if(sticker_details==''){
                $(".sticker_details_error").text('Please fill this field')
                return ;
            }else{
                $(".sticker_details_error").text('')
            }
            $('#frm-btn1').attr('disabled', 'disabled');
            document.getElementById('form-id5').submit();
        }


        @if (\Session::has('updated'))
        toastr.success('{!! \Session::get('updated') !!}', 'Updated');
        @endif

        $(document).on('change', '#termss', function (e) {
            if ($('#termss').is(":checked")) {
                $('.terms_error').text('');
                $('.btn-terms').removeClass('btn-light');
                $('.btn-terms').removeAttr('disabled');
                $('.btn-terms').addClass('btn-primary');
                $('.btn-terms').css('cursor', 'pointer');
            } else {
                $('.terms_error').text('Please agree our terms and conditions');
                $('.btn-terms').removeClass('btn-primary');
                $('.btn-terms').attr('disabled', 'disabled');
                $('.btn-terms').addClass('btn-light');
                $('.btn-terms').css('cursor', ' auto');
            }
        });

        var today = new Date();
        var tomorrow = new Date();
        tomorrow.setDate(today.getDate() + 1)
        $('#kt_daterangepicker_1').daterangepicker({
            buttonClasses: ' btn',
            applyClass: 'btn-primary',
            cancelClass: 'btn-secondary',
            minDate: moment(tomorrow),
            maxYear: parseInt(moment().add(1, 'years').format('YYYY'), 10),

        });

        $('#kt_daterangepicker_1').val('')
        $('.page-loader,.page-loader-logo').remove();
    </script>

    <script src="https://js.stripe.com/v3/"></script>
    <script>
        //start stripe
        // Create a Stripe client.
            {{--var stripe = Stripe('{{env('STRIPE_PUBLIC_KEY')}}');--}}
        var stripe = Stripe('pk_test_51HlYDGK7l7e9CDWNxOVqBen7Z7PYhRL6K0sUVS0OGEDFiH7l6Qg204EfEh2ju40UFsY13pbCLZzwPvugzt6D4eCy006htkwlIZ');
        // Create an instance of Elements.
        var elements = stripe.elements();

        // Custom styling can be passed to options when creating an Element.
        // (Note that this demo uses a wider set of styles than the guide below.)
        var style = {
            base: {
                color: '#32325d',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        // Create an instance of the card Element.
        var card = elements.create('card', {style: style});

        // Add an instance of the card Element into the `card-element` <div>.
        card.mount('#card-element');

        // Handle real-time validation errors from the card Element.
        card.addEventListener('change', function(event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });
        // Handle form submission.
        var form = document.getElementById('payment-form');
        form.addEventListener('submit', function(event) {
            event.preventDefault();

            stripe.createToken(card).then(function(result) {
                if (result.error) {
// Inform the user if there was an error.
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
// Send the token to your server.
                    stripeTokenHandler(result.token);
                }
            });
        });
        // Submit the form with the token ID.
        function stripeTokenHandler(token) {
// Insert the token ID into the form so it gets submitted to the server
            var form = document.getElementById('payment-form');
            var hiddenInput = document.createElement('input');
            hiddenInput.setAttribute('type', 'hidden');
            hiddenInput.setAttribute('name', 'stripeToken');
            hiddenInput.setAttribute('value', token.id);
            form.appendChild(hiddenInput);
// Submit the form
            form.submit();
        }
        //end stripe




        @if (\Session::has('error'))
        toastr.error('{!! \Session::get('error') !!}', 'Action not successfull');
        @endif
        @if (\Session::has('success'))
        toastr.success('{!! \Session::get('success') !!}', 'Action successfull');
        @endif
    </script>


    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjEWlKLj1LeT9v8eO2EsZDd8LtSK3szxk&callback=myMap"></script>
@endsection

