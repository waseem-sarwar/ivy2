{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 25px;
            background-color: #ffffff;
        }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Manage Campaign
                    <div class="text-muted pt-2 font-size-sm">Managment of Campaign</div>
                </h3>

            </div>
            <div class="card-toolbar">
                <select  class="data-table-10list float-right">
                    <option  value="">10</option >
                    <option  value="">20</option >
                </select>
                <a class="btn btn-light-primary mr-2" href="javascript:;" id="toggleSearchForm">
                     <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                                                 <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                                                      version="1.1">
                                                     <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                         <rect x="0" y="0" width="24" height="24"/>
                                                         <path
                                                             d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
                                                             fill="#000000" opacity="0.3"/>
                                                         <path
                                                             d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
                                                             fill="#000000"/>
                                                     </g>
                                                 </svg>
                                                 <!--end::Svg Icon-->
                                             </span>
                    Search
                </a>
                <!--begin::Dropdown-->
{{--                <div class="dropdown dropdown-inline mr-2">--}}
{{--                    <button type="button" class="btn btn-light-primary font-weight-bolder dropdown-toggle"--}}
{{--                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                    <span class="svg-icon svg-icon-md">--}}
{{--                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->--}}
{{--                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"--}}
{{--                             version="1.1">--}}
{{--                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                <rect x="0" y="0" width="24" height="24"/>--}}
{{--                                <path--}}
{{--                                    d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"--}}
{{--                                    fill="#000000" opacity="0.3"/>--}}
{{--                                <path--}}
{{--                                    d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"--}}
{{--                                    fill="#000000"/>--}}
{{--                            </g>--}}
{{--                        </svg>--}}
{{--                        <!--end::Svg Icon-->--}}
{{--                    </span>Export--}}
{{--                    </button>--}}
{{--                    <!--begin::Dropdown Menu-->--}}
{{--                    <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">--}}
{{--                        <!--begin::Navigation-->--}}
{{--                        <ul class="navi flex-column navi-hover py-2">--}}
{{--                            <li class="navi-header font-weight-bolder text-uppercase font-size-sm text-primary pb-2">--}}
{{--                                Choose an option:--}}
{{--                            </li>--}}
{{--                            <li class="navi-item">--}}
{{--                                <a href="#" class="navi-link">--}}
{{--                                    <span class="navi-icon">--}}
{{--                                        <i class="la la-print"></i>--}}
{{--                                    </span>--}}
{{--                                    <span class="navi-text">Print</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="navi-item">--}}
{{--                                <a href="#" class="navi-link">--}}
{{--                                    <span class="navi-icon">--}}
{{--                                        <i class="la la-copy"></i>--}}
{{--                                    </span>--}}
{{--                                    <span class="navi-text">Copy</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="navi-item">--}}
{{--                                <a href="#" class="navi-link">--}}
{{--                                    <span class="navi-icon">--}}
{{--                                        <i class="la la-file-excel-o"></i>--}}
{{--                                    </span>--}}
{{--                                    <span class="navi-text">Excel</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="navi-item">--}}
{{--                                <a href="#" class="navi-link">--}}
{{--                                    <span class="navi-icon">--}}
{{--                                        <i class="la la-file-text-o"></i>--}}
{{--                                    </span>--}}
{{--                                    <span class="navi-text">CSV</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li class="navi-item">--}}
{{--                                <a href="#" class="navi-link">--}}
{{--                                    <span class="navi-icon">--}}
{{--                                        <i class="la la-file-pdf-o"></i>--}}
{{--                                    </span>--}}
{{--                                    <span class="navi-text">PDF</span>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <!--end::Navigation-->--}}
{{--                    </div>--}}
{{--                    <!--end::Dropdown Menu-->--}}
{{--                </div>--}}
                <!--end::Dropdown-->
                <!--begin::Button-->

                <a href="{{url('/Advertiser/Dashboard/create-campaign')}}" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                         version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path
                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>New Campaign</a>

                <!--end::Button-->
            </div>
        </div>

        <div class="card-body">

            <!--begin::Search Form-->
            <div class="mt-2 mb-5 mt-lg-5 mb-lg-10" id="table-search-form" style="display:none">
                <div class="row align-items-center">
                    <div class="col-lg-9 col-xl-8">
                        <div class="row align-items-center">
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="input-icon">
                                    <input type="text" class="form-control" placeholder="Search..."
                                           id="kt_datatable_search_query"/>
                                    <span><i class="flaticon2-search-1 text-muted"></i></span>
                                </div>
                            </div>

                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Campaign Status:</label>
                                    <select class="form-control" id="kt_datatable_search_status">
                                        <option value="">All</option>
                                        <option value="1">Pending</option>
                                        <option value="2">In Review</option>
                                        <option value="3">Cancelled</option>
                                        <option value="4">Completed</option>
{{--                                                                                <option value="5">Info</option>--}}
{{--                                                                                <option value="6">Danger</option>--}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <div class="d-flex align-items-center">
                                    <label class="mr-3 mb-0 d-none d-md-block">Payment Status:</label>
                                    <select class="form-control" id="kt_datatable_search_type">
                                        <option value="">All</option>
                                        <option value="1">Paid</option>
                                        <option value="2">Refunded</option>
                                        <option value="2">On Hold</option>
                                        <option value="2">pending</option>
                                        {{--                                        <option value="3">Direct</option>--}}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                        <a href="#" class="btn btn-light-primary px-6 font-weight-bold">
                            Search
                        </a>
                    </div>
                </div>
            </div>

            <!-- HTML Table Begin Here -->
            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="">
                    <table class="table main-table table-head-custom table-head-bg pb-3 table-responsive table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">Campaign Id</th>
                            <th style="min-width: 250px" class="pl-7">
                                <span class="">Campaign</span>
                            </th>
                            <th style="min-width: 100px">Location</th>
                            <th style="min-width: 100px">Payment</th>
                            <th style="min-width: 130px">Campaign Status</th>
                            <th style="min-width: 130px">Payment Status</th>
                            <th style="min-width: 100px">Start Date</th>
                            <th style="min-width: 130px">End Date</th>

                            <th class="fix-table-head" style="min-width: 135px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{url('/Advertiser/Dashboard/view-campaign/11')}}">CMP-0018</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">Taxas USA </span>
                                <span class="text-muted font-weight-bold"></span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-warning label-inline">In Progress</span>
                            </td>

                            <td>
                                <span class="label label-lg label-light-success label-inline">Success</span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"></span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>


                            <td class="pr-0 fix-table-column">

                                {{--                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm mx-3">--}}
                                {{--                                        <span class="svg-icon svg-icon-md svg-icon-primary">--}}
                                {{--                                            <!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Write.svg-->--}}
                                {{--                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
                                {{--                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
                                {{--                                                    <rect x="0" y="0" width="24" height="24" />--}}
                                {{--                                                    <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953)" />--}}
                                {{--                                                    <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
                                {{--                                                </g>--}}
                                {{--                                            </svg>--}}
                                {{--                                            <!--end::Svg Icon-->--}}
                                {{--                                        </span>--}}
                                {{--                                    </a>--}}
                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>


                                {{--                                    <a href="#" class="btn btn-icon btn-light btn-hover-primary btn-sm">--}}
                                {{--                                        <span class="svg-icon svg-icon-md svg-icon-primary">--}}
                                {{--                                            <!--begin::Svg Icon | path:assets/media/svg/icons/General/Trash.svg-->--}}
                                {{--                                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
                                {{--                                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
                                {{--                                                    <rect x="0" y="0" width="24" height="24" />--}}
                                {{--                                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero" />--}}
                                {{--                                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3" />--}}
                                {{--                                                </g>--}}
                                {{--                                            </svg>--}}
                                {{--                                            <!--end::Svg Icon-->--}}
                                {{--                                        </span>--}}
                                {{--                                    </a>--}}
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{url('/Advertiser/Dashboard/view-campaign/11')}}">CMP-0017</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">Calfornia USA </span>
                                <span class="text-muted font-weight-bold">In Proccess</span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-success label-inline">Completed</span>
                            </td>

                            <td>
                                <button class="btn btn-primary btn-sm"><i class="far fa-credit-card"></i>

                                    pay  </button>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold">  </span>
                            </td>


                            <td class="pr-0 fix-table-column">

                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>


                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href={{url('/Advertiser/Dashboard/view-campaign/11')}}">CMP-0018</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">England London </span>
                                <span class="text-muted font-weight-bold">In Proccess</span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-danger label-inline">Closed</span>
                            </td>

                            <td>
                                <span class="label label-lg label-light-warning label-inline">On hold</span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold">  </span>
                            </td>


                            <td class="pr-0 fix-table-column">


                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>

                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="/Advertiser/Dashboard/view-campaign/11">CMP-0018</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">England London </span>
                                <span class="text-muted font-weight-bold">In Proccess</span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-warning label-inline">In Progress</span>
                            </td>

                            <td>
                                <span class="label label-lg label-light-success label-inline">Success</span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold">  </span>
                            </td>


                            <td class="pr-0 fix-table-column">

                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{url('/Advertiser/Dashboard/view-campaign/11')}}">CMP-0010</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">England London </span>
                                <span class="text-muted font-weight-bold">In Proccess</span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-warning label-inline">In Progress</span>
                            </td>

                            <td>
                                <span class="label label-lg label-light-success label-inline">Success</span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold">  </span>
                            </td>


                            <td class="pr-0 fix-table-column">
                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{url('/Advertiser/Dashboard/view-campaign/11')}}">CMP-0038</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">England London </span>
                                <span class="text-muted font-weight-bold"></span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-warning label-inline">In Progress</span>
                            </td>

                            <td>
                                <span class="label label-lg label-light-success label-inline">Success</span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold">  </span>
                            </td>


                            <td class="pr-0 fix-table-column">
                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{url('/Advertiser/Dashboard/view-campaign/11')}}">CMP-0014</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">England London </span>
                                <span class="text-muted font-weight-bold">In Proccess</span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-success label-inline">In Review</span>
                            </td>

                            <td>
                                <span class="label label-lg label-light-warning label-inline">Pending</span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold">  </span>
                            </td>


                            <td class="pr-0 fix-table-column">

                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>
                            </td>

                        </tr>
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"><a href="{{url('/Advertiser/Dashboard/view-campaign/11')}}">CMP-0098</a></span>
                                {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div class="symbol symbol-50 symbol-light mr-4">
                                            <span class="symbol-label">
                                                <img src="{{ asset('media/svg/avatars/001-boy.svg') }}" class="h-75 align-self-end" alt="" />
                                            </span>
                                    </div>
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Sample Title</a>
                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">England London </span>
                                <span class="text-muted font-weight-bold">In Proccess</span>
                            </td>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">$520</span>
                                <span class="text-muted font-weight-bold">Paid</span>
                            </td>
                            <td >
                                <span class="label label-lg label-light-danger label-inline">Closed</span>
                            </td>

                            <td>
                                <span class="label label-lg label-light-warning label-inline">On hold</span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold"> </span>
                            </td>

                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">20-01-2020</span>
                                <span class="text-muted font-weight-bold">  </span>
                            </td>


                            <td class="pr-0 fix-table-column">
                                <a href="javascript:;"  data-toggle="modal" data-target="#viewModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="{{url('/Advertiser/Dashboard/edit-campaign')}}"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="edit details">
                                    <i class="la la-edit"></i>
                                </a>

                                <a href="javascript:;"   data-toggle="modal" data-target="#statusUpdate" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Change status">
                                    <i class="la la-wrench"></i>
                                </a>
                                <a href="javascript:;"   data-toggle="modal" data-target="#feedback" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Give feedback">
                                    <i class="la la-comments-o"></i>
                                </a>
                                <a href="javascript:;"  data-toggle="modal" data-target="#payment" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Pay now">
                                    <i class="la la-credit-card"></i>
                                </a>
                                <a href="javascript:;"  onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2 mt-2" title="Delete">
                                    <i class="la la-trash"></i>
                                </a>
                            </td>

                        </tr>

                        </tbody>
                    </table>
                    <nav aria-label="Page navigation example ">
                        <ul class="pagination float-right mt-4">
                            <li class="page-item "><a class="page-link" href="#">Previous</a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item "><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div>
                <!--end::Table-->
            </div>
            <!-- HTML Table End Here -->
        </div>
    </div>

    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Title</h5>
                    <button class="btn btn-light-primary font-weight-bold text-right" style="
    margin-left: 267px;
"><i class="fa fa-download" aria-hidden="true"></i>download</button>
                    <button class="btn btn-light-primary font-weight-bold" style="
    margin-left: -68px;
"><i class="fa fa-print" aria-hidden="true"></i>print</button>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">
                    <div class="container">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">
                                    <div class="col-md-9">
                                        <!-- begin: Invoice header-->
                                        <div
                                            class="d-flex justify-content-between pb-5 pb-md-10 flex-column flex-md-row">
                                            <h1 class="display-4 font-weight-boldest mb-10">Sample Campaign Title</h1>
{{--                                            <span>Duration : 20m </span>--}}

                                            <div class="d-flex flex-column align-items-md-end px-0">
                                                <!--begin::Logo-->
                                                <a href="#" class="mb-5 max-w-200px">
															<span class="svg-icon svg-icon-full">
																<!--begin::Svg Icon | path:assets/media/svg/logos/duolingo.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" width="113"
                                                                     height="31" viewBox="0 0 113 31" fill="none">
																	<path
                                                                        d="M26.6802 13.33V13.485C26.6802 14.9575 26.6087 15.8875 26.4659 16.275C26.2517 16.895 25.966 17.36 25.4661 17.67C25.0376 17.98 24.5377 18.1358 23.8949 18.1358C23.2529 18.1358 22.7523 17.9808 22.3245 17.67C21.8953 17.3608 21.6103 16.895 21.3954 16.275C21.1811 15.7325 21.1104 14.8025 21.1104 13.4075V5.425H17.3254V13.485C17.3968 15.345 17.5396 16.6625 17.7539 17.515C18.1824 18.9108 18.8965 19.995 19.8963 20.8483C20.8962 21.6233 22.2531 22.0108 23.8242 22.0108C25.324 22.0108 26.538 21.7008 27.5379 21.0025C28.537 20.3058 29.2511 19.375 29.7518 18.0575C30.1088 17.205 30.3231 15.655 30.3231 13.485V5.425H26.6087V13.33H26.6802ZM15.6107 0H11.897V6.2775C11.8256 6.2775 11.7542 6.2 11.6828 6.2C10.4687 5.425 9.11182 5.0375 7.75492 5.0375C5.82669 5.0375 4.1127 5.735 2.61297 7.2075C0.898984 8.9125 0.0419922 11.005 0.0419922 13.485C0.0419922 15.81 0.827568 17.825 2.3273 19.4525C3.82704 21.0808 5.68314 21.9325 7.75492 21.9325C9.18324 21.9325 10.5401 21.545 11.7542 20.8483C12.9683 20.0733 13.8967 19.065 14.6109 17.7483C15.2536 16.5858 15.54 15.345 15.6107 14.0283V13.4858V0ZM10.6823 16.895C9.89668 17.7483 8.96899 18.2125 7.7542 18.2125C6.61155 18.2125 5.61172 17.7475 4.89756 16.895C4.11199 16.0425 3.75491 14.88 3.75491 13.485C3.75491 12.1675 4.11199 11.005 4.89756 10.1525C5.68314 9.3 6.61155 8.835 7.7542 8.835C8.96828 8.835 9.89668 9.3 10.6823 10.1525C11.4685 11.005 11.8256 12.1675 11.8256 13.485C11.8256 14.88 11.4685 16.0425 10.6823 16.895ZM46.2481 9.3C45.534 7.9825 44.6049 6.975 43.3915 6.2C42.1774 5.425 40.8205 5.0375 39.4636 5.0375C37.5354 5.0375 35.8207 5.735 34.3217 7.2075C32.6077 8.9125 31.75 10.9275 31.75 13.485C31.75 15.81 32.5363 17.825 34.0353 19.4525C35.5357 21.0808 37.3918 21.9325 39.4636 21.9325C40.8919 21.9325 42.2481 21.545 43.4629 20.8483C44.677 20.0733 45.6054 19.065 46.3195 17.7483C47.0337 16.4308 47.3908 15.0358 47.3908 13.4858C47.248 12.0125 46.9623 10.6175 46.2481 9.3ZM42.3917 16.895C41.6061 17.7483 40.6777 18.2125 39.4636 18.2125C38.2495 18.2125 37.3211 17.7475 36.607 16.895C35.8214 16.0425 35.4643 14.88 35.4643 13.485C35.4643 12.1675 35.8214 11.005 36.6077 10.1525C37.3933 9.3 38.321 8.835 39.4643 8.835C40.6777 8.835 41.6068 9.3 42.3924 10.1525C43.178 11.005 43.535 12.1675 43.535 13.485C43.5336 14.88 43.1058 16.0425 42.3917 16.895ZM60.2457 21.7H64.0307V6.1225H60.2457V21.7ZM53.8182 17.515C53.3897 17.2058 53.0326 16.74 52.8898 16.12C52.747 15.655 52.6756 14.725 52.6756 13.33V0H48.9619V13.2525C49.0334 15.4225 49.1762 16.895 49.5333 17.825C50.0332 19.065 50.7473 20.0725 51.7472 20.77C52.747 21.4675 53.9618 21.7775 55.4608 21.7775H58.3174V17.98H55.3894C54.7466 17.98 54.246 17.825 53.8182 17.515ZM78.7424 9.145C78.2425 7.905 77.5283 6.8975 76.5278 6.2C75.5287 5.5025 74.3139 5.1925 72.8142 5.1925C71.1716 5.1925 69.8861 5.58 68.8863 6.355C67.8872 7.13 67.173 8.215 66.7438 9.6875C66.5296 10.4625 66.3867 11.8575 66.3153 13.7175V21.7H70.1003V13.95V13.64C70.1003 12.3225 70.1725 11.315 70.386 10.7725C70.6003 10.1525 70.8859 9.765 71.3144 9.3775C71.7429 9.0675 72.2428 8.9125 72.8856 8.9125C73.5276 8.9125 74.0282 9.0675 74.4567 9.3775C74.8852 9.6875 75.2423 10.1525 75.4558 10.7725C75.5987 11.2375 75.6701 12.1675 75.6701 13.5625V21.7H79.3837V14.88V13.64C79.3137 11.5475 79.0995 9.9975 78.7424 9.145ZM62.0311 0.31C60.9598 0.31 60.0314 1.3175 60.0314 2.48C60.0314 3.6425 60.8884 4.65 62.0311 4.65C63.1023 4.65 64.0307 3.72 64.0307 2.48C64.0307 1.24 63.1023 0.31 62.0311 0.31ZM111.951 9.455C111.237 8.1375 110.308 7.13 109.094 6.355C107.88 5.58 106.595 5.27 105.094 5.27C102.952 5.27 101.166 6.1225 99.6666 7.75C98.1661 9.3775 97.3813 11.3925 97.3813 13.7175C97.3813 16.1975 98.2383 18.2892 99.9522 19.995C101.453 21.4675 103.166 22.165 105.094 22.165C106.522 22.165 107.808 21.7775 109.022 21.0025C110.236 20.2275 111.237 19.22 111.879 17.9025C112.593 16.5858 112.95 15.19 112.95 13.7183C113.022 12.1675 112.665 10.7725 111.951 9.455ZM109.236 13.7175C109.236 15.1125 108.879 16.1975 108.094 17.05C107.308 17.9025 106.38 18.3675 105.166 18.3675C104.022 18.3675 103.023 17.9017 102.309 17.05C101.523 16.1975 101.166 15.035 101.166 13.7175C101.166 12.3225 101.523 11.16 102.309 10.3075C103.095 9.455 104.022 8.99 105.166 8.99C106.308 8.99 107.308 9.455 108.094 10.3075C108.879 11.16 109.236 12.3225 109.236 13.7175C109.236 13.64 109.236 13.64 109.236 13.7175ZM96.0965 13.02C96.0244 11.7025 95.6673 10.54 95.0967 9.3775C94.3825 8.06 93.4541 7.0525 92.3122 6.2775C91.0981 5.5025 89.8126 5.1925 88.3843 5.1925C86.2418 5.1925 84.4564 5.9675 82.9559 7.6725C81.4562 9.3 80.7421 11.315 80.7421 13.64C80.7421 16.12 81.599 18.2125 83.2416 19.84C84.7413 21.3125 86.3832 22.0108 88.3121 22.0108C89.669 22.0108 91.026 21.6233 92.24 20.8483C92.3122 20.77 92.3829 20.77 92.4543 20.6933V22.5525C92.4543 23.9483 92.0258 25.0325 91.3116 25.8858C90.526 26.7383 89.5976 27.2025 88.455 27.2025C87.3123 27.2025 86.3839 26.815 85.5983 25.9625L83.1702 28.7525L83.2416 28.83C84.7413 30.3017 86.4553 31 88.3121 31C89.669 31 91.026 30.6125 92.24 29.8375C93.4541 29.0625 94.3825 28.055 95.0967 26.7375C95.8108 25.42 96.0965 24.025 96.0965 22.5525V14.105V13.5625C96.1679 13.4075 96.1679 13.175 96.0965 13.02ZM92.4536 13.5625C92.4536 14.9575 92.0965 16.0425 91.3109 16.895C90.5253 17.7483 89.5969 18.2125 88.4543 18.2125C87.3116 18.2125 86.3832 17.7475 85.5976 16.895C84.812 16.0425 84.455 14.9575 84.455 13.5625C84.455 12.1675 84.812 11.0825 85.5976 10.1525C86.3832 9.3 87.3116 8.835 88.4543 8.835C89.5969 8.835 90.5967 9.3 91.3109 10.1525C92.0258 11.0825 92.4536 12.1675 92.4536 13.5625Z"
                                                                        fill="#1CB0F6"></path>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                                </a>
                                                <!--end::Logo-->
                                                <span
                                                    class="d-flex flex-column text-center align-items-md-end font-size-h6 font-weight-bold text-muted">
{{--															<span class="text-center">Start Campaign  </span>--}}
{{--															<span>12 May 2020  </span>--}}

                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="d-flex  font-size-lg mb-3">
                                                <span class="font-weight-bold mr-12">Campaign Status:</span>
                                                <span class="text-right ">
                                                    	<span class="navi-icon mr-2">
																			<i class="fa fa-genderless text-primary font-size-h2"></i>
                                                            																		</span>
                                                    In review
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="d-flex  font-size-lg mb-3">
                                                <span class="font-weight-bold mr-15">Payment Status:   </span>
                                                <span class="text-right ">
                                                    	<span class="navi-icon mr-2">
																			<i class="fa fa-genderless text-success font-size-h2"></i>
                                                            																		</span>
                                                   Paid
                                                </span>
                                            </div>
                                        </div>


                                        <!--end: Invoice header-->
                                        <!--begin: Invoice body-->
                                        <div class="row border-bottom pb-10">
                                            <div class="col-md-7 py-md-10 pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Location
                                                            </th>

                                                            <th class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Zipcode
                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Cost
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">
                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-center">
																		<span class="navi-icon mr-2">
																			<i class="fa fa-genderless text-danger font-size-h2"></i>
																		</span>England London
                                                            </td>

                                                            <td class="text-right pt-7">564</td>
                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                $3200.00
                                                            </td>
                                                        </tr>
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                                        {{--																		</span>Front-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $4800.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                        {{--																		</span>Back-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $12600.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                <div class="d-flex flex-column flex-md-row">
                                                    <div class="d-flex flex-column mb-10 mb-md-0">
                                                        <div class=" font-size-h6 mb-3 pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            Vehicle Info
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-3">
                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>
                                                            <span class="text-right">Car</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-3">
                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>
                                                            <span class="text-right">Honda</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg">
                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>
                                                            <span class="text-right">2020</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 border-left-md pl-md-10 py-md-10">
                                                <!--begin::Total Amount-->
                                                <!--begin::Total Amount-->
                                                <div class="font-size-h6 font-weight-bolder text-muted mb-3">Grand
                                                    Amount
                                                </div>
                                                <div class="font-size-h6 font-weight-boldest">$20,60</div>
{{--                                                <div class="text-muted font-weight-bold mb-16">Taxes included</div>--}}
                                                <!--end::Total Amount-->
                                                <div class="border-bottom w-100 mb-16"></div>
                                                <!--begin::Invoice To-->
                                            {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Campaign Status--}}
                                            {{----}}
                                            {{--                                                </div>--}}
                                            {{--                                                <div class="font-size-lg font-weight-bold mb-10">Iris Watson.--}}
                                            {{--                                                    <br>In review--}}
                                            {{--                                                </div>--}}
                                            <!--end::Invoice To-->
                                                <!--begin::Invoice No-->
                                            {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Payment Status--}}
                                            {{--                                                </div>--}}
                                            {{--                                                <div class="font-size-lg font-weight-bold mb-10">Pending</div>--}}
                                            <!--end::Invoice No-->
                                                <!--begin::Invoice Date-->
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Start Date 12 May, 2020</div>
                                                <div class="font-size-lg font-weight-bold"></div>
                                                <!--end::Invoice Date--><!--begin::Invoice Date-->
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">End Date 18 May, 2020</div>
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Duration 7 Days</div>
                                                <div class="font-size-lg font-weight-bold"></div>
                                                <!--end::Invoice Date--><!--begin::Invoice Date-->
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="d-flex flex-column flex-md-row">
                                                    <div class="d-flex flex-column mb-10 mb-md-0">
{{--                                                        <div class=" font-size-h6 mb-3 pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

{{--                                                            Vehicle Info--}}
{{--                                                        </div>--}}
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span class="font-weight-bold mr-15">Number of Cars:</span>
                                                            <span class="text-right">12</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span class="font-weight-bold mr-15">How often miles drived:</span>
                                                            <span class="text-right">12</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span class="font-weight-bold mr-1">How often drives a car:</span>
                                                            <span class="text-right">Weekly</span>
                                                        </div>
{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
{{--                                                            <span class="font-weight-bold mr-15">How often miles drived:</span>--}}
{{--                                                            <span class="text-right">34</span>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
{{--                                                            <span class="text-right">BARC0032UK</span>--}}
{{--                                                        </div>--}}
                                                    </div>
                                                </div>
{{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
{{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Number of Cars : 3 </div>--}}
{{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
{{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">How often miles drived: 20 </div>--}}
{{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
{{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">How often drives a car: Weekly </div>--}}
{{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
{{--                                                <!--end::Invoice Date-->--}}
                                            </div>
                                        </div>
                                        <!--end: Invoice body-->
                                        <div class="row border-bottom pb-1">
                                            <div class="col-md-6  pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                My Sticker Info
                                                            </th>

                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">

{{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
{{--																		<span class="navi-icon mr-2">--}}
{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
{{--																		</span>in progress--}}
{{--                                                            </td>--}}

                                                            {{--                                                            <td class="text-right pt-7">564</td>--}}
                                                            {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                                            {{--                                                                $3200.00--}}
                                                            {{--                                                            </td>--}}
                                                        </tr>

                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                                        {{--																		</span>Front-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $4800.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                        {{--																		</span>Back-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $12600.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        </tbody>
                                                    </table>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Company:</span>
                                                        <span class="text-right">20thFloor Technologies</span>
                                                    </div><div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15"> Slogan</span>
                                                        <span class="text-right">Technologies</span>
                                                    </div><div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Contact</span>
                                                        <span class="text-right">+166855869</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>
                                                        <span class="text-right"></span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
{{--                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>--}}
                                                        <span class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </span>
                                                    </div> <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Logo</span>
                                                        <span class="text-right"></span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
{{--                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>--}}
                                                        <span class="text-left">
                                                            <img src="{{ url('/media/cinqueterre.jpg') }}" class="img-rounded" alt="Cinque Terre" width="150px" height="150px" style="
   border-radius: 20px;
">
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                <div class="d-flex flex-column flex-md-row">
                                                    {{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                                    {{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                                    {{--                                                            <span class="text-right">Loader</span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                                    {{--                                                            <span class="text-right">1234567890934</span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                                    {{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                    </div>--}}
                                                </div>
                                            </div>
                                            <div class="col-md-6 py-md-10 pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Preview
                                                            </th>

                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">

                                                            {{--                                                            <td class="text-right pt-7">564</td>--}}
                                                            {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                                            {{--                                                                $3200.00--}}
                                                            {{--                                                            </td>--}}
                                                        </tr>
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                                        {{--																		</span>Front-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $4800.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                        {{--																		</span>Back-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $12600.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        </tbody>
                                                    </table>
                                                    <img src="http://127.0.0.1:8000/media/cinqueterre.jpg" class="img-rounded" alt="Cinque Terre" width="233" height="236" style="
    width: 100%;
">


<br>
<br>
{{--<br>--}}
{{--<br>--}}
                                                </div>
                                                <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                <div class="d-flex flex-column flex-md-row">

                                                    {{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                                    {{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                                    {{--                                                            <span class="text-right">Loader</span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                                    {{--                                                            <span class="text-right">1234567890934</span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                                    {{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                                    {{--                                                        </div>--}}
                                                    {{--                                                    </div>--}}
                                                </div>
                                            </div>
                                <div class="row mt-n5">
                                    <div class="col-sm-12">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/U_lTuI8Ck0I" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                                    </div>
                                </div>

                                        </div>
{{--                                        <div class="row border-bottom pb-2">--}}
{{--                                            <div class="col-md-6 py-md-10 pr-md-10">--}}
{{--                                                <div class="table-responsive">--}}
{{--                                                    <table class="table">--}}
{{--                                                        <thead>--}}
{{--                                                        <tr>--}}
{{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
{{--                                                                Campaign Status--}}
{{--                                                            </th>--}}

{{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

{{--                                                            </th>--}}
{{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

{{--                                                            </th>--}}
{{--                                                        </tr>--}}
{{--                                                        </thead>--}}
{{--                                                        <tbody>--}}
{{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
{{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
{{--																		<span class="navi-icon mr-2">--}}
{{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
{{--																		</span>in review--}}
{{--                                                            </td>--}}
{{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
{{--																		<span class="navi-icon mr-2">--}}
{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
{{--																		</span>in progress--}}
{{--                                                            </td>--}}

{{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
{{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                            --}}{{--                                                                $3200.00--}}
{{--                                                            --}}{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
{{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
{{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
{{--                                                        --}}{{--																		</span>Front-End Development--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                        --}}{{--                                                                $4800.00--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                        </tr>--}}
{{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
{{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
{{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
{{--                                                        --}}{{--																		</span>Back-End Development--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                        --}}{{--                                                                $12600.00--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                        </tr>--}}
{{--                                                        </tbody>--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
{{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
{{--                                                <div class="d-flex flex-column flex-md-row">--}}
{{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
{{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                            <div class="col-md-6 py-md-10 pr-md-10">--}}
{{--                                                <div class="table-responsive">--}}
{{--                                                    <table class="table">--}}
{{--                                                        <thead>--}}
{{--                                                        <tr>--}}
{{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
{{--                                                                Payment Status--}}
{{--                                                            </th>--}}

{{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

{{--                                                            </th>--}}
{{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

{{--                                                            </th>--}}
{{--                                                        </tr>--}}
{{--                                                        </thead>--}}
{{--                                                        <tbody>--}}
{{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
{{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
{{--																		<span class="navi-icon mr-2">--}}
{{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
{{--																		</span>in review--}}
{{--                                                            </td>--}}
{{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
{{--																		<span class="navi-icon mr-2">--}}
{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
{{--																		</span>in progress--}}
{{--                                                            </td>--}}

{{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
{{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                            --}}{{--                                                                $3200.00--}}
{{--                                                            --}}{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
{{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
{{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
{{--                                                        --}}{{--																		</span>Front-End Development--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                        --}}{{--                                                                $4800.00--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                        </tr>--}}
{{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
{{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
{{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
{{--                                                        --}}{{--																		</span>Back-End Development--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                        --}}{{--                                                                $12600.00--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                        </tr>--}}
{{--                                                        </tbody>--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
{{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
{{--                                                <div class="d-flex flex-column flex-md-row">--}}
{{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
{{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}
{{--                                        <div class="row border-bottom pb-2">--}}
{{--                                            <div class="col-md-12 py-md-10 pr-md-10">--}}
{{--                                                <div class="table-responsive">--}}
{{--                                                    <table class="table">--}}
{{--                                                        <thead>--}}
{{--                                                        <tr>--}}
{{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
{{--                                                                Payment Status--}}
{{--                                                            </th>--}}

{{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

{{--                                                            </th>--}}
{{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

{{--                                                            </th>--}}
{{--                                                        </tr>--}}
{{--                                                        </thead>--}}
{{--                                                        <tbody>--}}
{{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
{{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
{{--																		<span class="navi-icon mr-2">--}}
{{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
{{--																		</span>1 -pending--}}
{{--                                                            </td>--}}
{{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
{{--																		<span class="navi-icon mr-2">--}}
{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
{{--																		</span>2  - paid--}}
{{--                                                            </td>--}}

{{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
{{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                            --}}{{--                                                                $3200.00--}}
{{--                                                            --}}{{--                                                            </td>--}}
{{--                                                        </tr>--}}
{{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
{{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
{{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
{{--                                                        --}}{{--																		</span>Front-End Development--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                        --}}{{--                                                                $4800.00--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                        </tr>--}}
{{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
{{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
{{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
{{--                                                        --}}{{--																		</span>Back-End Development--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
{{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
{{--                                                        --}}{{--                                                                $12600.00--}}
{{--                                                        --}}{{--                                                            </td>--}}
{{--                                                        --}}{{--                                                        </tr>--}}
{{--                                                        </tbody>--}}
{{--                                                    </table>--}}
{{--                                                </div>--}}
{{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
{{--                                                <div class="d-flex flex-column flex-md-row">--}}
{{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
{{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
{{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
{{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
{{--                                                    --}}{{--                                                        </div>--}}
{{--                                                    --}}{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

{{--                                        </div>--}}
                                    </div>

                                </div>

                                <!--end::Invoice-->

                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">I am Compaign Title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">


                                <div class="col-md-12">
                                    <div id="kt_repeater_1">
                                        <div class="form-group row" id="kt_repeater_1">
                                            <div data-repeater-list="">
                                                <div data-repeater-item="" class="form-group row align-items-center">
                                                    <div class="col-md-6">
                                                        <label>State:</label>
                                                        <input type="text" class="form-control" name="[0][state]"
                                                               placeholder="Select state" onkeyup="showdetail();">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>City:</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Select city" name="[0][city]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Zip:</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Enter zip code" name="[0][zip]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6 mt-4">
                                                        <a href="javascript:;" data-repeater-delete=""
                                                           class=" btn btn-sm font-weight-bolder btn-light-danger">
                                                            <i class="la la-trash-o"></i>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 ">
                                                <a href="javascript:;" data-repeater-create=""
                                                   class="btn btn-sm font-weight-bolder btn-light-primary">
                                                    <i class="la la-plus"></i> Add more
                                                </a>
                                            </div>


                                        </div>
                                        <div class="col-md-4 border text-center p-1">
                                            <div class="form-group">
                                                <h3>Details</h3>
                                                <div class="inner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">--}}
    {{--        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">--}}
    {{--            <div class="modal-content">--}}
    {{--                <div class="modal-header">--}}
    {{--                    <h5 class="modal-title" >I am Compaign Title</h5>--}}
    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
    {{--                        <i aria-hidden="true" class="ki ki-close"></i>--}}
    {{--                    </button>--}}
    {{--                </div>--}}
    {{--                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->--}}
    {{--                <div class="modal-body">--}}
    {{--                    <div class="container">--}}
    {{--                        <div class="card card-custom">--}}
    {{--                            <div class="card-body p-0">--}}
    {{--                                <!--begin::Invoice-->--}}
    {{--                                <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">--}}
    {{--                                    <div class="col-md-9">--}}
    {{--                                        <!-- begin: Invoice header-->--}}
    {{--                                        <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">--}}
    {{--                                            <h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>--}}
    {{--                                            <div class="d-flex flex-column align-items-md-end px-0">--}}
    {{--                                                <!--begin::Logo-->--}}
    {{--                                                <a href="#" class="mb-5 max-w-200px">--}}
    {{--															<span class="svg-icon svg-icon-full">--}}
    {{--																<!--begin::Svg Icon | path:assets/media/svg/logos/duolingo.svg-->--}}
    {{--																<svg xmlns="http://www.w3.org/2000/svg" width="113" height="31" viewBox="0 0 113 31" fill="none">--}}
    {{--																	<path d="M26.6802 13.33V13.485C26.6802 14.9575 26.6087 15.8875 26.4659 16.275C26.2517 16.895 25.966 17.36 25.4661 17.67C25.0376 17.98 24.5377 18.1358 23.8949 18.1358C23.2529 18.1358 22.7523 17.9808 22.3245 17.67C21.8953 17.3608 21.6103 16.895 21.3954 16.275C21.1811 15.7325 21.1104 14.8025 21.1104 13.4075V5.425H17.3254V13.485C17.3968 15.345 17.5396 16.6625 17.7539 17.515C18.1824 18.9108 18.8965 19.995 19.8963 20.8483C20.8962 21.6233 22.2531 22.0108 23.8242 22.0108C25.324 22.0108 26.538 21.7008 27.5379 21.0025C28.537 20.3058 29.2511 19.375 29.7518 18.0575C30.1088 17.205 30.3231 15.655 30.3231 13.485V5.425H26.6087V13.33H26.6802ZM15.6107 0H11.897V6.2775C11.8256 6.2775 11.7542 6.2 11.6828 6.2C10.4687 5.425 9.11182 5.0375 7.75492 5.0375C5.82669 5.0375 4.1127 5.735 2.61297 7.2075C0.898984 8.9125 0.0419922 11.005 0.0419922 13.485C0.0419922 15.81 0.827568 17.825 2.3273 19.4525C3.82704 21.0808 5.68314 21.9325 7.75492 21.9325C9.18324 21.9325 10.5401 21.545 11.7542 20.8483C12.9683 20.0733 13.8967 19.065 14.6109 17.7483C15.2536 16.5858 15.54 15.345 15.6107 14.0283V13.4858V0ZM10.6823 16.895C9.89668 17.7483 8.96899 18.2125 7.7542 18.2125C6.61155 18.2125 5.61172 17.7475 4.89756 16.895C4.11199 16.0425 3.75491 14.88 3.75491 13.485C3.75491 12.1675 4.11199 11.005 4.89756 10.1525C5.68314 9.3 6.61155 8.835 7.7542 8.835C8.96828 8.835 9.89668 9.3 10.6823 10.1525C11.4685 11.005 11.8256 12.1675 11.8256 13.485C11.8256 14.88 11.4685 16.0425 10.6823 16.895ZM46.2481 9.3C45.534 7.9825 44.6049 6.975 43.3915 6.2C42.1774 5.425 40.8205 5.0375 39.4636 5.0375C37.5354 5.0375 35.8207 5.735 34.3217 7.2075C32.6077 8.9125 31.75 10.9275 31.75 13.485C31.75 15.81 32.5363 17.825 34.0353 19.4525C35.5357 21.0808 37.3918 21.9325 39.4636 21.9325C40.8919 21.9325 42.2481 21.545 43.4629 20.8483C44.677 20.0733 45.6054 19.065 46.3195 17.7483C47.0337 16.4308 47.3908 15.0358 47.3908 13.4858C47.248 12.0125 46.9623 10.6175 46.2481 9.3ZM42.3917 16.895C41.6061 17.7483 40.6777 18.2125 39.4636 18.2125C38.2495 18.2125 37.3211 17.7475 36.607 16.895C35.8214 16.0425 35.4643 14.88 35.4643 13.485C35.4643 12.1675 35.8214 11.005 36.6077 10.1525C37.3933 9.3 38.321 8.835 39.4643 8.835C40.6777 8.835 41.6068 9.3 42.3924 10.1525C43.178 11.005 43.535 12.1675 43.535 13.485C43.5336 14.88 43.1058 16.0425 42.3917 16.895ZM60.2457 21.7H64.0307V6.1225H60.2457V21.7ZM53.8182 17.515C53.3897 17.2058 53.0326 16.74 52.8898 16.12C52.747 15.655 52.6756 14.725 52.6756 13.33V0H48.9619V13.2525C49.0334 15.4225 49.1762 16.895 49.5333 17.825C50.0332 19.065 50.7473 20.0725 51.7472 20.77C52.747 21.4675 53.9618 21.7775 55.4608 21.7775H58.3174V17.98H55.3894C54.7466 17.98 54.246 17.825 53.8182 17.515ZM78.7424 9.145C78.2425 7.905 77.5283 6.8975 76.5278 6.2C75.5287 5.5025 74.3139 5.1925 72.8142 5.1925C71.1716 5.1925 69.8861 5.58 68.8863 6.355C67.8872 7.13 67.173 8.215 66.7438 9.6875C66.5296 10.4625 66.3867 11.8575 66.3153 13.7175V21.7H70.1003V13.95V13.64C70.1003 12.3225 70.1725 11.315 70.386 10.7725C70.6003 10.1525 70.8859 9.765 71.3144 9.3775C71.7429 9.0675 72.2428 8.9125 72.8856 8.9125C73.5276 8.9125 74.0282 9.0675 74.4567 9.3775C74.8852 9.6875 75.2423 10.1525 75.4558 10.7725C75.5987 11.2375 75.6701 12.1675 75.6701 13.5625V21.7H79.3837V14.88V13.64C79.3137 11.5475 79.0995 9.9975 78.7424 9.145ZM62.0311 0.31C60.9598 0.31 60.0314 1.3175 60.0314 2.48C60.0314 3.6425 60.8884 4.65 62.0311 4.65C63.1023 4.65 64.0307 3.72 64.0307 2.48C64.0307 1.24 63.1023 0.31 62.0311 0.31ZM111.951 9.455C111.237 8.1375 110.308 7.13 109.094 6.355C107.88 5.58 106.595 5.27 105.094 5.27C102.952 5.27 101.166 6.1225 99.6666 7.75C98.1661 9.3775 97.3813 11.3925 97.3813 13.7175C97.3813 16.1975 98.2383 18.2892 99.9522 19.995C101.453 21.4675 103.166 22.165 105.094 22.165C106.522 22.165 107.808 21.7775 109.022 21.0025C110.236 20.2275 111.237 19.22 111.879 17.9025C112.593 16.5858 112.95 15.19 112.95 13.7183C113.022 12.1675 112.665 10.7725 111.951 9.455ZM109.236 13.7175C109.236 15.1125 108.879 16.1975 108.094 17.05C107.308 17.9025 106.38 18.3675 105.166 18.3675C104.022 18.3675 103.023 17.9017 102.309 17.05C101.523 16.1975 101.166 15.035 101.166 13.7175C101.166 12.3225 101.523 11.16 102.309 10.3075C103.095 9.455 104.022 8.99 105.166 8.99C106.308 8.99 107.308 9.455 108.094 10.3075C108.879 11.16 109.236 12.3225 109.236 13.7175C109.236 13.64 109.236 13.64 109.236 13.7175ZM96.0965 13.02C96.0244 11.7025 95.6673 10.54 95.0967 9.3775C94.3825 8.06 93.4541 7.0525 92.3122 6.2775C91.0981 5.5025 89.8126 5.1925 88.3843 5.1925C86.2418 5.1925 84.4564 5.9675 82.9559 7.6725C81.4562 9.3 80.7421 11.315 80.7421 13.64C80.7421 16.12 81.599 18.2125 83.2416 19.84C84.7413 21.3125 86.3832 22.0108 88.3121 22.0108C89.669 22.0108 91.026 21.6233 92.24 20.8483C92.3122 20.77 92.3829 20.77 92.4543 20.6933V22.5525C92.4543 23.9483 92.0258 25.0325 91.3116 25.8858C90.526 26.7383 89.5976 27.2025 88.455 27.2025C87.3123 27.2025 86.3839 26.815 85.5983 25.9625L83.1702 28.7525L83.2416 28.83C84.7413 30.3017 86.4553 31 88.3121 31C89.669 31 91.026 30.6125 92.24 29.8375C93.4541 29.0625 94.3825 28.055 95.0967 26.7375C95.8108 25.42 96.0965 24.025 96.0965 22.5525V14.105V13.5625C96.1679 13.4075 96.1679 13.175 96.0965 13.02ZM92.4536 13.5625C92.4536 14.9575 92.0965 16.0425 91.3109 16.895C90.5253 17.7483 89.5969 18.2125 88.4543 18.2125C87.3116 18.2125 86.3832 17.7475 85.5976 16.895C84.812 16.0425 84.455 14.9575 84.455 13.5625C84.455 12.1675 84.812 11.0825 85.5976 10.1525C86.3832 9.3 87.3116 8.835 88.4543 8.835C89.5969 8.835 90.5967 9.3 91.3109 10.1525C92.0258 11.0825 92.4536 12.1675 92.4536 13.5625Z" fill="#1CB0F6"></path>--}}
    {{--																</svg>--}}
    {{--                                                                <!--end::Svg Icon-->--}}
    {{--															</span>--}}
    {{--                                                </a>--}}
    {{--                                                <!--end::Logo-->--}}
    {{--                                                <span class="d-flex flex-column align-items-md-end font-size-h5 font-weight-bold text-muted">--}}
    {{--															<span>Cecilia Chapman, 711-2880 Nulla St, Mankato</span>--}}
    {{--															<span>Mississippi 96522</span>--}}
    {{--														</span>--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}

    {{--                                        <!--end: Invoice header-->--}}
    {{--                                        <!--begin: Invoice body-->--}}
    {{--                                        <div class="row border-bottom pb-10">--}}
    {{--                                            <div class="col-md-9 py-md-10 pr-md-10">--}}
    {{--                                                <div class="table-responsive">--}}
    {{--                                                    <table class="table">--}}
    {{--                                                        <thead>--}}
    {{--                                                        <tr>--}}
    {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">Description</th>--}}
    {{--                                                            <th class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">Hours</th>--}}
    {{--                                                            <th class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">Rate</th>--}}
    {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">Amount</th>--}}
    {{--                                                        </tr>--}}
    {{--                                                        </thead>--}}
    {{--                                                        <tbody>--}}
    {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
    {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-center">--}}
    {{--																		<span class="navi-icon mr-2">--}}
    {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
    {{--																		</span>Creative Designs</td>--}}
    {{--                                                            <td class="text-right pt-7">80</td>--}}
    {{--                                                            <td class="text-right pt-7">$40.00</td>--}}
    {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">$3200.00</td>--}}
    {{--                                                        </tr>--}}

    {{--                                                        </tbody>--}}
    {{--                                                    </table>--}}
    {{--                                                </div>--}}
    {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
    {{--                                                <div class="d-flex flex-column flex-md-row">--}}
    {{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
    {{--                                                        <div class="font-weight-bold font-size-h6 mb-3">BANK TRANSFER</div>--}}
    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
    {{--                                                            <span class="text-right">Barclays UK</span>--}}
    {{--                                                        </div>--}}
    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
    {{--                                                            <span class="text-right">1234567890934</span>--}}
    {{--                                                        </div>--}}
    {{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Model</span>--}}
    {{--                                                            <span class="text-right">BARC0032UK</span>--}}
    {{--                                                        </div>--}}
    {{--                                                    </div>--}}
    {{--                                                </div>--}}
    {{--                                            </div>--}}
    {{--                                            <div class="col-md-3 border-left-md pl-md-10 py-md-10 text-right">--}}
    {{--                                                <!--begin::Total Amount-->--}}
    {{--                                                <div class="font-size-h4 font-weight-bolder text-muted mb-3">TOTAL AMOUNT</div>--}}
    {{--                                                <div class="font-size-h1 font-weight-boldest">$20,600.00</div>--}}
    {{--                                                <div class="text-muted font-weight-bold mb-16">Taxes included</div>--}}
    {{--                                                <!--end::Total Amount-->--}}
    {{--                                                <div class="border-bottom w-100 mb-16"></div>--}}
    {{--                                                <!--begin::Invoice To-->--}}
    {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Campaign Status.</div>--}}
    {{--                                                <div class="font-size-lg font-weight-bold mb-10">Iris Watson.--}}
    {{--                                                    <br>Fredrick Nebraska 20620</div>--}}
    {{--                                                <!--end::Invoice To-->--}}
    {{--                                                <!--begin::Invoice No-->--}}
    {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Payment Status</div>--}}
    {{--                                                <div class="font-size-lg font-weight-bold mb-10">56758</div>--}}
    {{--                                                <!--end::Invoice No-->--}}
    {{--                                                <!--begin::Invoice Date-->--}}
    {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">DATE</div>--}}
    {{--                                                <div class="font-size-lg font-weight-bold">12 May, 2020</div>--}}
    {{--                                                <!--end::Invoice Date-->--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                        <!--end: Invoice body-->--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}

    {{--                                <!--end::Invoice-->--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="modal-footer">--}}
    {{--                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>--}}
    {{--                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}


    <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Choose one Payment option: </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-2 pr-2" >
                                        <div class="col-md-12 mt-4 ">
{{--                                            <h5 class="text-center d-block">Choose payment options</h5>--}}
                                        </div><br><br><br>
                                        <div class="col-lg-6  ">
                                            <div class="form-group text-center" >

                                                <a href="javascript:;" data-toggle="modal"   data-target="#payment" class="paypalA" title="Pay now">
                                                    <i class="fab fa-paypal mt-3" style="color: #2222ca !important; font-size: 50px; "></i>
                                                </a>
                                            </div>
                                        </div>      <div class="col-lg-6 ">
                                            <div class="form-group text-center" >
                                                <a href="javascript:;"  data-toggle="modal"   data-target="#payment"  class="paypalA" title="Pay now">
                                                    <i class="la la-cc-stripe" style="color: #4db9fe; !important; font-size:65px;"></i>
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create=""
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Pay now
                    </a>&nbsp
                    <button type="button" class="btn btn-sm font-weight-bolder btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-2 pr-2" >
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block"><h5 class="d-inline">Campaign Id:  </h5> <span class="float-right mr-30">CMP-1212</span></span>
                                        </div><br><br><br>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Write feedback about campaign:</label>
                                                <textarea name="s_u" class="w-100" type="textarea" rows="10" ></textarea>
                                                <span class="form-text text-muted"> feedback.</span>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create=""
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>



                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="statusUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Campaign Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">

                <div class="card card-custom">
                    <div class="card-body p-0">
                        <!--begin::Invoice-->
                        <div class="row justify-content-center pl-4 p-1 px-md-0">
                            <div class="col-md-12">

                                <div class="form-group row pl-4" >
                                    <div class="col-md-12 mt-4 ">
                                        <span class="text-center d-block"><h5 class="d-inline">Campaign Id:  </h5> <span >CMP-1212</span></span>
                                    </div><br><br><br>
                                    <div class="col-lg-9">
                                        <div class="form-check">
                                            <label>Status Update:</label>
                                            <select name="s_u"
                                                    class="form-control">
                                                <option value="" selected disabled>
                                                    Choose one option
                                                </option>
                                                <option value="hyundai">Approved</option>
                                                <option value="suzuki">Rejected</option>
                                                <option value="Toyota">Pending</option>
                                                <option value="Toyota">onhold</option>
                                            </select>
                                            <span class="form-text text-muted"> Choose status.</span>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <!--end::Invoice-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" data-repeater-create=""
                   class="btn btn-sm font-weight-bolder btn-light-primary">
                    Update
                </a>&nbsp
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                </button>



                <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
            </div>
        </div>
    </div>
    </div>




    <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Update Campaign Status</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">

                <div class="card card-custom">
                    <div class="card-body p-0">
                        <!--begin::Invoice-->
                        <div class="row justify-content-center pl-4 p-1 px-md-0">
                            <div class="col-md-12">

                                <div class="form-group row pl-4" >
                                    <div class="col-md-12 mt-4 ">
                                        <span class="text-center d-block"><h5 class="d-inline">Campaign Id:  </h5> <span >CMP-1212</span></span>
                                    </div><br><br><br>
                                    <div class="col-lg-8">
                                        <div class="form-check">
                                            <label>Status Update:</label>
                                            <select name="s_u"
                                                    class="form-control">
                                                <option value="" selected disabled>
                                                    Choose one option
                                                </option>
                                                <option value="hyundai">Approved</option>
                                                <option value="suzuki">Rejected</option>
                                                <option value="Toyota">Pending</option>
                                                <option value="Toyota">onhold</option>
                                            </select>
                                            <span class="form-text text-muted"> Choose status.</span>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <!--end::Invoice-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" data-repeater-create=""
                   class="btn btn-sm font-weight-bolder btn-light-primary">
                    Update
                </a>&nbsp
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                </button>



                <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
            </div>
        </div>
    </div>
    </div>



    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" >Edit campaign</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">
                                <div class="col-md-12">
                                    <div id="kt_repeater_1">
                                        <div class="form-group row" id="kt_repeater_1">
                                            <div data-repeater-list="">
                                                <div data-repeater-item="" class="form-group row align-items-center">
                                                    <div class="col-md-6">
                                                        <label>State:</label>
                                                        <input type="text" class="form-control" name="[0][state]" placeholder="Select state" onkeyup="showdetail();">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>City:</label>
                                                        <input type="text" class="form-control" placeholder="Select city" name="[0][city]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Zip:</label>
                                                        <input type="text" class="form-control" placeholder="Enter zip code" name="[0][zip]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6 mt-4">
                                                        <a href="javascript:;" data-repeater-delete="" class=" btn btn-sm font-weight-bolder btn-light-danger">
                                                            <i class="la la-trash-o"></i>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 ">
                                                <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary">
                                                    <i class="la la-plus"></i> Add more
                                                </a>
                                            </div>





                                        </div>
                                        <div class="col-md-4 border text-center p-1">
                                            <div class="form-group">
                                                <h3>Details</h3>
                                                <div class="inner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>






@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection


{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- vendors --}}
    <script>
        function del()
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }


        $('#kt_datatable').dataTable({
            "scrollX": true
        });
    </script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

    {{-- page scripts --}}
    <script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/contacts/list-datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

    <script>

        $(document).on('click', '#toggleSearchForm', function(){

            $('#table-search-form').slideToggle();

        });







    </script>

@endsection
