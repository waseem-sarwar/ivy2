@php  $url= (Request::segment(1) == "admin")?'camp.detail':'user.camp.detail' @endphp
@if(Auth::user()->role_id == 3 )
    <div class="card card-custom {{ @$class }}">
        {{--         Header--}}
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">My Campaigns</span>
                {{--                <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new campaigns</span>--}}
            </h3>
        </div>

        {{--         Body--}}
        <div class="card-body pt-3 pb-0">
            {{--             Table--}}
            <div class="table-responsive">
                <table class="table table-borderless table-vertical-center">
                    <thead>
                    <tr>
                        <th class="p-0"></th>
                        <th class="p-0" ></th>
                        <th class="p-0" style="min-width: 100px"></th>
                        <th class="p-0" style="min-width: 150px"></th>
                        <th class="p-0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($my_campaigns as $my_campaign)
                        <tr>
                            {{--                            @dd($my_campaign);--}}
                            <td class="pl-0 py-4">
                                <div class="symbol symbol-50 symbol-light mr-1">
                                <span class="font-weight-boldest">
                                   CMP-{{$my_campaign->car_id}}
                                    {{--                                                                        <img src="{{ asset('media/svg/misc/005-bebo.svg') }}" class="h-50 align-self-center"/>--}}
                                </span>
                                </div>
                            </td>
                            <td class="pl-0">
                                <a  style="cursor: default"  class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{@$my_campaign->campaign->campaign->title}}</a>
                            </td>
                            <td class="text-right">
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                              {{@$my_campaign->campaign->campaign->interval}}
                            </span>
                            </td>
                            {{--                            <td class="text-right">--}}
                            {{--                            <span class="font-weight-bold">--}}
                            {{--                            {{$my_campaign->country}}--}}
                            {{--                            </span>--}}
                            {{--                            </td>--}}
                            <td class="text-right">
                                <span class="label label-lg label-light- label-inline">{{@$my_campaign->status}}</span>
                            </td>
                            <td class="text-right pr-0">
                                <a href="{{route($url,[$my_campaign->campaign->slug])}}" class="btn btn-icon btn-light btn-sm">
                                    {{ Metronic::getSVG("media/svg/icons/General/Settings-1.svg", "svg-icon-md svg-icon-primary") }}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif

{{--Ent_Driver--}}
@if(Auth::user()->role_id == 4 )
    <div class="card card-custom {{ @$class }}">
        {{--         Header--}}
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">My Driver Campaigns</span>
                {{--                <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new campaigns</span>--}}
            </h3>
        </div>

        {{--         Body--}}
        <div class="card-body pt-3 pb-0">
            {{--             Table--}}
            <div class="table-responsive">
                <table class="table table-borderless table-vertical-center">
                    <thead>
                    <tr>
                        <th class="p-0" style="width: 50px"></th>
                        <th class="p-0" style="min-width: 200px"></th>
                        <th class="p-0" style="min-width: 100px"></th>
                        <th class="p-0" style="min-width: 100px"></th>
                        {{--                        <th class="p-0" style="min-width: 115px"></th>--}}
                        {{--                        <th class="p-0" style="min-width: 110px"></th>--}}
                        {{--                        <th class="p-0" style="min-width: 110px"></th>--}}
                    </tr>
                    </thead>
                    <tbody>


                    @foreach($my_campaigns as $my_campaign)
                        @if($my_campaign->appliedcampaigns->count() > 0 )
                            @foreach($my_campaign->appliedcampaigns as $applied)
                                {{--                    {dd($my_campaign)}--}}
                                <tr>
                                    <td class="pl-0 py-2">
                                        <div class="symbol symbol-100 w-100 symbol-light mr-1">
                                <span class="font-weight-boldest">
                                   CMP-{{@$applied->campaign->id}}

                                    {{--                                                                        <img src="{{ asset('media/svg/misc/005-bebo.svg') }}" class="h-50 align-self-center"/>--}}
                                </span>
                                        </div>
                                    </td>
                                    <td class="pl-0">
                                        <a style="cursor: default" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{@$applied->campaign->campaign->title}}</a>
                                        <div>
                                            {{--                                    <span class="text-muted font-weight-bolder">{{@$my_campaign->first_name}}</span>--}}
                                            {{--                                    <a class="text-muted font-weight-bold text-hover-primary" href="#">{{$my_campaign->Assignedcampaign->user->email}}</a>--}}
                                        </div>
                                    </td>
                                    <td class="pl-0">
                                        <a style="cursor: default" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{@$applied->user->name}}</a>
                                        <div>
                                            {{--                                    <span class="text-muted font-weight-bolder">{{@$my_campaign->first_name}}</span>--}}
                                            {{--                                    <a class="text-muted font-weight-bold text-hover-primary" href="#">{{$my_campaign->Assignedcampaign->user->email}}</a>--}}
                                        </div>
                                    </td>
                                    {{--                            <td class="text-right">--}}
                                    {{--                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">--}}
                                    {{--                                $2,50--}}
                                    {{--                            </span>--}}
                                    {{--                            </td>--}}
                                    {{--                            <td class="text-right">--}}
                                    {{--                            <span class="font-weight-bold">--}}
                                    {{--                            {{@$applied->Assignedcampaign->country}}--}}
                                    {{--                            </span>--}}
                                    {{--                            </td>--}}
                                    <td class="text-right" style="min-width:180px">
                                        <span class="label label-lg label-light-success label-inline" >{{@$applied->status}}</span>
                                    </td>
                                    {{--                            <td class="text-right pr-0">--}}

                                    {{--                                <a href="{{@route('camp.detail',[$applied->Assignedcampaign->id])}}" class="btn btn-icon btn-light btn-sm">--}}
                                    {{--                                    {{ Metronic::getSVG("media/svg/icons/General/Settings-1.svg", "svg-icon-md svg-icon-primary") }}--}}
                                    {{--                                </a>--}}
                                    {{--                            </td>--}}

                                </tr>
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif

{{--Admin--}}
@if(Auth::user()->role_id == 1 )
    <div class="card card-custom {{ @$class }}">
        {{--         Header--}}
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">Campaigns</span>
                {{--                <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new campaigns</span>--}}
            </h3>
        </div>

        {{--         Body--}}
        <div class="card-body pt-3 pb-0">
            {{--             Table--}}
            <div class="table-responsive">
                <table class="table table-borderless table-vertical-center">
                    <thead>
                    <tr>
                        <th class="p-0"></th>
                        <th class="p-0" ></th>
                        <th class="p-0" style="min-width: 100px"></th>
                        <th class="p-0" style="min-width: 150px"></th>
                        <th class="p-0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($my_campaigns as $my_campaign)
                        <tr>
                            {{--                            @dd($my_campaign);--}}
                            <td class="pl-0 py-4">
                                <div class="symbol symbol-50 symbol-light mr-1">
                                <span class="font-weight-boldest">
                                   CMP-{{$my_campaign->id}}
                                    {{--                                                                        <img src="{{ asset('media/svg/misc/005-bebo.svg') }}" class="h-50 align-self-center"/>--}}
                                </span>
                                </div>
                            </td>
                            <td class="pl-0">
                                <a  style="cursor: default" href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$my_campaign->campaign->title}}</a>
                                <div>
                                    <span  class="text-muted font-weight-bolder">{{$my_campaign->user->name}}</span>
                                    <a class="text-muted font-weight-bold text-hover-primary" href="#">{{$my_campaign->user->email}}</a>
                                </div>
                            </td>
                            <td class="text-right">
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                 {{ $my_campaign->total_price}}
                            </span>
                            </td>
                            {{--                            <td class="text-right">--}}
                            {{--                            <span class="font-weight-bold">--}}
                            {{--                            {{$my_campaign->country}}--}}
                            {{--                            </span>--}}
                            {{--                            </td>--}}
                            <td class="text-right">
                                <span class="label label-lg label-light-{{@$my_campaign->status->class}} label-inline">{{@$my_campaign->status->name}}</span>
                            </td>
                            <td class="text-right pr-0">
                                <a href="{{route($url,[$my_campaign->slug])}}" class="btn btn-icon btn-light btn-sm">
                                    {{ Metronic::getSVG("media/svg/icons/General/Settings-1.svg", "svg-icon-md svg-icon-primary") }}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif

{{--Adviser--}}
@if(Auth::user()->role_id == 2 )
    <div class="card card-custom {{ @$class }}">
        {{--         Header--}}
        <div class="card-header border-0 pt-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label font-weight-bolder text-dark">My Campaigns</span>
                {{--                <span class="text-muted mt-3 font-weight-bold font-size-sm">More than 400+ new campaigns</span>--}}
            </h3>
        </div>

        {{--         Body--}}
        <div class="card-body pt-3 pb-0">
            {{--             Table--}}
            <div class="table-responsive">
                <table class="table table-borderless table-vertical-center">
                    <thead>
                    <tr>
                        <th class="p-0"></th>
                        <th class="p-0" ></th>
                        <th class="p-0" style="min-width: 100px"></th>
                        <th class="p-0" style="min-width: 150px"></th>
                        <th class="p-0"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($my_campaigns as $my_campaign)
                        <tr>
                            {{--                            @dd($my_campaign);--}}
                            <td class="pl-0 py-4">
                                <div class="symbol symbol-50 symbol-light mr-1">
                                <span class="font-weight-boldest">
                                   CMP-{{$my_campaign->id}}
                                    {{--                                                                        <img src="{{ asset('media/svg/misc/005-bebo.svg') }}" class="h-50 align-self-center"/>--}}
                                </span>
                                </div>
                            </td>
                            <td class="pl-0">
                                <a  style="cursor: default" href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$my_campaign->campaign->title}}</a>
                                <div>
{{--                                  --}}{{----}}{{--  <span  class="text-muted font-weight-bolder">{{$my_campaign->user->name}}</span>--}}
{{--                                    <a class="text-muted font-weight-bold text-hover-primary" href="#">{{$my_campaign->user->email}}</a>--}}
                                </div>
                            </td>
                            <td class="text-right">
                            <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                 {{ $my_campaign->total_price}}
                            </span>
                            </td>
                            {{--                            <td class="text-right">--}}
                            {{--                            <span class="font-weight-bold">--}}
                            {{--                            {{$my_campaign->country}}--}}
                            {{--                            </span>--}}
                            {{--                            </td>--}}
                            <td class="text-right">
                                <span class="label label-lg label-light-{{@$my_campaign->status->class}} label-inline">{{@$my_campaign->status->name}}</span>
                            </td>
                            <td class="text-right pr-0">
                                <a href="{{route($url,[$my_campaign->slug])}}" class="btn btn-icon btn-light btn-sm">
                                    {{ Metronic::getSVG("media/svg/icons/General/Settings-1.svg", "svg-icon-md svg-icon-primary") }}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endif
