{{-- List Widget 1 --}}

<div class="card card-custom {{ @$class }}">
    {{-- Header --}}
    <div class="card-header border-0 pt-5">
        <h3 class="card-title align-items-start flex-column">
            <span class="card-label font-weight-bolder text-dark">Tickets Overview</span>
            <span class="text-muted mt-3 font-weight-bold font-size-sm">Recent tickets</span>
        </h3>
    </div>

    {{-- Body --}}
    <div class="card-body pt-8">
        @if(\Illuminate\Support\Facades\Auth::user()->role_id == '1')
            @foreach($data as $dat)
{{--                    @dd($dat)--}}
{{--                 Item--}}
                @foreach($dat as $datum)
                    <div class="d-flex align-items-center mb-10">
{{--                         Symbol--}}
                        <div class="symbol symbol-40 symbol-light-success mr-5">
                        <span class="symbol-label">
                            {{ Metronic::getSVG("media/svg/icons/Communication/Group-chat.svg", "svg-icon-lg svg-icon-success") }}
                        </span>
                        </div>

{{--                         Text--}}
                        <div class="d-flex flex-column font-weight-bold">
                            <p style="cursor: default"  class="text-dark text-hover-primary mb-1 font-size-lg">{{$datum->title}}</p>
                            <span class="text-muted">{{$datum->detail}}</span>
                        </div>
                    </div>

                @endforeach
            @endforeach
        @endif

        @if(\Illuminate\Support\Facades\Auth::user()->role_id == 2)
            @foreach($data as $dat)
                @foreach($dat as $datum)
                    <div class="d-flex align-items-center mb-10">
{{--                         Symbol--}}
                        <div class="symbol symbol-40 symbol-light-success mr-5">
                <span class="symbol-label">
                    {{ Metronic::getSVG("media/svg/icons/Communication/Group-chat.svg", "svg-icon-lg svg-icon-success") }}
                </span>
                        </div>

{{--                         Text--}}
                        <div class="d-flex flex-column font-weight-bold">
                            <p style="cursor: default"  class="text-dark text-hover-primary mb-1 font-size-lg">{{$datum->title}}</p>
                            <span class="text-muted">{{$datum->detail}}</span>
                        </div>
                    </div>

                @endforeach
            @endforeach
        @endif

        @if(\Illuminate\Support\Facades\Auth::user()->role_id == 3)
                @foreach($data as $dat)
                    {{--    @dd($dat)--}}
                    {{-- Item --}}
                    @foreach($dat as $datum)
                        <div class="d-flex align-items-center mb-10">
                            {{-- Symbol --}}
                            <div class="symbol symbol-40 symbol-light-success mr-5">
                        <span class="symbol-label">
                            {{ Metronic::getSVG("media/svg/icons/Communication/Group-chat.svg", "svg-icon-lg svg-icon-success") }}
                        </span>
                            </div>

                            {{-- Text --}}
                            <div class="d-flex flex-column font-weight-bold">
                                <a style="cursor: default" class="text-dark text-hover-primary mb-1 font-size-lg">{{$datum->title}}</a>
                                <span class="text-muted">{{$datum->detail}}</span>
                            </div>
                        </div>

                    @endforeach
                @endforeach
            @endif

        @if(\Illuminate\Support\Facades\Auth::user()->role_id == 4)
            @foreach($data as $dat)
                @foreach($dat as $datum)
{{--                    @dd($dat);--}}
                    <div class="d-flex align-items-center mb-10">
{{--                         Symbol--}}
                        <div class="symbol symbol-40 symbol-light-success mr-5">
                <span class="symbol-label">
                    {{ Metronic::getSVG("media/svg/icons/Communication/Group-chat.svg", "svg-icon-lg svg-icon-success") }}
                </span>
                        </div>

{{--                         Text--}}
                        <div class="d-flex flex-column font-weight-bold">
                            <p style="cursor: default" class="text-dark text-hover-primary mb-1 font-size-lg">{{$datum->title}}</p>
                            <span class="text-muted">{{$datum->detail}}</span>
                        </div>
                    </div>

                @endforeach
            @endforeach
        @endif

    </div>
</div>
