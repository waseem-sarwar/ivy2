{{-- List Widget 9 --}}

<div class="card card-custom {{ @$class }}">
    {{-- Header --}}
    <div class="card-header align-items-center border-0 mt-4">


    {{-- Body --}}
        @php $data=systemAlerts(); @endphp
        @if(count($data)==0)
        <h3 class="card-title align-items-start flex-column">
        <span class="font-weight-bolder text-dark">Recent Notifications</span>
        <span class="text-muted mt-3 font-weight-bold font-size-sm">0 Notifications</span>
        </h3>
       <div class="card-body pt-4">
        <div class="">
            <!--begin::Item-->
            <div class="timeline-item align-items-start ">
                <!--begin::Label-->
{{--                <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg">08:42</div>--}}
                <!--end::Label-->
                <!--begin::Badge-->
{{--                <div class="timeline-badge">--}}
{{--                    <i class="fa fa-genderless text-dark icon-xl"></i>--}}
{{--                </div>--}}
                <!--end::Badge-->
                <!--begin::Text-->
                <div class="font-weight-bold font-size-lg timeline-content pl-20">No Notification Found</div>
                <!--end::Text-->
            </div>
            <!--end::Item-->
        </div>
    </div>
        @else
            <h3 class="card-title align-items-start flex-column">
                <span class="font-weight-bolder text-dark">Recent Notifications</span>
{{--                <span class="text-muted mt-3 font-weight-bold font-size-sm">Recent Notifications</span>--}}
            </h3>
          @foreach($data as $dat)
                <div class="card-body pt-4">
                    <div class="timeline timeline-6 mt-3">
                        <!--begin::Item-->
                        <div class="timeline-item align-items-start">
                            <!--begin::Label-->
                            <div class="timeline-label font-weight-bolder text-dark-75 font-size-lg text-muted">{{Carbon\Carbon::parse($dat->created_at)->diffForHumans()}}</div>
                            <!--end::Label-->
                            <!--begin::Badge-->
                            <div class="timeline-badge">
                                <i class="fa fa-genderless text-success icon-xl"></i>
                            </div>
                            <!--end::Badge-->
                            <!--begin::Text-->
                            <div class="font-weight-bold font-size-lg timeline-content  pl-3">{{$dat->subject}}<br>
                            <span class="font-weight-bold font-size-lg timeline-content text-muted">{!! $dat->detail !!}</span></div>
                            <!--end::Text-->
                        </div>
                        <!--end::Item-->
                    </div>
                </div>
          @endforeach
        @endif
    </div>
</div>
