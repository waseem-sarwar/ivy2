@php
 $role=Auth::user()->role_id;
@endphp
@if($role=='4')
<?php $layout='layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout='layout.driver'; ?>
@elseif($role=='2')
    <?php $layout='layout.default'; ?>
@elseif($role=='1')
    <?php $layout='layout.admin'; ?>
@endif

@extends($layout)

@section('content')
    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
        .asdfasdf {
            border: 1px solid #ffffff;
            margin-top: 18px;
            padding: 14px 19px;
            border-radius: 6px;
            box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
        }
        ul.compainlist li {
            margin-top: 8px;
            font-size: 15px;
        }
        .owpierjpwoq {
            font-size: 52px;
            color: #3c9bd1;
            margin-top: 11px;
            margin-right: 19px;
        }
    </style>
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
            <!--begin::Card-->
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <form action="/stripe-paymnent-charge" method="post" id="payment-form">
                        <div class="form-row">
                            <label for="card-element">
                                Credit or debit card
                            </label>
                            <div id="card-element">
                                <!-- card fields display here. -->
                            </div>
                        {{ csrf_field() }}
                        <!-- Used to display form errors. -->
                            <div id="card-errors" role="alert"></div>
                        </div>
                        <button>Submit Payment</button>
                    </form>

                    <style>

                        .StripeElement {
                            box-sizing: border-box;
                            width:100%;
                            height: 40px;
                            padding: 10px 12px;
                            border: 1px solid transparent;
                            border-radius: 4px;
                            background-color: white;
                            box-shadow: 0 1px 3px 0 #e6ebf1;
                            -webkit-transition: box-shadow 150ms ease;
                            transition: box-shadow 150ms ease;
                        }
                        #payment-form button{
                            margin-top:15px;
                        }
                        #payment-form button:hover{
                            background-color:#aadaff;
                            border-radius:5px;
                        }

                        .StripeElement--focus {
                            box-shadow: 0 1px 3px 0 #cfd7df;
                        }

                        .StripeElement--invalid {
                            border-color: #fa755a;
                        }

                        .StripeElement--webkit-autofill {
                            background-color: #fefde5 !important;
                        }
                    </style>

                    <script src="https://js.stripe.com/v3/"></script>
                    <script>
                        //start stripe
                        // Create a Stripe client.
                        var stripe = Stripe('pk_test_51HlYDGK7l7e9CDWNxOVqBen7Z7PYhRL6K0sUVS0OGEDFiH7l6Qg204EfEh2ju40UFsY13pbCLZzwPvugzt6D4eCy006htkwlIZ');

                        // Create an instance of Elements.
                        var elements = stripe.elements();

                        // Custom styling can be passed to options when creating an Element.
                        // (Note that this demo uses a wider set of styles than the guide below.)
                        var style = {
                            base: {
                                color: '#32325d',
                                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                                fontSmoothing: 'antialiased',
                                fontSize: '16px',
                                '::placeholder': {
                                    color: '#aab7c4'
                                }
                            },
                            invalid: {
                                color: '#fa755a',
                                iconColor: '#fa755a'
                            }
                        };

                        // Create an instance of the card Element.
                        var card = elements.create('card', {style: style});

                        // Add an instance of the card Element into the `card-element` <div>.
                        card.mount('#card-element');

                        // Handle real-time validation errors from the card Element.
                        card.addEventListener('change', function(event) {
                            var displayError = document.getElementById('card-errors');
                            if (event.error) {
                                displayError.textContent = event.error.message;
                            } else {
                                displayError.textContent = '';
                            }
                        });
                        // Handle form submission.
                        var form = document.getElementById('payment-form');
                        form.addEventListener('submit', function(event) {
                            event.preventDefault();

                            stripe.createToken(card).then(function(result) {
                                if (result.error) {
                                    // Inform the user if there was an error.
                                    var errorElement = document.getElementById('card-errors');
                                    errorElement.textContent = result.error.message;
                                } else {
                                    // Send the token to your server.
                                    stripeTokenHandler(result.token);
                                }
                            });
                        });
                        // Submit the form with the token ID.
                        function stripeTokenHandler(token) {
                            // Insert the token ID into the form so it gets submitted to the server
                            var form = document.getElementById('payment-form');
                            var hiddenInput = document.createElement('input');
                            hiddenInput.setAttribute('type', 'hidden');
                            hiddenInput.setAttribute('name', 'stripeToken');
                            hiddenInput.setAttribute('value', token.id);
                            form.appendChild(hiddenInput);
                            // Submit the form
                            form.submit();
                        }
                        //end stripe
                    </script>

                </div>
            </div>
            <!--end::Card-->

        </div>

@endsection



{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- vendors --}}
    <script>
        @if (\Session::has('error'))
        toastr.error('{!! \Session::get('error') !!}', 'Transaction Error');
        @endif
        @if (\Session::has('success'))
        toastr.success('{!! \Session::get('success') !!}', 'Transaction Successfull');
        @endif

        function del()
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }

        $('#kt_datatable').dataTable({
            "scrollX": true
        });
    </script>

    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

    {{-- page scripts --}}
    <script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/contacts/list-datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>



@endsection
