{{--@extends('layout.admin')--}}
{{--@section('title','Campaign Type')--}}
{{--@section('content')--}}

{{--    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>--}}
{{--    <script>--}}
{{--        @if (\Session::has('success'))--}}
{{--        toastr.success('{!! \Session::get('success') !!}', 'Created successfull');--}}
{{--        @endif--}}
{{--        @if (\Session::has('delete'))--}}
{{--        toastr.error('{!! \Session::get('delete') !!}', 'Deleted successfull');--}}
{{--        @endif--}}
{{--    </script>--}}
{{--@endsection--}}
@extends('layout.admin')
@section('title','Campaign Templates')
@section('content')
    <style>
        .morehide {
            background: #000000c2;
        }
    </style>
{{--    <div class="content d-flex flex-column flex-column-fluid pt-0 pb-0" id="kt_content">--}}
{{--        <!--begin::Subheader-->--}}
{{--        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">--}}
{{--            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">--}}
{{--                <!--begin::Info-->--}}
{{--                <div class="d-flex align-items-center flex-wrap mr-1">--}}
{{--                    <!--begin::Mobile Toggle-->--}}
{{--                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">--}}
{{--                        <span></span>--}}
{{--                    </button>--}}
{{--                    <!--end::Mobile Toggle-->--}}
{{--                    <!--begin::Page Heading-->--}}
{{--                    <div class="d-flex align-items-baseline flex-wrap mr-5">--}}
{{--                        <!--begin::Page Title-->--}}
{{--                        <h5 class="text-dark font-weight-bold my-1 mr-5">Campaign</h5>--}}
{{--                        <!--end::Page Title-->--}}
{{--                        <!--begin::Breadcrumb-->--}}
{{--                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">--}}
{{--                            <li class="breadcrumb-item">--}}
{{--                                <a href="" class="text-muted">Campaign Templates</a>--}}
{{--                            </li>--}}
{{--                            --}}{{----}}{{--                            <li class="breadcrumb-item">--}}
{{--                            --}}{{----}}{{--                                <a href="" class="text-muted">Profile</a>--}}
{{--                            --}}{{----}}{{--                            </li>--}}
{{--                            --}}{{----}}{{--                            <li class="breadcrumb-item">--}}
{{--                            --}}{{----}}{{--                                <a href="" class="text-muted">Profile 1</a>--}}
{{--                            --}}{{----}}{{--                            </li>--}}
{{--                            --}}{{----}}{{--                            <li class="breadcrumb-item">--}}
{{--                            --}}{{----}}{{--                                <a href="" class="text-muted">Business Information</a>--}}
{{--                            --}}{{----}}{{--                            </li>--}}
{{--                        </ul>--}}
{{--                        <!--end::Breadcrumb-->--}}
{{--                    </div>--}}
{{--                    <!--end::Page Heading-->--}}
{{--                </div>--}}
{{--                <!--end::Info-->--}}
{{--                <!--begin::Toolbar-->--}}
{{--            --}}{{--                <div class="d-flex align-items-center">--}}
{{--            --}}{{--                    <!--begin::Actions-->--}}
{{--            --}}{{--                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>--}}
{{--            --}}{{--                    <!--end::Actions-->--}}
{{--            --}}{{--                    <!--begin::Dropdown-->--}}
{{--            --}}{{--                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">--}}
{{--            --}}{{--                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--            --}}{{--											<span class="svg-icon svg-icon-success svg-icon-2x">--}}
{{--            --}}{{--												<!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->--}}
{{--            --}}{{--												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--            --}}{{--													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--            --}}{{--														<polygon points="0 0 24 0 24 24 0 24" />--}}
{{--            --}}{{--														<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--            --}}{{--														<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />--}}
{{--            --}}{{--													</g>--}}
{{--            --}}{{--												</svg>--}}
{{--            --}}{{--                                                <!--end::Svg Icon-->--}}
{{--            --}}{{--											</span>--}}
{{--            --}}{{--                        </a>--}}
{{--            --}}{{--                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">--}}
{{--            --}}{{--                            <!--begin::Navigation-->--}}
{{--            --}}{{--                            <ul class="navi navi-hover">--}}
{{--            --}}{{--                                <li class="navi-header font-weight-bold py-4">--}}
{{--            --}}{{--                                    <span class="font-size-lg">Choose Label:</span>--}}
{{--            --}}{{--                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>--}}
{{--            --}}{{--                                </li>--}}
{{--            --}}{{--                                <li class="navi-separator mb-3 opacity-70"></li>--}}
{{--            --}}{{--                                <li class="navi-item">--}}
{{--            --}}{{--                                    <a href="#" class="navi-link">--}}
{{--            --}}{{--														<span class="navi-text">--}}
{{--            --}}{{--															<span class="label label-xl label-inline label-light-success">Customer</span>--}}
{{--            --}}{{--														</span>--}}
{{--            --}}{{--                                    </a>--}}
{{--            --}}{{--                                </li>--}}
{{--            --}}{{--                                <li class="navi-item">--}}
{{--            --}}{{--                                    <a href="#" class="navi-link">--}}
{{--            --}}{{--														<span class="navi-text">--}}
{{--            --}}{{--															<span class="label label-xl label-inline label-light-danger">Partner</span>--}}
{{--            --}}{{--														</span>--}}
{{--            --}}{{--                                    </a>--}}
{{--            --}}{{--                                </li>--}}
{{--            --}}{{--                                <li class="navi-item">--}}
{{--            --}}{{--                                    <a href="#" class="navi-link">--}}
{{--            --}}{{--														<span class="navi-text">--}}
{{--            --}}{{--															<span class="label label-xl label-inline label-light-warning">Suplier</span>--}}
{{--            --}}{{--														</span>--}}
{{--            --}}{{--                                    </a>--}}
{{--            --}}{{--                                </li>--}}
{{--            --}}{{--                                <li class="navi-item">--}}
{{--            --}}{{--                                    <a href="#" class="navi-link">--}}
{{--            --}}{{--														<span class="navi-text">--}}
{{--            --}}{{--															<span class="label label-xl label-inline label-light-primary">Member</span>--}}
{{--            --}}{{--														</span>--}}
{{--            --}}{{--                                    </a>--}}
{{--            --}}{{--                                </li>--}}
{{--            --}}{{--                                <li class="navi-item">--}}
{{--            --}}{{--                                    <a href="#" class="navi-link">--}}
{{--            --}}{{--														<span class="navi-text">--}}
{{--            --}}{{--															<span class="label label-xl label-inline label-light-dark">Staff</span>--}}
{{--            --}}{{--														</span>--}}
{{--            --}}{{--                                    </a>--}}
{{--            --}}{{--                                </li>--}}
{{--            --}}{{--                                <li class="navi-separator mt-3 opacity-70"></li>--}}
{{--            --}}{{--                                <li class="navi-footer py-4">--}}
{{--            --}}{{--                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">--}}
{{--            --}}{{--                                        <i class="ki ki-plus icon-sm"></i>Add new</a>--}}
{{--            --}}{{--                                </li>--}}
{{--            --}}{{--                            </ul>--}}
{{--            --}}{{--                            <!--end::Navigation-->--}}
{{--            --}}{{--                        </div>--}}
{{--            --}}{{--                    </div>--}}
{{--            --}}{{--                    <!--end::Dropdown-->--}}
{{--            --}}{{--                </div>--}}
{{--            <!--end::Toolbar-->--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="content d-flex flex-column flex-column-fluid pt-0 pb-0" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Mobile Toggle-->
                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">
                        <span></span>
                    </button>
                    <!--end::Mobile Toggle-->
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Templates</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
{{--                            <li class="breadcrumb-item">--}}
{{--                                <a href="" class="text-muted">Title</a>--}}
{{--                            </li>--}}
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Profile</a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Profile 1</a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Business Information</a>--}}
                            {{--                            </li>--}}
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
{{--                <div class="d-flex align-items-center">--}}
{{--                    <!--begin::Actions-->--}}
{{--                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>--}}
{{--                    <!--end::Actions-->--}}
{{--                    <!--begin::Dropdown-->--}}
{{--                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">--}}
{{--                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                            <span class="svg-icon svg-icon-success svg-icon-2x">--}}
{{--                                <!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->--}}
{{--                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
{{--                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
{{--                                        <polygon points="0 0 24 0 24 24 0 24" />--}}
{{--                                        <path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
{{--                                        <path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />--}}
{{--                                    </g>--}}
{{--                                </svg>--}}
{{--                                <!--end::Svg Icon-->--}}
{{--                            </span>--}}
{{--                        </a>--}}
{{--                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">--}}
{{--                            <!--begin::Navigation-->--}}
{{--                            <ul class="navi navi-hover">--}}
{{--                                <li class="navi-header font-weight-bold py-4">--}}
{{--                                    <span class="font-size-lg">Choose Label:</span>--}}
{{--                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>--}}
{{--                                </li>--}}
{{--                                <li class="navi-separator mb-3 opacity-70"></li>--}}
{{--                                <li class="navi-item">--}}
{{--                                    <a href="#" class="navi-link">--}}
{{--                                        <span class="navi-text">--}}
{{--                                            <span class="label label-xl label-inline label-light-success">Customer</span>--}}
{{--                                        </span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li class="navi-item">--}}
{{--                                    <a href="#" class="navi-link">--}}
{{--														<span class="navi-text">--}}
{{--															<span class="label label-xl label-inline label-light-danger">Partner</span>--}}
{{--														</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li class="navi-item">--}}
{{--                                    <a href="#" class="navi-link">--}}
{{--														<span class="navi-text">--}}
{{--															<span class="label label-xl label-inline label-light-warning">Suplier</span>--}}
{{--														</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li class="navi-item">--}}
{{--                                    <a href="#" class="navi-link">--}}
{{--                                        <span class="navi-text">--}}
{{--                                            <span class="label label-xl label-inline label-light-primary">Member</span>--}}
{{--                                        </span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li class="navi-item">--}}
{{--                                    <a href="#" class="navi-link">--}}
{{--														<span class="navi-text">--}}
{{--															<span class="label label-xl label-inline label-light-dark">Staff</span>--}}
{{--														</span>--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li class="navi-separator mt-3 opacity-70"></li>--}}
{{--                                <li class="navi-footer py-4">--}}
{{--                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">--}}
{{--                                        <i class="ki ki-plus icon-sm"></i>Add new</a>--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <!--end::Navigation-->--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <!--end::Dropdown-->--}}
{{--                </div>--}}
                <!--end::Toolbar-->
            </div>
        </div>
    </div>
    <div class="container-lg bg-white rounded py-3">
        <div class="row">
            <div class="col-6 offset-6 py-2">
                <h2 class="float-right mt-2"><button class="btn btn-primary" data-toggle="modal" data-target="#addcar">Add Template</button>
               &nbsp<button class="btn btn-primary mt-1" onclick='getcats({{$cats}})'>View Categories</button></h2>
            </div>

            <div class="col-12 ">
                @if(Session::has('success'))
                    <p class="alert alert-success text-center">{{ Session::get('success') }}</p>
                @endif
            </div>
        </div>
            <div class="col-lg-12">
                <div class="p-2">
                    <table class="table text-center table-responsive">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Title</th>
                            <th>Dimensions</th>
                            <th>Category</th>
                            <th>Created at</th>
                            <th>Colours</th>
                            <th>Status</th>
                            <th>Image</th>
                            <th>Logo</th>
                        </tr>
                        @foreach($templates as $template)
                            <tr>
                                <td>{{$template->id}}</td>
                                <td>{{$template->name}}</td>
                                <td>{{$template->title}}</td>
                                <td>{{$template->width}} X {{$template->height}}</td>
                                <td>{{$template->typ->title}}</td>
                                <td>{{$template->created_at->format('M-d-Y')}}</td>
                                <td>
                            @if($template->color!=null)
                                    <a href="javascript:;" data-toggle="modal" data-target="#addcolor1" onclick="colors1({{$template}})" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="View colours">
                                            <i class="fa fa-edit"></i>
                                    </a>
                                <a href="javascript:;" data-toggle="modal" data-target="#showColours" onclick="colors({{$template->color}})" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="View colours">
                                            <i class="fa fa-eye"></i>
                                    </a>
                            @else
                                    <a href="javascript:;" data-toggle="modal" data-target="#addcolor" onclick="addColor({{$template->id}})" class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Add colors">
                                            <i class="fa fa-plus"></i>
                                    </a>
                                @endif
                                </td>
                                <td class="text-{{($template->status==1)?'':'danger'}}">{{($template->status==1)?'Active':'Deactive'}}</td>
                                <td><img src="{{asset('/images/campaign-templates/'.$template->url)}}" class="img-thumbnail" alt="" style="max-height: 100px;max-width:170px;cursor: pointer"  onclick="imgpreview('{{asset('/images/campaign-templates/'.$template->url)}}')" ></td>
                                <td>

                            @if($template->color!=null)
                                    <img src="{{asset('/images/campaign-templates/logo/'.$template->color->company_logo)}}" class="img-thumbnail" alt="" style="max-height: 100px;max-width:150px;"  ></td>
                                @endif
                            </tr>
                        @endforeach


                    </table>
                    <span class="text-right text-center float-right">{{$templates->links()}}  </span>
            </div>
                </div>
            <!--end::Content-->

    </div>

    <!-- Button trigger modal -->
    <!-- Modal -->
    <div class="modal fade "  id="addcar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if(old('colour')==null)
                        @if($errors->has('name') || $errors->has('template')|| $errors->has('contact_detail'))
                            @if(count($errors) > 0)
                                @foreach ($errors->all() as $error)
                                    <div class="col-10">
                                        <p class="alert alert-danger">{{ $error }}</p>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                    @endif
                    <form action="{{route('createTemplate')}}"  method="post" enctype="multipart/form-data" >
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Template Name</label>
                                <input type="text" class="form-control" name="name"  value="{{old('name')}}" required  placeholder="Name">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Title</label>
                                <input type="text" class="form-control" name="title"  value="{{old('title')}}" required  placeholder="Title">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Template Category* </label>
                                <select name="type" class=" form-control " required>
                                    <option selected value="" disabled>
                                        Choose type
                                    </option>
                                   @foreach($status as $stat)
                                        <option value="{{$stat->id}}" {{($stat->id==old('type')?'selected':'')}}>
                                            {{$stat->title}}
                                        </option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status* </label>
                                <select name="status" class=" form-control " required>
                                    <option selected value="" disabled>
                                        Choose status
                                    </option>
                                        <option value="1" {{('1'==old('status')?'selected':'')}}>Enable</option>
                                        <option value="0" {{('0'==old('status')?'selected':'')}}>Disable</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <label>Template Picture* </label>
                            <input type="file" class="form-control" name="template" required accept="image/*">
                            <div class="d-md-none mb-2"></div>
                        </div>
                        <div class="col-md-12 pt-3 mb-2">
                            {{--                                <input type="text" class="form-control" name="description" required >--}}
                            <textarea name="description"  required  cols="100" rows="3" class="form-control" placeholder="Description">{{old('description')}}</textarea>
                        </div>

                    </div>
                        <button type="submit" class="btn  btn-md float-right btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade "  id="addcolor" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Colour</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('createTemplate.color')}}"  method="post" enctype="multipart/form-data" >
                        @if(count($errors) > 0)
                            @foreach ($errors->all() as $error)
                                <div class="col-10">
                                    <p class="alert alert-danger">{{ $error }}</p>
                                </div>
                            @endforeach
                        @endif

                        @csrf
                        <input name="template_id" type="hidden" id="template_id" value="{{old('template_id')}}">
                        <input name="colour" type="hidden" value="{{old('1',1)}}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pick background colour:</label>
                                <input type="color"  class="form-control"  name="background_colour"   value="{{old('background_colour','#ff0000')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Pick font colour:</label>
                                <input type="color" class="form-control" name="font_colour" required  value="{{old('font_colour')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Contact detail:</label>
                                <input type="text" class="form-control" name="contact_detail" required  placeholder="Contact detail" value="{{old('contact_detail')}}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Side Text</label>
                                <input type="text" class="form-control" name="side_text" required  placeholder="Right Side Text" value="{{old('side_text')}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status* </label>
                                <select name="status" class=" form-control " required>
                                    <option selected value="" disabled>
                                        Status
                                    </option>
                                        <option value="1" {{(old('status')=='1')?'selected':''}}>Enable</option>
                                        <option value="0" {{(old('status')=='0')?'selected':''}} >Disable</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <label>Company logo* </label>
                            <input type="file" class="form-control" name="company_logo" required accept="image/*">
                            <div class="d-md-none mb-2"></div>
                        </div>
                    </div>
                        <button type="submit" class="btn  btn-md float-right btn-primary">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade"  id="addcolor1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Template</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('updateTemplate')}}" method="post" id='sbmit_form' enctype="multipart/form-data" >
                        @if(old('template_id')!=null  && old('colour')==null)
                            @if(count($errors) > 0)
                                @foreach ($errors->all() as $error)
                                    <div class="col-10">
                                        <p class="alert alert-danger">{{ $error }}</p>
                                    </div>
                                @endforeach
                            @endif
                        @endif
                        @csrf
                        <input name="template_id" type="hidden" id="template-id" value="{{old('template_id')}}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Template Name</label>
                                    <input type="text" id='template_name' class="form-control" name="name" required placeholder="Name" value="{{old('name')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Title</label>
                                    <input type="text" id="template_title" class="form-control" name="title" required placeholder="Title" value="{{old('title')}}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Template Category* </label>
                                    <select name="type" class=" form-control"  id="template_type" required>
                                        <option selected value="" disabled>
                                            Choose type
                                        </option>
                                        @foreach($status as $stat)
                                            <option value="{{$stat->id}}" {{(old('type')==$stat->id)?'selected':''}}>{{$stat->title}}
                                            </option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Status* </label>
                                    <select name="status" class=" form-control" id="template_status" required >
                                        <option selected value="" disabled>
                                            Choose status
                                        </option>
                                        <option value="1" {{(old('status')=='1')?'selected':''}}>Enable</option>
                                        <option value="0" {{(old('status')=='0')?'selected':''}}>Disable</option>
                                    </select>

                                </div>
                            </div>
                            <div class="col-md-6 ">
                                <label>Template Picture* </label>
                                <input type="file" class="form-control" name="template" accept="image/*" value="{{old('template')}}">
                                <div class="d-md-none mb-2"></div>
                            </div>
                            <div class="col-md-12 pt-3 mb-2">
                                <label for="exampleInputEmail1">Description</label>
                                <textarea name="description" required id="descriptien" cols="100" rows="3"  class="form-control" placeholder="Description">{{old('description')}}</textarea>
                            </div>

                        </div>


                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Background colour:</label>
                                <input type="color" required  class="form-control" id="background_colour" name="background_colour" value="{{old('background_colour')}}" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Font colour:</label>
                                <input type="color"  required class="form-control" name="font_colour" required id="font_colour" placeholder="Title" value="{{old('font_colour')}}" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Contact detail:</label>
                                <input type="text" class="form-control" name="contact_detail"  id="contact_detail" required  placeholder="Contact detail" value="{{old('contact_detail')}}" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Side Text</label>
                                <input type="text" class="form-control" name="side_text" id="side_text" required  placeholder="Right Side Text" value="{{old('side_text')}}" >
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status* </label>
                                <select name="status1" id="stats" class=" form-control" required>
                                    <option selected value="" disabled>
                                        Status
                                    </option>
                                        <option value="1">Enable</option>
                                        <option value="0">Disable</option>
                                </select>

                            </div>
                        </div>
                        <div class="col-md-6 ">
                            <label>Company logo* </label>
                            <input type="file" class="form-control" name="company_logo" accept="image/*">
                            <div class="d-md-none mb-2"></div>
                        </div>
                    </div>
                        <button onclick="sbmit()" class="btn  btn-md float-right btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade"  id="showColours" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Colors</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pt-0">
                    <div class="row">
                        <div class="col-md-12 ">
                            <br>
                            <table class="table " id="jqueryTabl">
                                <thead>
                                <th>#</th>
                                <th>Font Colour</th>
                                <th>Background color</th>
                                <th>Contact info</th>
                                <th>Side Text</th>
                                <th>Logo</th>
                                <th>Status</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade"  id="cats1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header pb-0">
                    <h5 class="modal-title">Template Categories</h5>
                    <div class="float-right">
                        <button class="btn btn-primary badge badge-pill mb-2" data-toggle="modal" data-target="#addcar1">Add Category</button>
                   </div>
                </div>
                <div class="modal-body pt-0">
                    <div class="row">
                        <div class="col-md-12 ">
                            <br>
                            <table class="table" id="jqueryTabl2">
                                <thead>
                                <th>#</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade morehide" id="addcar1" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Template Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @if($errors->has('title') && old('template_id')==null && old('cate_form')=='1')
                        @foreach ($errors->all() as $error)
                            <div class="col-10">
                                <p class="alert alert-danger">{{ $error }}</p>
                            </div>
                        @endforeach
                    @endif
                    <form action="{{route('stickerCreateType')}}"  method="post" >
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control" name="title" required placeholder="Title" value="{{old('title')}}">
                            <input type="hidden" name="cate_form" value="1">
                            {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status* </label>
                                <select name="type" class=" form-control " required>
                                    <option selected value="" disabled>
                                        Status
                                    </option>
                                    <option value="1" {{(old('type')=='1')?'selected':''}}>Enable</option>
                                    <option value="0"  {{(old('type')=='0')?'selected':''}}>Disable</option>
                                </select>

                            </div>
                        </div>
                    </div>
                        <button type="submit" class="btn w-100px btn-md float-right btn-primary mr-2">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade morehide" id="addcar5" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog  modal-lg modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Template Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <form action="{{route('stickerUpdateType')}}" method="post" >
                        @if($errors->has('title') && null!=old('cat_id') && old('template_id')==null)
                            @foreach ($errors->all() as $error)
                                <div class="col-10">
                                    <p class="alert alert-danger">{{ $error }}</p>
                                </div>
                            @endforeach
                        @endif
                        @csrf
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Title</label>
                            <input type="text" class="form-control cat-title" name="title" required placeholder="Title" value="{{old('title')}}">
                            <input type="hidden" name='cat_id' class="cat-id" value="{{old('cat_id')}}">
                            {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Status* </label>
                                <select name="type" class="  form-control " id='cat_type' required>
                                    <option selected value="" disabled>
                                        Status
                                    </option>
                                    <option value="1" {{(old('type')=='1')?'selected':''}}>Enable</option>
                                    <option value="0"  {{(old('type')=='0')?'selected':''}}>Disable</option>
                                </select>

                            </div>
                        </div>
                    </div>
                        <button type="submit" class="btn w-100px btn-md float-right btn-primary mr-2">Add</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="img-preview" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"  aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header" style="border: none;padding-bottom:0px;">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="col-lg-12">
                                        <img src="" class=" img-thumbnail temp1" width="100%" alt=""  height="100%" >
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
   @push('after-script')
        <script>
            @if(old('template_id')!=null  && old('colour')==null)
                @if(($errors->has('name') || $errors->has('template')|| $errors->has('title')|| $errors->has('contact_detail')|| $errors->has('font_colour')))
                    {{--alert({{old('template_id')}})--}}
                      $('#addcolor1').modal('show');
                  @endif
            @elseif($errors->has('name') || $errors->has('template')|| $errors->has('contact_detail') && old('colour')==null )
                  $('#addcar').modal('show');
                @elseif( old('colour')==1 && ($errors->has('contact_detail') || $errors->has('company_logo')) )
                   $('#addcolor').modal('show');
                    @endif
            @if($errors->has('title') && null!=old('cat_id') && old('template_id')==null)
                   $('#addcar5').modal('show');
            @elseif($errors->has('title') && old('template_id')==null && old('cate_form')=='1')
                 $('#addcar1').modal('show');
                @endif

        </script>
    @endpush
    <script>
        function colors1(data){
            // console.log(data.description);
            $('#template-id').val(data.id);
            $('#template_name').val(data.name);
            $('#template_title').val(data.title);
            $('#template_type').val(data.type).change();
            $('#template_status').val(data.status).change();
            $('#descriptien').val(data.description);
            $('#background_colour').val(data.color.background_colour);
            $('#font_colour').val(data.color.font_colour);
            $('#contact_detail').val(data.color.contact_detail);
            $('#stats').val(data.color.status).change();
            $('#side_text').val(data.color.side_text);
        }
        function sbmit(){
            $('#sbmit_form').submit();
            $('#loader').removeClass('d-none');
        }

        function addColor(id){
            $('#template_id').val(id);

        }
        function colors(data){
            $('#jqueryTabl tbody').html('');
            if (data==null) {
                $('#jqueryTabl tbody').append("<tr><td colspan='7' class='text-center'>Sorry. No record found</td></tr>");
            }else {
                var value=data;
                var colors = '';

                    var status='';
                    if(value.status==0){
                        status="Not Active";
                    }else{
                        status='Active';
                    }

                    var img = `<img width="150px" src="${globalPublicPath+"/images/campaign-templates/logo/"+value.company_logo}"/>`
                    colors += "<tr><td>" + value.id + "</td><td><input type='color' class='form-control' value='"+value.font_colour+ "'  disabled name='favcolor2'></td><td><input type='color' class='form-control' disabled value='"+value.background_colour+ "' name='favcolor'></td><td >" + value.contact_detail + "</td><td>" +value.side_text + "</td><td>" +img + "</td><td>" + status + "</td></tr>"

                $('#jqueryTabl tbody').append(colors);
            }
        }
        function imgpreview(img){
            $('.temp1').attr('src',img);
            $('#img-preview').modal();
        }
        function run(id,title,status){
            $('.cat-id').val(id);
            $('.cat-title').val(title);
            $( '#cat_type').find('option[value='+status+']').attr('selected','selected')
           // alert(status);
            $('#addcar5').modal();

        }
        function getcats(cats){
            var cat='';
            $('#jqueryTabl2 tbody').html('');
            if (cats.length==null) {
                $('#jqueryTabl2 tbody').append("<tr><td colspan='5' class='text-center'>Sorry. No record found</td></tr>");
            }else {
                $.each(cats, function (index, value) {
                    if(value.status==1){
                        status='Active';
                    }else{
                        status='Not Active';
                    }
                     cat +=`
                       <tr>
                    <td >${value.id}</td>
                    <td >${value.title}</td>
                    <td>${status}</td>
                    <td><a href='#' onclick="run('${value.id}','${value.title}','${value.status}')" >edit</a></td>
                    </tr>
                    `

                })

                $('#jqueryTabl2 tbody').append(cat);
            }

            $('#cats1').modal();
        }
        @if (\Session::has('success'))
        toastr.success('{!! \Session::get('success') !!}', 'Created successfull');
        @endif
        @if (\Session::has('delete'))
        toastr.error('{!! \Session::get('delete') !!}', 'Deleted successfull');
        @endif
    </script>
@endsection
