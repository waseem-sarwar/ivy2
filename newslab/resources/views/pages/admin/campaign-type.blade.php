{{--@extends('layout.admin')--}}
{{--@section('title','Campaign Type')--}}
{{--@section('content')--}}

{{--    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>--}}
{{--    <script>--}}
{{--        @if (\Session::has('success'))--}}
{{--        toastr.success('{!! \Session::get('success') !!}', 'Created successfull');--}}
{{--        @endif--}}
{{--        @if (\Session::has('delete'))--}}
{{--        toastr.error('{!! \Session::get('delete') !!}', 'Deleted successfull');--}}
{{--        @endif--}}
{{--    </script>--}}
{{--@endsection--}}
@extends('layout.admin')
@section('title','Campaign Type')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Mobile Toggle-->
                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">
                        <span></span>
                    </button>
                    <!--end::Mobile Toggle-->
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Campaign</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Campaign Types </a>
                            </li>
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Profile</a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Profile 1</a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Business Information</a>--}}
                            {{--                            </li>--}}
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
            {{--                <div class="d-flex align-items-center">--}}
            {{--                    <!--begin::Actions-->--}}
            {{--                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>--}}
            {{--                    <!--end::Actions-->--}}
            {{--                    <!--begin::Dropdown-->--}}
            {{--                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">--}}
            {{--                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
            {{--											<span class="svg-icon svg-icon-success svg-icon-2x">--}}
            {{--												<!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->--}}
            {{--												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">--}}
            {{--													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
            {{--														<polygon points="0 0 24 0 24 24 0 24" />--}}
            {{--														<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />--}}
            {{--														<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />--}}
            {{--													</g>--}}
            {{--												</svg>--}}
            {{--                                                <!--end::Svg Icon-->--}}
            {{--											</span>--}}
            {{--                        </a>--}}
            {{--                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">--}}
            {{--                            <!--begin::Navigation-->--}}
            {{--                            <ul class="navi navi-hover">--}}
            {{--                                <li class="navi-header font-weight-bold py-4">--}}
            {{--                                    <span class="font-size-lg">Choose Label:</span>--}}
            {{--                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-separator mb-3 opacity-70"></li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-success">Customer</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-danger">Partner</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-warning">Suplier</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-primary">Member</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-item">--}}
            {{--                                    <a href="#" class="navi-link">--}}
            {{--														<span class="navi-text">--}}
            {{--															<span class="label label-xl label-inline label-light-dark">Staff</span>--}}
            {{--														</span>--}}
            {{--                                    </a>--}}
            {{--                                </li>--}}
            {{--                                <li class="navi-separator mt-3 opacity-70"></li>--}}
            {{--                                <li class="navi-footer py-4">--}}
            {{--                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">--}}
            {{--                                        <i class="ki ki-plus icon-sm"></i>Add new</a>--}}
            {{--                                </li>--}}
            {{--                            </ul>--}}
            {{--                            <!--end::Navigation-->--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                    <!--end::Dropdown-->--}}
            {{--                </div>--}}
            <!--end::Toolbar-->
            </div>
        </div>
    </div>








    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Mobile Toggle-->
                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">
                        <span></span>
                    </button>
                    <!--end::Mobile Toggle-->
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Campaign</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a href="" class="text-muted">Campaign Types </a>
                            </li>
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Profile</a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Profile 1</a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="breadcrumb-item">--}}
                            {{--                                <a href="" class="text-muted">Business Information</a>--}}
                            {{--                            </li>--}}
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
                <!--end::Info-->
                <!--begin::Toolbar-->
                <div class="d-flex align-items-center">
                    <!--begin::Actions-->
                    <a href="#" class="btn btn-light-primary font-weight-bolder btn-sm">Actions</a>
                    <!--end::Actions-->
                    <!--begin::Dropdown-->
                    <div class="dropdown dropdown-inline" data-toggle="tooltip" title="Quick actions" data-placement="left">
                        <a href="#" class="btn btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="svg-icon svg-icon-success svg-icon-2x">
												<!--begin::Svg Icon | path:assets/media/svg/icons/Files/File-plus.svg-->
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<polygon points="0 0 24 0 24 24 0 24" />
														<path d="M5.85714286,2 L13.7364114,2 C14.0910962,2 14.4343066,2.12568431 14.7051108,2.35473959 L19.4686994,6.3839416 C19.8056532,6.66894833 20,7.08787823 20,7.52920201 L20,20.0833333 C20,21.8738751 19.9795521,22 18.1428571,22 L5.85714286,22 C4.02044787,22 4,21.8738751 4,20.0833333 L4,3.91666667 C4,2.12612489 4.02044787,2 5.85714286,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
														<path d="M11,14 L9,14 C8.44771525,14 8,13.5522847 8,13 C8,12.4477153 8.44771525,12 9,12 L11,12 L11,10 C11,9.44771525 11.4477153,9 12,9 C12.5522847,9 13,9.44771525 13,10 L13,12 L15,12 C15.5522847,12 16,12.4477153 16,13 C16,13.5522847 15.5522847,14 15,14 L13,14 L13,16 C13,16.5522847 12.5522847,17 12,17 C11.4477153,17 11,16.5522847 11,16 L11,14 Z" fill="#000000" />
													</g>
												</svg>
                                                <!--end::Svg Icon-->
											</span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right p-0 m-0">
                            <!--begin::Navigation-->
                            <ul class="navi navi-hover">
                                <li class="navi-header font-weight-bold py-4">
                                    <span class="font-size-lg">Choose Label:</span>
                                    <i class="flaticon2-information icon-md text-muted" data-toggle="tooltip" data-placement="right" title="Click to learn more..."></i>
                                </li>
                                <li class="navi-separator mb-3 opacity-70"></li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-success">Customer</span>
														</span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-danger">Partner</span>
														</span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-warning">Suplier</span>
														</span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-primary">Member</span>
														</span>
                                    </a>
                                </li>
                                <li class="navi-item">
                                    <a href="#" class="navi-link">
														<span class="navi-text">
															<span class="label label-xl label-inline label-light-dark">Staff</span>
														</span>
                                    </a>
                                </li>
                                <li class="navi-separator mt-3 opacity-70"></li>
                                <li class="navi-footer py-4">
                                    <a class="btn btn-clean font-weight-bold btn-sm" href="#">
                                        <i class="ki ki-plus icon-sm"></i>Add new</a>
                                </li>
                            </ul>
                            <!--end::Navigation-->
                        </div>
                    </div>
                    <!--end::Dropdown-->
                </div>
                <!--end::Toolbar-->
            </div>
        </div>
    </div>
    <div class="container-lg bg-white rounded py-3">
        <div class="row">

            <div class="col-2 offset-10 pt-1">
                <h2><button class="btn btn-primary float-right" data-toggle="modal" data-target="#addcar">ADD</button></h2>
            </div>
            <div class="col-10 offset-1">
                @if(Session::has('success'))
                    <p class="alert alert-success text-center">{{ Session::get('success') }}</p>
                @endif
                @if(Session::has('delete'))
                    <p class="alert alert-danger text-center">{{ Session::get('delete') }}</p>
                @endif
                <h2>Campaign Type</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="p-2">
                    <table class="table text-center">
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $count=1;
                        ?>
                        @foreach($types as $type)
                            <tr>
                                <td>{{$count}}</td>
                                <td>{{$type->name}}</td>
                                <td>{{$type->created_at}}</td>
                                <td>
                                    @if($type->status == 1)
                                        <a href="{{route('deleteType',[$type->id])}}" class="btn btn-outline-danger"  onclick="return confirm('Are you sure?')">Disable</a>
                                    @else
                                        <a href="{{route('deleteType',[$type->id])}}" class="btn btn-outline-success"  onclick="return confirm('Are you sure?')">Enable</a>
                                    @endif
                                </td>
                                @php
                                    $count++
                                @endphp
                            </tr>
                        @endforeach
                    </table>             </div>
            </div>
            <!--end::Content-->
        </div>
    </div>
    <!-- Button trigger modal -->
    <!-- Modal -->
    <div class="modal fade" id="addcar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Campaign Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('createType')}}"  method="post" >
                        @csrf
                        <div class="form-group">
                            <label for="">Campaign Type</label>
                            <input type="text" class="form-control" name="name" value="{{old('name')}}" required placeholder="Name" >
                            {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        </div>
                        <button type="submit" class="btn float-right btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @push('after-script')
        <script>

            @if (\Session::has('error'))
            $('#addcar').modal();
            toastr.error('{!! \Session::get('error') !!}');


            @endif

        </script>
    @endpush
    <script>
        @if (\Session::has('success'))
        toastr.success('{!! \Session::get('success') !!}', 'Created successfull');
        @endif
        @if (\Session::has('delete'))
        toastr.error('{!! \Session::get('delete') !!}', 'Deleted successfull');
        @endif


    </script>
@endsection
