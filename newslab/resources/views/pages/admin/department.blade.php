@extends('layout.admin')
@section('title',' Departments')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Subheader-->
        <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
            <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
                <!--begin::Info-->
                <div class="d-flex align-items-center flex-wrap mr-1">
                    <!--begin::Mobile Toggle-->
                    <button class="burger-icon burger-icon-left mr-4 d-inline-block d-lg-none" id="kt_subheader_mobile_toggle">
                        <span></span>
                    </button>
                    <!--end::Mobile Toggle-->
                    <!--begin::Page Heading-->
                    <div class="d-flex align-items-baseline flex-wrap mr-5">
                        <!--begin::Page Title-->
                        <h5 class="text-dark font-weight-bold my-1 mr-5">Departments</h5>
                        <!--end::Page Title-->
                        <!--begin::Breadcrumb-->
                        <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                            <li class="breadcrumb-item">
                                <a class="text-muted">Add Departments</a>
                            </li>
                        </ul>
                        <!--end::Breadcrumb-->
                    </div>
                    <!--end::Page Heading-->
                </div>
            </div>
        </div>

    </div>
    <div class="container-lg bg-white rounded py-3">
        <div class="row">
            <div class="col-10">
                @if(Session::has('success'))
                    <p class="alert alert-info">{{ Session::get('success') }}</p>
                @endif
                @if(Session::has('delete'))
                    <p class="alert alert-danger">{{ Session::get('delete') }}</p>
                @endif
                <h2>Add Department</h2>
            </div>
            <div class="col-2">
                <h2><button class="btn btn-primary float-right" data-toggle="modal" data-target="#addcar">ADD</button></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="p-2">
                    <table class="table text-center">

                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Created at</th>
                            <th>Action</th>
                        </tr>
                        <?php
                        $count=1;
                        ?>
                        @foreach($departments as $department)

                            <tr>
                                <td>{{$count}}</td>
                                <td>{{$department->name}}</td>
                                <td>{{$department->created_at}}</td>
                                <td>
                                    @if($department->status == 1)
                                        <a href="{{route('disable.department',[$department->id])}}" class="btn btn-outline-danger"  onclick="return confirm('Are you sure?')">Disable</a>
                                    @else
                                        <a href="{{route('enable.department',[$department->id])}}" class="btn btn-outline-success"  onclick="return confirm('Are you sure?')">Enable</a>
                                    @endif
                                </td>
                                @php
                                    $count++
                                @endphp
                            </tr>
                        @endforeach
                    </table>             </div>
            </div>

            <!--end::Content-->
        </div>

    </div>
    <!-- Button trigger modal -->

    <!-- Modal -->
    <div class="modal fade" id="addcar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Department</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('create.department')}}"  method="post" >
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Departments </label>
                            <input type="text" class="form-control" name="name" placeholder="Department Name" id="exampleInputEmail1" aria-describedby="emailHelp" required >
                            {{--<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>--}}
                        </div>
                        <button type="submit" class="btn float-right btn-primary">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>

        $('.kt_slider_a').ionRangeSlider({
            type: "double",
            grid: true,
            min:0,
            max:3000,
            from: 200,
            to: 400,
            step:1,
            prefix: "Miles",
            drag_interval: true,
        });

        @if (\Session::has('updated'))
        toastr.warning('{!! \Session::get('updated') !!}', 'Updated');
        @endif
        @if (\Session::has('created'))
        toastr.success('{!! \Session::get('created') !!}', 'Created Successfully');
        @endif
        @if (\Session::has('error'))
        toastr.error('{!! \Session::get('error') !!}', 'Error occurred');
        @endif
        @if (\Session::has('deleted'))
        toastr.info('{!! \Session::get('deleted') !!}', 'Successfully');
        @endif

        @if ($errors->any())
        $('#createModal').modal();
        @endif
    </script>
    @endsection
