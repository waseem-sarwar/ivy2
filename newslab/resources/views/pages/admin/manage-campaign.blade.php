@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout = 'layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout = 'layout.driver'; ?>
@elseif($role=='2')
    <?php $layout = 'layout.default'; ?>
@elseif($role=='1')
    <?php $layout = 'layout.admin'; ?>
@endif
@extends($layout)
@section('content')
    {{-- Content --}}
@section('content')

    <style>
        /* Important part */
        .modal-dialog {
            overflow-y: initial !important
        }

        .modal-body {
            max-height: calc(100vh - 200px);
            overflow-y: auto;
        }

        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }

        .fix-table-column-old2 {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 25px;
            background-color: #ffffff;
        }

        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }

        .fix-table-head {
            position: sticky;
            right: 0;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Campaigns
{{--                    <div class="text-muted pt-2 font-size-sm">Management of Campaign</div>--}}
                </h3>
            </div>
            <div class="card-toolbar">
                {{--                <select class="data-table-10list float-right">--}}
                {{--                    <option value="">10</option>--}}
                {{--                    <option value="">20</option>--}}
                {{--                </select>--}}
                <a class="btn btn-light-primary mr-2" href="javascript:;" id="toggleSearchForm">
                     <span class="svg-icon svg-icon-md">
                        <!--begin::Svg Icon | path:assets/media/svg/icons/Design/PenAndRuller.svg-->
                             <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px"
                                  viewBox="0 0 24 24"
                                  version="1.1">
                                 <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                     <rect x="0" y="0" width="24" height="24"/>
                                     <path
                                         d="M3,16 L5,16 C5.55228475,16 6,15.5522847 6,15 C6,14.4477153 5.55228475,14 5,14 L3,14 L3,12 L5,12 C5.55228475,12 6,11.5522847 6,11 C6,10.4477153 5.55228475,10 5,10 L3,10 L3,8 L5,8 C5.55228475,8 6,7.55228475 6,7 C6,6.44771525 5.55228475,6 5,6 L3,6 L3,4 C3,3.44771525 3.44771525,3 4,3 L10,3 C10.5522847,3 11,3.44771525 11,4 L11,19 C11,19.5522847 10.5522847,20 10,20 L4,20 C3.44771525,20 3,19.5522847 3,19 L3,16 Z"
                                         fill="#000000" opacity="0.3"/>
                                     <path
                                         d="M16,3 L19,3 C20.1045695,3 21,3.8954305 21,5 L21,15.2485298 C21,15.7329761 20.8241635,16.200956 20.5051534,16.565539 L17.8762883,19.5699562 C17.6944473,19.7777745 17.378566,19.7988332 17.1707477,19.6169922 C17.1540423,19.602375 17.1383289,19.5866616 17.1237117,19.5699562 L14.4948466,16.565539 C14.1758365,16.200956 14,15.7329761 14,15.2485298 L14,5 C14,3.8954305 14.8954305,3 16,3 Z"
                                         fill="#000000"/>
                                 </g>
                             </svg>
                         <!--end::Svg Icon-->
                         </span>
                    Search
                </a>
                <a href="{!! route('campaigns.export')."?status=".@$_GET['campaign_status'] !!}"  class="btn btn-primary font-weight-bolder">
                    <span class="fa fa-upload">&nbsp
                </span>Export</a>
            </div>
        </div>

        <div class="card-body">

            <!--begin::Search Form-->
            <div class="mt-2 mb-5 mt-lg-5 mb-lg-10" id="table-search-form"
                 style="display: <?php  echo ($check == '1') ? 'block' : 'none'; ?>">

                {{--<option   <?php  echo (null!=($request->meal_type_id)&&($meal_type->id ==$request->meal_type_id))?'selected':''; ?> value="{{$meal_type->id}}">{{$meal_type->name}}</option>--}}
                <form action="#" id="frm">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-xl-12  ">
                            <div class="row align-items-center">
                                <div class="col-md-3 my-2 my-md-0  offset-2">
                                    <div class="input-icon">
                                        <input type="text" name="search" class="form-control" placeholder="Search..."
                                               value="{{(isset($_GET['search']))?$_GET['search']:''}}"
                                               id="kt_datatable_search_query"/>
                                        <span><i class="flaticon2-search-1 text-muted"></i></span>
                                    </div>
                                </div>

                                <div class="col-md-3 my-2 my-md-0 ">
                                    <div class="d-flex align-items-center">
                                        <label class=" mb-0 d-none d-md-block">Campaign Status:</label>&nbsp
                                        <select class="form-control" name='campaign_status'
                                                id="kt_datatable_search_status">
                                            {{--                                        <option selected disabled>Choose Campaign status</option>--}}
                                            @foreach($status as $stat)
                                                @if($stat->id!='1')
                                                    <option
                                                        value="{{$stat->id}}" {{(isset($_GET['campaign_status'])&& $stat->id==$_GET['campaign_status'])?'selected':''}} >{{$stat->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                {{--                            <div class="col-md-2 my-2 my-md-0">--}}
                                {{--                                <div class="d-flex align-items-center">--}}
                                {{--                                    <label class=" mb-0 d-none d-md-block">Payment Status:</label>&nbsp--}}
                                {{--                                    <select class="form-control"  name='payment_status' id="kt_datatable_search_type">--}}
                                {{--                                        <option>Any</option>--}}
                                {{--                                        <option value="1">Paid</option>--}}
                                {{--                                        <option value="6">pending</option>--}}
                                {{--                                        --}}{{--                                        <option value="3">Direct</option>--}}
                                {{--                                    </select>--}}
                                {{--                                </div>--}}
                                {{--                            </div>--}}
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" onclick="document.getElementById('frm').submit();"
                                       class="btn btn-light-primary px-6 font-weight-bold">
                                        Search
                                    </a>
                                    @if(isset($_GET['campaign_status']) || isset($_GET['search']))
                                        <a href="#" onClick="window.location.href='../admin/campaigns'"
                                           style="text-decoration: underline; padding-left:5px;">
                                            reset
                                        </a>
                                    @endif

                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>

            <!-- HTML Table Begin Here -->
            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase border-bottom">
                            <th style="min-width: 100px">Id</th>
                            <th>Advertisor</th>
                            <th style="max-width: 80px" >
                                Sticker
                            </th>
                            <th style="min-width: 200px">
                                <span >Title</span>
                            </th>
                            <th>Location</th>
                            <th style="max: 130px;text-align:center">Zip</th>
                            <th style="min-width: 100px">Amount</th>
                            <th >Drivers cost</th>
                            <th>Refunded Amount</th>
                            <th >Drivers required</th>
                            <th >Completed drivers</th>
                            <th style="min-width: 150px">Campaign Status</th>
                            <th style="min-width: 180px">Payment Status</th>
                            <th>Days</th>
                            <th style="min-width: 130px">Duration</th>
                            <th >Created</th>
                            <th ></th>

                        </tr>
                        </thead>
                        <tbody>

                        @if(count($campaigns)==0)
                            <tr class="text-center">
                                <td colspan="13"><h4>Sorry, No campaign exist </h4></td>
                            </tr>
                        @endif
                        @foreach($campaigns as $campaign)
                            <tr class="{{($campaign->sticker->sticker_id=='0')?'bg-light':''}}">
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                        CMP-{{$campaign->id}}
                                    </span>
                                    {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                                </td>
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                        {{$campaign->user->name}}
                                    </span>
                                    {{--                                    <span class="text-muted font-weight-bold">Paid</span>--}}
                                </td>
                                <td class="pl-0 ">
                                    <div class=" align-items-center">
                                        <div class="symbol-light ">
                                            <span class="symbol-label">
                                         @if(@$campaign->sticker->image_url!=null)
                                                    @php if(@$campaign->sticker->image_url!=null){
                                                        $url='/images/campaign-sticker/'.$campaign->sticker->image_url;
                                                        }else{
                                                          $url='/media/uploadimge.png';
                                                        } @endphp
                                                    @if($url)
                                                        <img width="100%" height="100%" src="{{asset($url)}}" class=" align-self-end" />
                                                    @endif
                                                @endif
                                            </span>
                                        </div>
                                        <div>

                                            {{--                                            <span class="text-muted font-weight-bold d-block">more infor sample </span>--}}
                                        </div>
                                    </div>
                                </td>
                                <td class="pl-0 py-8">
                                        <div>
                                            <a href="{{route('camp.detail',$campaign->slug)}}"
                                               class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$campaign->campaign->title}}</a>
                                            {{--                                            <span class="text-muted font-weight-bold d-block">more infor sample </span>--}}
                                        </div>
                                </td>



                                  <td class="text-center">
                                    <span class="font-weight-bolder " >{{@$campaign->state->state}} {{@$campaign->city->primary_city}} </span>
                                </td>
                                <td class="text-center">
                                    <span class="font-weight-bolder" >{{@$campaign->zip}} </span>
                                </td>
                                <td>
                                    <span class="text-dark-75 text-center font-weight-bolder d-block font-size-lg">${{$campaign->total_price}}</span>
                                </td>
                                <td>
                                    <span class="text-dark-75 text-center font-weight-bolder d-block font-size-lg">${{$campaign->drivers_cost->sum('amount')}}</span>
                                </td>
                                <td>
                                    <span class="text-dark-75 text-center font-weight-bolder d-block font-size-lg">${{$campaign->refund_amount->sum('amount')}}</span>
                                </td>
                                <td>
                                    <span class="text-center text-dark-75 font-weight-bolder d-block font-size-lg">{{$campaign->no_of_cars}}</span>
                                </td>  <td>
                                    <span class="text-center text-dark-75 font-weight-bolder d-block font-size-lg">{{$campaign->completed_drivers}}</span>
                                </td>

                                <td>
                                    <span style="min-width: 150px"
                                          class="label label-lg label-light-{{@$campaign->status->class}} label-inline">{{@$campaign->status->name}}</span>
                                </td>
                                <td class="text-center">
                                    @if($campaign->status_id==3)
                                        <span class="label label-lg w-100 label-light-success label-inline">{{$campaign->payment_status}}
                                            @else
                                            <span class="label label-lg  w-100 label-light-success label-inline">Pending</span>
                                    </span>
                                    @endif
                                </td>
                                <td class="text-center">
                                    <span class="font-weight-bolder " >{{@$campaign->campaign->campaign_total_days}}</span>
                                </td>

                                <td>
                                    @php  $interval=explode("-",$campaign->campaign->interval)@endphp
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                        {{ Carbon\Carbon::parse($interval[0])->format('M d ') }} -{{ Carbon\Carbon::parse($interval[1])->format('M d') }}</span>
                                    <span class="text-muted font-weight-bold"></span>
                                </td>
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                     {{ Carbon\Carbon::parse($campaign->created_at)->format('d/M/Y') }}</span>
                                    <span class="text-muted font-weight-bold"> </span>
                                </td>


                                <td class="pr-0">

                                    @if($campaign->sticker->sticker_id=='0')
                                    @php $date=(is_null($campaign->sticker_payment) && !is_null($campaign->sticker->bespoke_status))?" | Amount :".@$campaign->sticker->bespoke_price:" | Payment date:".@$campaign->sticker_payment->created_at @endphp
                                            <span class="example-tools justify-content-center"
                                                  <?php if(is_null($campaign->sticker->bespoke_status)){?> onclick="bespoke({{$campaign->sticker}},{{$campaign->campaign_id}},{{$campaign->user_id}})" <?php }?>>
                                                <a class="btn btn-sm btn-default  btn-icon "
                                                   data-toggle="tooltip"
                                                   data-original-title="{{($campaign->sticker->sticker_id=='0' && !is_null($campaign->sticker->bespoke_status))?'Status: '.$campaign->sticker->bespoke_status . ' | Sticker delivered date: '.$campaign->sticker->bespoke_days .$date:"Be Spoke"}}"
                                                   style="position: relative; {{($campaign->appliedDrivers->count()>0)?"":"cursor: pointer"}}">
                                               <i class="fas fa-pause-circle {{($campaign->sticker->bespoke_status=='Paid')?'text-success':'text-primary'}}"></i></a>
                                          </span>
                                   @endif






                                    @if($campaign->status_id!='3' && $campaign->status_id!='4')
                                        <a href="javascript:;" data-toggle="modal" data-target="#statusUpdate"
                                           onclick="status('{{$campaign->status_id}}','{{$campaign->id}}','{{$campaign->user_id}}')"
                                           class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
                                           title="Change status">
                                            <i class="la la-wrench"></i>
                                        </a>
                                    @endif


                                    @if($campaign->status_id=='3' && $campaign->payment_status=='Paid')
                                        <span class="example-tools justify-content-center"
                                           <?php if( @$campaign->appliedDrivers->count() > 0){?> onclick="appliedDrivers({{$campaign->id}},{{$campaign->appliedDrivers}}, {{$campaign}},);" <?php }?>>
                                            <a class="btn btn-sm btn-default btn-text-primary {{($campaign->appliedDrivers->count()>0)?'btn-hover-warning':''}} btn-icon "
                                               data-toggle="tooltip"
                                               data-original-title="{{($campaign->appliedDrivers->count()>0)?$campaign->appliedDrivers->count() ." drivers applied":"No driver applied"}}"
                                               style="position: relative; {{($campaign->appliedDrivers->count()>0)?"":"cursor: default"}}">
                                                 <span
                                                     class="logged-in {{($campaign->appliedDrivers->count()>0)?'text-success':'text-danger'}}"
                                                     style="position: absolute;top: -6px; right: -2px;">●</span>
                                           <i class="la la-truck"></i></a>
                                       </span>
                                    @endif

                                        @if($campaign->status_id=='3' && $campaign->payment_status=='Pending')
                                        <a href="javascript:;" data-toggle="modal" data-target="#statusUpdate"
                                           onclick="status('{{$campaign->status_id}}','{{$campaign->id}}','{{$campaign->user_id}}')"
                                           class=" btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
                                           title="Change status">
                                            <i class="la la-wrench"></i>
                                        </a>
                                    @endif

                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <nav aria-label="Page navigation example ">
                    <ul class="pagination float-right mt-5">
                        <h6 class="pt-3">Total records: {{$campaigns->total()}}  </h6>  &nbsp &nbsp&nbsp  {{$campaigns->appends(['search' => @$_GET['search'],'campaign_status' => @$_GET['campaign_status']])->links()}}

                    </ul>
                </nav>
                <!--end::Table-->
                <!-- HTML Table End Here -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Title</h5>
                    <button class="btn btn-light-primary font-weight-bold text-right" style="
    margin-left: 267px;
">
                        <i class="fa fa-download" aria-hidden="true"></i>download
                    </button>
                    <button class="btn btn-light-primary font-weight-bold" style="
    margin-left: -68px;
"><i class="fa fa-print" aria-hidden="true"></i>print
                    </button>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">
                    <div class="container">
                        <div class="card card-custom">
                            <div class="card-body p-0">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">
                                    <div class="col-md-9">
                                        <!-- begin: Invoice header-->
                                        <div
                                            class="d-flex justify-content-between pb-5 pb-md-10 flex-column flex-md-row">
                                            <h1 class="display-4 font-weight-boldest mb-10">Sample Campaign Title</h1>
                                            {{--                                            <span>Duration : 20m </span>--}}

                                            <div class="d-flex flex-column align-items-md-end px-0">
                                                <!--begin::Logo-->
                                                <a href="#" class="mb-5 max-w-200px">
															<span class="svg-icon svg-icon-full">
																<!--begin::Svg Icon | path:assets/media/svg/logos/duolingo.svg-->
																<svg xmlns="http://www.w3.org/2000/svg" width="113"
                                                                     height="31" viewBox="0 0 113 31" fill="none">
																	<path
                                                                        d="M26.6802 13.33V13.485C26.6802 14.9575 26.6087 15.8875 26.4659 16.275C26.2517 16.895 25.966 17.36 25.4661 17.67C25.0376 17.98 24.5377 18.1358 23.8949 18.1358C23.2529 18.1358 22.7523 17.9808 22.3245 17.67C21.8953 17.3608 21.6103 16.895 21.3954 16.275C21.1811 15.7325 21.1104 14.8025 21.1104 13.4075V5.425H17.3254V13.485C17.3968 15.345 17.5396 16.6625 17.7539 17.515C18.1824 18.9108 18.8965 19.995 19.8963 20.8483C20.8962 21.6233 22.2531 22.0108 23.8242 22.0108C25.324 22.0108 26.538 21.7008 27.5379 21.0025C28.537 20.3058 29.2511 19.375 29.7518 18.0575C30.1088 17.205 30.3231 15.655 30.3231 13.485V5.425H26.6087V13.33H26.6802ZM15.6107 0H11.897V6.2775C11.8256 6.2775 11.7542 6.2 11.6828 6.2C10.4687 5.425 9.11182 5.0375 7.75492 5.0375C5.82669 5.0375 4.1127 5.735 2.61297 7.2075C0.898984 8.9125 0.0419922 11.005 0.0419922 13.485C0.0419922 15.81 0.827568 17.825 2.3273 19.4525C3.82704 21.0808 5.68314 21.9325 7.75492 21.9325C9.18324 21.9325 10.5401 21.545 11.7542 20.8483C12.9683 20.0733 13.8967 19.065 14.6109 17.7483C15.2536 16.5858 15.54 15.345 15.6107 14.0283V13.4858V0ZM10.6823 16.895C9.89668 17.7483 8.96899 18.2125 7.7542 18.2125C6.61155 18.2125 5.61172 17.7475 4.89756 16.895C4.11199 16.0425 3.75491 14.88 3.75491 13.485C3.75491 12.1675 4.11199 11.005 4.89756 10.1525C5.68314 9.3 6.61155 8.835 7.7542 8.835C8.96828 8.835 9.89668 9.3 10.6823 10.1525C11.4685 11.005 11.8256 12.1675 11.8256 13.485C11.8256 14.88 11.4685 16.0425 10.6823 16.895ZM46.2481 9.3C45.534 7.9825 44.6049 6.975 43.3915 6.2C42.1774 5.425 40.8205 5.0375 39.4636 5.0375C37.5354 5.0375 35.8207 5.735 34.3217 7.2075C32.6077 8.9125 31.75 10.9275 31.75 13.485C31.75 15.81 32.5363 17.825 34.0353 19.4525C35.5357 21.0808 37.3918 21.9325 39.4636 21.9325C40.8919 21.9325 42.2481 21.545 43.4629 20.8483C44.677 20.0733 45.6054 19.065 46.3195 17.7483C47.0337 16.4308 47.3908 15.0358 47.3908 13.4858C47.248 12.0125 46.9623 10.6175 46.2481 9.3ZM42.3917 16.895C41.6061 17.7483 40.6777 18.2125 39.4636 18.2125C38.2495 18.2125 37.3211 17.7475 36.607 16.895C35.8214 16.0425 35.4643 14.88 35.4643 13.485C35.4643 12.1675 35.8214 11.005 36.6077 10.1525C37.3933 9.3 38.321 8.835 39.4643 8.835C40.6777 8.835 41.6068 9.3 42.3924 10.1525C43.178 11.005 43.535 12.1675 43.535 13.485C43.5336 14.88 43.1058 16.0425 42.3917 16.895ZM60.2457 21.7H64.0307V6.1225H60.2457V21.7ZM53.8182 17.515C53.3897 17.2058 53.0326 16.74 52.8898 16.12C52.747 15.655 52.6756 14.725 52.6756 13.33V0H48.9619V13.2525C49.0334 15.4225 49.1762 16.895 49.5333 17.825C50.0332 19.065 50.7473 20.0725 51.7472 20.77C52.747 21.4675 53.9618 21.7775 55.4608 21.7775H58.3174V17.98H55.3894C54.7466 17.98 54.246 17.825 53.8182 17.515ZM78.7424 9.145C78.2425 7.905 77.5283 6.8975 76.5278 6.2C75.5287 5.5025 74.3139 5.1925 72.8142 5.1925C71.1716 5.1925 69.8861 5.58 68.8863 6.355C67.8872 7.13 67.173 8.215 66.7438 9.6875C66.5296 10.4625 66.3867 11.8575 66.3153 13.7175V21.7H70.1003V13.95V13.64C70.1003 12.3225 70.1725 11.315 70.386 10.7725C70.6003 10.1525 70.8859 9.765 71.3144 9.3775C71.7429 9.0675 72.2428 8.9125 72.8856 8.9125C73.5276 8.9125 74.0282 9.0675 74.4567 9.3775C74.8852 9.6875 75.2423 10.1525 75.4558 10.7725C75.5987 11.2375 75.6701 12.1675 75.6701 13.5625V21.7H79.3837V14.88V13.64C79.3137 11.5475 79.0995 9.9975 78.7424 9.145ZM62.0311 0.31C60.9598 0.31 60.0314 1.3175 60.0314 2.48C60.0314 3.6425 60.8884 4.65 62.0311 4.65C63.1023 4.65 64.0307 3.72 64.0307 2.48C64.0307 1.24 63.1023 0.31 62.0311 0.31ZM111.951 9.455C111.237 8.1375 110.308 7.13 109.094 6.355C107.88 5.58 106.595 5.27 105.094 5.27C102.952 5.27 101.166 6.1225 99.6666 7.75C98.1661 9.3775 97.3813 11.3925 97.3813 13.7175C97.3813 16.1975 98.2383 18.2892 99.9522 19.995C101.453 21.4675 103.166 22.165 105.094 22.165C106.522 22.165 107.808 21.7775 109.022 21.0025C110.236 20.2275 111.237 19.22 111.879 17.9025C112.593 16.5858 112.95 15.19 112.95 13.7183C113.022 12.1675 112.665 10.7725 111.951 9.455ZM109.236 13.7175C109.236 15.1125 108.879 16.1975 108.094 17.05C107.308 17.9025 106.38 18.3675 105.166 18.3675C104.022 18.3675 103.023 17.9017 102.309 17.05C101.523 16.1975 101.166 15.035 101.166 13.7175C101.166 12.3225 101.523 11.16 102.309 10.3075C103.095 9.455 104.022 8.99 105.166 8.99C106.308 8.99 107.308 9.455 108.094 10.3075C108.879 11.16 109.236 12.3225 109.236 13.7175C109.236 13.64 109.236 13.64 109.236 13.7175ZM96.0965 13.02C96.0244 11.7025 95.6673 10.54 95.0967 9.3775C94.3825 8.06 93.4541 7.0525 92.3122 6.2775C91.0981 5.5025 89.8126 5.1925 88.3843 5.1925C86.2418 5.1925 84.4564 5.9675 82.9559 7.6725C81.4562 9.3 80.7421 11.315 80.7421 13.64C80.7421 16.12 81.599 18.2125 83.2416 19.84C84.7413 21.3125 86.3832 22.0108 88.3121 22.0108C89.669 22.0108 91.026 21.6233 92.24 20.8483C92.3122 20.77 92.3829 20.77 92.4543 20.6933V22.5525C92.4543 23.9483 92.0258 25.0325 91.3116 25.8858C90.526 26.7383 89.5976 27.2025 88.455 27.2025C87.3123 27.2025 86.3839 26.815 85.5983 25.9625L83.1702 28.7525L83.2416 28.83C84.7413 30.3017 86.4553 31 88.3121 31C89.669 31 91.026 30.6125 92.24 29.8375C93.4541 29.0625 94.3825 28.055 95.0967 26.7375C95.8108 25.42 96.0965 24.025 96.0965 22.5525V14.105V13.5625C96.1679 13.4075 96.1679 13.175 96.0965 13.02ZM92.4536 13.5625C92.4536 14.9575 92.0965 16.0425 91.3109 16.895C90.5253 17.7483 89.5969 18.2125 88.4543 18.2125C87.3116 18.2125 86.3832 17.7475 85.5976 16.895C84.812 16.0425 84.455 14.9575 84.455 13.5625C84.455 12.1675 84.812 11.0825 85.5976 10.1525C86.3832 9.3 87.3116 8.835 88.4543 8.835C89.5969 8.835 90.5967 9.3 91.3109 10.1525C92.0258 11.0825 92.4536 12.1675 92.4536 13.5625Z"
                                                                        fill="#1CB0F6"></path>
																</svg>
                                                                <!--end::Svg Icon-->
															</span>
                                                </a>
                                                <!--end::Logo-->
                                                <span
                                                    class="d-flex flex-column text-center align-items-md-end font-size-h6 font-weight-bold text-muted">
{{--															<span class="text-center">Start Campaign  </span>--}}
                                                    {{--															<span>12 May 2020  </span>--}}

                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="d-flex  font-size-lg mb-3">
                                                <span class="font-weight-bold mr-12">Campaign Status:</span>
                                                <span class="text-right ">
                                                    	<span class="navi-icon mr-2">
																			<i class="fa fa-genderless text-primary font-size-h2"></i>
                                                            																		</span>
                                                    In review
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="d-flex  font-size-lg mb-3">
                                                <span class="font-weight-bold mr-15">Payment Status:   </span>
                                                <span class="text-right ">
                                                    	<span class="navi-icon mr-2"><i
                                                                class="fa fa-genderless text-success font-size-h2"></i>
                                                 	</span>
                                                   Paid
                                                </span>
                                            </div>
                                        </div>


                                        <!--end: Invoice header-->
                                        <!--begin: Invoice body-->
                                        <div class="row border-bottom pb-10">
                                            <div class="col-md-7 py-md-10 pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Location
                                                            </th>

                                                            <th class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Zipcode
                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Cost
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">
                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-center">
																		<span class="navi-icon mr-2">
																			<i class="fa fa-genderless text-danger font-size-h2"></i>
																		</span>England London
                                                            </td>

                                                            <td class="text-right pt-7">564</td>
                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">
                                                                $3200.00
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                <div class="d-flex flex-column flex-md-row">
                                                    <div class="d-flex flex-column mb-10 mb-md-0">
                                                        <div
                                                            class=" font-size-h6 mb-3 pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                            Vehicle Info
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-3">
                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>
                                                            <span class="text-right">Car</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-3">
                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>
                                                            <span class="text-right">Honda</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg">
                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>
                                                            <span class="text-right">2020</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 border-left-md pl-md-10 py-md-10">
                                                <!--begin::Total Amount-->
                                                <!--begin::Total Amount-->
                                                <div class="font-size-h6 font-weight-bolder text-muted mb-3">Grand
                                                    Amount
                                                </div>
                                                <div class="font-size-h6 font-weight-boldest">$20,60</div>
                                            {{--                                                <div class="text-muted font-weight-bold mb-16">Taxes included</div>--}}
                                            <!--end::Total Amount-->
                                                <div class="border-bottom w-100 mb-16"></div>
                                                <!--begin::Invoice To-->
                                            {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Campaign Status--}}
                                            {{----}}
                                            {{--                                                </div>--}}
                                            {{--                                                <div class="font-size-lg font-weight-bold mb-10">Iris Watson.--}}
                                            {{--                                                    <br>In review--}}
                                            {{--                                                </div>--}}
                                            <!--end::Invoice To-->
                                                <!--begin::Invoice No-->
                                            {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Payment Status--}}
                                            {{--                                                </div>--}}
                                            {{--                                                <div class="font-size-lg font-weight-bold mb-10">Pending</div>--}}
                                            <!--end::Invoice No-->
                                                <!--begin::Invoice Date-->
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Start Date
                                                    12 May, 2020
                                                </div>
                                                <div class="font-size-lg font-weight-bold"></div>
                                                <!--end::Invoice Date--><!--begin::Invoice Date-->
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">End Date 18
                                                    May, 2020
                                                </div>
                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Duration 7
                                                    Days
                                                </div>
                                                <div class="font-size-lg font-weight-bold"></div>
                                                <!--end::Invoice Date--><!--begin::Invoice Date-->
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <div class="d-flex flex-column flex-md-row">
                                                    <div class="d-flex flex-column mb-10 mb-md-0">
                                                        {{--                                                        <div class=" font-size-h6 mb-3 pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                                        {{--                                                            Vehicle Info--}}
                                                        {{--                                                        </div>--}}
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span class="font-weight-bold mr-15">Number of Cars:</span>
                                                            <span class="text-right">12</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span
                                                                class="font-weight-bold mr-15">How often miles drived:</span>
                                                            <span class="text-right">12</span>
                                                        </div>
                                                        <div class="d-flex justify-content-between font-size-lg mb-5">
                                                            <span
                                                                class="font-weight-bold mr-1">How often drives a car:</span>
                                                            <span class="text-right">Weekly</span>
                                                        </div>
                                                        {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                        {{--                                                            <span class="font-weight-bold mr-15">How often miles drived:</span>--}}
                                                        {{--                                                            <span class="text-right">34</span>--}}
                                                        {{--                                                        </div>--}}
                                                        {{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                                        {{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                                        {{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                                        {{--                                                        </div>--}}
                                                    </div>
                                                </div>
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Number of Cars : 3 </div>--}}
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">How often miles drived: 20 </div>--}}
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">How often drives a car: Weekly </div>--}}
                                                {{--                                                <div class="font-size-lg font-weight-bold"></div>--}}
                                                {{--                                                <!--end::Invoice Date-->--}}
                                            </div>
                                        </div>
                                        <!--end: Invoice body-->
                                        <div class="row border-bottom pb-1">
                                            <div class="col-md-6  pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                My Sticker Info
                                                            </th>

                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">

                                                            {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                                            {{--																		<span class="navi-icon mr-2">--}}
                                                            {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                            {{--																		</span>in progress--}}
                                                            {{--                                                            </td>--}}

                                                            {{--                                                            <td class="text-right pt-7">564</td>--}}
                                                            {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                                            {{--                                                                $3200.00--}}
                                                            {{--                                                            </td>--}}
                                                        </tr>

                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                                        {{--																		</span>Front-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $4800.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        {{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                                        {{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                                        {{--																		<span class="navi-icon mr-2">--}}
                                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                                        {{--																		</span>Back-End Development--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                                        {{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                                        {{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                                        {{--                                                                $12600.00--}}
                                                        {{--                                                            </td>--}}
                                                        {{--                                                        </tr>--}}
                                                        </tbody>
                                                    </table>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Company:</span>
                                                        <span class="text-right">20thFloor Technologies</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15"> Slogan</span>
                                                        <span class="text-right">Technologies</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Contact</span>
                                                        <span class="text-right">+166855869</span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>
                                                        <span class="text-right"></span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        {{--                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>--}}
                                                        <span class="text-left">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, </span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-5">
                                                        <span class="font-weight-bold mr-15">Logo</span>
                                                        <span class="text-right"></span>
                                                    </div>
                                                    {{--                                                    <div class="d-flex justify-content-between font-size-lg mb-5">--}}
                                                    {{--                                                        --}}{{--                                                        <span class="font-weight-bold mr-15">Sticker Detail</span>--}}
                                                    {{--                                                        <span class="text-left">--}}
                                                    {{--                                                            <img src="{{ url('/media/cinqueterre.jpg') }}" class=" img-rounded" alt="Cinque Terre" width="150px" height="150px" style="--}}
                                                    {{--   border-radius: 20px;--}}
                                                    {{--">--}}
                                                    {{--                                                        </span>--}}
                                                    {{--                                                    </div>--}}
                                                </div>
                                                <div class="border-bottom w-100 mt-7 mb-13"></div>
                                                <div class="d-flex flex-column flex-md-row">

                                                </div>
                                            </div>
                                            <div class="col-md-6  pr-md-10">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                        <tr>
                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                                Preview
                                                            </th>

                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">

                                                            </th>
                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr class="font-weight-bolder font-size-lg">

                                                            {{--                                                            <td class="text-right pt-7">564</td>--}}
                                                            {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                                            {{--                                                                $3200.00--}}
                                                            {{--                                                            </td>--}}
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                    {{--                                                    <img src="http://127.0.0.1:8000/media/cinqueterre.jpg" class="img-rounded" alt="Cinque Terre" width="233" height="236" style="--}}
                                                    {{--    width: 100%;--}}
                                                    ">

                                                    <br>
                                                    <br>
                                                    <br>
                                                    {{--<br>--}}
                                                    {{--<br>--}}
                                                </div>
                                                <div class="border-bottom w-100  mb-13"></div>

                                            </div>

                                        </div>
                                        {{--                                        <div class="row border-bottom pb-2">--}}
                                        {{--                                            <div class="col-md-6 py-md-10 pr-md-10">--}}
                                        {{--                                                <div class="table-responsive">--}}
                                        {{--                                                    <table class="table">--}}
                                        {{--                                                        <thead>--}}
                                        {{--                                                        <tr>--}}
                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
                                        {{--                                                                Campaign Status--}}
                                        {{--                                                            </th>--}}

                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        </thead>--}}
                                        {{--                                                        <tbody>--}}
                                        {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
                                        {{--																		</span>in review--}}
                                        {{--                                                            </td>--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--																		</span>in progress--}}
                                        {{--                                                            </td>--}}

                                        {{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
                                        {{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                            --}}{{--                                                                $3200.00--}}
                                        {{--                                                            --}}{{--                                                            </td>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Front-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $4800.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Back-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $12600.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        </tbody>--}}
                                        {{--                                                    </table>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
                                        {{--                                                <div class="d-flex flex-column flex-md-row">--}}
                                        {{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                        {{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}
                                        {{--                                            <div class="col-md-6 py-md-10 pr-md-10">--}}
                                        {{--                                                <div class="table-responsive">--}}
                                        {{--                                                    <table class="table">--}}
                                        {{--                                                        <thead>--}}
                                        {{--                                                        <tr>--}}
                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
                                        {{--                                                                Payment Status--}}
                                        {{--                                                            </th>--}}

                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        </thead>--}}
                                        {{--                                                        <tbody>--}}
                                        {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
                                        {{--																		</span>in review--}}
                                        {{--                                                            </td>--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--																		</span>in progress--}}
                                        {{--                                                            </td>--}}

                                        {{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
                                        {{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                            --}}{{--                                                                $3200.00--}}
                                        {{--                                                            --}}{{--                                                            </td>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Front-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $4800.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Back-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $12600.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        </tbody>--}}
                                        {{--                                                    </table>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
                                        {{--                                                <div class="d-flex flex-column flex-md-row">--}}
                                        {{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                        {{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}

                                        {{--                                        </div>--}}
                                        {{--                                        <div class="row border-bottom pb-2">--}}
                                        {{--                                            <div class="col-md-12 py-md-10 pr-md-10">--}}
                                        {{--                                                <div class="table-responsive">--}}
                                        {{--                                                    <table class="table">--}}
                                        {{--                                                        <thead>--}}
                                        {{--                                                        <tr>--}}
                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}
                                        {{--                                                                Payment Status--}}
                                        {{--                                                            </th>--}}

                                        {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">--}}

                                        {{--                                                            </th>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        </thead>--}}
                                        {{--                                                        <tbody>--}}
                                        {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-left">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
                                        {{--																		</span>1 -pending--}}
                                        {{--                                                            </td>--}}
                                        {{--                                                            <td class="border-top-0 pl-0 pt-4 d-flex text-right">--}}
                                        {{--																		<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--																		</span>2  - paid--}}
                                        {{--                                                            </td>--}}

                                        {{--                                                            --}}{{--                                                            <td class="text-right pt-7">564</td>--}}
                                        {{--                                                            --}}{{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                            --}}{{--                                                                $3200.00--}}
                                        {{--                                                            --}}{{--                                                            </td>--}}
                                        {{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-success font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Front-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">120</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$40.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $4800.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        --}}{{--                                                        <tr class="font-weight-bolder border-bottom-0 font-size-lg">--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pl-0 py-4 d-flex align-items-center">--}}
                                        {{--                                                        --}}{{--																		<span class="navi-icon mr-2">--}}
                                        {{--                                                        --}}{{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        --}}{{--																		</span>Back-End Development--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">210</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 text-right py-4">$60.00</td>--}}
                                        {{--                                                        --}}{{--                                                            <td class="border-top-0 pr-0 py-4 font-size-h6 font-weight-boldest text-right">--}}
                                        {{--                                                        --}}{{--                                                                $12600.00--}}
                                        {{--                                                        --}}{{--                                                            </td>--}}
                                        {{--                                                        --}}{{--                                                        </tr>--}}
                                        {{--                                                        </tbody>--}}
                                        {{--                                                    </table>--}}
                                        {{--                                                </div>--}}
                                        {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
                                        {{--                                                <div class="d-flex flex-column flex-md-row">--}}
                                        {{--                                                    --}}{{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
                                        {{--                                                    --}}{{--                                                        <div class="font-weight-bold font-size-h6 mb-3">Vehicle Info--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">Loader</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">1234567890934</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
                                        {{--                                                    --}}{{--                                                            <span class="font-weight-bold mr-15">Vehicle Model:</span>--}}
                                        {{--                                                    --}}{{--                                                            <span class="text-right">BARC0032UK</span>--}}
                                        {{--                                                    --}}{{--                                                        </div>--}}
                                        {{--                                                    --}}{{--                                                    </div>--}}
                                        {{--                                                </div>--}}
                                        {{--                                            </div>--}}

                                        {{--                                        </div>--}}
                                    </div>
                                    <div class="row mt-n5">
                                        <div class="col-sm-12">
                                            <iframe width="560" height="315"
                                                    src="https://www.youtube.com/embed/U_lTuI8Ck0I" frameborder="0"
                                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                                    allowfullscreen></iframe>
                                        </div>
                                    </div>

                                </div>

                                <!--end::Invoice-->

                            </div>


                        </div>


                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="card-body p-4">
                                <!--begin::Invoice-->
                                <div class="row justify-content-center pt-4 px-4 pt-md-4 ">

                                    <div class="col-md-12">
                                        <div id="kt_repeater_1">
                                            <div class="form-group row" id="kt_repeater_1">
                                                <div data-repeater-list="">
                                                    <div data-repeater-item=""
                                                         class="form-group row align-items-center">
                                                        <div class="col-md-6">
                                                            <label>Campaign completed Video:</label>
                                                            <input type="file" class="form-control" name="state"
                                                                   placeholder="Select state">
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <label>Photos:</label>
                                                            <input type="file" class="form-control" name="[0][city]">
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>
                                                        <div class="col-md-6 mt-4">
                                                            <label>Miles :</label>
                                                            <input type="text" class="form-control"
                                                                   placeholder="Enter Miles" name="[0][zip]">
                                                            <div class="d-md-none mb-2"></div>
                                                        </div>


                                                    </div>
                                                </div>


                                            </div>


                                        </div>
                                    </div>

                                    <!--end::Invoice-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Submit
                    </button>
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>

    {{--    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-hidden="true">--}}
    {{--        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">--}}
    {{--            <div class="modal-content">--}}
    {{--                <div class="modal-header">--}}
    {{--                    <h5 class="modal-title" >I am Compaign Title</h5>--}}
    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
    {{--                        <i aria-hidden="true" class="ki ki-close"></i>--}}
    {{--                    </button>--}}
    {{--                </div>--}}
    {{--                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->--}}
    {{--                <div class="modal-body">--}}
    {{--                    <div class="container">--}}
    {{--                        <div class="card card-custom">--}}
    {{--                            <div class="card-body p-0">--}}
    {{--                                <!--begin::Invoice-->--}}
    {{--                                <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">--}}
    {{--                                    <div class="col-md-9">--}}
    {{--                                        <!-- begin: Invoice header-->--}}
    {{--                                        <div class="d-flex justify-content-between pb-10 pb-md-20 flex-column flex-md-row">--}}
    {{--                                            <h1 class="display-4 font-weight-boldest mb-10">INVOICE</h1>--}}
    {{--                                            <div class="d-flex flex-column align-items-md-end px-0">--}}
    {{--                                                <!--begin::Logo-->--}}
    {{--                                                <a href="#" class="mb-5 max-w-200px">--}}
    {{--															<span class="svg-icon svg-icon-full">--}}
    {{--																<!--begin::Svg Icon | path:assets/media/svg/logos/duolingo.svg-->--}}
    {{--																<svg xmlns="http://www.w3.org/2000/svg" width="113" height="31" viewBox="0 0 113 31" fill="none">--}}
    {{--																	<path d="M26.6802 13.33V13.485C26.6802 14.9575 26.6087 15.8875 26.4659 16.275C26.2517 16.895 25.966 17.36 25.4661 17.67C25.0376 17.98 24.5377 18.1358 23.8949 18.1358C23.2529 18.1358 22.7523 17.9808 22.3245 17.67C21.8953 17.3608 21.6103 16.895 21.3954 16.275C21.1811 15.7325 21.1104 14.8025 21.1104 13.4075V5.425H17.3254V13.485C17.3968 15.345 17.5396 16.6625 17.7539 17.515C18.1824 18.9108 18.8965 19.995 19.8963 20.8483C20.8962 21.6233 22.2531 22.0108 23.8242 22.0108C25.324 22.0108 26.538 21.7008 27.5379 21.0025C28.537 20.3058 29.2511 19.375 29.7518 18.0575C30.1088 17.205 30.3231 15.655 30.3231 13.485V5.425H26.6087V13.33H26.6802ZM15.6107 0H11.897V6.2775C11.8256 6.2775 11.7542 6.2 11.6828 6.2C10.4687 5.425 9.11182 5.0375 7.75492 5.0375C5.82669 5.0375 4.1127 5.735 2.61297 7.2075C0.898984 8.9125 0.0419922 11.005 0.0419922 13.485C0.0419922 15.81 0.827568 17.825 2.3273 19.4525C3.82704 21.0808 5.68314 21.9325 7.75492 21.9325C9.18324 21.9325 10.5401 21.545 11.7542 20.8483C12.9683 20.0733 13.8967 19.065 14.6109 17.7483C15.2536 16.5858 15.54 15.345 15.6107 14.0283V13.4858V0ZM10.6823 16.895C9.89668 17.7483 8.96899 18.2125 7.7542 18.2125C6.61155 18.2125 5.61172 17.7475 4.89756 16.895C4.11199 16.0425 3.75491 14.88 3.75491 13.485C3.75491 12.1675 4.11199 11.005 4.89756 10.1525C5.68314 9.3 6.61155 8.835 7.7542 8.835C8.96828 8.835 9.89668 9.3 10.6823 10.1525C11.4685 11.005 11.8256 12.1675 11.8256 13.485C11.8256 14.88 11.4685 16.0425 10.6823 16.895ZM46.2481 9.3C45.534 7.9825 44.6049 6.975 43.3915 6.2C42.1774 5.425 40.8205 5.0375 39.4636 5.0375C37.5354 5.0375 35.8207 5.735 34.3217 7.2075C32.6077 8.9125 31.75 10.9275 31.75 13.485C31.75 15.81 32.5363 17.825 34.0353 19.4525C35.5357 21.0808 37.3918 21.9325 39.4636 21.9325C40.8919 21.9325 42.2481 21.545 43.4629 20.8483C44.677 20.0733 45.6054 19.065 46.3195 17.7483C47.0337 16.4308 47.3908 15.0358 47.3908 13.4858C47.248 12.0125 46.9623 10.6175 46.2481 9.3ZM42.3917 16.895C41.6061 17.7483 40.6777 18.2125 39.4636 18.2125C38.2495 18.2125 37.3211 17.7475 36.607 16.895C35.8214 16.0425 35.4643 14.88 35.4643 13.485C35.4643 12.1675 35.8214 11.005 36.6077 10.1525C37.3933 9.3 38.321 8.835 39.4643 8.835C40.6777 8.835 41.6068 9.3 42.3924 10.1525C43.178 11.005 43.535 12.1675 43.535 13.485C43.5336 14.88 43.1058 16.0425 42.3917 16.895ZM60.2457 21.7H64.0307V6.1225H60.2457V21.7ZM53.8182 17.515C53.3897 17.2058 53.0326 16.74 52.8898 16.12C52.747 15.655 52.6756 14.725 52.6756 13.33V0H48.9619V13.2525C49.0334 15.4225 49.1762 16.895 49.5333 17.825C50.0332 19.065 50.7473 20.0725 51.7472 20.77C52.747 21.4675 53.9618 21.7775 55.4608 21.7775H58.3174V17.98H55.3894C54.7466 17.98 54.246 17.825 53.8182 17.515ZM78.7424 9.145C78.2425 7.905 77.5283 6.8975 76.5278 6.2C75.5287 5.5025 74.3139 5.1925 72.8142 5.1925C71.1716 5.1925 69.8861 5.58 68.8863 6.355C67.8872 7.13 67.173 8.215 66.7438 9.6875C66.5296 10.4625 66.3867 11.8575 66.3153 13.7175V21.7H70.1003V13.95V13.64C70.1003 12.3225 70.1725 11.315 70.386 10.7725C70.6003 10.1525 70.8859 9.765 71.3144 9.3775C71.7429 9.0675 72.2428 8.9125 72.8856 8.9125C73.5276 8.9125 74.0282 9.0675 74.4567 9.3775C74.8852 9.6875 75.2423 10.1525 75.4558 10.7725C75.5987 11.2375 75.6701 12.1675 75.6701 13.5625V21.7H79.3837V14.88V13.64C79.3137 11.5475 79.0995 9.9975 78.7424 9.145ZM62.0311 0.31C60.9598 0.31 60.0314 1.3175 60.0314 2.48C60.0314 3.6425 60.8884 4.65 62.0311 4.65C63.1023 4.65 64.0307 3.72 64.0307 2.48C64.0307 1.24 63.1023 0.31 62.0311 0.31ZM111.951 9.455C111.237 8.1375 110.308 7.13 109.094 6.355C107.88 5.58 106.595 5.27 105.094 5.27C102.952 5.27 101.166 6.1225 99.6666 7.75C98.1661 9.3775 97.3813 11.3925 97.3813 13.7175C97.3813 16.1975 98.2383 18.2892 99.9522 19.995C101.453 21.4675 103.166 22.165 105.094 22.165C106.522 22.165 107.808 21.7775 109.022 21.0025C110.236 20.2275 111.237 19.22 111.879 17.9025C112.593 16.5858 112.95 15.19 112.95 13.7183C113.022 12.1675 112.665 10.7725 111.951 9.455ZM109.236 13.7175C109.236 15.1125 108.879 16.1975 108.094 17.05C107.308 17.9025 106.38 18.3675 105.166 18.3675C104.022 18.3675 103.023 17.9017 102.309 17.05C101.523 16.1975 101.166 15.035 101.166 13.7175C101.166 12.3225 101.523 11.16 102.309 10.3075C103.095 9.455 104.022 8.99 105.166 8.99C106.308 8.99 107.308 9.455 108.094 10.3075C108.879 11.16 109.236 12.3225 109.236 13.7175C109.236 13.64 109.236 13.64 109.236 13.7175ZM96.0965 13.02C96.0244 11.7025 95.6673 10.54 95.0967 9.3775C94.3825 8.06 93.4541 7.0525 92.3122 6.2775C91.0981 5.5025 89.8126 5.1925 88.3843 5.1925C86.2418 5.1925 84.4564 5.9675 82.9559 7.6725C81.4562 9.3 80.7421 11.315 80.7421 13.64C80.7421 16.12 81.599 18.2125 83.2416 19.84C84.7413 21.3125 86.3832 22.0108 88.3121 22.0108C89.669 22.0108 91.026 21.6233 92.24 20.8483C92.3122 20.77 92.3829 20.77 92.4543 20.6933V22.5525C92.4543 23.9483 92.0258 25.0325 91.3116 25.8858C90.526 26.7383 89.5976 27.2025 88.455 27.2025C87.3123 27.2025 86.3839 26.815 85.5983 25.9625L83.1702 28.7525L83.2416 28.83C84.7413 30.3017 86.4553 31 88.3121 31C89.669 31 91.026 30.6125 92.24 29.8375C93.4541 29.0625 94.3825 28.055 95.0967 26.7375C95.8108 25.42 96.0965 24.025 96.0965 22.5525V14.105V13.5625C96.1679 13.4075 96.1679 13.175 96.0965 13.02ZM92.4536 13.5625C92.4536 14.9575 92.0965 16.0425 91.3109 16.895C90.5253 17.7483 89.5969 18.2125 88.4543 18.2125C87.3116 18.2125 86.3832 17.7475 85.5976 16.895C84.812 16.0425 84.455 14.9575 84.455 13.5625C84.455 12.1675 84.812 11.0825 85.5976 10.1525C86.3832 9.3 87.3116 8.835 88.4543 8.835C89.5969 8.835 90.5967 9.3 91.3109 10.1525C92.0258 11.0825 92.4536 12.1675 92.4536 13.5625Z" fill="#1CB0F6"></path>--}}
    {{--																</svg>--}}
    {{--                                                                <!--end::Svg Icon-->--}}
    {{--															</span>--}}
    {{--                                                </a>--}}
    {{--                                                <!--end::Logo-->--}}
    {{--                                                <span class="d-flex flex-column align-items-md-end font-size-h5 font-weight-bold text-muted">--}}
    {{--															<span>Cecilia Chapman, 711-2880 Nulla St, Mankato</span>--}}
    {{--															<span>Mississippi 96522</span>--}}
    {{--														</span>--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}

    {{--                                        <!--end: Invoice header-->--}}
    {{--                                        <!--begin: Invoice body-->--}}
    {{--                                        <div class="row border-bottom pb-10">--}}
    {{--                                            <div class="col-md-9 py-md-10 pr-md-10">--}}
    {{--                                                <div class="table-responsive">--}}
    {{--                                                    <table class="table">--}}
    {{--                                                        <thead>--}}
    {{--                                                        <tr>--}}
    {{--                                                            <th class="pt-1 pb-9 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">Description</th>--}}
    {{--                                                            <th class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">Hours</th>--}}
    {{--                                                            <th class="pt-1 pb-9 text-right font-weight-bolder text-muted font-size-lg text-uppercase">Rate</th>--}}
    {{--                                                            <th class="pt-1 pb-9 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">Amount</th>--}}
    {{--                                                        </tr>--}}
    {{--                                                        </thead>--}}
    {{--                                                        <tbody>--}}
    {{--                                                        <tr class="font-weight-bolder font-size-lg">--}}
    {{--                                                            <td class="border-top-0 pl-0 pt-7 d-flex align-items-center">--}}
    {{--																		<span class="navi-icon mr-2">--}}
    {{--																			<i class="fa fa-genderless text-danger font-size-h2"></i>--}}
    {{--																		</span>Creative Designs</td>--}}
    {{--                                                            <td class="text-right pt-7">80</td>--}}
    {{--                                                            <td class="text-right pt-7">$40.00</td>--}}
    {{--                                                            <td class="pr-0 pt-7 font-size-h6 font-weight-boldest text-right">$3200.00</td>--}}
    {{--                                                        </tr>--}}

    {{--                                                        </tbody>--}}
    {{--                                                    </table>--}}
    {{--                                                </div>--}}
    {{--                                                <div class="border-bottom w-100 mt-7 mb-13"></div>--}}
    {{--                                                <div class="d-flex flex-column flex-md-row">--}}
    {{--                                                    <div class="d-flex flex-column mb-10 mb-md-0">--}}
    {{--                                                        <div class="font-weight-bold font-size-h6 mb-3">BANK TRANSFER</div>--}}
    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Make:</span>--}}
    {{--                                                            <span class="text-right">Barclays UK</span>--}}
    {{--                                                        </div>--}}
    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
    {{--                                                            <span class="text-right">1234567890934</span>--}}
    {{--                                                        </div>--}}
    {{--                                                        <div class="d-flex justify-content-between font-size-lg">--}}
    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Model</span>--}}
    {{--                                                            <span class="text-right">BARC0032UK</span>--}}
    {{--                                                        </div>--}}
    {{--                                                    </div>--}}
    {{--                                                </div>--}}
    {{--                                            </div>--}}
    {{--                                            <div class="col-md-3 border-left-md pl-md-10 py-md-10 text-right">--}}
    {{--                                                <!--begin::Total Amount-->--}}
    {{--                                                <div class="font-size-h4 font-weight-bolder text-muted mb-3">TOTAL AMOUNT</div>--}}
    {{--                                                <div class="font-size-h1 font-weight-boldest">$20,600.00</div>--}}
    {{--                                                <div class="text-muted font-weight-bold mb-16">Taxes included</div>--}}
    {{--                                                <!--end::Total Amount-->--}}
    {{--                                                <div class="border-bottom w-100 mb-16"></div>--}}
    {{--                                                <!--begin::Invoice To-->--}}
    {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Campaign Status.</div>--}}
    {{--                                                <div class="font-size-lg font-weight-bold mb-10">Iris Watson.--}}
    {{--                                                    <br>Fredrick Nebraska 20620</div>--}}
    {{--                                                <!--end::Invoice To-->--}}
    {{--                                                <!--begin::Invoice No-->--}}
    {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">Payment Status</div>--}}
    {{--                                                <div class="font-size-lg font-weight-bold mb-10">56758</div>--}}
    {{--                                                <!--end::Invoice No-->--}}
    {{--                                                <!--begin::Invoice Date-->--}}
    {{--                                                <div class="text-dark-50 font-size-lg font-weight-bold mb-3">DATE</div>--}}
    {{--                                                <div class="font-size-lg font-weight-bold">12 May, 2020</div>--}}
    {{--                                                <!--end::Invoice Date-->--}}
    {{--                                            </div>--}}
    {{--                                        </div>--}}
    {{--                                        <!--end: Invoice body-->--}}
    {{--                                    </div>--}}
    {{--                                </div>--}}

    {{--                                <!--end::Invoice-->--}}
    {{--                            </div>--}}
    {{--                        </div>--}}
    {{--                    </div>--}}
    {{--                </div>--}}
    {{--                <div class="modal-footer">--}}
    {{--                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back</button>--}}
    {{--                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}










    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">I am Compaign Title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pt-8 px-8 pt-md-27 px-md-0">

                                <div class="col-md-12">
                                    <div id="kt_repeater_1">
                                        <div class="form-group row" id="kt_repeater_1">
                                            <div data-repeater-list="">
                                                <div data-repeater-item="" class="form-group row align-items-center">
                                                    <div class="col-md-6">
                                                        <label>State:</label>
                                                        <input type="text" class="form-control" name="[0][state]"
                                                               placeholder="Select state" onkeyup="showdetail();">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>City:</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Select city" name="[0][city]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <label>Zip:</label>
                                                        <input type="text" class="form-control"
                                                               placeholder="Enter zip code" name="[0][zip]">
                                                        <div class="d-md-none mb-2"></div>
                                                    </div>
                                                    <div class="col-md-6 mt-4">
                                                        <a href="javascript:;" data-repeater-delete=""
                                                           class=" btn btn-sm font-weight-bolder btn-light-danger">
                                                            <i class="la la-trash-o"></i>
                                                        </a>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="col-md-12 ">
                                                <a href="javascript:;" data-repeater-create=""
                                                   class="btn btn-sm font-weight-bolder btn-light-primary">
                                                    <i class="la la-plus"></i> Add more
                                                </a>
                                            </div>

                                        </div>
                                        <div class="col-md-4 border text-center p-1">
                                            <div class="form-group">
                                                <h3>Details</h3>
                                                <div class="inner"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade show" id="feedback" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Campaign Query</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block"><h5 class="d-inline">Campaign Id:  </h5> <span
                                                    id='cmp_id' class="float-right mr-30"></span></span>
                                        </div>
                                        <br><br><br>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Write feedback about campaign:</label>
                                                <textarea name="s_u1" class="w-100 " type="textarea"
                                                          rows="10"></textarea>
                                                <span class="form-text text-muted"> feedback.</span>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create=""
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade show" id="courier" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Mailing Address Details</h5>
                    {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{-- <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{-- </button>--}}
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block text-center"><h5
                                                    class="d-inline">Driver Name:</h5> <span
                                                    id='driv_name' class=" ml-1"></span></span>
                                        </div>
                                        <br><br>

                                        <div class="col-md-2 offset-3  p-0 text-center pt-5 feed" style="display: none">
                                            Driver feedback:
                                        </div>
                                        <div class="col-md-7  pt-5 text-left  p-0"><span class="feedback_text"></span>
                                        </div>
                                        <div class="col-md-12 mb-1 ">
                                            <span class="d-block text-center cor_error text-danger"></span>
                                        </div>
{{--                                        <div class="col-lg-12">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>Courier Company:</label>--}}
{{--                                                <input name="courier_company" class="w-100 form-control" type="text"--}}
{{--                                                       id='courier_company'/>--}}
                                                <input name="driver_id" type="hidden" id='driver_id'/>
                                                <input name="campaign_id" type="hidden" id='campaign_id'/>
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-12">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>Courier Url:</label>--}}
{{--                                                <input name="courier_url" class="w-100 form-control" type="url"--}}
{{--                                                       id='courier_url'/>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                        <div class="col-lg-12">--}}
{{--                                            <div class="form-group">--}}
{{--                                                <label>Tracking id:</label>--}}
{{--                                                <input name="tracking_id" class="w-100 form-control" type="text"--}}
{{--                                                       id='tracking_id'/>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
                                        <div class="col-lg-6 offset-3">
                                            <div class="form-group">
                                                <label>Mailing Address:</label>
                                                <textarea name="mail_address" class="w-100 form-control" rows="3" id="mail_address"></textarea>
                                            </div>
                                        </div>


                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="sendmail1()" id="sendmail1_btn"
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal"
                            data-toggle="modal" data-target="#appliedDriversModal" onclick="bck1()">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="courier18" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Courier Details</h5>
                    {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{-- <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{-- </button>--}}
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block text-center"><h5
                                                    class="d-inline">Driver Name:</h5> <span
                                                    id='drivv_name' class="ml-1"></span></span>
                                        </div>
                                        <br><br>

{{--                                        <div class="col-md-2 offset-3  p-0 text-center pt-5 feed" style="display: none">--}}
{{--                                            Driver feedback:--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-7  pt-5 text-left  p-0"><span class="feedback_text"></span>--}}
{{--                                        </div>--}}
                                        <div class="col-md-12 mb-1 ">
                                            <span class="d-block text-center cor_error text-danger"></span>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Company:</label>
                                                <input name="courier_company" class="w-100 form-control" type="text"
                                                       id='courier_companyy'/>
                                                <input name="driver_id" type="hidden" id='driverr_id'/>
                                                <input name="campaign_id" type="hidden" id='campaignn_id'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Url:</label>
                                                <input name="courier_url" class="w-100 form-control" type="url"
                                                       id='courier_urll'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tracking id:</label>
                                                <input name="tracking_id" class="w-100 form-control" type="text"
                                                       id='tracking_idd'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 offset-3">
                                            <div class="form-group">
                                                <label>Mailing Address:</label>
                                                <textarea name="mail_address" class="w-100 form-control" rows="3" id="mail_addresss"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="sendmail31()" id="sendmail31_btn"
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal"
                            data-toggle="modal" data-target="#appliedDriversModal" onclick="bck1()">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade show" id="courier19" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Courier Details</h5>
                    {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{-- <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{-- </button>--}}
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block text-center"><h5
                                                    class="d-inline">Driver Name:</h5> <span
                                                    id='drivvv_name' class="ml-1"></span></span>
                                        </div>
                                        <br><br>

{{--                                        <div class="col-md-2 offset-3  p-0 text-center pt-5 feed" style="display: none">--}}
{{--                                            Driver feedback:--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-7  pt-5 text-left  p-0"><span class="feedback_text"></span>--}}
{{--                                        </div>--}}
                                        <div class="col-md-12 mb-1 ">
                                            <span class="d-block text-center cor_error text-danger"></span>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Company:</label>
                                                <input name="courier_company" class="w-100 form-control" type="text"
                                                       id='courier_companyye'/>
                                                <input name="driver_id" type="hidden" id='driverr_id'/>
                                                <input name="campaign_id" type="hidden" id='campaignn_id'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Url:</label>
                                                <input name="courier_url" class="w-100 form-control" type="url"
                                                       id='courier_urlle'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tracking id:</label>
                                                <input name="tracking_id" class="w-100 form-control" type="text"
                                                       id='tracking_idde'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-9 offset-2">
                                            <div class="form-group">
                                                <label>Mailing Address:</label>
                                                <textarea name="mail_address" class="w-100 form-control" rows="3" id="mail_addressse"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal"
                            data-toggle="modal" data-target="#appliedDriversModal" onclick="bck1()">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
<div class="modal fade show" id="courier20" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Courier Details</h5>
                    {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{-- <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{-- </button>--}}
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block text-center"><h5
                                                    class="d-inline" >Driver Name:</h5> <span
                                                   class="ml-1  drivvvnamee"></span></span>
                                        </div>
                                        <br><br>

{{--                                        <div class="col-md-2 offset-3  p-0 text-center pt-5 feed" style="display: none">--}}
{{--                                            Driver feedback:--}}
{{--                                        </div>--}}
{{--                                        <div class="col-md-7  pt-5 text-left  p-0"><span class="feedbacktext"></span>--}}
{{--                                        </div>--}}

                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Company:</label>
                                                <input name="courier_company" class="w-100 form-control couriercompany" type="text"
                                                      />
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Url:</label>
                                                <input name="courier_url" class="w-100 form-control courierurl" type="url"/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tracking id:</label>
                                                <input name="tracking_id" class="w-100 form-control trackinggg" type="text"
                                                       />
                                            </div>
                                        </div>
                                        <div class="col-lg-9 offset-2">
                                            <div class="form-group">
                                                <label>Mailing Address:</label>
                                                <textarea name="mail_address" class="w-100 form-control mailaddress" rows="3"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal"
                            data-toggle="modal" data-target="#appliedDriversModal" onclick="bck1()">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="courier_rejectedModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Courier Details</h5>
                    <div class="cour_status float-right"></div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--                        <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{--                    </button>--}}
                </div>
                <div class="modal-body">

                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">
                                    <div class="form-group row pl-2 pr-2">
                                        <div class="col-md-12 mt-4 ">
                                            <span class=" d-block text-center"><h5
                                                    class="d-inline">Driver Name:</h5> <span
                                                    id='driv_namee' class=" ml-1"></span></span>
                                        </div>
                                        <br><br>

                                        <div class="col-md-3 offset-2  p-0 text-center pt-5 feed feed_hide"
                                             style="display: none">
                                            Driver reject courier feedback:
                                        </div>
                                        <div class="col-md-7  pt-5 text-left  p-0"><span class="feedback_text"></span>
                                        </div>
                                        <div class="col-md-12 mb-1 ">
                                            <span class="d-block text-center cor_error text-danger"></span>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Company:</label>
                                                <input name="courier_company" class="w-100 form-control" type="text"
                                                       id='courier_companyy' disabled/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Courier Url:</label>
                                                <input name="courier_url" class="w-100 form-control" type="url" disabled
                                                       id='courier_urll'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Tracking id:</label>
                                                <input name="tracking_id" class="w-100 form-control" type="text"
                                                       id='tracking_idd'/>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Mailing Address:</label>
                                                <input name="mail_address" class="w-100 form-control" id="mail_addresss"
                                                       disabled/>
                                            </div>
                                        </div>

                                        <div class="col-md-6 not_received_pic_hide  offset-1 p-0 text-center pt-5">Not
                                            received file:<span class="not_received_pic"></span></div>


                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="sendmail1()" id="sendmail1_btnn"
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal"
                            data-toggle="modal" data-target="#appliedDriversModal" onclick="bck()">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show " id="get_details_modal" tabindex="-1" role="dialog" style="z-index: 9999"
         aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog  modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver campaign sticker documents</h5>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--                        <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{--                    </button>--}}
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-lg-6 offset-1 reject_sticker_field d-none p-2">
                                    <div class="form-group">
                                        <label>Comment</label>&nbsp &nbsp &nbsp &nbsp<span
                                            id="reject_sticker_error" class="text-danger text-success"></span>
                                        <textarea name="s_u1" class='w-100 reject_field_inputtt' type="textarea"
                                                  rows="5"></textarea>
                                        <span class="form-text text-muted">Please write a short comment</span>
                                        <span class="btn btn-sm btn-primary float-right pr-3 sbt_sticker_reject_btn"
                                              onclick="sbt_sticker_reject()"> submit</span>

                                    </div>
                                </div>

                                <div class="col-md-9 pb-0 ">
                                    <div class="form-group row pl-2 pr-2">
                                        <br><br>
                                        <div class="col-lg-12">
                                            <video width="800" height="340" autoplay controls id="sticker_video">
                                                <source src="">
                                            </video>
                                        </div>
                                        <div class="col-lg-12 mt-1">
                                            <img width="800" height="300" id="sticker_img" alt="sticker image" src=""/>
                                        </div>

                                        <input type="hidden" id="drvr_id" name="drvr_id">
                                        <input type="hidden" id="campe_id" name="campe_id">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="reject_sticker()"
                       class="font-weight-bolder btn-light-primary reject_sticker_btn">
                        reject
                    </a>&nbsp;

                    <a href="javascript:;" onclick="accept_sticker()"
                       class="btn btn-sm font-weight-bolder btn-light-primary accept_sticker_btn">
                        Accept
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show " id="get_details_modal1" tabindex="-1" role="dialog" style="z-index: 99999"
         aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog  modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver campaign documents</h5>
                    <small class="badge badge-pill badge-light  ml-3" id="stats"></small>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-9 pb-0 ">
                                    <div class="form-group row pl-2 pr-2">
                                        <br><br>
                                        <div class="col-lg-12 pt-5">
                                            <table
                                                class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center table-bordered">
                                                <tr>
                                                    <td>Campaign Id</td>
                                                    <td id="campee_id"></td>
                                                    <td>Driver Name</td>
                                                    <td id="drvwr_name"></td>
                                                    <td>Start Miles</td>
                                                    <td id="strt_miles"></td>
                                                    <td>Capmaign start date</td>
                                                    <td id="strt_date"></td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="col-lg-12 pt-0" id='miles-comment'>

                                        </div>
                                        <div class="col-lg-6">
                                            <video width="400" height="340" controls id="sticker_video1">
                                                <source src="">
                                            </video>
                                        </div>
                                        <div class="col-lg-6 mt-1 pl-10">
                                            <img width="400" height="300" id="sticker_img1" alt="sticker image" src=""/>
                                        </div>
                                        <div class="col-lg-12 mt-1 mile_pic">
                                            <img width="800" height="300" id="mile_pic" alt="Mile image" src=""/>
                                        </div>

                                        <div class="col-lg-12 d-none">
                                            <div class="form-group">
                                                <label>Write details here</label>&nbsp &nbsp &nbsp &nbsp<span
                                                    id="reject_feedback_error" class="text-danger text-success"></span>
                                                <textarea name="s_u1" class='reject_feedback_field w-100 '
                                                          type="textarea" rows="10"></textarea>
                                                <span
                                                    class="form-text text-muted">Please send modification request</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade show " id="mile_details_modal" tabindex="-1" role="dialog" style="z-index: 9999"
         aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog  modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Mile details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-9 pb-0 ">
                                    {{--                                    <span class="text-center d-block"><h5 class="d-inline">Campaign Id:  </h5> <span id="campe_id"></span></span>--}}
                                    <div class="form-group row pl-2 pr-2">
                                        <br><br>
                                        <div class="col-lg-12 reject_feedback_field ">
                                            <div class="form-group">
                                                <label>Comment</label>&nbsp &nbsp &nbsp &nbsp<span
                                                    id="reject_mile_error" class="text-danger text-success"></span>
                                                <textarea name="s_u1" class='w-100 reject_field_inputt' type="textarea"
                                                          rows="5"></textarea>
                                                <span class="form-text text-muted">Please write a short comment</span>
                                                <span
                                                    class="btn btn-sm btn-primary float-right pr-3 sbt_mile_reject_btn"
                                                    onclick="sbt_mile_reject()"> submit</span>

                                            </div>
                                        </div>
                                        <div class="col-lg-12  ">
                                            <div class="form-group ">
                                                <label>Car starting Miles</label>
                                                <input name="starting_miles" id='starting_miles' class="form-control"
                                                       disabled/>
                                                {{--                                                <span class="form-text text-muted"></span>--}}
                                            </div>
                                        </div>
                                        <div class="col-lg-12 mt-1">
                                            <img width="500" height="300" id="mileage_picture" alt="sticker image"
                                                 src=""/>
                                        </div>
                                        <input type="hidden" id="drvr_id1" name="drvr_id">
                                        <input type="hidden" id="campe_id1" name="campe_id">

                                    </div>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="reject_miles()" id="reject_mile_btn"
                       class="  font-weight-bolder btn-light-primary reject_mile_btn">
                        reject
                    </a>&nbsp;

                    <a href="javascript:;" onclick="accept_mile()" id="accept_mile_btn"
                       class="btn btn-sm font-weight-bolder btn-light-primary accept_mile_btn">
                        Accept
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade show" id="statusUpdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Update Campaign Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12">

                                    <div class="form-group row pl-4">
                                        <div class="row">
                                            <div class="col-md-12 mt-4 ">
                                                <span class="text-center d-block"><h5
                                                        class="d-inline">Campaign Id:  </h5> <span id="camp_id"></span></span>
                                            </div>
                                            <br><br><br>

                                            <div class="col-md-12 text-center">
                                                <form id="form-id" method="POST"
                                                      action="{{ route('campaign.status.update') }}">
                                                    @csrf
                                                    <input type="hidden" name="car_id" id="car-id">
                                                    <input type="hidden" name="user_id" id="user_id">
                                                    <input type="hidden" name="prev_value" id="prev_value">
                                                    <div class="form-check">
                                                        <label>Status Update:</label>
                                                        <select name="s_u" class=" form-control stat1" required>
                                                            @foreach($status as $stats)
                                                                @if($stats)
                                                                    <option
                                                                        value="{{$stats->id}}">{{$stats->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                        <span class="form-text text-danger status_error"></span>
                                                    </div>
                                                    <div class="form-check">
                                                        <div class="comment d-none">
                                                            <label>Write Comment:</label>
                                                            <textarea name="comment" id='comment_field'
                                                                      class="form-control" required></textarea>
                                                            <span
                                                                class="form-text text-danger comment_field_error "> </span>
                                                        </div>


                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" data-repeater-create="" onclick="formsbmt()"
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Update
                    </a>&nbsp;
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade show" id="bespoke" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Be Spoke</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0">
                            <!--begin::Invoice-->
                            <div class="justify-content-center pl-4 p-1 px-md-0">
                                    <div class="form-group pl-4">
                                        <div class="row">
                                            <div class="col-md-12 mt-4 ">
                                                <span class="text-center d-block"><h5  class="d-inline">Campaign Id:  </h5> <span id="camp_ided"></span></span>
                                            </div>
                                            <br><br>
                                            <div class="col-md-8 offset-2 ">
                                                <table class="table-responsive">
                                                <tr>
                                                    <td>Company: </td>
                                                    <td ><span id="cmp"></span></td>
                                                </tr>
                                                <tr>
                                                    <td>Slogan: </td>
                                                    <td id="slogan"></td>
                                                </tr>
                                                <tr>
                                                    <td>Contact: </td>
                                                    <td id="contact"></td>
                                                </tr>

                                                <tr>
                                                    <td>Detail: </td>
                                                    <td id="detail"></td>
                                                </tr>
                                                </table>
                                                <form id="form-id" method="POST"
                                                      action="{{ route('campaignbespokeUpdate')}}">
                                                    @csrf
                                                    <input type="hidden" name="campaign_id" id="car-idd">
                                                    <input type="hidden" name="user_id" id="user-idd">
                                                    <div class="form-check pt-1">
                                                        <label>Delivery date:</label>
                                                         <input type="date" name="deliver" class="form-control" placeholder="Delivery Date" required />
                                                        <span class="form-text text-danger date_error"></span>
                                                    </div>
                                                    <div class="form-check">
                                                            <label>Price:</label>
                                                            <input name="price"  type="number" min="0" class="form-control" required value="1"/>
                                                            <span class="form-text text-danger comment_field_error "> </span>
                                                    </div>

                                                    <div class="form-check float-right pt-4">
                                                        <input name="submit" class="form-control btn-md btn-info"  type="submit" value="Save"/>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                    </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
{{--                <div class="modal-footer">--}}
{{--                    <a href="javascript:;" data-repeater-create="" onclick="formsbmt()"--}}
{{--                       class="btn btn-sm font-weight-bolder btn-light-primary">--}}
{{--                        Update--}}
{{--                    </a>&nbsp;--}}
{{--                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back--}}
{{--                    </button>--}}
{{--                </div>--}}
            </div>
        </div>
    </div>

    <div class="modal fade show" id="appliedDriversModal" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog  modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header pb-0">
                    <h5 class="modal-title">Drivers Management <small class="badge badge-pill badge-light ml-2" id="camp_id2"></small></h5>
                    <div class="float-right">
                        <small class="badge badge-pill badge-success mr-3 px-3 completed-ride" id="completed-rides">0</small>
                    </div>
                </div>
                <div class="modal-body pt-1">
                    <div class="card card-custom">
                        <div class="card-body p-0 ">
                            <div class="carss my-2"></div>
                            <hr>

                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4  px-md-0">
                                <div class="col-lg-12 ">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="list-group Campainge-Details-list" id="list-tab" role="tablist">
                                            </div>
                                        </div>
                                        <div class="col-8 border-left">
                                            <div class="tab-content Campainge-Details-others" id="nav-tabContent">

                                            </div>
                                        </div>
                                    </div>
                                     <br>
                                     <table class=" col-md-8 offset-1 table-fluid mb-5" id="jqueryTable">
                                         <thead>
                                         <th >Name</th>
                                         <th >Email</th>
                                         <th >Zip</th>
                                         <th >Assigned Campaigns</th>
                                         <th >Status</th>
                                         <th ></th>

                                         </thead>
                                         <tbody>

                                         </tbody>
                                     </table>
                                </div>

                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="location.reload()" class="btn btn-light-primary font-weight-bold"
                            data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade show" id="DriverDocuments" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Campaign Start Documents</h5>

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>

                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-1 ">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-2  pb-5">
                                    Start miles of car
                                </div>
                                <div class="col-md-9 pb-5" id="miles_no">
                                </div>


                                <div class="col-md-2  ">
                                    Sticker Picture
                                </div>
                                <div class="col-md-9 card card-body p-1 mb-4 ">
                                    <img id="sticker_pic" height="220px" alt="sticker picture">
                                </div>

                                <div class="col-md-2 ">
                                    Mileage Picture
                                </div>

                                <div class="col-md-9 card card-body p-1 mb-4 ">
                                    <img height="220px" id="mileage_pic" alt="mileage picture">
                                </div>

                            </div>

                            <!--end::Invoice-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    &nbsp;<input type="hidden" id="campp_id">
                    <input type="hidden" id="driverr_id">
                    <button type="button" class="btn btn-light-success font-weight-bold">Request to resend documents
                        again
                    </button>
                    <button href="javascript:;" id="approvedd" onclick="approveDriver()"
                            class="btn btn-sm font-weight-bolder btn-light-primary">
                        Approve
                    </button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade show" id="DriverCompleteDocuments" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm" aria-modal="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Driver Campaign Finish Documents <small class="badge badge-pill badge-light"
                                                                                    id="camp_id12"></small></h5>


                    <div class="float-right">
                        <small class="badge badge-pill badge-light ml-3" id="drriver"></small>
                    </div>
                    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                    {{--                        <i aria-hidden="true" class="ki ki-close"></i>--}}
                    {{--                    </button>--}}
                </div>
                <div class="modal-body ">
                    <div class="card card-custom">
                        <div class="card-body p-1 ">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-6  ">
                                    <div class="row ">
                                        <div class="col-md-9  pb-3 pl-4">
                                            Start miles of car
                                        </div>
                                        <div class="col-md-3 pb-3" id="start_miles_no"></div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-9  pb-3 pl-4">
                                            Campaign start date
                                        </div>
                                        <div class="col-md-3 pb-3" id="start_date"></div>
                                    </div>
                                    <div class="">
                                        Sticker Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img id="start_sticker_pic" height="280px" alt="Sticker picture">
                                    </div>

                                    <div class="">
                                        Mileage Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img height="280px" id="start_mileage_pic" alt="mileage picture">
                                    </div>

                                </div>
                                <div class="col-md-6  ">
                                    <div class="row ">
                                        <div class="col-md-6  pb-3 pl-4">
                                            Completed miles of car
                                        </div>
                                        <div class="col-md-3 pb-3" id="completed_miles_no"></div>
                                    </div>
                                    <div class="row ">
                                        <div class="col-md-6  pb-3 pl-4">
                                            Campaign completed date
                                        </div>
                                        <div class="col-md-3 pb-3" id="completed_date"></div>
                                    </div>
                                    <div class="">
                                        Sticker Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img id="completed_sticker_pic" height="280px" alt="sticker picture">
                                    </div>
                                    <div class="">
                                        Mileage Picture
                                    </div>
                                    <div class="col-md-12 card card-body p-1 mb-4 ">
                                        <img height="280px" id="completed_mileage_pic" alt="mileage picture">
                                    </div>
                                </div>

                                <div class="col-md-6 mt-4 ">
                                    <label>Covered Miles* </label>
                                    @csrf
                                    <input type="number" class="form-control" type="number" id="miles_covered"
                                           name="miles_covered">
                                    <div class="d-md-none mb-2"></div>
                                </div>

                                <div class="col-md-6 mt-4 ">
                                    <label>Days </label>  <small>&nbsp (Campaign days: <span id="campaign_days"></span>)</small>
                                    <input type="text" disabled class="form-control" type="number" id="days_covered">
                                    <div class="d-md-none mb-2"></div>
                                </div>
                                <div class="col-md-6 mt-4 ">
                                    <label>Refundable amount<small>&nbsp (Actual amount: <span id="actual-amount"></span>)</small> </label>
                                    <input type="number" class="form-control" type="number" id="actual_amount">
                                    <div class="d-md-none mb-2"></div>
                                </div>
                                <div class="col-md-6 mt-4 ">
                                    <label>Giving amount<small>(80%)</small></label>
                                    <input type="number" class="form-control" type="number" id="giving_amount">
                                    <div class="d-m text-danger mb-2 " id="giving_amount_error"></div>
                                </div>
                                {{--                                <div class="col-md-6 mt-4 ">--}}
                                {{--                                    <label>Actual Amount </label>--}}
                                {{--                                    <span id="actual_amount"></span>--}}
                                {{--                                    <div class="d-md-none mb-2"></div>--}}
                                {{--                                </div>--}}
                                {{--                                <div class="col-md-6 mt-4 ">--}}
                                {{--                                    <label>Giving Amount </label>--}}
                                {{--                                    <span id="giving_amount"></span>--}}
                                {{--                                    <div class="d-md-none mb-2"></div>--}}
                                {{--                                </div>--}}


                            </div>

                            <!--end::Invoice-->
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    &nbsp;<input type="hidden" id="camppp_id">
                    <input type="hidden" id="driverrr_id">
                    {{--                    <button type="button" class="btn btn-light-success font-weight-bold">Request to resend documents again</button>--}}
                    <a href="#" class=" btn-light-primary font-weight-bold"  data-dismiss="modal" onclick="backk()"
                       >Back</a>
                    <button href="javascript:;" id="approvedd1" onclick="paidDriver()"
                            class="btn btn-sm font-weight-bolder btn-light-primary">
                        Approve
                    </button>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade show" id="earningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-modal="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Drivers Earning</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card card-custom">
                        <div class="card-body p-0 ">
                            <!--begin::Invoice-->
                            <div class="row justify-content-center pl-4 p-1 px-md-0">
                                <div class="col-md-12 ">
                                    <br>
                                    <table class="table " id="jqueryTabl">
                                        <thead>
                                        <th>Ser.no</th>
                                        <th>Name</th>
                                        <th>Number of days</th>
                                        <th>Miles/Day</th>
                                        <th>Cost</th>
                                        <th>Refunded amount</th>
                                        <th>Total Cost</th>
                                        </thead>
                                        <tbody>
                                        <div id="driverEarning">
                                        </div>
                                        </tbody>
                                    </table>
                                </div>
                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn   btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
{{-- Styles Section --}}


{{-- Scripts Section --}}
@section('scripts')

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- vendors --}}
    <script>


        $('.stat1').on('change', function () {
            if (this.value == 4) {
                $('.comment').removeClass('d-none');
            } else {
                $('.comment').addClass('d-none');
            }
        });
        // alert(  globalPublicPath);
        var globalPublicPath = globalPublicPath + '/images/driver-documents';

        function accept_sticker() {
            $('#loader').removeClass('d-none');
            var driver_id = $('#drvr_id').val();
            var camp_id = $('#campe_id').val();
            var data = {
                'driver_id': driver_id,
                'campaign_id': camp_id,
            };
            $.ajax({
                url: '{{route('driverApproveSticker')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {
                        toastr.success('Updated successfully', 'Action successfull');
                        $('#loader1 ').addClass('d-none');
                        $('#accept_sticker').hide('2000');
                        $('#reject_sticker').hide('2000');
                        $('#loader').addClass('d-none');
                        $('#reject_sticker_' + driver_id).css('display', 'none');
                        $('#accept_sticker_' + driver_id).css('display', 'none');
                    } else {
                        toastr.error('Try later', 'Error occured');
                    }
                },

            })
        }

        function accept_mile() {
            var driver_id = $('#drvr_id1').val();
            var camp_id = $('#campe_id1').val();
            // console.log(driver_id);
            $('#loader').removeClass('d-none');
            var data = {
                'driver_id': driver_id,
                'campaign_id': camp_id,
            };
            $.ajax({
                url: '{{route('driverApproveMiles')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {
                        toastr.success('Updated successfully', 'Action successfull');
                        $('#loader').addClass('d-none');
                        $('#reject_' + driver_id).css('display', 'none');
                        $('#accept_' + driver_id).css('display', 'none');
                        $('#btn_mileage_' + driver_id).hide();
                        $('#' + driver_id).text('Assigned');
                        $('#status_' + driver_id).text('Assigned');

                    } else {
                        toastr.error('Try later', 'Error occured');
                    }
                },

            })
        }

        function reject_sticker() {

            $('.reject_sticker_field').removeClass('d-none')
            $(".modal-body").scrollTop(0);
            // var driver_id = $('#drvr_id').val();
            // var camp_id = $('#campe_id').val();
            // var data = {
            //     'driver_id': driver_id,
            //     'campaign_id': camp_id,
            // };
        }

        function reject_miles() {
            $(".modal-body").scrollTop(0);

            $('.reject_feedback_field').removeClass('d-none');

            var driver_id = $('#drvr_id1').val();
            var camp_id = $('#campe_id1').val();
            var data = {
                'driver_id': driver_id,
                'campaign_id': camp_id,
            };
        }

        function sbt_mile_reject() {
            var driver_id = $('#drvr_id1').val();
            var camp_id = $('#campe_id1').val();
            var reject_field = $("#comment_field_" + driver_id).val();
            if (reject_field == '') {
                $('#reject_mile_error').text('This field is required')
                return;
            }
            if (reject_field.length > 100) {
                $('#reject_mile_error').text('please write a short message')
                return;
            }

            $('#loader').removeClass('d-none');
            var data = {
                'driver_id': driver_id,
                'campaign_id': camp_id,
                'comment': reject_field,
            };
            $.ajax({
                url: '{{route('driverRejectMiles')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {
                        toastr.success('Notification send successfully', 'Action successfull');
                        $('#loader').addClass('d-none');

                        $('#reject_' + driver_id).css('display', 'none');
                        $('#accept_' + driver_id).css('display', 'none');
                        $("#comment_field_" + driver_id).css('display', 'block');
                        $("#comment_field_" + driver_id).attr('disabled', 'true')
                        $("#sbt_mile_reject_btn_" + driver_id).css('display', 'none');
                        $("#reject_mile_error").text('');
                        // $('.reject_feedback_field').addClass('d-none');
                        // $('#btn_mileage_'+driver_id).hide();
                    } else {
                        toastr.error('Try later', 'Error occured');
                    }
                }

            })
        }

        function sbt_sticker_reject() {
            var driver_id = $("#drvr_id").val();
            var camp_id = $("#campe_id").val();
            var reject_field = $("#comment_field_sticker_" + driver_id).val();
            if (reject_field == '') {
                $('#reject_sticker_error').text('This field is required')
                return;
            }
            if (reject_field.length > 100) {
                $('#reject_sticker_error').text('please write a short comment')
                return;
            }
            $('#loader').removeClass('d-none');
            var data = {
                'driver_id': driver_id,
                'campaign_id': camp_id,
                'comment': reject_field,
            };
            $.ajax({
                url: '{{route('driverRejectSticker')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {
                        toastr.success('Notification send successfully', 'Action successfull');
                        $('#loader').addClass('d-none');
                        $('#reject_sticker_' + driver_id).css('display', 'none');
                        $('#accept_sticker_' + driver_id).css('display', 'none');
                        $("#comment_field_sticker_" + driver_id).css('display', 'block');
                        $("#comment_field_sticker_" + driver_id).attr('disabled', 'true')
                        $("#sbt_sticker_reject_btn_" + driver_id).css('display', 'none');
                        $('#reject_sticker_error').text('');
                    } else {
                        toastr.error('Try later', 'Error occured');
                    }
                }

            })
        }


        function approveDriver() {
            // console.log('herer');
            $('#loader').removeClass('d-none');
            driver_id = $('#driverr_id').val();
            $('#approvedd').attr('disabled', 'disabled');
            $('#approvedd').css('cursor', 'default');
            camp_id = $('#campp_id').val();
            var data = {
                'campaign_id': camp_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('driverApproveStartDocuments')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result == '1') {
                        console.log(result);
                        $('#approvedd').text('Approved');
                        toastr.success('Driver request approved successfully', 'Request approved');
                        $('#loader').addClass('d-none');
                    } else {
                        toastr.error('Try later', 'Errror occured');
                    }
                },

            })
        }

        function paidDriver() {
            refundable_amount=$("#actual_amount").val()
            giving_amount = $('#giving_amount').val();
            miles = $('#miles_covered').val();
            if (giving_amount == '') {
                $('#giving_amount_error').text('Please fill this field');
                return
            }
            $('#giving_amount_error').text('');
            drv_id = $('#driverrr_id').val();
            camp_id = $('#camppp_id').val();

            Swal.fire({
                title: 'Are you really execute this transaction?',
                text: "Advertisor refundable amount is:" + refundable_amount + " & Giving amount is:" + $("#giving_amount").val(),
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, save!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#loader').removeClass('d-none');
                    var data = {
                        'campaign_id': camp_id,
                        'driver_id': drv_id,
                        'miles': miles,
                        'giving_amount': giving_amount,
                        'refundable_amount': refundable_amount,
                        '_token': $("input[name=_token]").val()
                    }
                    jQuery.ajax({
                        url: '{{route('driverTransaction')}}',
                        type: 'POST',
                        data: data,
                        success: function (result) {
                            if (result == '1') {
                                $('#approvedd1').hide();
                                // $('#comp_btn_'+drv_id).hide();
                                $('#status_' + drv_id).text('Completed');
                                var completed_drivers = parseInt($('#completed_drivers_'+campaign_id).text());
                                ++completed_drivers;
                                $('#completed_drivers_'+campaign_id).text(completed_drivers);

                                var assigneddrivers = parseInt($('#assigned_drivers_'+campaign_id).text());
                                --assigneddrivers;
                                $('#assigned_drivers_'+campaign_id).text(assigneddrivers);
                                $('#completed-rides').text(completed_drivers);
                                $('.completed-rides').text(completed_drivers);
                                var drivers_total = parseFloat($('#drivers_total').text());
                                drivers_total += giving_amount;
                                $('#drivers_total').text(drivers_total);
                                $("#DriverCompleteDocuments").modal('hide');
                                $("#appliedDriversModal").modal('show');
                                Swal.fire(
                                    'Updated!',
                                    'Record updated successfully.',
                                    'success'
                                )

                                $('#loader').addClass('d-none');
                            } else {
                                toastr.error('Try later', 'Errror occured');
                            }
                        }
                    })

                    $('#loader').addClass('d-none');
                } else {
                    $("#DriverCompleteDocuments").modal('show')
                }
            });


        }

        function appliedDrivers(camp_id, drivers, campaign) {
            // console.log('campaign'+JSON.stringify(campaign));
            $(".feedback_text").text('');
            $(".feed").hide();
            $('#jqueryTable tbody').html('');

            // var totalmiles = 0;


            // var text = "<div class='card text-center'><div class='card-body pt-7 pb-1'>";

            const carsZips = [];
            // $.each(campaign.cars, function (index, value) {
            //     requireddrivers = requireddrivers + parseInt(value.no_of_cars);
            //     assigned_drivers += parseInt(value.assigned_drivers);
            //     carsZips.push(value);
            // });

            text = '';
            if (drivers.length < 1) {
                $('#jqueryTable tbody').append("<tr><td colspan='5' class='text-center'>Sorry. No driver appiled</td></tr>");
                $("#appliedDriversModal").modal();
            } else {
                var addClass = '';

                var mail = '';
                var assigned = 0;
                var completed = 0;
                var drivers_total = 0;
                $('#applieddrivers').html(drivers.length);
                $('#camp_id2').html(camp_id);
                let sidebarUpdateId = 1;
                $('.Campainge-Details-others').html('');
                $(".Campainge-Details-list").html('');
                    var drivers_string = '';
                    // drivers_string += "<tr><td colspan='2' ></td><td colspan='4' class='  pb-1 card-body  cursor-pointer w-100% ' onclick=showdetails(" + cars.id + ")><b>State:" + cars.car_location_data.state.state + "&nbsp City:" + cars.car_location_data.city.primary_city + "&nbsp Zips:" + cars.zip + "&nbsp Drivers required:" + cars.no_of_cars + "&nbsp Completed: <span class='cmp_" + cars.id + "'>N/A</span></b></td><td >";
                    // var CampaingeSideList = `<a class="list-group-item list-group-item-action " id="list-home-list${sidebarUpdateId}" data-toggle="list" href="#list-home${sidebarUpdateId}" role="tab" aria-controls="home">State: 11 City:  111</a>`
                    // $(".Campainge-Details-list").append(CampaingeSideList);

                        $.each(drivers, function (index, value) {
                            var res='';
                                var mail = '';
                                if (value.status != '') {
                                    if (value.courier_status != '' && value.documents_status == null) {
                                        if ( value.availability ==0) {
                                            res=`<a  style="cursor: default" class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon  mr-1" data-toggle="tooltip"  data-original-title="Waiting for availability Confirmation">
                                                <i class="far fa-pause-circle text-warning"></i>`;
                                        }else if(value.availability==2){
                                            res = "<span  class='label label-lg label-light-danger label-inline'>Not Available</span>";
                                        }
                                        if (value.courier_status == 'Courier Received' && value.documents_status == '') {
                                            res += "<span class=\"btn btn-sm btn-primary btn-icon\"  data-toggle=\"tooltip\" title=" + value.courier_status + "   onclick=\"sendmail5('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.courier_driver_feedback + "','" + value.id + "')\"  ><i class=\"fa fa-envelope-open-text\"></i></span>\n";
                                        }
                                        if (value.courier_status == 'Courier not received') {
                                            res += "<span class=\"btn btn-sm btn-primary btn-icon\"  data-toggle=\"tooltip\" title=" + value.courier_status + " onclick=\"sendmail15('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.courier_status + "','" + value.courier_reject_message + "','" + value.courier_rejected_image + "','" + value.id + "')\"  ><i class=\"fa fa-envelope-open-text\"></i></span>\n";
                                        }

                                    } else if (value.documents_status == null) {
                                        res = "<span  class='label label-lg label-light-success label-inline'> " + value.status + "</span>";
                                    }
                                    if (value.availability == '1') {
                                        res+=`<a  style="cursor: default" class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon  mr-1" data-toggle="tooltip"  data-original-title="Driver Available">
                                                <i class="far fa-check-circle text-success"></i></a>`;
                                        if(value.address_acknowledged =='1' ) {
                                            if(value.address_acknowledged!=1) {
                                                res += `<a style="cursor: default" class=" btn btn-sm btn-default btn-text-success btn-hover-info btn-icon  mr-1" data-toggle="tooltip" title="" data-original-title="Address Confirmed" >
                                                <i class="far fa-envelope text-success"></i></a>`
                                            }
                                            if(value.courier_status=='Waits for courier received confirmation')
                                            res += "<span class=\"btn btn-sm btn-primary btn-icon\"  data-toggle=\"tooltip\" title='Waits for courier received confirmation'   onclick=\"sendmail24('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.courier_driver_feedback + "','" + value.id + "')\"  ><i class=\"fa fa-envelope-open-text\"></i></span>";

                                        }
                                        if(value.courier_status=='Waiting for address confirmation'){
                                            res+= `<a class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer mr-1" data-toggle="tooltip" title="" data-original-title="${value.courier_status}"
                                               >
                                                <i class="far fa-envelope"></i></a>`
                                        }else if(value.courier_status=='Address resend request'){
                                            res+= `&nbsp<a class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer mr-1" data-toggle="tooltip" title="" data-original-title="${value.courier_status}"
                                                >
                                                <i class="far fa-envelope"></i></a>`
                                            mail = "<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"sendmail3('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.courier_driver_feedback + "','" + value.id + "')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>Resend address</button>";

                                        }else if(value.courier_status=='Address Acknowledged'){
                                            res+= `<a class=" btn btn-sm btn-default  btn-text-primary btn-hover-warning btn-icon cursor-pointer mr-1" data-toggle="tooltip" title="" data-original-title="${value.courier_status}" >
                                                <i class="far fa-envelope text-success"></i></a>`
                                            mail = "&nbsp<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"sendmail21('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.id + "')\" class=' btn py-1 btn-primary ' style='min-width:180px !important;'>Send document sent mail</button>";

                                        }else if(value.address_acknowledged !='1'){
                                            res+= `&nbsp<a class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer mr-1" data-toggle="tooltip" title="" data-original-title="Send address confirmation mail"
                                               onclick='sendmail2("${value.driver_detail.id}","${camp_id}","${value.driver_detail.profile.first_name}","${value.driver_detail.email}","${value.courier_status}")'
                                                id='text_${value.driver_detail.id}'>
                                                <i class="far fa-envelope"></i></a>`
                                        }

                                        if(value.courier_status == 'Courier resend request') {
                                            mail = "<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"sendmail3('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.courier_driver_feedback + "','" + value.id + "')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>Resend address</button>";
                                        }
                                        // mail = "<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"sendmail2('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.courier_status + "')\" class=' btn btn-sm py-1 btn-primary w-100 ' >Send address confirmation mail</button>";
                                    } else if (value.courier_status == 'Mail not sent' && value.availability == '0') {
                                        mail = "<span  class=\"label label-lg label-light-success float-left label-inline\">Waiting for Availability Confirmation</span>";
                                    } else if (value.courier_status == 'Mail not sent' && value.availability == '2') {
                                        mail = "<span  class=\"label label-lg label-light-danger float-left label-inline\">Not Available</span>";
                                    }if (value.courier_status == 'Address Acknowledged' || value.courier_status == 'Courier Received' ) {
                                        if (value.courier_status == 'Address Acknowledged') {
                                            mail = "&nbsp<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"sendmail21('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.id + "')\"  class=' btn py-1 btn-primary ' style='min-width:180px !important;'>Send documents</button>";
                                        } else {
                                            if (value.courier_status == 'Courier Received') {
                                                res += "<span class=\"p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title='Courier Received' onclick=\"sendmail22('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.id + "')\"  ><i class=\"fa fa-envelope-open-text text-success\"></i></span>\n";
                                            }
                                            if (value.documents_status == 'Waiting for video sticker approval') {
                                                mail = "<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"get_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>View sticker details</button>";
                                                // res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\" style='cursor:default' data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_start_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file-video\"></i></span>\n";
                                            } else if (value.documents_status == 'Waiting for starting miles approval') {
                                                mail = "<button id=\"btn_mileage_" + value.driver_detail.id + "\" onclick=\"get_details1('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>View mileage details</button>";
                                                res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_start_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file-video\"></i></span>\n";
                                            } else if (value.documents_status == 'Waiting for miles') {
                                                res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_start_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file-video\"></i></span>\n";
                                            } else if (value.documents_status == 'Waiting for resend miles') {
                                                // res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file-video\"></i></span>\n";
                                                res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_start_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file-video\"></i></span>\n";
                                            } else if (value.documents_status == 'Waiting for resend sticker') {
                                                res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_start_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file-video\"></i></span>\n";
                                            }
                                        }
                                    }
                                }
                            // mail = "<button id=\"btn1_" + value.driver_detail.id + "\" onclick=\"sendmail12('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.courier_driver_feedback + "','" + value.id + "')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>confirmed</button>";
                                if (value.status == 'Applied') {
                                    res = "<a href='#' class='text-center'  onclick=\"statusUpdate('" + camp_id + "','" + value.driver_detail.id + "','" + value.driver_detail.role_id + "','" + value.driver_detail.email + "','" + value.driver_detail.profile.first_name + "','" + campaign.slug + "')\">Approve Request</a>";
                                    addClass = '';
                                }
                                if (value.status == 'Waiting for documents approval') {
                                    res = "<a href='#' class='text-center' title='Approve campaign start document' data-dismiss='modal' onclick=\"documentsModel('" + camp_id + "','" + value.driver_detail.id + "')\">View Documents</a>";
                                    addClass = '';
                                }
                                if (value.status == 'Finished' || value.status == 'Completed') {

                                    res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_start_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file text-success\"></i></span>\n";
                                    if(value.status == 'Completed'){

                                        res+= `&nbsp<a class=" btn btn-sm btn-default btn-text-primary btn-hover-warning btn-icon cursor-pointer mr-1" data-toggle="tooltip" title="" data-original-title="${value.status}"
                                               onclick='documentsCompleteModel("${camp_id}","${value.driver_detail.id}","${value.driver_detail.profile.first_name}")'
                                                id='text_${value.driver_detail.id}'>
                                                <i class="la la-car text-success"></i></a>`

                                    }else {
                                        res += "&nbsp<a href='#' id=\"comp_btn_" + value.driver_detail.id + "\" class='text-center'  title='View completed document' data-dismiss='modal' onclick=\"documentsCompleteModel('" + camp_id + "','" + value.driver_detail.id + "','" + value.driver_detail.profile.first_name + "')\">Documents</a>";
                                    }addClass = '';
                                    if (value.status == 'Completed') {
                                        if (value.amount_gives != null) {
                                            drivers_total += parseFloat(value.amount_gives);
                                        }
                                    }
                                }
                                if (value.status == 'Assigned') {
                                    res += "&nbsp<span class=\" p-1 btn btn-sm btn-default btn-icon\"  data-toggle=\"tooltip\" title=\"" + value.documents_status + "\" onclick=\"get_start_details('" + value.driver_detail.id + "','" + camp_id + "','" + value.driver_detail.profile.first_name + "','" + value.driver_detail.email + "','" + value.documents_status + "')\"><i class=\"fa fa-file text-success \"></i></span>\n";
                                    assigned++;
                                }
                                // var driver_state = value.driver_detail.enrolment.state.state + "/" + value.driver_detail.enrolment.city.primary_city + "/" + value.driver_detail.enrolment.car_drive_zip;
                         // console.log('campaignscount',value.driver_detail.campaignscount)
                            value.driver_detail.campaignscount.length
                            var appliied_count=0;
                            $.each(value.driver_detail.campaignscount, function (index, value1) {
                                if(value1.id!=value.id){
                                    ++appliied_count
                                }
                            });
                                drivers_string += "<tr class='border-bottom " + value.id + "' ><td>" + value.driver_detail.profile.first_name + "</td><td >" + value.driver_detail.email + "</td><td >" +value.applied_zip + "</td><td class='text-center'><a target='_blank' href=\"./driver-campaigns/" + value.driver_detail.id + "\">" + appliied_count + "</a></td><td  id=\"status_" + value.driver_detail.id + "\">" + value.status + "</td><td  style=\"min-width:250px;\" class='py-2'><span id=\"" + value.driver_detail.id + "\" class=\"" + addClass + "\" >" + res + "</span></td><td >" + mail + "</td></tr>";
                        });

                    $('#jqueryTable').append(drivers_string);

//                     var capainsDetails = ` <div class="tab-pane fade" id="list-home${sidebarUpdateId}" role="tabpanel" aria-labelledby="list-home-list${sidebarUpdateId}">
//                     <div class="row">
//                     <div class="col-6">
//                     <ul>
//                     <li><b>State</b>: 1 </li>
//                     <li><b>City</b>:  1</li>
//                     <li><b>Zips</b>: 1</li>
//                     </ul>
//                     </div>
// <div class="col-6">
// <ul>
// <li><b>Drivers required</b>:1 </li>
// <li><b>Completed Drivers</b>: N/A</li>
// </ul>
// </div>
//                     </div>
// <hr>
//                     <div class="row">
//                     <div class="col-12">
//                     <table>
//                     <thead>
//                     <tr>
//                     <th>Name</th>
//                     <th>Email</th>
//                     <th>Zips</th>
//                     <th>Status</th>
//                     <th>Action</th>
//                     </tr>
//                     </thead>
//                     <tbody>
//                     ${drivers_string}
//                     </tbody>
//                     </table>
//                     </div>
//                     </div>
//                            </div>`

                //     $('.Campainge-Details-others').append(capainsDetails);
                //     sidebarUpdateId++
                text += "<h5 class='mb-1  mt-1 text-center '>Amount: " + campaign.total_price + " Drivers cost: "+drivers_total+"&nbsp Drivers required: " + campaign.no_of_cars + " &nbsp Assigned drivers: <span id='assigned_drivers_"+ campaign.id+"'>" + assigned + "<span> &nbsp Completed drivers: <span id='completed_drivers_"+ campaign.id+"'>" + campaign.completed_drivers + "</span></h5>";
                text += "</div> </div>"
                $('.carss').html(text);
                $('#completed-rides').text(campaign.completed_drivers);
                $("#appliedDriversModal").modal();
            }
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        }

        function showdetails(id) {
            $('.' + id).slideToggle();
        }
        {{--function sendmail(driver_id, campaign_id, driver_name, driver_email) {--}}
        {{--    $('#loader').removeClass('d-none');--}}
        {{--    var data = {--}}
        {{--        'campaign_id': campaign_id,--}}
        {{--        'driver_id': driver_id,--}}
        {{--        'driver_name': driver_name,--}}
        {{--        'driver_email': driver_email,--}}
        {{--    };--}}
        {{--    $.ajax({--}}
        {{--        url: '{{route('sendCourierMailDriver')}}',--}}
        {{--        type: "GET",--}}
        {{--        data: data,--}}
        {{--        success: function (result) {--}}
        {{--            $('#loader').addClass('d-none');--}}
        {{--            if (result == 1) {--}}
        {{--                $('#btn_' + driver_id).attr('disabled', 'disable');--}}
        {{--                $('#btn_' + driver_id).removeClass('btn-primary');--}}
        {{--                $('#btn_' + driver_id).text('Mail Sent');--}}
        {{--                $('#btn_' + driver_id).addClass('btn-danger ');--}}
        {{--                $('#btn_' + driver_id).css('cursor', 'default');--}}
        {{--                // $('#status_'+driver_id).text('Waiting for documents');--}}
        {{--                toastr.success('Mail sends successfully', 'Mail sent');--}}
        {{--            } else {--}}
        {{--                toastr.error('Please contact to support', 'Error Occured');--}}
        {{--            }--}}
        {{--        }--}}
        {{--    })--}}
        {{--}--}}

        function sendmail2(driver_id, campaign_id, driver_name, driver_email, status) {
            $("#courier_company").val('')
            $("#courier_url").val('')
            $("#tracking_id").val('')
            $("#mail_address").val('')
            $(".cor_error").text('')
            $("#driver_id").val(driver_id)
            $("#campaign_id").val(campaign_id)
            $("#driv_name").text(driver_name)
            $("#appliedDriversModal").modal('hide');
            $('#courier').modal('show');
            // $('#' + driver_id).html("<span style='min-width:200px' class='label label-lg label-light-success label-inline'>" + status + "</span>");
        }
        function sendmail21(driver_id, car_id, driver_name,applied_id) {
        var data = {
            'applied_id': applied_id,
        };
        $("#driv_name").text(driver_name)
        $('#loader').removeClass('d-none');
        $.ajax({
            url: '{{route('getCourierDetails')}}',
            type: "GET",
            data: data,
            success: function (result) {
                if (result != 0) {
                    $("#courier_companyy").val('')
                    $("#courier_urll").val('')
                    $("#tracking_idd").val('')
                    $("#mail_addresss").val(result.address)
                    $(".cor_error").text('')
                    $("#driverr_id").val(driver_id)
                    $("#campaignn_id").val(car_id)
                    $("#drivv_name").text(driver_name)
                    $('#courier18').modal('show');
                    $('#loader').addClass('d-none');
                    $("#sendmail31_btn").addClass('sbmite_'+driver_id);
                    $("#appliedDriversModal").modal('hide');

                } }})
        }
       function sendmail22(driver_id, car_id, driver_name,applied_id) {
        var data = {
            'applied_id': applied_id,
        };
        $("#driv_name").text(driver_name)
        $('#loader').removeClass('d-none');
        $.ajax({
            url: '{{route('getCourierDetails')}}',
            type: "GET",
            data: data,
            success: function (result) {
                if (result != 0) {
                    $(".couriercompany").val('')
                    $(".trackinggg").val('')
                    $(".courierurl").val('')
                    $(".mailaddress").val('')
                    $(".mailaddress").val(result.address)
                    $(".mailaddress").attr('disabled','disbale')
                    $(".trackinggg").val(result.tracking_id)
                    $(".trackinggg").attr('disabled','disbale')
                    $(".courierurl").val(result.url)
                    $(".courierurl").attr('disabled','disbale')
                    $(".couriercompany").val(result.courier_company)
                    $(".couriercompany").attr('disabled','disbale')
                    $(".cor_error").text('')
                    $(".drivvvnamee").text(driver_name)
                    $('#courier20').modal('show');
                    $('#loader').addClass('d-none');

                    $("#appliedDriversModal").modal('hide');

                } }})
        }

        function sendmail13(driver_id, campaign_id, driver_name, driver_email) {
            $("#courier_company").val('')
            $("#courier_url").val('')
            $("#tracking_id").val('')
            $("#mail_address").val('')
            $(".cor_error").text('')
            $("#driver_id").val(driver_id)
            $("#campaign_id").val(campaign_id)
            $("#driv_name").text(driver_name)
            $("#appliedDriversModal").modal('hide');
            $('#courier').modal('show');
        }
        function bck() {
            driver_id = $('#driver_id').val();
            camp_id = $('#campaign_id').val();
            name = $("#driv_name").text();
            txt = "<button id=\"btn_" + driver_id + "\" onclick=\"sendmail2('" + driver_id + "','" + camp_id + "','" + name + "','" + driver_id + "')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>send courier mail</button>";
            // txt= "<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"sendmail2('"$('#driver_id').val() + "','" +$('#campaign_id').val() + "','" + $("#driv_name").text() + "','')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>send courier mail</button>";
            $('#' + driver_id).html(txt);
        }

        function bck1() {
            driver_id = $('#driver_id').val();
            camp_id = $('#campaign_id').val();
            name = $("#driv_name").text();
            // txt = "<button id=\"btn_" + driver_id + "\" onclick=\"sendmail2('" + driver_id + "','" + camp_id + "','" + name + "','" + driver_id + "')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>send courier mail</button>";
            // txt= "<button id=\"btn_" + value.driver_detail.id + "\" onclick=\"sendmail2('"$('#driver_id').val() + "','" +$('#campaign_id').val() + "','" + $("#driv_name").text() + "','')\" class=' btn  py-1 btn-primary ' style='min-width:180px !important;'>send courier mail</button>";
          // $('#'+driver_id).html(txt);
        }

        function get_details(driver_id, campaign_id, driver_name, driver_email, documents_status) {

            $('#reject_sticker_error').text('');
            $('.accept_sticker_btn').attr("id", 'reject_sticker_' + driver_id);
            $('.reject_sticker_btn').attr("id", 'accept_sticker_' + driver_id);
            $('.sbt_sticker_reject_btn').attr("id", 'sbt_sticker_reject_btn_' + driver_id);
            $('.reject_sticker_field').addClass('d-none');
            $('.reject_field_inputtt').val('');
            $('.reject_field_inputtt').attr('id', "comment_field_sticker_" + driver_id);
            if (documents_status == 'Waiting for miles') {
                $("#reject_sticker_" + driver_id).css('display', 'none');
                $("#accept_sticker_" + driver_id).css('display', 'none');
            }
            var data = {
                'campaign_id': campaign_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('getStickerDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    console.log(result);
                    if (result != 0) {
                        $("#sticker_img").attr('src', globalPublicPath + '/' + result.sticker_picture)
                        $("#sticker_video").attr('src', globalPublicPath + '/' + result.sticker_video)
                        $("#drvr_id").val(result.user_id)
                        $("#campe_id").val(result.car_id)
                        if (result.applied_campaign.documents_status == 'Waiting for video sticker approval') {
                            $('#reject_sticker_' + driver_id).css('display', 'block')
                            $('#accept_sticker_' + driver_id).css('display', 'block')
                            $('#sbt_sticker_reject_btn_' + driver_id).css('display', 'block')
                            $('#comment_field_sticker_' + driver_id).removeAttr('disabled')
                        } else if (result.applied_campaign.documents_status == 'Waiting for resend sticker') {
                            $('#reject_sticker_' + driver_id).css('display', 'none')
                            $('#accept_sticker_' + driver_id).css('display', 'none')
                        }

                        $('#get_details_modal').modal('show');
                    } else {
                        toastr.error('No record found', 'Error Occured');
                    }
                }
            })
        }

        function get_start_details(driver_id, campaign_id, driver_name, driver_email, documents_status) {
            $("#miles-comment").html('');
            $("#miles-comment").html('');
            var data = {
                'campaign_id': campaign_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('getStickerDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != 0) {
                        $("#sticker_img1").attr('src', globalPublicPath + '/' + result.sticker_picture)
                        $("#sticker_video1").attr('src', globalPublicPath + '/' + result.sticker_video)
                        if (result.campain_start_date == null) {
                            $("#strt_date").text('Not available')
                        } else {
                            $("#strt_date").text(result.campain_start_date)
                        }
                        if (result.mileage_picture == null) {
                            $(".mile_pic").addClass('d-none')
                        } else {
                            $(".mile_pic").removeClass('d-none')
                            $("#mile_pic").attr('src', globalPublicPath + '/' + result.mileage_picture)
                        }
                        if (result.starting_miles == null) {
                            $("#strt_miles").text('Not available');
                        } else {
                            $("#strt_miles").text(result.starting_miles)
                        }
                        if (result.applied_campaign.documents_status != null) {
                            $("#stats").text(result.applied_campaign.documents_status);
                        }
                        if (result.applied_campaign.documents_feedback != null || result.applied_campaign.document_sticker_feedback != null) {
                            var milesfeeedback = '';
                            var stickerfeeedback = '';
                            if (result.applied_campaign.documents_feedback == null) {
                                milesfeeedback = 'Not available';
                            } else {
                                milesfeeedback = result.applied_campaign.documents_feedback
                            }
                            if (result.applied_campaign.document_sticker_feedback == null) {
                                stickerfeeedback = 'Not available';
                            } else {
                                stickerfeeedback = result.applied_campaign.document_sticker_feedback
                            }
                            html = "<table\n" +
                                " class=\"table main-table table-head-custom table-head-bg table-borderless table-vertical-center table-bordered\">\n" +
                                "   <tr>\n" +
                                " <td>Sticker Feedback</td>\n" +
                                " <td >" + stickerfeeedback + "</td>\n" +
                                "  <td>Miles Feedback</td>\n" +
                                " <td >" + milesfeeedback + "</td>\n" +
                                " </tr>\n" +
                                "</table>"
                            $("#miles-comment").html(html)
                        }
                        $("#drvwr_name").text(driver_name)
                        $("#campee_id").text(result.campaign_id)
                        $('#get_details_modal1').modal('show');
                    } else {
                        toastr.error('No record found', 'Error Occured');
                    }
                }
            })
        }

        function get_details1(driver_id, campaign_id, driver_name, driver_email, documents_status) {
            $('#reject_mile_error').text('');
            $('.reject_mile_btn').attr("id", 'reject_' + driver_id);
            $('.accept_mile_btn').attr("id", 'accept_' + driver_id);
            $('.sbt_mile_reject_btn').attr("id", 'sbt_mile_reject_btn_' + driver_id);
            $('.reject_feedback_field').addClass('d-none');
            $('.reject_field_inputt').val('');
            $('.reject_field_inputt').attr('id', "comment_field_" + driver_id);
            // if(documents_status=='Waiting for miles'){
            //     $("#accept_sticker").hide();
            //     $("#reject_sticker").hide();
            // }
            var data = {
                'campaign_id': campaign_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('getStickerDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {

                    if (result != 0) {
                        $("#mileage_picture").attr('src', globalPublicPath + '/' + result.mileage_picture)
                        $("#drvr_id1").val(result.user_id)
                        $("#campe_id1").val(result.car_id)
                        $("#starting_miles").val(result.starting_miles)
                        console.log('status', result.applied_campaign.documents_status);
                        if (result.applied_campaign.documents_status == 'Waiting for starting miles approval') {
                            $('#reject_' + driver_id).css('display', 'block');
                            $('#accept_' + driver_id).css('display', 'block');
                            $('#sbt_mile_reject_btn_' + driver_id).css("display", "block");
                            $('#comment_field_' + driver_id).removeAttr('readonly');
                        } else if (result.applied_campaign.documents_status == 'Waiting for resend miles') {
                            // alert();
                            $('#reject_' + driver_id).css('display', 'none');
                            $('#accept_' + driver_id).css('display', 'none');
                            $('.reject_feedback_field').removeClass('d-none');
                            $('#comment_field_' + driver_id).attr('id', "comment_field_" + driver_id);
                            $('#comment_field_' + driver_id).val(result.applied_campaign.documents_feedback);
                            $('#comment_field_' + driver_id).attr('readonly', 'true');
                            $('#sbt_mile_reject_btn_' + driver_id).css("display", "none");
                        }

                        $('#mile_details_modal').modal('show');
                    } else {
                        toastr.error('No record found', 'Error Occured');
                    }
                }
            })
        }

        function sendmail4(driver_id, campaign_id, driver_name, driver_email, coment, id) {
            if (coment == '') {
                coment = 'N/A'
            }
            $(".feedback_text").text(coment);
            $(".feed").show();
            var data = {
                'applied_id': id,
            };
            $("#driv_name").text(driver_name)
            $('#loader').removeClass('d-none');
            $.ajax({
                url: '{{route('getCourierDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != 0) {
                        $("#courier_company").val(result.courier_company)
                        $("#courier_company").attr('disabled', 'true')
                        $("#courier_url").val(result.courier_company)
                        $("#courier_url").attr('disabled', 'true')
                        $("#tracking_id").val(result.tracking_id)
                        $("#tracking_id").attr('disabled', 'true')
                        $("#mail_address").val(result.address)
                        $("#mail_address").attr('disabled', 'true')
                        $('#loader').addClass('d-none');
                        $("#appliedDriversModal").modal('hide');
                        $('#courier').modal('show');
                        $('#sendmail1_btn').hide();
                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                        $('#loader').addClass('d-none');

                    }
                }
            })
        }
        function sendmail24(driver_id, campaign_id, driver_name, driver_email, coment, id) {
            if (coment == '') {
                coment = 'N/A'
            }
            $(".feedback_text").text(coment);
            $(".feed").show();
            var data = {
                'applied_id': id,
            };
            $("#driv_name").text(driver_name)
            $('#loader').removeClass('d-none');
            $.ajax({
                url: '{{route('getCourierDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != 0) {
                        $("#drivvv_name").val(driver_name)
                        $("#courier_companyye").val(result.courier_company)
                        $("#courier_companyye").attr('disabled', 'true')
                        $("#courier_urlle").val(result.url)
                        $("#courier_urlle").attr('disabled', 'true')
                        $("#tracking_idde").val(result.tracking_id)
                        $("#tracking_idde").attr('disabled', 'true')
                        $("#mail_addressse").val(result.address)
                        $("#mail_addressse").attr('disabled', 'true')
                        $('#loader').addClass('d-none');
                        $("#appliedDriversModal").modal('hide');
                        $('#courier19').modal('show');
                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                        $('#loader').addClass('d-none');

                    }
                }
            })
        }

        function sendmail15(driver_id, campaign_id, driver_name, courier_status, rejected_coment, rejected_img, id) {
            $('.not_received_pic_hide').removeClass('d-none');
            ;
            $('.feed_hide').removeClass('d-none');
            var globalPublicPath2 = '<?php echo e(url('/') . env('APP_URL_LIVE_SEGMENT')); ?>';
            var globalPublicPath2 = globalPublicPath2 + '/images/driver/courierreject';
            var img = globalPublicPath2 + '/' + rejected_img;
            // alert(rejected_coment);
            if (rejected_coment === 'null') {
                $('.not_received_pic_hide').addClass('d-none');
                $('.feed_hide').addClass('d-none');
            } else {
                $(".feedback_text").text(rejected_coment);
                // $('.not_received_pic').html("<img src='"+img+"' height='250px'/>");
                var re = /(?:\.([^.]+))?$/;
                if (re.exec(rejected_img)[1] == 'pdf') {
                    $('.not_received_pic').html("<a target='_blank' href='" + globalPublicPath2 + '/' + rejected_img + "' ><img src='" + globalPublicPath2 + '/courrier_reject.png' + "' height='150px'/></a>");
                } else {
                    var img = globalPublicPath2 + '/' + rejected_img;
                    $('.not_received_pic').html("<img src='" + img + "' height='250px'/>");
                }
            }


            $(".feed").show();
            var data = {
                'applied_id': id,
            };
            $("#driv_namee").text(driver_name)
            $('#loader').removeClass('d-none');
            $.ajax({
                url: '{{route('getCourierDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != 0) {
                        console.log('result.courier_company'+result.courier_company);
                        $("#courier_companyy").val(result.courier_company)
                        $("#courier_companyy").attr('disabled', 'true')
                        $("#courier_urll").val(result.courier_company)
                        $("#courier_urll").attr('disabled', 'true')
                        $("#tracking_idd").val(result.tracking_id)
                        $("#tracking_idd").attr('disabled', 'true')
                        $("#mail_addresss").val(result.address)
                        $("#mail_addresss").attr('disabled', 'true')
                        $('#loader').addClass('d-none');
                        $('.cour_status').html("<small class='badge badge-pill badge-light ml-2'>" + courier_status + "</small>");

                        $("#appliedDriversModal").modal('hide');
                        $('#courier_rejectedModal').modal('show');
                        $('#sendmail1_btnn').hide();


                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                        $('#loader').addClass('d-none');

                    }
                }
            })
        }

        function sendmail5(driver_id, campaign_id, driver_name, driver_email, coment, id) {
            $(".feedback_text").text(coment);
            $(".feed").show();
            // alert(id)
            var data = {
                'applied_id': id,
            };
            $("#driv_name").text(driver_name)
            $('#loader').removeClass('d-none');
            $.ajax({
                url: '{{route('getCourierDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != 0) {
                        $("#courier_company").val(result.courier_company)
                        $("#courier_company").attr('disabled', 'true')
                        $("#courier_url").val(result.courier_company)
                        $("#courier_url").attr('disabled', 'true')
                        $("#tracking_id").val(result.tracking_id)
                        $("#tracking_id").attr('disabled', 'true')
                        $("#mail_address").val(result.address)
                        $("#mail_address").attr('disabled', 'true')
                        $('#loader').addClass('d-none');
                        $("#appliedDriversModal").modal('hide');
                        $('#courier').modal('show');
                        $('#sendmail1_btn').hide();

                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                        $('#loader').addClass('d-none');

                    }
                }
            })
        }

        function sendmail3(driver_id, campaign_id, driver_name, driver_email, coment, id) {
            // alert(driver_email);
            // alert(coment)
            $(".feedback_text").text(coment);
            $(".feed").show();
            // alert(id)
            var data = {
                'applied_id': id,
            };
            $("#driv_name").text(driver_name)
            $('#loader').removeClass('d-none');
            $.ajax({
                url: '{{route('getCourierDetails')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != 0) {
                        $("#courier_company").val(result.courier_company)
                        $("#courier_company").removeAttr('disabled')
                        $("#courier_url").val(result.courier_company)
                        $("#courier_url").removeAttr('disabled')
                        $("#tracking_id").val(result.tracking_id)
                        $("#tracking_id").removeAttr('disabled')
                        $("#mail_address").val(result.address)
                        $("#mail_address").removeAttr('disabled')
                        $('#loader').addClass('d-none');
                        $("#appliedDriversModal").modal('hide');
                        $('#courier').modal('show');
                        $('#sendmail1_btn').show();

                        $('#driver_id').val(driver_id);
                        $('#campaign_id').val(campaign_id);


                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                        $('#loader').addClass('d-none');
                    }
                }
            })

        }
        function backk(){
            $('#appliedDriversModal').modal();
        }

        function sendmail1() {
            var mail_address = $('#mail_address').val();
            // var courier_url = $('#courier_url').val();
            // var courier_company = $('#courier_company').val();
            var driver_id = $('#driver_id').val();
            var campaign_id = $('#campaign_id').val();
            // var tracking_id = $('#tracking_id').val();
            if (mail_address == '') {
                $('.cor_error').text('Please fill mailing address')
                return;
            }
            // if (tracking_id == '') {
            //     $('.cor_error').text('Please fill  tracking id')
            //     return;
            // }
            // if (courier_url == '') {
            //     $('.cor_error').text('Please fill url field')
            //     return;
            // }
            // if (courier_company === '') {
            //     $('.cor_error').text('Please fill courier company field')
            //     return;
            // }
            $('.cor_error').text();
            $('#loader').removeClass('d-none');
            var data = {
                'car_id': campaign_id,
                'driver_id': driver_id,
                'mail_address': mail_address,
            };
            $.ajax({
                url: '{{route('sendCourierDetailsMail')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    $('#loader').addClass('d-none');
                    // $('#' + driver_id).html("<span style='min-width:200px' class='label label-lg label-light-success label-inline'>Mail sent</span>");
                    $('#text_'+driver_id).attr('data-original-title','Waiting for address confirmation');
                    $("#appliedDriversModal").modal('show');
                    $('#courier').modal('hide');


                    $('#btn_' + driver_id).hide();
                    if (result == 1) {

                        toastr.success('Address confirmation Mail send successfully', 'Mail sent');
                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                    }
                }
            })
        }
        function sendmail31() {
            var mail_address = $('#mail_addresss').val();
            var courier_url = $('#courier_urll').val();
            var courier_company = $('#courier_companyy').val();
            var driver_id = $('#driverr_id').val();
            var campaign_id = $('#campaignn_id').val();
            var tracking_id = $('#tracking_idd').val();
            if (mail_address == '') {
                $('.cor_error').text('Please fill mailing address')
                return;
            }
            if (tracking_id == '') {
                $('.cor_error').text('Please fill tracking id')
                return;
            }
            if (courier_url == '') {
                $('.cor_error').text('Please fill url field')
                return;
            }
            if (courier_company === '') {
                $('.cor_error').text('Please fill courier company field')
                return;
            }
            $('.cor_error').text();

            $('#loader').removeClass('d-none');
            var data = {
                'car_id': campaign_id,
                'driver_id': driver_id,
                'mail_address': mail_address,
                'courier_company': courier_company,
                'courier_url': courier_url,
                'tracking_id':tracking_id,
            };
            $.ajax({
                url: '{{route('sendCourierDetailsMail2')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    $('#loader').addClass('d-none');
                    // $('#text_'+driver_id).attr('data-original-title','Waiting for COURIER confirmation');
                    $("#appliedDriversModal").modal('show');
                    $('#btn_' + driver_id).hide();
                    $('.sbmite_' + driver_id).addClass('d-none');
                    $('#courier18').modal('hide');

                    if (result == 1) {
                        toastr.success('Action successfull');
                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                    }
                }
            })
        }

        function documentsModel(camp_id, driver_id) {
            $('#loader').removeClass('d-none');
            var data = {
                'campaign_id': camp_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('getDriverDocuments')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != '0') {

                        $('#driverr_id').val(driver_id);
                        $('#campp_id').val(camp_id);
                        sticker_pic = globalPublicPath + '/' + result.sticker_picture;
                        $('#sticker_pic').attr('src', sticker_pic);
                        mileage_pic = globalPublicPath + '/' + result.mileage_picture;
                        $('#mileage_pic').attr('src', mileage_pic);
                        $('#miles_no').text(result.starting_miles);

                        // toastr.success('Mail  sends successfully', 'Mail sent');
                    } else {
                        toastr.error('No data exist against this driver', 'No data found');
                    }
                },

            })
            $('#loader').addClass('d-none');
            $("#DriverDocuments").modal();

        }

        function documentsCompleteModel(camp_id, driver_id, name) {

            $('#loader').removeClass('d-none');
            $('#actual_amount').val('');
            $('#giving_amount').val('');
            var data = {
                'campaign_id': camp_id,
                'driver_id': driver_id,
            };
            $.ajax({
                url: '{{route('getDriverCompletedDocuments')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    if (result != '0'){

                        // console.log('result',result);
                        $('#driverrr_id').val(driver_id);
                        // $('#cmpp').val(camp_id);
                        $('#drriver').text(name);
                        $('#camppp_id').val(camp_id);
                        $('#camp_id12').text(camp_id);
                        // sticker_pic='/images/driver-documents/'+result['start'].sticker_picture;
                        sticker_pic = "{{url('/images/driver-documents/')}}/" + result['start'].sticker_picture;
                        $('#start_sticker_pic').attr('src', sticker_pic);
                        mileage_pic = "{{url('/images/driver-documents/')}}/" + result['start'].mileage_picture;
                        $('#start_mileage_pic').attr('src', mileage_pic);
                        $('#start_miles_no').text(result['start'].starting_miles);
                        $('#start_date').text(result['start'].campain_start_date);

                        sticker_pic = "{{url('/images/driver-documents/')}}/" + result['complete'].sticker_picture;
                        $('#completed_sticker_pic').attr('src', sticker_pic);
                        mileage_pic = "{{url('/images/driver-documents/')}}/" + result['complete'].mileage_picture;
                        $('#completed_mileage_pic').attr('src', mileage_pic);
                        $('#completed_miles_no').text(result['complete'].mileage);
                        $('#completed_date').text(result['complete'].campaign_finish_date);
                        $('#miles_covered').val(result['covered_miles']);
                        $('#days_covered').val(result['total_days']);
                        $('#campaign_days').text(result['campaigntotaldays']);
                        if (result['campaign'].status == "Completed") {
                            $('#approvedd1').hide();
                            $('#giving_amount').attr('disabled', 'disable');
                            $('#miles_covered').attr('disabled', 'disable');
                            $('#actual_amount').attr('disabled', 'disable');
                            $('#actual_amount').val(result['advertisorrefundableamount']);
                            if (result['complete'].admin_miles != null) {
                                $('#miles_covered').val(result['complete'].admin_miles);
                            }
                        } else {
                            $('#approvedd1').show();
                            $('#giving_amount').removeAttr('disabled');
                            $('#miles_covered').removeAttr('disabled');
                            $('#actual_amount').removeAttr('disabled', 'disable');
                        }

                        // if (result['campaign'].status=="Completed") {
                        //     toastr.error('Price not available against this driver miles');
                        // }
                        if (result['price']==0 && result['campaign'].status=="Finished") {
                            toastr.error('Price not available against this driver miles');
                        }

                        if (result['price']) {
                            $('#actual-amount').text(result['actual_amountt']);
                            $('#actual_amount').val(result['advertisorrefundableamount']);
                            $('#giving_amount').val((result['price'] * 0.8).toFixed(2));
                        } else {
                            $('#approvedd').attr('disabled', 'disabled');
                        }

                        if (result['campaign'] != null) {
                            if (result['campaign'].amount_gives != null) {
                                $('#giving_amount').val(result['campaign'].amount_gives);
                            }

                            $('#loader').addClass('d-none');
                            $("#DriverCompleteDocuments").modal();

                        }
                    } else {
                        toastr.error('No data exist against this driver', 'No data found');
                    }
                },
            })
            $('#loader').addClass('d-none');

        }

        function statusUpdate(camp_id, driver_id, role_id, driver_email, driver_name,slug) {
            $('#loader').removeClass('d-none');
            var data = {
                'campaign_id': camp_id,
                'driver_id': driver_id,
                'role_id': role_id,
                'driver_email': driver_email,
                'driver_name': driver_name,
                'slug': slug,
            };
            $.ajax({
                url: '{{route('claimStatusUpdate')}}',
                type: "GET",
                data: data,
                success: function (result) {
                    $('#loader').addClass('d-none');
                    if (result != 0) {
                        $('#mail_address').val('');
                        $('#courier_url').val('');
                        $('#courier_company').val('');
                        toastr.success('Mail sends to driver successfully', 'Mail send successfully');
                        // $('#' + driver_id).html('Waiting for availability Confirmation');
                        $('#' + driver_id).html("<span class='label label-lg  label-light-success label-inline p-5'>Waiting for availability Confirmation</span>");
                        $("#driver_id").val(driver_id)
                        $("#campaign_id").val(camp_id)
                        $("#mail_address").val(result.billing_address_personal)
                        $("#driv_name").text(driver_name)
                        // console.log('billl', result.billing_address_personal);
                        $("#appliedDriversModal").modal('show');
                        // $('#courier').modal('show');

                        $('#status_' + driver_id).text('Waiting for documents');
                        // alert(driver_id);

                    } else {
                        toastr.error('Please contact to support', 'Error Occured');
                    }
                }
            })
        }

        function status(status_id, car_id, user_id) {
            $("#prev_value").val(status_id);
            $(".stat1").removeAttr("style");
            $("#camp_id").html('CMP-' + car_id);
            $(".status_error").text('');
            $(".stat1").val(status_id)
            $("#car-id").val(car_id)
            $("#user_id").val(user_id)

            // $("#"+camp_id+ "option[value= '"+status_id+"']").attr('disabled','disbale');
        }
        function bespoke(sticker, car_id, user_id) {
            // console.log(sticker.slogan);
            $("#camp_ided").text(car_id);
            $("#user-idd").val(user_id);
            $("#car-idd").val(sticker.campaign_id);
            $("#slogan").text(sticker.slogan);
            $("#detail").text(sticker.sticker_details);
            $("#contact").text(sticker.contact);
            $("#cmp").text(sticker.detail);
            $('#bespoke').modal();
// alert($("#camp_ide").val());
            // $("#"+camp_id+ "option[value= '"+status_id+"']").attr('disabled','disbale');
        }

        function feedback(camp_id) {
            $("#cmp_id").html('CMP-' + camp_id);
        }

        function viewDetail(campaign) {
            console.log(campaign);
            // $("#cmp_id").html('CMP-'+camp_id);
        }

        function del() {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }

        $(document).on('click', '#toggleSearchForm', function () {
            $('#table-search-form').slideToggle();
        });
        @if (\Session::has('updated'))
        toastr.success('{!! \Session::get('updated') !!}', 'Updated');
        @endif
        @if(isset($_GET['id']))
        toastr.success('Campaign <?php echo $_GET['id'] ?> Created Successfully', 'Campaign Created');

        @endif
        {{--    @if (\Session::has('deleted'))--}}
        {{--        toastr.warning('{!! \Session::get('deleted') !!}', 'User Deleted');--}}
        {{--    @endif--}}




        function driverEarning(compaign_id) {
            var data = {
                "_token": "{{ csrf_token() }}",
                "compaign_id": compaign_id,
            }

            // var compaign_id = compaign_id;
            let url = "{{route('driverEarning')}}";

            $.post(url, data, function (response) {
            }).done(function () {
                console.log(response);
                // alert(response);
            })
        }

        function formsbmt() {
            if ($("#prev_value").val() == $(".stat1").val()) {
                $(".stat1").css("border-color", 'red');
                $(".status_error").text("Status must not be same");
                return;
            } else {
                $(".stat1").css("border-color", 'grey');
                $(".status_error").text("");
            }
            var status = $(".stat1").val();
            if (status == '4') {
                if ($("#comment_field").val() == '') {
                    $("#comment_field").css("border-color", 'red');
                    $(".comment_field_error").text("Please fill this field.");
                    return;
                }
            }

            $('#loader').removeClass('d-none');
            document.getElementById('form-id').submit();
        }
    </script>
@endsection
