{{-- Extends layout --}}
@extends('layout.admin')

{{-- Content --}}
@section('content')
    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
    </style>
    @php
    $check=0;
        if(isset($_GET['state_id'])){
            $check=1;
            $state_id=$_GET['state_id'];
        }else{
            $state_id=0;
        }


        @endphp
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-2 pb-0">
            <div class="card-title m-0">
                <h3 class="card-label">Manage prices
                    <div class="text-muted  font-size-sm"></div>
                </h3>
            </div>
            <div class="card-toolbar">
                <a   href="javascript:;" href="javascript:;" id="toggleSearchForm" class="btn btn-primary font-weight-bolder mr-1">
                <span class="svg-icon svg-icon-md">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                         version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path
                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>Search</a>
                <a   href="javascript:;"  data-toggle="modal"  data-target="#createModal" class="btn btn-primary font-weight-bolder mr-1">
                <span class="svg-icon svg-icon-md">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                         version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path
                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>Set percentage</a>
                <a href="{{route('state.price.export', ['state_id'=>$state_id]) }}"  class="btn btn-primary font-weight-bolder">
                <span class="fa fa-upload">&nbsp
                </span>Export</a>

                <!--end::Button-->
            </div>
        </div>

        <div class="card-body pt-0 pb-0">
            <div class="mt-2 mb-5 mt-lg-5 mb-lg-10" id="table-search-form" style="display: <?php  echo (@$check=='1')?'block':'none'; ?>" >
                {{--<option   <?php  echo (null!=($request->meal_type_id)&&($meal_type->id ==$request->meal_type_id))?'selected':''; ?> value="{{$meal_type->id}}">{{$meal_type->name}}</option>--}}
                <form action="#" id="frm">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-xl-12  ">
                            <div class="row align-items-center">
{{--                                <div class="col-md-3 my-2 my-md-0  offset-1">--}}
{{--                                    <div class="input-icon">--}}
{{--                                        <input type="text" name="search" class="form-control" placeholder="Search..."--}}
{{--                                               value="{{(isset($_GET['search']))?$_GET['search']:''}}"--}}
{{--                                               id="kt_datatable_search_query"/>--}}
{{--                                        <span><i class="flaticon2-search-1 text-muted"></i></span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}


                                <div class="col-md-2 my-2 my-md-0  offset-4">
                                    <div class="d-flex align-items-center">
{{--                                        <label class=" mb-0 d-none d-md-block">State:</label>&nbsp--}}
                                        <select class="form-control" name='state_id' id="kt_datatable_search_type">
                                            <option value="" disabled selected>Choose state </option>
                                            @foreach($states_dropdown_values as $state)
                                                <option value="{{$state->id}}" {{$state_id==$state->id?'selected':''}}>{{$state->state}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 mt-5 mt-lg-0">
                                    <a href="#" onclick="document.getElementById('frm').submit();" class="btn btn-light-primary px-6 font-weight-bold">
                                        Search
                                    </a>
                                    @if($state_id!=0)
                                        <a href="#" onClick="window.location.href='../admin/manage-state-price'"
                                           class="btn btn-light-danger px-6 ml-1 font-weight-bold">
                                            Reset
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <!--begin::Search Form-->
            <!-- HTML Table Begin Here -->
            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">State Id</th>
                            <th style="min-width: 130px" class="">
                                <span class="">Name</span>
                            </th>
                            <th class="fix-table-head" style="min-width: 135px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($states as $state)
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$state->id}}</span>
                              </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$state->state}}</a>
                                    </div>
                                </div>
                            </td>
{{--                            <td class="pl-0 py-8">--}}
{{--                                <div class="d-flex align-items-center">--}}
{{--                                    <div>--}}
{{--                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$state->default_price}}</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </td>--}}

                            <td class="pr-0 ">

                                <a href="javascript:;" onclick="viewDetail({{@$state->priceRanges}},{{@$highpercentage->data}},'{{@$state->state}}');" data-toggle="modal" data-target="#viewDetailModal"  class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="view price ranges">
                                    <i class="la la-eye"></i>
                                </a>
                                <a href="javascript:;" onclick="upd({{@$state}});" data-toggle="modal" data-target=".editModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="Add range">
                                    <i class="la la-plus"></i>
                                </a>
                            </td>

                        </tr>
                        @endforeach

                        </tbody>

                    </table>
                    <nav aria-label="Page navigation example ">
                        <ul class="pagination float-right mt-5">
                            <h6 class="pt-3">Total records:{{$states->total()}}  </h6> &nbsp &nbsp&nbsp  {{$states->links()}}
                        </ul>
                    </nav>
                </div>
                <!--end::Table-->
            </div>
            <!-- HTML Table End Here -->
        </div>
    </div>

    <div class="modal fade" id="createModal"  tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">High Price Percentage</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki k<i-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card" style="border:none;" >
                        <div class="card-body  p-0">
                            <!--begin::Invoice-->
                            <div class="row " >
                                <form id="form-id3" class="form" method="POST" action="{{ route('update.highpercentage') }}" >
                                    @csrf
                                    @if ($errors->any())
                                        <div class="alert alert-danger" style="background-color: #e0afb4; ">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li class="float-left">{{ $error }}</li><br>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                <div class="col-md-12 offset-1 ">

                                    <div class=" row mt-5 mb-5  " >
                                        <div class="col-md-12 ">
                                            <label class="mt-1">High price percentage:</label>
                                        </div>
                                            <div class="col-md-9">
                                            <input type="number" class="form-control" value="{{@$highpercentage->data}}" name="price" id="price" required  min="1"  max="100" >
                                            <input type="hidden" class="form-control" name="name"  value="high_percentage"  >
                                        </div>
{{--                                            <label class="col-xl-4 col-lg-4 col-form-label mt-2">For</label>--}}
{{--                                            <div class="col-lg-8 col-xl-8">--}}
{{--                                                <select class="form-control form-control-lg  mt-2" name="role">--}}
{{--                                                    @foreach($roles as $role )--}}
{{--                                                    <option  value="{{$role->id}}" >{{$role->name}}</option>--}}
{{--                                                    @endforeach--}}
{{--                                                        <option value="0" >All</option>--}}
{{--                                                      </select>--}}
{{--                                            </div>--}}

{{--                                            <label class="col-xl-4 col-lg-4 col-form-label font-weight-bold text-left text-lg-left mt-2">Status</label>--}}
{{--                                            <span></span>--}}
{{--                                            <div class="col-lg-8 col-xl-8 mt-2">--}}
{{--                                                <span class="switch switch-sm">--}}
{{--                                                    <label>--}}
{{--                                                        <input type="checkbox" name="status" checked>--}}
{{--                                                        <span></span>--}}
{{--                                                    </label>--}}
{{--												</span>--}}
{{--                                            </div>--}}


                                    </div>
                                </div>
                                </form>
                                <!--end::Invoice-->
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="document.getElementById('form-id3').submit();"
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Update
                    </a>&nbsp
                    <button type="button"    onClick="window.location.href={{route('priceRanges')}}"  class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>

                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add range against state: <span class="state_name"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card" style="border:none;" >
                        <div class="card-body  p-0">
                            <!--begin::Invoice-->
{{--                            <div class="col-md-2 offset-10 float-right">--}}
{{--                                <a  href="#" onclick="addrangermore();" class="mt-2">Add more</a>--}}
{{--                            </div>--}}
                                <form id="form-2" class="form" method="POST" action="{{ route('update.states.prices')}}" >
                                    @csrf
                                    <input type="hidden" name="state_id" id="state_id" class="state_id">
                                    <input type="hidden" name="status" id="status" value="0">

                                   <div id="addranger-id">
                                       <div id="error" class="text-center row col-12 offset-5 text-danger"></div>
                                       <div class=" row " >
                                           <div class="col-sm-8 offset-1">
                                               <div class="form-group">
                                                   <div class="ion-range-slider">
                                                       <label class="mt-2">Choose Range:</label>
                                                       <input type="hidden" name="range[]" class="kt_slider_a"/>
                                                   </div>
                                               </div><span class="form-text text-success  milage-notification mt-2"></span>

                                           </div>
                                           <div class="col-sm-2 mt-2 ">
                                               <label class="mt-2">Set Price:</label>
                                               <input type="number" class="form-control" required name="range_price[]" step=".01" placeholder="0" min="0" id="range_price">
                                           </div>
                                           <div class="col-sm-1 pt-5">
{{--                                               <i class="fa fa-trash cursor-pointer deletranger mt-5 pt-5" title="Delete "></i>--}}
                                           </div>
                                       </div>
                                   </div>

                                </form>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" onclick="formsubmit();" class="btn btn-light-primary font-weight-bold" >Save
                    </button>
                    <button type="button"  class="btn btn-light-primary font-weight-bold " data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="viewDetailModal" role="dialog" aria-labelledby="exampleModalSizeSm" data-backdrop="static" data-keyboard="false"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title titlee"></h5>
                    <button type="button" class="close on_update_back" data-dismiss="modal"  aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card" style="border:none;" >
                        <div class="card-body  p-0">
                            <!--begin::Invoice-->
                            <div class="col-md-10 offset-1 ">
                                <table class="table">
                                    <thead>
                                    <tr>
{{--                                        <th>Id</th>--}}
                                        <th>Min Range</th>
                                        <th>Max Range</th>
                                        <th>Low Price</th>
                                        <th>High Price</th>
                                        <th>High Price Status </th>
                                         <th>Status</th>
                                    </tr>
                                    </thead>
                                    <tbody id="add_data">
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
{{--                    <button type="button" onclick="document.getElementById('form-2').submit();" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Save--}}
{{--                    </button>--}}
                    <button type="button"  class="btn btn-light-primary font-weight-bold on_update_back" data-dismiss="modal">Back</button>
                </div>
            </div>
        </div>
    </div>











@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- vendors --}}
    <script>
        function formsubmit(){
            var status=$("input[name*='status']").val();
            if(status==0){
                $('#error').text('Please choose correct mileage');
                return;
            }
            var values = $("input[name*='range_price']")
                .map(function(){return $(this).val();}).get();
            var val=0;
            for (var i =0; i < values.length; i++) {
                if(values[i]==''){
                    var val=1;
                }if(values[i]<=0){
                    return $('#error').text('Price must be greater than 0');
                }
            }
            if(val==1){
                $('#error').text('Please fill all fields');
                $('#error').removeClass('text-success');
                $('#error').addClass('text-danger');
                // $('.editModal').modal("show");
            }
            else{
                document.getElementById('form-2').submit();
            }


        }
        function upd(price) {

            $('.state_name').text(price.state);
            $('#error').text('');
            $('.state_id').val(price.id);
        }
        function viewDetail(ranges, highpercentage, state){
            $(".titlee").text(state+' ranges');
            if(ranges.length>0){
                $("#add_data").html('')

                $.each(ranges, function (index, value) {
                    val='';
                    val1=0;
                    var checked= ((value.is_set_high=='1') ? 'checked' : '')
                    var checked1= ((value.status=='1') ? 'checked' : '')
                    var add_class= ((value.status=='0') ? 'bg-light' : '')
                    if(value.is_set_high=='1'){
                        var val= parseFloat(value.price*highpercentage/100)+ parseFloat(value.price);
                         val= val.toFixed(2);
                    }
                    var html="<tr id='"+value.id+"' class='"+add_class+"'><td>"+value.min_range+"</td><td>"+value.max_range+"</td><td >"+value.price+"</td><td id='high_price_"+value.id+"' class='text-danger'>"+val+"</td><td ><div class=\"custom-control custom-switch text-center\"> <input type=\"checkbox\" "+checked+"  class=\"custom-control-input\" id='customSwitch_"+value.id+"'><label class=\"custom-control-label\" for='customSwitch_"+value.id+"' onclick=\"setHighPriceStatus("+value.id+","+value.price+")\"></label></div> <div class=\"switch\"> </div></td><td ><div class=\"custom-control custom-switch text-center\"> <input type=\"checkbox\" "+checked1+"  class=\"custom-control-input\" id='customSwitch1_"+value.id+"'><label class=\"custom-control-label\" for='customSwitch1_"+value.id+"' onclick=statusUpdate("+value.id+")></label></div> <div class='switch'> </div></td></tr>"
                    // var html="<tr id='"+value.id+"' class='"+add_class+"'><td>"+value.min_range+"</td><td>"+value.max_range+"</td><td >"+value.price+"</td><td id='high_price_"+value.id+"' class='text-danger'>"+val+"</td><td ><div class=\"custom-control custom-switch text-center\"> <input type=\"checkbox\" "+checked+"  class=\"custom-control-input\" id='customSwitch_"+value.id+"'><label class=\"custom-control-label\" for='customSwitch_"+value.id+"' onclick=\"setHighPriceStatus("+value.id+","+value.price+")\"></label></div> <div class=\"switch\"> </div></td><td><a href='javascript:;' onclick='del("+value.id+")' class=' btn-sm btn-default btn-text-primary btn-hover-primary btn-icon  'title='Status'><i class='la la-trash'></i></a></td><td ><div class=\"custom-control custom-switch text-center\"> <input type=\"checkbox\" "+checked1+"  class=\"custom-control-input\" id='customSwitch1_"+value.id+"'><label class=\"custom-control-label\" for='customSwitch1_"+value.id+"' onclick=statusUpdate("+value.id+")></label></div> <div class='switch'> </div></td></tr>"
                    $("#add_data").append(html);
                });
            }
            else{
                $("#add_data").html('')
                var html="<tr><td colspan='5' class='text-center'>No Range Exist</td></tr>"
                $export_button_status=0;
                $("#add_data").append(html);
            }
           // console.log(ranges);
        }
            function setHighPriceStatus(id,price){
            $('.on_update_back').attr( 'onClick','window.location.reload()')
            if($('#customSwitch_'+id).is(":checked")){
                is_set_high=0;
                $('#high_price_'+id).text('');
            }else{
                is_set_high=1;
                percentage=$('#price').val();
                $('#high_price_'+id).text(((percentage*price/100)+price).toFixed(2));
            }
            var data = {
                'id': id,
                'is_set_high': is_set_high
            }
            var object = {
                'url':"{{route('setHighPriceStatus')}}",
                'type': 'GET',
                'data': data,
            };
            var result = centralAjax(object);
        if(result=='1'){

            toastr.success('Updated successfully ', 'Action Successfull');
        }
        else{
            toastr.error('Try again ', 'Error occurred');
        }
 }


        function del(id)
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    let url = "{{route('delete.range',':id')}}"
                    url = url.replace(':id', id)
                        var object = {
                            'url':url,
                            'type': 'get'
                        }
                      var result = centralAjax(object);
                    if(result ==1){

                        $('#'+id).html('');

                    Swal.fire(
                        'Deleted!',
                        'Record has been deleted.',
                        'success')
                    }
                }
            });
        }
        function statusUpdate(id)
        {
            $('.on_update_back').attr( 'onClick','window.location.reload()')
            if($('#customSwitch1_'+id).is(":checked")){
            status=0;
                $('#'+id).addClass('bg-light');
        }else{
            status=1;
            $('#'+id).removeClass('bg-light');
        }

                    let url = "{{route('status.update.range',':id')}}"
                    var data = {
                        'status':status
                    }
                    url = url.replace(':id', id)
                        var object = {
                            'url':url,
                            'type': 'get',
                            'data': data
                        }
                      var result = centralAjax(object);
                    if(result!=0){
                        toastr.success('Updated successfully ', 'Updated Successfull');
                    }
                    else{

                    Swal.fire(
                        'Error occured ',
                        'Please contact to supoport',
                        'error')
                    }


        }

            //
            // $('.kt_slider_a').ionRangeSlider({
            //     type: "double",
            //     grid: true,
            //     min: 0,
            //     max:2000,
            //     from: 0,
            //     to: 250,
            //     prefix: "Miles",
            // });

    </script>


<script>

    $('.kt_slider_a').ionRangeSlider({
        from_min:1,
        type: "double",
        grid: true,
        min:1,
        max:3000,
        from: 200,
        to: 400,
        step:1,
        prefix: "Miles",
        drag_interval: true,
    });

    @if (\Session::has('updated'))
    toastr.warning('{!! \Session::get('updated') !!}', 'Updated');
    @endif
    @if (\Session::has('created'))
    toastr.success('{!! \Session::get('created') !!}', 'Created Successfully');
    @endif
    @if (\Session::has('error'))
    toastr.error('{!! \Session::get('error') !!}', 'Error occurred');
    @endif

    @if ($errors->any())
    $('#createModal').modal();
    @endif
            function addrangermore() {
                var rangercontent = `<div class=" row" >
                                           <div class="col-sm-8 offset-1">
                                               <div class="form-group">
                                                   <div class="ion-range-slider">
                                                       <label class="mt-2">Choose Range:</label>
                                                       <input type="hidden" name="range[]" class="kt_slider_a"/>
                                                   </div>
                                               </div>
                                           </div>
                                           <div class="col-sm-2 mt-2 ">
                                               <label class="mt-2">Set Price:</label>
                                               <input type="number" class="form-control" name="range_price[]"  step=".01" placeholder="0" min="0" id="range_price">
                                           </div>
                                            <div class="col-sm-1 pt-5">
                                                <i class="fa fa-trash cursor-pointer deletranger mt-5 pt-5" title="Delete "></i>
                                            </div>
                                       </div>`;

            $("#addranger-id").append(rangercontent);
            $('.kt_slider_a').ionRangeSlider({
                // type: "double",
                grid: true,
                // min: 0,
                max: 3000,
                from_min:1,
                min: 1,
                from: 1,
                to: 3000,
                prefix: "Miles",
                drag_interval: true,
            });
        }
            $(document).on('click', '.deletranger', function (e) {
            $(this).parent().parent().remove();
        })

    $(document).on('click', '#toggleSearchForm', function(){
        $('#table-search-form').slideToggle();
    });




    $(document).on('change', '.kt_slider_a', function (e) {
    // $(this).parent().find('.milage-notification').addClass('text-danger');
    var rangevalue = $(this).val();
    if (rangevalue != "") {
    var statevalue = $(".state_id").val();
    var data = {
    'state_id': statevalue,
    'range': rangevalue,
    };
    var object = {
    'url': "{{route('miles.available.check')}}",
    'type': 'GET',
    'data': data
    };
    var result = centralAjax(object);
    console.log('resultt', result);
    if (result == 0) {
        $("input[name*='status']").val('1')
        $('#error').removeClass('text-danger');
        $('#error').addClass('text-success');
        $('#error').text('Mileage available');
        toastr.success('Mileage availiable');
    } else {
        $('#error').text('Mileage already taken');
        $('#error').removeClass('text-success');
        $('#error').addClass('text-danger');
        $("input[name*='status']").val('0')
        toastr.error('Mileage already taken');
    }

    } else {
    toastr.error('Choose miles.');
    }
    // .irs--flat .irs-from, .irs--flat .irs-to, .irs--flat .irs-single {color:#da4675}
    // .irs--flat .irs-bar {color:#da4675}

    // console.log('counter of car',$(this).attr('data-id'));
    // // console.log('location choosed :',value);


    });

</script>



@endsection
