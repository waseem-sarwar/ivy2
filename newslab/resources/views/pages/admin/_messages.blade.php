@foreach($messages as $message)
<div class="{{$message->from == Auth::id() ? 'd-flex flex-column mb-5 align-items-end':'d-flex flex-column mb-5 align-items-start'    }}">
    <div class="d-flex align-items-center">
        <div class="symbol symbol-circle symbol-40 mr-3">
            <img alt="Pic" src="{{asset('media/users/default.jpg')}}"/>
        </div>
        <div>
{{--            <a href="#" class="text-dark-75 text-hover-primary font-weight-bold font-size-h6">{{$}}</a>--}}
            <span class="text-muted font-size-sm">{{date('D M Y,h:i:a',strtotime($message->created_at))}}</span>
        </div>
    </div>
    <div class="mt-2 rounded p-5 bg-light-success text-dark-50 font-weight-bold font-size-lg text-left max-w-400px">
       <p> {{$message->message}}</p>
    </div>
</div>
<!--end::Message In-->
@endforeach
