@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout = 'layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout = 'layout.driver'; ?>
@elseif($role=='2')
    <?php $layout = 'layout.default'; ?>
@elseif($role=='1')
    <?php $layout = 'layout.admin'; ?>
@endif
@extends($layout)
@section('content')
    {{-- Content --}}
@section('content')
    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pb-0">
            <div class="card-title">
                <h3 class="card-label">
                    <h3 class="pt-2 font-size-xl">{{$user->name}}</h3>
                </h3>
            </div>
            {{--            <div class="card-toolbar">--}}

            {{--                <select class="data-table-10list float-right">--}}
            {{--                    <option value="">10</option>--}}
            {{--                    <option value="">20</option>--}}
            {{--                </select>--}}
            {{--            </div>--}}
        </div>

        <div class="card-body pt-0 ">
            <div class="mt-0 mb-2 mt-lg-5 d-none " id="table-search-form">
                {{--<option   <?php  echo (null!=($request->meal_type_id)&&($meal_type->id ==$request->meal_type_id))?'selected':''; ?> value="{{$meal_type->id}}">{{$meal_type->name}}</option>--}}
                <form action="#" id="frm">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-xl-12  ">
                            <div class="row align-items-center">
                                <div class="col-md-3 my-2 my-md-0  offset-2">
                                    <div class="input-icon">
                                        <input type="text" name="search" class="form-control" placeholder="Search by name..."
                                               value="{{(isset($_GET['search']))?$_GET['search']:''}}"
                                               id="kt_datatable_search_query"/>
                                        <span><i class="flaticon2-search-1 text-muted"></i></span>
                                    </div>
                                </div>

                                <div class="col-md-3 my-2 my-md-0 ">
                                    <div class="d-flex align-items-center">
                                        {{--                                        <label class=" mb-0 d-none d-md-block">Email:</label>&nbsp--}}
                                        <input type="email" name="email" class="form-control" placeholder="email..."
                                               value="{{(isset($_GET['email']))?$_GET['email']:''}}"
                                               id="kt_datatable_search_query"/>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 ">
                                    <a href="#" onclick="document.getElementById('frm').submit();"
                                       class="btn btn-light-primary px-6 font-weight-bold">
                                        Search
                                    </a>
                                    @if(isset($_GET['search']) || isset($_GET['email']))
                                        @if( $_GET['email']!=null || $_GET['search']!=null)
                                            <a href="#" onclick="window.location.replace('{{route('getAllDrivers')}}')"
                                               class="btn btn-light-primary px-6 font-weight-bold">
                                                reset
                                            </a>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>



            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">Id</th>
                            <th style="min-width: 130px" class="">
                                <span class="">Title</span>
                            </th>
                            <th style="min-width: 100px">Status</th>

                            <th class="fix-table-head" style="min-width: 135px;">Apply Time</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                            $key=1;
                        @endphp
                        @forelse($data as $campaign)
                            <tr>
                                <td>
                                    <a href="{{route('camp.detail',$campaign->campaign->slug)}}" >
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">CMP-{{$campaign->campaign->id}}</span>
                                </a>
                                </td>
                                <td class="pl-0 py-8">
                                    <div class="d-flex align-items-center">

                                        <div>
                                            <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$campaign->campaign->campaign->title}}</a>
                                            {{--                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>--}}
                                        </div>
                                    </div>
                                </td>
                                <td class="pl-0 py-8">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$campaign->status}}</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <span class="text-muted font-weight-bold">{{@$campaign->created_at}}</span>
                                </td>

                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">
                                    <span class="border p-5 text-dark-75 font-weight-bolder d-block font-size-lg text-center">No campaign found</span>
                                </td>
                            </tr>

                        @endforelse
                        </tbody>
                    </table>
                    <div class="float-right pr-5 ">
                        <span class="pl-10">Total: {{$data->total()}} </span>
                        {{$data->links()}}
                    </div>
                </div>
                <!--end::Table-->
            </div>
            <!-- HTML Table End Here -->
        </div>
    </div>

    <div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    {{--                    <button class="btn btn-light-primary font-weight-bold text-right" style="margin-left: 267px;"><i--}}
                    {{--                            class="fa fa-download" aria-hidden="true"></i>download--}}
                    {{--                    </button>--}}
                    {{--                    <button class="btn btn-light-primary font-weight-bold" style="margin-left: -68px;"><i--}}
                    {{--                            class="fa fa-print" aria-hidden="true"></i>print--}}
                    {{--                    </button>--}}

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <!--                                    <div class="modal-body">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</div>-->
                <div class="modal-body">
                    <div class="container">
                        <div class="row justify-content-center py-10 px-8 px-md-0">
                            <div class="col-md-10">
                                <!-- begin: Invoice header-->
                                <div
                                    class="d-flex justify-content-between pb-5 pb-md-10 flex-column flex-md-row">
                                    <h2 class="display-4 font-weight-boldest mb-10">Driver details</h2>
                                    {{--                                            <span>Duration : 20m </span>--}}

                                    <div class="d-flex flex-column align-items-md-end px-0">
                                        <!--begin::Logo-->
                                        <a href="#" class="mb-5 max-w-200px">
                                            <span class="svg-icon svg-icon-full">
                                                <!--begin::Svg Icon | path:assets/media/svg/logos/duolingo.svg-->
{{--                                                <svg xmlns="http://www.w3.org/2000/svg" width="113"--}}
                                            {{--                                                     height="31" viewBox="0 0 113 31" fill="none">--}}
                                            {{--                                                    <path--}}
                                            {{--                                                        d="M26.6802 13.33V13.485C26.6802 14.9575 26.6087 15.8875 26.4659 16.275C26.2517 16.895 25.966 17.36 25.4661 17.67C25.0376 17.98 24.5377 18.1358 23.8949 18.1358C23.2529 18.1358 22.7523 17.9808 22.3245 17.67C21.8953 17.3608 21.6103 16.895 21.3954 16.275C21.1811 15.7325 21.1104 14.8025 21.1104 13.4075V5.425H17.3254V13.485C17.3968 15.345 17.5396 16.6625 17.7539 17.515C18.1824 18.9108 18.8965 19.995 19.8963 20.8483C20.8962 21.6233 22.2531 22.0108 23.8242 22.0108C25.324 22.0108 26.538 21.7008 27.5379 21.0025C28.537 20.3058 29.2511 19.375 29.7518 18.0575C30.1088 17.205 30.3231 15.655 30.3231 13.485V5.425H26.6087V13.33H26.6802ZM15.6107 0H11.897V6.2775C11.8256 6.2775 11.7542 6.2 11.6828 6.2C10.4687 5.425 9.11182 5.0375 7.75492 5.0375C5.82669 5.0375 4.1127 5.735 2.61297 7.2075C0.898984 8.9125 0.0419922 11.005 0.0419922 13.485C0.0419922 15.81 0.827568 17.825 2.3273 19.4525C3.82704 21.0808 5.68314 21.9325 7.75492 21.9325C9.18324 21.9325 10.5401 21.545 11.7542 20.8483C12.9683 20.0733 13.8967 19.065 14.6109 17.7483C15.2536 16.5858 15.54 15.345 15.6107 14.0283V13.4858V0ZM10.6823 16.895C9.89668 17.7483 8.96899 18.2125 7.7542 18.2125C6.61155 18.2125 5.61172 17.7475 4.89756 16.895C4.11199 16.0425 3.75491 14.88 3.75491 13.485C3.75491 12.1675 4.11199 11.005 4.89756 10.1525C5.68314 9.3 6.61155 8.835 7.7542 8.835C8.96828 8.835 9.89668 9.3 10.6823 10.1525C11.4685 11.005 11.8256 12.1675 11.8256 13.485C11.8256 14.88 11.4685 16.0425 10.6823 16.895ZM46.2481 9.3C45.534 7.9825 44.6049 6.975 43.3915 6.2C42.1774 5.425 40.8205 5.0375 39.4636 5.0375C37.5354 5.0375 35.8207 5.735 34.3217 7.2075C32.6077 8.9125 31.75 10.9275 31.75 13.485C31.75 15.81 32.5363 17.825 34.0353 19.4525C35.5357 21.0808 37.3918 21.9325 39.4636 21.9325C40.8919 21.9325 42.2481 21.545 43.4629 20.8483C44.677 20.0733 45.6054 19.065 46.3195 17.7483C47.0337 16.4308 47.3908 15.0358 47.3908 13.4858C47.248 12.0125 46.9623 10.6175 46.2481 9.3ZM42.3917 16.895C41.6061 17.7483 40.6777 18.2125 39.4636 18.2125C38.2495 18.2125 37.3211 17.7475 36.607 16.895C35.8214 16.0425 35.4643 14.88 35.4643 13.485C35.4643 12.1675 35.8214 11.005 36.6077 10.1525C37.3933 9.3 38.321 8.835 39.4643 8.835C40.6777 8.835 41.6068 9.3 42.3924 10.1525C43.178 11.005 43.535 12.1675 43.535 13.485C43.5336 14.88 43.1058 16.0425 42.3917 16.895ZM60.2457 21.7H64.0307V6.1225H60.2457V21.7ZM53.8182 17.515C53.3897 17.2058 53.0326 16.74 52.8898 16.12C52.747 15.655 52.6756 14.725 52.6756 13.33V0H48.9619V13.2525C49.0334 15.4225 49.1762 16.895 49.5333 17.825C50.0332 19.065 50.7473 20.0725 51.7472 20.77C52.747 21.4675 53.9618 21.7775 55.4608 21.7775H58.3174V17.98H55.3894C54.7466 17.98 54.246 17.825 53.8182 17.515ZM78.7424 9.145C78.2425 7.905 77.5283 6.8975 76.5278 6.2C75.5287 5.5025 74.3139 5.1925 72.8142 5.1925C71.1716 5.1925 69.8861 5.58 68.8863 6.355C67.8872 7.13 67.173 8.215 66.7438 9.6875C66.5296 10.4625 66.3867 11.8575 66.3153 13.7175V21.7H70.1003V13.95V13.64C70.1003 12.3225 70.1725 11.315 70.386 10.7725C70.6003 10.1525 70.8859 9.765 71.3144 9.3775C71.7429 9.0675 72.2428 8.9125 72.8856 8.9125C73.5276 8.9125 74.0282 9.0675 74.4567 9.3775C74.8852 9.6875 75.2423 10.1525 75.4558 10.7725C75.5987 11.2375 75.6701 12.1675 75.6701 13.5625V21.7H79.3837V14.88V13.64C79.3137 11.5475 79.0995 9.9975 78.7424 9.145ZM62.0311 0.31C60.9598 0.31 60.0314 1.3175 60.0314 2.48C60.0314 3.6425 60.8884 4.65 62.0311 4.65C63.1023 4.65 64.0307 3.72 64.0307 2.48C64.0307 1.24 63.1023 0.31 62.0311 0.31ZM111.951 9.455C111.237 8.1375 110.308 7.13 109.094 6.355C107.88 5.58 106.595 5.27 105.094 5.27C102.952 5.27 101.166 6.1225 99.6666 7.75C98.1661 9.3775 97.3813 11.3925 97.3813 13.7175C97.3813 16.1975 98.2383 18.2892 99.9522 19.995C101.453 21.4675 103.166 22.165 105.094 22.165C106.522 22.165 107.808 21.7775 109.022 21.0025C110.236 20.2275 111.237 19.22 111.879 17.9025C112.593 16.5858 112.95 15.19 112.95 13.7183C113.022 12.1675 112.665 10.7725 111.951 9.455ZM109.236 13.7175C109.236 15.1125 108.879 16.1975 108.094 17.05C107.308 17.9025 106.38 18.3675 105.166 18.3675C104.022 18.3675 103.023 17.9017 102.309 17.05C101.523 16.1975 101.166 15.035 101.166 13.7175C101.166 12.3225 101.523 11.16 102.309 10.3075C103.095 9.455 104.022 8.99 105.166 8.99C106.308 8.99 107.308 9.455 108.094 10.3075C108.879 11.16 109.236 12.3225 109.236 13.7175C109.236 13.64 109.236 13.64 109.236 13.7175ZM96.0965 13.02C96.0244 11.7025 95.6673 10.54 95.0967 9.3775C94.3825 8.06 93.4541 7.0525 92.3122 6.2775C91.0981 5.5025 89.8126 5.1925 88.3843 5.1925C86.2418 5.1925 84.4564 5.9675 82.9559 7.6725C81.4562 9.3 80.7421 11.315 80.7421 13.64C80.7421 16.12 81.599 18.2125 83.2416 19.84C84.7413 21.3125 86.3832 22.0108 88.3121 22.0108C89.669 22.0108 91.026 21.6233 92.24 20.8483C92.3122 20.77 92.3829 20.77 92.4543 20.6933V22.5525C92.4543 23.9483 92.0258 25.0325 91.3116 25.8858C90.526 26.7383 89.5976 27.2025 88.455 27.2025C87.3123 27.2025 86.3839 26.815 85.5983 25.9625L83.1702 28.7525L83.2416 28.83C84.7413 30.3017 86.4553 31 88.3121 31C89.669 31 91.026 30.6125 92.24 29.8375C93.4541 29.0625 94.3825 28.055 95.0967 26.7375C95.8108 25.42 96.0965 24.025 96.0965 22.5525V14.105V13.5625C96.1679 13.4075 96.1679 13.175 96.0965 13.02ZM92.4536 13.5625C92.4536 14.9575 92.0965 16.0425 91.3109 16.895C90.5253 17.7483 89.5969 18.2125 88.4543 18.2125C87.3116 18.2125 86.3832 17.7475 85.5976 16.895C84.812 16.0425 84.455 14.9575 84.455 13.5625C84.455 12.1675 84.812 11.0825 85.5976 10.1525C86.3832 9.3 87.3116 8.835 88.4543 8.835C89.5969 8.835 90.5967 9.3 91.3109 10.1525C92.0258 11.0825 92.4536 12.1675 92.4536 13.5625Z"--}}
                                            {{--                                                        fill="#1CB0F6"></path>--}}
                                            {{--                                                </svg>--}}
                                            <!--end::Svg Icon-->
                                            </span>
                                        </a>
                                        <!--end::Logo-->
                                        <span
                                            class="d-flex flex-column text-center align-items-md-end font-size-h6 font-weight-bold text-muted">
{{--															<span class="text-center">Start Campaign  </span>--}}
                                            {{--															<span>12 May 2020  </span>--}}

                                        </span>
                                    </div>
                                </div>

                                <div class="row font-size-lg mb-3">
                                    <span class="col-md-2 font-weight-bold pr-1">Name:</span>
                                    <span class="col-md-4 text-right" id="first_name">
{{--                                                    	<span class="navi-icon mr-2">--}}
                                        {{--																			<i class="fa fa-genderless text-primary font-size-h2"></i>--}}
                                        {{--                                                        </span>--}}
                                    </span>
                                    <span class="col-md-2 font-weight-bold pl-3 pr-1">DOB:</span>
                                    <span class="col-md-4 text-right " id="last_name"></span>
                                </div>

                                {{--                                <div class="row font-size-lg mb-3">--}}
                                {{--                                    <span class="col-md-2 font-weight-bold pr-1">Country:</span>--}}
                                {{--                                    <span class="col-md-4 text-right " id="country"></span>--}}
                                {{--                                    <span class="col-md-2 font-weight-bold pl-3 pr-1">City</span>--}}
                                {{--                                    <span class="col-md-4 text-right " id="city_personal"></span>--}}
                                {{--                                </div>--}}
                            </div>

                        </div>

                        <div class="accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left text-decoration-none"
                                                type="button" data-toggle="collapse" data-target="#collapseOne"
                                                aria-expanded="true" aria-controls="collapseOne">Car Info
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="d-flex flex-column w-100">
                                            <div class="d-flex flex-column w-100">
                                                <div
                                                    class=" font-size-h6 pb-5 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                    Vehicle Info
                                                </div>
                                                <div class="d-flex flex-column mb-10 mb-md-0 ml-2  ">

                                                    <div class="d-flex justify-content-between font-size-lg mb-3">
                                                        <span class="font-weight-bold mr-15" >Vehicle Make:</span>
                                                        <span class="text-right " id="car_make"></span>
                                                    </div>

                                                    <div class="d-flex justify-content-between font-size-lg mb-3">
                                                        <span class="font-weight-bold mr-15">Vehicle Name:</span>
                                                        <span class="text-right" id="car_name"></span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg mb-3">
                                                        <span class="font-weight-bold mr-15">Vehicle Type:</span>
                                                        <span class="text-right" id="car_type"></span>
                                                    </div>
                                                    <div class="d-flex justify-content-between font-size-lg">
                                                        <span class="font-weight-bold mr-15">Vehicle Model:</span>
                                                        <span class="text-right" id="car_model"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="border-bottom  my-8"></div>

                                            <div class="d-flex flex-column w-100">
                                                <div
                                                    class=" font-size-h6 pb-5 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                    Car Pictures
                                                </div>
                                                <div class="d-flex flex-column mb-10 mb-md-0 ml-2  ">
                                                    <div class="p-2 d-flex align-items-end">
                                                        <span class="pr-1" style="max-width:190px">
                                                            <img class="img-fluid" alt="car_pic not availablr" id="car_pic" />
{{--                                                                 src="https://upload.wikimedia.org/wikipedia/commons/a/a4/2016_Toyota_Corolla_%28ZRE172R%29_Ascent_sedan_%282017-01-15%29_01.jpg"/>--}}
                                                        </span>
                                                        <span class="pr-1" style="max-width:190px">
                                                            <img class="img-fluid" alt="car_pic not availablr" id="car_registration_pic"  alt="car_registration_pic"/>
{{--                                                                 src="https://upload.wikimedia.org/wikipedia/commons/a/a4/2016_Toyota_Corolla_%28ZRE172R%29_Ascent_sedan_%282017-01-15%29_01.jpg"/>--}}
                                                        </span>
                                                        {{--                                                        <span style="max-width:190px">--}}
                                                        {{--                                                            <img class="img-fluid" alt=""--}}
                                                        {{--                                                                 src="https://upload.wikimedia.org/wikipedia/commons/2/25/2018_Toyota_Corolla_%28ZRE172R%29_Ascent_sedan_%282018-11-02%29_01.jpg"/>--}}
                                                        {{--                                                        </span>--}}
                                                    </div>
                                                    {{--                                                        <div class="d-flex justify-content-between font-size-lg mb-3">--}}
                                                    {{--                                                            <span class="font-weight-bold mr-15">Vehicle Type:</span>--}}
                                                    {{--                                                            <span class="text-right">Pic1</span>--}}
                                                    {{--                                                        </div>--}}

                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingTwo">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed text-decoration-none"
                                                type="button" data-toggle="collapse" data-target="#collapseTwo"
                                                aria-expanded="false" aria-controls="collapseTwo">Drive a car location
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th class="py-2 pl-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        Country
                                                    </th>
                                                    <th class="py-2 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        State
                                                    </th>

                                                    <th class="py-2 text-right font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        Address
                                                    </th>
                                                    <th class="py-2 text-right pr-0 font-weight-bolder text-muted font-size-lg text-uppercase">
                                                        Zip
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr class=" font-size-lg">
                                                    <td class="border-top-0 pl-0 py-3 d-flex align-items-center" id="car_drive_country">

                                                    </td>
                                                    <td class="pr-0 py-3 font-size-h6  text-right" id="car_drive_state">

                                                    </td>
                                                    <td class="text-right py-3" id="car_drive_address"></td>
                                                    <td class="text-right py-3" id="car_drive_zip"></td>

                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingThree">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed text-decoration-none"
                                                type="button" data-toggle="collapse" data-target="#collapseThree"
                                                aria-expanded="false" aria-controls="collapseThree">Uploaded Documents
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseThree" class="collapse" aria-labelledby="headingThree"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="document-uploaded d-flex flex-column mb-10 mb-md-0 ml-2  ">
                                            <div class="d-flex flex-column font-size-lg mb-3">
                                                <span class="font-weight-bold mb-5">Driving License </span>
                                                <span class="">
                                                    <span class="pr-1" style="max-width:190px">
                                                        <img class="img-fluid" alt="lic" id="licence_pic" />
                                                    </span>
                                                </span>
                                            </div>
                                            <div class="d-flex flex-column font-size-lg mb-3">
                                                <span class="font-weight-bold mb-5">Government CNIC/Passport</span>
                                                <span class=" ">
                                                    <span class="pr-1" style="max-width:190px">
                                                        <img class="img-fluid" alt="cnic" id="cnic_path" />
                                                        </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header" id="headingFour">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link btn-block text-left collapsed text-decoration-none"
                                                type="button"
                                                data-toggle="collapse" data-target="#collapseFour"
                                                aria-expanded="false" aria-controls="collapseFour">
                                            Other Details
                                        </button>
                                    </h2>
                                </div>
                                <div id="collapseFour" class="collapse" aria-labelledby="headingFour"
                                     data-parent="#accordionExample">
                                    <div class="card-body">
                                        <div class="d-flex flex-column mb-10 mb-md-0 ml-2  ">
                                            <div class="d-flex justify-content-between font-size-lg mb-3">
                                                <span
                                                    class="font-weight-bold mr-15">Mailing address :</span>
                                                <span class="text-right" id="mailing_address"></span>
                                            </div>

                                            <div class="d-flex justify-content-between font-size-lg mb-3">
                                                <span
                                                    class="font-weight-bold mr-15">How many hours drive a car? :</span>
                                                <span class="text-right" id="hours_drive_a_car"></span>
                                            </div>

                                            <div class="d-flex justify-content-between font-size-lg mb-3">
                                                <span class="font-weight-bold mr-15">How often drive a car?:</span>
                                                <span class="text-right" id="days_of_car"></span>
                                            </div>
                                            <div class="d-flex justify-content-between font-size-lg">
                                                <span class="font-weight-bold pr-5"> How often mileage a car? :</span>
                                                <span class="text-right" id="mileage_of_car"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>



@endsection

{{-- Styles Section --}}



{{-- Scripts Section --}}
@section('scripts')
    {{--    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>--}}


    {{--<link href="https://rawgit.com/t4t5/sweetalert/master/dist/sweetalert.css" rel="stylesheet">--}}
    {{--<script src="https://rawgit.com/t4t5/sweetalert/master/dist/sweetalert.min.js"></script>--}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    {{-- vendors --}}
    <script>
        $('.status').click(function() {
            var id=$(this).val()
            if ($(this).is(':checked')){
                swal({
                    title: "Are you sure?",
                    text: "You want to activate this user?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            var data = {
                                'id':id,
                                'status':1,
                            }
                            $.ajax({
                                url: '{{route('userStatusUpdate')}}',
                                type: "GET",
                                data: data,
                                success: function (result) {
                                    if (result=='1') {
                                        toastr.success('User activated successfully');
                                        // Swal.fire({title:'User activated!', icon: 'success'});
                                    } else{
                                        Swal.fire({title:'User not activated! try again', icon: 'warning'});
                                    }
                                }
                            })
                        } else {
                            $("#"+id).prop('checked', function(_, checked) {
                                return !checked;
                            });
                        }
                    });
            }
            else {
                swal({
                    title: "Are you sure?",
                    text: "You want to deactivate this user?",
                    icon: "error",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            var data = {
                                'id': id,
                                'status': 0,
                            }
                            $.ajax({
                                url: '{{route('userStatusUpdate')}}',
                                type: "GET",
                                data: data,
                                success: function (result) {
                                    if (result == '1') {
                                        toastr.error('User deactivated successfully');
                                        // Swal.fire({title: 'User deactivated!', icon: 'success'});
                                    } else {
                                        Swal.fire({title: 'User not deactivated!', icon: 'warning'});
                                    }
                                }
                            })
                        } else {
                            $("#" + id).prop('checked', function (_, checked) {
                                return !checked;
                            });
                        }
                    });

            }
        });

        function details(driver,profile){
            profile=JSON.parse(profile);
            driver=JSON.parse(driver);
            $('#car_make').text(driver.car_company.company);
            $('#car_type').text(driver.car_typee.name);
            $('#car_model').text(driver.car_yearr.model_name);
            $('#car_name').text(driver.car_namee.car_name);
            $('#city_personal').text(driver.city_personal);

            // $('#country').text(driver.country.name);
            // console.log('state',driver.state);
            $('#car_drive_state').text(driver.state.state);
            $('#car_drive_address').text(driver.car_drive_address);
            $('#car_drive_zip').text(driver.car_drive_zip);
            $('#first_name').text(profile.first_name+' '+profile.last_name);
            $('#last_name').text(driver.dob);
            $('#hours_drive_a_car').text(driver.hours_drive_a_car+'hrs');
            $('#mileage_of_car').text(driver.mileage_of_car+'Miles');
            $('#mailing_address').text(driver.billing_address_personal);
            // console.log('dircvicc',driver.driving_license_path)
            $('#licence_pic').attr('src',globalPublicPath+'/images/enrollements/'+driver.driving_license_path);
            $('#car_pic').attr('src',globalPublicPath+'/images/enrollements/'+driver.car_photo);
            $('#cnic_path').attr('src',globalPublicPath+'/images/enrollements/'+driver.cnic_path);
            $('#car_registration_pic').attr('src',globalPublicPath+'/images/enrollements/'+driver.car_registration_proof);
            $('#days_of_car').text(driver.days);
            $('#car_drive_country').text(driver.country.name);

        }
    </script>


@endsection
