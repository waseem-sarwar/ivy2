@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout = 'layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout = 'layout.driver'; ?>
@elseif($role=='2')
    <?php $layout = 'layout.default'; ?>
@elseif($role=='1')
    <?php $layout = 'layout.admin'; ?>
@endif
@extends($layout)
@section('content')
    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }

        .fix-table-column-old2 {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 25px;
            background-color: #ffffff;
        }

        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }

        .fix-table-head {
            position: sticky;
            right: 0;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-2 pb-0">
            <div class="card-title m-0">
                <h3 class="card-label">Car settings
                    <div class="text-muted  font-size-sm"></div>
                </h3>
            </div>
            <div class="card-toolbar row">
                <div class="col-md-12">
                    <span style="min-width: 120px !important" href="javascript:;" data-toggle="modal"
                          data-target="#createModal" class="btn btn-primary font-weight-bolder">
                         <span class="fa fa-plus"></span>
{{--                <span class="svg-icon svg-icon-md">--}}
                        {{--                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"--}}
                        {{--                         version="1.1">--}}
                        {{--                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
                        {{--                            <rect x="0" y="0" width="24" height="24"/>--}}
                        {{--                            <circle fill="#000000" cx="9" cy="15" r="6"/>--}}
                        {{--                            <path--}}
                        {{--                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"--}}
                        {{--                                fill="#000000" opacity="0.3"/>--}}
                        {{--                        </g>--}}
                        {{--                    </svg>--}}
                        {{--                    <!--end::Svg Icon-->--}}
                        {{--                </span>--}}
                        Add Company
                    </span>
                </div>
            {{--                <div class="col-md-3">--}}
            {{--                    <a href="javascript:;" style="min-width: 130px !important" data-toggle="modal"--}}
            {{--                       data-target="#cartype" class="btn btn-primary font-weight-bolder">--}}
            {{--                        --}}{{--                <span class="svg-icon svg-icon-md">--}}
            {{--                        --}}{{--                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"--}}
            {{--                        --}}{{--                         version="1.1">--}}
            {{--                        --}}{{--                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
            {{--                        --}}{{--                            <rect x="0" y="0" width="24" height="24"/>--}}
            {{--                        --}}{{--                            <circle fill="#000000" cx="9" cy="15" r="6"/>--}}
            {{--                        --}}{{--                            <path--}}
            {{--                        --}}{{--                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"--}}
            {{--                        --}}{{--                                fill="#000000" opacity="0.3"/>--}}
            {{--                        --}}{{--                        </g>--}}
            {{--                        --}}{{--                    </svg>--}}
            {{--                        --}}{{--                    <!--end::Svg Icon-->--}}
            {{--                        --}}{{--                </span>--}}
            {{--                        Car Type</a>--}}
            {{--                </div>--}}
            {{--                <div class="col-md-3  ">--}}
            {{--                <span href="javascript:;" style="min-width: 130px !important" data-toggle="modal" data-target="#carName"--}}
            {{--                      class="btn btn-primary font-weight-bolder">--}}
            {{--                <span class="svg-icon svg-icon-md">--}}
            {{--                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"--}}
            {{--                         version="1.1">--}}
            {{--                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
            {{--                            <rect x="0" y="0" width="24" height="24"/>--}}
            {{--                            <circle fill="#000000" cx="9" cy="15" r="6"/>--}}
            {{--                            <path--}}
            {{--                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"--}}
            {{--                                fill="#000000" opacity="0.3"/>--}}
            {{--                        </g>--}}
            {{--                    </svg>--}}
            {{--<!--end::Svg Icon-->--}}
            {{--    --}}{{--                </span>--}}
            {{--                    Car Name</span>--}}
            {{--                </div>--}}
            {{--                <div class="col-md-3">--}}
            {{--                    <span href="javascript:;" style="min-width: 130px !important" data-toggle="modal"--}}
            {{--                          data-target="#carModel" class="btn btn-primary font-weight-bolder">--}}
            {{--                <span class="svg-icon svg-icon-md">--}}
            {{--                        --}}{{--                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"--}}
            {{--                        --}}{{--                         version="1.1">--}}
            {{--                        --}}{{--                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">--}}
            {{--                        --}}{{--                            <rect x="0" y="0" width="24" height="24"/>--}}
            {{--                        --}}{{--                            <circle fill="#000000" cx="9" cy="15" r="6"/>--}}
            {{--                        --}}{{--                            <path--}}
            {{--                        --}}{{--                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"--}}
            {{--                        --}}{{--                                fill="#000000" opacity="0.3"/>--}}
            {{--                        --}}{{--                        </g>--}}
            {{--                        --}}{{--                    </svg>--}}
            {{--                        --}}{{--                    <!--end::Svg Icon-->--}}
            {{--                        --}}{{--                </span>--}}
            {{--                        Car Model</span>--}}
            {{--                </div>--}}

            <!--end::Button-->
            </div>
        </div>

        <div class="card-body pt-0 pb-0">
            <!--begin::Search Form-->
            <!-- HTML Table Begin Here -->
            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table
                        class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">NO.</th>
                            <th style="min-width: 130px" class="">
                                <span class="">Car Makers</span>
                            </th>

                            {{--                            <th style="min-width: 100px">Default price</th>--}}
                            <th class="fix-table-head" style="min-width: 135px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($carmakers as $carmaker)
                            {{--                            {{dd($carmaker)}}--}}
                            <tr>
                                <td>
                                    <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$carmaker->id}}</span>
                                </td>
                                <td class="pl-0 py-8">
                                    <div class="d-flex align-items-center">
                                        <div>
                                            <a href="#"
                                               class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$carmaker->company}}</a>
                                        </div>
                                    </div>
                                </td>

                                {{--                            <td class="pl-0 py-8">--}}
                                {{--                                <div class="d-flex align-items-center">--}}
                                {{--                                    <div>--}}
                                {{--                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$state->default_price}}</a>--}}
                                {{--                                    </div>--}}
                                {{--                                </div>--}}
                                {{--                            </td>--}}

                                <td class="pr-0 ">

                                    <a href="javascript:;" onclick="viewDetail('{{$carmaker->id}}','{{$carmaker->company}}');" data-toggle="modal"
                                       data-target="#viewDetailModal"
                                       class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
                                       title="view Cars">
                                        <i class="la la-eye"></i>
                                    </a>
                                    <a href="javascript:;" onclick="getcars({{$carmaker->id}},{{$carmaker->car_names}});" data-toggle="modal"
                                       data-target=".editModal"
                                       class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2"
                                       title="Add Car">
                                        <i class="la la-plus"></i>
                                    </a>
                                </td>

                            </tr>


                        @endforeach

                        </tbody>

                    </table>
                    <nav aria-label="Page navigation example ">
                        <ul class="pagination float-right mt-5">
                            {{--                            <h6 class="pt-3">Total records:{{$states->total()}}  </h6> &nbsp &nbsp&nbsp  {{$states->links()}}--}}
                        </ul>
                    </nav>
                </div>
                <!--end::Table-->
            </div>
            <!-- HTML Table End Here -->
        </div>
    </div>

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Company </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="POST" action="{{ route('addMaker') }}">
                    <div class="modal-body">
                        <div class="card" style="border:none;">
                            <div class="card-body  p-0">
                                <!--begin::Invoice-->
                                <div class="row ">

                                    @csrf
                                    @if ($errors->any())
                                        <div class="alert alert-danger" style="background-color: #e0afb4; ">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li class="float-left">{{ $error }}</li><br>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="col-md-12 offset-1 ">

                                        <div class=" row mt-5 mb-5  ">
                                            <div class="col-md-12 ">
                                                <label class="mt-1">Company Name:</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name ="companyy" value="{{old('companyy')}}"
                                                       placeholder="Company name" required>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end::Invoice-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-light-primary font-weight-bold">Submit</button>
                        <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Back
                        </button>

                        <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="cartype" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Type</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="POST" action="{{ route('addType') }}">
                    <div class="modal-body">
                        <div class="card" style="border:none;">
                            <div class="card-body  p-0">
                                <!--begin::Invoice-->
                                <div class="row ">

                                    @csrf
                                    @if ($errors->any())
                                        <div class="alert alert-danger" style="background-color: #e0afb4; ">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li class="float-left">{{ $error }}</li><br>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="col-md-12 offset-1 ">

                                        <div class=" row mt-5 mb-5  ">
                                            <div class="col-md-12 ">
                                                <label class="mt-1">Add Type</label>
                                            </div>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control" name="car_type"
                                                       placeholder="Add Type" required>
                                            </div>
                                        </div>
                                    </div>

                                    <!--end::Invoice-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-light-primary font-weight-bold">Submit</button>
                        <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Back
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="carName" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Car Name</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="POST" action="{{ route('makeCar') }}">
                    <div class="modal-body">
                        <div class="card" style="border:none;">
                            <div class="card-body  p-0">
                                <!--begin::Invoice-->
                                <div class="row ">

                                    @csrf
                                    @if ($errors->any())
                                        <div class="alert alert-danger" style="background-color: #e0afb4; ">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li class="float-left">{{ $error }}</li><br>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="row col-md-12">

                                        <div class="col-md-6">
                                            <label class="mt-1">select car maker:</label>

                                            <select class="form-control" name="company_id" required>
                                                <option value="" selected disabled>select maker</option>
                                                @foreach($carmakers as $carmaker)
                                                    <option value="{{$carmaker->id}}">{{$carmaker->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-6">
                                            <label class="mt-1">Add Name:</label>
                                            <input type="text" class="form-control" name="car_name"
                                                   placeholder="company name" required>
                                        </div>
                                    </div>

                                    <!--end::Invoice-->
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-light-primary font-weight-bold">Submit</button>
                        <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Back
                        </button>

                        <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="carModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Car model</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form class="form" method="POST" action="{{ route('makeModel') }}">
                    <div class="modal-body">
                        <div class="card" style="border:none;">
                            <div class="card-body  p-0">
                                <!--begin::Invoice-->
                                <div class="row ">

                                    @csrf
                                    @if ($errors->any())
                                        <div class="alert alert-danger" style="background-color: #e0afb4; ">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li class="float-left">{{ $error }}</li><br>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    <div class="row  form-group col-md-12 ">
                                        <div class="col-md-4">
                                            <label class="mt-1">Select Car Maker</label>
                                            <select class="form-control" name="company_id" required
                                                    onchange="getcar(this.value)">
                                                <option value="" selected disabled>select maker</option>
                                                @foreach($carmakers as $carmaker)
                                                    <option value="{{$carmaker->id}}">{{$carmaker->company}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4 car_name">
                                            <div class="form-group">
                                                <div>
                                                    <label class="mt-1 d-flex justify-content-between align-items-center" for="car_name">
                                                        <span>Select Car Name:</span>
                                                        <i id="car_name_click" class="fa fa-plus-circle ml-1 cursor-pointer"></i>
                                                    </label>
                                                    <select class="form-control" name="car_name_i" id="car_name_1" onchange="getmodel(this.value)">
                                                        <option value="" selected disabled>select name</option>
                                                        @if(isset($car_name))
                                                            @foreach($car_name as $car)
                                                                <option value="{{$car->id}}">{{$car->car_name}}</option>
                                                            @endforeach
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="car_name_option" style="display: none;">
                                            <div class="mt-3 d-flex align-items-center justify-content-between">
                                                <div class="form-group">
                                                    <label for="inp_car_name_option" class="mt-1">Write Car Option
                                                        Name</label>
                                                    <input type="text" class="form-control" name="inp_car_name_option"
                                                           id="inp_car_name_option" placeholder="Write name">
                                                </div>
                                                <button type="button"
                                                        class="btn btn-primary-revert">Save
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="mt-1">Add Models:</label>
                                    <input type="text" class="form-control" name="model"
                                           placeholder="Model Name" required>
                                </div>
                            </div>

                            <!--end::Invoice-->
                        </div>

                    </div>
                </form></div>

            <div class="modal-footer">
                <button type="submit" class="btn btn-light-primary font-weight-bold">Submit</button>
                <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Back
                </button>

                <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
            </div>
        </div>
    </div>


    <div class="modal fade" id="viewDetailModal" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Available cars against: <span id="car"></span></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card" style="border:none;">
                        <div class="card-body  p-0">
                            <!--begin::Invoice-->
                            <div class="col-md-10 offset-1 ">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        {{--                                        <th>Id</th>--}}
                                        <th>NO.</th>
                                        <th>Maker</th>
                                        <th>Car</th>
                                        <th>model</th>
                                        <th>Type</th>
                                        <th>Created_at</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody id="car_list">
                                    @include("pages.admin.partials._car-list")
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    {{--                    <button type="button" onclick="document.getElementById('form-2').submit();" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Save--}}
                    {{--                    </button>--}}
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add New Car</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <form method="POST" action="{{ route('createCar')}}">
                    <div class="modal-body">
                        <div class="card" style="border:none;">
                            <div class="card-body  p-0">
                                <!--begin::Invoice-->

                                <div class="row">
                                    <div class="col-md-12 message d-none"  >
                                        <p class="alert alert-warning text-center ">Option created successfull</p>
                                    </div>

                                    <div class="col-md-12 ">

                                        <div class="form-group">
                                            @csrf
                                            <input type="hidden" name="maker_id" id="maker_id">
                                            <label class="mt-1 d-flex justify-content-between align-items-center" for="car_name">
                                                <span>Select Car Name:</span>
                                                <i id="car_name_click_1" data-check="0" class="fa fa-plus-circle ml-1 cursor-pointer"></i>
                                            </label>
                                            <select class="form-control car_names" name="car_name" id="car_name" onchange="getmodels(this.value)" required>
                                                <option value="" selected disabled>Select name</option>
                                            </select>
                                        </div>

                                        <div id="car_name_option_1" style="display: none;" >
                                            <div class="mt-3 d-flex align-items-center" >
                                                <div class="form-group col-md-6 offset-2">
                                                    <label for="input_car_name_option" class="mt-1">Write Car Name</label> &nbsp<span class="input_car_error text-danger"></span>
                                                    <input type="text" class="form-control  input_car_name_option" name="inp_car_name_option"
                                                           id="input_car_name_option" placeholder="Write name">
                                                </div>
                                                <div class="mt-10 form-group col-md-1">
                                                    <a  id="btn_car_name_option" class="btn btn-primary" >create</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 car_model">
                                        <div class="form-group">
                                            <label for="car_model_1" class="d-flex justify-content-between align-items-center">
                                                <span>Model:</span>
                                                <i id="car_model_click"  data-check="0" class="fa fa-plus-circle ml-1 cursor-pointer"></i>
                                            </label>
                                            <select class="form-control" name="car_model" id="car_model_1" required>
                                                <option value="" selected disabled>Select name</option>
                                                @if(isset($car_model))
                                                    @foreach($car_model as $car)
                                                        <option value="{{$car->id}}">{{$car->model_name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>

                                        <div id="car_model_option" style="display: none;">
                                            <div class="mt-3 d-flex align-items-center">
                                                <div class="form-group col-md-6 offset-2">
                                                    <label for="inp_car_model_option" class="mt-1">Write Car Model<small>(Manufacturing year)</small></label>  &nbsp<span class="inp_car_model_option_error text-danger"></span>
                                                    <input type="number" class="form-control" name="inp_car_model_option" id="inp_car_model_option" placeholder="Write name">
                                                </div>
                                                <div class="mt-10 form-group col-md-1">
                                                    <a  id="btn_car_model_option" class="btn btn-primary" >create</a>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="car_type"
                                                   class="d-flex justify-content-between align-items-center">
                                                <span>Type:</span>
                                                <i id="car_type_click" data-check="0"
                                                   class="fa fa-plus-circle ml-1 cursor-pointer"></i>
                                            </label>
                                            <select name="car_type" id="car_type" required class="form-control">
                                                <option value="" selected disabled>Select Type</option>
                                                @foreach($carTypes as $carType)
                                                    <option value="{{$carType->id}}">{{$carType->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div id="car_type_option" style="display: none;">
                                            <div class="mt-3 d-flex align-items-center">
                                                <div class="form-group col-md-6 offset-2">
                                                    <label for="inp_car_type_option" class="mt-1">Write Car Type
                                                        Name</label>  &nbsp <span class="inp_car_type_option_error text-danger"></span>
                                                    <input type="text" class="form-control" name="inp_car_type_option"
                                                           id="inp_car_type_option" placeholder="Write name">
                                                </div>
                                                <div class="mt-10 form-group col-md-1">
                                                    <a  id="btn_car_type_option"  class="btn btn-primary" >create</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-light-success font-weight-bold">Submit</button>
                        <button type="button" class="btn btn-light-danger font-weight-bold" data-dismiss="modal">Back
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

{{-- Styles Section --}}
@section('styles')

@endsection


{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script>
        function formsubmit() {
            var values = $("input[name*='range_price']")
                .map(function () {
                    return $(this).val();
                }).get();
            var val = 0;
            for (var i = 0; i < values.length; i++) {
                if (values[i] == '') {
                    var val = 1;
                }

            }
            if (val == 1) {
                $('#error').text('Please fill all fields');
                // $('.editModal').modal("show");

            } else {
                document.getElementById('form-2').submit();
            }


        }

        function upd(maker) {
            $('#maker_id').val(maker);
        }

        // $('.kt_slider_a').ionRangeSlider({
        //     type: "double",
        //     grid: true,
        //     min: 0,
        //     max: 3000,
        //     from: 200,
        //     to: 400,
        //     step: 1,
        //     prefix: "Miles",
        //     drag_interval: true,
        // });

        @if (\Session::has('updated'))
        toastr.warning('{!! \Session::get('updated') !!}', 'Updated');
        @endif
        @if (\Session::has('created'))
        toastr.success('{!! \Session::get('created') !!}', 'Created Successfully');
        @endif
        @if (\Session::has('error'))
        $('#createModal').modal();
        toastr.error('{!! \Session::get('error') !!}');
        @endif
        @if (\Session::has('deleted'))
        toastr.info('{!! \Session::get('deleted') !!}', 'Successfully');
        @endif

{{--        @if ($errors->any())--}}
{{--            alert();--}}
{{--        $('#createModal').modal();--}}
{{--        @endif--}}

        function viewDetail(car_id, name) {
            $('#car').text(name);
            let url = 'filter/car/' + car_id;
            $.get(url, function (response) {
                $("#car_list").html(response)
            });

        }

        {{--        get cars against company--}}
        function getcar(company_id) {
            $('#maker_id').val(company_id);
            let url = '/get/car/' + company_id;
            $.get(url, function (response) {
                // console.log(response);
                $(".car_name").html(response)
            });

        }
        function getcars(company_id,cars) {
            $('.input_car_error').text('')
            $('.input_car_name_option').css('border-color','#e4e6ef')
            $('.car_names').css('border-color','#e4e6ef')
            $('#inp_car_model_option').css('border-color','#e4e6ef')
            $('#car_model_option').hide()
            $('#car_name_option_1').hide()
            $('#car_type_option').hide()
            $('.inp_car_model_option_error').text('')
            $('.input_car_name_option').val('')
            $('#inp_car_model_option').val('')
            $('#inp_car_type_option').val('')
            $('.inp_car_type_option_error').text('')
            $('#car_name_click_1').attr('data-check','0')
            $('#car_name_click_1').removeClass('fa-minus-circle')
            $('#car_name_click_1').addClass('fa-plus-circle')
            $('#car_model_click').attr('data-check','0')
            $('#car_model_click').removeClass('fa-minus-circle')
            $('#car_model_click').addClass('fa-plus-circle')
            $('#car_type_click').attr('data-check','0')
            $('#car_type_click').removeClass('fa-minus-circle')
            $('#car_type_click').addClass('fa-plus-circle')



            $('#car_model_1').html("<option selected disabled value=''> Choose Model</option>");
            $("#car_name").html("<option selected disabled value=''> Choose Car</option>");
            $.each(cars, function (index, value) {
                var id = value['id'];
                var name = value['car_name'];
                var newOption = new Option(name, id, false, false);
                $('#car_name').append(newOption);
            });

            $('#maker_id').val(company_id);
        }

        //gat model against cars
        function getmodel(car_id) {
            let url = "{{route('getModel',':id')}}"
            url = url.replace(':id', car_id)
            // let url = '/get/model/' + car_id;
            $.get(url, function (response) {
                $(".car_model").html(response)
            });

        }
        function getmodels(car_id) {
            // let url = '/get-models/'+ car_id;


            let url = "{{route('get-models',':id')}}"
            url = url.replace(':id', car_id)


            {{--let url = "{{route('get-models',:)}}";--}}

            $.get(url, function (response) {
                $('#car_model_1').html('');
                $('#car_model_1').html("<option selected disabled value=''> Choose Model</option>");
                $.each(response, function (index, value) {
                    var id = value['id'];
                    var name = value['model_name'];
                    var newOption = new Option(name, id, false, false);
                    $('#car_model_1').append(newOption);
                });
            })

        }

        $("#btn_car_name_option").click(function(){
            var option=$('.input_car_name_option').val();
            if(option==''){
                $('.input_car_name_option').css('border-color','red')
                $('.input_car_error').text('This field is required')
            }
            else if(option.length>30){
                $('.input_car_name_option').css('border-color','red')
                $('.input_car_error').text('Max length exceeded')
            }
            else {
                let car_make = $("#maker_id").val();
                $('.input_car_error').text('')
                $('.input_car_name_option').css('border-color', '#e4e6ef')
                var data = {
                    'car_name': option,
                    'car_make_id': car_make,
                };
                let url = "{{route('make-car')}}"

                $.get(url, data, function (result) {
                    if(result.errors) {
                            var arr = result.errors;
                            $.each(arr, function (index, value) {
                                if (result.length != 0) {
                                    $(".input_car_error").text(value);
                                 // console.log('value',value);
                                }
                            });

                    }else{
                        // var newOption = new Option(result.car_name, result.id,true, true);
                        $('.car_names').append("<option selected value=" + result.id + ">" + result.car_name + "</option>");

                        // $('.message').show();
                        $('#car_name_option_1').fadeOut(1000, function () {
                            $('#car_name_option_1').hide();
                        });
                        $('.message').fadeIn(1000, function () {
                            $('.message').removeClass('d-none');
                        });
                        setTimeout(function () {
                            $('.message').fadeOut('slow')
                        }, 2000);
                        $('#car_name_click_1').attr('data-check', '0');
                        $('#car_name_click_1').removeClass('fa-minus-circle');
                        $('#car_name_click_1').addClass('fa-plus-circle');
                        $(".input_car_error").text('');
                    }
                });
                $('#ajax-loading').hide();
            }
        });


        $("#btn_car_model_option").click(function(){

            var option=$('#inp_car_model_option').val();

            let car_name= $('.car_names').val();
            if(car_name!=null){
                $('.car_names').css('border-color','#e4e6ef')
                $('.inp_car_model_option_error').text('')
            }
            if(car_name==null){
                $('.car_names').css('border-color','red')
                $('.inp_car_model_option_error').text('Please select a car to proceed')
            }

            else if(option==''){
                $('#inp_car_model_option').css('border-color','red')
                $('.inp_car_model_option_error').text('This field is required')
            }

            else if(option.length>4){
                $('#inp_car_model_option').css('border-color','red')
                $('.inp_car_model_option_error').text('Not a valid Year')
            }
            else{

                let car_company=$("#maker_id").val();
                $('.input_car_error').text('')
                $('.input_car_name_option').css('border-color','#e4e6ef')
                var data = {
                    'option_value': option,
                    'car_name': car_name,
                    'car_company': car_company,
                };
                let url = "{{route('make-model')}}"
                var object = {
                    'url':url,
                    'cache': false,
                    'data': data,
                };


                // $.get(url, object, function (result) {
                //     if(result.errors) {
                //         var arr = result.errors;
                //         $.each(arr, function (index, value) {
                //             if (result.length != 0) {
                //                 $(".inp_car_model_option_error").text(value);
                //                 // console.log('value',value);
                //             }
                //         });
                //
                //     }else{
                //         // var newOption = new Option(result.car_name, result.id,true, true);
                //         $('.car_names').append("<option selected value=" + result.id + ">" + result.car_name + "</option>");
                //
                //         // $('.message').show();
                //         $('#car_name_option_1').fadeOut(1000, function () {
                //             $('#car_name_option_1').hide();
                //         });
                //         $('.message').fadeIn(1000, function () {
                //             $('.message').removeClass('d-none');
                //         });
                //         setTimeout(function () {
                //             $('.message').fadeOut('slow')
                //         }, 2000);
                //         $('#car_name_click_1').attr('data-check', '0');
                //         $('#car_name_click_1').removeClass('fa-minus-circle');
                //         $('#car_name_click_1').addClass('fa-plus-circle');
                //         $(".input_car_error").text('');
                //     }
                // });

                var result = centralAjax(object);
                if(result.errors) {
                    $('.inp_car_model_option_error').text(result.errors);
                }
                else{
                    // var newOption = new Option(result.model_name, result.id,true, true);
                    $('#car_model_1').append("<option selected value="+result.id+">"+result.model_name+"</option>");;
                    $('#car_model_option').fadeOut(1000, function () {
                        $('#car_model_option').hide();
                    });
                    $('.message').fadeIn(1000, function () {
                        $('.message').removeClass('d-none');
                    });
                    setTimeout(function(){
                        $('.message').fadeOut('slow')
                    },2000);
                    $('#car_model_click').attr('data-check','0');
                    $('#car_model_click').removeClass('fa-minus-circle');
                    $('#car_model_click').addClass('fa-plus-circle');
                    $('.inp_car_model_option_error').text('');
                }
                console.log('result',result);

            }
        });

        $("#btn_car_type_option").click(function(){
            var option=$('#inp_car_type_option').val();
            if(option==''){
                $('#inp_car_model_option').css('border-color','red')
                $('.inp_car_type_option_error').text('This field is required')
            }

            else if(option.length>33){
                $('#inp_car_type_option').css('border-color','red')
                $('.inp_car_type_option_error').text('Max length exceeded')
            }
            else{

                let car_company=$("#maker_id").val();
                $('.input_car_error').text('')
                $('.input_car_name_option').css('border-color','#e4e6ef')
                var data = {
                    'option_value': option,
                };

                let url = "{{route('add-type')}}"
                var object = {
                    'url': url,
                    'cache': false,
                    'data': data,
                };
                var result = centralAjax(object);

                if(result.errors) {
                    $('.inp_car_type_option_error').text(result.errors);
                }
                else{
                    // var newOption = new Option(result.name, result.id,true, true);
                    // $('#car_type').prepend(newOption);
                    $('#car_type').append("<option selected value="+result.id+">"+result.name+"</option>");;
                    $('#car_type_option').fadeOut(1000, function () {
                        $('#car_type_option').hide();
                    });
                    $('.message').fadeIn(1000, function () {
                        $('.message').removeClass('d-none');
                    });
                    setTimeout(function(){
                        $('.message').fadeOut('slow')
                    },2000);
                    $('#car_type_click').attr('data-check','0');
                    $('#car_type_click').removeClass('fa-minus-circle');
                    $('#car_type_click').addClass('fa-plus-circle');
                    $('.inp_car_type_option_error').text('');
                }


            }
        });




    </script>
    <script>
        $(document).on('click', '#car_name_click_1', function () {
            var check = $('#car_name_click_1').attr('data-check');
            if (check == '0'){
                $('#car_name_option_1').fadeIn(1000, function () {
                    $('#car_name_option_1').show();
                });
                $('#car_name_click_1').attr('data-check','1');
                $('#car_name_click_1').removeClass('fa-plus-circle');
                $('#car_name_click_1').addClass('fa-minus-circle');

            }
            else{
                $('#car_name_option_1').fadeOut(1000, function () {
                    $('#car_name_option_1').hide();
                });
                $('#car_name_click_1').attr('data-check','0');
                $('#car_name_click_1').removeClass('fa-minus-circle');
                $('#car_name_click_1').addClass('fa-plus-circle');
            }
        });

        $(document).on('click', '#car_model_click', function () {
            var check = $('#car_model_click').attr('data-check');
            if (check == '0'){
                $('#car_model_click').attr('data-check','1');
                $('#car_model_click').removeClass('fa-plus-circle');
                $('#car_model_click').addClass('fa-minus-circle');
                $('#car_model_option').fadeIn(1000, function () {
                    $('#car_model_option').show();
                });


            }
            else{
                $('#car_model_click').attr('data-check','0');
                $('#car_model_click').removeClass('fa-minus-circle');
                $('#car_model_click').addClass('fa-plus-circle');
                $('#car_model_option').fadeOut(1000, function () {
                    $('#car_model_option').hide();
                });


            }


        });

        $(document).on('click', '#car_type_click', function () {
            var check = $('#car_type_click').attr('data-check');
            if (check == '0'){
                $('#car_type_option').fadeIn(1000, function () {
                    $('#car_type_option').show();
                });
                $('#car_type_click').attr('data-check','1');
                $('#car_type_click').removeClass('fa-plus-circle');
                $('#car_type_click').addClass('fa-minus-circle');
            }
            else{
                $('#car_type_option').fadeOut(1000, function () {
                    $('#car_type_option').hide();
                });
                $('#car_type_click').attr('data-check','0');
                $('#car_type_click').removeClass('fa-minus-circle');
                $('#car_type_click').addClass('fa-plus-circle');

            }


        });
    </script>

@endsection
