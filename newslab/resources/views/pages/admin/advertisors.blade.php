{{-- Extends layout --}}
@extends('layout.admin')

{{-- Content --}}
@section('content')

    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pb-0">
            <div class="card-title">
                <h3 class="card-label">All ADVERTISORS
                    <div class="text-muted pt-2 font-size-sm"></div>
                </h3>
            </div>
{{--            <div class="card-toolbar">--}}

{{--                <select class="data-table-10list float-right">--}}
{{--                    <option value="">10</option>--}}
{{--                    <option value="">20</option>--}}
{{--                </select>--}}
{{--            </div>--}}
        </div>

        <div class="card-body pt-0">


            <div class="mt-0 mb-2 mt-lg-5 " id="table-search-form">
                {{--<option   <?php  echo (null!=($request->meal_type_id)&&($meal_type->id ==$request->meal_type_id))?'selected':''; ?> value="{{$meal_type->id}}">{{$meal_type->name}}</option>--}}
                <form action="#" id="frm">
                    <div class="row align-items-center">
                        <div class="col-lg-12 col-xl-12  ">
                            <div class="row align-items-center">
                                <div class="col-md-3 my-2 my-md-0  offset-2">
                                    <div class="input-icon">
                                        <input type="text" name="search" class="form-control" placeholder="Search by name..."
                                               value="{{(isset($_GET['search']))?$_GET['search']:''}}"
                                               id="kt_datatable_search_query"/>
                                        <span><i class="flaticon2-search-1 text-muted"></i></span>
                                    </div>
                                </div>

                                <div class="col-md-3 my-2 my-md-0 ">
                                    <div class="d-flex align-items-center">
{{--                                        <label class=" mb-0 d-none d-md-block">Email:</label>&nbsp--}}
                                        <input type="email" name="email" class="form-control" placeholder="email..."
                                               value="{{(isset($_GET['email']))?$_GET['email']:''}}"
                                               id="kt_datatable_search_query"/>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-xl-4 ">
                                    <a href="#" onclick="document.getElementById('frm').submit();"
                                       class="btn btn-light-primary px-6 font-weight-bold">
                                        Search
                                    </a>
                                    @if(isset($_GET['search']) || isset($_GET['email']))
                                        @if( $_GET['email']!=null || $_GET['search']!=null)
                                    <a href="#" onclick="window.location.replace('{{route('getAllAdvertisors')}}')"
                                       class="btn btn-light-primary px-6 font-weight-bold">
                                        reset
                                    </a>
                                            @endif
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>



            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">Id.</th>
                            <th style="min-width: 130px" class="">
                                <span class="">Name</span>
                            </th>
                            <th style="min-width: 100px">Email</th>
                            <th>Total Campaigns</th>
                            <th class="fix-table-head" style="min-width: 135px;">Created at</th>
                            <th style="min-width: 100px">Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php
                        $key=1;
                        @endphp
                        @forelse($users as $user)
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$user->id}}</span>
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">

                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$user->name}}</a>
                                        {{--                                        <span class="text-muted font-weight-bold d-block">more infor sample </span>--}}
                                    </div>
                                </div>
                            </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center text-center">
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$user->email}}</a>
                                    </div>
                                </div>
                            </td>
                            <td class="pl-0 py-8">
                                <div class="text-center">
                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$user->campaigns_count}}</a>
                                    </div>
                                </div>
                            </td>
{{--                            <td>--}}
{{--                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{@$user->role->name}}</span>--}}
{{--                                --}}{{--                                <span class="text-muted font-weight-bold">In Proccess</span>--}}
{{--                            </td>--}}
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg"></span>
                                                                <span class="text-muted font-weight-bold">{{@$user->created_at}}</span>
                            </td>  <td>
                                <span class="switch switch-outline switch-icon switch-success">
                                    <label>
                                        <input type="checkbox"  id="{{$user->id}}" name="select" value="{{$user->id}}" class="status" {{($user->status==1)?'checked':''}}>
                                        <span></span>
                                    </label>
                                </span>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="6">
                                    <span class="border p-5 text-dark-75 font-weight-bolder d-block font-size-lg text-center">No user found</span>
                                </td>
                            </tr>

                        @endforelse
                        </tbody>
                    </table>
                    <div class="float-right pr-5 ">
                        <span class="pl-10">Total: {{$users->total()}} </span>
                        {{$users->links()}}
                    </div>
                </div>
    <!--end::Table-->
            </div>
    <!-- HTML Table End Here -->
        </div>
    </div>





@endsection

{{-- Styles Section --}}



{{-- Scripts Section --}}
@section('scripts')
{{--    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>--}}


{{--<link href="https://rawgit.com/t4t5/sweetalert/master/dist/sweetalert.css" rel="stylesheet">--}}
{{--<script src="https://rawgit.com/t4t5/sweetalert/master/dist/sweetalert.min.js"></script>--}}
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    {{-- vendors --}}
    <script>
        $('.status').click(function() {
            var id=$(this).val()
            if ($(this).is(':checked')){
                swal({
                    title: "Are you sure?",
                    text: "You want to activate this user?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                    if (willDelete) {
                        var data = {
                            'id':id,
                            'status':1,
                        }
                        $.ajax({
                            url: '{{route('userStatusUpdate')}}',
                            type: "GET",
                            data: data,
                            success: function (result) {
                                if (result=='1') {
                                    toastr.success('User activated successfully');
                                    // Swal.fire({title:'User activated!', icon: 'success'});
                                } else{
                                    Swal.fire({title:'User not activated! try again', icon: 'warning'});
                                }
                            }
                        })
                    } else {
                            $("#"+id).prop('checked', function(_, checked) {
                                return !checked;
                            });
                    }
                });
            }
            else {
                swal({
                    title: "Are you sure?",
                    text: "You want to deactivate this user?",
                    icon: "error",
                    buttons: true,
                    dangerMode: true,
                })
                    .then((willDelete) => {
                        if (willDelete) {
                            var data = {
                                'id': id,
                                'status': 0,
                            }
                            $.ajax({
                                url: '{{route('userStatusUpdate')}}',
                                type: "GET",
                                data: data,
                                success: function (result) {
                                    if (result == '1') {
                                        toastr.error('User deactivated successfully');
                                        // Swal.fire({title: 'User deactivated!', icon: 'success'});
                                    } else {
                                        Swal.fire({title: 'User not deactivated!', icon: 'warning'});
                                    }
                                }
                            })
                        } else {
                            $("#" + id).prop('checked', function (_, checked) {
                                return !checked;
                            });
                        }
                    });

            }
            });








        // function del()
        // {
        //     Swal.fire({
        //         title: 'Are you sure?',
        //         text: "You want to deactivate this user",
        //         icon: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085D6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Yes, deactivate it!'
        //     }).then((result) => {
        //         if (result.isConfirmed) {
        //             Swal.fire(
        //                 'Deactivated!',
        //                 'User deactivated successfully.',
        //                 'success'
        //             )
        //         }
        //     });
        // }

</script>


@endsection
