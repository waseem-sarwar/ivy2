@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout='layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout='layout.driver'; ?>
@elseif($role=='2')
    <?php $layout='layout.default'; ?>
@elseif($role=='1')
    <?php $layout='layout.admin'; ?>
@endif
@extends($layout)
@section('content')
    <style>
        .fix-table-column {
            position: absolute;
            width: 160px;
            height: 99px;
            padding-top: 34px !important;
            right: 0;
            background-color: #ffffff;
        }.fix-table-column-old2 {
             position: absolute;
             width: 160px;
             right: 0;
             top: auto;
             border-top-width: 1px;
             margin-top: 25px;
             background-color: #ffffff;
         }
        .fix-table-column-old {
            position: absolute;
            width: 160px;
            right: 0;
            top: auto;
            border-top-width: 1px;
            margin-top: 22px;
            background-color: #ffffff;
        }
        .fix-table-head{
            position: sticky;
            right: 0;
        }
    </style>
    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Manage Notifications
                    <div class="text-muted pt-2 font-size-sm"> </div>
                </h3>
            </div>
{{--            <div class="card-toolbar">
--}}{{--            <select class="data-table-10list float-right">--}}{{--
--}}{{--                <option value="">10</option>--}}{{--
--}}{{--                <option value="">20</option>--}}{{--
--}}{{--            </select>--}}{{--
                <a   href="javascript:;"  data-toggle="modal" data-target="#createModal" class="btn btn-primary font-weight-bolder">
                <span class="svg-icon svg-icon-md">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24"
                         version="1.1">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <rect x="0" y="0" width="24" height="24"/>
                            <circle fill="#000000" cx="9" cy="15" r="6"/>
                            <path
                                d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z"
                                fill="#000000" opacity="0.3"/>
                        </g>
                    </svg>
                    <!--end::Svg Icon-->
                </span>Add Notification</a>

                <!--end::Button-->
            </div>--}}
        </div>

        <div class="card-body">

            <!--begin::Search Form-->


            <!-- HTML Table Begin Here -->
            <div class="tab-content" id="table-scroll">
                <!--begin::Table-->
                <div class="table-responsive">
                    <table class="table main-table table-head-custom table-head-bg table-borderless table-vertical-center">
                        <thead>
                        <tr class="text-left text-uppercase">
                            <th style="min-width: 100px">Id</th>
                            <th style="min-width: 130px" class="">
                                <span class="">Name</span>
                            </th>
                            <th>
                              User Type
                            </th>
                            <th style="min-width: 100px">Status</th>


                            <th class="fix-table-head" style="min-width: 135px;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                    @foreach($notifications as $notification)
                        <tr>
                            <td>
                                <span class="text-dark-75 font-weight-bolder d-block font-size-lg">{{$notification->id}}</span>
                              </td>
                            <td class="pl-0 py-8">
                                <div class="d-flex align-items-center">

                                    <div>
                                        <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$notification->name}}</a>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="d-flex align-items-center">
                                    <div>
                                        @if($notification->role)
                                            <a href="#"   class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">{{$notification->role->name}}</a>
                                        @else
                                            <a href="#"   class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">All Users</a>
                                        @endif

                                    </div>
                                </div>
                            </td>

                            <td class="pl-0 py-8">


                                @if($notification->status=='1')
                                    <span class="label label-lg  label-light-success label-inline">Active</span>
                                @else
                                    <span class="label label-lg  label-light-danger label-inline">In Active</span>
                                @endif
                            </td>




                            <td class="pr-0 ">
                                <a href="javascript:;"  onclick="upd({{$notification}});" data-toggle="modal" data-target="#editModal" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="view details">
                                    <i class="la la-edit"></i>
                                </a>
{{--                                <a href="javascript:;"   onclick="del();" class="btn btn-sm btn-default btn-text-primary btn-hover-primary btn-icon mr-2" title="view details">--}}
{{--                                    <i class="la la-trash"></i>--}}
{{--                                </a>--}}

                            </td>

                        </tr>
                        @endforeach

                        </tbody>

                    </table>
                    <nav aria-label="Page navigation example ">
                        <ul class="pagination float-right mt-5">
                            <h6 class="pt-3">Total records:{{$notifications->total()}}  </h6>  &nbsp &nbsp&nbsp  {{$notifications->links()}}
                        </ul>
                    </nav>
                </div>
                <!--end::Table-->
            </div>
            <!-- HTML Table End Here -->
        </div>
    </div>

    <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Insert Noftification Details</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card" style="border:none;" >
                        <div class="card-body  p-0">
                            <!--begin::Invoice-->
                            <div class="row " >
                                <form id="form-id3" class="form" method="POST" action="{{ route('create.notification') }}" >
                                    @csrf

                                    @if ($errors->any())
                                        <div class="alert alert-danger" style="background-color: #e0afb4; ">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li class="float-left">{{ $error }}</li><br>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                <div class="col-md-10 offset-1 ">

                                    <div class=" row mt-5 mb-5  " >

                                        <div class="col-md-12 ">
                                            <label class="mt-2">Name:</label>
                                            <input type="text" class="form-control" name="name" id="name" required
                                                   placeholder="Notification name" >
                                            <div class="d-md-none mb-2"></div>
                                        </div>

                                            <label class="col-xl-4 col-lg-4 col-form-label mt-2">For</label>
                                            <div class="col-lg-8 col-xl-8">
                                                <select class="form-control form-control-lg  mt-2" name="role">
                                                    @foreach($roles as $role )
                                                    <option  value="{{$role->id}}" >{{$role->name}}</option>
                                                    @endforeach
                                                        <option value="0" >All</option>
                                                      </select>
                                            </div>



                                            <label class="col-xl-4 col-lg-4 col-form-label font-weight-bold text-left text-lg-left mt-2">Status</label>
                                            <span></span>
                                            <div class="col-lg-8 col-xl-8 mt-2">
                                                <span class="switch switch-sm">
                                                    <label>
                                                        <input type="checkbox" name="status" checked>
                                                        <span></span>
                                                    </label>
												</span>
                                            </div>


                                    </div>
                                </div>
                                </form>
                                <!--end::Invoice-->
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" onclick="document.getElementById('form-id3').submit();"
                       class="btn btn-sm font-weight-bolder btn-light-primary">
                        Submit
                    </a>&nbsp
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>



                    <!--                                        <button type="button" class="btn btn-primary font-weight-bold">Save changes</button>-->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalSizeSm"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-md" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Noftification Detail Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card" style="border:none;" >
                        <div class="card-body  p-0">
                            <!--begin::Invoice-->
                            <div class="row ">
                                <form id="form-2"  class="form" method="POST" action="{{ route('update.notification1') }}" >
                                    @csrf
                                <div class="col-md-10 offset-1 ">

                                    <div class=" row mt-5 mb-5  " >

                                        <div class="col-md-12 ">
                                            <label class="mt-2">Name:</label>
                                            <input type="text" class="form-control" name="update_name" disabled  id="update_name"
                                                   placeholder="Notification name" >
                                             <div class="d-md-none mb-2"></div>
                                        </div>

                                        <div class="form-group row align-items-center mt-3 ml-1">
                                            <label class="col-xl-10 col-lg-9 col-form-label font-weight-bold text-left text-lg-left">Status</label>
                                            <span></span>
                                            <div class="col-lg-3 col-xl-3 offset-5">
                                                <span class="switch switch-sm">
                                                    <label>
                                                         <input type="hidden"  name="update_id" id="update_id">
                                                        <input type="checkbox" id="update_status" name="update_status">
                                                        <span></span>
                                                    </label>
												</span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                </form>
                                <!--end::Invoice-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="document.getElementById('form-2').submit();" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Update
                    </button>
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Back
                    </button>
                </div>
            </div>
        </div>
    </div>










@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>
@endsection


{{-- Scripts Section --}}
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{-- vendors --}}
    <script>

        function upd(notification) {
            $('#update_name').val(notification.name);
            $('#update_id').val(notification.id);
            if(notification.status=='1'){
                // console.log(notification.status);
            $("#update_status").attr("checked", "checked");
            }
            else{
                $("#update_status").removeAttr("checked");
            }
        }
        function del()
        {
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085D6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    )
                }
            });
        }


        $('#kt_datatable').dataTable({
            "scrollX": true
        });
    </script>
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>

    {{-- page scripts --}}
    <script src="{{ asset('js/pages/crud/datatables/basic/basic.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/pages/custom/contacts/list-datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>

    <script>

        $(document).on('click', '#toggleSearchForm', function(){

            $('#table-search-form').slideToggle();

        });


        @if (\Session::has('updated'))
        toastr.info('{!! \Session::get('updated') !!}', 'Updated');
        @endif
        @if (\Session::has('created'))
        toastr.success('{!! \Session::get('created') !!}', 'Created Successfully');
        @endif

        @if ($errors->any())
        $('#createModal').modal();
        @endif


    </script>

@endsection
