<div class="form-group">
    <div>
        <label class="mt-1 d-flex justify-content-between align-items-center" for="car_name">
            <span>Select Car Name:</span>
            <i id="car_name_click" class="fa fa-plus-circle ml-1 cursor-pointer"></i>
        </label>
        <select class="form-control" name="car_name" id="car_name" onchange="getmodel(this.value)">
            <option value="" selected disabled>select name</option>
            @if(isset($car_name))
                @foreach($car_name as $car)
                    <option value="{{$car->id}}">{{$car->car_name}}</option>
                @endforeach
            @endif
        </select>
    </div>

    <div id="elem_car_name_option" style="display: none;">
        <div class="mt-3 d-flex align-items-center justify-content-between">
            <div class="form-group">
                <label for="inp_car_name_option" class="mt-1">Write Car Option
                    Name</label>
                <input type="text" class="form-control" name="inp_car_name_option"
                       id="inp_car_name_option" placeholder="Write name">
            </div>
            <button type="button" id="btn_car_name_option"
                    class="btn btn-primary-revert">Save
            </button>
        </div>
    </div>
</div>


