@if(isset($carlists))
@php
$key=1;
@endphp
@forelse($carlists as $carlist)
    <tr>
    <td>{{$key}}</td>
    <td>{{$carlist->maker->company}}</td>
    <td>{{$carlist->carName->car_name}}</td>
    <td>{{$carlist->model->model_name}}</td>
    <td>{{$carlist->carType->name}}</td>
    <td>{{$carlist->created_at}}</td>
    <td>
        @if($carlist->status == 1)
        <a href="{{route('deleteCar',[$carlist->id])}}" class="btn btn-sm btn-light-danger">Disable</a>
        @else
            <a href="{{route('deleteCar',[$carlist->id])}}" class="btn btn-sm btn-light-success">Enable</a>
        @endif
    </td>
        @php
            $key++;
        @endphp
    </tr>
    @empty
    <tr>
        <td colspan="7" class="text-center">No car found</td>
    </tr>

@endforelse
@endif
