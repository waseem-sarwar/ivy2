@php
    $role=Auth::user()->role_id;
@endphp
@if($role=='4')
    <?php $layout='layout.ent_driver'; ?>
@elseif($role=='3')
    <?php $layout='layout.driver'; ?>
@elseif($role=='2')
    <?php $layout='layout.default'; ?>
@elseif($role=='1')
    <?php $layout='layout.admin'; ?>
@endif
@extends($layout)

@section('styles')

    {{--    <link href="{{ asset('css/pages/wizard/wizard-3.css') }}" rel="stylesheet" type="text/css" />--}}
    <style>
        .hov1:hover {
            cursor: pointer;
            color:#b4d7ff !important;
        }
        .hov2:hover {
            cursor: pointer;
            color:#b4d7ff !important;
        }
        .side-scoller {
            height: 50vh;
            overflow: hidden;
            overflow-y: scroll;
        }
    </style>
@endsection
{{-- Content --}}
@section('content')

    <!--begin::Content-->
    <!--begin::Content-->
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <!--begin::Entry-->
        <div class="d-flex flex-column-fluid">
            <!--begin::Container-->
            <div class="container">
                <!--begin::Chat-->
                <div class="d-flex flex-row">
                    <!--begin::Aside-->
                    <div class="flex-row-auto offcanvas-mobile w-350px w-xl-400px" id="kt_chat_aside">
                        <!--begin::Card-->
                        <div class="card card-custom">
                            <!--begin::Body-->
                            <div class="card-body">
                                <!--begin:Search-->
                                <div class="input-group input-group-solid">
                                </div>
                                <!--end:Search-->
                                <!--begin:Users-->
                                <div class="mt-7 scroll scroll-pull side-scoller" style="height: 67vh">
                                    <!--begin:User-->
                                    @foreach($users as $user)
                                        <div class=" user d-flex align-items-center justify-content-between mb-5" id="{{$user->id}}">
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-50 mr-3">
                                                    <div class="d-flex flex-column align-items-end">
                                                        <span class="text-muted font-weight-bold font-size-sm">5 mins</span>
                                                        <span class="label label-sm label-success">9</span>
                                                    </div>
                                                    <img alt="Pic" src="{{ asset('media/users/default.jpg') }}" />
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <a class="text-dark-75 text-hover-primary font-weight-bold font-size-lg">{{$user->name}}</a>
                                                    <span class="text-muted font-weight-bold font-size-sm">{{$user->email}}</span>
                                                    {{--                                                <span class="text-muted font-weight-bold font-size-sm">{{$user->id}}</span>--}}
                                                </div>

                                            </div>
                                        </div>
                                @endforeach
                                <!--end:User-->
                                </div>
                                <!--end:Users-->
                            </div>
                            <!--end::Body-->
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Aside-->
                    <!--begin::Content-->
                    <div class="flex-row-fluid ml-lg-8" id="kt_chat_content">
                        <!--begin::Card-->
                        <div class="card card-custom">
                            <!--begin::Header-->
                            <div class="card-header align-items-center px-4 py-3">
                                <div class="text-left flex-grow-1">
                                    <!--begin::Aside Mobile Toggle-->
                                    <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md d-lg-none" id="kt_app_chat_toggle">
														<span class="svg-icon svg-icon-lg">
															<!--begin::Svg Icon | path:assets/media/svg/icons/Communication/Adress-book2.svg-->
															<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																	<rect x="0" y="0" width="24" height="24" />
																	<path d="M18,2 L20,2 C21.6568542,2 23,3.34314575 23,5 L23,19 C23,20.6568542 21.6568542,22 20,22 L18,22 L18,2 Z" fill="#000000" opacity="0.3" />
																	<path d="M5,2 L17,2 C18.6568542,2 20,3.34314575 20,5 L20,19 C20,20.6568542 18.6568542,22 17,22 L5,22 C4.44771525,22 4,21.5522847 4,21 L4,3 C4,2.44771525 4.44771525,2 5,2 Z M12,11 C13.1045695,11 14,10.1045695 14,9 C14,7.8954305 13.1045695,7 12,7 C10.8954305,7 10,7.8954305 10,9 C10,10.1045695 10.8954305,11 12,11 Z M7.00036205,16.4995035 C6.98863236,16.6619875 7.26484009,17 7.4041679,17 C11.463736,17 14.5228466,17 16.5815,17 C16.9988413,17 17.0053266,16.6221713 16.9988413,16.5 C16.8360465,13.4332455 14.6506758,12 11.9907452,12 C9.36772908,12 7.21569918,13.5165724 7.00036205,16.4995035 Z" fill="#000000" />
																</g>
															</svg>
                                                            <!--end::Svg Icon-->
														</span>
                                    </button>
                                    <!--end::Aside Mobile Toggle-->
                                    <!--begin::Dropdown Menu-->
                                    <div class="dropdown dropdown-inline">
                                        <button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="ki ki-bold-more-hor icon-md"></i>
                                        </button>

                                    </div>
                                    <!--end::Dropdown Menu-->
                                </div>

                                <div class="text-center flex-grow-1">
                                    <div class="text-dark-75 font-weight-bold font-size-h5">{{Auth()->user()->name}}</div>
                                    {{--                                    <div>--}}
                                    {{--                                        <span class="label label-sm label-dot label-success"></span>--}}
                                    {{--                                        <span class="font-weight-bold text-muted font-size-sm">Active</span>--}}
                                    {{--                                    </div>--}}
                                </div>
                                <div class="text-right flex-grow-1">

                                </div>
                            </div>
                            <!--end::Header-->
                            <!--begin::Body-->
                            <div class="card-body side-scoller">
                                <!--begin::Scroll-->
                                <div class="scroll scroll-pull" data-mobile-height="350">
                                    <!--begin::Messages-->
                                    <div class="messages" id="messages">
                                        <!--begin::Message In-->


                                    </div>
                                    <!--end::Messages-->
                                </div>
                                <!--end::Scroll-->
                            </div>
                            <!--end::Body-->
                            <!--begin::Footer-->
                            <div class="card-footer align-items-center input-text">
                                <!--begin::Compose-->
                                <input class="form-control border-0 p-0 submit" rows="2" name="message" type="text" placeholder="Type a message">
                                <div class="d-flex align-items-center justify-content-end mt-5">
                                    {{--                                                                        <div class="mr-3">--}}

                                    {{--                                                                            <label for="file" title="Upload image">--}}
                                    {{--                                                                                <i class="flaticon2-photo-camera icon-lg hov1" ></i>--}}
                                    {{--                                                                                <input type="file" id="file" style="display: none" name="image" accept="image/gif,image/jpeg,image/jpg,image/png" multiple="" data-original-title="upload photos">--}}
                                    {{--                                                                            </label> &nbsp--}}
                                    {{--                                                                            <label for="file" title="Upload file">--}}
                                    {{--                                                                                <i class="fa fa-upload  icon-lg hov2" ></i>--}}
                                    {{--                                                                                <input type="file" id="file" style="display: none" name="image"  multiple="" data-original-title="upload file">--}}
                                    {{--                                                                            </label>--}}
                                    {{--                                                                        </div>--}}


                                    <div>
                                        <button type="button" id="#snd_btn" class="btn btn-primary btn-md text-uppercase font-weight-bold chat-send py-2 px-6">Send</button>
                                    </div>
                                </div>
                                <!--begin::Compose-->
                            </div>
                            <!--end::Footer-->
                        </div>
                        <!--end::Card-->
                    </div>
                    <!--end::Content-->
                </div>
                <!--end::Chat-->
            </div>
            <!--end::Container-->
        </div>
        <!--end::Entry-->
    </div>
    <!--end::Content-->

    <!--end::Content-->

@endsection

{{-- Scripts Section --}}
@section('scripts')
    <script src="{{ asset('js/pages/widgets.js') }}" type="text/javascript"></script>
    {{--    <script src="{{ asset('js/pages/custom/wizard/wizard-3.js') }}" type="text/javascript"></script>--}}
    {{--    <script src="assets/js/pages/custom/wizard/wizard-3.js"></script>--}}
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script>
        var receiver_id='';
        var my_id="{{\Illuminate\Support\Facades\Auth::id()}}";
        $(document).ready(function (){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;

            var pusher = new Pusher('ad6a3e54ebdd15a3f395', {
                cluster: 'ap2',
            });

            var channel = pusher.subscribe('my-channel');
            // console.log(channel , 'test')
// Bind a function to a Event (the full Laravel class)
            channel.bind('my-event', function(data) {
                // alert(JSON.stringify(data));
                // alert(data.message.from);
                if (my_id ==  data.message.from){
                    // alert('from');
                } else if (my_id == data.message.to){

                    if (receiver_id == data.message.from){

                        $('#'+data.message.from).click()

                    }else {
                        var pending =parseInt($('#' + data.message.from).find('.pending').html());
                        if(pending){
                            $('#'+data.message.from).find('.pending').html(pending + 1)
                        }else {
                            $('#'+data.message.from).append('<span class="pending">1</span>')
                        }
                    }
                }

            });



            $(document).ready(function () {
                $('.user').click(function () {
                    $('.user').removeClass('active');
                    $(this).addClass('active');
                    receiver_id=$(this).attr('id');
                    // alert(receiver_id);
                    $.ajax({
                        type: "GET",
                        url: "/message/" + receiver_id,
                        data: "",
                        cache:false,
                        success:function (data) {
                            $('#messages').html(data);

                        }
                    });
                });

            });
            $(document).on('keyup','.input-text input', function (e) {
                var message=$(this).val()
                if(e.keyCode == 13 && message !='' && receiver_id !=''){
                    $(this).val('');
                    var datastr="receiver_id="+receiver_id+"&message="+message;
                    // alert(datastr);
                    $.ajax({
                        type:"POST",
                        url:"/message",
                        data:datastr,
                        cache:false,
                        success:function (data) {

                        },
                        error:function (jqXHR,status,err) {

                        },
                        completet:function(){

                        }


                    })

                }
            });
        });
    </script>
@endsection
