@extends('errors::illustrated-layout')

@section('code', '😭')

@section('title', __('Server error'))

@section('image')

    <div style="background-image: url('{{asset('images/errors/error_500.jpg')}}');" class="absolute pin bg-no-repeat md:bg-left lg:bg-center">
    </div>
    <style>
        .tracking-wide{
            margin-top:20px;
            text-align:center;
            margin-left:25px;
        }
        .bg-purple-light{
            display: none !important;
        } .leading-normal{
              display: none !important;
          }

    </style>
@endsection

@section('message', __('Error occured'))
@section('message', __('Server Error'))
