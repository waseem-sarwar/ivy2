@extends('errors::illustrated-layout')

@section('code', '😭')

@section('title', __('Action  unauthorized'))

@section('image')

    <div style="background-image: url('{{asset('images/errors/403.png')}}');" class="absolute pin bg-no-repeat md:bg-left lg:bg-center">
    </div>
    <style>
        .tracking-wide{
           margin-top:20px;
           text-align:center;
           margin-left:25px;
        }
        .bg-purple-light{
            display: none !important;
        } .leading-normal{
              display: none !important;
          }

    </style>
@endsection

@section('message', __('This action is unauthorized'))
