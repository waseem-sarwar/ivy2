@extends('errors::illustrated-layout')

@section('code', '😭')

@section('title', __('Page Not Found'))

@section('image')

    <div style="background-image: url('{{asset('images/errors/404-bg.jpg')}}');" class="absolute pin bg-no-repeat md:bg-left lg:bg-center">
   </div>
<style>
    .tracking-wide{
        display: none !important;
    }
    .bg-purple-light{
        display: none !important;
    } .leading-normal{
        display: none !important;
    }

</style>
@endsection

@section('message', __('Sorry, the page you are looking for could not be found.'))
