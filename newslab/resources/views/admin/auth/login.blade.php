{{--@extends('admin.layout.auth')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-8 col-md-offset-2">--}}
{{--            <div class="panel panel-default">--}}
{{--                <div class="panel-heading">Login</div>--}}
{{--                <div class="panel-body">--}}
{{--                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">--}}
{{--                        {{ csrf_field() }}--}}

{{--                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" autofocus>--}}

{{--                                @if ($errors->has('email'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('email') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
{{--                            <label for="password" class="col-md-4 control-label">Password</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control" name="password">--}}

{{--                                @if ($errors->has('password'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('password') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="col-md-6 col-md-offset-4">--}}
{{--                                <div class="checkbox">--}}
{{--                                    <label>--}}
{{--                                        <input type="checkbox" name="remember"> Remember Me--}}
{{--                                    </label>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="col-md-8 col-md-offset-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    Login--}}
{{--                                </button>--}}

{{--                                <a class="btn btn-link" href="{{ url('/admin/password/reset') }}">--}}
{{--                                    Forgot Your Password?--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}

@if(Auth::check())
    <script>
        window.location.replace('{{route('hom')}}');

    </script>
@endif





<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('site/css/allstyles.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" type="text/css" href="{{ asset('site/style.css') }}">
</head>

<body class="">
<div class="accountWrap">
    <header class="header accountHeader">
        <div class="topBarWrap">
            <div class="container-fluid">
                <div class="topBar">
                    <div class="left">
                        <a  href="{{route('home')}}" class="logo"><img src="{{ asset('site/images/logo.png')}}" height="" width="" alt="logo"></a>
                    </div>
                    <div class="right">
                        <nav class="main-menu-nav ">
                            <div class="body-overlay"></div>
                            <div class="menu-toggler"></div>
                            <div class="main-menu-div">
                                <div class="menu-header">
                                    <a href="#" title="Home" class="home-icon fa fa-home"></a>
                                    <span class="close-icon fa fa-times"></span>
                                </div>
                                <ul>
                                    <li class="active">
                                        <a href="{{route('home')}}" title="home">Home</a>
                                    </li>
                                    <li>
                                        <a href="" title="about">Business</a>
                                        <ul>
                                            <li><a href="{{url('/adv/sign-up')}}">Business Signup</a></li>
                                            <li><a href="{{ url('/terms') }}">Advertiser Benefits</a></li>
                                            <li><a href="{{ url('/how-it-works') }}">How it work?</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{ url('/about') }}" title="about">About</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/contact') }}" title="about">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </nav>
                        <div class="headerButtons">
                            <button class="btn adverBtn" onclick="window.location.href='{{url('adv/sign-up')}}';">Advertiser Signup</button>
                            <button class="btn adverBtn" onclick="window.location.href='{{url('drv/sign-up')}}';">Driver Signup</button>
                            {{--                        <button class="btn driverBtn">Driver Signup</button>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main class="main">
        <section class="enquirySection">
            <h1>Signin</h1>
            @if(isset($errors)&& count($errors)>0)
                <span  class="text-danger">Credentials not match</span>
            @endif
            @if(Session::has('unauthorize'))
                <span class="text-danger">{{ Session::get('unauthorize') }}</span>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/login') }}">
{{--            <form method="POST" action="{{ route('login') }}">--}}
                @csrf
                <div class="form-group">
                    <input type="email" name="email" placeholder="Email" class="form-control"  value="{{old('email')}}" required autocomplete="false">
                    <i class="flaticon-user icon"></i>
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="Password" class="form-control" required>
                    <i class="flaticon-lock icon"></i>
                </div>
                <div class="formFlex form-group">
                    <a href="{{route('admin.password.reset')}}">Forgot Password?</a>
                </div>
                <button type="submit" class="btn primaryBtn">Sign In</button>
            </form>
        </section>
    </main>
</div>
<script type="text/javascript " src="{{ asset('site/js/jquery.min.js')}} "></script>
<script src="{{ asset('site/js/plugins.js')}} "></script>
<script src="{{ asset('site/js/system.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css" integrity="sha512-3pIirOrwegjM6erE5gPSwkUzO+3cTjpnV9lexlNZqvupR64iZBnOOTiiLPb9M36zpMScbmUNIcHUqKD47M719g==" crossorigin="anonymous" />

<script>

    @if (\Session::has('message'))
    toastr.success('{!! \Session::get('success') !!}', 'Logged out successfully');
    @endif
</script>
</body>

</html>
