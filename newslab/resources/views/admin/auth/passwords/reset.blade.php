{{--@extends('admin.layout.auth')--}}

{{--@section('content')--}}
{{--<div class="container">--}}
{{--    <div class="row">--}}
{{--        <div class="col-md-8 col-md-offset-2">--}}
{{--            <div class="panel panel-default">--}}
{{--                <div class="panel-heading">Reset Password</div>--}}

{{--                <div class="panel-body">--}}
{{--                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/password/reset') }}">--}}
{{--                        {{ csrf_field() }}--}}

{{--                        <input type="hidden" name="token" value="{{ $token }}">--}}

{{--                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">--}}
{{--                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" autofocus>--}}

{{--                                @if ($errors->has('email'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('email') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">--}}
{{--                            <label for="password" class="col-md-4 control-label">Password</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control" name="password">--}}

{{--                                @if ($errors->has('password'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('password') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">--}}
{{--                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>--}}
{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">--}}

{{--                                @if ($errors->has('password_confirmation'))--}}
{{--                                    <span class="help-block">--}}
{{--                                        <strong>{{ $errors->first('password_confirmation') }}</strong>--}}
{{--                                    </span>--}}
{{--                                @endif--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="form-group">--}}
{{--                            <div class="col-md-6 col-md-offset-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    Reset Password--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--@endsection--}}







@extends('pages.site.layout.login')
@section('content')


    <main class="main">
        <section class="enquirySection">

            <h1>Reset Your Password</h1>
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/admin/password/reset') }}">
                @csrf
                @if($errors->any())
                    <div class="form-group alert alert-danger">
                        {{ $errors->first('password') }}
                        {{ $errors->first('email') }}
                    </div>
                @endif
                @if (session('status'))
                    <div class="form-group alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="ml-4 control-label float-left">E-Mail Address</label>
                    <input  id="email" type="email"  placeholder="Email" class="form-control" name="email"  value="{{old('email') }}" required autocomplete="email" autofocus>
                    <i class="flaticon-user icon"></i>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                         <strong>{{ $errors->first('email') }}</strong>
                </span>
                    @endif
                </div>




                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="ml-4 control-label float-left">Password</label>
                    <input id="password" type="password" placeholder="New Password" class="form-control " name="password" value="{{ old('password') }}" required autocomplete="off" autofocus>
                    <i class="flaticon-user icon"></i>
                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                </span>
                    @endif

                </div>





                <div class="form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password_confirmation" class="sr-only d-none">Confirm New Password</label>
                    <input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirm New Password" class="form-control"  required autocomplete="off" autofocus>
                    <i class="flaticon-user icon"></i>
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
                    @endif

                </div>

                <div class="formFlex form-group">
                    <p></p>
{{--                    <a href="{{route('login')}}">Signin?</a>--}}

                </div>
                <button type="submit" class="btn primaryBtn">RESET PASSWORD</button>
            </form>
        </section>
    </main>
@endsection
