
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" {{ Metronic::printAttrs('html') }} {{ Metronic::printClasses('html') }}>
<head>
    <meta charset="utf-8"/>

    {{-- Title Section --}}
    <title>{{ config('app.name') }} | @yield('title', $page_title ?? '')</title>

    {{-- Meta Data --}}
    <meta name="description" content="@yield('page_description', $page_description ?? '')"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
    <link rel = "shortcut icon" href ="{{asset('/site/images/about-car2.png')}}" type = "image/x-icon">

    {{-- Favicon --}}
{{--    <link rel="shortcut icon" href="{{ asset('media/logos/favicon.ico') }}" />--}}

    {{-- Fonts --}}
    {{ Metronic::getGoogleFontsInclude() }}

    {{-- Global Theme Styles (used by all pages) --}}
    @foreach(config('layout.resources.css') as $style)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($style)) : asset($style) }}" rel="stylesheet" type="text/css"/>
    @endforeach

    {{-- Layout Themes (used by all pages) --}}
    @foreach (Metronic::initThemes() as $theme)
        <link href="{{ config('layout.self.rtl') ? asset(Metronic::rtlCssPath($theme)) : asset($theme) }}" rel="stylesheet" type="text/css"/>
    @endforeach

    {{-- Includable CSS --}}
    @yield('styles')
</head>

<body {{ Metronic::printAttrs('body') }} {{ Metronic::printClasses('body') }}>

@if (config('layout.page-loader.type') != '')
    @include('layout.partials._page-loader')
@endif

@include('layout.ent_driver.base._layout')
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<script>var HOST_URL = "{{ route('quick-search') }}";</script>

{{-- Global Config (global config for global JS scripts) --}}
<script>
    var KTAppSettings = {!! json_encode(config('layout.js'), JSON_PRETTY_PRINT|JSON_UNESCAPED_SLASHES) !!};
</script>
<script type="text/javascript">
    var globalPublicPath = '{{ URL('/') }}';


</script>
<script>
    centralAjax = function (object) {
        var centralAjaxData = null;
        $.ajax({
            url: object.url,
            method: object.type,
            data: object.data,
            async: false,
            beforeSend: function () {
//                console.log('showing loading div fro simple ajax');
//                $('#loadingDiv').show();

            },

            success: function (result) {
//                $('#loadingDiv').hide();
                centralAjaxData = result
            },
            error: function (result) {
//                $('#loadingDiv').hide();
                alert('some thing went wrong');
//                console.log(result);
            }
        });
        return centralAjaxData;
    };


    centralAjaxForLatestMapData = function (object) {
        var centralAjaxData = null;
        $.ajax({
            url: object.url,
            method: object.type,
            data: object.data,
            async: false,
            beforeSend: function () {
//                console.log('showing loading div fro simple ajax');
                $('#loading_img_index').show();
            },

            success: function (result) {
                $('#loading_img_index').hide();
                centralAjaxData = result
            },
            error: function (result) {
                $('#loading_img_index').hide();
//                console.log(result);
            }
        });
        return centralAjaxData;
    };


    centralAjaxImageUpload = function (object) {
        var centralAjaxImageUploadData = null;
        $.ajax({
            url: object.url,
            method: object.type,
            data: object.data,
            async: false,
            contentType: false,
            processData: false,
            beforeSend: function () {
//                console.log('showing loading div from image upload ajax');
//                $('#loadingDiv').show();
            },

            success: function (result) {
//                $('#loadingDiv').hide();
                centralAjaxImageUploadData = result
            },
            error: function (result) {
//                $('#loadingDiv').hide();
                alert('Something went wrong');
//                console.log(result);
            }
        });
        return centralAjaxImageUploadData;
    };

    var letter_regix = new RegExp("^[A-Za-z\\s]+$");
    var number_regix = new RegExp("^\\d+$");


    function diff_years(dt2, dt1) {

        var diff = (dt2.getTime() - dt1.getTime()) / 1000;
        diff /= (60 * 60 * 24);

        return Math.abs(Math.floor(diff / 365.25));
    }

    function checkIsFromDirtyBeforeLeave(form_id) {
        var old_value = $('#' + form_id).serialize();
        $(window).on("beforeunload", function () {
            if (old_value != $('#' + form_id).serialize()) {
                return true;
            }
        });
        $("#" + form_id).on("submit", function (e) {

            $(window).off("beforeunload");
            return true;
        });
    }
</script>
<script>
    successNotificationCustom = function (message) {
        var successNotification = noty({
            text: message,
            type: 'success',
            layout: 'topRight',
            timeout: 5000,

            animation: {
                open: 'animated bounceInRight',
                close: 'animated bounceOutRight',

            }
        });

    };

    warningNotificationCustom = function (message, time) {

        time = time ? time : 5000;

        var warningNotification = noty({
            text: message,
            type: 'warning',
            layout: 'topRight',
            timeout: time,

            animation: {
                open: 'animated bounceInRight',
                close: 'animated bounceOutRight',

            }
        });

    };

    errorNotificationCustom = function (message) {
        var errorNotification = noty({
            text: message,
            type: 'error',
            layout: 'topRight',
            timeout: 5000,

            animation: {
                open: 'animated bounceInRight',
                close: 'animated bounceOutRight',

            }
        });

    };
</script>

{{-- Global Theme JS Bundle (used by all pages)  --}}
@foreach(config('layout.resources.js') as $script)
    <script src="{{ asset($script) }}" type="text/javascript"></script>
@endforeach

{{-- Includable JS --}}
@yield('scripts')

</body>
</html>

