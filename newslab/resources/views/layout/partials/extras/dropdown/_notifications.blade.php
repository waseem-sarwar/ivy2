
@if (config('layout.extras.notifications.dropdown.style') == 'light')
    <div class="d-flex flex-column pt-12 bg-dark-o-5 rounded-top">
        {{-- Title --}}
        <h4 class="d-flex flex-center">
            <span class="text-dark">Notifications</span>
            <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">{{(notificationCount())}} new</span>
        </h4>

        {{-- Tabs --}}
        <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-primary mt-3 px-8" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications">System Alerts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_events">Job Alerts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs">Logs</a>
            </li>
        </ul>
    </div>
@else
    <div class="d-flex flex-column pt-12 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url('{{ asset('media/misc/bg-1.jpg') }}')">
        {{-- Title --}}
        <h4 class="d-flex flex-center rounded-top">
            <span class="text-white">User Notifications</span>
            <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">{{notificationCount()}} new</span>
        </h4>

        {{-- Tabs --}}
        <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-line-transparent-white nav-tabs-line-active-border-success mt-3 px-8" role="tablist">
            <li class="nav-item">
                <a  class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications" >System Alerts</a>
            </li>
            <li class="nav-item">
                <a style="pointer:default;" class="nav-link " data-toggle="tab" href="#topbar_notifications_events" >Job Alerts</a>
            </li>
            {{--            <li class="nav-item">--}}
            {{--                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs" >Logs</a>--}}
            {{--            </li>--}}
            <li class="nav-item">
                <a   style="pointer:default;"  class="nav-link " data-toggle="tab"  href="#topbar_notifications_events1" >All </a>
            </li>
        </ul>
    </div>
@endif

{{-- Content --}}
<div class="tab-content px-4">
    {{-- Tabpane --}}
    <div class="tab-pane p-1 active show" id="topbar_notifications_notifications" role="tabpanel">
        {{-- Scroll --}}
        <div class="scroll pr-7 mr-n7" data-scroll="true" data-height="330" data-mobile-height="330">
            {{-- Item --}}
            @php $data=systemAlerts(); @endphp
            @if(count($data)==0)
                <p style="pointer:default !important;"  class="navi-item ">
                <div class="navi-link m-3 pt-3" >
                    <div class="navi-text text-center" style="border: 1px solid #eef0f8 !important; padding: 8px">
                        <div class="font-weight-bold my-5" >
                            Sorry, No notification found
                        </div>

                    </div>
                </div>
                </p>
            @endif
            @foreach($data as $dat)
                <div class="d-flex align-items-center mb-6 border-bottom py-2 px-2">
                    {{-- Symbol --}}
                    <div class=" symbol symbol-40 symbol-light-primary mr-5">
                                    <span class="symbol-label">
                                        {{ Metronic::getSVG("media/svg/icons/Home/Library.svg", "svg-icon-lg svg-icon-primary") }}
                                    </span>
                    </div>

                    {{-- Text --}}
                    <div class="d-flex flex-column font-weight-bold">
                        <p style="pointer:default !important;" class="text-dark text-hover-primary mb-1 font-size-lg">{{$dat->subject}}</p>
                        <span class="text-muted">{!!$dat->detail!!}  </span>
                        <div class="text-muted text-right float-right">
                            {{Carbon\Carbon::parse($dat->created_at)->diffForHumans()}}
                        </div>
                    </div>
                </div>
            @endforeach

            {{--            <div class="d-flex align-items-center mb-6">--}}
            {{--                --}}{{-- Symbol --}}
            {{--                <div class="symbol symbol-40 symbol-light-warning mr-5">--}}
            {{--                    <span class="symbol-label">--}}
            {{--                        {{ Metronic::getSVG("media/svg/icons/Communication/Write.svg", "svg-icon-lg svg-icon-warning") }}--}}
            {{--                    </span>--}}
            {{--                </div>--}}

            {{--                --}}{{-- Text --}}
            {{--                <div class="d-flex flex-column font-weight-bold">--}}
            {{--                    <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg">New Massage from Briviba </a>--}}
            {{--                    <span class="text-muted">hy , there what is the campaign status </span>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            --}}{{-- Item --}}
            {{--            <div class="d-flex align-items-center mb-6">--}}
            {{--                --}}{{-- Symbol --}}
            {{--                <div class="symbol symbol-40 symbol-light-success mr-5">--}}
            {{--                    <span class="symbol-label">--}}
            {{--                        {{ Metronic::getSVG("media/svg/icons/Communication/Group-chat.svg", "svg-icon-lg svg-icon-success") }}--}}
            {{--                    </span>--}}
            {{--                </div>--}}

            {{--                --}}{{-- Text --}}
            {{--                <div class="d-flex flex-column font-weight-bold">--}}
            {{--                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Support Department </a>--}}
            {{--                    <span class="text-muted">Issue HAS BEEN RESOLVED PLEASE CHECK</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}






            {{-- Item --}}
            {{--            <div class="d-flex align-items-center mb-6">--}}
            {{--                --}}{{-- Symbol --}}
            {{--                <div class="symbol symbol-40 symbol-light-danger mr-5">--}}
            {{--                    <span class="symbol-label">--}}
            {{--                        {{ Metronic::getSVG("media/svg/icons/General/Attachment2.svg", "svg-icon-lg svg-icon-danger") }}--}}
            {{--                    </span>--}}
            {{--                </div>--}}

            {{--                --}}{{-- Text --}}
            {{--                <div class="d-flex flex-column font-weight-bold">--}}
            {{--                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Briviba SaaS</a>--}}
            {{--                    <span class="text-muted">PHP, SQLite, Artisan CLIмм</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            --}}{{-- Item --}}
            {{--            <div class="d-flex align-items-center mb-2">--}}
            {{--                --}}{{-- Symbol --}}
            {{--                <div class="symbol symbol-40 symbol-light-info mr-5">--}}
            {{--                    <span class="symbol-label">--}}
            {{--                        {{ Metronic::getSVG("media/svg/icons/Communication/Shield-user.svg", "svg-icon-lg  svg-icon-info") }}--}}
            {{--                    </span>--}}
            {{--                </div>--}}

            {{--                --}}{{-- Text --}}
            {{--                <div class="d-flex flex-column font-weight-bold">--}}
            {{--                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Briviba SaaS</a>--}}
            {{--                    <span class="text-muted">PHP, SQLite, Artisan CLIмм</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            --}}{{-- Item --}}
            {{--            <div class="d-flex align-items-center mb-2">--}}
            {{--                --}}{{-- Symbol --}}
            {{--                <div class="symbol symbol-40 symbol-light-info mr-5">--}}
            {{--                    <span class="symbol-label">--}}
            {{--                        {{ Metronic::getSVG("media/svg/icons/Communication/Mail-notification.svg", "svg-icon-lg  svg-icon-info") }}--}}
            {{--                    </span>--}}
            {{--                </div>--}}

            {{--                --}}{{-- Text --}}
            {{--                <div class="d-flex flex-column font-weight-bold">--}}
            {{--                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Briviba SaaS</a>--}}
            {{--                    <span class="text-muted">PHP, SQLite, Artisan CLIмм</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}

            {{--            --}}{{-- Item --}}
            {{--            <div class="d-flex align-items-center mb-2">--}}
            {{--                --}}{{-- Symbol --}}
            {{--                <div class="symbol symbol-40 symbol-light-info mr-5">--}}
            {{--                    <span class="symbol-label">--}}
            {{--                        {{ Metronic::getSVG("media/svg/icons/Design/Bucket.svg", "svg-icon-lg  svg-icon-info") }}--}}
            {{--                    </span>--}}
            {{--                </div>--}}

            {{--                --}}{{-- Text --}}
            {{--                <div class="d-flex flex-column font-weight-bold">--}}
            {{--                    <a href="#" class="text-dark text-hover-primary mb-1 font-size-lg">Briviba SaaS</a>--}}
            {{--                    <span class="text-muted">PHP, SQLite, Artisan CLIмм</span>--}}
            {{--                </div>--}}
            {{--            </div>--}}
        </div>
    </div>

    {{-- Tabpane --}}
    <div class="tab-pane " id="topbar_notifications_events" role="tabpanel">
        {{-- Nav --}}
        <div class="navi navi-hover scroll my-4" data-scroll="true" data-height="310" data-mobile-height="310">
            @php $data=jobAlerts();  @endphp
            @if($data=='0')
                <p  class="navi-item ">
                <div class="navi-link m-3" >
                    <div class="navi-text text-center" style="border: 1px solid #eef0f8 !important; padding: 12px">
                        <div class="font-weight-bold my-4" >
                            No notification found
                        </div>

                    </div>
                </div>
                </p>
            @else
                @php  $url= (Request::segment(1) == "admin")?'admin/notifications':'user/notifications' @endphp
                @foreach($data as $job)

                    <a style="pointer:default;" href="{{ url($url)}}" class="navi-item">
                        <div class="navi-link">
                            <div class="navi-icon  ">
                                <i class="flaticon2-paper-plane text-danger"></i>
                            </div>
                            <div class="navi-text ">
                                <div class="font-weight-bold">
                                    {{$job->subject}}
                                </div>
                                <div class="text-muted float-right ">
                                    {{Carbon\Carbon::parse($job->created_at)->diffForHumans()}}
                                </div>
                            </div>
                        </div>
                    </a>
                @endforeach
            @endif()
        </div>
    </div>
    <div class="tab-pane " id="topbar_notifications_events1" role="tabpanel">
        {{-- Nav --}}
        <div class="navi navi-hover scroll my-4" data-scroll="true" data-height="310" data-mobile-height="310">
            @php $data=allNotifications();  @endphp
            @if(count($data)=='0')
                <p  class="navi-item ">
                <div class="navi-link  m-3" >
                    <div class="navi-text text-center" style="border: 1px solid #eef0f8 !important; padding: 12px">
                        <div class="font-weight-bold my-4" >
                            No Latest Notification Found
                        </div>

                    </div>
                </div>
                </p>
            @else
                @foreach($data as $job)
                    <p  style="pointer:default !important;" class="navi-item ">
                    <div class="navi-link d-inline-flex pr-1">
                        <div class="navi-icon d-inline-flex pr-2">
                            <i class="flaticon2-paper-plane text-danger pl-2"></i>
                        </div>
                        <div class="navi-text ">
                            <div class="font-weight-bold pr-2">
                                {{$job->subject}}

                                <span class="text-muted float-right text-right pl-4 ">
                                        {{Carbon\Carbon::parse($job->created_at)->diffForHumans()}}
                                    </span>
                            </div>

                        </div>
                    </div>
                    </p>
                @endforeach
            @endif()
        </div>
    </div>

    {{-- Tabpane --}}
    <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
        {{-- Nav --}}
        <div class="d-flex flex-center text-center text-muted min-h-200px">
            All caught up!
            <br/>
            No new notifications.
        </div>
    </div>
</div>
