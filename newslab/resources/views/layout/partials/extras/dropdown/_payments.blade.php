
@if (config('layout.extras.notifications.dropdown.style') == 'light')
    <div class="d-flex flex-column pt-12 bg-dark-o-5 rounded-top">
        {{-- Title --}}
        <h4 class="d-flex flex-center">
            <span class="text-dark">Notifications</span>
            <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">{{(notificationCount())}} new</span>
        </h4>

        {{-- Tabs --}}
        <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-primary mt-3 px-8" role="tablist">
            <li class="nav-item">
                <a class="nav-link active show" data-toggle="tab" href="#topbar_notifications_notifications">System Alerts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_events">Job Alerts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#topbar_notifications_logs">Logs</a>
            </li>
        </ul>
    </div>
@else
    <div class="d-flex flex-column pt-12 bgi-size-cover bgi-no-repeat rounded-top" style="background-image: url('{{ asset('media/misc/bg-1.jpg') }}')">
        {{-- Title --}}
        <h4 class="d-flex flex-center rounded-top">
            <span class="text-white">Payments</span>
            <span class="btn btn-text btn-success btn-sm font-weight-bold btn-font-md ml-2">{{paymentCount()}} </span>
        </h4>

        {{-- Tabs --}}
        <ul class="nav nav-bold nav-tabs nav-tabs-line nav-tabs-line-3x nav-tabs-line-transparent-white nav-tabs-line-active-border-success mt-3 px-8" role="tablist">
        </ul>
    </div>
@endif

{{-- Content --}}
<div class="tab-content px-4">
    {{-- Tabpane --}}


    <div class="tab-pane active show" id="topbar_notifications_events1" role="tabpanel">
        {{-- Nav --}}
        <div class="navi navi-hover scroll my-4" data-scroll="true" data-height="100" data-mobile-height="310">
            @php $data=driverTotal();  @endphp
            @if($data)
                <p  class="navi-item ">
                <div class="navi-link  m-3" >
                    <div class="navi-text text-center" style="border: 1px solid #eef0f8 !important; padding: 12px">
                        <div class="font-weight-bold " >
                            Balance: {{$data}}
                        </div>
                        <div class="font-weight-bold " >
                            Credited Amount: 0
                        </div>
                        <div class="font-weight-bold " >
                            Debited Amount: 0
                        </div>

                    </div>
                </div>
                </p>
            @endif()
        </div>
    </div>

    {{-- Tabpane --}}
    <div class="tab-pane" id="topbar_notifications_logs" role="tabpanel">
        {{-- Nav --}}
        <div class="d-flex flex-center text-center text-muted min-h-200px">
            All caught up!
            <br/>
            No new notifications.
        </div>
    </div>
</div>
