@php
	$direction = config('layout.extras.user.offcanvas.direction', 'right');
@endphp


 {{-- User Panel --}}
<div id="kt_quick_user" class="offcanvas offcanvas-{{ $direction }} p-10">
	{{-- Header --}}
	<div class="offcanvas-header d-flex align-items-center justify-content-between pb-5">
		<h3 class="font-weight-bold m-0">
			 Profile
{{--			<small class="text-muted font-size-sm ml-2">0 message</small>--}}
		</h3>
		<a href="#" class="btn btn-xs btn-icon btn-light btn-hover-primary" id="kt_quick_user_close">
			<i class="ki ki-close icon-xs text-muted"></i>
		</a>
	</div>

	{{-- Content --}}
    <div class="offcanvas-content pr-5 mr-n5">
		{{-- Header --}}
        <div class="d-flex align-items-center mt-5">
            <div class="symbol symbol-100 mr-5">
                @if(@Auth::user()->profile->profile_pic)
                    @php $profile_pic=asset('/images/profile'.'/'.Auth::user()->profile->profile_pic);  @endphp
                @else
                    @php $profile_pic=asset('/images/profile/'.@Auth::user()->profile->profile_pic)  @endphp
                @endif
                {{--                <div class="symbol-label 1" style="background-image:url('{{ asset($profile_pic) }}')"></div>--}}
                <div class="symbol-label img-wrapper">
                    <img class="img-fluid" src="{{$profile_pic}}" alt="" style="max-width: 100px !important;max-height: 100px">
                </div>
                <i class="symbol-badge bg-success"></i>
            </div>
            <div class="d-flex flex-column">
                <a href="#" class="font-weight-bold font-size-h5 text-dark-75 text-hover-primary">
                    {{@Auth::user()->name}}
                </a>
                <div class="text-muted mt-1">
                    {{@Auth::user()->profile->company_name}}
                </div>
                <div class="navi mt-2">
                    <a href="#" class="navi-item">
                        <span class="navi-link p-0 pb-2">
                            <span class="navi-icon mr-1">
								{{ Metronic::getSVG("media/svg/icons/Communication/Mail-notification.svg", "svg-icon-lg svg-icon-primary") }}
							</span>
                            <span class="navi-text text-muted text-hover-primary">{{@Auth::user()->email}}</span>
                        </span>
                    </a>
                </div>

                <div class="navi mt-2">
                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="navi-item">
                        <span class="navi-link p-0 pb-2">
                            <i class="la la-sign-out mr-1 logoutClick" style="color: #3699ff;font-size: 20px;font-weight: bolder;"></i>
                            <span class="navi-text text-muted text-hover-primary ml-2">Log Out</span>
                        </span>
                    </a>
                </div>
{{--                @if(Request::segment(1) == "admin")--}}
{{--                <div class="navi mt-2">--}}
{{--                    <a  target="_blank" href="{{route('login')}}" class="navi-item">--}}
{{--                        <span class="navi-link p-0 pb-2">--}}
{{--                            <i class="la la-sign-in mr-1 logoutClick" style="color: #3699ff;font-size: 20px;font-weight: bolder;"></i>--}}
{{--                            <span class="navi-text text-muted text-hover-primary ml-2">Login as user</span>--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                    @endif--}}
            </div>
        </div>

        <form id="logout-form" @if(Request::segment(1) == "admin") action="{{ url('admin/logout') }}" @else action="{{ url('user/logout') }}" @endif method="POST" class="d-none">
         @csrf
        </form>
		<div class="separator separator-dashed mt-8 mb-5"></div>
{{--@if(Auth()->role_id)--}}
		{{-- Nav --}}
		<div class="navi navi-spacer-x-0 p-0">
		    {{-- Item --}}

		    <a href="@if(Request::segment(1) == "admin"){{ route('admin.personal.info') }} @else {{ route('user.personal.info') }} @endif " class="navi-item">
		        <div class="navi-link">
		            <div class="symbol symbol-40 bg-light mr-3">
		                <div class="symbol-label">
							{{ Metronic::getSVG("media/svg/icons/General/Notification2.svg", "svg-icon-md svg-icon-success") }}
						</div>
		            </div>
		            <div class="navi-text">
		                <div class="font-weight-bold">
		                    Personal Profile
		                </div>
		            </div>
		        </div>
		    </a>
            {{-- Item --}}
            @if(Auth()->user()->role_id == '4')
            <a href="@if(Request::segment(1) == "user"){{ route('user.bussiness.info') }} @endif" class="navi-item">
                <div class="navi-link">
                    <div class="symbol symbol-40 bg-light mr-3">
                        <div class="symbol-label">
                            {{ Metronic::getSVG("media/svg/icons/Communication/Mail-opened.svg", "svg-icon-md svg-icon-primary") }}
                        </div>
                    </div>

                        <div class="font-weight-bold">
                             Business Profile
                        </div>

                </div>
            </a>
            @endif

		    {{-- Item --}}
            @if(Auth()->user()->role_id == '1')
		    <a href="{{route('admin.tickets')}}"  class="navi-item">
		        <div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
 						   {{ Metronic::getSVG("media/svg/icons/Shopping/Chart-bar1.svg", "svg-icon-md svg-icon-warning") }}
 					   </div>
				   	</div>
		            <div class="navi-text">
		                <div class="font-weight-bold">
		                    Tickets
		                </div>
		            </div>
		        </div>
		    </a>
            @endif
            @if(Auth()->user()->role_id != '1')
                <a href="{{route('user.userTickets')}}"  class="navi-item">
                    <div class="navi-link">
                        <div class="symbol symbol-40 bg-light mr-3">
                            <div class="symbol-label">
                                {{ Metronic::getSVG("media/svg/icons/Shopping/Chart-bar1.svg", "svg-icon-md svg-icon-warning") }}
                            </div>
                        </div>
                        <div class="navi-text">
                            <div class="font-weight-bold">
                                Tickets
                            </div>
                            <div class="text-muted">
                                Inbox and tasks
                            </div>
                        </div>
                    </div>
                </a>
            @endif
		    <a href="@if(Request::segment(1) == "admin")  {{route('admin.notification.setting')}} @else {{route('user.notification.setting') }}" @endif"  class="navi-item">
		        <div class="navi-link">
					<div class="symbol symbol-40 bg-light mr-3">
						<div class="symbol-label">
							{{ Metronic::getSVG("media/svg/icons/Files/Selected-file.svg", "svg-icon-md svg-icon-danger") }}
						</div>
				   	</div>
		            <div class="navi-text">
		                <div class="font-weight-bold">
		                    Notification Settings
		                </div>

		            </div>

                </div>

		    </a>


		</div>

		{{-- Separator --}}

		<div class="separator separator-dashed my-7"></div>
        @php $data=systemAlerts(); @endphp
        @if(count($data)==0)
		{{-- Notifications --}}
		<div>
			{{-- Heading --}}
        	<h5 class="mb-5">
            	Recent Notifications
        	</h5>

			{{-- Item --}}
	        <div class="d-flex align-items-center bg-light-info rounded p-5 gutter-b">
	            <span class="svg-icon svg-icon-warning mr-5">
	                {{ Metronic::getSVG("media/svg/icons/Home/Library.svg", "svg-icon-lg") }}
	            </span>

	            <div class="d-flex flex-column flex-grow-1 mr-2">
	                <p class="cursor-pointer font-weight-normal text-dark-75 font-size-lg mb-1">No notification found</p>
{{--	                <span class="text-muted font-size-sm">10 secs ago</span>--}}
	            </div>

	        </div>
		</div>
        @else


            <div>
                {{-- Heading --}}
                <h5 class="mb-5">
                    Recent Notifications
                </h5>

                {{-- Item --}}
                @foreach($data as $dat)
                <div class="d-flex align-items-center bg-light-success rounded p-5 gutter-b">
	            <span class="svg-icon svg-icon-warning mr-5">
	                {{ Metronic::getSVG("media/svg/icons/Home/Library.svg", "svg-icon-lg") }}
	            </span>

                    <div class="d-flex flex-column flex-grow-1 mr-2">
                        <a href="#" class="font-weight-normal text-dark-75 text-hover-primary font-size-lg mb-1">{{$dat->subject}}</a>
                       <span class="text-muted font-size-sm">{!!$dat->detail!!}</span>
                    </div>

                </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
