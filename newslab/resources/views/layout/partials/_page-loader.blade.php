{{-- Page Loader Types --}}

{{-- Default --}}
@if (config('layout.page-loader.type') == 'default')
    <div class="page-loader">
        <div class="spinner spinner-primary"></div>
    </div>
@endif

{{-- Spinner Message --}}
{{--@if (config('layout.page-loader.type') == 'spinner-message')--}}
{{--    <div class="page-loader page-loader-base">--}}
{{--        <div class="blockui">--}}
{{--            <span>Please wait...</span>--}}
{{--            <span><div class="spinner spinner-primary"></div></span>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endif--}}

{{-- Spinner Logo --}}
{{--@if (config('layout.page-loader.type') == 'spinner-logo')--}}

{{--@endif--}}



{{--<div class="page-loader page-loader-logo">--}}
{{--    <img alt="{{ config('app.name') }}" src="{{ asset('media/logos/slabcar-logo.png') }}" style="max-height: 50px;max-width: 100px;"/>--}}
{{--    <div class="spinner spinner-primary"></div>--}}
{{--</div>--}}
